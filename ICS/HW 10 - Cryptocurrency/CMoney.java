import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.security.*;
import java.security.spec.*;
import java.util.*;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.regex.*;
import java.math.*;
import javax.crypto.Cipher;

public class CMoney{

    public static KeyPair buildKeyPair() throws NoSuchAlgorithmException {
        final int keySize = 2048;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);      
        return keyPairGenerator.genKeyPair();
    }

    public static byte[] encrypt(PrivateKey privateKey, String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);  

        return cipher.doFinal(message.getBytes());  
    }
    
    public static byte[] decrypt(PublicKey publicKey, byte [] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        
        return cipher.doFinal(encrypted);
    }
    static void SaveKeyPair(String filename, KeyPair keyPair) throws Exception {
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(keyPair.getPublic().getEncoded());
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(keyPair.getPrivate().getEncoded());
        PrintWriter fout = new PrintWriter(new FileOutputStream(filename));
        fout.println(getHexString(x509EncodedKeySpec.getEncoded()));
        fout.println(getHexString(pkcs8EncodedKeySpec.getEncoded()));
        fout.close();
        }
     static String getHexString(byte[] b) {
        String result = "";
        for (int i = 0; i < b.length; i++) {
            int val = b[i];
            if ( val < 0 )
            val += 256;
            if ( val <= 0xf )
            result += "0";
            result += Integer.toString(val, 16);
        }
        return result;
        }
    static byte[] getByteArray(String hexstring) {
	    byte[] ret = new byte[hexstring.length()/2];
	    for (int i = 0; i < hexstring.length(); i += 2) {
	        String hex = hexstring.substring(i,i+2);
	        if ( hex.equals("") )
		    continue;
	        ret[i/2] = (byte) Integer.parseInt(hex,16);}
            return ret;}
    static KeyPair LoadKeyPair(String filename) throws Exception {
        Scanner sin = new Scanner(new File(filename));
        byte[] encodedPublicKey = getByteArray(sin.next());
        byte[] encodedPrivateKey = getByteArray(sin.next());
        sin.close();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);
        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
        return new KeyPair(publicKey, privateKey);
        }
        static String getHashOfFile(String filename) throws Exception {
        byte[] filebytes = Files.readAllBytes(Paths.get(filename));
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] encodedHash = digest.digest(filebytes);
        return getHexString(encodedHash);
        } 
    public static void name(){
        System.out.println("QuitCoin");
    }
    public static void genesis(String fileName){
        String fileContent = "Welcome to QuitCoin: The Coin that Made Me Want To Quit It All.";
       try {
            File file = new File(fileName);
            FileWriter fw = new FileWriter(file);
            fw.write(fileContent);
            fw.close();
        } catch ( IOException e ) {
            e.printStackTrace();}
        System.out.println("Genesis block created in 'block_0.txt'");
    }
    public static void generate(String fileName){
       try{
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(1024, new SecureRandom());
        KeyPair pair = generator.generateKeyPair();
        Signature sig = Signature.getInstance("SHA256withRSA");
        sig.initSign(pair.getPrivate());
        String plainText = pair.getPublic().toString();
        sig.update(plainText.getBytes());
        byte[] signat = sig.sign();
        SaveKeyPair(fileName, pair);
        String ss = Base64.getEncoder().encodeToString(signat);
        PrintWriter out = new PrintWriter(new FileWriter(fileName, true));
        out.append("\n" + ss);
        out.close();
       /* File file = new File(fileName);
        Scanner scan = new Scanner(file);
        String s = scan.nextLine();
        scan.close();*/
        System.out.println("New wallet generated in " + fileName + " with signature: " + ss.substring(0, 16));}
        catch ( IOException e ) {
            e.printStackTrace();}
        catch ( Exception e){
            e.printStackTrace();
        }
    }
    public static String address(String fileName){
       try{ File file = new File(fileName);
        Scanner scan = new Scanner(file);
        scan.nextLine();
        scan.nextLine();
        scan.nextLine();
        String v = scan.nextLine();
        scan.close();
        System.out.println(fileName + " wallet signature: " + v.substring(0, 16));
        return v.substring(0,16);}
        catch ( IOException e ) {
            e.printStackTrace();}
        catch ( Exception e){
            e.printStackTrace();}
        return null;
    }
    public static void fund(String d, double a, String fileName) throws IOException{
        String o = "";
        Date date = new Date();
        o += "From: Mom \n";
        o += "To: " + d + "\n";
        o += "Amount: " + a + "\n";
        o += "Date: " + date;
        try(FileWriter ot = new FileWriter(fileName, false)){
            ot.write(o);
        }
        System.out.println("Funded wallet " + d + " with " + a + " QuitCoin on " + date);
    }
    public static void transfer(String s, String d, double a, String fileName) throws Exception{
        String o = "";
        String h = "";
        Date date = new Date();
        o += "From: " + address(s);
        o += "To: " + d + "\n";
        o += "Amount: " + a + "\n";
        o += "Date: " + date;
        try(FileWriter ot = new FileWriter(fileName, false)){
            ot.write(o);
        }
        try{
            h = getHashOfFile(fileName);}
            catch (Exception e){
                e.printStackTrace();
            }
        KeyPair kp = null;
        try{
            kp = LoadKeyPair(s);
        } catch (Exception e){
            e.printStackTrace();
        }
        PrivateKey privkey = kp.getPrivate();
        byte[] t = encrypt(privkey, h);
        o += "\n" + getHexString(t);
        try(FileWriter fout = new FileWriter(fileName, false)){
            fout.write(o);
        }
        System.out.println("Transferred " + a + " from " + s + " to " + d + " and the statement to " + fileName + " on " + d);
    }
    public static double findAmount(String str){
        StringBuffer sb = new StringBuffer();
        Pattern pat = Pattern.compile("[0-9]+\\.[0-9]+ ");
        Matcher mat = pat.matcher(str);
        while(mat.find()){
            sb.append(mat.group());
        }
        String dub = sb.toString();
        dub = dub.substring(0, dub.indexOf(' '));
        return Double.valueOf(dub);
    }
    public static double balance(String a) throws IOException{
        double r = 0.0;
        int l = 0;
        BufferedReader read = new BufferedReader(new FileReader("ledger.txt"));
        while (read.readLine() != null){
            l++;
        }
        read.close();
        for (int i = 0; i < l; i++){
            String line = Files.readAllLines(Paths.get("ledger.txt")).get(i);
            if(line.indexOf(a) >= 0){
                if(line.indexOf(a) == 0){
                    r -= findAmount(line);}
                    else{
                        r += findAmount(line);
                    }
                }
            }
            int n = 1;
            int find = 1;
            String b = "";
            while(find == 1){
                b = "block_" + String.valueOf(n) + ".txt";
                File tf = new File(b);
                if(!tf.exists()){
                    find = 0;
                }
                else{
                    int l1 = 0;
                    BufferedReader rdr = new BufferedReader(new FileReader(b));
                    while(rdr.readLine() != null){
                        l1++;
                    }
                    rdr.close();
                    for(int i = 0; i < l1; i++){
                        String linee = Files.readAllLines(Paths.get(b)).get(i);
                        if(linee.indexOf(a) >= 0){
                            if (linee.indexOf(a) == 0){
                                r -= findAmount(linee);
                            }
                            else{
                                r += findAmount(linee);
                            }
                        }
                    }n++;
                }
            } return r;
        }
    public static void verify(String wall, String trans) throws Exception{
        BufferedReader rdr = new BufferedReader(new FileReader(trans));
        int l = 0;
        while(rdr.readLine() != null){
            l++;
        }
        rdr.close();
        if(l >= 5){
            String l5 = Files.readAllLines(Paths.get(trans)).get(4);
            KeyPair kp = null;
            try{
                kp = LoadKeyPair(wall);
            } catch (Exception e){
                e.printStackTrace();
            }
            PublicKey pubkey = kp.getPublic();
            byte[] t = decrypt(pubkey, getByteArray(l5));
            String f = "";
            Scanner sc = new Scanner(new File(trans));
            for(int i = 0; i < 3; i++){
                f += sc.nextLine() + "\n";
            }
            f += sc.nextLine();
            sc.close();
            MessageDigest dig = MessageDigest.getInstance("SHA-256");
            byte[] encodedH = dig.digest(f.getBytes());
            String l1 = Files.readAllLines(Paths.get(trans)).get(0);
            String l2 = Files.readAllLines(Paths.get(trans)).get(1);
            String l3 = Files.readAllLines(Paths.get(trans)).get(2);
            String l4 = Files.readAllLines(Paths.get(trans)).get(3);
            String frm = l1.substring(l1.lastIndexOf(' '));
            String to = l2.substring(l2.lastIndexOf(' '));
            String amt = l3.substring(l3.lastIndexOf(' '));
            String dte = l4.substring(l4.lastIndexOf(": ") + 2);
            double mon = Double.valueOf(amt);
            if(getHexString(encodedH).equals(getByteArray(getHexString(t))) && balance(frm) >= mon){
                String led = frm.substring(1) + " transferred " + amt + " to " + to + " on " + dte;
                try(FileWriter output = new FileWriter("ledger.txt", true)){
                    output.write(led + "\n");
                }
                System.out.println("The transaction in file " + trans + " with wallet " + wall + " is valid, and was written to the ledger.");
            }
        }else{
            String l2 = Files.readAllLines(Paths.get(trans)).get(1);
            String to = l2.substring(l2.lastIndexOf(' ') + 1);
            String l3 = Files.readAllLines(Paths.get(trans)).get(2);
            String amt = l3.substring(l3.lastIndexOf(' ') + 1);
            String l4 = Files.readAllLines(Paths.get(trans)).get(3);
            String dte = l4.substring(l4.lastIndexOf(": ") + 2);
            String led = "Mom transferred " + amt + " to " + to + " on " + dte;
            try(FileWriter output = new FileWriter("ledger.txt", true)){
                output.write(led + "\n");
            }
        System.out.println("Any funding request (i.e., from Mom) is considered valid, written to the ledger.");
        }
    }
    public static void mine(int dif) throws Exception{
        String zs = "";
        for(int k = 0; k < dif; k++){
            zs += "0";
        }
        int i = 0;
        int find = 1;
        String b = "";
        while(find == 1){
            b= "block_" + String.valueOf(i) + ".txt";
            File tf = new File(b);
            if(!tf.exists()){
                find = 0;
            }
            else{
                i++;
            }
        }
        String prevB = "block_" + String.valueOf(i-1)+".txt";
        String tmp = getHashOfFile(prevB);
        tmp += "\n";
        File f = new File("ledger.txt");
        Scanner sc = new Scanner(f);
        while(sc.hasNextLine()){
            tmp += sc.nextLine() + "\n";
        }
        new PrintWriter("ledger.txt").close();
        int l = tmp.length();
        String non = "";
        for(int j = 0; j < Integer.MAX_VALUE; j++){
            tmp = tmp.substring(0, l);
            non = String.valueOf(j);
            tmp += non;
            try(FileWriter otpt = new FileWriter(b, false)){
                otpt.write(tmp);
            }
            if (getHashOfFile(b).substring(0, dif).equals(zs)){
                break;
            }
        }
        System.out.println("Ledger transactions moved to " + b + " and mined with difficulty " + dif + " and non " + non);
    }
    public static boolean validate() throws Exception{
        boolean ret = true;
        int i = 0;
        int find = 1;
        String b = "";
        while(find == 1){
            b = "block_" + String.valueOf(i) + ".txt";
            File tf = new File(b);
            if(!tf.exists()){
                find = 0;
            } else{
                i++;
            }
        }
        for(int j = 1; j < 1; j++){
            b = "block_" + String.valueOf(j) + ".txt";
            String l = Files.readAllLines(Paths.get(b)).get(0);
            String prevB = "block_" + String.valueOf(j-1) + ".txt";
            String t = getHashOfFile(prevB);
            if(!l.equals(t)){
                ret = false;
                break;
            }
        }
        if(ret){
            System.out.println("The entire blockchain is valid");
        }
        return ret;
    }
    public static void main(String[] args) throws Exception {
        CMoney c = new CMoney();
            if(args[0].equals("name")){
                c.name();
            }
            else if(args[0].equals("genesis")){
                c.genesis(args[1]);
            }
            else if(args[0].equals("generate")){
                c.generate(args[1]);
            }
            else if(args[0].equals("address")){
                System.out.println(c.address(args[1]));
            }
            else if(args[0].equals("fund")){
                c.fund(args[1], Double.valueOf(args[2]), args[3]);
            }
            else if(args[0].equals("transfer")){
                c.transfer(args[1], args[2], Double.valueOf(args[3]), args[4]);
            }
            else if(args[0].equals("balance")){
                System.out.println("The balance for wallet " + (args[1]) + " is: " + c.balance(args[1]));
            }
            else if(args[0].equals("verify")){
                c.verify(args[1], args[2]);
            }
            else if(args[0].equals("mine")){
                c.mine(Integer.valueOf(args[1]));
            }
            else if(args[0].equals("validate")){
                c.validate();
            }
        }
}