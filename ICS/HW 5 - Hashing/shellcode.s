; shellcode.s 
; Ranjodh Sandhu (rss6py)

global shellcode
section .text

shellcode:
    jmp code

string:
    db "Ranjodh Sandhu, your grade on this assignment is an A"

code:

    xor rax, rax 
    mov al, 1
    mov rdi, rax
    lea rsi, [rel string]
    mov byte [rsi+53], 0x7
    add byte [rsi+53], 0x3
    xor rdx, rdx
    mov dl, 54
    syscall
    mov al, 60
    xor rdi, rdi
    syscall
    
