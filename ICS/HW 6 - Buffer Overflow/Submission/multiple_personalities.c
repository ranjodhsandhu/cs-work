#include <stdio.h>
#include <unistd.h>

/* do something innocent */
int main_good(int ac, char *av[]) {
  char buf[10];
  fprintf(stdout, "No such thing as a life that's better than yours\n");
  fflush(stdout);
  fgets(buf, 10, stdin);
  return 0;
}

/* do something evil */
int main_evil(int ac, char *av[]) {
  char buf[10];
  fprintf(stdout, "Uploading browser search history...0 \n");
  fflush(stdout);
  sleep(1);
  fprintf(stdout, "Uploading browser search history...10 \n");
  fflush(stdout);
  sleep(1);
  fprintf(stdout, "Uploading browser search history...25 \n");
  sleep(1);
  fprintf(stdout, "Uploading browser search history...37 \n");
  sleep(1);
  fprintf(stdout, "Uploading browser search history...61 \n");
  sleep(1);
  fprintf(stdout, "Uploading browser search history...85 \n");
  sleep(1);
  fprintf(stdout, "Uploading browser search history...100 \n");
  fflush(stdout);
  fgets(buf, 10, stdin);
  return 0;
}
