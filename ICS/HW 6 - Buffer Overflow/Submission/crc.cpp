#include <boost/crc.hpp>
#include <boost/cstdint.hpp>
#include <fstream>
#include <iostream>
#include <vector>
#include <bits/stdc++.h>
using namespace std;
using namespace boost;

int getcrc16(string name) {
    crc_16_type crc;
    ifstream is(name.c_str(), ios::in | ios::binary);
    streampos current = is.tellg();
    is.seekg(0, ios::end);
    size_t s = is.tellg();
    is.seekg(current, ios::beg);
    vector<char> vec(s);
    is.read(&vec[0], s);
    crc.process_bytes(&vec[0], s);
    return crc.checksum();
}

void intoout(string inname, string outname){
    ifstream input(inname.c_str(), ios::in);
    ofstream otpt("output.txt", ios::out | ios::trunc);
    otpt << input.rdbuf();
    otpt << "\nabcdefg";

}


void pm(char charar[], string p, int a, int b, vector<string> &vs){
        if(b == 0){
            vs.push_back(p);
            return;
        }
        for(int i = 0; i < a; i++){
            string x = p + charar[i];
            pm(charar, x, a, b-1, vs);
        }

}


int main(int argc, char** argv){
    if(argc < 2){
        cout << "Error: Include Correct Parameters." << endl;
        exit(-1);
    }
    string outputname = "output.txt";
    intoout(argv[1], outputname);
    char ar[97];
    char *hexadecimal = argv[2];
    int t = (int)strtol(hexadecimal, NULL, 16);
    size_t sizet = t;
    int c = 0;
    for(int i = 32; i < 128; i++){
        ar[c] = (char)i;
        c++;
    }
    vector<string> a;
    pm(ar, "", 97, 3, a);
    vector<string>::iterator it = a.begin();
    while(true){
        if(sizet == getcrc16(outputname)){
            break;
        }
        intoout(argv[1], outputname);
        ofstream otptd("output.txt", ios::out | ios::app);
        otptd << *it;
        ++it;}
        return 0;
    }
    

 




