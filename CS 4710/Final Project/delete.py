import pandas as pd
newdf = pd.read_csv("RelatedSeries.csv")
newdf['Date'] = pd.to_datetime(newdf['Date']).dt.strftime('%Y-%m-%d')
newdf.to_csv("RelatedSeries.csv", index=False)