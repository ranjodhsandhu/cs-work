import pandas as pd
import matplotlib.pyplot as plt
df = pd.read_csv("StatesAggregateData.csv")

#List of states that voted for Trump
votedTrumpList = ['AL', 'AK', 'AR', 'ID', 'KS', 'KY', 'LA', 'MS', 'MO', 'NE', 'ND', 'OK', 'SC', 'SD', 'TN', 'TX', 'UT', 'WV', 'WY', 'AZ', 'GA', 'IN', 'MO', 'NC', 'FL', 'IA', 'OH', 'PA', 'WI', 'MI']
#List of states that have ocean access
oceanStates = ['CT', 'DE', 'GA', 'FL', 'ME', 'MD', 'MA', 'NH', 'NY', 'NJ', 'NC', 'RI', 'SC', 'VA', 'AK', 'HI', 'OR', 'WA', 'CA', 'TX', 'MS', 'LA', 'AL']
#List of states that have Republican governors
repubGStates = ['AL', 'AK', 'AZ', 'AR', 'FL', 'GA', 'ID', 'IN', 'IA', 'MD', 'MA', 'MS', 'MO', 'NE', 'NH', 'ND', 'OH', 'OK', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'WV', 'WY']

#add a column of "no" title votedTrump
df = df.assign(votedTrump='no')
#modify the entries for states that are in votedTrumpList and change those votedTrump entries to "yes"
df.loc[df['State'].isin(votedTrumpList), 'votedTrump'] = 'yes'
#add a column of "no" title oceanAccess
df = df.assign(oceanAccess='no')
#modify the entries for states that are in oceanStates and change those oceanAccess entries to "yes"
df.loc[df['State'].isin(oceanStates), 'oceanAccess'] = 'yes'
#add a column of "no" title repubGovernor
df = df.assign(repubGovernor='no')
#modify the entries for states that are in repubGStates and change those repubGovernor entries to "yes"
df.loc[df['State'].isin(repubGStates), 'repubGovernor'] = 'yes'

#create dictionaries for the number of national parks and number of beaches
nationalParks = {"AK" : 8 , 
                 "AZ" : 3, 
                 "AR" : 1, 
                 "CA" : 9, 
                 "CO" : 4, 
                 "FL" : 3, 
                 "HI" : 2, 
                 "ID" : 1, 
                 "KY" : 1, 
                 "IL" : 1, 
                 "IN" : 1, 
                 "ME" : 1, 
                 "MI" : 1, 
                 "MN" : 1, 
                 "MO" : 1, 
                 "MT" : 2, 
                 "NV" : 2, 
                 "NM" : 1, 
                 "ND" : 1, 
                 "NC" : 1, 
                 "OH" : 1, 
                 "OR" : 1, 
                 "SC" : 1, 
                 "SD" : 2, 
                 "TN" : 1, 
                 "TX" : 2, 
                 "UT" : 5, 
                 "VA" : 1, 
                 "WA" : 3, 
                 "WY" : 2}
beaches = {'CT' : 66,
             'DE' : 21,
             'GA' : 41, 
             'FL' : 556, 
             'ME' : 59, 
             'MD' : 70, 
             'MA' : 605, 
             'NH' : 16, 
             'NY' : 346, 
             'NJ' : 219, 
             'NC' : 241, 
             'RI' : 230, 
             'SC' : 23, 
             'VA' : 45, 
             'AK' : 3, 
             'HI' : 385, 
              'OR' : 91, 
              'WA' : 1368, 
              'CA' : 438, 
              'TX' : 169, 
              'MS' : 22, 
              'LA' : 28, 
              'AL' : 25}


#create a column for the number of national parks with a default of 0
df = df.assign(numNationalParks=0)
#update the values for those states that are in the dictionary
df.loc[df['State'].isin(nationalParks.keys()), 'numNationalParks'] = df['State'].map(nationalParks)
#create a column for the number of beaches with a default of 0
df = df.assign(numBeaches=0)
#update the values for those states that are in the dictionary
df.loc[df['State'].isin(beaches.keys()), 'numBeaches'] = df['State'].map(beaches)



unempdf = pd.read_csv("unemploymentRate.csv")
abbrev = {
        'Alabama': 'AL',
        'Alaska': 'AK',
        'Arizona': 'AZ',
        'Arkansas': 'AR',
        'California': 'CA',
        'Colorado': 'CO',
        'Connecticut': 'CT',
        'Delaware': 'DE',
        'Florida': 'FL',
        'Georgia': 'GA',
        'Hawaii': 'HI',
        'Idaho': 'ID',
        'Illinois': 'IL',
        'Indiana': 'IN',
        'Iowa': 'IA',
        'Kansas': 'KS',
        'Kentucky': 'KY',
        'Louisiana': 'LA',
        'Maine': 'ME',
        'Maryland': 'MD',
        'Massachusetts': 'MA',
        'Michigan': 'MI',
        'Minnesota': 'MN',
        'Mississippi': 'MS',
        'Missouri': 'MO',
        'Montana': 'MT',
        'Nebraska': 'NE',
        'Nevada': 'NV',
        'New Hampshire': 'NH',
        'New Jersey': 'NJ',
        'New Mexico': 'NM',
        'New York': 'NY',
        'North Carolina': 'NC',
        'North Dakota': 'ND',
        'Ohio': 'OH',
        'Oklahoma': 'OK',
        'Oregon': 'OR',
        'Pennsylvania': 'PA',
        'Rhode Island': 'RI',
        'South Carolina': 'SC',
        'South Dakota': 'SD',
        'Tennessee': 'TN',
        'Texas': 'TX',
        'Utah': 'UT',
        'Vermont': 'VT',
        'Virginia': 'VA',
        'Washington': 'WA',
        'West Virginia': 'WV',
        'Wisconsin': 'WI',
        'Wyoming': 'WY'}
unempdf = unempdf[unempdf['State'].isin(abbrev.keys())]
unempdf['State'] = unempdf['State'].map(abbrev).fillna(unempdf['State'])


unempdf = unempdf[['State', '20-Jan', '20-Feb', '20-Mar', '20-Apr', '20-May', '20-Jun', '20-Jul', '20-Aug', '20-Sep']]
unempdf.columns = ['State', '1/20', '2/20', '3/20', '4/20', '5/20', '6/20', '7/20', '8/20', '9/20']

newdf = df.copy()
dateList = newdf['Date'].tolist()
for i in range(len(dateList)):
    dateList[i] = (dateList[i].split('-')[1] + "/" + dateList[i].split('-')[0][0:2]).lstrip("0")
newdf['Date'] = dateList
newdf.head()

for index, row in newdf.iterrows():
    if row[0] != '10/20' and row[0] != '11/20':
        df.loc[index,'unemploymentRate'] = unempdf.at[unempdf.State[unempdf.State == row[1]].index.tolist()[0], row[0]]

racedf = pd.read_csv("popBreakdownByRace.csv")
agedf = pd.read_csv("popBreakdownByAge.csv")

#convert state names to abbreviation
racedf = racedf[racedf['Location'].isin(abbrev.keys())]
racedf['Location'] = racedf['Location'].map(abbrev).fillna(racedf['Location'])
#convert decimals
racedf['White'] = racedf['White'] * 100
racedf['Black'] = racedf['Black'] * 100
racedf['Hispanic'] = racedf['Hispanic'] * 100
racedf['Asian'] = racedf['Asian'] * 100
#change column names
racedf.columns = ['State', 'percentOfPop_white', 'percentOfPop_black', 'percentOfPop_hispanic', 'percentOfPop_asian']
#merge
df = pd.merge(df, racedf,'left', on=['State'])

#repeat for age breakdown
agedf = agedf[agedf['Location'].isin(abbrev.keys())]
agedf['Location'] = agedf['Location'].map(abbrev).fillna(agedf['Location'])
agedf['Children 0-18'] = agedf['Children 0-18'] * 100
agedf['Adults 19-25'] = agedf['Adults 19-25'] * 100
agedf['Adults 26-34'] = agedf['Adults 26-34'] * 100
agedf['Adults 35-54'] = agedf['Adults 35-54'] * 100
agedf['Adults 55-64'] = agedf['Adults 55-64'] * 100
agedf['65+'] = agedf['65+'] * 100
agedf.columns = ['State', 'percentOfPop_0-18','percentOfPop_19-25', 'percentOfPop_26-34', 'percentOfPop_35-54', 'percentOfPop_55-64', 'percentOfPop_65+']
df = pd.merge(df, agedf,'left', on=['State'])

pddf = pd.read_csv("PopDensity.csv")

#convert state name
pddf = pddf[pddf['State'].isin(abbrev.keys())]
pddf['State'] = pddf['State'].map(abbrev).fillna(pddf['State'])
#filter to only states and population density
pddf = pddf[['State', 'Population Density (P/mi^2)']]
#rename population density column to match aggregate csv
pddf.columns = ['State', 'popDensity_(P/mi^2)' ]
#merge
df = pd.merge(df, pddf, 'left', on = ['State'])
df.to_csv("StateData.csv", index=False)