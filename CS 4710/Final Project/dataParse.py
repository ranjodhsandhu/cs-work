import csv
import os
from csv import writer
import pandas as pd 
#dictionary of state names and abbreviations
abbrev = { 'Alabama': 'AL', 'Alaska': 'AK', 'Arizona': 'AZ', 'Arkansas': 'AR', 'California': 'CA', 'Colorado': 'CO', 'Connecticut': 'CT', 'Delaware': 'DE', 'Florida': 'FL', 'Georgia': 'GA', 'Hawaii': 'HI', 'Idaho': 'ID', 'Illinois': 'IL', 'Indiana': 'IN', 'Iowa': 'IA', 'Kansas': 'KS', 'Kentucky': 'KY', 'Louisiana': 'LA', 'Maine': 'ME', 'Maryland': 'MD', 'Massachusetts': 'MA', 'Michigan': 'MI', 'Minnesota': 'MN', 'Mississippi': 'MS', 'Missouri': 'MO', 'Montana': 'MT', 'Nebraska': 'NE', 'Nevada': 'NV', 'New Hampshire': 'NH', 'New Jersey': 'NJ', 'New Mexico': 'NM', 'New York': 'NY', 'North Carolina': 'NC', 'North Dakota': 'ND', 'Ohio': 'OH', 'Oklahoma': 'OK', 'Oregon': 'OR', 'Pennsylvania': 'PA', 'Rhode Island': 'RI', 'South Carolina': 'SC', 'South Dakota': 'SD', 'Tennessee': 'TN', 'Texas': 'TX', 'Utah': 'UT', 'Vermont': 'VT', 'Virginia': 'VA', 'Washington': 'WA', 'West Virginia': 'WV', 'Wisconsin': 'WI', 'Wyoming': 'WY'}
def filterStates(f, statelabel):
    #open the CSV file as a dataframe
    df = pd.read_csv(f)
    #filter out locations that are not in the 50 states
    df = df[df[statelabel].isin(abbrev.keys())]
    #convert state names to abbreviations
    df[statelabel] = df[statelabel].map(abbrev).fillna(df[statelabel])
    return df

def mergeJHUCountyData(cases):
    #file name of the JHU Aggregate cases that will be opened
    f = "jhuAggregateCases.csv"
    #label of the column when added to CountiesAggregateData.csv
    label = 'JHUAggregate_ConfirmedCases'
    #names of the columns that need to be dropped
    dropColumns = ['UID', 'iso2', 'iso3', 'code3', 'FIPS', 'Country_Region', 'Lat', 'Long_', 'Combined_Key']
    #adjust the values if working with the deaths file
    if not cases:
        f = "jhuAggregateDeaths.csv"
        label = 'JHUAggregate_ConfirmedDeaths'
        dropColumns = ['UID', 'iso2', 'iso3', 'code3', 'FIPS', 'Country_Region', 'Lat', 'Long_', 'Combined_Key', 'Population']
    #convert states to abbreviated form and filter out data that is not part of 50 states
    df = filterStates(f, "Province_State")
    #filter out "unknown" areas
    df = df[df['Lat'] != 0]
    #drop the columns listed above
    df = df.drop(columns= dropColumns)
    #create a new dataframe that focus on only the cases 
    justCases = df.drop(columns=['Admin2', 'Province_State']).copy()
    #create a list of the counties merged with their states
    ids = (df['Admin2'] + df['Province_State']).tolist()
    #create an empty list that will contain the date, county, and state for each entry
    countyStateList = []
    #create a list of the dates that the file covers
    dates = justCases.columns
    #index for the list of dates
    j = 0
    #loop through the list of the county/state merge and for each value in the list create an entry for each day (reset the day once it reaches the end)
    for idd in ids:
        for i in range(len(df.columns) - 2):
            countyStateList.append([dates[j], idd[:-2] , idd[-2:]])
            j += 1
            if j == len(dates):
                j = 0
    #create a new dataframe from the list of entries
    newdf = pd.DataFrame(countyStateList, columns = ['Date','County', 'State'])
    #transpose the dataframe that is focused on only the cases
    justCases = justCases.transpose()
    #create an empty list to add all of the cases into
    casesList = []
    #get the list of columns for the cases only dataframe
    columns = list(justCases) 
    #loop through the list of columns
    for i in columns: 
        #convert the column to a list and append it to the list created for all cases
        casesList.append(justCases[i].tolist())
    #convert the list of lists into one long list
    cases_list = [item for sublist in casesList for item in sublist]
    #convert the list to a new column with the label set in the beginning
    newdf[label] = cases_list
    #if working with the deaths file, merge the data with CountiesAggregateData.csv
    newdf = newdf.drop(columns=["County"])
    newdf = newdf.groupby(by=["Date","State"]).sum().reset_index()
    newdf['Date'] = pd.to_datetime(newdf['Date']).dt.strftime('%Y-%m-%d')
    if not cases:
        df1 = pd.read_csv("StatesAggregateData.csv")
        joined = pd.merge(df1, newdf,'left', on=['Date', 'State'])
        joined.to_csv("StatesAggregateData.csv", index=False)
    #if working with the cases file, create the CountiesAggregateData.csv
    else:
        newdf.to_csv("StatesAggregateData.csv", index=False)
        
#run the above function for cases and then for deaths
def initializeAndAddJHUCountyData():
    mergeJHUCountyData(True)
    mergeJHUCountyData(False)

initializeAndAddJHUCountyData()