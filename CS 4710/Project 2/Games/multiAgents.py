# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util, math

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"
        ghostsPositions = currentGameState.getGhostPositions()
        curFood = currentGameState.getFood()

        temp = -10000000000000
        
        walls = currentGameState.getWalls()
        
        if walls[newPos[0]][newPos[1]] == True:
            return temp
        if action == 'Stop':
            return temp 
        for ghost in newGhostStates:
            if tuple(newPos) == ghost.getPosition():
                if (ghost.scaredTimer == 0):
                    return temp
        curFoodList = curFood.asList()
        foodDists = []
        for food in curFoodList:
            foodDists.append(-1 * manhattanDistance(newPos, food))
        if len(foodDists) > 0:
            return max(foodDists) + currentGameState.getScore()
        return currentGameState.getScore()

def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        def minimax(gameState, depth, ismax, act, agents):
            if gameState.isLose() or gameState.isWin() or depth == 0:
                return self.evaluationFunction(gameState), act
            if ismax:
                ismax = False
                maxi = float ("-inf")
                ac = ''
                for i in gameState.getLegalActions(0):
                    eva, mov = minimax(gameState.generateSuccessor(0, i), depth, ismax, i, 1)
                    if eva > maxi:
                        maxi = max(eva, maxi)
                        ac = i
                return maxi, ac
            else:
                mini = float ("inf")
                for i in gameState.getLegalActions(agents):
                    if agents == gameState.getNumAgents() - 1:
                        ismax = True
                        eva, mov = minimax(gameState.generateSuccessor(agents, i), depth - 1, ismax, i, agents)
                    else:
                        eva, mov = minimax(gameState.generateSuccessor(agents, i), depth, ismax, i, agents + 1)
                    if eva < mini:
                        mini = min(mini, eva)
                        ac = i
                return mini, ac

        return minimax(gameState, self.depth, True, "", 1)[1]
                


class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        def alphabet(gameState, depth, ismax, act, agents, alpha, beta):
            if gameState.isLose() or gameState.isWin() or depth == 0:
                return self.evaluationFunction(gameState), act
            if ismax:
                ismax = False
                maxi = float ("-inf")
                ac = ''
                for i in gameState.getLegalActions(0):
                    eva, mov = alphabet(gameState.generateSuccessor(0, i), depth, ismax, i, 1, alpha, beta)
                    if eva > maxi:
                        maxi = max(eva, maxi)
                        ac = i
                    alpha = max(maxi, alpha)
                    if alpha > beta:
                        return maxi, ac
                return maxi, ac
            else:
                mini = float ("inf")
                for i in gameState.getLegalActions(agents):
                    if agents == gameState.getNumAgents() - 1:
                        ismax = True
                        eva, mov = alphabet(gameState.generateSuccessor(agents, i), depth - 1, ismax, i, agents, alpha, beta)
                    else:
                        eva, mov = alphabet(gameState.generateSuccessor(agents, i), depth, ismax, i, agents + 1, alpha, beta)
                    if eva < mini:
                        mini = min(mini, eva)
                        ac = i
                    beta = min(mini, beta)
                    if beta < alpha:
                        return mini, ac
                return mini, ac

        return alphabet(gameState, self.depth, True, "", 1, float('-Inf'), float('Inf'))[1]

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        def expectimax(gameState, depth, ismax, act, agents):
            if gameState.isLose() or gameState.isWin() or depth == 0:
                return self.evaluationFunction(gameState), act
            if ismax:
                ismax = False
                maxi = float ("-inf")
                a = ''
                for i in gameState.getLegalActions(0):
                    eva, mov = expectimax(gameState.generateSuccessor(0, i), depth, ismax, i, 1)
                    if eva > maxi:
                        maxi = max(eva, maxi)
                        a = i
                return maxi, a
            else:
                acc = []
                mini = float ("inf")
                val = 0
                ac = ''
                p = (1/len(gameState.getLegalActions(agents)))
                for i in gameState.getLegalActions(agents):
                    if agents == gameState.getNumAgents() - 1:
                        ismax = True
                        eva, mov = expectimax(gameState.generateSuccessor(agents, i), depth - 1, ismax, i, agents)
                    else:
                        eva, mov = expectimax(gameState.generateSuccessor(agents, i), depth, ismax, i, agents + 1)
                    # acc.append(i)
                    val += (p * eva)
                    ac = i
                return val, ac
        return expectimax(gameState, self.depth, True, "", 1)[1]
        
        

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: Looks for food based off -1 * manhattanDistance. Similar to original evaluation function
    """
    "*** YOUR CODE HERE ***"
    curPos = currentGameState.getPacmanPosition()
    curFood = currentGameState.getFood()
    curFoodList = curFood.asList()
    foodDists = []
    for food in curFoodList:
        foodDists.append(-1 * manhattanDistance(curPos, food))
    if len(foodDists) > 0:
        return max(foodDists) + currentGameState.getScore()
    return currentGameState.getScore()
        

# Abbreviation
better = betterEvaluationFunction
