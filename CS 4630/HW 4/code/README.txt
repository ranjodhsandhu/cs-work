Ranjodh Sandhu - rss6py

In order to defend against the first attack from HW 3, I simply changed the behavior of the js when making a search query for a user that does not exist.
Instead of outputting the possibly malicious username, it instead renders 'profile/view' with an error message that says the user does not exist. It does not
specify the particular user being searched for. This way, the error message does not contain the malicious code.

To defend against the second attack, I ideally would have made a hidden input field in the transer form that would be set by the server rendering the page.
Then, in transfer, I would create an HMAC of a randomly generated key with the account's session and saved in the session. In post transfer, I would then
compare the value in the hidden field to the HMAC. If they don't match, an error would be presented. Unfortunately, this would require me modifying another file and 
we were only allowed to modify route.js, so I decided to take a simplified approach. I noticed that the second attack basically skips the "transfer" section and goes
straight to "post_transfer". I wanted to make sure a transfer was initiated in order to allow the post_transfer to go through. To do this, I created a global variable 
ti (which stands for transfer initiated) and set it equal to false. It is set to true whenever a transfer is initiated in "transfer" (similar to the hidden input field 
in the form), right before rendering and then set back to false once the transfer has either completed or it is found that the user does not exist. In post_transfer, I 
check if ti has been set to true before allowing the transfer to take place. If ti is false, then an error is presented. When using attack b, the page will still end on 
Wikipedia but no transfer will have taken place. Since attack b goes directly to post_transfer and the user does not initiate the transfer, ti is false and the transfer 
does not take place. 
