import boto3
import json
import time
iam = boto3.client('iam')
ec2 = boto3.client('ec2', region_name='us-east-1')

def pa():
    pa5_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:GetObject",
                    "dynamodb:PutItem"
                ],
                "Resource": [
                    "arn:aws:s3:::cs4740-bucket/*",
                    "arn:aws:dynamodb:*:347348566121:table/cs4740-table"
                ]
            }
        ]
    }






    p_response = iam.create_policy(
        PolicyName='pa5_p',
        PolicyDocument=json.dumps(pa5_policy),
        Description='string'
    )

    allow_ec2 = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": "ec2.amazonaws.com"
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }


    r_response = iam.create_role(
        RoleName='pa5',
        AssumeRolePolicyDocument= json.dumps(allow_ec2),
    )

    add_response = iam.attach_role_policy(
        RoleName='pa5',
        PolicyArn= p_response['Policy']['Arn']
    )
    instance_profile = iam.create_instance_profile(InstanceProfileName='pa5',Path='/')
    iam.add_role_to_instance_profile(InstanceProfileName='pa5', RoleName='pa5')
    time.sleep(10)

    sg_response = ec2.create_security_group(
    Description='Security group required for PA5',
    GroupName='pa5_secgroup')

    ir_response = ec2.authorize_security_group_ingress(
        GroupName='pa5_secgroup',
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 8080,
             'ToPort': 8080,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '128.143.0.0/16'}]}
        ])
    
    instance_response = ec2.run_instances(
    BlockDeviceMappings=[
        {
            'DeviceName': '/dev/xvda',
            'Ebs': {

                'DeleteOnTermination': True,
                'VolumeSize': 18,
                'VolumeType': 'gp2'
            },
        },
    ],
    ImageId='ami-0947d2ba12ee1ff75',
    InstanceType='t2.micro',
    MinCount=1,
    MaxCount=1,
    SecurityGroupIds=[
        sg_response['GroupId']
    ],
    IamInstanceProfile={
                            'Name': 'pa5'
                     },
    KeyName='EC2Test',
    UserData = ''' 
    #!/bin/sh
    yum update -y
    yum install docker -y
    service docker start
    sudo docker run -d -p 8080:80 cs4740/accord-demo
    '''
    )

pa()