import boto3
ec2 = boto3.client('ec2', region_name='us-east-1')

group_name = 'pa5_secgroup'
response = ec2.describe_security_groups(
    Filters=[
        dict(Name='group-name', Values=[group_name])
    ]
)
group_id = response['SecurityGroups'][0]['GroupId']



instance_response = ec2.run_instances(
    BlockDeviceMappings=[
        {
            'DeviceName': '/dev/xvda',
            'Ebs': {

                'DeleteOnTermination': True,
                'VolumeSize': 18,
                'VolumeType': 'gp2'
            },
        },
    ],
    ImageId='ami-0947d2ba12ee1ff75',
    InstanceType='t2.micro',
    MinCount=1,
    MaxCount=1,
    SecurityGroupIds=[
        group_id
    ],
    IamInstanceProfile={
                            'Name': 'pa5'
                     },
    KeyName='EC2Test',
    UserData = ''' 
    #!/bin/sh
    yum update -y
    yum install docker -y
    service docker start
    sudo docker run -d -p 8080:80 cs4740/accord-demo
    '''
    )