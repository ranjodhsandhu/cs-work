import boto3
import json
iam = boto3.client('iam')

pa5_policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "s3:GetObject",
                    "dynamodb:PutItem"
                ],
                "Resource": [
                    "arn:aws:s3:::cs4740-bucket/*",
                    "arn:aws:dynamodb:*:347348566121:table/cs4740-table"
                ]
            }
        ]
    }
p_response = iam.create_policy(
        PolicyName='pa5_p',
        PolicyDocument=json.dumps(pa5_policy),
        Description='string'
    )

allow_ec2 = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": {
                    "Service": "ec2.amazonaws.com"
                },
                "Action": "sts:AssumeRole"
            }
        ]
    }


r_response = iam.create_role(
        RoleName='pa5',
        AssumeRolePolicyDocument= json.dumps(allow_ec2),
    )

add_response = iam.attach_role_policy(
        RoleName='pa5',
        PolicyArn= p_response['Policy']['Arn']
    )
instance_profile = iam.create_instance_profile(InstanceProfileName='pa5',Path='/')
iam.add_role_to_instance_profile(InstanceProfileName='pa5', RoleName='pa5')
