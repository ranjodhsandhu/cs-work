import boto3
import json
ec2 = boto3.client('ec2', region_name='us-east-1')

sg_response = ec2.create_security_group(
    Description='Security group required for PA5',
    GroupName='pa5_secgroup')

ir_response = ec2.authorize_security_group_ingress(
        GroupName='pa5_secgroup',
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 8080,
             'ToPort': 8080,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '128.143.0.0/16'}]}
        ])