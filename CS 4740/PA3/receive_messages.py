#!/usr/bin/python3

import json
import boto3
import csv
import datetime
sqs = boto3.client('sqs', region_name='us-east-1')
QURL = 'https://queue.amazonaws.com/347348566121/pa3-queue'
with open("messages.csv", 'a') as file:
    writer = csv.writer(file)
    numMessages = sqs.get_queue_attributes(QueueUrl=QURL, AttributeNames=['ApproximateNumberOfMessages',])['Attributes']['ApproximateNumberOfMessages']
    while numMessages != '0':
        response = sqs.receive_message(QueueUrl=QURL, MessageAttributeNames=['interval'], ReceiveRequestAttemptId='string', VisibilityTimeout=0)
        if 'Messages' in response:
            body = response['Messages'][0]['Body']
            interval = response['Messages'][0]['MessageAttributes']['interval']['StringValue']
            date = datetime.datetime.now().strftime("%H:%M:%S %m/%d/%Y")
            numMessages = sqs.get_queue_attributes(QueueUrl=QURL, AttributeNames=['ApproximateNumberOfMessages',])['Attributes']['ApproximateNumberOfMessages']
            writer.writerow([numMessages, body, interval, date])
            deleted = sqs.delete_message(QueueUrl=QURL, ReceiptHandle=response['Messages'][0]['ReceiptHandle'])


