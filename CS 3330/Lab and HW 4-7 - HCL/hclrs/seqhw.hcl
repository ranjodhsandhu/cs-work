# Ranjodh Sandhu (rss6py)

register pP {  
	pc:64 = 0; 
} 
register cC {
	SF:1 = 0;
	ZF:1 = 1;
}

pc = P_pc;

wire opcode:8, icode:4, ifun:4;
opcode = i10bytes[0..8];  
icode = opcode[4..8]; 
ifun = i10bytes[0..4];

wire rB : 4;
wire rA : 4;
wire valC : 64;
wire val : 64;
wire val1 : 64;

rB = i10bytes[8..12];
rA = i10bytes[12..16];

valC = [
	icode == JXX : i10bytes[8..72];
	1 : i10bytes[16..80];
];

 val = [
	icode in {IRMOVQ, RMMOVQ, MRMOVQ}: 10;
	icode in {JXX} : 9;
	icode in {RRMOVQ, OPQ, CMOVXX} :2;
	1 : 1;
];

val1 = P_pc + val;

reg_srcA = [
	icode in {RRMOVQ, RMMOVQ, OPQ} : rA;
	1 : REG_NONE;
];
reg_srcB = [
	icode in {OPQ, RMMOVQ, MRMOVQ} : rB;
	1 : REG_NONE;
];
wire conditionsMet:1;
conditionsMet = [
	ifun == ALWAYS : true;
	ifun == LE : C_SF || C_ZF;
	ifun == LT : C_SF;
	ifun == EQ : C_ZF;
	ifun == NE : !C_ZF;
	ifun == GE : !C_SF;
	ifun == GT : !C_SF && !C_ZF;
	1 : false;
];

wire valE:64;
valE = [
	icode == OPQ && ifun == ADDQ : reg_outputA + reg_outputB;
	icode == OPQ && ifun == SUBQ : reg_outputB - reg_outputA;
	icode == OPQ && ifun == ANDQ : reg_outputA & reg_outputB;
	icode == OPQ && ifun == XORQ : reg_outputA ^ reg_outputB;
	icode in { RMMOVQ, MRMOVQ } : valC + reg_outputB;
	1 : 0;
];

c_ZF = (valE == 0);
c_SF = (valE >= 0x8000000000000000);
stall_C = (icode != OPQ);

mem_readbit = icode in { MRMOVQ };
mem_writebit = icode in { RMMOVQ };
mem_addr = valE;
mem_input = reg_outputA;

reg_dstE = [
	icode in {IRMOVQ, OPQ} : rB;
    icode == RRMOVQ && conditionsMet : rB;
	1 : REG_NONE;
];
reg_dstM = [
    icode == MRMOVQ : rA;
    1 : REG_NONE;
];

reg_inputE = [
    icode == OPQ : valE;
	icode == RRMOVQ : reg_outputA;
	icode == IRMOVQ : valC;
	1 : 1;
];
reg_inputM = [
    icode == MRMOVQ : mem_output;
    1 : 1;
];

Stat = [
	icode == HALT : STAT_HLT;
	icode > 0xb : STAT_INS;
	1 : STAT_AOK;
	];
	

p_pc = [
	icode == JXX && conditionsMet : valC;
	1 : val1;
];