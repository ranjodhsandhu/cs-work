#Ranjodh Sandhu (rss6py)

########## the PC and condition codes registers #############
register fF { predPC:64 = 0; }
register cC {
	SF:1 = 0;
	ZF:1 = 1;
}
########## Fetch #############
pc = [
	D_icode == JXX && !M_conditionsMet && D_ifun == M_ifun : D_valP;
	E_icode == JXX && !M_conditionsMet && E_ifun == M_ifun : E_valA;
	M_icode == JXX && !M_conditionsMet : M_valA;
	1 : F_predPC;
];
wire ifun:4, rA:4, rB:4, icode:4, valC:64;
icode = i10bytes[4..8];
ifun = i10bytes[0..4];
rA = i10bytes[12..16];
rB = i10bytes[8..12];
valC = [
	icode in { JXX } : i10bytes[8..72];
	1 : i10bytes[16..80];
];
wire offset:64, valP:64;
offset = [
	icode in { HALT, NOP, RET } : 1;
	icode in { RRMOVQ, OPQ, PUSHQ, POPQ } : 2;
	icode in { JXX, CALL } : 9;
	1 : 10;
];
valP = pc + offset;
f_stat = [
	icode == HALT : STAT_HLT;
	icode > 0xb : STAT_INS;
	1 : STAT_AOK;
];
c_ZF = (e_valE == 0);
c_SF = (e_valE >= 0x8000000000000000);
stall_C = (e_icode != OPQ);
f_icode = icode;
f_ifun = ifun;
f_rA = rA;
f_rB = rB;
f_valC = valC;
f_valP = valP;
########## Decode #############
register fD {
	stat:3 = STAT_BUB;
	icode:4 = NOP;
	rA:4 = REG_NONE;
	rB:4 = REG_NONE;
	valC:64 = 0;
	ifun:4 = 0;
	valP:64 = 0;
}
d_stat = D_stat;
d_icode = D_icode;
d_valC = D_valC;
d_ifun = D_ifun;
d_rA = D_rA;
d_rB = D_rB;
# source selection
reg_srcA = [
        D_icode in {RMMOVQ} : D_rA;
        1 : d_srcA;
];
reg_srcB = [
        D_icode in {RMMOVQ, MRMOVQ} : D_rB;
        1 : d_srcB; ];
d_srcA = [
	D_icode == IRMOVQ : REG_NONE;
	1 : D_rA;
];
d_srcB = [
	D_icode == IRMOVQ : REG_NONE;
	1 : D_rB;
];
d_reg_dstE = [
	D_icode in {IRMOVQ, RRMOVQ, OPQ, CMOVXX} : D_rB;
	D_icode == MRMOVQ : D_rA;
	1 : REG_NONE;
];
d_valA = [
	D_icode in {JXX } : D_valP;
	reg_srcA == REG_NONE: 0;
 	reg_srcA == m_dstM : m_valM;
	reg_srcA == W_dstM : W_valM;
	reg_srcA == e_reg_dstE : e_valE; 
	reg_srcA == M_reg_dstE : M_valE;
	reg_srcA == W_reg_dstE : W_valE; 
	1 : reg_outputA; 
];
d_valB = [
	reg_srcB == REG_NONE: 0;
	reg_srcB == e_reg_dstE : e_valE; 
	reg_srcB == m_dstM : m_valM; 
	reg_srcB == M_reg_dstE : M_valE;
	reg_srcB == W_dstM : W_valM; 
	reg_srcB == W_reg_dstE : W_valE; 
	1 : reg_outputB; 
];
d_dstM = [
	D_icode in { MRMOVQ } : D_rA;
	1 : REG_NONE;
];
########## Execute #############
register dE {
	stat:3 = STAT_BUB;
	icode:4 = NOP;
	ifun:4 = 0;
	valC:64 = 0;
	valA:64 = 0;
	valB:64 = 0;
	rA:4 = REG_NONE;
	rB:4 = REG_NONE;
	srcA:4 = REG_NONE;
	srcB:4 = REG_NONE;
	reg_dstE:4 = REG_NONE;
    dstM:4 = REG_NONE;
}
e_valE = [
	E_icode == IRMOVQ : E_valC;
	E_icode == RRMOVQ : E_valA;
	E_icode in { MRMOVQ, RMMOVQ } : E_valB + E_valC;
	E_icode == OPQ && E_ifun == ADDQ : E_valA + E_valB;
	E_icode == OPQ && E_ifun == SUBQ : E_valB - E_valA;
	E_icode == OPQ && E_ifun == ANDQ : E_valA & E_valB;
	E_icode == OPQ && E_ifun == XORQ : E_valA ^ E_valB;
	1 : 0;
];
wire conditionsMet:1;
conditionsMet = [
	E_ifun == ALWAYS : true;
	E_ifun == LE : C_SF || C_ZF;
	E_ifun == LT : C_SF;
	E_ifun == EQ : C_ZF;
	E_ifun == NE : !C_ZF;
	E_ifun == GE : !C_SF;
	E_ifun == GT : !C_SF && !C_ZF;
	1 : false;
];
e_reg_dstE = [
		E_icode == CMOVXX && (!conditionsMet): REG_NONE;
		1 : E_reg_dstE;
];
e_valA = E_valA;
e_stat =  E_stat;
e_icode = E_icode;
e_conditionsMet = conditionsMet;
e_dstM = E_dstM;
e_ifun = E_ifun;
########## Memory #############
register eM {
	stat:3 = STAT_BUB;
	icode:4 = NOP;
	ifun:4 = NOP;
	valE:64 = 0;
	valA:64 = 0;
	reg_dstE:4 = REG_NONE;
	conditionsMet:1 = 0;
    dstM:4 = REG_NONE;
}

mem_readbit = M_icode in { MRMOVQ };
mem_writebit = M_icode in { RMMOVQ };
mem_addr =  M_valE;
mem_input =  M_valA;
m_reg_dstE = M_reg_dstE;
m_stat = M_stat;
m_valM = [
	mem_readbit : mem_output;
	1 : M_valE
];
m_valE = M_valE;
m_icode = M_icode;
m_dstM = M_dstM;
########## Writeback #############
register mW {
	stat:3 = STAT_BUB;
	icode:4 = NOP;
	valM:64 = 0;
	valE:64 = 0;
	reg_dstE:4 = REG_NONE;
    dstM:4 = REG_NONE;
}
reg_dstM =  W_dstM;
reg_inputM =  W_valM;
# destination selection
reg_dstE = W_reg_dstE;
reg_inputE = W_valE;
########## PC and Status updates #############
f_predPC = [
	f_icode in {JXX} : f_valC;
	1 : f_valP; ];
Stat = W_stat;

wire loadUse:1;
loadUse = (E_icode in {MRMOVQ}) && (E_dstM in {reg_srcA, reg_srcB}); 
stall_F = loadUse || f_stat != STAT_AOK;
stall_D = loadUse;
bubble_E = (E_icode == JXX && !e_conditionsMet) || loadUse;
bubble_D = (E_icode == JXX && !e_conditionsMet);

