#include <stdlib.h>
#include <limits.h>  /* for USHRT_MAX */

#include <immintrin.h>

#include "min.h"
/* reference implementation in C */
short min_C(long size, short * a) {
    short result = SHRT_MAX;
    for (int i = 0; i < size; ++i) {
        if (a[i] < result)
            result = a[i];
    }
    return result;
}
short min_C1(long size, short * a) {
    /*short result = SHRT_MAX;
    int j = 0;
    for (int i = 0; i < size; ++i) {
        if (a[i] < result){
            result = a[i];
            j = i;}
    }
    __m256i first = _mm256_set1_epi16(a[j]);
    unsigned short val[16];
    _mm256_storeu_si256((__m256i*) &val, first);
    return val[0];*/
        __m256i first = _mm256_set1_epi16(a[0]);
    for (int i = 0; i < size; ++i) {
        __m256i second = _mm256_set1_epi16(a[i]);
        //values = _mm256_setr_epi16(a[i], a[i+1], a[i+2], a[i+3], a[i+4], a[i+5], a[i+6], a[i+7], a[i+8], a[i+9], a[i+10], a[i+11], a[i+12], a[i+13], a[i+14], a[i+15]);
        first = _mm256_min_epi16(first, second);
    }
    
    unsigned short val[16];
    _mm256_storeu_si256((__m256i*) &val, first);
    return val[0];
  
}
short min_C2(long size, short *a){
      __m256i first = _mm256_set1_epi16(a[0]);
    for (int i = 0; i < size; i+=4) {

        if (_mm256_extract_epi16(first, 0) > a[i]){
        __m256i second = _mm256_set1_epi16(a[i]);
        first = _mm256_min_epi16(first, second);}
         if (_mm256_extract_epi16(first, 0) > a[i+1]){
        __m256i second = _mm256_set1_epi16(a[i+1]);
        first = _mm256_min_epi16(first, second);}
        if (_mm256_extract_epi16(first, 0) > a[i+2]){
        __m256i second = _mm256_set1_epi16(a[i+2]);
        first = _mm256_min_epi16(first, second);}
         if (_mm256_extract_epi16(first, 0) > a[i+3]){
        __m256i second = _mm256_set1_epi16(a[i+3]);
        first = _mm256_min_epi16(first, second);}
    }
    
    unsigned short val[16];
    _mm256_storeu_si256((__m256i*) &val, first);
    return val[0];

}

/* This is the list of functions to test */
function_info functions[] = {
    {min_C, "C (local)"},
    {min_C1, "First Attempt"},
    {min_C2, "Second Attempt"},
    {NULL, NULL},
};
