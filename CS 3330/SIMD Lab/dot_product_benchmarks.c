#include <stdlib.h>

#include <immintrin.h>

#include "dot_product.h"
/* reference implementation in C */
unsigned int dot_product_C(long size, unsigned short * a, unsigned short *b) {
    unsigned int sum = 0;
    for (int i = 0; i < size; ++i) {
        sum += a[i] * b[i];
    }
    return sum;
}
unsigned int dot_product_C1(long size, unsigned short *a, unsigned short *b) {
    __m256i p_sums = _mm256_setzero_si256();
    for(int i = 0; i < size; i+=8){
        __m128i as = _mm_loadu_si128((__m128i*) &a[i]);
        __m128i bs = _mm_loadu_si128((__m128i*) &b[i]);
        __m256i as1 = _mm256_cvtepu16_epi32(as);
        __m256i bs1 = _mm256_cvtepu16_epi32(bs);
        __m256i p = _mm256_mullo_epi32(as1, bs1);
        p_sums = _mm256_add_epi32(p_sums, p);
    }
    unsigned int ep_sums[8];
    _mm256_storeu_si256((__m256i*) &ep_sums, p_sums);
    return ep_sums[0] + ep_sums[1] + ep_sums[2] + ep_sums[3] + ep_sums[4] + ep_sums[5] + ep_sums[6] + ep_sums[7];
    /*for(int i = 0; i < 16; i++){
        result += ep_sums[i];
    }*/
    //return result;
}
// add prototypes here!
extern unsigned int dot_product_gcc7_O3(long size, unsigned short * a, unsigned short *b);

/* This is the list of functions to test */
function_info functions[] = {
    {dot_product_C, "C (local)"},
    {dot_product_gcc7_O3, "C (compiled with GCC7.2 -O3 -mavx2)"},
    {dot_product_C1, "First Attempt"},
    // add entries here!
    {NULL, NULL},
};
