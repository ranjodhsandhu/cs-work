	.file	"min_benchmarks.c"
	.text
	.globl	min_C
	.type	min_C, @function
min_C:
.LFB4750:
	.cfi_startproc
	testq	%rdi, %rdi
	jle	.L4
	movl	$0, %edx
	movl	$32767, %eax
.L3:
	movslq	%edx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%cx, %ax
	cmovg	%ecx, %eax
	addl	$1, %edx
	movslq	%edx, %rcx
	cmpq	%rdi, %rcx
	jl	.L3
	ret
.L4:
	movl	$32767, %eax
	ret
	.cfi_endproc
.LFE4750:
	.size	min_C, .-min_C
	.globl	min_C1
	.type	min_C1, @function
min_C1:
.LFB4751:
	.cfi_startproc
	vpbroadcastw	(%rsi), %ymm0
	testq	%rdi, %rdi
	jle	.L7
	movl	$0, %eax
.L8:
	movslq	%eax, %rdx
	vpbroadcastw	(%rsi,%rdx,2), %ymm1
	vpminsw	%ymm0, %ymm1, %ymm0
	addl	$1, %eax
	movslq	%eax, %rdx
	cmpq	%rdi, %rdx
	jl	.L8
.L7:
	vpextrw	$0, %xmm0, %eax
	ret
	.cfi_endproc
.LFE4751:
	.size	min_C1, .-min_C1
	.globl	min_C2
	.type	min_C2, @function
min_C2:
.LFB4752:
	.cfi_startproc
	vpbroadcastw	(%rsi), %ymm0
	testq	%rdi, %rdi
	jle	.L11
	movl	$0, %edx
	jmp	.L16
.L15:
	addl	$4, %edx
	movslq	%edx, %rax
	cmpq	%rdi, %rax
	jge	.L11
.L16:
	movslq	%edx, %rax
	leaq	(%rax,%rax), %rcx
	movzwl	(%rsi,%rax,2), %r8d
	movswl	%r8w, %r9d
	vpextrw	$0, %xmm0, %eax
	cmpl	%eax, %r9d
	jge	.L12
	vmovd	%r8d, %xmm1
	vpbroadcastw	%xmm1, %ymm1
	vpminsw	%ymm0, %ymm1, %ymm0
.L12:
	movzwl	2(%rsi,%rcx), %r8d
	movswl	%r8w, %r9d
	vpextrw	$0, %xmm0, %eax
	cmpl	%eax, %r9d
	jge	.L13
	vmovd	%r8d, %xmm1
	vpbroadcastw	%xmm1, %ymm1
	vpminsw	%ymm0, %ymm1, %ymm0
.L13:
	movzwl	4(%rsi,%rcx), %r8d
	movswl	%r8w, %r9d
	vpextrw	$0, %xmm0, %eax
	cmpl	%eax, %r9d
	jge	.L14
	vmovd	%r8d, %xmm1
	vpbroadcastw	%xmm1, %ymm1
	vpminsw	%ymm0, %ymm1, %ymm0
.L14:
	movzwl	6(%rsi,%rcx), %ecx
	movswl	%cx, %r8d
	vpextrw	$0, %xmm0, %eax
	cmpl	%eax, %r8d
	jge	.L15
	vmovd	%ecx, %xmm1
	vpbroadcastw	%xmm1, %ymm1
	vpminsw	%ymm0, %ymm1, %ymm0
	jmp	.L15
.L11:
	vpextrw	$0, %xmm0, %eax
	ret
	.cfi_endproc
.LFE4752:
	.size	min_C2, .-min_C2
	.globl	functions
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"C (local)"
.LC1:
	.string	"First Attempt"
.LC2:
	.string	"Second Attempt"
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	functions, @object
	.size	functions, 64
functions:
	.quad	min_C
	.quad	.LC0
	.quad	min_C1
	.quad	.LC1
	.quad	min_C2
	.quad	.LC2
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 7.3.0-16ubuntu3) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
