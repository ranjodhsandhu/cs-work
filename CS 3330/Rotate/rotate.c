#include <stdio.h>
#include <stdlib.h>
#include "defs.h"

/* 
 * Please fill in the following struct with your name and the name you'd like to appear on the scoreboard
 */
who_t who = {
    "Your Scoreboard Idenitifer Here",           /* Scoreboard name */

    "Ranjodh Sandhu",   /* Full name */
    "rss6py@virginia.edu",  /* Email address */
};

/***************
 * ROTATE KERNEL
 ***************/

/******************************************************
 * Your different versions of the rotate kernel go here
 ******************************************************/

/* 
 * naive_rotate - The naive baseline version of rotate 
 */
char naive_rotate_descr[] = "naive_rotate: Naive baseline implementation";
void naive_rotate(int dim, pixel *src, pixel *dst) 
{
    for (int i = 0; i < dim; i++)
	for (int j = 0; j < dim; j++)
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
}
char new_rotate_descr[] = "new_rotate: unrolled both loops";
void new_rotate(int dim, pixel *src, pixel *dst) 
{
    for (int i = 0; i < dim; i+=2)
	for (int j = 0; j < dim; j+=2){
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
        dst[RIDX(dim-1-(j+1), i, dim)] = src[RIDX(i, (j+1), dim)];
        dst[RIDX(dim-1-j, i+1, dim)] = src[RIDX(i+1, j, dim)];
        dst[RIDX(dim-1-(j+1), i+1, dim)] = src[RIDX(i+1, (j+1), dim)];
        
        }
}
char rotate_1_descr[] = "rotate_1: unrolled outer loop";
void rotate_1(int dim, pixel *src, pixel *dst) 
{
    for (int i = 0; i < dim; i+=2)
	for (int j = 0; j < dim; j++){
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
        //dst[RIDX(dim-1-(j+1), i, dim)] = src[RIDX(i, (j+1), dim)];
        dst[RIDX(dim-1-j, i+1, dim)] = src[RIDX(i+1, j, dim)];
        //dst[RIDX(dim-1-(j+1), i+1, dim)] = src[RIDX(i+1, (j+1), dim)];
        
        }
}
char rotate_2_descr[] = "rotate_2: stored variables";
void rotate_2(int dim, pixel *src, pixel *dst) 
{
    int a;
    for (int i = 0; i < dim; i+=2)
	for (int j = 0; j < dim; j++){
        a = ((dim - 1 - j)*dim)+i;
	    dst[a] = src[RIDX(i, j, dim)];
        //dst[RIDX(dim-1-(j+1), i, dim)] = src[RIDX(i, (j+1), dim)];
        dst[a+1] = src[RIDX(i+1, j, dim)];
        //dst[RIDX(dim-1-(j+1), i+1, dim)] = src[RIDX(i+1, (j+1), dim)];
        
        }
}
char rotate_3_descr[] = "rotate_3: unrolled outer loop with stored variable";
void rotate_3(int dim, pixel *src, pixel *dst) 
{
    int a;
    for (int i = 0; i < dim; i+=4)
	for (int j = 0; j < dim; j++){
        a = ((dim - 1 - j)*dim)+i;
	    dst[a] = src[RIDX(i, j, dim)];
        //dst[RIDX(dim-1-(j+1), i, dim)] = src[RIDX(i, (j+1), dim)];
        dst[a+1] = src[RIDX(i+1, j, dim)];
        //dst[RIDX(dim-1-(j+1), i+1, dim)] = src[RIDX(i+1, (j+1), dim)];
        dst[a+2] = src[RIDX(i+2, j, dim)];
        dst[a+3] = src[RIDX(i+3, j, dim)];
        
        }
}
char rotate_4_descr[] = "rotate_4: unrolled outer loop more";
void rotate_4(int dim, pixel *src, pixel *dst) 
{
    for (int i = 0; i < dim; i+=8)
	for (int j = 0; j < dim; j++){
	    dst[RIDX(dim-1-j, i, dim)] = src[RIDX(i, j, dim)];
        //dst[RIDX(dim-1-(j+1), i, dim)] = src[RIDX(i, (j+1), dim)];
        dst[RIDX(dim-1-j, i+1, dim)] = src[RIDX(i+1, j, dim)];
        //dst[RIDX(dim-1-(j+1), i+1, dim)] = src[RIDX(i+1, (j+1), dim)];
       dst[RIDX(dim-1-j, i+2, dim)] = src[RIDX(i+2, j, dim)];
       dst[RIDX(dim-1-j, i+3, dim)] = src[RIDX(i+3, j, dim)];
       dst[RIDX(dim-1-j, i+4, dim)] = src[RIDX(i+4, j, dim)];
       dst[RIDX(dim-1-j, i+5, dim)] = src[RIDX(i+5, j, dim)];
       dst[RIDX(dim-1-j, i+6, dim)] = src[RIDX(i+6, j, dim)];
       dst[RIDX(dim-1-j, i+7, dim)] = src[RIDX(i+7, j, dim)];
}}

/* 
 * rotate - Your current working version of rotate
 *          Our supplied version simply calls naive_rotate
 */
char another_rotate_descr[] = "another_rotate: Another version of rotate";
void another_rotate(int dim, pixel *src, pixel *dst) 
{
    naive_rotate(dim, src, dst);
}

/*********************************************************************
 * register_rotate_functions - Register all of your different versions
 *     of the rotate function by calling the add_rotate_function() for
 *     each test function. When you run the benchmark program, it will
 *     test and report the performance of each registered test
 *     function.  
 *********************************************************************/

void register_rotate_functions() {
    add_rotate_function(&naive_rotate, naive_rotate_descr);
    add_rotate_function(&another_rotate, another_rotate_descr);
    add_rotate_function(&new_rotate, new_rotate_descr);
    add_rotate_function(&rotate_1, rotate_1_descr);
    add_rotate_function(&rotate_2, rotate_2_descr);
    add_rotate_function(&rotate_3, rotate_3_descr);
    add_rotate_function(&rotate_4, rotate_4_descr);
}
