#include <stdio.h>
#include <stdlib.h>
#include "defs.h"
#include <immintrin.h>

/* 
 * Please fill in the following team struct 
 */
who_t who = {
    "Kyle Guy",           /* Scoreboard name */

    "Ranjodh Sandhu",      /* First member full name */
    "rss6py@virginia.edu",     /* First member email address */
};

/*** UTILITY FUNCTIONS ***/

/* You are free to use these utility functions, or write your own versions
 * of them. */

/* A struct used to compute averaged pixel value */
typedef struct {
    unsigned short red;
    unsigned short green;
    unsigned short blue;
    unsigned short alpha;
    unsigned short num;
} pixel_sum;

/* Compute min and max of two integers, respectively */
static int min(int a, int b) { return (a < b ? a : b); }
static int max(int a, int b) { return (a > b ? a : b); }

/* 
 * initialize_pixel_sum - Initializes all fields of sum to 0 
 */
static void initialize_pixel_sum(pixel_sum *sum) 
{
    sum->red = sum->green = sum->blue = sum->alpha = 0;
    sum->num = 0;
    return;
}

/* 
 * accumulate_sum - Accumulates field values of p in corresponding 
 * fields of sum 
 */
static void accumulate_sum(pixel_sum *sum, pixel p) 
{
    sum->red += (int) p.red;
    sum->green += (int) p.green;
    sum->blue += (int) p.blue;
    sum->alpha += (int) p.alpha;
    sum->num++;
    return;
}

/* 
 * assign_sum_to_pixel - Computes averaged pixel value in current_pixel 
 */
static void assign_sum_to_pixel(pixel *current_pixel, pixel_sum sum) 
{
    current_pixel->red = (unsigned short) (sum.red/sum.num);
    current_pixel->green = (unsigned short) (sum.green/sum.num);
    current_pixel->blue = (unsigned short) (sum.blue/sum.num);
    current_pixel->alpha = (unsigned short) (sum.alpha/sum.num);
    return;
}

/* 
 * avg - Returns averaged pixel value at (i,j) 
 */
static pixel avg(int dim, int i, int j, pixel *src) 
{
    pixel_sum sum;
    pixel current_pixel;

    initialize_pixel_sum(&sum);
    for(int jj=max(j-1, 0); jj <= min(j+1, dim-1); jj++) 
	for(int ii=max(i-1, 0); ii <= min(i+1, dim-1); ii++) 
	    accumulate_sum(&sum, src[RIDX(ii,jj,dim)]);

    assign_sum_to_pixel(&current_pixel, sum);
 
    return current_pixel;
}



/******************************************************
 * Your different versions of the smooth go here
 ******************************************************/

/* 
 * naive_smooth - The naive baseline version of smooth
 */
char naive_smooth_descr[] = "naive_smooth: Naive baseline implementation";
void naive_smooth(int dim, pixel *src, pixel *dst) 
{
    for (int i = 0; i < dim; i++)
	for (int j = 0; j < dim; j++)
            dst[RIDX(i,j, dim)] = avg(dim, i, j, src);
}
/* 
 * smooth - Your current working version of smooth
 *          Our supplied version simply calls naive_smooth
 */
char another_smooth_descr[] = "another_smooth: Another version of smooth";
void another_smooth(int dim, pixel *src, pixel *dst) 
{
    naive_smooth(dim, src, dst);
}
char smooth_1_descr[] = "If Statements";
void smooth_1(int dim, pixel *src, pixel *dst) 
{   int a;
    for (int i = 0; i < dim; i++)
	for (int j = 0; j < dim; j+=2){
            if(i == 0 || dim == 0){a = j;}
            else{a = i * dim + j;}
                dst[a] = avg(dim, i, j, src);
                dst[a+1] = avg(dim, i, j+1, src);}
}
char smooth_2_descr[] = "More Inline";
void smooth_2(int dim, pixel *src, pixel *dst) 
{   
    pixel_sum sum;
    pixel current_pixel;
    for (int i = 0; i < dim; i++)
	for (int j = 0; j < dim; j++){
            (&sum)->red = (&sum)->green = (&sum)->blue = (&sum)->alpha = 0;
            (&sum)->num = 0;
            for(int jj=max(j-1, 0); jj <= min(j+1, dim-1); jj++) 
	        for(int ii=max(i-1, 0); ii <= min(i+1, dim-1); ii++){
                pixel pix = src[((ii)*dim+(jj))];
                (&sum)->red += (int) pix.red;
                (&sum)->green += (int) pix.green;
                (&sum)->blue += (int) pix.blue;
                (&sum)->alpha += (int) pix.alpha;
                (&sum)->num++;}
            (&current_pixel) -> red = (unsigned short) (sum.red/sum.num);
            (&current_pixel) -> green = (unsigned short) (sum.green/sum.num);
            (&current_pixel) -> blue = (unsigned short) (sum.blue/sum.num);
            (&current_pixel) -> alpha = (unsigned short) (sum.alpha/sum.num);

            dst[((i)*(dim))+(j)] = current_pixel;}
}
char smooth_3_descr[] = "Corner Cases and Edges";
void smooth_3(int dim, pixel *src, pixel *dst){
    int j;
    int i = 0;
    int a = (dim-1)*(dim)+1;
    int b = (dim*dim)-1;
    int c = dim*2-1;
    int d;
	for(i=1;i< dim-1;i++){
		dst[i].red = (src[i-1].red+src[i].red+src[i+1].red+src[i-1+dim].red+src[i+dim].red+src[i+1+dim].red)/6;
		dst[i].green = (src[i-1].green +src[i].green+src[i+1].green+src[i-1+dim].green+src[i+dim].green+src[i+1+dim].green)/6;
		dst[i].blue = (src[i-1].blue+src[i].blue+src[i+1].blue+src[i-1+dim].blue+src[i+dim].blue+src[i+1+dim].blue)/6;
        dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i+1].alpha+src[i-1+dim].alpha+src[i+dim].alpha+src[i+1+dim].alpha)/6;}
	for(i=a;i<b;i++){
		dst[i].red = (src[i-1].red+src[i].red+src[i+1].red+src[i-dim].red+src[i-1-dim].red+src[i+1-dim].red)/6;
		dst[i].green = (src[i-1].green +src[i].green+src[i+1].green+src[i-dim].green+src[i-1-dim].green+src[i+1-dim].green)/6;
		dst[i].blue = (src[i-1].blue+src[i].blue+src[i+1].blue+src[i-dim].blue+src[i-1-dim].blue+src[i+1-dim].blue)/6;
        dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i+1].alpha+src[i-dim].alpha+src[i-1-dim].alpha+src[i+1-dim].alpha)/6;}

	for(i = dim;i<a-1;i+=dim){
		dst[i].red = (src[i-dim].red+src[i-dim+1].red+src[i].red+src[i+1].red+src[i+dim].red+src[i+dim+1].red)/6;
		dst[i].green = (src[i-dim].green+src[i-dim+1].green+src[i].green+src[i+1].green+src[i+dim].green+src[i+dim+1].green)/6;
		dst[i].blue = (src[i-dim].blue+src[i-dim+1].blue+src[i].blue+src[i+1].blue+src[i+dim].blue+src[i+dim+1].blue)/6;
        dst[i].alpha = (src[i-dim].alpha+src[i-dim+1].alpha+src[i].alpha+src[i+1].alpha+src[i+dim].alpha+src[i+dim+1].alpha)/6;}

	for(i = c;i<b;i+=dim){
		dst[i].red = (src[i-dim].red+src[i-dim-1].red+src[i-1].red+src[i].red+src[i+dim-1].red+src[i+dim].red)/6;
		dst[i].green = (src[i-dim].green +src[i-dim-1].green +src[i-1].green +src[i].green +src[i+dim-1].green +src[i+dim].green )/6;
		dst[i].blue = (src[i-dim].blue+src[i-dim-1].blue+src[i-1].blue+src[i].blue+src[i+dim-1].blue+src[i+dim].blue)/6;
        dst[i].alpha = (src[i-dim].alpha+src[i-dim-1].alpha+src[i-1].alpha+src[i].alpha+src[i+dim-1].alpha+src[i+dim].alpha)/6;}
	for(i=1;i<dim-1;i++)
		for(j=1;j<dim-1;j++){
			d=(i*dim)+j;
			dst[d].red = (src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)/9;
			dst[d].green = (src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)/9;
			dst[d].blue = (src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)/9;
            dst[d].alpha = (src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)/9;}
	i = 0;
    dst[i].red = (src[i].red+src[i+1].red+src[dim].red+src[dim+1].red)/4;
	dst[i].green = (src[i].green+src[i+1].green+src[dim].green+src[dim+1].green)/4;
	dst[i].blue = (src[i].blue+src[i+1].blue+src[dim].blue+src[dim+1].blue)/4;
    dst[i].alpha = (src[i].alpha+src[i+1].alpha+src[dim].alpha+src[dim+1].alpha)/4;
	i=a-1;
	dst[i].red = (src[i-dim].red+src[i-dim+1].red+src[i].red+src[i+1].red)/4;
	dst[i].green = (src[i-dim].green+src[i-dim+1].green+src[i].green+src[i+1].green)/4;
	dst[i].blue = (src[i-dim].blue+src[i-dim+1].blue+src[i].blue+src[i+1].blue)/4;
    dst[i].alpha = (src[i-dim].alpha+src[i-dim+1].alpha+src[i].alpha+src[i+1].alpha)/4;
    i=dim-1;
	dst[i].red = (src[i-1].red+src[i].red+src[i-1+dim].red+src[i+dim].red)/4;
	dst[i].green = (src[i-1].green+src[i].green+src[i-1+dim].green+src[i+dim].green)/4;
	dst[i].blue = (src[i-1].blue+src[i].blue+src[i-1+dim].blue+src[i+dim].blue)/4;
    dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i-1+dim].alpha+src[i+dim].alpha)/4;
	i=b;
	dst[i].red = (src[i-dim].red+src[i-dim-1].red+src[i-1].red+src[i].red)/4;
	dst[i].green = (src[i-dim].green+src[i-dim-1].green+src[i-1].green+src[i].green)/4;
	dst[i].blue = (src[i-dim].blue+src[i-dim-1].blue+src[i-1].blue+src[i].blue)/4;
    dst[i].alpha = (src[i-dim].alpha+src[i-dim-1].alpha+src[i-1].alpha+src[i].alpha)/4;

        }
char smooth_4_descr[] = "Smooth Center Loop";
void smooth_4(int dim, pixel *src, pixel *dst){
    int j;
    int i = 0;
    int a = (dim-1)*(dim)+1;
    int b = (dim*dim)-1;
    int c = dim*2-1;
        for(i=1;i<dim-1;i+=3){
		for(j=1;j<dim-1;j+=3){
            pixel_sum s;
            pixel cp;
            pixel a = src[RIDX(i, j-1, dim)];
            pixel b = src[RIDX(i+1, j-1, dim)];
            pixel c = src[RIDX(i-1, j, dim)];
            pixel d = src[RIDX(i, j, dim)];
            pixel e = src[RIDX(i+1, j, dim)];
            pixel f = src[RIDX(i-1, j+1, dim)];
            pixel g = src[RIDX(i, j+1, dim)];
            pixel h = src[RIDX(i+1, j+1, dim)];
            pixel i1 = src[RIDX(i+2, j-1, dim)];
            pixel j1 = src[RIDX(i+2, j, dim)];
            pixel k1 = src[RIDX(i+2, j+1, dim)];
            pixel l1 = src[RIDX(i-1, j+2, dim)];
            pixel m1 = src[RIDX(i, j+2, dim)];
            pixel n1 = src[RIDX(i+1, j+2, dim)];
            pixel o1 = src[RIDX(i+2, j+2, dim)];
            pixel p1 = src[RIDX(i+3, j, dim)];
            pixel q1 = src[RIDX(i+3, j+1, dim)];
            pixel r1 = src[RIDX(i+3, j+2, dim)];
            //i, j
            initialize_pixel_sum(&s);
            accumulate_sum(&s, src[RIDX(i-1, j-1, dim)]);
            accumulate_sum(&s, a);
            accumulate_sum(&s, b);
            accumulate_sum(&s, c);
            accumulate_sum(&s, d);
            accumulate_sum(&s, e);
            accumulate_sum(&s, f);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[(i*dim)+j] = cp;
            //i + 1, j
            initialize_pixel_sum(&s);
            accumulate_sum(&s, a);
            accumulate_sum(&s, b);
            accumulate_sum(&s, i1);
            accumulate_sum(&s, d);
            accumulate_sum(&s, e);
            accumulate_sum(&s, j1);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+1)*dim)+j] = cp;
            //i, j+1
            initialize_pixel_sum(&s);
            accumulate_sum(&s, c);
            accumulate_sum(&s, d);
            accumulate_sum(&s, e);
            accumulate_sum(&s, f);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            accumulate_sum(&s, l1);
            accumulate_sum(&s, m1);
            accumulate_sum(&s, n1);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[(i*dim)+j+1] = cp;
            //i+1, j+1
            initialize_pixel_sum(&s);
            accumulate_sum(&s, d);
            accumulate_sum(&s, e);
            accumulate_sum(&s, j1);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            accumulate_sum(&s, m1);
            accumulate_sum(&s, n1);
            accumulate_sum(&s, o1);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+1)*dim)+j+1] = cp;
            //i+2, j
            initialize_pixel_sum(&s);
            accumulate_sum(&s, b);
            accumulate_sum(&s, i1);
            accumulate_sum(&s, src[RIDX(i+3, j-1, dim)]);
            accumulate_sum(&s, e);
            accumulate_sum(&s, j1);
            accumulate_sum(&s, p1);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            accumulate_sum(&s, q1);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+2)*dim)+j] = cp;
            //i+2, j+1
             initialize_pixel_sum(&s);
            accumulate_sum(&s, e);
            accumulate_sum(&s, j1);
            accumulate_sum(&s, p1);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            accumulate_sum(&s, q1);
            accumulate_sum(&s, n1);
            accumulate_sum(&s, o1);
            accumulate_sum(&s, r1);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+2)*dim)+j+1] = cp;
            //i+2, j+2
             initialize_pixel_sum(&s);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            accumulate_sum(&s, q1);
            accumulate_sum(&s, n1);
            accumulate_sum(&s, o1);
            accumulate_sum(&s, r1);
            accumulate_sum(&s, src[RIDX(i+1, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i+2, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i+3, j+3, dim)]);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+2)*dim)+j+2] = cp;
            //i, j+2
            initialize_pixel_sum(&s);
            accumulate_sum(&s, f);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            accumulate_sum(&s, l1);
            accumulate_sum(&s, m1);
            accumulate_sum(&s, n1);
            accumulate_sum(&s, src[RIDX(i-1, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i+1, j+3, dim)]);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[(i*dim)+j+2] = cp;
            //i+1, j+2
            initialize_pixel_sum(&s);
            accumulate_sum(&s, g);
            accumulate_sum(&s, h);
            accumulate_sum(&s, k1);
            accumulate_sum(&s, m1);
            accumulate_sum(&s, n1);
            accumulate_sum(&s, o1);
            accumulate_sum(&s, src[RIDX(i, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i+1, j+3, dim)]);
            accumulate_sum(&s, src[RIDX(i+2, j+3, dim)]);
            cp.red = (unsigned short) ((s.red * 7282) >> 16);
            cp.green = (unsigned short) ((s.green * 7282) >> 16);
            cp.blue = (unsigned short) ((s.blue * 7282) >> 16);
            cp.alpha = (unsigned short) ((s.alpha * 7282) >> 16);
            assign_sum_to_pixel(&cp, s);
            dst[((i+1)*dim)+j+2] = cp;
            }}
  //  int d;
	for(i=1;i< dim-1;i+=2){
		dst[i].red = (src[i-1].red+src[i].red+src[i+1].red+src[i-1+dim].red+src[i+dim].red+src[i+1+dim].red)/6;
		dst[i].green = (src[i-1].green +src[i].green+src[i+1].green+src[i-1+dim].green+src[i+dim].green+src[i+1+dim].green)/6;
		dst[i].blue = (src[i-1].blue+src[i].blue+src[i+1].blue+src[i-1+dim].blue+src[i+dim].blue+src[i+1+dim].blue)/6;
        dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i+1].alpha+src[i-1+dim].alpha+src[i+dim].alpha+src[i+1+dim].alpha)/6;
        
        dst[i+1].red = (src[i].red+src[i+1].red+src[i+2].red+src[i+dim].red+src[i+1+dim].red+src[i+2+dim].red)/6;
		dst[i+1].green = (src[i].green +src[i+1].green+src[i+2].green+src[i+dim].green+src[i+1+dim].green+src[i+2+dim].green)/6;
		dst[i+1].blue = (src[i].blue+src[i+1].blue+src[i+2].blue+src[i+dim].blue+src[i+1+dim].blue+src[i+2+dim].blue)/6;
        dst[i+1].alpha = (src[i].alpha+src[i+1].alpha+src[i+2].alpha+src[i+dim].alpha+src[i+1+dim].alpha+src[i+2+dim].alpha)/6;
        }
	for(i=a;i<b;i+=2){
		dst[i].red = (src[i-1].red+src[i].red+src[i+1].red+src[i-dim].red+src[i-1-dim].red+src[i+1-dim].red)/6;
		dst[i].green = (src[i-1].green +src[i].green+src[i+1].green+src[i-dim].green+src[i-1-dim].green+src[i+1-dim].green)/6;
		dst[i].blue = (src[i-1].blue+src[i].blue+src[i+1].blue+src[i-dim].blue+src[i-1-dim].blue+src[i+1-dim].blue)/6;
        dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i+1].alpha+src[i-dim].alpha+src[i-1-dim].alpha+src[i+1-dim].alpha)/6;
        
        dst[i+1].red = (src[i].red+src[i+1].red+src[i+2].red+src[i-dim+1].red+src[i-dim].red+src[i+2-dim].red)/6;
		dst[i+1].green = (src[i].green +src[i+1].green+src[i+2].green+src[i-dim+1].green+src[i-dim].green+src[i+2-dim].green)/6;
		dst[i+1].blue = (src[i].blue+src[i+1].blue+src[i+2].blue+src[i-dim+1].blue+src[i-dim].blue+src[i+2-dim].blue)/6;
        dst[i+1].alpha = (src[i].alpha+src[i+1].alpha+src[i+2].alpha+src[i-dim+1].alpha+src[i-dim].alpha+src[i+2-dim].alpha)/6;}

	for(i = dim;i<a-1;i+=dim*2){
		dst[i].red = (src[i-dim].red+src[i-dim+1].red+src[i].red+src[i+1].red+src[i+dim].red+src[i+dim+1].red)/6;
		dst[i].green = (src[i-dim].green+src[i-dim+1].green+src[i].green+src[i+1].green+src[i+dim].green+src[i+dim+1].green)/6;
		dst[i].blue = (src[i-dim].blue+src[i-dim+1].blue+src[i].blue+src[i+1].blue+src[i+dim].blue+src[i+dim+1].blue)/6;
        dst[i].alpha = (src[i-dim].alpha+src[i-dim+1].alpha+src[i].alpha+src[i+1].alpha+src[i+dim].alpha+src[i+dim+1].alpha)/6;
        
        dst[i+dim].red = (src[i].red+src[i+1].red+src[i+dim].red+src[i+1+dim].red+src[i+dim+dim].red+src[i+dim+dim+1].red)/6;
		dst[i+dim].green = (src[i].green+src[i+1].green+src[i+dim].green+src[i+1+dim].green+src[i+dim+dim].green+src[i+dim+dim+1].green)/6;
		dst[i+dim].blue = (src[i].blue+src[i+1].blue+src[i+dim].blue+src[i+1+dim].blue+src[i+dim+dim].blue+src[i+dim+dim+1].blue)/6;
        dst[i+dim].alpha = (src[i].alpha+src[i+1].alpha+src[i+dim].alpha+src[i+1+dim].alpha+src[i+dim+dim].alpha+src[i+dim+dim+1].alpha)/6;}

	for(i = c;i<b;i+=dim*2){
		dst[i].red = (src[i-dim].red+src[i-dim-1].red+src[i-1].red+src[i].red+src[i+dim-1].red+src[i+dim].red)/6;
		dst[i].green = (src[i-dim].green +src[i-dim-1].green +src[i-1].green +src[i].green +src[i+dim-1].green +src[i+dim].green )/6;
		dst[i].blue = (src[i-dim].blue+src[i-dim-1].blue+src[i-1].blue+src[i].blue+src[i+dim-1].blue+src[i+dim].blue)/6;
        dst[i].alpha = (src[i-dim].alpha+src[i-dim-1].alpha+src[i-1].alpha+src[i].alpha+src[i+dim-1].alpha+src[i+dim].alpha)/6;
        
        dst[i+dim].red = (src[i].red+src[i-1].red+src[i+dim-1].red+src[i+dim].red+src[i+dim+dim-1].red+src[i+dim+dim].red)/6;
		dst[i+dim].green = (src[i].green+src[i-1].green+src[i+dim-1].green+src[i+dim].green+src[i+dim+dim-1].green+src[i+dim+dim].green)/6;
		dst[i+dim].blue = (src[i].blue+src[i-1].blue+src[i+dim-1].blue+src[i+dim].blue+src[i+dim+dim-1].blue+src[i+dim+dim].blue)/6;
        dst[i+dim].alpha = (src[i].alpha+src[i-1].alpha+src[i+dim-1].alpha+src[i+dim].alpha+src[i+dim+dim-1].alpha+src[i+dim+dim].alpha)/6;}



            
    /*for(i=1;i<dim-1;i+=2){
		for(j=1;j<dim-1;j+=2){
			d=(i*dim)+j;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=((i+1)*dim)+j;
            dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i*dim)+(j+1);
            dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i+1)*dim+j+1;
            dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i+2)*dim+j;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i+2)*dim+j+1;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i+2)*dim+j+2;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i)*dim+j+2;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            d=(i+1)*dim+j+2;
		    dst[d].red = ((src[d-dim-1].red+src[d-dim].red+src[d-dim+1].red+src[d-1].red+src[d].red+src[d+1].red+src[d-1+dim].red+src[d+dim].red+src[d+dim+1].red)*7282) >> 16;
			dst[d].green = ((src[d-dim-1].green+src[d-dim].green+src[d-dim+1].green+src[d-1].green+src[d].green+src[d+1].green+src[d-1+dim].green+src[d+dim].green+src[d+dim+1].green)*7282) >> 16;
			dst[d].blue = ((src[d-dim-1].blue+src[d-dim].blue+src[d-dim+1].blue+src[d-1].blue+src[d].blue+src[d+1].blue+src[d-1+dim].blue+src[d+dim].blue+src[d+dim+1].blue)*7282) >> 16;
            dst[d].alpha = ((src[d-dim-1].alpha+src[d-dim].alpha+src[d-dim+1].alpha+src[d-1].alpha+src[d].alpha+src[d+1].alpha+src[d-1+dim].alpha+src[d+dim].alpha+src[d+dim+1].alpha)*7282) >> 16;
            */
	i = 0;
    dst[i].red = (src[i].red+src[i+1].red+src[dim].red+src[dim+1].red) >> 2;
	dst[i].green = (src[i].green+src[i+1].green+src[dim].green+src[dim+1].green)>>2;
	dst[i].blue = (src[i].blue+src[i+1].blue+src[dim].blue+src[dim+1].blue)>>2;
    dst[i].alpha = (src[i].alpha+src[i+1].alpha+src[dim].alpha+src[dim+1].alpha)>>2;
	i=a-1;
	dst[i].red = (src[i-dim].red+src[i-dim+1].red+src[i].red+src[i+1].red)>>2;
	dst[i].green = (src[i-dim].green+src[i-dim+1].green+src[i].green+src[i+1].green)>>2;
	dst[i].blue = (src[i-dim].blue+src[i-dim+1].blue+src[i].blue+src[i+1].blue)>>2;
    dst[i].alpha = (src[i-dim].alpha+src[i-dim+1].alpha+src[i].alpha+src[i+1].alpha)>>2;
    i=dim-1;
	dst[i].red = (src[i-1].red+src[i].red+src[i-1+dim].red+src[i+dim].red)>>2;
	dst[i].green = (src[i-1].green+src[i].green+src[i-1+dim].green+src[i+dim].green)>>2;
	dst[i].blue = (src[i-1].blue+src[i].blue+src[i-1+dim].blue+src[i+dim].blue)>>2;
    dst[i].alpha = (src[i-1].alpha+src[i].alpha+src[i-1+dim].alpha+src[i+dim].alpha)>>2;
	i=b;
	dst[i].red = (src[i-dim].red+src[i-dim-1].red+src[i-1].red+src[i].red)>>2;
	dst[i].green = (src[i-dim].green+src[i-dim-1].green+src[i-1].green+src[i].green)>>2;
	dst[i].blue = (src[i-dim].blue+src[i-dim-1].blue+src[i-1].blue+src[i].blue)>>2;
    dst[i].alpha = (src[i-dim].alpha+src[i-dim-1].alpha+src[i-1].alpha+src[i].alpha)>>2;
    }
/*********************************************************************
 * register_smooth_functions - Register all of your different versions
 *     of the smooth function by calling the add_smooth_function() for
 *     each test function. When you run the benchmark program, it will
 *     test and report the performance of each registered test
 *     function.  
 *********************************************************************/

void register_smooth_functions() {
    add_smooth_function(&naive_smooth, naive_smooth_descr);
    add_smooth_function(&another_smooth, another_smooth_descr);
    add_smooth_function(&smooth_1, smooth_1_descr);
    add_smooth_function(&smooth_2, smooth_2_descr);
    add_smooth_function(&smooth_3, smooth_3_descr);
    add_smooth_function(&smooth_4, smooth_4_descr);
}
