# Ranjodh Sandhu (rss6py)

register pP {  
	pc:64 = 0; 
} 
register cC {
	SF:1 = 0;
	ZF:1 = 1;
}
c_ZF = (valE == 0);
c_SF = (valE >= 0x8000000000000000);
stall_C = (icode != OPQ);
pc = P_pc;

wire opcode:8, icode:4;
opcode = i10bytes[0..8];  
icode = opcode[4..8]; 


wire rB : 4;
wire rA : 4;
wire valC : 64;
wire val : 64;
wire val1 : 64;

rB = [
	icode in {RRMOVQ, IRMOVQ}: i10bytes[8..12];
	1: REG_NONE;
];

rA = [
	icode in {RRMOVQ, IRMOVQ}: i10bytes[12..16];
	1: REG_NONE;
];

valC = [
	icode == JXX : i10bytes[8..72];
	1 : i10bytes[16..80];
];

 val = [
	icode in {IRMOVQ, RMMOVQ, MRMOVQ}: 10;
	icode in {JXX, CALL} : 9;
	icode in {RRMOVQ, PUSHQ, POPQ, OPQ, CMOVXX} :2;
	1 : 1;
];

val1 = P_pc + val;

reg_srcA = [
	icode == RRMOVQ : rA;
	1 : REG_NONE;
];


reg_dstE = [
	icode in {IRMOVQ, RRMOVQ} : rB;
	1 : REG_NONE;
];


reg_inputE = [
	icode == RRMOVQ : reg_outputA;
	icode == IRMOVQ : valC;
	1 : 1;
];

Stat = [
	icode == HALT : STAT_HLT;
	icode > 0xb : STAT_INS;
	1 : STAT_AOK;
	];
	

p_pc = [
	icode == JXX : valC;
	1 : val1;
];