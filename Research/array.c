//#include <stdlib.h>
//#include <stdio.h>
//#include <assert.h>
extern void __VERIFIER_error() __attribute__ ((__noreturn__));
extern int __VERIFIER_nondet_int();
extern void __VERIFIER_assume(int x);
int maxVal(int a[], int a_size) /* modified the function signature */
{
  int y = a[0];
  for(int i = 0; i < a_size-2; i++)
    {
      if(a[i+1] > y)
	{
	  y = a[i+1];
	}
    }
  return y;
}


int main(){
  
  int a = __VERIFIER_nondet_int();
  int b = __VERIFIER_nondet_int();
  __VERIFIER_assume(b > a);
  int c = __VERIFIER_nondet_int();
  __VERIFIER_assume(c > b);
  int d = __VERIFIER_nondet_int();
  __VERIFIER_assume(d > c);
  int e = __VERIFIER_nondet_int();
  __VERIFIER_assume(e > d);

  int ar[5];

  ar[0] = a; /* changed "a" on the lefthand side to "ar" */
  ar[1] = b; /* changed "a" on the lefthand side to "ar" */
  ar[2] = c; /* changed "a" on the lefthand side to "ar" */
  ar[3] = d; /* changed "a" on the lefthand side to "ar" */
  ar[4] = e; /* changed "a" on the lefthand side to "ar" */
 
  int ind0 = __VERIFIER_nondet_int();
  __VERIFIER_assume( ind0 >= 0 && ind0 < 5);
  int ind1 = __VERIFIER_nondet_int();
  __VERIFIER_assume( ind1 >= 0 && ind1 < 5 && ind1 != ind0);
  int ind2 =__VERIFIER_nondet_int();
  __VERIFIER_assume( ind2 >= 0 && ind2 < 5 && ind2 != ind0 && ind2 != ind1);
  int ind3 = __VERIFIER_nondet_int();
  __VERIFIER_assume( ind3 >= 0 && ind3 < 5 && ind3 != ind0 && ind3 != ind1 && ind3 != ind2);
  int ind4 = __VERIFIER_nondet_int();
  __VERIFIER_assume( ind4 >= 0 && ind4 < 5 && ind4 != ind0 && ind4 != ind1 && ind4 != ind2 && ind4 != ind3);
  int arf[5];
 
  arf[0] = ar[ind0];
  arf[1] = ar[ind1];
  arf[2] = ar[ind2];
  arf[3] = ar[ind3];
  arf[4] = ar[ind4];
  int z = maxVal(arf, 5);  /* modified maxVal(arf) -> maxVal(arf, 5) */
  
  if(z!=e)
    {
      __VERIFIER_error();
    }
  
  return 0;
}
