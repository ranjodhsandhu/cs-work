#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "fcntl.h"
struct file {
  enum { FD_NONE, FD_PIPE, FD_INODE } type;
  int ref; // reference count
  char readable;
  char writable;
  struct pipe *pipe;
  struct inode *ip;
  uint off;
  int ffd;
};

struct stat {
  short type;  // Type of file
  int dev;     // File system's disk device
  uint ino;    // Inode number
  short nlink; // Number of links to file
  uint size;   // Size of file in bytes
};

#define PRINTLOG 0


void __strcpy(char* dest, char* src, int maxlen)
{
	int i = 0;
	for( i = 0; i < maxlen; i++ ) {
		dest[i] = src[i];
		if( src[i] == 0 ) {
			break;
		}
	}
	dest[maxlen-1] = 0;
}

int sys_record_start(void)
{
	char* filename;
	if( argstr(0, &filename) < 0 ) {
		return -1;
	}
	__strcpy(record_filename, filename, 255);
	record_syscalls = 1;
#if PRINTLOG
	cprintf("sys_record_start: %d, %s\n", record_syscalls, record_filename);
#endif 
	return record_syscalls;
}

int sys_record_stop(void)
{	
	if( record_syscalls == 1 ) {
		record_syscalls = 0;
	if(overload == 1){
		overload = 0;
		return record_syscalls;
	}
#if PRINTLOG		
		cprintf("sys_record_stop: %d\n", record_syscalls);
#endif
	my_sys_unlink(record_filename);
	struct file *f =  my_sys_open(record_filename, O_CREATE | O_RDWR);
	for(int i =  0; i < cur_record_index; i++){
		char out[1000];
		char * syscall1 = cur_record[i].syscall;
		int syscall1len = strlen(syscall1) + 1;
		int sz = 0;
		strncpy(out + sz, syscall1, syscall1len);
		sz += syscall1len;
		char * comma = ", ";
		int commalen = strlen(comma) + 1;
		strncpy(out + sz, comma, commalen);
		sz += commalen;
		if(cur_record[i].arg1sz != 0){
			char * arg1c = (char *)cur_record[i].arg1;
			int arg1clen = strlen(arg1c)+1;
			strncpy(out + sz, arg1c, arg1clen);
			sz += arg1clen;
			strncpy(out + sz, comma, commalen);
			sz += commalen;
		}
		if(cur_record[i].arg2sz != 0){
			char * arg2c = (char *)cur_record[i].arg2;
			int arg2clen = strlen(arg2c)+1;
			strncpy(out + sz, arg2c, arg2clen);
			sz += arg2clen;
			strncpy(out + sz, comma, commalen);
			sz += commalen;
		}
		if(cur_record[i].arg3sz != 0){
			char * arg3c = (char *)cur_record[i].arg3;
			int arg3clen = strlen(arg3c)+1;
			strncpy(out + sz, arg3c, arg3clen);
			sz += arg3clen;
			strncpy(out + sz, comma, commalen);
			sz += commalen;
		}
		if(cur_record[i].arg4sz != 0){
			char * arg4c = (char *)cur_record[i].arg4;
			int arg4clen = strlen(arg4c)+1;
			strncpy(out + sz, arg4c, arg4clen);
			sz += arg4clen;
			strncpy(out + sz, comma, commalen);
			sz += commalen;
		}
		char * syscallout = (char *)cur_record[i].output;
		int syscalloutlen = strlen(syscallout) + 1;
		strncpy(out + sz, syscallout, syscalloutlen);
		sz += syscalloutlen;
		char * bks = "\n";
		int bkslen = strlen(bks) + 1;
		strncpy(out + sz, bks, bkslen);
		sz += bkslen;
		/*my_sys_write(f, sizeof(cur_record[i].syscall), (char *)&cur_record[i].syscall);
		my_sys_write(f, sizeof(", "), ", ");
		if(cur_record[i].arg1sz != 0){
		my_sys_write(f, sizeof(cur_record[i].arg1), (char *)&cur_record[i].arg1);
		my_sys_write(f, sizeof(", "), ", ");	
		}	
		if(cur_record[i].arg2sz != 0){
		my_sys_write(f, sizeof(cur_record[i].arg2), (char *)&cur_record[i].arg2);
		my_sys_write(f, sizeof(", "), ", ");				
		}
		if(cur_record[i].arg3sz != 0){
		my_sys_write(f, sizeof(cur_record[i].arg3), (char *)&cur_record[i].arg3);
		my_sys_write(f, sizeof(", "), ", ");				
		}
		if(cur_record[i].arg4sz != 0){
		my_sys_write(f, sizeof(cur_record[i].arg4), (char *)&cur_record[i].arg4);
		my_sys_write(f, sizeof(", "), ", ");				
		}
		my_sys_write(f, sizeof(cur_record[i].output), (char *)&cur_record[i].output);
		my_sys_write(f, sizeof("\n"), "\n");*/
		my_sys_write(f, sz, out);
	}

	my_sys_close(f->ffd, f);}	
	for(int i = 0; i < cur_record_index; i++){
		cur_record[i] = empty;}
	cur_record_index = 0;
	return record_syscalls;
}

int sys_replay_start(void)
{
	char* filename;
	if( argstr(0, &filename) < 0 ) {
		return -1;
	}	
	__strcpy(replay_filename, filename, 255);
	replay_syscalls = 1;
#if PRINTLOG	
	cprintf("sys_replay_start: %d, %s\n", replay_syscalls, replay_filename);	
#endif	
	struct file *f = my_sys_open(record_filename, O_RDONLY);
	struct stat st;
	my_sys_fstat(f, &st);
	int siz = st.size;
	char out[1000000];
	int prev_comma = 0;
	int call = 0;
	int args = 0;
	my_sys_read(f, siz, out);
	for(int i = 0; i < siz; i++){
		if(strncmp(", ", out+i, sizeof(", ")) == 0){
			//char temp[200];
			//memmove(temp, &out[prev_comma], (i - prev_comma)*sizeof(*out));
			
			if(args == 0){
				memmove(recorded[call].syscall, &out[prev_comma], (i - prev_comma)*sizeof(*out));
			}
			if(args == 1){
				memmove(recorded[call].arg1, &out[prev_comma], (i - prev_comma)*sizeof(*out));
				recorded[call].arg1sz = sizeof(recorded[call].arg1);

			}
			if(args == 2){
				memmove(recorded[call].arg2, &out[prev_comma], (i - prev_comma)*sizeof(*out));
				recorded[call].arg2sz = sizeof(recorded[call].arg2);
			}
			if(args == 3){
				memmove(recorded[call].arg3, &out[prev_comma], (i - prev_comma)*sizeof(*out));
				recorded[call].arg3sz = sizeof(recorded[call].arg3);
			}
			if(args == 4){
				memmove(recorded[call].arg4, &out[prev_comma], (i - prev_comma)*sizeof(*out));
				recorded[call].arg4sz = sizeof(recorded[call].arg4);
			}
			prev_comma = i+3;
			args++;
			//cprintf("%s ", temp);
			//cprintf("Comma: %d\n", i);


		}
		if(strncmp("\n", out+i, sizeof("\n")) == 0){
			memmove(recorded[call].output, &out[prev_comma], (i - prev_comma)*sizeof(*out));
			prev_comma = i+2;
			call++;
			args = 0;
			//cprintf("Newline: %d\n", i);
		}
	}
	//
	// open the recorded file, and get the data ready for replay.
	// then close the file.
	//
	return replay_syscalls;
}

int sys_replay_stop(void)
{
	if( replay_syscalls == 1 ) {
		replay_syscalls = 0;
#if PRINTLOG		
		cprintf("sys_replay_stop: %d\n", replay_syscalls);
#endif		
	}
	for(int i = 0; i < cur_replay_index; i++){
		recorded[i] = empty;}
	cur_replay_index = 0;
	return replay_syscalls;
}



int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
	sys_replay_stop();
	sys_record_stop();
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
if(replay_syscalls == 1){
    if(strncmp(recorded[cur_replay_index].syscall, "11", sizeof("11")) != 0 ){
      cprintf("Error: Invalid Replay\n");
      exit();
    }
    int x = atoi(recorded[cur_replay_index].output);
    cur_replay_index++;
    return x;
  }


 if(record_syscalls == 1){
    if(cur_record_index < 1000){
	  memmove((void *)cur_record[cur_record_index].syscall, "11", strlen("11"));
	  itoa(myproc()->pid, cur_record[cur_record_index].output, 10);
	  cur_record_index++;
  }
    else{
    cprintf("Error: Too many system calls.\n");
	overload = 1;
    exit();
  }}	
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
	if(replay_syscalls == 1){
    
    if(strncmp(recorded[cur_replay_index].syscall, "14", sizeof("14")) != 0 ){
      cprintf("Error: Invalid Replay\n");
      exit();
    }
    int x = atoi(recorded[cur_replay_index].output);
    cur_replay_index++;
    return x;
  }


  uint xticks;
  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  if(record_syscalls == 1){
  if(cur_record_index < 1000){

	  memmove((void *)cur_record[cur_record_index].syscall, "14", strlen("14"));
	  itoa((int)xticks, cur_record[cur_record_index].output, 10);
	  cur_record_index++;
  }}
  return xticks;
}

int
sys_yield(void)
{
  yield();
  return 0;
}

int sys_shutdown(void)
{
  shutdown();
  return 0;
}

