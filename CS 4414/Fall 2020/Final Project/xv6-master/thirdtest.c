#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(){
    char *p = malloc(200);
    replay_start("record.txt");
    int fd = open("test.txt", O_CREATE | O_RDWR);
    printf(0, "FD: %d\n", fd);
    write(fd, "Hello World", strlen("Hello World"));
    read(fd, p, sizeof("Hello World"));
    close(fd);
    exit();
}