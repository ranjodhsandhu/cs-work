typedef unsigned int   uint;
typedef unsigned short ushort;
typedef unsigned char  uchar;
typedef uint pde_t;
struct syscall_record{
    char syscall[200];
    char arg1[200];
    int arg1sz;
    char arg2[200];
    int arg2sz;
    char arg3[200];
    int arg3sz;
    char arg4[200];
    int arg4sz;
    char output[200];
};
