#include "fat_internal.h"
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include<bits/stdc++.h> 
#include <unistd.h>
using namespace std;
char* image;
int ifd;
struct Fat32BPB bpb;
bool mounted = false;
struct fileDescriptor{
    string name = "";
    bool available = true;
    uint32_t size = 0;
    uint32_t clusternum = 0;
};
vector<fileDescriptor> fdv(128, fileDescriptor{});

vector<string> breakup(const string &path){
    string ps = path;
    vector<string> tokens;
    size_t p = 0;
    string token;
    if(ps.size() > 0 && ps[0] == '/'){
        tokens.push_back("/");
    }
    while((p = ps.find("/")) != string::npos){
        token = ps.substr(0, p);
        if(token != ""){
            tokens.push_back(ps.substr(0, p));
        }
        ps.erase(0, p + ((string)"/").length());
    }
    if(ps != ""){
        tokens.push_back(ps);
    }
    return tokens;
}

uint32_t get_subdir_cluster(vector<AnyDirEntry> dirs, string cp){
    transform(cp.begin(), cp.end(), cp.begin(), ::toupper);
    for(AnyDirEntry &d : dirs){
        string temp(reinterpret_cast<char const*>(d.dir.DIR_Name), sizeof d.dir.DIR_Name);
        while(temp.find(' ') != string::npos){
            temp.erase(temp.find(' '), 1);
        }
        while(cp.find(' ') != string::npos){
            cp.erase(temp.find(' '), 1);
        }
        if(strcmp(temp.c_str(), cp.c_str()) == 0){
            return (d.dir.DIR_FstClusHI << 8) | d.dir.DIR_FstClusLO;
        }
    }
    return (uint32_t)-1;
}

vector<AnyDirEntry> get_dirs(vector<uint32_t> clusters){
    vector<AnyDirEntry> entries;
    uint32_t reservedBytes = bpb.BPB_BytsPerSec * bpb.BPB_RsvdSecCnt;
    uint32_t clusterSize = bpb.BPB_BytsPerSec * bpb.BPB_SecPerClus;
    uint32_t FATBytes = bpb.BPB_BytsPerSec * bpb.BPB_FATSz32 * bpb.BPB_NumFATs;

    for(auto &cluster: clusters){
        if(cluster == 0){
            cluster = bpb.BPB_RootClus;
        }
       uint32_t cluster_base_address = ((cluster - 2) * clusterSize) + reservedBytes + FATBytes;
       for(uint32_t i = 0; i < clusterSize; i+= sizeof(AnyDirEntry)){
           AnyDirEntry e = *(AnyDirEntry*)&image[cluster_base_address + i];
           if (e.dir.DIR_Name[0]  == 0x00){
               continue;
           }
        if (e.dir.DIR_Attr & DirEntryAttributes::VOLUME_ID)
            continue;
        if ((e.dir.DIR_Attr & DirEntryAttributes::LONG_NAME_MASK) == DirEntryAttributes::LONG_NAME)
        continue;
        if (e.dir.DIR_Name[0] == 0xE5)
        continue;
            entries.push_back(e);
           
       }
    }
    return entries;
}

vector<uint32_t> get_clusters(uint32_t clusterNum){

    vector<uint32_t> nums;
    uint32_t curr_cluster_number = clusterNum;
    if(curr_cluster_number == 0){
            curr_cluster_number = bpb.BPB_RootClus;
        }
    while (curr_cluster_number != 0 && curr_cluster_number < 0x0FFFFFF8){
        if (curr_cluster_number == 0x0FFFFFF7){
            break;
        }
        nums.push_back(curr_cluster_number & 0x0FFFFFFF);
        uint32_t offset = curr_cluster_number*4;
        uint32_t start = bpb.BPB_BytsPerSec * bpb.BPB_RsvdSecCnt;
		uint32_t byteLocation = offset + start;
        uint32_t next_cluster_number = *(uint32_t*)&image[byteLocation];
		curr_cluster_number = next_cluster_number;
    }
    return nums;
}

AnyDirEntry *get_direntry(vector<AnyDirEntry> &des, string s){
    transform(s.begin(), s.end(), s.begin(), ::toupper); 
    for(auto &de : des){
        string extension;
        string filename;
        int i = 0;
        while(i < 11){
        if(de.dir.DIR_Name[i] == ' '){
            i++;
        }
        else if(i >= 8){
            extension.push_back(de.dir.DIR_Name[i]);
            i++;
        }
        else{
            filename.push_back(de.dir.DIR_Name[i]);
            i++;
        }
        }
        if(extension.size() > 0){
            filename = filename + "." + extension;
        }
        if(s == filename){
            return &de;
        }
    }
    return NULL;
}

bool fat_mount(const string &path) {
    struct stat buffer;
    stat(path.c_str(), &buffer);
    uint size = buffer.st_size;
    ifd = open(path.c_str(), O_RDONLY);
    if(ifd == -1){
        return false;
    }
    image = (char*) mmap(NULL, size, PROT_READ, MAP_PRIVATE, ifd, 0);
    if(image == MAP_FAILED){
        return false;
    }
    memcpy((void*) &bpb, (void*) image, sizeof(struct Fat32BPB));
    mounted = true;
    return true;
}

int fat_open(const string &path) {
    if(!mounted){
        return -1;
    }
    vector<string> cleanPath = breakup(path);
    if(cleanPath.size() == 0){
        return -1;
    }
    string fileName = cleanPath.at(cleanPath.size() - 1);
    int pos = path.find(fileName);
    string p = path.substr(0, pos);
    for(int i = 0; i < 128; i++){
        if(strcmp(fdv[i].name.c_str(), fileName.c_str()) == 0){
            return i;
        }
    }
    vector <AnyDirEntry> parent_entries = fat_readdir(p);
    AnyDirEntry* entry = get_direntry(parent_entries, fileName);
    if(entry == NULL || entry->dir.DIR_Attr == 0x10){
        return -1;
    }
    for(int i = 0; i < 128; i++){
        if(fdv[i].available){
            fdv[i].name = fileName;
            fdv[i].clusternum = (entry->dir.DIR_FstClusHI << 8) | entry->dir.DIR_FstClusLO;
            fdv[i].size = entry->dir.DIR_FileSize;
            fdv[i].available = false;
            return i;
        }
    }
    return -1;
}

bool fat_close(int fd) {
    if(!mounted){
        return false;
    }
    if(!fdv[fd].available){
        fdv[fd].available = true;
        return true;
    }
    return false;
}

int fat_pread(int fd, void *buffer, int count, int offset) {
    if(!mounted){
    return 0;}
    fileDescriptor fde = fdv[fd];
    if(fde.size == 0){
        return 0;
    }
    if(count == 0){
        return 0;
    }
    uint32_t reservedBytes = bpb.BPB_BytsPerSec * bpb.BPB_RsvdSecCnt;
    uint32_t clusterSize = bpb.BPB_BytsPerSec * bpb.BPB_SecPerClus;
    uint32_t FATBytes = bpb.BPB_BytsPerSec * bpb.BPB_FATSz32 * bpb.BPB_NumFATs;
    uint32_t cNum = fde.clusternum;
    vector<uint32_t> cls = get_clusters(cNum);
    uint32_t fSize = fde.size;
    uint32_t bytesSeen = 0;
    uint32_t bytesInBuffer = 0;
    for(uint i = 0; i < cls.size(); i++){
        for(uint j = ((cls[i] - 2) * clusterSize + reservedBytes + FATBytes); j < ((cls[i] - 2) * clusterSize + reservedBytes + FATBytes) + clusterSize; j++){
            if (bytesSeen == fSize || bytesInBuffer == (uint) count){
                break;
            }
            if(bytesSeen >= (uint)offset){
                *(((char *) buffer + bytesInBuffer)) = *(char *)&image[j];
                bytesInBuffer++;
            }
            bytesSeen++;
        }
    }
    return bytesInBuffer;      

}

vector<AnyDirEntry> fat_readdir(const string &path) {
    vector<string> tokenizedPath = breakup(path);
    vector<AnyDirEntry> result;
    if(!mounted){
        return result;
    }
    vector<uint32_t> root_clusters = get_clusters(bpb.BPB_RootClus);
    vector<AnyDirEntry> root_dirs = get_dirs(root_clusters);
    vector<AnyDirEntry> cur_dir = root_dirs;
    for(uint i = 0; i < tokenizedPath.size(); i++){
        cout << tokenizedPath[i] << endl;
        uint32_t subdir_cluster_num = -1;
        if(tokenizedPath[0] == "/" && tokenizedPath.size() == 1){
            subdir_cluster_num = bpb.BPB_RootClus;
            }
        else{
            if(tokenizedPath[i] != "/"){
            subdir_cluster_num = get_subdir_cluster(cur_dir, tokenizedPath[i]);
            vector<uint32_t> temp_clusters = get_clusters(subdir_cluster_num);
            cur_dir = get_dirs(temp_clusters);}
        }
        if(subdir_cluster_num != (uint32_t) -1){
            vector<uint32_t> subdirClusters = get_clusters(subdir_cluster_num);
            result = get_dirs(subdirClusters);
        }
    }
    
    return result;
}