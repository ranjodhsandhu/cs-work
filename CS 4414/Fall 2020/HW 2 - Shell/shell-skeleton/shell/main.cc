#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
using namespace std;
#define LIMIT 256


void parse_and_run_command(const std::string &command) {
    /* TODO: Implement this. */
    /* Note that this is not the correct way to test for the exit command.
       For example the command "   exit  " should also exit your shell.
     */
    istringstream ss(command);
    string token;
    

    int pflag = 0;
    vector<int> statuses;
    vector<pid_t> pids;
    struct commandStruct {
    vector<string> command;
    string inputFile;
    string outputFile;
    bool hasInput = false;
    bool hasOutput = false;
    bool hasPipe = false;
    int rfd = 0;
    int wfd= 0;};

    struct commandStruct com;
    vector<struct commandStruct> cStructs;
    struct commandStruct currentCommand;
    /*vector<string> temp;
    vector<vector<string>> args;*/

    /*tokens[a] = (char*)malloc(token.size()+1);
    args[b] = (char*)malloc(token.size()+1);
    strcpy(tokens[a], token.c_str());
    strcpy(args[b], tokens[a]);
    a++;
    b++;*/
    if(command == "exit"){
        exit(0);
    }
    while (ss >> token) {

        /*if(pflag == 1){
            pflag = 0;
            if(token[0] == '|'|| token[0] == '>' || token[0] == '<'){
            cerr << "Invalid Command" << endl; }
            tokens[a] = (char*)malloc(token.size()+1);
            strcpy(tokens[a], token.c_str());
            strcpy(args[b], tokens[a]);
            a++;
            b++;
            continue;
        }*/

            if(token == ">"){
            ss >> com.outputFile;
            if(strlen(com.outputFile.c_str()) == 0 || com.outputFile == "|"|| com.outputFile == ">" || com.outputFile == "<"){
            cerr << "Invalid Command" << endl; 
            return;}
            else{com.hasOutput = true;}}
  


            else if(token == "<"){
            ss >> com.inputFile;
            if(strlen(com.inputFile.c_str()) == 0 || com.inputFile == "|"|| com.inputFile == ">" || com.inputFile == "<"){
            cerr << "Invalid Command" << endl;
            return;
            }
            else{
            com.hasInput = true;}}


            else if(token == "|"){
                pflag = 1;
                com.hasPipe = true;
                cStructs.push_back(com);
                struct commandStruct com2;
                com = com2;
            }
            else{
                if(pflag == 1){
                    pflag = 0;
                }
                com.command.push_back(token);
            }}
        if(pflag == 1){
                cerr << "Invalid Command" << endl;
                return;}

        if(com.command.size() == 0){
            cerr << "Invalid Command" << endl;
        }
            cStructs.push_back(com);
        for(int j = 0; j < (int) cStructs.size(); j++){
            currentCommand = cStructs[j];
            if(currentCommand.command[0] == "exit"){
                exit(0);
            }
            int pipe_fd[2];
            if(currentCommand.hasPipe == true){
            if(pipe(pipe_fd) < 0){
            cerr << "Out of File Descriptors" << endl;}

            if(j != (int) (cStructs.size()-1)){
            cStructs[j+1].rfd = pipe_fd[0];}
            currentCommand.wfd = pipe_fd[1];}

            int pid = fork();
            if(pid > 0){

            int spid = pid;
            pids.push_back(spid);
            close(pipe_fd[1]);
            if(j > 0){
            close(currentCommand.rfd);
            }
            }

            if(pid == 0){
            if(j != 0){
                dup2(currentCommand.rfd, STDIN_FILENO);
                close(currentCommand.rfd);
            }
            if(j != (int)cStructs.size() - 1){
                dup2(currentCommand.wfd, STDOUT_FILENO);
                close(currentCommand.wfd);
            }

            if(currentCommand.hasOutput == true){
                int out_fd = open(currentCommand.outputFile.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0666);
                dup2(out_fd, 1);
                close(out_fd);              
            }
            if(currentCommand.hasInput == true){
                int in_fd = open(currentCommand.inputFile.c_str(), O_RDONLY, 0666);
                dup2(in_fd, 0);
                close(in_fd);}

            vector<char *> args(currentCommand.command.size() + 1);
            for(int i = 0; i < (int) currentCommand.command.size(); i++){
                args[i] = (char *) currentCommand.command[i].c_str();
            }
            args[currentCommand.command.size()] = NULL;
            execv(currentCommand.command[0].c_str(), args.data());
            cerr << "Command not found" << endl;
            exit(1);}
            
            if(pid < 0){
                cerr << "Pipe Error" << endl;}}
        
        for (int j = 0; j < (int) pids.size(); j++){
            int status;
        waitpid(pids[j], &status, 0);
        statuses.push_back(status);}


    for(int j= 0; j < (int) statuses.size(); j++){
        if (WIFEXITED(statuses[j])){
        cout << cStructs[j].command[0] << " exit status: " << WEXITSTATUS(statuses[j]) << endl;}}
    }




int main(void) {
    string command;
    cout << "> ";
    while (getline(cin, command)) {
        parse_and_run_command(command);
        cout << "> ";
    }
    return 0;
}


