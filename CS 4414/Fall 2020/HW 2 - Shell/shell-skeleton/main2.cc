#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
using namespace std;
#define LIMIT 256


void parse_and_run_command(const std::string &command) {
    /* TODO: Implement this. */
    /* Note that this is not the correct way to test for the exit command.
       For example the command "   exit  " should also exit your shell.
     */
    istringstream ss(command);
    string token;
    pid_t pid;
    pid_t spid;
    int a = 0;
    int b = 0;
    int it = 0;
    int iflag = 0;
    int oflag = 0;
    int pflag = 0;
    int eflag = 0;
    int in_fd;
    int out_fd;
    int status;
    char i[LIMIT];
    char o[LIMIT];
    vector<char *> tokens;
    vector<vector<char *>> args;
    vector<char *> temp;
    /*vector<string> temp;
    vector<vector<string>> args;*/
    char *chtokens[LIMIT];
    char *chargs[LIMIT];

    i[0] = '\0'; o[0] = '\0';
    ss >> token;
    chargs[b] = (char*)malloc(token.size()+1);
    chtokens[a] = (char*)malloc(token.size()+1);
    strcpy(chtokens[a], token.c_str());
    strcpy(chargs[b], chtokens[a]);
    tokens.push_back(chtokens[a]);
    temp.push_back(chargs[b]);
    a++;
    b++;
    /*tokens[a] = (char*)malloc(token.size()+1);
    args[b] = (char*)malloc(token.size()+1);
    strcpy(tokens[a], token.c_str());
    strcpy(args[b], tokens[a]);
    a++;
    b++;*/
    while (ss >> token) {
        /*if(pflag == 1){
            pflag = 0;
            if(token[0] == '|'|| token[0] == '>' || token[0] == '<'){
            cerr << "Invalid Command" << endl; }
            tokens[a] = (char*)malloc(token.size()+1);
            strcpy(tokens[a], token.c_str());
            strcpy(args[b], tokens[a]);
            a++;
            b++;
            continue;
        }*/
        if(pflag == 1){
            pflag = 0;
            if(token == "exit"){
            tokens.clear();
            args.clear();
            exit(0);
            }
            if(token[0] == '|'|| token[0] == '>' || token[0] == '<' || token.length() == 0){
            cerr << "Invalid Command" << endl; }
            chtokens[a] = (char*)malloc(token.size()+1);
            strcpy(chtokens[a], token.c_str());
            tokens.push_back(chtokens[a]);
            a++;
            args.push_back(temp);
            temp.clear();
            continue;
        }
	 if(token == ">"){
            ss >> o;
            if(strlen(o) == 0 || o[0] == '|' || o[0] == '>' || o[0] == '<' ){
                cerr << "Invalid Command" << endl;
                eflag++;
            }
            oflag++;
        }
        else if(token == "<"){
            ss >> i;
            if(strlen(i) == 0 || i[0] == '|'|| i[0] == '>' || i[0] == '<' ){
                cerr << "Invalid Command" << endl;
                eflag++;
            }
            iflag++;
        }
        else{
            if(token == "|"){
                pflag = 1;
                continue;
            }
            chargs[b] = (char*)malloc(token.size()+1);
            strcpy(chargs[b], token.c_str());
            temp.push_back(chargs[b]);
            b++;
            /*args[b] = (char*)malloc(token.size()+1);
	        strcpy(args[b], token.c_str());
	        b++;
            */
        }}
        temp.push_back(NULL);
        args.push_back(temp);
        temp.clear();
        /*tokens[a] = NULL;
        args[b] = NULL;*/
    if (tokens.size() > 0 && pflag != 1 && eflag != 1) {
    if (strcmp(tokens[0], "exit") == 0){
    /*it= 0;
    while(tokens[it] != NULL){
        free(tokens[it]);
        it++;}
    it= 0;
    while(args[it] != NULL){
        free(args[it]);
        it++;}
    it= 0;*/
    
    tokens.clear();
    args.clear();
    exit(0);
    }

    spid = 0;
    for(it= 0; it< (int) tokens.size(); it++){
        pid = fork();
        if(pid != 0){spid = pid;}
        else{
            if(iflag != 0){
                in_fd = open(i, O_RDONLY);
                dup2(in_fd, 0);
                close(in_fd);
            }
            if(oflag != 0){
                out_fd = open(o, O_WRONLY | O_CREAT | O_TRUNC, 0666);
                dup2(out_fd, 1);
                close(out_fd);
            }
            
            char *tempargs[256];
            copy(args[it].begin(), args[it].end(), tempargs);
            execv(tokens[it], tempargs);
            exit(1);
        }
    }
    for(it= 0; it < (int) tokens.size(); it++){
        waitpid(spid, &status, 0);
        if (WEXITSTATUS(status) == 1){
            cerr << "Not implemented.\n";
        }
        cout << tokens[it] << " exit status: " << WEXITSTATUS(status) << endl;
    }
    it= 0;
    while(chtokens[it] != NULL){
        free(chtokens[it]);
        it++;}
    it= 0;
    while(chargs[it] != NULL){
        free(chargs[it]);
        it++;}
    it= 0;
    tokens.clear();
    args.clear();
    }
    else if(tokens.size() <= 0){
        cerr << "No Commands.\n";
    }
    else{
            it= 0;
    while(chtokens[it] != NULL){
        free(chtokens[it]);
        it++;}
    it= 0;
    while(chargs[it] != NULL){
        free(chargs[it]);
        it++;}
        it= 0;
    tokens.clear();
    args.clear();
    }
    
    }

int main(void) {
    string command;
    cout << "> ";
    while (getline(cin, command)) {
        parse_and_run_command(command);
        cout << "> ";
    }
    return 0;
}


