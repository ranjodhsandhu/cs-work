#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
using namespace std;
#define LIMIT 256


void parse_and_run_command(const std::string &command) {
    /* TODO: Implement this. */
    /* Note that this is not the correct way to test for the exit command.
       For example the command "   exit  " should also exit your shell.
     */
    istringstream ss(command);
    string token;
    pid_t pid;
    pid_t spid;
    int a = 0;
    int b = 0;
    int it = 0;
    int iflag = 0;
    int oflag = 0;
    int pflag = 0;
    int eflag = 0;
    int in_fd;
    int out_fd;
    int itr;
    int checkpoint = 0;
    int status;
    char i[LIMIT];
    char o[LIMIT];
    char *tokens[LIMIT];
    char *args[LIMIT];
   // int pipe_fd[2];
    i[0] = '\0'; o[0] = '\0';
    ss >> token;
    tokens[a] = (char*)malloc(token.size()+1);
    args[b] = (char*)malloc(token.size()+1);
    strcpy(tokens[a], token.c_str());
    strcpy(args[b], tokens[a]);
    a++;
    b++;
    while (ss >> token) {
        if(pflag == 1){
            if(token[0] == '|' || token[0] == '>' || token[0] == '<' ){
                cerr << "Invalid Command" << endl;
                continue;
            }
            tokens[a] = (char*)malloc(token.size()+1);
            args[b] = (char*)malloc(token.size()+1);
            strcpy(tokens[a], token.c_str());
            strcpy(args[b], tokens[a]);
            a++;
            b++;
            pflag = 0;
            continue;
        }
	 if(token == ">"){
            ss >> o;
            if(strlen(o) == 0 || o[0] == '|' || o[0] == '>' || o[0] == '<' ){
                cerr << "Invalid Command" << endl;
                eflag++;
            }
            oflag++;
        }
        else if(token == "<"){
            ss >> i;
            if(strlen(i) == 0 || i[0] == '|'|| i[0] == '>' || i[0] == '<' ){
                cerr << "Invalid Command" << endl;
                eflag++;
            }
            iflag++;
        }
        else{
            if(token == "|"){
                pflag = 1;
                continue;
            }
            args[b] = (char*)malloc(token.size()+1);
	        strcpy(args[b], token.c_str());
	        b++;
        }}
        if (pflag == 1){
            cerr << "Invalid Command.\n";
        }
        tokens[a] = NULL;
        args[b] = NULL;
    if (a > 0 && pflag != 1 && eflag != 1) {
   
    itr = 0;
    
    while (tokens[itr] != NULL){
    if (strcmp(tokens[itr], "exit") == 0){
    it= 0;
    while(tokens[it] != NULL){
        free(tokens[it]);
        it++;}
    it= 0;
    while(args[it] != NULL){
        free(args[it]);
        it++;}
    it= 0;
    exit(0);
    }
    itr++;}

    spid = 0;
    for(it= 0; it < a; it++){
        /*if(pipe(pipe_fd) < 0){
            cerr << "Out of File Descriptors" << endl;
        }*/
        pid = fork();

        if(pid > 0){
            spid = pid;
            }
        else if (pid == 0){
            /*if (it == 0){}
            if (it == a-1){}
            dup2(pipe_fd[1], STDOUT_FILENO);
            close(pipe_fd[0]);
            close(pipe_fd[1]);*/

            if(iflag != 0){
                in_fd = open(i, O_RDONLY);
                dup2(in_fd, 0);
                close(in_fd);
            }
            if(oflag != 0){
                out_fd = open(o, O_WRONLY | O_CREAT | O_TRUNC, 0666);
                dup2(out_fd, 1);
                close(out_fd);
            }
            itr = 0;
            char *tempargs[LIMIT];
            if(it < a-1){
                while(strcmp(tokens[it + 1], args[checkpoint]) != 0){
                    tempargs[itr] = (char*)malloc(2*sizeof(args[checkpoint]));
                    strcpy(tempargs[itr], args[checkpoint]);
                    itr++;
                    checkpoint++;
                }}
            if(it == a-1){
                    tempargs[itr] = (char*)malloc(2*sizeof(args[checkpoint]));
                    strcpy(tempargs[itr], args[checkpoint]);
                    itr++;
                    checkpoint++;
            }
            tempargs[itr] = NULL;
            execv(tokens[it], tempargs);
            itr = 0;
            while(tempargs[itr] != NULL){
                free(tempargs[itr]);
                itr++;
            }
            itr = 0;
        }
        else{
            cerr << "Error" << endl;
        }}
    for(it= 0; it< a; it++){
        waitpid(spid, &status, 0);
        if (WEXITSTATUS(status) == 1 && tokens[1] != NULL){
            cerr << "Not implemented.\n";
        }
        cout << tokens[0] << " exit status: " << WEXITSTATUS(status) << endl;
    }
    it= 0;
    while(tokens[it] != NULL){
        free(tokens[it]);
        it++;}
    it= 0;
    while(args[it] != NULL){
        free(args[it]);
        it++;}
    }
    else if(a <= 0){
        cerr << "No Commands.\n";
    }
    else{
            it= 0;
    while(tokens[it] != NULL){
        free(tokens[it]);
        it++;}
    it= 0;
    while(args[it] != NULL){
        free(args[it]);
        it++;}
    }
    
    }

int main(void) {
    string command;
    cout << "> ";
    while (getline(cin, command)) {
        parse_and_run_command(command);
        cout << "> ";
    }
    return 0;
}


