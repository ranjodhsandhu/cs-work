#include "pool.h"
using namespace std;
Task::Task() {
}

Task::~Task() {
}

ThreadPool::ThreadPool(int num_threads) {
    end = false;
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&jobReady, NULL);
    pthread_cond_init(&jobStopped, NULL);
    pthreads.resize(num_threads);
    for (int i = 0; i < num_threads; i++){
        pthread_create(&pthreads[i], NULL, RunFunction, (void *)this);}
}

void* RunFunction(void *threadp){
    while (true){
        ThreadPool *tmp = (ThreadPool *) threadp;
        pthread_mutex_lock(&tmp -> lock);
        while(tmp->end == false && tmp->pts.size() == 0){
		 	pthread_cond_wait(&tmp->jobReady, &tmp->lock);}
        if(tmp->end == true && tmp->pts.size() == 0){
            pthread_mutex_unlock(&tmp->lock);
            break;}
        Task *temp = tmp->pts.front();
        tmp->pts.pop_front();
        pthread_mutex_unlock(&tmp->lock);
        temp -> Run();
        pthread_mutex_lock(&tmp->lock);
        tmp->hmap.insert(pair<string, Task *>(temp -> taskName, temp));
        pthread_mutex_unlock(&tmp->lock);
        pthread_cond_broadcast(&tmp->jobStopped);
        delete temp;
    }
    return NULL;
}

void ThreadPool::SubmitTask(const string &name, Task* task) {
    pthread_mutex_lock(&lock);
    task -> taskName = name;
    pts.push_back(task);
    pthread_cond_signal(&jobReady);
    pthread_mutex_unlock(&lock);
}

void ThreadPool::WaitForTask(const string &name) {
    pthread_mutex_lock(&lock);
    while(hmap.count(name) == 0){
    pthread_cond_wait(&jobStopped, &lock);}
    pthread_mutex_unlock(&lock);
}

void ThreadPool::Stop() {
    pthread_mutex_lock(&lock);
    end = true;
    pts.clear();
    pthread_cond_broadcast(&jobReady);
    pthread_mutex_unlock(&lock);
    for (int i = 0; i < (int) pthreads.size(); i++){
        pthread_join(pthreads[i], NULL);}
    pthread_cond_destroy(&jobReady);
    pthread_cond_destroy(&jobStopped);
    pthread_mutex_destroy(&lock);
}
