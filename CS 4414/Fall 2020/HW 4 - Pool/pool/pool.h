#ifndef POOL_H_
#include <string>
#include <pthread.h>
#include <map>
#include <vector>
#include <deque>
using namespace std;
class Task {
public:
    Task();
    virtual ~Task();
    string taskName; 
    virtual void Run() = 0;  // implemented by subclass
};


class ThreadPool {
public:
    bool end;
    ThreadPool(int num_threads);
    vector<pthread_t> pthreads;
    pthread_mutex_t lock;
    pthread_cond_t jobReady;
    pthread_cond_t jobStopped;
    deque<Task *> pts;
    map<string, Task *> hmap;
    void SubmitTask(const std::string &name, Task *task);
    void WaitForTask(const std::string &name);
    void Stop();
};
void* RunFunction(void* threadp);
#endif
