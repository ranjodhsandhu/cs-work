#include "pagingtestlib.h"

void read_guard_page() {
    __asm__ volatile(
        "mov %%esp, %%eax\n"
        "and $0xFFFFF000, %%eax\n"
        "sub $0x800, %%eax\n"
        "mov (%%eax), %%eax\n"
    ::: "%eax", "memory"
    );
}

int main(void) {
    setup();
    test_simple_crash(read_guard_page,
        PASS_MSG "reading from guard page killed process",
        FAIL_MSG "reading from guard page did not kill process?");
    finish();
}
