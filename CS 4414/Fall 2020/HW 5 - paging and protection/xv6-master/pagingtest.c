#include "pagingtestlib.h"

int
main(int argc, char *argv[])
{
    setup();
    int passed = 0;
    passed += test_out_of_bounds_fork(1, 
        PASS_MSG "reading immediately after heap end crashes",
        FAIL_MSG "reading immediately after heap end does NOT crash");
    passed += test_out_of_bounds_fork(1024 * 1024 * 1024 * 2U, 
        PASS_MSG "reading 2GB after heap end crashes",
        FAIL_MSG "reading 2GB after heap end does NOT crash");
    passed += test_allocation_fork(32 * 1024, "32KB", "two 4KB", 4 * 1024,  4096, 16 * 1024, 4096); 
    passed += test_allocation_fork(768 * 1024 * 1024, "768MB", "two 8KB", 0, 8192, 256 * 1024 * 1024, 8192); 
    passed += test_allocation_fork(768 * 1024 * 1024, "768MB", "two 32KB", 128 * 1024 * 1024, 32 * 1024, 384 * 1024 * 1024, 32 * 1024); 
    passed += test_copy_on_write(32 * 1024, "32KB", 2); 
    passed += test_copy_on_write_alt(32 * 1024, "32KB", 2); 
    passed += test_copy_on_write_alloc_unused(4 * 1024, "4KB", 32 * 1024, "32KB", 2); 
    passed += test_copy_on_write_alloc_unused(512 * 1024 * 1024, "512MB", 32 * 1024, "32KB", 2); 
    passed += test_copy_on_write(64 * 1024 * 1024, "64MB", 7); 
    passed += test_copy_on_write_alt(64 * 1024 * 1024, "64MB", 4); 
    printf(1, "running repeated copy-on-write tests to make sure "
              "memory is eventually reclaimed\n");
    for (int i = 0; i < 10; ++i) {
        passed += test_copy_on_write(100 * 1024 * 1024, "100MB", 2); 
    }
    passed += test_read_into_alloc(
        4096 * 16, 4096 * 8, 128, "128KB", "32KB"
    );
    passed += test_read_into_cow(
        4096 * 16, 4096 * 8, 128, "128KB", "32KB"
    );
    printf(1, "passed %d tests of 23\n", passed);
    finish();
}
