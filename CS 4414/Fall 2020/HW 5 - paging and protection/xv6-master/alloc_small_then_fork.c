#include "pagingtestlib.h"

int main() {
    setup();
    printf(1, COUNT_MSG, 2);
    test_allocation_then_fork(4096, "4K", "1K", 512, 1024, 1024+512,512, 1, 0);
    finish();
}
