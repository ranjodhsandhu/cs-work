//rss6py
#include "life.h"
#include <pthread.h>
#include <stdio.h>
#include <iostream>

struct life_pt{
    LifeBoard *currentState;
    LifeBoard *nextState;
    pthread_barrier_t *ptBarrier;
    int id;
    int s;
    int sz;

};
void switchState(life_pt *in){
    LifeBoard *t = in -> currentState;
    in -> currentState = in -> nextState;
    in -> nextState = t;
}
void *pRoutine(void *inputPT){
    life_pt *pt = (life_pt *)inputPT;
    pthread_barrier_t *b = pt -> ptBarrier;
    int st = pt -> s;
    int w = pt -> currentState -> width();
    int h = pt -> currentState -> height();
    int sz = pt -> sz;
    int ind = pt -> id;
    int i = 0;
    while (i < st){
        for(int y = pt -> id; y < sz + ind; ++y){
            for(int x= 1; x < w - 1; ++x){
                if((x != 0) && (y != 0) && (x != w-1) && (y != h-1)){   
                int liw = 0;
                for (int yOff = -1; yOff <= 1; ++yOff){
                    for(int xOff = -1; xOff <= 1; ++xOff){
                        if(pt -> currentState -> at(x + xOff, y + yOff)){
                            ++liw;
                        }
                    }
                }
                int a = ((liw == 4 && pt->currentState->at(x, y)) || liw == 3);
                pt->nextState->at(x, y) = a;
            }
        }
        }
    pthread_barrier_wait(b);
    switchState(pt);
    pthread_barrier_wait(b);
    i++;
    }
    return NULL;
}

void simulate_life_parallel(int threads, LifeBoard& st, int steps)
{
    life_pt *pt = (life_pt *)calloc(threads, sizeof(life_pt));
    pthread_t *tArr = (pthread_t *)calloc(threads, sizeof(pthread_t));
    LifeBoard*currentState = &st;
    pthread_barrier_t b;
    pthread_barrier_init(&b, NULL, threads+1);
    LifeBoard next(currentState->width(), currentState->height());
    LifeBoard *nextState = &next;
    int szc = currentState -> height();
    int c = szc / threads;
    int id = 0;
    int i = 0;
    while(i < threads){
        pt[i].nextState = nextState;
        pt[i].currentState = currentState;
        pt[i].id = id;
        pt[i].s = steps;
        pt[i].ptBarrier = &b;
        if(i == threads-1){
            pt[i].sz = c + szc % threads;
        }
        else{
            pt[i].sz = c;
            id += c;
        }
        i++;
    }
    i = 0;
    while(i < threads){
        pthread_create(&tArr[i], NULL, pRoutine, (void *)(&pt[i])); 
        i++;
    }
    i = 0;
    while(i < steps) {
        pthread_barrier_wait(&b);
        LifeBoard* t = currentState;
        currentState = nextState;
        nextState = t;
        pthread_barrier_wait(&b);
        i++;
    }
    i = 0;
    while( i < threads){
        pthread_join(tArr[i], NULL);
        i++;
    }
    st = *currentState;
    free(pt);
    pthread_barrier_destroy(&b);
    free(tArr);
}
