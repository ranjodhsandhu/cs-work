#include "pagingtestlib.h"

int main() {
    setup();
    printf(1, COUNT_MSG, 2);
    test_allocation_then_fork(700 * 1024 * 1024, "700MB", "8K",8 * 1024, 8 * 1024, 64 * 1024, 8 * 1024, 1, 0);
    finish();
}
