
_processlist:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "mmu.h"
#include "param.h"
#include "proc.h"
#include "user.h"
int main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	53                   	push   %ebx
   e:	51                   	push   %ecx
   f:	81 ec 1c 03 00 00    	sub    $0x31c,%esp
    struct processes_info info;
    info.num_processes = -9999;  // to make sure getprocessesinfo() doesn't
  15:	c7 85 f4 fc ff ff f1 	movl   $0xffffd8f1,-0x30c(%ebp)
  1c:	d8 ff ff 
                                 // depend on its initial value
    getprocessesinfo(&info);
  1f:	8d 85 f4 fc ff ff    	lea    -0x30c(%ebp),%eax
  25:	50                   	push   %eax
  26:	e8 da 02 00 00       	call   305 <getprocessesinfo>
    if (info.num_processes < 0) {
  2b:	83 c4 10             	add    $0x10,%esp
  2e:	83 bd f4 fc ff ff 00 	cmpl   $0x0,-0x30c(%ebp)
  35:	78 2e                	js     65 <main+0x65>
        printf(1, "ERROR: negative number of processes!\n"
                  "Myabe getprocessesinfo() assumes that num_processes is\n"
                  "always initialized to 0?\n");
    }
    printf(1, "%d running processes\n", info.num_processes);
  37:	83 ec 04             	sub    $0x4,%esp
  3a:	ff b5 f4 fc ff ff    	pushl  -0x30c(%ebp)
  40:	68 e0 06 00 00       	push   $0x6e0
  45:	6a 01                	push   $0x1
  47:	e8 63 03 00 00       	call   3af <printf>
    printf(1, "PID\tTICKETS\tTIMES-SCHEDULED\n");
  4c:	83 c4 08             	add    $0x8,%esp
  4f:	68 f6 06 00 00       	push   $0x6f6
  54:	6a 01                	push   $0x1
  56:	e8 54 03 00 00       	call   3af <printf>
    for (int i = 0; i < info.num_processes; ++i) {
  5b:	83 c4 10             	add    $0x10,%esp
  5e:	bb 00 00 00 00       	mov    $0x0,%ebx
  63:	eb 3e                	jmp    a3 <main+0xa3>
        printf(1, "ERROR: negative number of processes!\n"
  65:	83 ec 08             	sub    $0x8,%esp
  68:	68 68 06 00 00       	push   $0x668
  6d:	6a 01                	push   $0x1
  6f:	e8 3b 03 00 00       	call   3af <printf>
  74:	83 c4 10             	add    $0x10,%esp
  77:	eb be                	jmp    37 <main+0x37>
        printf(1, "%d\t%d\t%d\n", info.pids[i], info.tickets[i], info.times_scheduled[i]);
  79:	83 ec 0c             	sub    $0xc,%esp
  7c:	ff b4 9d f8 fd ff ff 	pushl  -0x208(%ebp,%ebx,4)
  83:	ff b4 9d f8 fe ff ff 	pushl  -0x108(%ebp,%ebx,4)
  8a:	ff b4 9d f8 fc ff ff 	pushl  -0x308(%ebp,%ebx,4)
  91:	68 13 07 00 00       	push   $0x713
  96:	6a 01                	push   $0x1
  98:	e8 12 03 00 00       	call   3af <printf>
    for (int i = 0; i < info.num_processes; ++i) {
  9d:	83 c3 01             	add    $0x1,%ebx
  a0:	83 c4 20             	add    $0x20,%esp
  a3:	39 9d f4 fc ff ff    	cmp    %ebx,-0x30c(%ebp)
  a9:	7f ce                	jg     79 <main+0x79>
    }
    exit();
  ab:	e8 8d 01 00 00       	call   23d <exit>

000000b0 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  b0:	55                   	push   %ebp
  b1:	89 e5                	mov    %esp,%ebp
  b3:	53                   	push   %ebx
  b4:	8b 45 08             	mov    0x8(%ebp),%eax
  b7:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  ba:	89 c2                	mov    %eax,%edx
  bc:	0f b6 19             	movzbl (%ecx),%ebx
  bf:	88 1a                	mov    %bl,(%edx)
  c1:	8d 52 01             	lea    0x1(%edx),%edx
  c4:	8d 49 01             	lea    0x1(%ecx),%ecx
  c7:	84 db                	test   %bl,%bl
  c9:	75 f1                	jne    bc <strcpy+0xc>
    ;
  return os;
}
  cb:	5b                   	pop    %ebx
  cc:	5d                   	pop    %ebp
  cd:	c3                   	ret    

000000ce <strcmp>:

int
strcmp(const char *p, const char *q)
{
  ce:	55                   	push   %ebp
  cf:	89 e5                	mov    %esp,%ebp
  d1:	8b 4d 08             	mov    0x8(%ebp),%ecx
  d4:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  d7:	eb 06                	jmp    df <strcmp+0x11>
    p++, q++;
  d9:	83 c1 01             	add    $0x1,%ecx
  dc:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  df:	0f b6 01             	movzbl (%ecx),%eax
  e2:	84 c0                	test   %al,%al
  e4:	74 04                	je     ea <strcmp+0x1c>
  e6:	3a 02                	cmp    (%edx),%al
  e8:	74 ef                	je     d9 <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
  ea:	0f b6 c0             	movzbl %al,%eax
  ed:	0f b6 12             	movzbl (%edx),%edx
  f0:	29 d0                	sub    %edx,%eax
}
  f2:	5d                   	pop    %ebp
  f3:	c3                   	ret    

000000f4 <strlen>:

uint
strlen(const char *s)
{
  f4:	55                   	push   %ebp
  f5:	89 e5                	mov    %esp,%ebp
  f7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
  fa:	ba 00 00 00 00       	mov    $0x0,%edx
  ff:	eb 03                	jmp    104 <strlen+0x10>
 101:	83 c2 01             	add    $0x1,%edx
 104:	89 d0                	mov    %edx,%eax
 106:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
 10a:	75 f5                	jne    101 <strlen+0xd>
    ;
  return n;
}
 10c:	5d                   	pop    %ebp
 10d:	c3                   	ret    

0000010e <memset>:

void*
memset(void *dst, int c, uint n)
{
 10e:	55                   	push   %ebp
 10f:	89 e5                	mov    %esp,%ebp
 111:	57                   	push   %edi
 112:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 115:	89 d7                	mov    %edx,%edi
 117:	8b 4d 10             	mov    0x10(%ebp),%ecx
 11a:	8b 45 0c             	mov    0xc(%ebp),%eax
 11d:	fc                   	cld    
 11e:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 120:	89 d0                	mov    %edx,%eax
 122:	5f                   	pop    %edi
 123:	5d                   	pop    %ebp
 124:	c3                   	ret    

00000125 <strchr>:

char*
strchr(const char *s, char c)
{
 125:	55                   	push   %ebp
 126:	89 e5                	mov    %esp,%ebp
 128:	8b 45 08             	mov    0x8(%ebp),%eax
 12b:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 12f:	0f b6 10             	movzbl (%eax),%edx
 132:	84 d2                	test   %dl,%dl
 134:	74 09                	je     13f <strchr+0x1a>
    if(*s == c)
 136:	38 ca                	cmp    %cl,%dl
 138:	74 0a                	je     144 <strchr+0x1f>
  for(; *s; s++)
 13a:	83 c0 01             	add    $0x1,%eax
 13d:	eb f0                	jmp    12f <strchr+0xa>
      return (char*)s;
  return 0;
 13f:	b8 00 00 00 00       	mov    $0x0,%eax
}
 144:	5d                   	pop    %ebp
 145:	c3                   	ret    

00000146 <gets>:

char*
gets(char *buf, int max)
{
 146:	55                   	push   %ebp
 147:	89 e5                	mov    %esp,%ebp
 149:	57                   	push   %edi
 14a:	56                   	push   %esi
 14b:	53                   	push   %ebx
 14c:	83 ec 1c             	sub    $0x1c,%esp
 14f:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 152:	bb 00 00 00 00       	mov    $0x0,%ebx
 157:	8d 73 01             	lea    0x1(%ebx),%esi
 15a:	3b 75 0c             	cmp    0xc(%ebp),%esi
 15d:	7d 2e                	jge    18d <gets+0x47>
    cc = read(0, &c, 1);
 15f:	83 ec 04             	sub    $0x4,%esp
 162:	6a 01                	push   $0x1
 164:	8d 45 e7             	lea    -0x19(%ebp),%eax
 167:	50                   	push   %eax
 168:	6a 00                	push   $0x0
 16a:	e8 e6 00 00 00       	call   255 <read>
    if(cc < 1)
 16f:	83 c4 10             	add    $0x10,%esp
 172:	85 c0                	test   %eax,%eax
 174:	7e 17                	jle    18d <gets+0x47>
      break;
    buf[i++] = c;
 176:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 17a:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 17d:	3c 0a                	cmp    $0xa,%al
 17f:	0f 94 c2             	sete   %dl
 182:	3c 0d                	cmp    $0xd,%al
 184:	0f 94 c0             	sete   %al
    buf[i++] = c;
 187:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 189:	08 c2                	or     %al,%dl
 18b:	74 ca                	je     157 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 18d:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 191:	89 f8                	mov    %edi,%eax
 193:	8d 65 f4             	lea    -0xc(%ebp),%esp
 196:	5b                   	pop    %ebx
 197:	5e                   	pop    %esi
 198:	5f                   	pop    %edi
 199:	5d                   	pop    %ebp
 19a:	c3                   	ret    

0000019b <stat>:

int
stat(const char *n, struct stat *st)
{
 19b:	55                   	push   %ebp
 19c:	89 e5                	mov    %esp,%ebp
 19e:	56                   	push   %esi
 19f:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 1a0:	83 ec 08             	sub    $0x8,%esp
 1a3:	6a 00                	push   $0x0
 1a5:	ff 75 08             	pushl  0x8(%ebp)
 1a8:	e8 d0 00 00 00       	call   27d <open>
  if(fd < 0)
 1ad:	83 c4 10             	add    $0x10,%esp
 1b0:	85 c0                	test   %eax,%eax
 1b2:	78 24                	js     1d8 <stat+0x3d>
 1b4:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 1b6:	83 ec 08             	sub    $0x8,%esp
 1b9:	ff 75 0c             	pushl  0xc(%ebp)
 1bc:	50                   	push   %eax
 1bd:	e8 d3 00 00 00       	call   295 <fstat>
 1c2:	89 c6                	mov    %eax,%esi
  close(fd);
 1c4:	89 1c 24             	mov    %ebx,(%esp)
 1c7:	e8 99 00 00 00       	call   265 <close>
  return r;
 1cc:	83 c4 10             	add    $0x10,%esp
}
 1cf:	89 f0                	mov    %esi,%eax
 1d1:	8d 65 f8             	lea    -0x8(%ebp),%esp
 1d4:	5b                   	pop    %ebx
 1d5:	5e                   	pop    %esi
 1d6:	5d                   	pop    %ebp
 1d7:	c3                   	ret    
    return -1;
 1d8:	be ff ff ff ff       	mov    $0xffffffff,%esi
 1dd:	eb f0                	jmp    1cf <stat+0x34>

000001df <atoi>:

int
atoi(const char *s)
{
 1df:	55                   	push   %ebp
 1e0:	89 e5                	mov    %esp,%ebp
 1e2:	53                   	push   %ebx
 1e3:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 1e6:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 1eb:	eb 10                	jmp    1fd <atoi+0x1e>
    n = n*10 + *s++ - '0';
 1ed:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 1f0:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 1f3:	83 c1 01             	add    $0x1,%ecx
 1f6:	0f be d2             	movsbl %dl,%edx
 1f9:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 1fd:	0f b6 11             	movzbl (%ecx),%edx
 200:	8d 5a d0             	lea    -0x30(%edx),%ebx
 203:	80 fb 09             	cmp    $0x9,%bl
 206:	76 e5                	jbe    1ed <atoi+0xe>
  return n;
}
 208:	5b                   	pop    %ebx
 209:	5d                   	pop    %ebp
 20a:	c3                   	ret    

0000020b <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 20b:	55                   	push   %ebp
 20c:	89 e5                	mov    %esp,%ebp
 20e:	56                   	push   %esi
 20f:	53                   	push   %ebx
 210:	8b 45 08             	mov    0x8(%ebp),%eax
 213:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 216:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 219:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 21b:	eb 0d                	jmp    22a <memmove+0x1f>
    *dst++ = *src++;
 21d:	0f b6 13             	movzbl (%ebx),%edx
 220:	88 11                	mov    %dl,(%ecx)
 222:	8d 5b 01             	lea    0x1(%ebx),%ebx
 225:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 228:	89 f2                	mov    %esi,%edx
 22a:	8d 72 ff             	lea    -0x1(%edx),%esi
 22d:	85 d2                	test   %edx,%edx
 22f:	7f ec                	jg     21d <memmove+0x12>
  return vdst;
}
 231:	5b                   	pop    %ebx
 232:	5e                   	pop    %esi
 233:	5d                   	pop    %ebp
 234:	c3                   	ret    

00000235 <fork>:
 235:	b8 01 00 00 00       	mov    $0x1,%eax
 23a:	cd 40                	int    $0x40
 23c:	c3                   	ret    

0000023d <exit>:
 23d:	b8 02 00 00 00       	mov    $0x2,%eax
 242:	cd 40                	int    $0x40
 244:	c3                   	ret    

00000245 <wait>:
 245:	b8 03 00 00 00       	mov    $0x3,%eax
 24a:	cd 40                	int    $0x40
 24c:	c3                   	ret    

0000024d <pipe>:
 24d:	b8 04 00 00 00       	mov    $0x4,%eax
 252:	cd 40                	int    $0x40
 254:	c3                   	ret    

00000255 <read>:
 255:	b8 05 00 00 00       	mov    $0x5,%eax
 25a:	cd 40                	int    $0x40
 25c:	c3                   	ret    

0000025d <write>:
 25d:	b8 10 00 00 00       	mov    $0x10,%eax
 262:	cd 40                	int    $0x40
 264:	c3                   	ret    

00000265 <close>:
 265:	b8 15 00 00 00       	mov    $0x15,%eax
 26a:	cd 40                	int    $0x40
 26c:	c3                   	ret    

0000026d <kill>:
 26d:	b8 06 00 00 00       	mov    $0x6,%eax
 272:	cd 40                	int    $0x40
 274:	c3                   	ret    

00000275 <exec>:
 275:	b8 07 00 00 00       	mov    $0x7,%eax
 27a:	cd 40                	int    $0x40
 27c:	c3                   	ret    

0000027d <open>:
 27d:	b8 0f 00 00 00       	mov    $0xf,%eax
 282:	cd 40                	int    $0x40
 284:	c3                   	ret    

00000285 <mknod>:
 285:	b8 11 00 00 00       	mov    $0x11,%eax
 28a:	cd 40                	int    $0x40
 28c:	c3                   	ret    

0000028d <unlink>:
 28d:	b8 12 00 00 00       	mov    $0x12,%eax
 292:	cd 40                	int    $0x40
 294:	c3                   	ret    

00000295 <fstat>:
 295:	b8 08 00 00 00       	mov    $0x8,%eax
 29a:	cd 40                	int    $0x40
 29c:	c3                   	ret    

0000029d <link>:
 29d:	b8 13 00 00 00       	mov    $0x13,%eax
 2a2:	cd 40                	int    $0x40
 2a4:	c3                   	ret    

000002a5 <mkdir>:
 2a5:	b8 14 00 00 00       	mov    $0x14,%eax
 2aa:	cd 40                	int    $0x40
 2ac:	c3                   	ret    

000002ad <chdir>:
 2ad:	b8 09 00 00 00       	mov    $0x9,%eax
 2b2:	cd 40                	int    $0x40
 2b4:	c3                   	ret    

000002b5 <dup>:
 2b5:	b8 0a 00 00 00       	mov    $0xa,%eax
 2ba:	cd 40                	int    $0x40
 2bc:	c3                   	ret    

000002bd <getpid>:
 2bd:	b8 0b 00 00 00       	mov    $0xb,%eax
 2c2:	cd 40                	int    $0x40
 2c4:	c3                   	ret    

000002c5 <sbrk>:
 2c5:	b8 0c 00 00 00       	mov    $0xc,%eax
 2ca:	cd 40                	int    $0x40
 2cc:	c3                   	ret    

000002cd <sleep>:
 2cd:	b8 0d 00 00 00       	mov    $0xd,%eax
 2d2:	cd 40                	int    $0x40
 2d4:	c3                   	ret    

000002d5 <uptime>:
 2d5:	b8 0e 00 00 00       	mov    $0xe,%eax
 2da:	cd 40                	int    $0x40
 2dc:	c3                   	ret    

000002dd <yield>:
 2dd:	b8 16 00 00 00       	mov    $0x16,%eax
 2e2:	cd 40                	int    $0x40
 2e4:	c3                   	ret    

000002e5 <shutdown>:
 2e5:	b8 17 00 00 00       	mov    $0x17,%eax
 2ea:	cd 40                	int    $0x40
 2ec:	c3                   	ret    

000002ed <writecount>:
 2ed:	b8 18 00 00 00       	mov    $0x18,%eax
 2f2:	cd 40                	int    $0x40
 2f4:	c3                   	ret    

000002f5 <setwritecount>:
 2f5:	b8 19 00 00 00       	mov    $0x19,%eax
 2fa:	cd 40                	int    $0x40
 2fc:	c3                   	ret    

000002fd <settickets>:
 2fd:	b8 1a 00 00 00       	mov    $0x1a,%eax
 302:	cd 40                	int    $0x40
 304:	c3                   	ret    

00000305 <getprocessesinfo>:
 305:	b8 1b 00 00 00       	mov    $0x1b,%eax
 30a:	cd 40                	int    $0x40
 30c:	c3                   	ret    

0000030d <dumppagetable>:
 30d:	b8 1c 00 00 00       	mov    $0x1c,%eax
 312:	cd 40                	int    $0x40
 314:	c3                   	ret    

00000315 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 315:	55                   	push   %ebp
 316:	89 e5                	mov    %esp,%ebp
 318:	83 ec 1c             	sub    $0x1c,%esp
 31b:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 31e:	6a 01                	push   $0x1
 320:	8d 55 f4             	lea    -0xc(%ebp),%edx
 323:	52                   	push   %edx
 324:	50                   	push   %eax
 325:	e8 33 ff ff ff       	call   25d <write>
}
 32a:	83 c4 10             	add    $0x10,%esp
 32d:	c9                   	leave  
 32e:	c3                   	ret    

0000032f <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 32f:	55                   	push   %ebp
 330:	89 e5                	mov    %esp,%ebp
 332:	57                   	push   %edi
 333:	56                   	push   %esi
 334:	53                   	push   %ebx
 335:	83 ec 2c             	sub    $0x2c,%esp
 338:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 33a:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 33e:	0f 95 c3             	setne  %bl
 341:	89 d0                	mov    %edx,%eax
 343:	c1 e8 1f             	shr    $0x1f,%eax
 346:	84 c3                	test   %al,%bl
 348:	74 10                	je     35a <printint+0x2b>
    neg = 1;
    x = -xx;
 34a:	f7 da                	neg    %edx
    neg = 1;
 34c:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 353:	be 00 00 00 00       	mov    $0x0,%esi
 358:	eb 0b                	jmp    365 <printint+0x36>
  neg = 0;
 35a:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 361:	eb f0                	jmp    353 <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 363:	89 c6                	mov    %eax,%esi
 365:	89 d0                	mov    %edx,%eax
 367:	ba 00 00 00 00       	mov    $0x0,%edx
 36c:	f7 f1                	div    %ecx
 36e:	89 c3                	mov    %eax,%ebx
 370:	8d 46 01             	lea    0x1(%esi),%eax
 373:	0f b6 92 24 07 00 00 	movzbl 0x724(%edx),%edx
 37a:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 37e:	89 da                	mov    %ebx,%edx
 380:	85 db                	test   %ebx,%ebx
 382:	75 df                	jne    363 <printint+0x34>
 384:	89 c3                	mov    %eax,%ebx
  if(neg)
 386:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 38a:	74 16                	je     3a2 <printint+0x73>
    buf[i++] = '-';
 38c:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 391:	8d 5e 02             	lea    0x2(%esi),%ebx
 394:	eb 0c                	jmp    3a2 <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 396:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 39b:	89 f8                	mov    %edi,%eax
 39d:	e8 73 ff ff ff       	call   315 <putc>
  while(--i >= 0)
 3a2:	83 eb 01             	sub    $0x1,%ebx
 3a5:	79 ef                	jns    396 <printint+0x67>
}
 3a7:	83 c4 2c             	add    $0x2c,%esp
 3aa:	5b                   	pop    %ebx
 3ab:	5e                   	pop    %esi
 3ac:	5f                   	pop    %edi
 3ad:	5d                   	pop    %ebp
 3ae:	c3                   	ret    

000003af <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 3af:	55                   	push   %ebp
 3b0:	89 e5                	mov    %esp,%ebp
 3b2:	57                   	push   %edi
 3b3:	56                   	push   %esi
 3b4:	53                   	push   %ebx
 3b5:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 3b8:	8d 45 10             	lea    0x10(%ebp),%eax
 3bb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 3be:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 3c3:	bb 00 00 00 00       	mov    $0x0,%ebx
 3c8:	eb 14                	jmp    3de <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 3ca:	89 fa                	mov    %edi,%edx
 3cc:	8b 45 08             	mov    0x8(%ebp),%eax
 3cf:	e8 41 ff ff ff       	call   315 <putc>
 3d4:	eb 05                	jmp    3db <printf+0x2c>
      }
    } else if(state == '%'){
 3d6:	83 fe 25             	cmp    $0x25,%esi
 3d9:	74 25                	je     400 <printf+0x51>
  for(i = 0; fmt[i]; i++){
 3db:	83 c3 01             	add    $0x1,%ebx
 3de:	8b 45 0c             	mov    0xc(%ebp),%eax
 3e1:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 3e5:	84 c0                	test   %al,%al
 3e7:	0f 84 23 01 00 00    	je     510 <printf+0x161>
    c = fmt[i] & 0xff;
 3ed:	0f be f8             	movsbl %al,%edi
 3f0:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 3f3:	85 f6                	test   %esi,%esi
 3f5:	75 df                	jne    3d6 <printf+0x27>
      if(c == '%'){
 3f7:	83 f8 25             	cmp    $0x25,%eax
 3fa:	75 ce                	jne    3ca <printf+0x1b>
        state = '%';
 3fc:	89 c6                	mov    %eax,%esi
 3fe:	eb db                	jmp    3db <printf+0x2c>
      if(c == 'd'){
 400:	83 f8 64             	cmp    $0x64,%eax
 403:	74 49                	je     44e <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 405:	83 f8 78             	cmp    $0x78,%eax
 408:	0f 94 c1             	sete   %cl
 40b:	83 f8 70             	cmp    $0x70,%eax
 40e:	0f 94 c2             	sete   %dl
 411:	08 d1                	or     %dl,%cl
 413:	75 63                	jne    478 <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 415:	83 f8 73             	cmp    $0x73,%eax
 418:	0f 84 84 00 00 00    	je     4a2 <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 41e:	83 f8 63             	cmp    $0x63,%eax
 421:	0f 84 b7 00 00 00    	je     4de <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 427:	83 f8 25             	cmp    $0x25,%eax
 42a:	0f 84 cc 00 00 00    	je     4fc <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 430:	ba 25 00 00 00       	mov    $0x25,%edx
 435:	8b 45 08             	mov    0x8(%ebp),%eax
 438:	e8 d8 fe ff ff       	call   315 <putc>
        putc(fd, c);
 43d:	89 fa                	mov    %edi,%edx
 43f:	8b 45 08             	mov    0x8(%ebp),%eax
 442:	e8 ce fe ff ff       	call   315 <putc>
      }
      state = 0;
 447:	be 00 00 00 00       	mov    $0x0,%esi
 44c:	eb 8d                	jmp    3db <printf+0x2c>
        printint(fd, *ap, 10, 1);
 44e:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 451:	8b 17                	mov    (%edi),%edx
 453:	83 ec 0c             	sub    $0xc,%esp
 456:	6a 01                	push   $0x1
 458:	b9 0a 00 00 00       	mov    $0xa,%ecx
 45d:	8b 45 08             	mov    0x8(%ebp),%eax
 460:	e8 ca fe ff ff       	call   32f <printint>
        ap++;
 465:	83 c7 04             	add    $0x4,%edi
 468:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 46b:	83 c4 10             	add    $0x10,%esp
      state = 0;
 46e:	be 00 00 00 00       	mov    $0x0,%esi
 473:	e9 63 ff ff ff       	jmp    3db <printf+0x2c>
        printint(fd, *ap, 16, 0);
 478:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 47b:	8b 17                	mov    (%edi),%edx
 47d:	83 ec 0c             	sub    $0xc,%esp
 480:	6a 00                	push   $0x0
 482:	b9 10 00 00 00       	mov    $0x10,%ecx
 487:	8b 45 08             	mov    0x8(%ebp),%eax
 48a:	e8 a0 fe ff ff       	call   32f <printint>
        ap++;
 48f:	83 c7 04             	add    $0x4,%edi
 492:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 495:	83 c4 10             	add    $0x10,%esp
      state = 0;
 498:	be 00 00 00 00       	mov    $0x0,%esi
 49d:	e9 39 ff ff ff       	jmp    3db <printf+0x2c>
        s = (char*)*ap;
 4a2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 4a5:	8b 30                	mov    (%eax),%esi
        ap++;
 4a7:	83 c0 04             	add    $0x4,%eax
 4aa:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 4ad:	85 f6                	test   %esi,%esi
 4af:	75 28                	jne    4d9 <printf+0x12a>
          s = "(null)";
 4b1:	be 1d 07 00 00       	mov    $0x71d,%esi
 4b6:	8b 7d 08             	mov    0x8(%ebp),%edi
 4b9:	eb 0d                	jmp    4c8 <printf+0x119>
          putc(fd, *s);
 4bb:	0f be d2             	movsbl %dl,%edx
 4be:	89 f8                	mov    %edi,%eax
 4c0:	e8 50 fe ff ff       	call   315 <putc>
          s++;
 4c5:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 4c8:	0f b6 16             	movzbl (%esi),%edx
 4cb:	84 d2                	test   %dl,%dl
 4cd:	75 ec                	jne    4bb <printf+0x10c>
      state = 0;
 4cf:	be 00 00 00 00       	mov    $0x0,%esi
 4d4:	e9 02 ff ff ff       	jmp    3db <printf+0x2c>
 4d9:	8b 7d 08             	mov    0x8(%ebp),%edi
 4dc:	eb ea                	jmp    4c8 <printf+0x119>
        putc(fd, *ap);
 4de:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4e1:	0f be 17             	movsbl (%edi),%edx
 4e4:	8b 45 08             	mov    0x8(%ebp),%eax
 4e7:	e8 29 fe ff ff       	call   315 <putc>
        ap++;
 4ec:	83 c7 04             	add    $0x4,%edi
 4ef:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 4f2:	be 00 00 00 00       	mov    $0x0,%esi
 4f7:	e9 df fe ff ff       	jmp    3db <printf+0x2c>
        putc(fd, c);
 4fc:	89 fa                	mov    %edi,%edx
 4fe:	8b 45 08             	mov    0x8(%ebp),%eax
 501:	e8 0f fe ff ff       	call   315 <putc>
      state = 0;
 506:	be 00 00 00 00       	mov    $0x0,%esi
 50b:	e9 cb fe ff ff       	jmp    3db <printf+0x2c>
    }
  }
}
 510:	8d 65 f4             	lea    -0xc(%ebp),%esp
 513:	5b                   	pop    %ebx
 514:	5e                   	pop    %esi
 515:	5f                   	pop    %edi
 516:	5d                   	pop    %ebp
 517:	c3                   	ret    

00000518 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 518:	55                   	push   %ebp
 519:	89 e5                	mov    %esp,%ebp
 51b:	57                   	push   %edi
 51c:	56                   	push   %esi
 51d:	53                   	push   %ebx
 51e:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 521:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 524:	a1 c0 09 00 00       	mov    0x9c0,%eax
 529:	eb 02                	jmp    52d <free+0x15>
 52b:	89 d0                	mov    %edx,%eax
 52d:	39 c8                	cmp    %ecx,%eax
 52f:	73 04                	jae    535 <free+0x1d>
 531:	39 08                	cmp    %ecx,(%eax)
 533:	77 12                	ja     547 <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 535:	8b 10                	mov    (%eax),%edx
 537:	39 c2                	cmp    %eax,%edx
 539:	77 f0                	ja     52b <free+0x13>
 53b:	39 c8                	cmp    %ecx,%eax
 53d:	72 08                	jb     547 <free+0x2f>
 53f:	39 ca                	cmp    %ecx,%edx
 541:	77 04                	ja     547 <free+0x2f>
 543:	89 d0                	mov    %edx,%eax
 545:	eb e6                	jmp    52d <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 547:	8b 73 fc             	mov    -0x4(%ebx),%esi
 54a:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 54d:	8b 10                	mov    (%eax),%edx
 54f:	39 d7                	cmp    %edx,%edi
 551:	74 19                	je     56c <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 553:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 556:	8b 50 04             	mov    0x4(%eax),%edx
 559:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 55c:	39 ce                	cmp    %ecx,%esi
 55e:	74 1b                	je     57b <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 560:	89 08                	mov    %ecx,(%eax)
  freep = p;
 562:	a3 c0 09 00 00       	mov    %eax,0x9c0
}
 567:	5b                   	pop    %ebx
 568:	5e                   	pop    %esi
 569:	5f                   	pop    %edi
 56a:	5d                   	pop    %ebp
 56b:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 56c:	03 72 04             	add    0x4(%edx),%esi
 56f:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 572:	8b 10                	mov    (%eax),%edx
 574:	8b 12                	mov    (%edx),%edx
 576:	89 53 f8             	mov    %edx,-0x8(%ebx)
 579:	eb db                	jmp    556 <free+0x3e>
    p->s.size += bp->s.size;
 57b:	03 53 fc             	add    -0x4(%ebx),%edx
 57e:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 581:	8b 53 f8             	mov    -0x8(%ebx),%edx
 584:	89 10                	mov    %edx,(%eax)
 586:	eb da                	jmp    562 <free+0x4a>

00000588 <morecore>:

static Header*
morecore(uint nu)
{
 588:	55                   	push   %ebp
 589:	89 e5                	mov    %esp,%ebp
 58b:	53                   	push   %ebx
 58c:	83 ec 04             	sub    $0x4,%esp
 58f:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 591:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 596:	77 05                	ja     59d <morecore+0x15>
    nu = 4096;
 598:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 59d:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 5a4:	83 ec 0c             	sub    $0xc,%esp
 5a7:	50                   	push   %eax
 5a8:	e8 18 fd ff ff       	call   2c5 <sbrk>
  if(p == (char*)-1)
 5ad:	83 c4 10             	add    $0x10,%esp
 5b0:	83 f8 ff             	cmp    $0xffffffff,%eax
 5b3:	74 1c                	je     5d1 <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 5b5:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 5b8:	83 c0 08             	add    $0x8,%eax
 5bb:	83 ec 0c             	sub    $0xc,%esp
 5be:	50                   	push   %eax
 5bf:	e8 54 ff ff ff       	call   518 <free>
  return freep;
 5c4:	a1 c0 09 00 00       	mov    0x9c0,%eax
 5c9:	83 c4 10             	add    $0x10,%esp
}
 5cc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 5cf:	c9                   	leave  
 5d0:	c3                   	ret    
    return 0;
 5d1:	b8 00 00 00 00       	mov    $0x0,%eax
 5d6:	eb f4                	jmp    5cc <morecore+0x44>

000005d8 <malloc>:

void*
malloc(uint nbytes)
{
 5d8:	55                   	push   %ebp
 5d9:	89 e5                	mov    %esp,%ebp
 5db:	53                   	push   %ebx
 5dc:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 5df:	8b 45 08             	mov    0x8(%ebp),%eax
 5e2:	8d 58 07             	lea    0x7(%eax),%ebx
 5e5:	c1 eb 03             	shr    $0x3,%ebx
 5e8:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 5eb:	8b 0d c0 09 00 00    	mov    0x9c0,%ecx
 5f1:	85 c9                	test   %ecx,%ecx
 5f3:	74 04                	je     5f9 <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 5f5:	8b 01                	mov    (%ecx),%eax
 5f7:	eb 4d                	jmp    646 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 5f9:	c7 05 c0 09 00 00 c4 	movl   $0x9c4,0x9c0
 600:	09 00 00 
 603:	c7 05 c4 09 00 00 c4 	movl   $0x9c4,0x9c4
 60a:	09 00 00 
    base.s.size = 0;
 60d:	c7 05 c8 09 00 00 00 	movl   $0x0,0x9c8
 614:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 617:	b9 c4 09 00 00       	mov    $0x9c4,%ecx
 61c:	eb d7                	jmp    5f5 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 61e:	39 da                	cmp    %ebx,%edx
 620:	74 1a                	je     63c <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 622:	29 da                	sub    %ebx,%edx
 624:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 627:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 62a:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 62d:	89 0d c0 09 00 00    	mov    %ecx,0x9c0
      return (void*)(p + 1);
 633:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 636:	83 c4 04             	add    $0x4,%esp
 639:	5b                   	pop    %ebx
 63a:	5d                   	pop    %ebp
 63b:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 63c:	8b 10                	mov    (%eax),%edx
 63e:	89 11                	mov    %edx,(%ecx)
 640:	eb eb                	jmp    62d <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 642:	89 c1                	mov    %eax,%ecx
 644:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 646:	8b 50 04             	mov    0x4(%eax),%edx
 649:	39 da                	cmp    %ebx,%edx
 64b:	73 d1                	jae    61e <malloc+0x46>
    if(p == freep)
 64d:	39 05 c0 09 00 00    	cmp    %eax,0x9c0
 653:	75 ed                	jne    642 <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 655:	89 d8                	mov    %ebx,%eax
 657:	e8 2c ff ff ff       	call   588 <morecore>
 65c:	85 c0                	test   %eax,%eax
 65e:	75 e2                	jne    642 <malloc+0x6a>
        return 0;
 660:	b8 00 00 00 00       	mov    $0x0,%eax
 665:	eb cf                	jmp    636 <malloc+0x5e>
