
kernel:     file format elf32-i386


Disassembly of section .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 90 10 00       	mov    $0x109000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc d0 b5 10 80       	mov    $0x8010b5d0,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 a1 2a 10 80       	mov    $0x80102aa1,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax

80100034 <bget>:
// Look through buffer cache for block on device dev.
// If not found, allocate a buffer.
// In either case, return locked buffer.
static struct buf*
bget(uint dev, uint blockno)
{
80100034:	55                   	push   %ebp
80100035:	89 e5                	mov    %esp,%ebp
80100037:	57                   	push   %edi
80100038:	56                   	push   %esi
80100039:	53                   	push   %ebx
8010003a:	83 ec 18             	sub    $0x18,%esp
8010003d:	89 c6                	mov    %eax,%esi
8010003f:	89 d7                	mov    %edx,%edi
  struct buf *b;

  acquire(&bcache.lock);
80100041:	68 e0 b5 10 80       	push   $0x8010b5e0
80100046:	e8 d0 3e 00 00       	call   80103f1b <acquire>

  // Is the block already cached?
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
8010004b:	8b 1d 30 fd 10 80    	mov    0x8010fd30,%ebx
80100051:	83 c4 10             	add    $0x10,%esp
80100054:	eb 03                	jmp    80100059 <bget+0x25>
80100056:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100059:	81 fb dc fc 10 80    	cmp    $0x8010fcdc,%ebx
8010005f:	74 30                	je     80100091 <bget+0x5d>
    if(b->dev == dev && b->blockno == blockno){
80100061:	39 73 04             	cmp    %esi,0x4(%ebx)
80100064:	75 f0                	jne    80100056 <bget+0x22>
80100066:	39 7b 08             	cmp    %edi,0x8(%ebx)
80100069:	75 eb                	jne    80100056 <bget+0x22>
      b->refcnt++;
8010006b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010006e:	83 c0 01             	add    $0x1,%eax
80100071:	89 43 4c             	mov    %eax,0x4c(%ebx)
      release(&bcache.lock);
80100074:	83 ec 0c             	sub    $0xc,%esp
80100077:	68 e0 b5 10 80       	push   $0x8010b5e0
8010007c:	e8 ff 3e 00 00       	call   80103f80 <release>
      acquiresleep(&b->lock);
80100081:	8d 43 0c             	lea    0xc(%ebx),%eax
80100084:	89 04 24             	mov    %eax,(%esp)
80100087:	e8 7b 3c 00 00       	call   80103d07 <acquiresleep>
      return b;
8010008c:	83 c4 10             	add    $0x10,%esp
8010008f:	eb 4c                	jmp    801000dd <bget+0xa9>
  }

  // Not cached; recycle an unused buffer.
  // Even if refcnt==0, B_DIRTY indicates a buffer is in use
  // because log.c has modified it but not yet committed it.
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100091:	8b 1d 2c fd 10 80    	mov    0x8010fd2c,%ebx
80100097:	eb 03                	jmp    8010009c <bget+0x68>
80100099:	8b 5b 50             	mov    0x50(%ebx),%ebx
8010009c:	81 fb dc fc 10 80    	cmp    $0x8010fcdc,%ebx
801000a2:	74 43                	je     801000e7 <bget+0xb3>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
801000a4:	83 7b 4c 00          	cmpl   $0x0,0x4c(%ebx)
801000a8:	75 ef                	jne    80100099 <bget+0x65>
801000aa:	f6 03 04             	testb  $0x4,(%ebx)
801000ad:	75 ea                	jne    80100099 <bget+0x65>
      b->dev = dev;
801000af:	89 73 04             	mov    %esi,0x4(%ebx)
      b->blockno = blockno;
801000b2:	89 7b 08             	mov    %edi,0x8(%ebx)
      b->flags = 0;
801000b5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
801000bb:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
801000c2:	83 ec 0c             	sub    $0xc,%esp
801000c5:	68 e0 b5 10 80       	push   $0x8010b5e0
801000ca:	e8 b1 3e 00 00       	call   80103f80 <release>
      acquiresleep(&b->lock);
801000cf:	8d 43 0c             	lea    0xc(%ebx),%eax
801000d2:	89 04 24             	mov    %eax,(%esp)
801000d5:	e8 2d 3c 00 00       	call   80103d07 <acquiresleep>
      return b;
801000da:	83 c4 10             	add    $0x10,%esp
    }
  }
  panic("bget: no buffers");
}
801000dd:	89 d8                	mov    %ebx,%eax
801000df:	8d 65 f4             	lea    -0xc(%ebp),%esp
801000e2:	5b                   	pop    %ebx
801000e3:	5e                   	pop    %esi
801000e4:	5f                   	pop    %edi
801000e5:	5d                   	pop    %ebp
801000e6:	c3                   	ret    
  panic("bget: no buffers");
801000e7:	83 ec 0c             	sub    $0xc,%esp
801000ea:	68 c0 6c 10 80       	push   $0x80106cc0
801000ef:	e8 54 02 00 00       	call   80100348 <panic>

801000f4 <binit>:
{
801000f4:	55                   	push   %ebp
801000f5:	89 e5                	mov    %esp,%ebp
801000f7:	53                   	push   %ebx
801000f8:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
801000fb:	68 d1 6c 10 80       	push   $0x80106cd1
80100100:	68 e0 b5 10 80       	push   $0x8010b5e0
80100105:	e8 d5 3c 00 00       	call   80103ddf <initlock>
  bcache.head.prev = &bcache.head;
8010010a:	c7 05 2c fd 10 80 dc 	movl   $0x8010fcdc,0x8010fd2c
80100111:	fc 10 80 
  bcache.head.next = &bcache.head;
80100114:	c7 05 30 fd 10 80 dc 	movl   $0x8010fcdc,0x8010fd30
8010011b:	fc 10 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
8010011e:	83 c4 10             	add    $0x10,%esp
80100121:	bb 14 b6 10 80       	mov    $0x8010b614,%ebx
80100126:	eb 37                	jmp    8010015f <binit+0x6b>
    b->next = bcache.head.next;
80100128:	a1 30 fd 10 80       	mov    0x8010fd30,%eax
8010012d:	89 43 54             	mov    %eax,0x54(%ebx)
    b->prev = &bcache.head;
80100130:	c7 43 50 dc fc 10 80 	movl   $0x8010fcdc,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100137:	83 ec 08             	sub    $0x8,%esp
8010013a:	68 d8 6c 10 80       	push   $0x80106cd8
8010013f:	8d 43 0c             	lea    0xc(%ebx),%eax
80100142:	50                   	push   %eax
80100143:	e8 8c 3b 00 00       	call   80103cd4 <initsleeplock>
    bcache.head.next->prev = b;
80100148:	a1 30 fd 10 80       	mov    0x8010fd30,%eax
8010014d:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
80100150:	89 1d 30 fd 10 80    	mov    %ebx,0x8010fd30
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100156:	81 c3 5c 02 00 00    	add    $0x25c,%ebx
8010015c:	83 c4 10             	add    $0x10,%esp
8010015f:	81 fb dc fc 10 80    	cmp    $0x8010fcdc,%ebx
80100165:	72 c1                	jb     80100128 <binit+0x34>
}
80100167:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010016a:	c9                   	leave  
8010016b:	c3                   	ret    

8010016c <bread>:

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
8010016c:	55                   	push   %ebp
8010016d:	89 e5                	mov    %esp,%ebp
8010016f:	53                   	push   %ebx
80100170:	83 ec 04             	sub    $0x4,%esp
  struct buf *b;

  b = bget(dev, blockno);
80100173:	8b 55 0c             	mov    0xc(%ebp),%edx
80100176:	8b 45 08             	mov    0x8(%ebp),%eax
80100179:	e8 b6 fe ff ff       	call   80100034 <bget>
8010017e:	89 c3                	mov    %eax,%ebx
  if((b->flags & B_VALID) == 0) {
80100180:	f6 00 02             	testb  $0x2,(%eax)
80100183:	74 07                	je     8010018c <bread+0x20>
    iderw(b);
  }
  return b;
}
80100185:	89 d8                	mov    %ebx,%eax
80100187:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010018a:	c9                   	leave  
8010018b:	c3                   	ret    
    iderw(b);
8010018c:	83 ec 0c             	sub    $0xc,%esp
8010018f:	50                   	push   %eax
80100190:	e8 65 1c 00 00       	call   80101dfa <iderw>
80100195:	83 c4 10             	add    $0x10,%esp
  return b;
80100198:	eb eb                	jmp    80100185 <bread+0x19>

8010019a <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
8010019a:	55                   	push   %ebp
8010019b:	89 e5                	mov    %esp,%ebp
8010019d:	53                   	push   %ebx
8010019e:	83 ec 10             	sub    $0x10,%esp
801001a1:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001a4:	8d 43 0c             	lea    0xc(%ebx),%eax
801001a7:	50                   	push   %eax
801001a8:	e8 e4 3b 00 00       	call   80103d91 <holdingsleep>
801001ad:	83 c4 10             	add    $0x10,%esp
801001b0:	85 c0                	test   %eax,%eax
801001b2:	74 14                	je     801001c8 <bwrite+0x2e>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001b4:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001b7:	83 ec 0c             	sub    $0xc,%esp
801001ba:	53                   	push   %ebx
801001bb:	e8 3a 1c 00 00       	call   80101dfa <iderw>
}
801001c0:	83 c4 10             	add    $0x10,%esp
801001c3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001c6:	c9                   	leave  
801001c7:	c3                   	ret    
    panic("bwrite");
801001c8:	83 ec 0c             	sub    $0xc,%esp
801001cb:	68 df 6c 10 80       	push   $0x80106cdf
801001d0:	e8 73 01 00 00       	call   80100348 <panic>

801001d5 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001d5:	55                   	push   %ebp
801001d6:	89 e5                	mov    %esp,%ebp
801001d8:	56                   	push   %esi
801001d9:	53                   	push   %ebx
801001da:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001dd:	8d 73 0c             	lea    0xc(%ebx),%esi
801001e0:	83 ec 0c             	sub    $0xc,%esp
801001e3:	56                   	push   %esi
801001e4:	e8 a8 3b 00 00       	call   80103d91 <holdingsleep>
801001e9:	83 c4 10             	add    $0x10,%esp
801001ec:	85 c0                	test   %eax,%eax
801001ee:	74 6b                	je     8010025b <brelse+0x86>
    panic("brelse");

  releasesleep(&b->lock);
801001f0:	83 ec 0c             	sub    $0xc,%esp
801001f3:	56                   	push   %esi
801001f4:	e8 5d 3b 00 00       	call   80103d56 <releasesleep>

  acquire(&bcache.lock);
801001f9:	c7 04 24 e0 b5 10 80 	movl   $0x8010b5e0,(%esp)
80100200:	e8 16 3d 00 00       	call   80103f1b <acquire>
  b->refcnt--;
80100205:	8b 43 4c             	mov    0x4c(%ebx),%eax
80100208:	83 e8 01             	sub    $0x1,%eax
8010020b:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
8010020e:	83 c4 10             	add    $0x10,%esp
80100211:	85 c0                	test   %eax,%eax
80100213:	75 2f                	jne    80100244 <brelse+0x6f>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100215:	8b 43 54             	mov    0x54(%ebx),%eax
80100218:	8b 53 50             	mov    0x50(%ebx),%edx
8010021b:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
8010021e:	8b 43 50             	mov    0x50(%ebx),%eax
80100221:	8b 53 54             	mov    0x54(%ebx),%edx
80100224:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100227:	a1 30 fd 10 80       	mov    0x8010fd30,%eax
8010022c:	89 43 54             	mov    %eax,0x54(%ebx)
    b->prev = &bcache.head;
8010022f:	c7 43 50 dc fc 10 80 	movl   $0x8010fcdc,0x50(%ebx)
    bcache.head.next->prev = b;
80100236:	a1 30 fd 10 80       	mov    0x8010fd30,%eax
8010023b:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
8010023e:	89 1d 30 fd 10 80    	mov    %ebx,0x8010fd30
  }
  
  release(&bcache.lock);
80100244:	83 ec 0c             	sub    $0xc,%esp
80100247:	68 e0 b5 10 80       	push   $0x8010b5e0
8010024c:	e8 2f 3d 00 00       	call   80103f80 <release>
}
80100251:	83 c4 10             	add    $0x10,%esp
80100254:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100257:	5b                   	pop    %ebx
80100258:	5e                   	pop    %esi
80100259:	5d                   	pop    %ebp
8010025a:	c3                   	ret    
    panic("brelse");
8010025b:	83 ec 0c             	sub    $0xc,%esp
8010025e:	68 e6 6c 10 80       	push   $0x80106ce6
80100263:	e8 e0 00 00 00       	call   80100348 <panic>

80100268 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100268:	55                   	push   %ebp
80100269:	89 e5                	mov    %esp,%ebp
8010026b:	57                   	push   %edi
8010026c:	56                   	push   %esi
8010026d:	53                   	push   %ebx
8010026e:	83 ec 28             	sub    $0x28,%esp
80100271:	8b 7d 08             	mov    0x8(%ebp),%edi
80100274:	8b 75 0c             	mov    0xc(%ebp),%esi
80100277:	8b 5d 10             	mov    0x10(%ebp),%ebx
  uint target;
  int c;

  iunlock(ip);
8010027a:	57                   	push   %edi
8010027b:	e8 b1 13 00 00       	call   80101631 <iunlock>
  target = n;
80100280:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
  acquire(&cons.lock);
80100283:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
8010028a:	e8 8c 3c 00 00       	call   80103f1b <acquire>
  while(n > 0){
8010028f:	83 c4 10             	add    $0x10,%esp
80100292:	85 db                	test   %ebx,%ebx
80100294:	0f 8e 8f 00 00 00    	jle    80100329 <consoleread+0xc1>
    while(input.r == input.w){
8010029a:	a1 c0 ff 10 80       	mov    0x8010ffc0,%eax
8010029f:	3b 05 c4 ff 10 80    	cmp    0x8010ffc4,%eax
801002a5:	75 47                	jne    801002ee <consoleread+0x86>
      if(myproc()->killed){
801002a7:	e8 ab 2f 00 00       	call   80103257 <myproc>
801002ac:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
801002b0:	75 17                	jne    801002c9 <consoleread+0x61>
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input.r, &cons.lock);
801002b2:	83 ec 08             	sub    $0x8,%esp
801002b5:	68 20 a5 10 80       	push   $0x8010a520
801002ba:	68 c0 ff 10 80       	push   $0x8010ffc0
801002bf:	e8 cf 33 00 00       	call   80103693 <sleep>
801002c4:	83 c4 10             	add    $0x10,%esp
801002c7:	eb d1                	jmp    8010029a <consoleread+0x32>
        release(&cons.lock);
801002c9:	83 ec 0c             	sub    $0xc,%esp
801002cc:	68 20 a5 10 80       	push   $0x8010a520
801002d1:	e8 aa 3c 00 00       	call   80103f80 <release>
        ilock(ip);
801002d6:	89 3c 24             	mov    %edi,(%esp)
801002d9:	e8 91 12 00 00       	call   8010156f <ilock>
        return -1;
801002de:	83 c4 10             	add    $0x10,%esp
801002e1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
801002e6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801002e9:	5b                   	pop    %ebx
801002ea:	5e                   	pop    %esi
801002eb:	5f                   	pop    %edi
801002ec:	5d                   	pop    %ebp
801002ed:	c3                   	ret    
    c = input.buf[input.r++ % INPUT_BUF];
801002ee:	8d 50 01             	lea    0x1(%eax),%edx
801002f1:	89 15 c0 ff 10 80    	mov    %edx,0x8010ffc0
801002f7:	89 c2                	mov    %eax,%edx
801002f9:	83 e2 7f             	and    $0x7f,%edx
801002fc:	0f b6 8a 40 ff 10 80 	movzbl -0x7fef00c0(%edx),%ecx
80100303:	0f be d1             	movsbl %cl,%edx
    if(c == C('D')){  // EOF
80100306:	83 fa 04             	cmp    $0x4,%edx
80100309:	74 14                	je     8010031f <consoleread+0xb7>
    *dst++ = c;
8010030b:	8d 46 01             	lea    0x1(%esi),%eax
8010030e:	88 0e                	mov    %cl,(%esi)
    --n;
80100310:	83 eb 01             	sub    $0x1,%ebx
    if(c == '\n')
80100313:	83 fa 0a             	cmp    $0xa,%edx
80100316:	74 11                	je     80100329 <consoleread+0xc1>
    *dst++ = c;
80100318:	89 c6                	mov    %eax,%esi
8010031a:	e9 73 ff ff ff       	jmp    80100292 <consoleread+0x2a>
      if(n < target){
8010031f:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
80100322:	73 05                	jae    80100329 <consoleread+0xc1>
        input.r--;
80100324:	a3 c0 ff 10 80       	mov    %eax,0x8010ffc0
  release(&cons.lock);
80100329:	83 ec 0c             	sub    $0xc,%esp
8010032c:	68 20 a5 10 80       	push   $0x8010a520
80100331:	e8 4a 3c 00 00       	call   80103f80 <release>
  ilock(ip);
80100336:	89 3c 24             	mov    %edi,(%esp)
80100339:	e8 31 12 00 00       	call   8010156f <ilock>
  return target - n;
8010033e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100341:	29 d8                	sub    %ebx,%eax
80100343:	83 c4 10             	add    $0x10,%esp
80100346:	eb 9e                	jmp    801002e6 <consoleread+0x7e>

80100348 <panic>:
{
80100348:	55                   	push   %ebp
80100349:	89 e5                	mov    %esp,%ebp
8010034b:	53                   	push   %ebx
8010034c:	83 ec 34             	sub    $0x34,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
8010034f:	fa                   	cli    
  cons.locking = 0;
80100350:	c7 05 54 a5 10 80 00 	movl   $0x0,0x8010a554
80100357:	00 00 00 
  cprintf("lapicid %d: panic: ", lapicid());
8010035a:	e8 43 20 00 00       	call   801023a2 <lapicid>
8010035f:	83 ec 08             	sub    $0x8,%esp
80100362:	50                   	push   %eax
80100363:	68 ed 6c 10 80       	push   $0x80106ced
80100368:	e8 9e 02 00 00       	call   8010060b <cprintf>
  cprintf(s);
8010036d:	83 c4 04             	add    $0x4,%esp
80100370:	ff 75 08             	pushl  0x8(%ebp)
80100373:	e8 93 02 00 00       	call   8010060b <cprintf>
  cprintf("\n");
80100378:	c7 04 24 91 75 10 80 	movl   $0x80107591,(%esp)
8010037f:	e8 87 02 00 00       	call   8010060b <cprintf>
  getcallerpcs(&s, pcs);
80100384:	83 c4 08             	add    $0x8,%esp
80100387:	8d 45 d0             	lea    -0x30(%ebp),%eax
8010038a:	50                   	push   %eax
8010038b:	8d 45 08             	lea    0x8(%ebp),%eax
8010038e:	50                   	push   %eax
8010038f:	e8 66 3a 00 00       	call   80103dfa <getcallerpcs>
  for(i=0; i<10; i++)
80100394:	83 c4 10             	add    $0x10,%esp
80100397:	bb 00 00 00 00       	mov    $0x0,%ebx
8010039c:	eb 17                	jmp    801003b5 <panic+0x6d>
    cprintf(" %p", pcs[i]);
8010039e:	83 ec 08             	sub    $0x8,%esp
801003a1:	ff 74 9d d0          	pushl  -0x30(%ebp,%ebx,4)
801003a5:	68 01 6d 10 80       	push   $0x80106d01
801003aa:	e8 5c 02 00 00       	call   8010060b <cprintf>
  for(i=0; i<10; i++)
801003af:	83 c3 01             	add    $0x1,%ebx
801003b2:	83 c4 10             	add    $0x10,%esp
801003b5:	83 fb 09             	cmp    $0x9,%ebx
801003b8:	7e e4                	jle    8010039e <panic+0x56>
  panicked = 1; // freeze other CPU
801003ba:	c7 05 58 a5 10 80 01 	movl   $0x1,0x8010a558
801003c1:	00 00 00 
801003c4:	eb fe                	jmp    801003c4 <panic+0x7c>

801003c6 <cgaputc>:
{
801003c6:	55                   	push   %ebp
801003c7:	89 e5                	mov    %esp,%ebp
801003c9:	57                   	push   %edi
801003ca:	56                   	push   %esi
801003cb:	53                   	push   %ebx
801003cc:	83 ec 0c             	sub    $0xc,%esp
801003cf:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801003d1:	b9 d4 03 00 00       	mov    $0x3d4,%ecx
801003d6:	b8 0e 00 00 00       	mov    $0xe,%eax
801003db:	89 ca                	mov    %ecx,%edx
801003dd:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801003de:	bb d5 03 00 00       	mov    $0x3d5,%ebx
801003e3:	89 da                	mov    %ebx,%edx
801003e5:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
801003e6:	0f b6 f8             	movzbl %al,%edi
801003e9:	c1 e7 08             	shl    $0x8,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801003ec:	b8 0f 00 00 00       	mov    $0xf,%eax
801003f1:	89 ca                	mov    %ecx,%edx
801003f3:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801003f4:	89 da                	mov    %ebx,%edx
801003f6:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
801003f7:	0f b6 c8             	movzbl %al,%ecx
801003fa:	09 f9                	or     %edi,%ecx
  if(c == '\n')
801003fc:	83 fe 0a             	cmp    $0xa,%esi
801003ff:	74 6a                	je     8010046b <cgaputc+0xa5>
  else if(c == BACKSPACE){
80100401:	81 fe 00 01 00 00    	cmp    $0x100,%esi
80100407:	0f 84 81 00 00 00    	je     8010048e <cgaputc+0xc8>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
8010040d:	89 f0                	mov    %esi,%eax
8010040f:	0f b6 f0             	movzbl %al,%esi
80100412:	8d 59 01             	lea    0x1(%ecx),%ebx
80100415:	66 81 ce 00 07       	or     $0x700,%si
8010041a:	66 89 b4 09 00 80 0b 	mov    %si,-0x7ff48000(%ecx,%ecx,1)
80100421:	80 
  if(pos < 0 || pos > 25*80)
80100422:	81 fb d0 07 00 00    	cmp    $0x7d0,%ebx
80100428:	77 71                	ja     8010049b <cgaputc+0xd5>
  if((pos/80) >= 24){  // Scroll up.
8010042a:	81 fb 7f 07 00 00    	cmp    $0x77f,%ebx
80100430:	7f 76                	jg     801004a8 <cgaputc+0xe2>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100432:	be d4 03 00 00       	mov    $0x3d4,%esi
80100437:	b8 0e 00 00 00       	mov    $0xe,%eax
8010043c:	89 f2                	mov    %esi,%edx
8010043e:	ee                   	out    %al,(%dx)
  outb(CRTPORT+1, pos>>8);
8010043f:	89 d8                	mov    %ebx,%eax
80100441:	c1 f8 08             	sar    $0x8,%eax
80100444:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100449:	89 ca                	mov    %ecx,%edx
8010044b:	ee                   	out    %al,(%dx)
8010044c:	b8 0f 00 00 00       	mov    $0xf,%eax
80100451:	89 f2                	mov    %esi,%edx
80100453:	ee                   	out    %al,(%dx)
80100454:	89 d8                	mov    %ebx,%eax
80100456:	89 ca                	mov    %ecx,%edx
80100458:	ee                   	out    %al,(%dx)
  crt[pos] = ' ' | 0x0700;
80100459:	66 c7 84 1b 00 80 0b 	movw   $0x720,-0x7ff48000(%ebx,%ebx,1)
80100460:	80 20 07 
}
80100463:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100466:	5b                   	pop    %ebx
80100467:	5e                   	pop    %esi
80100468:	5f                   	pop    %edi
80100469:	5d                   	pop    %ebp
8010046a:	c3                   	ret    
    pos += 80 - pos%80;
8010046b:	ba 67 66 66 66       	mov    $0x66666667,%edx
80100470:	89 c8                	mov    %ecx,%eax
80100472:	f7 ea                	imul   %edx
80100474:	c1 fa 05             	sar    $0x5,%edx
80100477:	8d 14 92             	lea    (%edx,%edx,4),%edx
8010047a:	89 d0                	mov    %edx,%eax
8010047c:	c1 e0 04             	shl    $0x4,%eax
8010047f:	89 ca                	mov    %ecx,%edx
80100481:	29 c2                	sub    %eax,%edx
80100483:	bb 50 00 00 00       	mov    $0x50,%ebx
80100488:	29 d3                	sub    %edx,%ebx
8010048a:	01 cb                	add    %ecx,%ebx
8010048c:	eb 94                	jmp    80100422 <cgaputc+0x5c>
    if(pos > 0) --pos;
8010048e:	85 c9                	test   %ecx,%ecx
80100490:	7e 05                	jle    80100497 <cgaputc+0xd1>
80100492:	8d 59 ff             	lea    -0x1(%ecx),%ebx
80100495:	eb 8b                	jmp    80100422 <cgaputc+0x5c>
  pos |= inb(CRTPORT+1);
80100497:	89 cb                	mov    %ecx,%ebx
80100499:	eb 87                	jmp    80100422 <cgaputc+0x5c>
    panic("pos under/overflow");
8010049b:	83 ec 0c             	sub    $0xc,%esp
8010049e:	68 05 6d 10 80       	push   $0x80106d05
801004a3:	e8 a0 fe ff ff       	call   80100348 <panic>
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
801004a8:	83 ec 04             	sub    $0x4,%esp
801004ab:	68 60 0e 00 00       	push   $0xe60
801004b0:	68 a0 80 0b 80       	push   $0x800b80a0
801004b5:	68 00 80 0b 80       	push   $0x800b8000
801004ba:	e8 83 3b 00 00       	call   80104042 <memmove>
    pos -= 80;
801004bf:	83 eb 50             	sub    $0x50,%ebx
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
801004c2:	b8 80 07 00 00       	mov    $0x780,%eax
801004c7:	29 d8                	sub    %ebx,%eax
801004c9:	8d 94 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%edx
801004d0:	83 c4 0c             	add    $0xc,%esp
801004d3:	01 c0                	add    %eax,%eax
801004d5:	50                   	push   %eax
801004d6:	6a 00                	push   $0x0
801004d8:	52                   	push   %edx
801004d9:	e8 e9 3a 00 00       	call   80103fc7 <memset>
801004de:	83 c4 10             	add    $0x10,%esp
801004e1:	e9 4c ff ff ff       	jmp    80100432 <cgaputc+0x6c>

801004e6 <consputc>:
  if(panicked){
801004e6:	83 3d 58 a5 10 80 00 	cmpl   $0x0,0x8010a558
801004ed:	74 03                	je     801004f2 <consputc+0xc>
  asm volatile("cli");
801004ef:	fa                   	cli    
801004f0:	eb fe                	jmp    801004f0 <consputc+0xa>
{
801004f2:	55                   	push   %ebp
801004f3:	89 e5                	mov    %esp,%ebp
801004f5:	53                   	push   %ebx
801004f6:	83 ec 04             	sub    $0x4,%esp
801004f9:	89 c3                	mov    %eax,%ebx
  if(c == BACKSPACE){
801004fb:	3d 00 01 00 00       	cmp    $0x100,%eax
80100500:	74 18                	je     8010051a <consputc+0x34>
    uartputc(c);
80100502:	83 ec 0c             	sub    $0xc,%esp
80100505:	50                   	push   %eax
80100506:	e8 03 50 00 00       	call   8010550e <uartputc>
8010050b:	83 c4 10             	add    $0x10,%esp
  cgaputc(c);
8010050e:	89 d8                	mov    %ebx,%eax
80100510:	e8 b1 fe ff ff       	call   801003c6 <cgaputc>
}
80100515:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100518:	c9                   	leave  
80100519:	c3                   	ret    
    uartputc('\b'); uartputc(' '); uartputc('\b');
8010051a:	83 ec 0c             	sub    $0xc,%esp
8010051d:	6a 08                	push   $0x8
8010051f:	e8 ea 4f 00 00       	call   8010550e <uartputc>
80100524:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
8010052b:	e8 de 4f 00 00       	call   8010550e <uartputc>
80100530:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
80100537:	e8 d2 4f 00 00       	call   8010550e <uartputc>
8010053c:	83 c4 10             	add    $0x10,%esp
8010053f:	eb cd                	jmp    8010050e <consputc+0x28>

80100541 <printint>:
{
80100541:	55                   	push   %ebp
80100542:	89 e5                	mov    %esp,%ebp
80100544:	57                   	push   %edi
80100545:	56                   	push   %esi
80100546:	53                   	push   %ebx
80100547:	83 ec 1c             	sub    $0x1c,%esp
8010054a:	89 d7                	mov    %edx,%edi
  if(sign && (sign = xx < 0))
8010054c:	85 c9                	test   %ecx,%ecx
8010054e:	74 09                	je     80100559 <printint+0x18>
80100550:	89 c1                	mov    %eax,%ecx
80100552:	c1 e9 1f             	shr    $0x1f,%ecx
80100555:	85 c0                	test   %eax,%eax
80100557:	78 09                	js     80100562 <printint+0x21>
    x = xx;
80100559:	89 c2                	mov    %eax,%edx
  i = 0;
8010055b:	be 00 00 00 00       	mov    $0x0,%esi
80100560:	eb 08                	jmp    8010056a <printint+0x29>
    x = -xx;
80100562:	f7 d8                	neg    %eax
80100564:	89 c2                	mov    %eax,%edx
80100566:	eb f3                	jmp    8010055b <printint+0x1a>
    buf[i++] = digits[x % base];
80100568:	89 de                	mov    %ebx,%esi
8010056a:	89 d0                	mov    %edx,%eax
8010056c:	ba 00 00 00 00       	mov    $0x0,%edx
80100571:	f7 f7                	div    %edi
80100573:	8d 5e 01             	lea    0x1(%esi),%ebx
80100576:	0f b6 92 30 6d 10 80 	movzbl -0x7fef92d0(%edx),%edx
8010057d:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
80100581:	89 c2                	mov    %eax,%edx
80100583:	85 c0                	test   %eax,%eax
80100585:	75 e1                	jne    80100568 <printint+0x27>
  if(sign)
80100587:	85 c9                	test   %ecx,%ecx
80100589:	74 14                	je     8010059f <printint+0x5e>
    buf[i++] = '-';
8010058b:	c6 44 1d d8 2d       	movb   $0x2d,-0x28(%ebp,%ebx,1)
80100590:	8d 5e 02             	lea    0x2(%esi),%ebx
80100593:	eb 0a                	jmp    8010059f <printint+0x5e>
    consputc(buf[i]);
80100595:	0f be 44 1d d8       	movsbl -0x28(%ebp,%ebx,1),%eax
8010059a:	e8 47 ff ff ff       	call   801004e6 <consputc>
  while(--i >= 0)
8010059f:	83 eb 01             	sub    $0x1,%ebx
801005a2:	79 f1                	jns    80100595 <printint+0x54>
}
801005a4:	83 c4 1c             	add    $0x1c,%esp
801005a7:	5b                   	pop    %ebx
801005a8:	5e                   	pop    %esi
801005a9:	5f                   	pop    %edi
801005aa:	5d                   	pop    %ebp
801005ab:	c3                   	ret    

801005ac <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
801005ac:	55                   	push   %ebp
801005ad:	89 e5                	mov    %esp,%ebp
801005af:	57                   	push   %edi
801005b0:	56                   	push   %esi
801005b1:	53                   	push   %ebx
801005b2:	83 ec 18             	sub    $0x18,%esp
801005b5:	8b 7d 0c             	mov    0xc(%ebp),%edi
801005b8:	8b 75 10             	mov    0x10(%ebp),%esi
  int i;

  iunlock(ip);
801005bb:	ff 75 08             	pushl  0x8(%ebp)
801005be:	e8 6e 10 00 00       	call   80101631 <iunlock>
  acquire(&cons.lock);
801005c3:	c7 04 24 20 a5 10 80 	movl   $0x8010a520,(%esp)
801005ca:	e8 4c 39 00 00       	call   80103f1b <acquire>
  for(i = 0; i < n; i++)
801005cf:	83 c4 10             	add    $0x10,%esp
801005d2:	bb 00 00 00 00       	mov    $0x0,%ebx
801005d7:	eb 0c                	jmp    801005e5 <consolewrite+0x39>
    consputc(buf[i] & 0xff);
801005d9:	0f b6 04 1f          	movzbl (%edi,%ebx,1),%eax
801005dd:	e8 04 ff ff ff       	call   801004e6 <consputc>
  for(i = 0; i < n; i++)
801005e2:	83 c3 01             	add    $0x1,%ebx
801005e5:	39 f3                	cmp    %esi,%ebx
801005e7:	7c f0                	jl     801005d9 <consolewrite+0x2d>
  release(&cons.lock);
801005e9:	83 ec 0c             	sub    $0xc,%esp
801005ec:	68 20 a5 10 80       	push   $0x8010a520
801005f1:	e8 8a 39 00 00       	call   80103f80 <release>
  ilock(ip);
801005f6:	83 c4 04             	add    $0x4,%esp
801005f9:	ff 75 08             	pushl  0x8(%ebp)
801005fc:	e8 6e 0f 00 00       	call   8010156f <ilock>

  return n;
}
80100601:	89 f0                	mov    %esi,%eax
80100603:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100606:	5b                   	pop    %ebx
80100607:	5e                   	pop    %esi
80100608:	5f                   	pop    %edi
80100609:	5d                   	pop    %ebp
8010060a:	c3                   	ret    

8010060b <cprintf>:
{
8010060b:	55                   	push   %ebp
8010060c:	89 e5                	mov    %esp,%ebp
8010060e:	57                   	push   %edi
8010060f:	56                   	push   %esi
80100610:	53                   	push   %ebx
80100611:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
80100614:	a1 54 a5 10 80       	mov    0x8010a554,%eax
80100619:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(locking)
8010061c:	85 c0                	test   %eax,%eax
8010061e:	75 10                	jne    80100630 <cprintf+0x25>
  if (fmt == 0)
80100620:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
80100624:	74 1c                	je     80100642 <cprintf+0x37>
  argp = (uint*)(void*)(&fmt + 1);
80100626:	8d 7d 0c             	lea    0xc(%ebp),%edi
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100629:	bb 00 00 00 00       	mov    $0x0,%ebx
8010062e:	eb 27                	jmp    80100657 <cprintf+0x4c>
    acquire(&cons.lock);
80100630:	83 ec 0c             	sub    $0xc,%esp
80100633:	68 20 a5 10 80       	push   $0x8010a520
80100638:	e8 de 38 00 00       	call   80103f1b <acquire>
8010063d:	83 c4 10             	add    $0x10,%esp
80100640:	eb de                	jmp    80100620 <cprintf+0x15>
    panic("null fmt");
80100642:	83 ec 0c             	sub    $0xc,%esp
80100645:	68 1f 6d 10 80       	push   $0x80106d1f
8010064a:	e8 f9 fc ff ff       	call   80100348 <panic>
      consputc(c);
8010064f:	e8 92 fe ff ff       	call   801004e6 <consputc>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100654:	83 c3 01             	add    $0x1,%ebx
80100657:	8b 55 08             	mov    0x8(%ebp),%edx
8010065a:	0f b6 04 1a          	movzbl (%edx,%ebx,1),%eax
8010065e:	85 c0                	test   %eax,%eax
80100660:	0f 84 b8 00 00 00    	je     8010071e <cprintf+0x113>
    if(c != '%'){
80100666:	83 f8 25             	cmp    $0x25,%eax
80100669:	75 e4                	jne    8010064f <cprintf+0x44>
    c = fmt[++i] & 0xff;
8010066b:	83 c3 01             	add    $0x1,%ebx
8010066e:	0f b6 34 1a          	movzbl (%edx,%ebx,1),%esi
    if(c == 0)
80100672:	85 f6                	test   %esi,%esi
80100674:	0f 84 a4 00 00 00    	je     8010071e <cprintf+0x113>
    switch(c){
8010067a:	83 fe 70             	cmp    $0x70,%esi
8010067d:	74 48                	je     801006c7 <cprintf+0xbc>
8010067f:	83 fe 70             	cmp    $0x70,%esi
80100682:	7f 26                	jg     801006aa <cprintf+0x9f>
80100684:	83 fe 25             	cmp    $0x25,%esi
80100687:	0f 84 82 00 00 00    	je     8010070f <cprintf+0x104>
8010068d:	83 fe 64             	cmp    $0x64,%esi
80100690:	75 22                	jne    801006b4 <cprintf+0xa9>
      printint(*argp++, 10, 1);
80100692:	8d 77 04             	lea    0x4(%edi),%esi
80100695:	8b 07                	mov    (%edi),%eax
80100697:	b9 01 00 00 00       	mov    $0x1,%ecx
8010069c:	ba 0a 00 00 00       	mov    $0xa,%edx
801006a1:	e8 9b fe ff ff       	call   80100541 <printint>
801006a6:	89 f7                	mov    %esi,%edi
      break;
801006a8:	eb aa                	jmp    80100654 <cprintf+0x49>
    switch(c){
801006aa:	83 fe 73             	cmp    $0x73,%esi
801006ad:	74 33                	je     801006e2 <cprintf+0xd7>
801006af:	83 fe 78             	cmp    $0x78,%esi
801006b2:	74 13                	je     801006c7 <cprintf+0xbc>
      consputc('%');
801006b4:	b8 25 00 00 00       	mov    $0x25,%eax
801006b9:	e8 28 fe ff ff       	call   801004e6 <consputc>
      consputc(c);
801006be:	89 f0                	mov    %esi,%eax
801006c0:	e8 21 fe ff ff       	call   801004e6 <consputc>
      break;
801006c5:	eb 8d                	jmp    80100654 <cprintf+0x49>
      printint(*argp++, 16, 0);
801006c7:	8d 77 04             	lea    0x4(%edi),%esi
801006ca:	8b 07                	mov    (%edi),%eax
801006cc:	b9 00 00 00 00       	mov    $0x0,%ecx
801006d1:	ba 10 00 00 00       	mov    $0x10,%edx
801006d6:	e8 66 fe ff ff       	call   80100541 <printint>
801006db:	89 f7                	mov    %esi,%edi
      break;
801006dd:	e9 72 ff ff ff       	jmp    80100654 <cprintf+0x49>
      if((s = (char*)*argp++) == 0)
801006e2:	8d 47 04             	lea    0x4(%edi),%eax
801006e5:	89 45 e0             	mov    %eax,-0x20(%ebp)
801006e8:	8b 37                	mov    (%edi),%esi
801006ea:	85 f6                	test   %esi,%esi
801006ec:	75 12                	jne    80100700 <cprintf+0xf5>
        s = "(null)";
801006ee:	be 18 6d 10 80       	mov    $0x80106d18,%esi
801006f3:	eb 0b                	jmp    80100700 <cprintf+0xf5>
        consputc(*s);
801006f5:	0f be c0             	movsbl %al,%eax
801006f8:	e8 e9 fd ff ff       	call   801004e6 <consputc>
      for(; *s; s++)
801006fd:	83 c6 01             	add    $0x1,%esi
80100700:	0f b6 06             	movzbl (%esi),%eax
80100703:	84 c0                	test   %al,%al
80100705:	75 ee                	jne    801006f5 <cprintf+0xea>
      if((s = (char*)*argp++) == 0)
80100707:	8b 7d e0             	mov    -0x20(%ebp),%edi
8010070a:	e9 45 ff ff ff       	jmp    80100654 <cprintf+0x49>
      consputc('%');
8010070f:	b8 25 00 00 00       	mov    $0x25,%eax
80100714:	e8 cd fd ff ff       	call   801004e6 <consputc>
      break;
80100719:	e9 36 ff ff ff       	jmp    80100654 <cprintf+0x49>
  if(locking)
8010071e:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
80100722:	75 08                	jne    8010072c <cprintf+0x121>
}
80100724:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100727:	5b                   	pop    %ebx
80100728:	5e                   	pop    %esi
80100729:	5f                   	pop    %edi
8010072a:	5d                   	pop    %ebp
8010072b:	c3                   	ret    
    release(&cons.lock);
8010072c:	83 ec 0c             	sub    $0xc,%esp
8010072f:	68 20 a5 10 80       	push   $0x8010a520
80100734:	e8 47 38 00 00       	call   80103f80 <release>
80100739:	83 c4 10             	add    $0x10,%esp
}
8010073c:	eb e6                	jmp    80100724 <cprintf+0x119>

8010073e <consoleintr>:
{
8010073e:	55                   	push   %ebp
8010073f:	89 e5                	mov    %esp,%ebp
80100741:	57                   	push   %edi
80100742:	56                   	push   %esi
80100743:	53                   	push   %ebx
80100744:	83 ec 18             	sub    $0x18,%esp
80100747:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&cons.lock);
8010074a:	68 20 a5 10 80       	push   $0x8010a520
8010074f:	e8 c7 37 00 00       	call   80103f1b <acquire>
  while((c = getc()) >= 0){
80100754:	83 c4 10             	add    $0x10,%esp
  int c, doprocdump = 0;
80100757:	be 00 00 00 00       	mov    $0x0,%esi
  while((c = getc()) >= 0){
8010075c:	e9 c5 00 00 00       	jmp    80100826 <consoleintr+0xe8>
    switch(c){
80100761:	83 ff 08             	cmp    $0x8,%edi
80100764:	0f 84 e0 00 00 00    	je     8010084a <consoleintr+0x10c>
      if(c != 0 && input.e-input.r < INPUT_BUF){
8010076a:	85 ff                	test   %edi,%edi
8010076c:	0f 84 b4 00 00 00    	je     80100826 <consoleintr+0xe8>
80100772:	a1 c8 ff 10 80       	mov    0x8010ffc8,%eax
80100777:	89 c2                	mov    %eax,%edx
80100779:	2b 15 c0 ff 10 80    	sub    0x8010ffc0,%edx
8010077f:	83 fa 7f             	cmp    $0x7f,%edx
80100782:	0f 87 9e 00 00 00    	ja     80100826 <consoleintr+0xe8>
        c = (c == '\r') ? '\n' : c;
80100788:	83 ff 0d             	cmp    $0xd,%edi
8010078b:	0f 84 86 00 00 00    	je     80100817 <consoleintr+0xd9>
        input.buf[input.e++ % INPUT_BUF] = c;
80100791:	8d 50 01             	lea    0x1(%eax),%edx
80100794:	89 15 c8 ff 10 80    	mov    %edx,0x8010ffc8
8010079a:	83 e0 7f             	and    $0x7f,%eax
8010079d:	89 f9                	mov    %edi,%ecx
8010079f:	88 88 40 ff 10 80    	mov    %cl,-0x7fef00c0(%eax)
        consputc(c);
801007a5:	89 f8                	mov    %edi,%eax
801007a7:	e8 3a fd ff ff       	call   801004e6 <consputc>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
801007ac:	83 ff 0a             	cmp    $0xa,%edi
801007af:	0f 94 c2             	sete   %dl
801007b2:	83 ff 04             	cmp    $0x4,%edi
801007b5:	0f 94 c0             	sete   %al
801007b8:	08 c2                	or     %al,%dl
801007ba:	75 10                	jne    801007cc <consoleintr+0x8e>
801007bc:	a1 c0 ff 10 80       	mov    0x8010ffc0,%eax
801007c1:	83 e8 80             	sub    $0xffffff80,%eax
801007c4:	39 05 c8 ff 10 80    	cmp    %eax,0x8010ffc8
801007ca:	75 5a                	jne    80100826 <consoleintr+0xe8>
          input.w = input.e;
801007cc:	a1 c8 ff 10 80       	mov    0x8010ffc8,%eax
801007d1:	a3 c4 ff 10 80       	mov    %eax,0x8010ffc4
          wakeup(&input.r);
801007d6:	83 ec 0c             	sub    $0xc,%esp
801007d9:	68 c0 ff 10 80       	push   $0x8010ffc0
801007de:	e8 18 30 00 00       	call   801037fb <wakeup>
801007e3:	83 c4 10             	add    $0x10,%esp
801007e6:	eb 3e                	jmp    80100826 <consoleintr+0xe8>
        input.e--;
801007e8:	a3 c8 ff 10 80       	mov    %eax,0x8010ffc8
        consputc(BACKSPACE);
801007ed:	b8 00 01 00 00       	mov    $0x100,%eax
801007f2:	e8 ef fc ff ff       	call   801004e6 <consputc>
      while(input.e != input.w &&
801007f7:	a1 c8 ff 10 80       	mov    0x8010ffc8,%eax
801007fc:	3b 05 c4 ff 10 80    	cmp    0x8010ffc4,%eax
80100802:	74 22                	je     80100826 <consoleintr+0xe8>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
80100804:	83 e8 01             	sub    $0x1,%eax
80100807:	89 c2                	mov    %eax,%edx
80100809:	83 e2 7f             	and    $0x7f,%edx
      while(input.e != input.w &&
8010080c:	80 ba 40 ff 10 80 0a 	cmpb   $0xa,-0x7fef00c0(%edx)
80100813:	75 d3                	jne    801007e8 <consoleintr+0xaa>
80100815:	eb 0f                	jmp    80100826 <consoleintr+0xe8>
        c = (c == '\r') ? '\n' : c;
80100817:	bf 0a 00 00 00       	mov    $0xa,%edi
8010081c:	e9 70 ff ff ff       	jmp    80100791 <consoleintr+0x53>
      doprocdump = 1;
80100821:	be 01 00 00 00       	mov    $0x1,%esi
  while((c = getc()) >= 0){
80100826:	ff d3                	call   *%ebx
80100828:	89 c7                	mov    %eax,%edi
8010082a:	85 c0                	test   %eax,%eax
8010082c:	78 3d                	js     8010086b <consoleintr+0x12d>
    switch(c){
8010082e:	83 ff 10             	cmp    $0x10,%edi
80100831:	74 ee                	je     80100821 <consoleintr+0xe3>
80100833:	83 ff 10             	cmp    $0x10,%edi
80100836:	0f 8e 25 ff ff ff    	jle    80100761 <consoleintr+0x23>
8010083c:	83 ff 15             	cmp    $0x15,%edi
8010083f:	74 b6                	je     801007f7 <consoleintr+0xb9>
80100841:	83 ff 7f             	cmp    $0x7f,%edi
80100844:	0f 85 20 ff ff ff    	jne    8010076a <consoleintr+0x2c>
      if(input.e != input.w){
8010084a:	a1 c8 ff 10 80       	mov    0x8010ffc8,%eax
8010084f:	3b 05 c4 ff 10 80    	cmp    0x8010ffc4,%eax
80100855:	74 cf                	je     80100826 <consoleintr+0xe8>
        input.e--;
80100857:	83 e8 01             	sub    $0x1,%eax
8010085a:	a3 c8 ff 10 80       	mov    %eax,0x8010ffc8
        consputc(BACKSPACE);
8010085f:	b8 00 01 00 00       	mov    $0x100,%eax
80100864:	e8 7d fc ff ff       	call   801004e6 <consputc>
80100869:	eb bb                	jmp    80100826 <consoleintr+0xe8>
  release(&cons.lock);
8010086b:	83 ec 0c             	sub    $0xc,%esp
8010086e:	68 20 a5 10 80       	push   $0x8010a520
80100873:	e8 08 37 00 00       	call   80103f80 <release>
  if(doprocdump) {
80100878:	83 c4 10             	add    $0x10,%esp
8010087b:	85 f6                	test   %esi,%esi
8010087d:	75 08                	jne    80100887 <consoleintr+0x149>
}
8010087f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100882:	5b                   	pop    %ebx
80100883:	5e                   	pop    %esi
80100884:	5f                   	pop    %edi
80100885:	5d                   	pop    %ebp
80100886:	c3                   	ret    
    procdump();  // now call procdump() wo. cons.lock held
80100887:	e8 0e 30 00 00       	call   8010389a <procdump>
}
8010088c:	eb f1                	jmp    8010087f <consoleintr+0x141>

8010088e <consoleinit>:

void
consoleinit(void)
{
8010088e:	55                   	push   %ebp
8010088f:	89 e5                	mov    %esp,%ebp
80100891:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
80100894:	68 28 6d 10 80       	push   $0x80106d28
80100899:	68 20 a5 10 80       	push   $0x8010a520
8010089e:	e8 3c 35 00 00       	call   80103ddf <initlock>

  devsw[CONSOLE].write = consolewrite;
801008a3:	c7 05 8c 09 11 80 ac 	movl   $0x801005ac,0x8011098c
801008aa:	05 10 80 
  devsw[CONSOLE].read = consoleread;
801008ad:	c7 05 88 09 11 80 68 	movl   $0x80100268,0x80110988
801008b4:	02 10 80 
  cons.locking = 1;
801008b7:	c7 05 54 a5 10 80 01 	movl   $0x1,0x8010a554
801008be:	00 00 00 

  ioapicenable(IRQ_KBD, 0);
801008c1:	83 c4 08             	add    $0x8,%esp
801008c4:	6a 00                	push   $0x0
801008c6:	6a 01                	push   $0x1
801008c8:	e8 9f 16 00 00       	call   80101f6c <ioapicenable>
}
801008cd:	83 c4 10             	add    $0x10,%esp
801008d0:	c9                   	leave  
801008d1:	c3                   	ret    

801008d2 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
801008d2:	55                   	push   %ebp
801008d3:	89 e5                	mov    %esp,%ebp
801008d5:	57                   	push   %edi
801008d6:	56                   	push   %esi
801008d7:	53                   	push   %ebx
801008d8:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
801008de:	e8 74 29 00 00       	call   80103257 <myproc>
801008e3:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)

  begin_op();
801008e9:	e8 e4 1e 00 00       	call   801027d2 <begin_op>

  if((ip = namei(path)) == 0){
801008ee:	83 ec 0c             	sub    $0xc,%esp
801008f1:	ff 75 08             	pushl  0x8(%ebp)
801008f4:	e8 d6 12 00 00       	call   80101bcf <namei>
801008f9:	83 c4 10             	add    $0x10,%esp
801008fc:	85 c0                	test   %eax,%eax
801008fe:	74 4a                	je     8010094a <exec+0x78>
80100900:	89 c3                	mov    %eax,%ebx
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100902:	83 ec 0c             	sub    $0xc,%esp
80100905:	50                   	push   %eax
80100906:	e8 64 0c 00 00       	call   8010156f <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
8010090b:	6a 34                	push   $0x34
8010090d:	6a 00                	push   $0x0
8010090f:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100915:	50                   	push   %eax
80100916:	53                   	push   %ebx
80100917:	e8 45 0e 00 00       	call   80101761 <readi>
8010091c:	83 c4 20             	add    $0x20,%esp
8010091f:	83 f8 34             	cmp    $0x34,%eax
80100922:	74 42                	je     80100966 <exec+0x94>
  return 0;

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
80100924:	85 db                	test   %ebx,%ebx
80100926:	0f 84 dd 02 00 00    	je     80100c09 <exec+0x337>
    iunlockput(ip);
8010092c:	83 ec 0c             	sub    $0xc,%esp
8010092f:	53                   	push   %ebx
80100930:	e8 e1 0d 00 00       	call   80101716 <iunlockput>
    end_op();
80100935:	e8 12 1f 00 00       	call   8010284c <end_op>
8010093a:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
8010093d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100942:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100945:	5b                   	pop    %ebx
80100946:	5e                   	pop    %esi
80100947:	5f                   	pop    %edi
80100948:	5d                   	pop    %ebp
80100949:	c3                   	ret    
    end_op();
8010094a:	e8 fd 1e 00 00       	call   8010284c <end_op>
    cprintf("exec: fail\n");
8010094f:	83 ec 0c             	sub    $0xc,%esp
80100952:	68 41 6d 10 80       	push   $0x80106d41
80100957:	e8 af fc ff ff       	call   8010060b <cprintf>
    return -1;
8010095c:	83 c4 10             	add    $0x10,%esp
8010095f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100964:	eb dc                	jmp    80100942 <exec+0x70>
  if(elf.magic != ELF_MAGIC)
80100966:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
8010096d:	45 4c 46 
80100970:	75 b2                	jne    80100924 <exec+0x52>
  if((pgdir = setupkvm()) == 0)
80100972:	e8 92 5e 00 00       	call   80106809 <setupkvm>
80100977:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)
8010097d:	85 c0                	test   %eax,%eax
8010097f:	0f 84 06 01 00 00    	je     80100a8b <exec+0x1b9>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100985:	8b 85 40 ff ff ff    	mov    -0xc0(%ebp),%eax
  sz = 0;
8010098b:	bf 00 00 00 00       	mov    $0x0,%edi
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100990:	be 00 00 00 00       	mov    $0x0,%esi
80100995:	eb 0c                	jmp    801009a3 <exec+0xd1>
80100997:	83 c6 01             	add    $0x1,%esi
8010099a:	8b 85 f0 fe ff ff    	mov    -0x110(%ebp),%eax
801009a0:	83 c0 20             	add    $0x20,%eax
801009a3:	0f b7 95 50 ff ff ff 	movzwl -0xb0(%ebp),%edx
801009aa:	39 f2                	cmp    %esi,%edx
801009ac:	0f 8e 98 00 00 00    	jle    80100a4a <exec+0x178>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
801009b2:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
801009b8:	6a 20                	push   $0x20
801009ba:	50                   	push   %eax
801009bb:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
801009c1:	50                   	push   %eax
801009c2:	53                   	push   %ebx
801009c3:	e8 99 0d 00 00       	call   80101761 <readi>
801009c8:	83 c4 10             	add    $0x10,%esp
801009cb:	83 f8 20             	cmp    $0x20,%eax
801009ce:	0f 85 b7 00 00 00    	jne    80100a8b <exec+0x1b9>
    if(ph.type != ELF_PROG_LOAD)
801009d4:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
801009db:	75 ba                	jne    80100997 <exec+0xc5>
    if(ph.memsz < ph.filesz)
801009dd:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
801009e3:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
801009e9:	0f 82 9c 00 00 00    	jb     80100a8b <exec+0x1b9>
    if(ph.vaddr + ph.memsz < ph.vaddr)
801009ef:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
801009f5:	0f 82 90 00 00 00    	jb     80100a8b <exec+0x1b9>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
801009fb:	83 ec 04             	sub    $0x4,%esp
801009fe:	50                   	push   %eax
801009ff:	57                   	push   %edi
80100a00:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100a06:	e8 79 5c 00 00       	call   80106684 <allocuvm>
80100a0b:	89 c7                	mov    %eax,%edi
80100a0d:	83 c4 10             	add    $0x10,%esp
80100a10:	85 c0                	test   %eax,%eax
80100a12:	74 77                	je     80100a8b <exec+0x1b9>
    if(ph.vaddr % PGSIZE != 0)
80100a14:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100a1a:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100a1f:	75 6a                	jne    80100a8b <exec+0x1b9>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100a21:	83 ec 0c             	sub    $0xc,%esp
80100a24:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100a2a:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100a30:	53                   	push   %ebx
80100a31:	50                   	push   %eax
80100a32:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100a38:	e8 9f 5a 00 00       	call   801064dc <loaduvm>
80100a3d:	83 c4 20             	add    $0x20,%esp
80100a40:	85 c0                	test   %eax,%eax
80100a42:	0f 89 4f ff ff ff    	jns    80100997 <exec+0xc5>
 bad:
80100a48:	eb 41                	jmp    80100a8b <exec+0x1b9>
  iunlockput(ip);
80100a4a:	83 ec 0c             	sub    $0xc,%esp
80100a4d:	53                   	push   %ebx
80100a4e:	e8 c3 0c 00 00       	call   80101716 <iunlockput>
  end_op();
80100a53:	e8 f4 1d 00 00       	call   8010284c <end_op>
  sz = PGROUNDUP(sz);
80100a58:	8d 87 ff 0f 00 00    	lea    0xfff(%edi),%eax
80100a5e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((sz = allocuvm(pgdir, sz, sz + 2*PGSIZE)) == 0)
80100a63:	83 c4 0c             	add    $0xc,%esp
80100a66:	8d 90 00 20 00 00    	lea    0x2000(%eax),%edx
80100a6c:	52                   	push   %edx
80100a6d:	50                   	push   %eax
80100a6e:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100a74:	e8 0b 5c 00 00       	call   80106684 <allocuvm>
80100a79:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100a7f:	83 c4 10             	add    $0x10,%esp
80100a82:	85 c0                	test   %eax,%eax
80100a84:	75 24                	jne    80100aaa <exec+0x1d8>
  ip = 0;
80100a86:	bb 00 00 00 00       	mov    $0x0,%ebx
  if(pgdir)
80100a8b:	8b 85 ec fe ff ff    	mov    -0x114(%ebp),%eax
80100a91:	85 c0                	test   %eax,%eax
80100a93:	0f 84 8b fe ff ff    	je     80100924 <exec+0x52>
    freevm(pgdir);
80100a99:	83 ec 0c             	sub    $0xc,%esp
80100a9c:	50                   	push   %eax
80100a9d:	e8 e3 5c 00 00       	call   80106785 <freevm>
80100aa2:	83 c4 10             	add    $0x10,%esp
80100aa5:	e9 7a fe ff ff       	jmp    80100924 <exec+0x52>
  clearpteu(pgdir, (char*)(sz - 2*PGSIZE));
80100aaa:	89 c7                	mov    %eax,%edi
80100aac:	8d 80 00 e0 ff ff    	lea    -0x2000(%eax),%eax
80100ab2:	83 ec 08             	sub    $0x8,%esp
80100ab5:	50                   	push   %eax
80100ab6:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100abc:	e8 cd 5d 00 00       	call   8010688e <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100ac1:	83 c4 10             	add    $0x10,%esp
80100ac4:	bb 00 00 00 00       	mov    $0x0,%ebx
80100ac9:	8b 45 0c             	mov    0xc(%ebp),%eax
80100acc:	8d 34 98             	lea    (%eax,%ebx,4),%esi
80100acf:	8b 06                	mov    (%esi),%eax
80100ad1:	85 c0                	test   %eax,%eax
80100ad3:	74 4d                	je     80100b22 <exec+0x250>
    if(argc >= MAXARG)
80100ad5:	83 fb 1f             	cmp    $0x1f,%ebx
80100ad8:	0f 87 0d 01 00 00    	ja     80100beb <exec+0x319>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100ade:	83 ec 0c             	sub    $0xc,%esp
80100ae1:	50                   	push   %eax
80100ae2:	e8 82 36 00 00       	call   80104169 <strlen>
80100ae7:	29 c7                	sub    %eax,%edi
80100ae9:	83 ef 01             	sub    $0x1,%edi
80100aec:	83 e7 fc             	and    $0xfffffffc,%edi
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100aef:	83 c4 04             	add    $0x4,%esp
80100af2:	ff 36                	pushl  (%esi)
80100af4:	e8 70 36 00 00       	call   80104169 <strlen>
80100af9:	83 c0 01             	add    $0x1,%eax
80100afc:	50                   	push   %eax
80100afd:	ff 36                	pushl  (%esi)
80100aff:	57                   	push   %edi
80100b00:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100b06:	e8 24 61 00 00       	call   80106c2f <copyout>
80100b0b:	83 c4 20             	add    $0x20,%esp
80100b0e:	85 c0                	test   %eax,%eax
80100b10:	0f 88 df 00 00 00    	js     80100bf5 <exec+0x323>
    ustack[3+argc] = sp;
80100b16:	89 bc 9d 64 ff ff ff 	mov    %edi,-0x9c(%ebp,%ebx,4)
  for(argc = 0; argv[argc]; argc++) {
80100b1d:	83 c3 01             	add    $0x1,%ebx
80100b20:	eb a7                	jmp    80100ac9 <exec+0x1f7>
  ustack[3+argc] = 0;
80100b22:	c7 84 9d 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%ebx,4)
80100b29:	00 00 00 00 
  ustack[0] = 0xffffffff;  // fake return PC
80100b2d:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100b34:	ff ff ff 
  ustack[1] = argc;
80100b37:	89 9d 5c ff ff ff    	mov    %ebx,-0xa4(%ebp)
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100b3d:	8d 04 9d 04 00 00 00 	lea    0x4(,%ebx,4),%eax
80100b44:	89 f9                	mov    %edi,%ecx
80100b46:	29 c1                	sub    %eax,%ecx
80100b48:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  sp -= (3+argc+1) * 4;
80100b4e:	8d 04 9d 10 00 00 00 	lea    0x10(,%ebx,4),%eax
80100b55:	29 c7                	sub    %eax,%edi
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100b57:	50                   	push   %eax
80100b58:	8d 85 58 ff ff ff    	lea    -0xa8(%ebp),%eax
80100b5e:	50                   	push   %eax
80100b5f:	57                   	push   %edi
80100b60:	ff b5 ec fe ff ff    	pushl  -0x114(%ebp)
80100b66:	e8 c4 60 00 00       	call   80106c2f <copyout>
80100b6b:	83 c4 10             	add    $0x10,%esp
80100b6e:	85 c0                	test   %eax,%eax
80100b70:	0f 88 89 00 00 00    	js     80100bff <exec+0x32d>
  for(last=s=path; *s; s++)
80100b76:	8b 55 08             	mov    0x8(%ebp),%edx
80100b79:	89 d0                	mov    %edx,%eax
80100b7b:	eb 03                	jmp    80100b80 <exec+0x2ae>
80100b7d:	83 c0 01             	add    $0x1,%eax
80100b80:	0f b6 08             	movzbl (%eax),%ecx
80100b83:	84 c9                	test   %cl,%cl
80100b85:	74 0a                	je     80100b91 <exec+0x2bf>
    if(*s == '/')
80100b87:	80 f9 2f             	cmp    $0x2f,%cl
80100b8a:	75 f1                	jne    80100b7d <exec+0x2ab>
      last = s+1;
80100b8c:	8d 50 01             	lea    0x1(%eax),%edx
80100b8f:	eb ec                	jmp    80100b7d <exec+0x2ab>
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100b91:	8b b5 f4 fe ff ff    	mov    -0x10c(%ebp),%esi
80100b97:	89 f0                	mov    %esi,%eax
80100b99:	83 c0 6c             	add    $0x6c,%eax
80100b9c:	83 ec 04             	sub    $0x4,%esp
80100b9f:	6a 10                	push   $0x10
80100ba1:	52                   	push   %edx
80100ba2:	50                   	push   %eax
80100ba3:	e8 86 35 00 00       	call   8010412e <safestrcpy>
  oldpgdir = curproc->pgdir;
80100ba8:	8b 5e 04             	mov    0x4(%esi),%ebx
  curproc->pgdir = pgdir;
80100bab:	8b 8d ec fe ff ff    	mov    -0x114(%ebp),%ecx
80100bb1:	89 4e 04             	mov    %ecx,0x4(%esi)
  curproc->sz = sz;
80100bb4:	8b 8d f0 fe ff ff    	mov    -0x110(%ebp),%ecx
80100bba:	89 0e                	mov    %ecx,(%esi)
  curproc->tf->eip = elf.entry;  // main
80100bbc:	8b 46 18             	mov    0x18(%esi),%eax
80100bbf:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80100bc5:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80100bc8:	8b 46 18             	mov    0x18(%esi),%eax
80100bcb:	89 78 44             	mov    %edi,0x44(%eax)
  switchuvm(curproc);
80100bce:	89 34 24             	mov    %esi,(%esp)
80100bd1:	e8 5c 57 00 00       	call   80106332 <switchuvm>
  freevm(oldpgdir);
80100bd6:	89 1c 24             	mov    %ebx,(%esp)
80100bd9:	e8 a7 5b 00 00       	call   80106785 <freevm>
  return 0;
80100bde:	83 c4 10             	add    $0x10,%esp
80100be1:	b8 00 00 00 00       	mov    $0x0,%eax
80100be6:	e9 57 fd ff ff       	jmp    80100942 <exec+0x70>
  ip = 0;
80100beb:	bb 00 00 00 00       	mov    $0x0,%ebx
80100bf0:	e9 96 fe ff ff       	jmp    80100a8b <exec+0x1b9>
80100bf5:	bb 00 00 00 00       	mov    $0x0,%ebx
80100bfa:	e9 8c fe ff ff       	jmp    80100a8b <exec+0x1b9>
80100bff:	bb 00 00 00 00       	mov    $0x0,%ebx
80100c04:	e9 82 fe ff ff       	jmp    80100a8b <exec+0x1b9>
  return -1;
80100c09:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100c0e:	e9 2f fd ff ff       	jmp    80100942 <exec+0x70>

80100c13 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100c13:	55                   	push   %ebp
80100c14:	89 e5                	mov    %esp,%ebp
80100c16:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80100c19:	68 4d 6d 10 80       	push   $0x80106d4d
80100c1e:	68 e0 ff 10 80       	push   $0x8010ffe0
80100c23:	e8 b7 31 00 00       	call   80103ddf <initlock>
}
80100c28:	83 c4 10             	add    $0x10,%esp
80100c2b:	c9                   	leave  
80100c2c:	c3                   	ret    

80100c2d <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80100c2d:	55                   	push   %ebp
80100c2e:	89 e5                	mov    %esp,%ebp
80100c30:	53                   	push   %ebx
80100c31:	83 ec 10             	sub    $0x10,%esp
  struct file *f;

  acquire(&ftable.lock);
80100c34:	68 e0 ff 10 80       	push   $0x8010ffe0
80100c39:	e8 dd 32 00 00       	call   80103f1b <acquire>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100c3e:	83 c4 10             	add    $0x10,%esp
80100c41:	bb 14 00 11 80       	mov    $0x80110014,%ebx
80100c46:	81 fb 74 09 11 80    	cmp    $0x80110974,%ebx
80100c4c:	73 29                	jae    80100c77 <filealloc+0x4a>
    if(f->ref == 0){
80100c4e:	83 7b 04 00          	cmpl   $0x0,0x4(%ebx)
80100c52:	74 05                	je     80100c59 <filealloc+0x2c>
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100c54:	83 c3 18             	add    $0x18,%ebx
80100c57:	eb ed                	jmp    80100c46 <filealloc+0x19>
      f->ref = 1;
80100c59:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
80100c60:	83 ec 0c             	sub    $0xc,%esp
80100c63:	68 e0 ff 10 80       	push   $0x8010ffe0
80100c68:	e8 13 33 00 00       	call   80103f80 <release>
      return f;
80100c6d:	83 c4 10             	add    $0x10,%esp
    }
  }
  release(&ftable.lock);
  return 0;
}
80100c70:	89 d8                	mov    %ebx,%eax
80100c72:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100c75:	c9                   	leave  
80100c76:	c3                   	ret    
  release(&ftable.lock);
80100c77:	83 ec 0c             	sub    $0xc,%esp
80100c7a:	68 e0 ff 10 80       	push   $0x8010ffe0
80100c7f:	e8 fc 32 00 00       	call   80103f80 <release>
  return 0;
80100c84:	83 c4 10             	add    $0x10,%esp
80100c87:	bb 00 00 00 00       	mov    $0x0,%ebx
80100c8c:	eb e2                	jmp    80100c70 <filealloc+0x43>

80100c8e <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80100c8e:	55                   	push   %ebp
80100c8f:	89 e5                	mov    %esp,%ebp
80100c91:	53                   	push   %ebx
80100c92:	83 ec 10             	sub    $0x10,%esp
80100c95:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
80100c98:	68 e0 ff 10 80       	push   $0x8010ffe0
80100c9d:	e8 79 32 00 00       	call   80103f1b <acquire>
  if(f->ref < 1)
80100ca2:	8b 43 04             	mov    0x4(%ebx),%eax
80100ca5:	83 c4 10             	add    $0x10,%esp
80100ca8:	85 c0                	test   %eax,%eax
80100caa:	7e 1a                	jle    80100cc6 <filedup+0x38>
    panic("filedup");
  f->ref++;
80100cac:	83 c0 01             	add    $0x1,%eax
80100caf:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
80100cb2:	83 ec 0c             	sub    $0xc,%esp
80100cb5:	68 e0 ff 10 80       	push   $0x8010ffe0
80100cba:	e8 c1 32 00 00       	call   80103f80 <release>
  return f;
}
80100cbf:	89 d8                	mov    %ebx,%eax
80100cc1:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100cc4:	c9                   	leave  
80100cc5:	c3                   	ret    
    panic("filedup");
80100cc6:	83 ec 0c             	sub    $0xc,%esp
80100cc9:	68 54 6d 10 80       	push   $0x80106d54
80100cce:	e8 75 f6 ff ff       	call   80100348 <panic>

80100cd3 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80100cd3:	55                   	push   %ebp
80100cd4:	89 e5                	mov    %esp,%ebp
80100cd6:	53                   	push   %ebx
80100cd7:	83 ec 30             	sub    $0x30,%esp
80100cda:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
80100cdd:	68 e0 ff 10 80       	push   $0x8010ffe0
80100ce2:	e8 34 32 00 00       	call   80103f1b <acquire>
  if(f->ref < 1)
80100ce7:	8b 43 04             	mov    0x4(%ebx),%eax
80100cea:	83 c4 10             	add    $0x10,%esp
80100ced:	85 c0                	test   %eax,%eax
80100cef:	7e 1f                	jle    80100d10 <fileclose+0x3d>
    panic("fileclose");
  if(--f->ref > 0){
80100cf1:	83 e8 01             	sub    $0x1,%eax
80100cf4:	89 43 04             	mov    %eax,0x4(%ebx)
80100cf7:	85 c0                	test   %eax,%eax
80100cf9:	7e 22                	jle    80100d1d <fileclose+0x4a>
    release(&ftable.lock);
80100cfb:	83 ec 0c             	sub    $0xc,%esp
80100cfe:	68 e0 ff 10 80       	push   $0x8010ffe0
80100d03:	e8 78 32 00 00       	call   80103f80 <release>
    return;
80100d08:	83 c4 10             	add    $0x10,%esp
  else if(ff.type == FD_INODE){
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80100d0b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100d0e:	c9                   	leave  
80100d0f:	c3                   	ret    
    panic("fileclose");
80100d10:	83 ec 0c             	sub    $0xc,%esp
80100d13:	68 5c 6d 10 80       	push   $0x80106d5c
80100d18:	e8 2b f6 ff ff       	call   80100348 <panic>
  ff = *f;
80100d1d:	8b 03                	mov    (%ebx),%eax
80100d1f:	89 45 e0             	mov    %eax,-0x20(%ebp)
80100d22:	8b 43 08             	mov    0x8(%ebx),%eax
80100d25:	89 45 e8             	mov    %eax,-0x18(%ebp)
80100d28:	8b 43 0c             	mov    0xc(%ebx),%eax
80100d2b:	89 45 ec             	mov    %eax,-0x14(%ebp)
80100d2e:	8b 43 10             	mov    0x10(%ebx),%eax
80100d31:	89 45 f0             	mov    %eax,-0x10(%ebp)
  f->ref = 0;
80100d34:	c7 43 04 00 00 00 00 	movl   $0x0,0x4(%ebx)
  f->type = FD_NONE;
80100d3b:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  release(&ftable.lock);
80100d41:	83 ec 0c             	sub    $0xc,%esp
80100d44:	68 e0 ff 10 80       	push   $0x8010ffe0
80100d49:	e8 32 32 00 00       	call   80103f80 <release>
  if(ff.type == FD_PIPE)
80100d4e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80100d51:	83 c4 10             	add    $0x10,%esp
80100d54:	83 f8 01             	cmp    $0x1,%eax
80100d57:	74 1f                	je     80100d78 <fileclose+0xa5>
  else if(ff.type == FD_INODE){
80100d59:	83 f8 02             	cmp    $0x2,%eax
80100d5c:	75 ad                	jne    80100d0b <fileclose+0x38>
    begin_op();
80100d5e:	e8 6f 1a 00 00       	call   801027d2 <begin_op>
    iput(ff.ip);
80100d63:	83 ec 0c             	sub    $0xc,%esp
80100d66:	ff 75 f0             	pushl  -0x10(%ebp)
80100d69:	e8 08 09 00 00       	call   80101676 <iput>
    end_op();
80100d6e:	e8 d9 1a 00 00       	call   8010284c <end_op>
80100d73:	83 c4 10             	add    $0x10,%esp
80100d76:	eb 93                	jmp    80100d0b <fileclose+0x38>
    pipeclose(ff.pipe, ff.writable);
80100d78:	83 ec 08             	sub    $0x8,%esp
80100d7b:	0f be 45 e9          	movsbl -0x17(%ebp),%eax
80100d7f:	50                   	push   %eax
80100d80:	ff 75 ec             	pushl  -0x14(%ebp)
80100d83:	e8 f5 20 00 00       	call   80102e7d <pipeclose>
80100d88:	83 c4 10             	add    $0x10,%esp
80100d8b:	e9 7b ff ff ff       	jmp    80100d0b <fileclose+0x38>

80100d90 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80100d90:	55                   	push   %ebp
80100d91:	89 e5                	mov    %esp,%ebp
80100d93:	53                   	push   %ebx
80100d94:	83 ec 04             	sub    $0x4,%esp
80100d97:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
80100d9a:	83 3b 02             	cmpl   $0x2,(%ebx)
80100d9d:	75 31                	jne    80100dd0 <filestat+0x40>
    ilock(f->ip);
80100d9f:	83 ec 0c             	sub    $0xc,%esp
80100da2:	ff 73 10             	pushl  0x10(%ebx)
80100da5:	e8 c5 07 00 00       	call   8010156f <ilock>
    stati(f->ip, st);
80100daa:	83 c4 08             	add    $0x8,%esp
80100dad:	ff 75 0c             	pushl  0xc(%ebp)
80100db0:	ff 73 10             	pushl  0x10(%ebx)
80100db3:	e8 7e 09 00 00       	call   80101736 <stati>
    iunlock(f->ip);
80100db8:	83 c4 04             	add    $0x4,%esp
80100dbb:	ff 73 10             	pushl  0x10(%ebx)
80100dbe:	e8 6e 08 00 00       	call   80101631 <iunlock>
    return 0;
80100dc3:	83 c4 10             	add    $0x10,%esp
80100dc6:	b8 00 00 00 00       	mov    $0x0,%eax
  }
  return -1;
}
80100dcb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100dce:	c9                   	leave  
80100dcf:	c3                   	ret    
  return -1;
80100dd0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100dd5:	eb f4                	jmp    80100dcb <filestat+0x3b>

80100dd7 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80100dd7:	55                   	push   %ebp
80100dd8:	89 e5                	mov    %esp,%ebp
80100dda:	56                   	push   %esi
80100ddb:	53                   	push   %ebx
80100ddc:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;

  if(f->readable == 0)
80100ddf:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
80100de3:	74 70                	je     80100e55 <fileread+0x7e>
    return -1;
  if(f->type == FD_PIPE)
80100de5:	8b 03                	mov    (%ebx),%eax
80100de7:	83 f8 01             	cmp    $0x1,%eax
80100dea:	74 44                	je     80100e30 <fileread+0x59>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
80100dec:	83 f8 02             	cmp    $0x2,%eax
80100def:	75 57                	jne    80100e48 <fileread+0x71>
    ilock(f->ip);
80100df1:	83 ec 0c             	sub    $0xc,%esp
80100df4:	ff 73 10             	pushl  0x10(%ebx)
80100df7:	e8 73 07 00 00       	call   8010156f <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
80100dfc:	ff 75 10             	pushl  0x10(%ebp)
80100dff:	ff 73 14             	pushl  0x14(%ebx)
80100e02:	ff 75 0c             	pushl  0xc(%ebp)
80100e05:	ff 73 10             	pushl  0x10(%ebx)
80100e08:	e8 54 09 00 00       	call   80101761 <readi>
80100e0d:	89 c6                	mov    %eax,%esi
80100e0f:	83 c4 20             	add    $0x20,%esp
80100e12:	85 c0                	test   %eax,%eax
80100e14:	7e 03                	jle    80100e19 <fileread+0x42>
      f->off += r;
80100e16:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
80100e19:	83 ec 0c             	sub    $0xc,%esp
80100e1c:	ff 73 10             	pushl  0x10(%ebx)
80100e1f:	e8 0d 08 00 00       	call   80101631 <iunlock>
    return r;
80100e24:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
80100e27:	89 f0                	mov    %esi,%eax
80100e29:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100e2c:	5b                   	pop    %ebx
80100e2d:	5e                   	pop    %esi
80100e2e:	5d                   	pop    %ebp
80100e2f:	c3                   	ret    
    return piperead(f->pipe, addr, n);
80100e30:	83 ec 04             	sub    $0x4,%esp
80100e33:	ff 75 10             	pushl  0x10(%ebp)
80100e36:	ff 75 0c             	pushl  0xc(%ebp)
80100e39:	ff 73 0c             	pushl  0xc(%ebx)
80100e3c:	e8 94 21 00 00       	call   80102fd5 <piperead>
80100e41:	89 c6                	mov    %eax,%esi
80100e43:	83 c4 10             	add    $0x10,%esp
80100e46:	eb df                	jmp    80100e27 <fileread+0x50>
  panic("fileread");
80100e48:	83 ec 0c             	sub    $0xc,%esp
80100e4b:	68 66 6d 10 80       	push   $0x80106d66
80100e50:	e8 f3 f4 ff ff       	call   80100348 <panic>
    return -1;
80100e55:	be ff ff ff ff       	mov    $0xffffffff,%esi
80100e5a:	eb cb                	jmp    80100e27 <fileread+0x50>

80100e5c <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
80100e5c:	55                   	push   %ebp
80100e5d:	89 e5                	mov    %esp,%ebp
80100e5f:	57                   	push   %edi
80100e60:	56                   	push   %esi
80100e61:	53                   	push   %ebx
80100e62:	83 ec 1c             	sub    $0x1c,%esp
80100e65:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;

  if(f->writable == 0)
80100e68:	80 7b 09 00          	cmpb   $0x0,0x9(%ebx)
80100e6c:	0f 84 c5 00 00 00    	je     80100f37 <filewrite+0xdb>
    return -1;
  if(f->type == FD_PIPE)
80100e72:	8b 03                	mov    (%ebx),%eax
80100e74:	83 f8 01             	cmp    $0x1,%eax
80100e77:	74 10                	je     80100e89 <filewrite+0x2d>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
80100e79:	83 f8 02             	cmp    $0x2,%eax
80100e7c:	0f 85 a8 00 00 00    	jne    80100f2a <filewrite+0xce>
    // i-node, indirect block, allocation blocks,
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
80100e82:	bf 00 00 00 00       	mov    $0x0,%edi
80100e87:	eb 67                	jmp    80100ef0 <filewrite+0x94>
    return pipewrite(f->pipe, addr, n);
80100e89:	83 ec 04             	sub    $0x4,%esp
80100e8c:	ff 75 10             	pushl  0x10(%ebp)
80100e8f:	ff 75 0c             	pushl  0xc(%ebp)
80100e92:	ff 73 0c             	pushl  0xc(%ebx)
80100e95:	e8 6f 20 00 00       	call   80102f09 <pipewrite>
80100e9a:	83 c4 10             	add    $0x10,%esp
80100e9d:	e9 80 00 00 00       	jmp    80100f22 <filewrite+0xc6>
    while(i < n){
      int n1 = n - i;
      if(n1 > max)
        n1 = max;

      begin_op();
80100ea2:	e8 2b 19 00 00       	call   801027d2 <begin_op>
      ilock(f->ip);
80100ea7:	83 ec 0c             	sub    $0xc,%esp
80100eaa:	ff 73 10             	pushl  0x10(%ebx)
80100ead:	e8 bd 06 00 00       	call   8010156f <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80100eb2:	89 f8                	mov    %edi,%eax
80100eb4:	03 45 0c             	add    0xc(%ebp),%eax
80100eb7:	ff 75 e4             	pushl  -0x1c(%ebp)
80100eba:	ff 73 14             	pushl  0x14(%ebx)
80100ebd:	50                   	push   %eax
80100ebe:	ff 73 10             	pushl  0x10(%ebx)
80100ec1:	e8 98 09 00 00       	call   8010185e <writei>
80100ec6:	89 c6                	mov    %eax,%esi
80100ec8:	83 c4 20             	add    $0x20,%esp
80100ecb:	85 c0                	test   %eax,%eax
80100ecd:	7e 03                	jle    80100ed2 <filewrite+0x76>
        f->off += r;
80100ecf:	01 43 14             	add    %eax,0x14(%ebx)
      iunlock(f->ip);
80100ed2:	83 ec 0c             	sub    $0xc,%esp
80100ed5:	ff 73 10             	pushl  0x10(%ebx)
80100ed8:	e8 54 07 00 00       	call   80101631 <iunlock>
      end_op();
80100edd:	e8 6a 19 00 00       	call   8010284c <end_op>

      if(r < 0)
80100ee2:	83 c4 10             	add    $0x10,%esp
80100ee5:	85 f6                	test   %esi,%esi
80100ee7:	78 31                	js     80100f1a <filewrite+0xbe>
        break;
      if(r != n1)
80100ee9:	39 75 e4             	cmp    %esi,-0x1c(%ebp)
80100eec:	75 1f                	jne    80100f0d <filewrite+0xb1>
        panic("short filewrite");
      i += r;
80100eee:	01 f7                	add    %esi,%edi
    while(i < n){
80100ef0:	3b 7d 10             	cmp    0x10(%ebp),%edi
80100ef3:	7d 25                	jge    80100f1a <filewrite+0xbe>
      int n1 = n - i;
80100ef5:	8b 45 10             	mov    0x10(%ebp),%eax
80100ef8:	29 f8                	sub    %edi,%eax
80100efa:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      if(n1 > max)
80100efd:	3d 00 06 00 00       	cmp    $0x600,%eax
80100f02:	7e 9e                	jle    80100ea2 <filewrite+0x46>
        n1 = max;
80100f04:	c7 45 e4 00 06 00 00 	movl   $0x600,-0x1c(%ebp)
80100f0b:	eb 95                	jmp    80100ea2 <filewrite+0x46>
        panic("short filewrite");
80100f0d:	83 ec 0c             	sub    $0xc,%esp
80100f10:	68 6f 6d 10 80       	push   $0x80106d6f
80100f15:	e8 2e f4 ff ff       	call   80100348 <panic>
    }
    return i == n ? n : -1;
80100f1a:	3b 7d 10             	cmp    0x10(%ebp),%edi
80100f1d:	75 1f                	jne    80100f3e <filewrite+0xe2>
80100f1f:	8b 45 10             	mov    0x10(%ebp),%eax
  }
  panic("filewrite");
}
80100f22:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f25:	5b                   	pop    %ebx
80100f26:	5e                   	pop    %esi
80100f27:	5f                   	pop    %edi
80100f28:	5d                   	pop    %ebp
80100f29:	c3                   	ret    
  panic("filewrite");
80100f2a:	83 ec 0c             	sub    $0xc,%esp
80100f2d:	68 75 6d 10 80       	push   $0x80106d75
80100f32:	e8 11 f4 ff ff       	call   80100348 <panic>
    return -1;
80100f37:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100f3c:	eb e4                	jmp    80100f22 <filewrite+0xc6>
    return i == n ? n : -1;
80100f3e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100f43:	eb dd                	jmp    80100f22 <filewrite+0xc6>

80100f45 <skipelem>:
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char*
skipelem(char *path, char *name)
{
80100f45:	55                   	push   %ebp
80100f46:	89 e5                	mov    %esp,%ebp
80100f48:	57                   	push   %edi
80100f49:	56                   	push   %esi
80100f4a:	53                   	push   %ebx
80100f4b:	83 ec 0c             	sub    $0xc,%esp
80100f4e:	89 d7                	mov    %edx,%edi
  char *s;
  int len;

  while(*path == '/')
80100f50:	eb 03                	jmp    80100f55 <skipelem+0x10>
    path++;
80100f52:	83 c0 01             	add    $0x1,%eax
  while(*path == '/')
80100f55:	0f b6 10             	movzbl (%eax),%edx
80100f58:	80 fa 2f             	cmp    $0x2f,%dl
80100f5b:	74 f5                	je     80100f52 <skipelem+0xd>
  if(*path == 0)
80100f5d:	84 d2                	test   %dl,%dl
80100f5f:	74 59                	je     80100fba <skipelem+0x75>
80100f61:	89 c3                	mov    %eax,%ebx
80100f63:	eb 03                	jmp    80100f68 <skipelem+0x23>
    return 0;
  s = path;
  while(*path != '/' && *path != 0)
    path++;
80100f65:	83 c3 01             	add    $0x1,%ebx
  while(*path != '/' && *path != 0)
80100f68:	0f b6 13             	movzbl (%ebx),%edx
80100f6b:	80 fa 2f             	cmp    $0x2f,%dl
80100f6e:	0f 95 c1             	setne  %cl
80100f71:	84 d2                	test   %dl,%dl
80100f73:	0f 95 c2             	setne  %dl
80100f76:	84 d1                	test   %dl,%cl
80100f78:	75 eb                	jne    80100f65 <skipelem+0x20>
  len = path - s;
80100f7a:	89 de                	mov    %ebx,%esi
80100f7c:	29 c6                	sub    %eax,%esi
  if(len >= DIRSIZ)
80100f7e:	83 fe 0d             	cmp    $0xd,%esi
80100f81:	7e 11                	jle    80100f94 <skipelem+0x4f>
    memmove(name, s, DIRSIZ);
80100f83:	83 ec 04             	sub    $0x4,%esp
80100f86:	6a 0e                	push   $0xe
80100f88:	50                   	push   %eax
80100f89:	57                   	push   %edi
80100f8a:	e8 b3 30 00 00       	call   80104042 <memmove>
80100f8f:	83 c4 10             	add    $0x10,%esp
80100f92:	eb 17                	jmp    80100fab <skipelem+0x66>
  else {
    memmove(name, s, len);
80100f94:	83 ec 04             	sub    $0x4,%esp
80100f97:	56                   	push   %esi
80100f98:	50                   	push   %eax
80100f99:	57                   	push   %edi
80100f9a:	e8 a3 30 00 00       	call   80104042 <memmove>
    name[len] = 0;
80100f9f:	c6 04 37 00          	movb   $0x0,(%edi,%esi,1)
80100fa3:	83 c4 10             	add    $0x10,%esp
80100fa6:	eb 03                	jmp    80100fab <skipelem+0x66>
  }
  while(*path == '/')
    path++;
80100fa8:	83 c3 01             	add    $0x1,%ebx
  while(*path == '/')
80100fab:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80100fae:	74 f8                	je     80100fa8 <skipelem+0x63>
  return path;
}
80100fb0:	89 d8                	mov    %ebx,%eax
80100fb2:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100fb5:	5b                   	pop    %ebx
80100fb6:	5e                   	pop    %esi
80100fb7:	5f                   	pop    %edi
80100fb8:	5d                   	pop    %ebp
80100fb9:	c3                   	ret    
    return 0;
80100fba:	bb 00 00 00 00       	mov    $0x0,%ebx
80100fbf:	eb ef                	jmp    80100fb0 <skipelem+0x6b>

80100fc1 <bzero>:
{
80100fc1:	55                   	push   %ebp
80100fc2:	89 e5                	mov    %esp,%ebp
80100fc4:	53                   	push   %ebx
80100fc5:	83 ec 0c             	sub    $0xc,%esp
  bp = bread(dev, bno);
80100fc8:	52                   	push   %edx
80100fc9:	50                   	push   %eax
80100fca:	e8 9d f1 ff ff       	call   8010016c <bread>
80100fcf:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
80100fd1:	8d 40 5c             	lea    0x5c(%eax),%eax
80100fd4:	83 c4 0c             	add    $0xc,%esp
80100fd7:	68 00 02 00 00       	push   $0x200
80100fdc:	6a 00                	push   $0x0
80100fde:	50                   	push   %eax
80100fdf:	e8 e3 2f 00 00       	call   80103fc7 <memset>
  log_write(bp);
80100fe4:	89 1c 24             	mov    %ebx,(%esp)
80100fe7:	e8 0f 19 00 00       	call   801028fb <log_write>
  brelse(bp);
80100fec:	89 1c 24             	mov    %ebx,(%esp)
80100fef:	e8 e1 f1 ff ff       	call   801001d5 <brelse>
}
80100ff4:	83 c4 10             	add    $0x10,%esp
80100ff7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100ffa:	c9                   	leave  
80100ffb:	c3                   	ret    

80100ffc <bfree>:
{
80100ffc:	55                   	push   %ebp
80100ffd:	89 e5                	mov    %esp,%ebp
80100fff:	56                   	push   %esi
80101000:	53                   	push   %ebx
80101001:	89 d3                	mov    %edx,%ebx
  bp = bread(dev, BBLOCK(b, sb));
80101003:	c1 ea 0c             	shr    $0xc,%edx
80101006:	03 15 f8 09 11 80    	add    0x801109f8,%edx
8010100c:	83 ec 08             	sub    $0x8,%esp
8010100f:	52                   	push   %edx
80101010:	50                   	push   %eax
80101011:	e8 56 f1 ff ff       	call   8010016c <bread>
80101016:	89 c6                	mov    %eax,%esi
  m = 1 << (bi % 8);
80101018:	89 d9                	mov    %ebx,%ecx
8010101a:	83 e1 07             	and    $0x7,%ecx
8010101d:	b8 01 00 00 00       	mov    $0x1,%eax
80101022:	d3 e0                	shl    %cl,%eax
  if((bp->data[bi/8] & m) == 0)
80101024:	83 c4 10             	add    $0x10,%esp
80101027:	81 e3 ff 0f 00 00    	and    $0xfff,%ebx
8010102d:	c1 fb 03             	sar    $0x3,%ebx
80101030:	0f b6 54 1e 5c       	movzbl 0x5c(%esi,%ebx,1),%edx
80101035:	0f b6 ca             	movzbl %dl,%ecx
80101038:	85 c1                	test   %eax,%ecx
8010103a:	74 23                	je     8010105f <bfree+0x63>
  bp->data[bi/8] &= ~m;
8010103c:	f7 d0                	not    %eax
8010103e:	21 d0                	and    %edx,%eax
80101040:	88 44 1e 5c          	mov    %al,0x5c(%esi,%ebx,1)
  log_write(bp);
80101044:	83 ec 0c             	sub    $0xc,%esp
80101047:	56                   	push   %esi
80101048:	e8 ae 18 00 00       	call   801028fb <log_write>
  brelse(bp);
8010104d:	89 34 24             	mov    %esi,(%esp)
80101050:	e8 80 f1 ff ff       	call   801001d5 <brelse>
}
80101055:	83 c4 10             	add    $0x10,%esp
80101058:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010105b:	5b                   	pop    %ebx
8010105c:	5e                   	pop    %esi
8010105d:	5d                   	pop    %ebp
8010105e:	c3                   	ret    
    panic("freeing free block");
8010105f:	83 ec 0c             	sub    $0xc,%esp
80101062:	68 7f 6d 10 80       	push   $0x80106d7f
80101067:	e8 dc f2 ff ff       	call   80100348 <panic>

8010106c <balloc>:
{
8010106c:	55                   	push   %ebp
8010106d:	89 e5                	mov    %esp,%ebp
8010106f:	57                   	push   %edi
80101070:	56                   	push   %esi
80101071:	53                   	push   %ebx
80101072:	83 ec 1c             	sub    $0x1c,%esp
80101075:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
80101078:	be 00 00 00 00       	mov    $0x0,%esi
8010107d:	eb 14                	jmp    80101093 <balloc+0x27>
    brelse(bp);
8010107f:	83 ec 0c             	sub    $0xc,%esp
80101082:	ff 75 e4             	pushl  -0x1c(%ebp)
80101085:	e8 4b f1 ff ff       	call   801001d5 <brelse>
  for(b = 0; b < sb.size; b += BPB){
8010108a:	81 c6 00 10 00 00    	add    $0x1000,%esi
80101090:	83 c4 10             	add    $0x10,%esp
80101093:	39 35 e0 09 11 80    	cmp    %esi,0x801109e0
80101099:	76 75                	jbe    80101110 <balloc+0xa4>
    bp = bread(dev, BBLOCK(b, sb));
8010109b:	8d 86 ff 0f 00 00    	lea    0xfff(%esi),%eax
801010a1:	85 f6                	test   %esi,%esi
801010a3:	0f 49 c6             	cmovns %esi,%eax
801010a6:	c1 f8 0c             	sar    $0xc,%eax
801010a9:	03 05 f8 09 11 80    	add    0x801109f8,%eax
801010af:	83 ec 08             	sub    $0x8,%esp
801010b2:	50                   	push   %eax
801010b3:	ff 75 d8             	pushl  -0x28(%ebp)
801010b6:	e8 b1 f0 ff ff       	call   8010016c <bread>
801010bb:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
801010be:	83 c4 10             	add    $0x10,%esp
801010c1:	b8 00 00 00 00       	mov    $0x0,%eax
801010c6:	3d ff 0f 00 00       	cmp    $0xfff,%eax
801010cb:	7f b2                	jg     8010107f <balloc+0x13>
801010cd:	8d 1c 06             	lea    (%esi,%eax,1),%ebx
801010d0:	89 5d e0             	mov    %ebx,-0x20(%ebp)
801010d3:	3b 1d e0 09 11 80    	cmp    0x801109e0,%ebx
801010d9:	73 a4                	jae    8010107f <balloc+0x13>
      m = 1 << (bi % 8);
801010db:	99                   	cltd   
801010dc:	c1 ea 1d             	shr    $0x1d,%edx
801010df:	8d 0c 10             	lea    (%eax,%edx,1),%ecx
801010e2:	83 e1 07             	and    $0x7,%ecx
801010e5:	29 d1                	sub    %edx,%ecx
801010e7:	ba 01 00 00 00       	mov    $0x1,%edx
801010ec:	d3 e2                	shl    %cl,%edx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
801010ee:	8d 48 07             	lea    0x7(%eax),%ecx
801010f1:	85 c0                	test   %eax,%eax
801010f3:	0f 49 c8             	cmovns %eax,%ecx
801010f6:	c1 f9 03             	sar    $0x3,%ecx
801010f9:	89 4d dc             	mov    %ecx,-0x24(%ebp)
801010fc:	8b 7d e4             	mov    -0x1c(%ebp),%edi
801010ff:	0f b6 4c 0f 5c       	movzbl 0x5c(%edi,%ecx,1),%ecx
80101104:	0f b6 f9             	movzbl %cl,%edi
80101107:	85 d7                	test   %edx,%edi
80101109:	74 12                	je     8010111d <balloc+0xb1>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010110b:	83 c0 01             	add    $0x1,%eax
8010110e:	eb b6                	jmp    801010c6 <balloc+0x5a>
  panic("balloc: out of blocks");
80101110:	83 ec 0c             	sub    $0xc,%esp
80101113:	68 92 6d 10 80       	push   $0x80106d92
80101118:	e8 2b f2 ff ff       	call   80100348 <panic>
        bp->data[bi/8] |= m;  // Mark block in use.
8010111d:	09 ca                	or     %ecx,%edx
8010111f:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101122:	8b 75 dc             	mov    -0x24(%ebp),%esi
80101125:	88 54 30 5c          	mov    %dl,0x5c(%eax,%esi,1)
        log_write(bp);
80101129:	83 ec 0c             	sub    $0xc,%esp
8010112c:	89 c6                	mov    %eax,%esi
8010112e:	50                   	push   %eax
8010112f:	e8 c7 17 00 00       	call   801028fb <log_write>
        brelse(bp);
80101134:	89 34 24             	mov    %esi,(%esp)
80101137:	e8 99 f0 ff ff       	call   801001d5 <brelse>
        bzero(dev, b + bi);
8010113c:	89 da                	mov    %ebx,%edx
8010113e:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101141:	e8 7b fe ff ff       	call   80100fc1 <bzero>
}
80101146:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101149:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010114c:	5b                   	pop    %ebx
8010114d:	5e                   	pop    %esi
8010114e:	5f                   	pop    %edi
8010114f:	5d                   	pop    %ebp
80101150:	c3                   	ret    

80101151 <bmap>:
{
80101151:	55                   	push   %ebp
80101152:	89 e5                	mov    %esp,%ebp
80101154:	57                   	push   %edi
80101155:	56                   	push   %esi
80101156:	53                   	push   %ebx
80101157:	83 ec 1c             	sub    $0x1c,%esp
8010115a:	89 c6                	mov    %eax,%esi
8010115c:	89 d7                	mov    %edx,%edi
  if(bn < NDIRECT){
8010115e:	83 fa 0b             	cmp    $0xb,%edx
80101161:	77 17                	ja     8010117a <bmap+0x29>
    if((addr = ip->addrs[bn]) == 0)
80101163:	8b 5c 90 5c          	mov    0x5c(%eax,%edx,4),%ebx
80101167:	85 db                	test   %ebx,%ebx
80101169:	75 4a                	jne    801011b5 <bmap+0x64>
      ip->addrs[bn] = addr = balloc(ip->dev);
8010116b:	8b 00                	mov    (%eax),%eax
8010116d:	e8 fa fe ff ff       	call   8010106c <balloc>
80101172:	89 c3                	mov    %eax,%ebx
80101174:	89 44 be 5c          	mov    %eax,0x5c(%esi,%edi,4)
80101178:	eb 3b                	jmp    801011b5 <bmap+0x64>
  bn -= NDIRECT;
8010117a:	8d 5a f4             	lea    -0xc(%edx),%ebx
  if(bn < NINDIRECT){
8010117d:	83 fb 7f             	cmp    $0x7f,%ebx
80101180:	77 68                	ja     801011ea <bmap+0x99>
    if((addr = ip->addrs[NDIRECT]) == 0)
80101182:	8b 80 8c 00 00 00    	mov    0x8c(%eax),%eax
80101188:	85 c0                	test   %eax,%eax
8010118a:	74 33                	je     801011bf <bmap+0x6e>
    bp = bread(ip->dev, addr);
8010118c:	83 ec 08             	sub    $0x8,%esp
8010118f:	50                   	push   %eax
80101190:	ff 36                	pushl  (%esi)
80101192:	e8 d5 ef ff ff       	call   8010016c <bread>
80101197:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
80101199:	8d 44 98 5c          	lea    0x5c(%eax,%ebx,4),%eax
8010119d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801011a0:	8b 18                	mov    (%eax),%ebx
801011a2:	83 c4 10             	add    $0x10,%esp
801011a5:	85 db                	test   %ebx,%ebx
801011a7:	74 25                	je     801011ce <bmap+0x7d>
    brelse(bp);
801011a9:	83 ec 0c             	sub    $0xc,%esp
801011ac:	57                   	push   %edi
801011ad:	e8 23 f0 ff ff       	call   801001d5 <brelse>
    return addr;
801011b2:	83 c4 10             	add    $0x10,%esp
}
801011b5:	89 d8                	mov    %ebx,%eax
801011b7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801011ba:	5b                   	pop    %ebx
801011bb:	5e                   	pop    %esi
801011bc:	5f                   	pop    %edi
801011bd:	5d                   	pop    %ebp
801011be:	c3                   	ret    
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
801011bf:	8b 06                	mov    (%esi),%eax
801011c1:	e8 a6 fe ff ff       	call   8010106c <balloc>
801011c6:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
801011cc:	eb be                	jmp    8010118c <bmap+0x3b>
      a[bn] = addr = balloc(ip->dev);
801011ce:	8b 06                	mov    (%esi),%eax
801011d0:	e8 97 fe ff ff       	call   8010106c <balloc>
801011d5:	89 c3                	mov    %eax,%ebx
801011d7:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801011da:	89 18                	mov    %ebx,(%eax)
      log_write(bp);
801011dc:	83 ec 0c             	sub    $0xc,%esp
801011df:	57                   	push   %edi
801011e0:	e8 16 17 00 00       	call   801028fb <log_write>
801011e5:	83 c4 10             	add    $0x10,%esp
801011e8:	eb bf                	jmp    801011a9 <bmap+0x58>
  panic("bmap: out of range");
801011ea:	83 ec 0c             	sub    $0xc,%esp
801011ed:	68 a8 6d 10 80       	push   $0x80106da8
801011f2:	e8 51 f1 ff ff       	call   80100348 <panic>

801011f7 <iget>:
{
801011f7:	55                   	push   %ebp
801011f8:	89 e5                	mov    %esp,%ebp
801011fa:	57                   	push   %edi
801011fb:	56                   	push   %esi
801011fc:	53                   	push   %ebx
801011fd:	83 ec 28             	sub    $0x28,%esp
80101200:	89 c7                	mov    %eax,%edi
80101202:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
80101205:	68 00 0a 11 80       	push   $0x80110a00
8010120a:	e8 0c 2d 00 00       	call   80103f1b <acquire>
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010120f:	83 c4 10             	add    $0x10,%esp
  empty = 0;
80101212:	be 00 00 00 00       	mov    $0x0,%esi
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101217:	bb 34 0a 11 80       	mov    $0x80110a34,%ebx
8010121c:	eb 0a                	jmp    80101228 <iget+0x31>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
8010121e:	85 f6                	test   %esi,%esi
80101220:	74 3b                	je     8010125d <iget+0x66>
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101222:	81 c3 90 00 00 00    	add    $0x90,%ebx
80101228:	81 fb 54 26 11 80    	cmp    $0x80112654,%ebx
8010122e:	73 35                	jae    80101265 <iget+0x6e>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101230:	8b 43 08             	mov    0x8(%ebx),%eax
80101233:	85 c0                	test   %eax,%eax
80101235:	7e e7                	jle    8010121e <iget+0x27>
80101237:	39 3b                	cmp    %edi,(%ebx)
80101239:	75 e3                	jne    8010121e <iget+0x27>
8010123b:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
8010123e:	39 4b 04             	cmp    %ecx,0x4(%ebx)
80101241:	75 db                	jne    8010121e <iget+0x27>
      ip->ref++;
80101243:	83 c0 01             	add    $0x1,%eax
80101246:	89 43 08             	mov    %eax,0x8(%ebx)
      release(&icache.lock);
80101249:	83 ec 0c             	sub    $0xc,%esp
8010124c:	68 00 0a 11 80       	push   $0x80110a00
80101251:	e8 2a 2d 00 00       	call   80103f80 <release>
      return ip;
80101256:	83 c4 10             	add    $0x10,%esp
80101259:	89 de                	mov    %ebx,%esi
8010125b:	eb 32                	jmp    8010128f <iget+0x98>
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
8010125d:	85 c0                	test   %eax,%eax
8010125f:	75 c1                	jne    80101222 <iget+0x2b>
      empty = ip;
80101261:	89 de                	mov    %ebx,%esi
80101263:	eb bd                	jmp    80101222 <iget+0x2b>
  if(empty == 0)
80101265:	85 f6                	test   %esi,%esi
80101267:	74 30                	je     80101299 <iget+0xa2>
  ip->dev = dev;
80101269:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
8010126b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010126e:	89 46 04             	mov    %eax,0x4(%esi)
  ip->ref = 1;
80101271:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
80101278:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
8010127f:	83 ec 0c             	sub    $0xc,%esp
80101282:	68 00 0a 11 80       	push   $0x80110a00
80101287:	e8 f4 2c 00 00       	call   80103f80 <release>
  return ip;
8010128c:	83 c4 10             	add    $0x10,%esp
}
8010128f:	89 f0                	mov    %esi,%eax
80101291:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101294:	5b                   	pop    %ebx
80101295:	5e                   	pop    %esi
80101296:	5f                   	pop    %edi
80101297:	5d                   	pop    %ebp
80101298:	c3                   	ret    
    panic("iget: no inodes");
80101299:	83 ec 0c             	sub    $0xc,%esp
8010129c:	68 bb 6d 10 80       	push   $0x80106dbb
801012a1:	e8 a2 f0 ff ff       	call   80100348 <panic>

801012a6 <readsb>:
{
801012a6:	55                   	push   %ebp
801012a7:	89 e5                	mov    %esp,%ebp
801012a9:	53                   	push   %ebx
801012aa:	83 ec 0c             	sub    $0xc,%esp
  bp = bread(dev, 1);
801012ad:	6a 01                	push   $0x1
801012af:	ff 75 08             	pushl  0x8(%ebp)
801012b2:	e8 b5 ee ff ff       	call   8010016c <bread>
801012b7:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
801012b9:	8d 40 5c             	lea    0x5c(%eax),%eax
801012bc:	83 c4 0c             	add    $0xc,%esp
801012bf:	6a 1c                	push   $0x1c
801012c1:	50                   	push   %eax
801012c2:	ff 75 0c             	pushl  0xc(%ebp)
801012c5:	e8 78 2d 00 00       	call   80104042 <memmove>
  brelse(bp);
801012ca:	89 1c 24             	mov    %ebx,(%esp)
801012cd:	e8 03 ef ff ff       	call   801001d5 <brelse>
}
801012d2:	83 c4 10             	add    $0x10,%esp
801012d5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801012d8:	c9                   	leave  
801012d9:	c3                   	ret    

801012da <iinit>:
{
801012da:	55                   	push   %ebp
801012db:	89 e5                	mov    %esp,%ebp
801012dd:	53                   	push   %ebx
801012de:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
801012e1:	68 cb 6d 10 80       	push   $0x80106dcb
801012e6:	68 00 0a 11 80       	push   $0x80110a00
801012eb:	e8 ef 2a 00 00       	call   80103ddf <initlock>
  for(i = 0; i < NINODE; i++) {
801012f0:	83 c4 10             	add    $0x10,%esp
801012f3:	bb 00 00 00 00       	mov    $0x0,%ebx
801012f8:	eb 21                	jmp    8010131b <iinit+0x41>
    initsleeplock(&icache.inode[i].lock, "inode");
801012fa:	83 ec 08             	sub    $0x8,%esp
801012fd:	68 d2 6d 10 80       	push   $0x80106dd2
80101302:	8d 14 db             	lea    (%ebx,%ebx,8),%edx
80101305:	89 d0                	mov    %edx,%eax
80101307:	c1 e0 04             	shl    $0x4,%eax
8010130a:	05 40 0a 11 80       	add    $0x80110a40,%eax
8010130f:	50                   	push   %eax
80101310:	e8 bf 29 00 00       	call   80103cd4 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
80101315:	83 c3 01             	add    $0x1,%ebx
80101318:	83 c4 10             	add    $0x10,%esp
8010131b:	83 fb 31             	cmp    $0x31,%ebx
8010131e:	7e da                	jle    801012fa <iinit+0x20>
  readsb(dev, &sb);
80101320:	83 ec 08             	sub    $0x8,%esp
80101323:	68 e0 09 11 80       	push   $0x801109e0
80101328:	ff 75 08             	pushl  0x8(%ebp)
8010132b:	e8 76 ff ff ff       	call   801012a6 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
80101330:	ff 35 f8 09 11 80    	pushl  0x801109f8
80101336:	ff 35 f4 09 11 80    	pushl  0x801109f4
8010133c:	ff 35 f0 09 11 80    	pushl  0x801109f0
80101342:	ff 35 ec 09 11 80    	pushl  0x801109ec
80101348:	ff 35 e8 09 11 80    	pushl  0x801109e8
8010134e:	ff 35 e4 09 11 80    	pushl  0x801109e4
80101354:	ff 35 e0 09 11 80    	pushl  0x801109e0
8010135a:	68 38 6e 10 80       	push   $0x80106e38
8010135f:	e8 a7 f2 ff ff       	call   8010060b <cprintf>
}
80101364:	83 c4 30             	add    $0x30,%esp
80101367:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010136a:	c9                   	leave  
8010136b:	c3                   	ret    

8010136c <ialloc>:
{
8010136c:	55                   	push   %ebp
8010136d:	89 e5                	mov    %esp,%ebp
8010136f:	57                   	push   %edi
80101370:	56                   	push   %esi
80101371:	53                   	push   %ebx
80101372:	83 ec 1c             	sub    $0x1c,%esp
80101375:	8b 45 0c             	mov    0xc(%ebp),%eax
80101378:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
8010137b:	bb 01 00 00 00       	mov    $0x1,%ebx
80101380:	89 5d e4             	mov    %ebx,-0x1c(%ebp)
80101383:	39 1d e8 09 11 80    	cmp    %ebx,0x801109e8
80101389:	76 3f                	jbe    801013ca <ialloc+0x5e>
    bp = bread(dev, IBLOCK(inum, sb));
8010138b:	89 d8                	mov    %ebx,%eax
8010138d:	c1 e8 03             	shr    $0x3,%eax
80101390:	03 05 f4 09 11 80    	add    0x801109f4,%eax
80101396:	83 ec 08             	sub    $0x8,%esp
80101399:	50                   	push   %eax
8010139a:	ff 75 08             	pushl  0x8(%ebp)
8010139d:	e8 ca ed ff ff       	call   8010016c <bread>
801013a2:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + inum%IPB;
801013a4:	89 d8                	mov    %ebx,%eax
801013a6:	83 e0 07             	and    $0x7,%eax
801013a9:	c1 e0 06             	shl    $0x6,%eax
801013ac:	8d 7c 06 5c          	lea    0x5c(%esi,%eax,1),%edi
    if(dip->type == 0){  // a free inode
801013b0:	83 c4 10             	add    $0x10,%esp
801013b3:	66 83 3f 00          	cmpw   $0x0,(%edi)
801013b7:	74 1e                	je     801013d7 <ialloc+0x6b>
    brelse(bp);
801013b9:	83 ec 0c             	sub    $0xc,%esp
801013bc:	56                   	push   %esi
801013bd:	e8 13 ee ff ff       	call   801001d5 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
801013c2:	83 c3 01             	add    $0x1,%ebx
801013c5:	83 c4 10             	add    $0x10,%esp
801013c8:	eb b6                	jmp    80101380 <ialloc+0x14>
  panic("ialloc: no inodes");
801013ca:	83 ec 0c             	sub    $0xc,%esp
801013cd:	68 d8 6d 10 80       	push   $0x80106dd8
801013d2:	e8 71 ef ff ff       	call   80100348 <panic>
      memset(dip, 0, sizeof(*dip));
801013d7:	83 ec 04             	sub    $0x4,%esp
801013da:	6a 40                	push   $0x40
801013dc:	6a 00                	push   $0x0
801013de:	57                   	push   %edi
801013df:	e8 e3 2b 00 00       	call   80103fc7 <memset>
      dip->type = type;
801013e4:	0f b7 45 e0          	movzwl -0x20(%ebp),%eax
801013e8:	66 89 07             	mov    %ax,(%edi)
      log_write(bp);   // mark it allocated on the disk
801013eb:	89 34 24             	mov    %esi,(%esp)
801013ee:	e8 08 15 00 00       	call   801028fb <log_write>
      brelse(bp);
801013f3:	89 34 24             	mov    %esi,(%esp)
801013f6:	e8 da ed ff ff       	call   801001d5 <brelse>
      return iget(dev, inum);
801013fb:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801013fe:	8b 45 08             	mov    0x8(%ebp),%eax
80101401:	e8 f1 fd ff ff       	call   801011f7 <iget>
}
80101406:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101409:	5b                   	pop    %ebx
8010140a:	5e                   	pop    %esi
8010140b:	5f                   	pop    %edi
8010140c:	5d                   	pop    %ebp
8010140d:	c3                   	ret    

8010140e <iupdate>:
{
8010140e:	55                   	push   %ebp
8010140f:	89 e5                	mov    %esp,%ebp
80101411:	56                   	push   %esi
80101412:	53                   	push   %ebx
80101413:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101416:	8b 43 04             	mov    0x4(%ebx),%eax
80101419:	c1 e8 03             	shr    $0x3,%eax
8010141c:	03 05 f4 09 11 80    	add    0x801109f4,%eax
80101422:	83 ec 08             	sub    $0x8,%esp
80101425:	50                   	push   %eax
80101426:	ff 33                	pushl  (%ebx)
80101428:	e8 3f ed ff ff       	call   8010016c <bread>
8010142d:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
8010142f:	8b 43 04             	mov    0x4(%ebx),%eax
80101432:	83 e0 07             	and    $0x7,%eax
80101435:	c1 e0 06             	shl    $0x6,%eax
80101438:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
8010143c:	0f b7 53 50          	movzwl 0x50(%ebx),%edx
80101440:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
80101443:	0f b7 53 52          	movzwl 0x52(%ebx),%edx
80101447:	66 89 50 02          	mov    %dx,0x2(%eax)
  dip->minor = ip->minor;
8010144b:	0f b7 53 54          	movzwl 0x54(%ebx),%edx
8010144f:	66 89 50 04          	mov    %dx,0x4(%eax)
  dip->nlink = ip->nlink;
80101453:	0f b7 53 56          	movzwl 0x56(%ebx),%edx
80101457:	66 89 50 06          	mov    %dx,0x6(%eax)
  dip->size = ip->size;
8010145b:	8b 53 58             	mov    0x58(%ebx),%edx
8010145e:	89 50 08             	mov    %edx,0x8(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
80101461:	83 c3 5c             	add    $0x5c,%ebx
80101464:	83 c0 0c             	add    $0xc,%eax
80101467:	83 c4 0c             	add    $0xc,%esp
8010146a:	6a 34                	push   $0x34
8010146c:	53                   	push   %ebx
8010146d:	50                   	push   %eax
8010146e:	e8 cf 2b 00 00       	call   80104042 <memmove>
  log_write(bp);
80101473:	89 34 24             	mov    %esi,(%esp)
80101476:	e8 80 14 00 00       	call   801028fb <log_write>
  brelse(bp);
8010147b:	89 34 24             	mov    %esi,(%esp)
8010147e:	e8 52 ed ff ff       	call   801001d5 <brelse>
}
80101483:	83 c4 10             	add    $0x10,%esp
80101486:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101489:	5b                   	pop    %ebx
8010148a:	5e                   	pop    %esi
8010148b:	5d                   	pop    %ebp
8010148c:	c3                   	ret    

8010148d <itrunc>:
{
8010148d:	55                   	push   %ebp
8010148e:	89 e5                	mov    %esp,%ebp
80101490:	57                   	push   %edi
80101491:	56                   	push   %esi
80101492:	53                   	push   %ebx
80101493:	83 ec 1c             	sub    $0x1c,%esp
80101496:	89 c6                	mov    %eax,%esi
  for(i = 0; i < NDIRECT; i++){
80101498:	bb 00 00 00 00       	mov    $0x0,%ebx
8010149d:	eb 03                	jmp    801014a2 <itrunc+0x15>
8010149f:	83 c3 01             	add    $0x1,%ebx
801014a2:	83 fb 0b             	cmp    $0xb,%ebx
801014a5:	7f 19                	jg     801014c0 <itrunc+0x33>
    if(ip->addrs[i]){
801014a7:	8b 54 9e 5c          	mov    0x5c(%esi,%ebx,4),%edx
801014ab:	85 d2                	test   %edx,%edx
801014ad:	74 f0                	je     8010149f <itrunc+0x12>
      bfree(ip->dev, ip->addrs[i]);
801014af:	8b 06                	mov    (%esi),%eax
801014b1:	e8 46 fb ff ff       	call   80100ffc <bfree>
      ip->addrs[i] = 0;
801014b6:	c7 44 9e 5c 00 00 00 	movl   $0x0,0x5c(%esi,%ebx,4)
801014bd:	00 
801014be:	eb df                	jmp    8010149f <itrunc+0x12>
  if(ip->addrs[NDIRECT]){
801014c0:	8b 86 8c 00 00 00    	mov    0x8c(%esi),%eax
801014c6:	85 c0                	test   %eax,%eax
801014c8:	75 1b                	jne    801014e5 <itrunc+0x58>
  ip->size = 0;
801014ca:	c7 46 58 00 00 00 00 	movl   $0x0,0x58(%esi)
  iupdate(ip);
801014d1:	83 ec 0c             	sub    $0xc,%esp
801014d4:	56                   	push   %esi
801014d5:	e8 34 ff ff ff       	call   8010140e <iupdate>
}
801014da:	83 c4 10             	add    $0x10,%esp
801014dd:	8d 65 f4             	lea    -0xc(%ebp),%esp
801014e0:	5b                   	pop    %ebx
801014e1:	5e                   	pop    %esi
801014e2:	5f                   	pop    %edi
801014e3:	5d                   	pop    %ebp
801014e4:	c3                   	ret    
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
801014e5:	83 ec 08             	sub    $0x8,%esp
801014e8:	50                   	push   %eax
801014e9:	ff 36                	pushl  (%esi)
801014eb:	e8 7c ec ff ff       	call   8010016c <bread>
801014f0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    a = (uint*)bp->data;
801014f3:	8d 78 5c             	lea    0x5c(%eax),%edi
    for(j = 0; j < NINDIRECT; j++){
801014f6:	83 c4 10             	add    $0x10,%esp
801014f9:	bb 00 00 00 00       	mov    $0x0,%ebx
801014fe:	eb 03                	jmp    80101503 <itrunc+0x76>
80101500:	83 c3 01             	add    $0x1,%ebx
80101503:	83 fb 7f             	cmp    $0x7f,%ebx
80101506:	77 10                	ja     80101518 <itrunc+0x8b>
      if(a[j])
80101508:	8b 14 9f             	mov    (%edi,%ebx,4),%edx
8010150b:	85 d2                	test   %edx,%edx
8010150d:	74 f1                	je     80101500 <itrunc+0x73>
        bfree(ip->dev, a[j]);
8010150f:	8b 06                	mov    (%esi),%eax
80101511:	e8 e6 fa ff ff       	call   80100ffc <bfree>
80101516:	eb e8                	jmp    80101500 <itrunc+0x73>
    brelse(bp);
80101518:	83 ec 0c             	sub    $0xc,%esp
8010151b:	ff 75 e4             	pushl  -0x1c(%ebp)
8010151e:	e8 b2 ec ff ff       	call   801001d5 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
80101523:	8b 06                	mov    (%esi),%eax
80101525:	8b 96 8c 00 00 00    	mov    0x8c(%esi),%edx
8010152b:	e8 cc fa ff ff       	call   80100ffc <bfree>
    ip->addrs[NDIRECT] = 0;
80101530:	c7 86 8c 00 00 00 00 	movl   $0x0,0x8c(%esi)
80101537:	00 00 00 
8010153a:	83 c4 10             	add    $0x10,%esp
8010153d:	eb 8b                	jmp    801014ca <itrunc+0x3d>

8010153f <idup>:
{
8010153f:	55                   	push   %ebp
80101540:	89 e5                	mov    %esp,%ebp
80101542:	53                   	push   %ebx
80101543:	83 ec 10             	sub    $0x10,%esp
80101546:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
80101549:	68 00 0a 11 80       	push   $0x80110a00
8010154e:	e8 c8 29 00 00       	call   80103f1b <acquire>
  ip->ref++;
80101553:	8b 43 08             	mov    0x8(%ebx),%eax
80101556:	83 c0 01             	add    $0x1,%eax
80101559:	89 43 08             	mov    %eax,0x8(%ebx)
  release(&icache.lock);
8010155c:	c7 04 24 00 0a 11 80 	movl   $0x80110a00,(%esp)
80101563:	e8 18 2a 00 00       	call   80103f80 <release>
}
80101568:	89 d8                	mov    %ebx,%eax
8010156a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010156d:	c9                   	leave  
8010156e:	c3                   	ret    

8010156f <ilock>:
{
8010156f:	55                   	push   %ebp
80101570:	89 e5                	mov    %esp,%ebp
80101572:	56                   	push   %esi
80101573:	53                   	push   %ebx
80101574:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101577:	85 db                	test   %ebx,%ebx
80101579:	74 22                	je     8010159d <ilock+0x2e>
8010157b:	83 7b 08 00          	cmpl   $0x0,0x8(%ebx)
8010157f:	7e 1c                	jle    8010159d <ilock+0x2e>
  acquiresleep(&ip->lock);
80101581:	83 ec 0c             	sub    $0xc,%esp
80101584:	8d 43 0c             	lea    0xc(%ebx),%eax
80101587:	50                   	push   %eax
80101588:	e8 7a 27 00 00       	call   80103d07 <acquiresleep>
  if(ip->valid == 0){
8010158d:	83 c4 10             	add    $0x10,%esp
80101590:	83 7b 4c 00          	cmpl   $0x0,0x4c(%ebx)
80101594:	74 14                	je     801015aa <ilock+0x3b>
}
80101596:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101599:	5b                   	pop    %ebx
8010159a:	5e                   	pop    %esi
8010159b:	5d                   	pop    %ebp
8010159c:	c3                   	ret    
    panic("ilock");
8010159d:	83 ec 0c             	sub    $0xc,%esp
801015a0:	68 ea 6d 10 80       	push   $0x80106dea
801015a5:	e8 9e ed ff ff       	call   80100348 <panic>
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801015aa:	8b 43 04             	mov    0x4(%ebx),%eax
801015ad:	c1 e8 03             	shr    $0x3,%eax
801015b0:	03 05 f4 09 11 80    	add    0x801109f4,%eax
801015b6:	83 ec 08             	sub    $0x8,%esp
801015b9:	50                   	push   %eax
801015ba:	ff 33                	pushl  (%ebx)
801015bc:	e8 ab eb ff ff       	call   8010016c <bread>
801015c1:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801015c3:	8b 43 04             	mov    0x4(%ebx),%eax
801015c6:	83 e0 07             	and    $0x7,%eax
801015c9:	c1 e0 06             	shl    $0x6,%eax
801015cc:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
801015d0:	0f b7 10             	movzwl (%eax),%edx
801015d3:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
801015d7:	0f b7 50 02          	movzwl 0x2(%eax),%edx
801015db:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
801015df:	0f b7 50 04          	movzwl 0x4(%eax),%edx
801015e3:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
801015e7:	0f b7 50 06          	movzwl 0x6(%eax),%edx
801015eb:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
801015ef:	8b 50 08             	mov    0x8(%eax),%edx
801015f2:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801015f5:	83 c0 0c             	add    $0xc,%eax
801015f8:	8d 53 5c             	lea    0x5c(%ebx),%edx
801015fb:	83 c4 0c             	add    $0xc,%esp
801015fe:	6a 34                	push   $0x34
80101600:	50                   	push   %eax
80101601:	52                   	push   %edx
80101602:	e8 3b 2a 00 00       	call   80104042 <memmove>
    brelse(bp);
80101607:	89 34 24             	mov    %esi,(%esp)
8010160a:	e8 c6 eb ff ff       	call   801001d5 <brelse>
    ip->valid = 1;
8010160f:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101616:	83 c4 10             	add    $0x10,%esp
80101619:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
8010161e:	0f 85 72 ff ff ff    	jne    80101596 <ilock+0x27>
      panic("ilock: no type");
80101624:	83 ec 0c             	sub    $0xc,%esp
80101627:	68 f0 6d 10 80       	push   $0x80106df0
8010162c:	e8 17 ed ff ff       	call   80100348 <panic>

80101631 <iunlock>:
{
80101631:	55                   	push   %ebp
80101632:	89 e5                	mov    %esp,%ebp
80101634:	56                   	push   %esi
80101635:	53                   	push   %ebx
80101636:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80101639:	85 db                	test   %ebx,%ebx
8010163b:	74 2c                	je     80101669 <iunlock+0x38>
8010163d:	8d 73 0c             	lea    0xc(%ebx),%esi
80101640:	83 ec 0c             	sub    $0xc,%esp
80101643:	56                   	push   %esi
80101644:	e8 48 27 00 00       	call   80103d91 <holdingsleep>
80101649:	83 c4 10             	add    $0x10,%esp
8010164c:	85 c0                	test   %eax,%eax
8010164e:	74 19                	je     80101669 <iunlock+0x38>
80101650:	83 7b 08 00          	cmpl   $0x0,0x8(%ebx)
80101654:	7e 13                	jle    80101669 <iunlock+0x38>
  releasesleep(&ip->lock);
80101656:	83 ec 0c             	sub    $0xc,%esp
80101659:	56                   	push   %esi
8010165a:	e8 f7 26 00 00       	call   80103d56 <releasesleep>
}
8010165f:	83 c4 10             	add    $0x10,%esp
80101662:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101665:	5b                   	pop    %ebx
80101666:	5e                   	pop    %esi
80101667:	5d                   	pop    %ebp
80101668:	c3                   	ret    
    panic("iunlock");
80101669:	83 ec 0c             	sub    $0xc,%esp
8010166c:	68 ff 6d 10 80       	push   $0x80106dff
80101671:	e8 d2 ec ff ff       	call   80100348 <panic>

80101676 <iput>:
{
80101676:	55                   	push   %ebp
80101677:	89 e5                	mov    %esp,%ebp
80101679:	57                   	push   %edi
8010167a:	56                   	push   %esi
8010167b:	53                   	push   %ebx
8010167c:	83 ec 18             	sub    $0x18,%esp
8010167f:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
80101682:	8d 73 0c             	lea    0xc(%ebx),%esi
80101685:	56                   	push   %esi
80101686:	e8 7c 26 00 00       	call   80103d07 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
8010168b:	83 c4 10             	add    $0x10,%esp
8010168e:	83 7b 4c 00          	cmpl   $0x0,0x4c(%ebx)
80101692:	74 07                	je     8010169b <iput+0x25>
80101694:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80101699:	74 35                	je     801016d0 <iput+0x5a>
  releasesleep(&ip->lock);
8010169b:	83 ec 0c             	sub    $0xc,%esp
8010169e:	56                   	push   %esi
8010169f:	e8 b2 26 00 00       	call   80103d56 <releasesleep>
  acquire(&icache.lock);
801016a4:	c7 04 24 00 0a 11 80 	movl   $0x80110a00,(%esp)
801016ab:	e8 6b 28 00 00       	call   80103f1b <acquire>
  ip->ref--;
801016b0:	8b 43 08             	mov    0x8(%ebx),%eax
801016b3:	83 e8 01             	sub    $0x1,%eax
801016b6:	89 43 08             	mov    %eax,0x8(%ebx)
  release(&icache.lock);
801016b9:	c7 04 24 00 0a 11 80 	movl   $0x80110a00,(%esp)
801016c0:	e8 bb 28 00 00       	call   80103f80 <release>
}
801016c5:	83 c4 10             	add    $0x10,%esp
801016c8:	8d 65 f4             	lea    -0xc(%ebp),%esp
801016cb:	5b                   	pop    %ebx
801016cc:	5e                   	pop    %esi
801016cd:	5f                   	pop    %edi
801016ce:	5d                   	pop    %ebp
801016cf:	c3                   	ret    
    acquire(&icache.lock);
801016d0:	83 ec 0c             	sub    $0xc,%esp
801016d3:	68 00 0a 11 80       	push   $0x80110a00
801016d8:	e8 3e 28 00 00       	call   80103f1b <acquire>
    int r = ip->ref;
801016dd:	8b 7b 08             	mov    0x8(%ebx),%edi
    release(&icache.lock);
801016e0:	c7 04 24 00 0a 11 80 	movl   $0x80110a00,(%esp)
801016e7:	e8 94 28 00 00       	call   80103f80 <release>
    if(r == 1){
801016ec:	83 c4 10             	add    $0x10,%esp
801016ef:	83 ff 01             	cmp    $0x1,%edi
801016f2:	75 a7                	jne    8010169b <iput+0x25>
      itrunc(ip);
801016f4:	89 d8                	mov    %ebx,%eax
801016f6:	e8 92 fd ff ff       	call   8010148d <itrunc>
      ip->type = 0;
801016fb:	66 c7 43 50 00 00    	movw   $0x0,0x50(%ebx)
      iupdate(ip);
80101701:	83 ec 0c             	sub    $0xc,%esp
80101704:	53                   	push   %ebx
80101705:	e8 04 fd ff ff       	call   8010140e <iupdate>
      ip->valid = 0;
8010170a:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
80101711:	83 c4 10             	add    $0x10,%esp
80101714:	eb 85                	jmp    8010169b <iput+0x25>

80101716 <iunlockput>:
{
80101716:	55                   	push   %ebp
80101717:	89 e5                	mov    %esp,%ebp
80101719:	53                   	push   %ebx
8010171a:	83 ec 10             	sub    $0x10,%esp
8010171d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
80101720:	53                   	push   %ebx
80101721:	e8 0b ff ff ff       	call   80101631 <iunlock>
  iput(ip);
80101726:	89 1c 24             	mov    %ebx,(%esp)
80101729:	e8 48 ff ff ff       	call   80101676 <iput>
}
8010172e:	83 c4 10             	add    $0x10,%esp
80101731:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101734:	c9                   	leave  
80101735:	c3                   	ret    

80101736 <stati>:
{
80101736:	55                   	push   %ebp
80101737:	89 e5                	mov    %esp,%ebp
80101739:	8b 55 08             	mov    0x8(%ebp),%edx
8010173c:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
8010173f:	8b 0a                	mov    (%edx),%ecx
80101741:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80101744:	8b 4a 04             	mov    0x4(%edx),%ecx
80101747:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
8010174a:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
8010174e:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
80101751:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80101755:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101759:	8b 52 58             	mov    0x58(%edx),%edx
8010175c:	89 50 10             	mov    %edx,0x10(%eax)
}
8010175f:	5d                   	pop    %ebp
80101760:	c3                   	ret    

80101761 <readi>:
{
80101761:	55                   	push   %ebp
80101762:	89 e5                	mov    %esp,%ebp
80101764:	57                   	push   %edi
80101765:	56                   	push   %esi
80101766:	53                   	push   %ebx
80101767:	83 ec 1c             	sub    $0x1c,%esp
8010176a:	8b 7d 10             	mov    0x10(%ebp),%edi
  if(ip->type == T_DEV){
8010176d:	8b 45 08             	mov    0x8(%ebp),%eax
80101770:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
80101775:	74 2c                	je     801017a3 <readi+0x42>
  if(off > ip->size || off + n < off)
80101777:	8b 45 08             	mov    0x8(%ebp),%eax
8010177a:	8b 40 58             	mov    0x58(%eax),%eax
8010177d:	39 f8                	cmp    %edi,%eax
8010177f:	0f 82 cb 00 00 00    	jb     80101850 <readi+0xef>
80101785:	89 fa                	mov    %edi,%edx
80101787:	03 55 14             	add    0x14(%ebp),%edx
8010178a:	0f 82 c7 00 00 00    	jb     80101857 <readi+0xf6>
  if(off + n > ip->size)
80101790:	39 d0                	cmp    %edx,%eax
80101792:	73 05                	jae    80101799 <readi+0x38>
    n = ip->size - off;
80101794:	29 f8                	sub    %edi,%eax
80101796:	89 45 14             	mov    %eax,0x14(%ebp)
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101799:	be 00 00 00 00       	mov    $0x0,%esi
8010179e:	e9 8f 00 00 00       	jmp    80101832 <readi+0xd1>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
801017a3:	0f b7 40 52          	movzwl 0x52(%eax),%eax
801017a7:	66 83 f8 09          	cmp    $0x9,%ax
801017ab:	0f 87 91 00 00 00    	ja     80101842 <readi+0xe1>
801017b1:	98                   	cwtl   
801017b2:	8b 04 c5 80 09 11 80 	mov    -0x7feef680(,%eax,8),%eax
801017b9:	85 c0                	test   %eax,%eax
801017bb:	0f 84 88 00 00 00    	je     80101849 <readi+0xe8>
    return devsw[ip->major].read(ip, dst, n);
801017c1:	83 ec 04             	sub    $0x4,%esp
801017c4:	ff 75 14             	pushl  0x14(%ebp)
801017c7:	ff 75 0c             	pushl  0xc(%ebp)
801017ca:	ff 75 08             	pushl  0x8(%ebp)
801017cd:	ff d0                	call   *%eax
801017cf:	83 c4 10             	add    $0x10,%esp
801017d2:	eb 66                	jmp    8010183a <readi+0xd9>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801017d4:	89 fa                	mov    %edi,%edx
801017d6:	c1 ea 09             	shr    $0x9,%edx
801017d9:	8b 45 08             	mov    0x8(%ebp),%eax
801017dc:	e8 70 f9 ff ff       	call   80101151 <bmap>
801017e1:	83 ec 08             	sub    $0x8,%esp
801017e4:	50                   	push   %eax
801017e5:	8b 45 08             	mov    0x8(%ebp),%eax
801017e8:	ff 30                	pushl  (%eax)
801017ea:	e8 7d e9 ff ff       	call   8010016c <bread>
801017ef:	89 c1                	mov    %eax,%ecx
    m = min(n - tot, BSIZE - off%BSIZE);
801017f1:	89 f8                	mov    %edi,%eax
801017f3:	25 ff 01 00 00       	and    $0x1ff,%eax
801017f8:	bb 00 02 00 00       	mov    $0x200,%ebx
801017fd:	29 c3                	sub    %eax,%ebx
801017ff:	8b 55 14             	mov    0x14(%ebp),%edx
80101802:	29 f2                	sub    %esi,%edx
80101804:	83 c4 0c             	add    $0xc,%esp
80101807:	39 d3                	cmp    %edx,%ebx
80101809:	0f 47 da             	cmova  %edx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
8010180c:	53                   	push   %ebx
8010180d:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80101810:	8d 44 01 5c          	lea    0x5c(%ecx,%eax,1),%eax
80101814:	50                   	push   %eax
80101815:	ff 75 0c             	pushl  0xc(%ebp)
80101818:	e8 25 28 00 00       	call   80104042 <memmove>
    brelse(bp);
8010181d:	83 c4 04             	add    $0x4,%esp
80101820:	ff 75 e4             	pushl  -0x1c(%ebp)
80101823:	e8 ad e9 ff ff       	call   801001d5 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101828:	01 de                	add    %ebx,%esi
8010182a:	01 df                	add    %ebx,%edi
8010182c:	01 5d 0c             	add    %ebx,0xc(%ebp)
8010182f:	83 c4 10             	add    $0x10,%esp
80101832:	39 75 14             	cmp    %esi,0x14(%ebp)
80101835:	77 9d                	ja     801017d4 <readi+0x73>
  return n;
80101837:	8b 45 14             	mov    0x14(%ebp),%eax
}
8010183a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010183d:	5b                   	pop    %ebx
8010183e:	5e                   	pop    %esi
8010183f:	5f                   	pop    %edi
80101840:	5d                   	pop    %ebp
80101841:	c3                   	ret    
      return -1;
80101842:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101847:	eb f1                	jmp    8010183a <readi+0xd9>
80101849:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010184e:	eb ea                	jmp    8010183a <readi+0xd9>
    return -1;
80101850:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101855:	eb e3                	jmp    8010183a <readi+0xd9>
80101857:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010185c:	eb dc                	jmp    8010183a <readi+0xd9>

8010185e <writei>:
{
8010185e:	55                   	push   %ebp
8010185f:	89 e5                	mov    %esp,%ebp
80101861:	57                   	push   %edi
80101862:	56                   	push   %esi
80101863:	53                   	push   %ebx
80101864:	83 ec 0c             	sub    $0xc,%esp
  if(ip->type == T_DEV){
80101867:	8b 45 08             	mov    0x8(%ebp),%eax
8010186a:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
8010186f:	74 2f                	je     801018a0 <writei+0x42>
  if(off > ip->size || off + n < off)
80101871:	8b 45 08             	mov    0x8(%ebp),%eax
80101874:	8b 4d 10             	mov    0x10(%ebp),%ecx
80101877:	39 48 58             	cmp    %ecx,0x58(%eax)
8010187a:	0f 82 f4 00 00 00    	jb     80101974 <writei+0x116>
80101880:	89 c8                	mov    %ecx,%eax
80101882:	03 45 14             	add    0x14(%ebp),%eax
80101885:	0f 82 f0 00 00 00    	jb     8010197b <writei+0x11d>
  if(off + n > MAXFILE*BSIZE)
8010188b:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101890:	0f 87 ec 00 00 00    	ja     80101982 <writei+0x124>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101896:	be 00 00 00 00       	mov    $0x0,%esi
8010189b:	e9 94 00 00 00       	jmp    80101934 <writei+0xd6>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
801018a0:	0f b7 40 52          	movzwl 0x52(%eax),%eax
801018a4:	66 83 f8 09          	cmp    $0x9,%ax
801018a8:	0f 87 b8 00 00 00    	ja     80101966 <writei+0x108>
801018ae:	98                   	cwtl   
801018af:	8b 04 c5 84 09 11 80 	mov    -0x7feef67c(,%eax,8),%eax
801018b6:	85 c0                	test   %eax,%eax
801018b8:	0f 84 af 00 00 00    	je     8010196d <writei+0x10f>
    return devsw[ip->major].write(ip, src, n);
801018be:	83 ec 04             	sub    $0x4,%esp
801018c1:	ff 75 14             	pushl  0x14(%ebp)
801018c4:	ff 75 0c             	pushl  0xc(%ebp)
801018c7:	ff 75 08             	pushl  0x8(%ebp)
801018ca:	ff d0                	call   *%eax
801018cc:	83 c4 10             	add    $0x10,%esp
801018cf:	eb 7c                	jmp    8010194d <writei+0xef>
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
801018d1:	8b 55 10             	mov    0x10(%ebp),%edx
801018d4:	c1 ea 09             	shr    $0x9,%edx
801018d7:	8b 45 08             	mov    0x8(%ebp),%eax
801018da:	e8 72 f8 ff ff       	call   80101151 <bmap>
801018df:	83 ec 08             	sub    $0x8,%esp
801018e2:	50                   	push   %eax
801018e3:	8b 45 08             	mov    0x8(%ebp),%eax
801018e6:	ff 30                	pushl  (%eax)
801018e8:	e8 7f e8 ff ff       	call   8010016c <bread>
801018ed:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
801018ef:	8b 45 10             	mov    0x10(%ebp),%eax
801018f2:	25 ff 01 00 00       	and    $0x1ff,%eax
801018f7:	bb 00 02 00 00       	mov    $0x200,%ebx
801018fc:	29 c3                	sub    %eax,%ebx
801018fe:	8b 55 14             	mov    0x14(%ebp),%edx
80101901:	29 f2                	sub    %esi,%edx
80101903:	83 c4 0c             	add    $0xc,%esp
80101906:	39 d3                	cmp    %edx,%ebx
80101908:	0f 47 da             	cmova  %edx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
8010190b:	53                   	push   %ebx
8010190c:	ff 75 0c             	pushl  0xc(%ebp)
8010190f:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
80101913:	50                   	push   %eax
80101914:	e8 29 27 00 00       	call   80104042 <memmove>
    log_write(bp);
80101919:	89 3c 24             	mov    %edi,(%esp)
8010191c:	e8 da 0f 00 00       	call   801028fb <log_write>
    brelse(bp);
80101921:	89 3c 24             	mov    %edi,(%esp)
80101924:	e8 ac e8 ff ff       	call   801001d5 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101929:	01 de                	add    %ebx,%esi
8010192b:	01 5d 10             	add    %ebx,0x10(%ebp)
8010192e:	01 5d 0c             	add    %ebx,0xc(%ebp)
80101931:	83 c4 10             	add    $0x10,%esp
80101934:	3b 75 14             	cmp    0x14(%ebp),%esi
80101937:	72 98                	jb     801018d1 <writei+0x73>
  if(n > 0 && off > ip->size){
80101939:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
8010193d:	74 0b                	je     8010194a <writei+0xec>
8010193f:	8b 45 08             	mov    0x8(%ebp),%eax
80101942:	8b 4d 10             	mov    0x10(%ebp),%ecx
80101945:	39 48 58             	cmp    %ecx,0x58(%eax)
80101948:	72 0b                	jb     80101955 <writei+0xf7>
  return n;
8010194a:	8b 45 14             	mov    0x14(%ebp),%eax
}
8010194d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101950:	5b                   	pop    %ebx
80101951:	5e                   	pop    %esi
80101952:	5f                   	pop    %edi
80101953:	5d                   	pop    %ebp
80101954:	c3                   	ret    
    ip->size = off;
80101955:	89 48 58             	mov    %ecx,0x58(%eax)
    iupdate(ip);
80101958:	83 ec 0c             	sub    $0xc,%esp
8010195b:	50                   	push   %eax
8010195c:	e8 ad fa ff ff       	call   8010140e <iupdate>
80101961:	83 c4 10             	add    $0x10,%esp
80101964:	eb e4                	jmp    8010194a <writei+0xec>
      return -1;
80101966:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010196b:	eb e0                	jmp    8010194d <writei+0xef>
8010196d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101972:	eb d9                	jmp    8010194d <writei+0xef>
    return -1;
80101974:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101979:	eb d2                	jmp    8010194d <writei+0xef>
8010197b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101980:	eb cb                	jmp    8010194d <writei+0xef>
    return -1;
80101982:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101987:	eb c4                	jmp    8010194d <writei+0xef>

80101989 <namecmp>:
{
80101989:	55                   	push   %ebp
8010198a:	89 e5                	mov    %esp,%ebp
8010198c:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
8010198f:	6a 0e                	push   $0xe
80101991:	ff 75 0c             	pushl  0xc(%ebp)
80101994:	ff 75 08             	pushl  0x8(%ebp)
80101997:	e8 0d 27 00 00       	call   801040a9 <strncmp>
}
8010199c:	c9                   	leave  
8010199d:	c3                   	ret    

8010199e <dirlookup>:
{
8010199e:	55                   	push   %ebp
8010199f:	89 e5                	mov    %esp,%ebp
801019a1:	57                   	push   %edi
801019a2:	56                   	push   %esi
801019a3:	53                   	push   %ebx
801019a4:	83 ec 1c             	sub    $0x1c,%esp
801019a7:	8b 75 08             	mov    0x8(%ebp),%esi
801019aa:	8b 7d 0c             	mov    0xc(%ebp),%edi
  if(dp->type != T_DIR)
801019ad:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
801019b2:	75 07                	jne    801019bb <dirlookup+0x1d>
  for(off = 0; off < dp->size; off += sizeof(de)){
801019b4:	bb 00 00 00 00       	mov    $0x0,%ebx
801019b9:	eb 1d                	jmp    801019d8 <dirlookup+0x3a>
    panic("dirlookup not DIR");
801019bb:	83 ec 0c             	sub    $0xc,%esp
801019be:	68 07 6e 10 80       	push   $0x80106e07
801019c3:	e8 80 e9 ff ff       	call   80100348 <panic>
      panic("dirlookup read");
801019c8:	83 ec 0c             	sub    $0xc,%esp
801019cb:	68 19 6e 10 80       	push   $0x80106e19
801019d0:	e8 73 e9 ff ff       	call   80100348 <panic>
  for(off = 0; off < dp->size; off += sizeof(de)){
801019d5:	83 c3 10             	add    $0x10,%ebx
801019d8:	39 5e 58             	cmp    %ebx,0x58(%esi)
801019db:	76 48                	jbe    80101a25 <dirlookup+0x87>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801019dd:	6a 10                	push   $0x10
801019df:	53                   	push   %ebx
801019e0:	8d 45 d8             	lea    -0x28(%ebp),%eax
801019e3:	50                   	push   %eax
801019e4:	56                   	push   %esi
801019e5:	e8 77 fd ff ff       	call   80101761 <readi>
801019ea:	83 c4 10             	add    $0x10,%esp
801019ed:	83 f8 10             	cmp    $0x10,%eax
801019f0:	75 d6                	jne    801019c8 <dirlookup+0x2a>
    if(de.inum == 0)
801019f2:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
801019f7:	74 dc                	je     801019d5 <dirlookup+0x37>
    if(namecmp(name, de.name) == 0){
801019f9:	83 ec 08             	sub    $0x8,%esp
801019fc:	8d 45 da             	lea    -0x26(%ebp),%eax
801019ff:	50                   	push   %eax
80101a00:	57                   	push   %edi
80101a01:	e8 83 ff ff ff       	call   80101989 <namecmp>
80101a06:	83 c4 10             	add    $0x10,%esp
80101a09:	85 c0                	test   %eax,%eax
80101a0b:	75 c8                	jne    801019d5 <dirlookup+0x37>
      if(poff)
80101a0d:	83 7d 10 00          	cmpl   $0x0,0x10(%ebp)
80101a11:	74 05                	je     80101a18 <dirlookup+0x7a>
        *poff = off;
80101a13:	8b 45 10             	mov    0x10(%ebp),%eax
80101a16:	89 18                	mov    %ebx,(%eax)
      inum = de.inum;
80101a18:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101a1c:	8b 06                	mov    (%esi),%eax
80101a1e:	e8 d4 f7 ff ff       	call   801011f7 <iget>
80101a23:	eb 05                	jmp    80101a2a <dirlookup+0x8c>
  return 0;
80101a25:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101a2a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a2d:	5b                   	pop    %ebx
80101a2e:	5e                   	pop    %esi
80101a2f:	5f                   	pop    %edi
80101a30:	5d                   	pop    %ebp
80101a31:	c3                   	ret    

80101a32 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101a32:	55                   	push   %ebp
80101a33:	89 e5                	mov    %esp,%ebp
80101a35:	57                   	push   %edi
80101a36:	56                   	push   %esi
80101a37:	53                   	push   %ebx
80101a38:	83 ec 1c             	sub    $0x1c,%esp
80101a3b:	89 c6                	mov    %eax,%esi
80101a3d:	89 55 e0             	mov    %edx,-0x20(%ebp)
80101a40:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  struct inode *ip, *next;

  if(*path == '/')
80101a43:	80 38 2f             	cmpb   $0x2f,(%eax)
80101a46:	74 17                	je     80101a5f <namex+0x2d>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101a48:	e8 0a 18 00 00       	call   80103257 <myproc>
80101a4d:	83 ec 0c             	sub    $0xc,%esp
80101a50:	ff 70 68             	pushl  0x68(%eax)
80101a53:	e8 e7 fa ff ff       	call   8010153f <idup>
80101a58:	89 c3                	mov    %eax,%ebx
80101a5a:	83 c4 10             	add    $0x10,%esp
80101a5d:	eb 53                	jmp    80101ab2 <namex+0x80>
    ip = iget(ROOTDEV, ROOTINO);
80101a5f:	ba 01 00 00 00       	mov    $0x1,%edx
80101a64:	b8 01 00 00 00       	mov    $0x1,%eax
80101a69:	e8 89 f7 ff ff       	call   801011f7 <iget>
80101a6e:	89 c3                	mov    %eax,%ebx
80101a70:	eb 40                	jmp    80101ab2 <namex+0x80>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
    if(ip->type != T_DIR){
      iunlockput(ip);
80101a72:	83 ec 0c             	sub    $0xc,%esp
80101a75:	53                   	push   %ebx
80101a76:	e8 9b fc ff ff       	call   80101716 <iunlockput>
      return 0;
80101a7b:	83 c4 10             	add    $0x10,%esp
80101a7e:	bb 00 00 00 00       	mov    $0x0,%ebx
  if(nameiparent){
    iput(ip);
    return 0;
  }
  return ip;
}
80101a83:	89 d8                	mov    %ebx,%eax
80101a85:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101a88:	5b                   	pop    %ebx
80101a89:	5e                   	pop    %esi
80101a8a:	5f                   	pop    %edi
80101a8b:	5d                   	pop    %ebp
80101a8c:	c3                   	ret    
    if((next = dirlookup(ip, name, 0)) == 0){
80101a8d:	83 ec 04             	sub    $0x4,%esp
80101a90:	6a 00                	push   $0x0
80101a92:	ff 75 e4             	pushl  -0x1c(%ebp)
80101a95:	53                   	push   %ebx
80101a96:	e8 03 ff ff ff       	call   8010199e <dirlookup>
80101a9b:	89 c7                	mov    %eax,%edi
80101a9d:	83 c4 10             	add    $0x10,%esp
80101aa0:	85 c0                	test   %eax,%eax
80101aa2:	74 4a                	je     80101aee <namex+0xbc>
    iunlockput(ip);
80101aa4:	83 ec 0c             	sub    $0xc,%esp
80101aa7:	53                   	push   %ebx
80101aa8:	e8 69 fc ff ff       	call   80101716 <iunlockput>
    ip = next;
80101aad:	83 c4 10             	add    $0x10,%esp
80101ab0:	89 fb                	mov    %edi,%ebx
  while((path = skipelem(path, name)) != 0){
80101ab2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101ab5:	89 f0                	mov    %esi,%eax
80101ab7:	e8 89 f4 ff ff       	call   80100f45 <skipelem>
80101abc:	89 c6                	mov    %eax,%esi
80101abe:	85 c0                	test   %eax,%eax
80101ac0:	74 3c                	je     80101afe <namex+0xcc>
    ilock(ip);
80101ac2:	83 ec 0c             	sub    $0xc,%esp
80101ac5:	53                   	push   %ebx
80101ac6:	e8 a4 fa ff ff       	call   8010156f <ilock>
    if(ip->type != T_DIR){
80101acb:	83 c4 10             	add    $0x10,%esp
80101ace:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101ad3:	75 9d                	jne    80101a72 <namex+0x40>
    if(nameiparent && *path == '\0'){
80101ad5:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80101ad9:	74 b2                	je     80101a8d <namex+0x5b>
80101adb:	80 3e 00             	cmpb   $0x0,(%esi)
80101ade:	75 ad                	jne    80101a8d <namex+0x5b>
      iunlock(ip);
80101ae0:	83 ec 0c             	sub    $0xc,%esp
80101ae3:	53                   	push   %ebx
80101ae4:	e8 48 fb ff ff       	call   80101631 <iunlock>
      return ip;
80101ae9:	83 c4 10             	add    $0x10,%esp
80101aec:	eb 95                	jmp    80101a83 <namex+0x51>
      iunlockput(ip);
80101aee:	83 ec 0c             	sub    $0xc,%esp
80101af1:	53                   	push   %ebx
80101af2:	e8 1f fc ff ff       	call   80101716 <iunlockput>
      return 0;
80101af7:	83 c4 10             	add    $0x10,%esp
80101afa:	89 fb                	mov    %edi,%ebx
80101afc:	eb 85                	jmp    80101a83 <namex+0x51>
  if(nameiparent){
80101afe:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80101b02:	0f 84 7b ff ff ff    	je     80101a83 <namex+0x51>
    iput(ip);
80101b08:	83 ec 0c             	sub    $0xc,%esp
80101b0b:	53                   	push   %ebx
80101b0c:	e8 65 fb ff ff       	call   80101676 <iput>
    return 0;
80101b11:	83 c4 10             	add    $0x10,%esp
80101b14:	bb 00 00 00 00       	mov    $0x0,%ebx
80101b19:	e9 65 ff ff ff       	jmp    80101a83 <namex+0x51>

80101b1e <dirlink>:
{
80101b1e:	55                   	push   %ebp
80101b1f:	89 e5                	mov    %esp,%ebp
80101b21:	57                   	push   %edi
80101b22:	56                   	push   %esi
80101b23:	53                   	push   %ebx
80101b24:	83 ec 20             	sub    $0x20,%esp
80101b27:	8b 5d 08             	mov    0x8(%ebp),%ebx
80101b2a:	8b 7d 0c             	mov    0xc(%ebp),%edi
  if((ip = dirlookup(dp, name, 0)) != 0){
80101b2d:	6a 00                	push   $0x0
80101b2f:	57                   	push   %edi
80101b30:	53                   	push   %ebx
80101b31:	e8 68 fe ff ff       	call   8010199e <dirlookup>
80101b36:	83 c4 10             	add    $0x10,%esp
80101b39:	85 c0                	test   %eax,%eax
80101b3b:	75 2d                	jne    80101b6a <dirlink+0x4c>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101b3d:	b8 00 00 00 00       	mov    $0x0,%eax
80101b42:	89 c6                	mov    %eax,%esi
80101b44:	39 43 58             	cmp    %eax,0x58(%ebx)
80101b47:	76 41                	jbe    80101b8a <dirlink+0x6c>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101b49:	6a 10                	push   $0x10
80101b4b:	50                   	push   %eax
80101b4c:	8d 45 d8             	lea    -0x28(%ebp),%eax
80101b4f:	50                   	push   %eax
80101b50:	53                   	push   %ebx
80101b51:	e8 0b fc ff ff       	call   80101761 <readi>
80101b56:	83 c4 10             	add    $0x10,%esp
80101b59:	83 f8 10             	cmp    $0x10,%eax
80101b5c:	75 1f                	jne    80101b7d <dirlink+0x5f>
    if(de.inum == 0)
80101b5e:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101b63:	74 25                	je     80101b8a <dirlink+0x6c>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101b65:	8d 46 10             	lea    0x10(%esi),%eax
80101b68:	eb d8                	jmp    80101b42 <dirlink+0x24>
    iput(ip);
80101b6a:	83 ec 0c             	sub    $0xc,%esp
80101b6d:	50                   	push   %eax
80101b6e:	e8 03 fb ff ff       	call   80101676 <iput>
    return -1;
80101b73:	83 c4 10             	add    $0x10,%esp
80101b76:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101b7b:	eb 3d                	jmp    80101bba <dirlink+0x9c>
      panic("dirlink read");
80101b7d:	83 ec 0c             	sub    $0xc,%esp
80101b80:	68 28 6e 10 80       	push   $0x80106e28
80101b85:	e8 be e7 ff ff       	call   80100348 <panic>
  strncpy(de.name, name, DIRSIZ);
80101b8a:	83 ec 04             	sub    $0x4,%esp
80101b8d:	6a 0e                	push   $0xe
80101b8f:	57                   	push   %edi
80101b90:	8d 7d d8             	lea    -0x28(%ebp),%edi
80101b93:	8d 45 da             	lea    -0x26(%ebp),%eax
80101b96:	50                   	push   %eax
80101b97:	e8 4a 25 00 00       	call   801040e6 <strncpy>
  de.inum = inum;
80101b9c:	8b 45 10             	mov    0x10(%ebp),%eax
80101b9f:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101ba3:	6a 10                	push   $0x10
80101ba5:	56                   	push   %esi
80101ba6:	57                   	push   %edi
80101ba7:	53                   	push   %ebx
80101ba8:	e8 b1 fc ff ff       	call   8010185e <writei>
80101bad:	83 c4 20             	add    $0x20,%esp
80101bb0:	83 f8 10             	cmp    $0x10,%eax
80101bb3:	75 0d                	jne    80101bc2 <dirlink+0xa4>
  return 0;
80101bb5:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101bba:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101bbd:	5b                   	pop    %ebx
80101bbe:	5e                   	pop    %esi
80101bbf:	5f                   	pop    %edi
80101bc0:	5d                   	pop    %ebp
80101bc1:	c3                   	ret    
    panic("dirlink");
80101bc2:	83 ec 0c             	sub    $0xc,%esp
80101bc5:	68 49 75 10 80       	push   $0x80107549
80101bca:	e8 79 e7 ff ff       	call   80100348 <panic>

80101bcf <namei>:

struct inode*
namei(char *path)
{
80101bcf:	55                   	push   %ebp
80101bd0:	89 e5                	mov    %esp,%ebp
80101bd2:	83 ec 18             	sub    $0x18,%esp
  char name[DIRSIZ];
  return namex(path, 0, name);
80101bd5:	8d 4d ea             	lea    -0x16(%ebp),%ecx
80101bd8:	ba 00 00 00 00       	mov    $0x0,%edx
80101bdd:	8b 45 08             	mov    0x8(%ebp),%eax
80101be0:	e8 4d fe ff ff       	call   80101a32 <namex>
}
80101be5:	c9                   	leave  
80101be6:	c3                   	ret    

80101be7 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80101be7:	55                   	push   %ebp
80101be8:	89 e5                	mov    %esp,%ebp
80101bea:	83 ec 08             	sub    $0x8,%esp
  return namex(path, 1, name);
80101bed:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80101bf0:	ba 01 00 00 00       	mov    $0x1,%edx
80101bf5:	8b 45 08             	mov    0x8(%ebp),%eax
80101bf8:	e8 35 fe ff ff       	call   80101a32 <namex>
}
80101bfd:	c9                   	leave  
80101bfe:	c3                   	ret    

80101bff <idewait>:
static void idestart(struct buf*);

// Wait for IDE disk to become ready.
static int
idewait(int checkerr)
{
80101bff:	55                   	push   %ebp
80101c00:	89 e5                	mov    %esp,%ebp
80101c02:	89 c1                	mov    %eax,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80101c04:	ba f7 01 00 00       	mov    $0x1f7,%edx
80101c09:	ec                   	in     (%dx),%al
80101c0a:	89 c2                	mov    %eax,%edx
  int r;

  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80101c0c:	83 e0 c0             	and    $0xffffffc0,%eax
80101c0f:	3c 40                	cmp    $0x40,%al
80101c11:	75 f1                	jne    80101c04 <idewait+0x5>
    ;
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
80101c13:	85 c9                	test   %ecx,%ecx
80101c15:	74 0c                	je     80101c23 <idewait+0x24>
80101c17:	f6 c2 21             	test   $0x21,%dl
80101c1a:	75 0e                	jne    80101c2a <idewait+0x2b>
    return -1;
  return 0;
80101c1c:	b8 00 00 00 00       	mov    $0x0,%eax
80101c21:	eb 05                	jmp    80101c28 <idewait+0x29>
80101c23:	b8 00 00 00 00       	mov    $0x0,%eax
}
80101c28:	5d                   	pop    %ebp
80101c29:	c3                   	ret    
    return -1;
80101c2a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101c2f:	eb f7                	jmp    80101c28 <idewait+0x29>

80101c31 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80101c31:	55                   	push   %ebp
80101c32:	89 e5                	mov    %esp,%ebp
80101c34:	56                   	push   %esi
80101c35:	53                   	push   %ebx
  if(b == 0)
80101c36:	85 c0                	test   %eax,%eax
80101c38:	74 7d                	je     80101cb7 <idestart+0x86>
80101c3a:	89 c6                	mov    %eax,%esi
    panic("idestart");
  if(b->blockno >= FSSIZE)
80101c3c:	8b 58 08             	mov    0x8(%eax),%ebx
80101c3f:	81 fb cf 07 00 00    	cmp    $0x7cf,%ebx
80101c45:	77 7d                	ja     80101cc4 <idestart+0x93>
  int read_cmd = (sector_per_block == 1) ? IDE_CMD_READ :  IDE_CMD_RDMUL;
  int write_cmd = (sector_per_block == 1) ? IDE_CMD_WRITE : IDE_CMD_WRMUL;

  if (sector_per_block > 7) panic("idestart");

  idewait(0);
80101c47:	b8 00 00 00 00       	mov    $0x0,%eax
80101c4c:	e8 ae ff ff ff       	call   80101bff <idewait>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80101c51:	b8 00 00 00 00       	mov    $0x0,%eax
80101c56:	ba f6 03 00 00       	mov    $0x3f6,%edx
80101c5b:	ee                   	out    %al,(%dx)
80101c5c:	b8 01 00 00 00       	mov    $0x1,%eax
80101c61:	ba f2 01 00 00       	mov    $0x1f2,%edx
80101c66:	ee                   	out    %al,(%dx)
80101c67:	ba f3 01 00 00       	mov    $0x1f3,%edx
80101c6c:	89 d8                	mov    %ebx,%eax
80101c6e:	ee                   	out    %al,(%dx)
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80101c6f:	89 d8                	mov    %ebx,%eax
80101c71:	c1 f8 08             	sar    $0x8,%eax
80101c74:	ba f4 01 00 00       	mov    $0x1f4,%edx
80101c79:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
80101c7a:	89 d8                	mov    %ebx,%eax
80101c7c:	c1 f8 10             	sar    $0x10,%eax
80101c7f:	ba f5 01 00 00       	mov    $0x1f5,%edx
80101c84:	ee                   	out    %al,(%dx)
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
80101c85:	0f b6 46 04          	movzbl 0x4(%esi),%eax
80101c89:	c1 e0 04             	shl    $0x4,%eax
80101c8c:	83 e0 10             	and    $0x10,%eax
80101c8f:	c1 fb 18             	sar    $0x18,%ebx
80101c92:	83 e3 0f             	and    $0xf,%ebx
80101c95:	09 d8                	or     %ebx,%eax
80101c97:	83 c8 e0             	or     $0xffffffe0,%eax
80101c9a:	ba f6 01 00 00       	mov    $0x1f6,%edx
80101c9f:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
80101ca0:	f6 06 04             	testb  $0x4,(%esi)
80101ca3:	75 2c                	jne    80101cd1 <idestart+0xa0>
80101ca5:	b8 20 00 00 00       	mov    $0x20,%eax
80101caa:	ba f7 01 00 00       	mov    $0x1f7,%edx
80101caf:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
80101cb0:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101cb3:	5b                   	pop    %ebx
80101cb4:	5e                   	pop    %esi
80101cb5:	5d                   	pop    %ebp
80101cb6:	c3                   	ret    
    panic("idestart");
80101cb7:	83 ec 0c             	sub    $0xc,%esp
80101cba:	68 8b 6e 10 80       	push   $0x80106e8b
80101cbf:	e8 84 e6 ff ff       	call   80100348 <panic>
    panic("incorrect blockno");
80101cc4:	83 ec 0c             	sub    $0xc,%esp
80101cc7:	68 94 6e 10 80       	push   $0x80106e94
80101ccc:	e8 77 e6 ff ff       	call   80100348 <panic>
80101cd1:	b8 30 00 00 00       	mov    $0x30,%eax
80101cd6:	ba f7 01 00 00       	mov    $0x1f7,%edx
80101cdb:	ee                   	out    %al,(%dx)
    outsl(0x1f0, b->data, BSIZE/4);
80101cdc:	83 c6 5c             	add    $0x5c,%esi
  asm volatile("cld; rep outsl" :
80101cdf:	b9 80 00 00 00       	mov    $0x80,%ecx
80101ce4:	ba f0 01 00 00       	mov    $0x1f0,%edx
80101ce9:	fc                   	cld    
80101cea:	f3 6f                	rep outsl %ds:(%esi),(%dx)
80101cec:	eb c2                	jmp    80101cb0 <idestart+0x7f>

80101cee <ideinit>:
{
80101cee:	55                   	push   %ebp
80101cef:	89 e5                	mov    %esp,%ebp
80101cf1:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
80101cf4:	68 a6 6e 10 80       	push   $0x80106ea6
80101cf9:	68 80 a5 10 80       	push   $0x8010a580
80101cfe:	e8 dc 20 00 00       	call   80103ddf <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
80101d03:	83 c4 08             	add    $0x8,%esp
80101d06:	a1 20 2d 11 80       	mov    0x80112d20,%eax
80101d0b:	83 e8 01             	sub    $0x1,%eax
80101d0e:	50                   	push   %eax
80101d0f:	6a 0e                	push   $0xe
80101d11:	e8 56 02 00 00       	call   80101f6c <ioapicenable>
  idewait(0);
80101d16:	b8 00 00 00 00       	mov    $0x0,%eax
80101d1b:	e8 df fe ff ff       	call   80101bff <idewait>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80101d20:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
80101d25:	ba f6 01 00 00       	mov    $0x1f6,%edx
80101d2a:	ee                   	out    %al,(%dx)
  for(i=0; i<1000; i++){
80101d2b:	83 c4 10             	add    $0x10,%esp
80101d2e:	b9 00 00 00 00       	mov    $0x0,%ecx
80101d33:	81 f9 e7 03 00 00    	cmp    $0x3e7,%ecx
80101d39:	7f 19                	jg     80101d54 <ideinit+0x66>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80101d3b:	ba f7 01 00 00       	mov    $0x1f7,%edx
80101d40:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
80101d41:	84 c0                	test   %al,%al
80101d43:	75 05                	jne    80101d4a <ideinit+0x5c>
  for(i=0; i<1000; i++){
80101d45:	83 c1 01             	add    $0x1,%ecx
80101d48:	eb e9                	jmp    80101d33 <ideinit+0x45>
      havedisk1 = 1;
80101d4a:	c7 05 60 a5 10 80 01 	movl   $0x1,0x8010a560
80101d51:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80101d54:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
80101d59:	ba f6 01 00 00       	mov    $0x1f6,%edx
80101d5e:	ee                   	out    %al,(%dx)
}
80101d5f:	c9                   	leave  
80101d60:	c3                   	ret    

80101d61 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80101d61:	55                   	push   %ebp
80101d62:	89 e5                	mov    %esp,%ebp
80101d64:	57                   	push   %edi
80101d65:	53                   	push   %ebx
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80101d66:	83 ec 0c             	sub    $0xc,%esp
80101d69:	68 80 a5 10 80       	push   $0x8010a580
80101d6e:	e8 a8 21 00 00       	call   80103f1b <acquire>

  if((b = idequeue) == 0){
80101d73:	8b 1d 64 a5 10 80    	mov    0x8010a564,%ebx
80101d79:	83 c4 10             	add    $0x10,%esp
80101d7c:	85 db                	test   %ebx,%ebx
80101d7e:	74 48                	je     80101dc8 <ideintr+0x67>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
80101d80:	8b 43 58             	mov    0x58(%ebx),%eax
80101d83:	a3 64 a5 10 80       	mov    %eax,0x8010a564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80101d88:	f6 03 04             	testb  $0x4,(%ebx)
80101d8b:	74 4d                	je     80101dda <ideintr+0x79>
    insl(0x1f0, b->data, BSIZE/4);

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
80101d8d:	8b 03                	mov    (%ebx),%eax
80101d8f:	83 c8 02             	or     $0x2,%eax
  b->flags &= ~B_DIRTY;
80101d92:	83 e0 fb             	and    $0xfffffffb,%eax
80101d95:	89 03                	mov    %eax,(%ebx)
  wakeup(b);
80101d97:	83 ec 0c             	sub    $0xc,%esp
80101d9a:	53                   	push   %ebx
80101d9b:	e8 5b 1a 00 00       	call   801037fb <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80101da0:	a1 64 a5 10 80       	mov    0x8010a564,%eax
80101da5:	83 c4 10             	add    $0x10,%esp
80101da8:	85 c0                	test   %eax,%eax
80101daa:	74 05                	je     80101db1 <ideintr+0x50>
    idestart(idequeue);
80101dac:	e8 80 fe ff ff       	call   80101c31 <idestart>

  release(&idelock);
80101db1:	83 ec 0c             	sub    $0xc,%esp
80101db4:	68 80 a5 10 80       	push   $0x8010a580
80101db9:	e8 c2 21 00 00       	call   80103f80 <release>
80101dbe:	83 c4 10             	add    $0x10,%esp
}
80101dc1:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101dc4:	5b                   	pop    %ebx
80101dc5:	5f                   	pop    %edi
80101dc6:	5d                   	pop    %ebp
80101dc7:	c3                   	ret    
    release(&idelock);
80101dc8:	83 ec 0c             	sub    $0xc,%esp
80101dcb:	68 80 a5 10 80       	push   $0x8010a580
80101dd0:	e8 ab 21 00 00       	call   80103f80 <release>
    return;
80101dd5:	83 c4 10             	add    $0x10,%esp
80101dd8:	eb e7                	jmp    80101dc1 <ideintr+0x60>
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
80101dda:	b8 01 00 00 00       	mov    $0x1,%eax
80101ddf:	e8 1b fe ff ff       	call   80101bff <idewait>
80101de4:	85 c0                	test   %eax,%eax
80101de6:	78 a5                	js     80101d8d <ideintr+0x2c>
    insl(0x1f0, b->data, BSIZE/4);
80101de8:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
80101deb:	b9 80 00 00 00       	mov    $0x80,%ecx
80101df0:	ba f0 01 00 00       	mov    $0x1f0,%edx
80101df5:	fc                   	cld    
80101df6:	f3 6d                	rep insl (%dx),%es:(%edi)
80101df8:	eb 93                	jmp    80101d8d <ideintr+0x2c>

80101dfa <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80101dfa:	55                   	push   %ebp
80101dfb:	89 e5                	mov    %esp,%ebp
80101dfd:	53                   	push   %ebx
80101dfe:	83 ec 10             	sub    $0x10,%esp
80101e01:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
80101e04:	8d 43 0c             	lea    0xc(%ebx),%eax
80101e07:	50                   	push   %eax
80101e08:	e8 84 1f 00 00       	call   80103d91 <holdingsleep>
80101e0d:	83 c4 10             	add    $0x10,%esp
80101e10:	85 c0                	test   %eax,%eax
80101e12:	74 37                	je     80101e4b <iderw+0x51>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
80101e14:	8b 03                	mov    (%ebx),%eax
80101e16:	83 e0 06             	and    $0x6,%eax
80101e19:	83 f8 02             	cmp    $0x2,%eax
80101e1c:	74 3a                	je     80101e58 <iderw+0x5e>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
80101e1e:	83 7b 04 00          	cmpl   $0x0,0x4(%ebx)
80101e22:	74 09                	je     80101e2d <iderw+0x33>
80101e24:	83 3d 60 a5 10 80 00 	cmpl   $0x0,0x8010a560
80101e2b:	74 38                	je     80101e65 <iderw+0x6b>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
80101e2d:	83 ec 0c             	sub    $0xc,%esp
80101e30:	68 80 a5 10 80       	push   $0x8010a580
80101e35:	e8 e1 20 00 00       	call   80103f1b <acquire>

  // Append b to idequeue.
  b->qnext = 0;
80101e3a:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80101e41:	83 c4 10             	add    $0x10,%esp
80101e44:	ba 64 a5 10 80       	mov    $0x8010a564,%edx
80101e49:	eb 2a                	jmp    80101e75 <iderw+0x7b>
    panic("iderw: buf not locked");
80101e4b:	83 ec 0c             	sub    $0xc,%esp
80101e4e:	68 aa 6e 10 80       	push   $0x80106eaa
80101e53:	e8 f0 e4 ff ff       	call   80100348 <panic>
    panic("iderw: nothing to do");
80101e58:	83 ec 0c             	sub    $0xc,%esp
80101e5b:	68 c0 6e 10 80       	push   $0x80106ec0
80101e60:	e8 e3 e4 ff ff       	call   80100348 <panic>
    panic("iderw: ide disk 1 not present");
80101e65:	83 ec 0c             	sub    $0xc,%esp
80101e68:	68 d5 6e 10 80       	push   $0x80106ed5
80101e6d:	e8 d6 e4 ff ff       	call   80100348 <panic>
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80101e72:	8d 50 58             	lea    0x58(%eax),%edx
80101e75:	8b 02                	mov    (%edx),%eax
80101e77:	85 c0                	test   %eax,%eax
80101e79:	75 f7                	jne    80101e72 <iderw+0x78>
    ;
  *pp = b;
80101e7b:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
80101e7d:	39 1d 64 a5 10 80    	cmp    %ebx,0x8010a564
80101e83:	75 1a                	jne    80101e9f <iderw+0xa5>
    idestart(b);
80101e85:	89 d8                	mov    %ebx,%eax
80101e87:	e8 a5 fd ff ff       	call   80101c31 <idestart>
80101e8c:	eb 11                	jmp    80101e9f <iderw+0xa5>

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
    sleep(b, &idelock);
80101e8e:	83 ec 08             	sub    $0x8,%esp
80101e91:	68 80 a5 10 80       	push   $0x8010a580
80101e96:	53                   	push   %ebx
80101e97:	e8 f7 17 00 00       	call   80103693 <sleep>
80101e9c:	83 c4 10             	add    $0x10,%esp
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
80101e9f:	8b 03                	mov    (%ebx),%eax
80101ea1:	83 e0 06             	and    $0x6,%eax
80101ea4:	83 f8 02             	cmp    $0x2,%eax
80101ea7:	75 e5                	jne    80101e8e <iderw+0x94>
  }


  release(&idelock);
80101ea9:	83 ec 0c             	sub    $0xc,%esp
80101eac:	68 80 a5 10 80       	push   $0x8010a580
80101eb1:	e8 ca 20 00 00       	call   80103f80 <release>
}
80101eb6:	83 c4 10             	add    $0x10,%esp
80101eb9:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101ebc:	c9                   	leave  
80101ebd:	c3                   	ret    

80101ebe <ioapicread>:
  uint data;
};

static uint
ioapicread(int reg)
{
80101ebe:	55                   	push   %ebp
80101ebf:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80101ec1:	8b 15 54 26 11 80    	mov    0x80112654,%edx
80101ec7:	89 02                	mov    %eax,(%edx)
  return ioapic->data;
80101ec9:	a1 54 26 11 80       	mov    0x80112654,%eax
80101ece:	8b 40 10             	mov    0x10(%eax),%eax
}
80101ed1:	5d                   	pop    %ebp
80101ed2:	c3                   	ret    

80101ed3 <ioapicwrite>:

static void
ioapicwrite(int reg, uint data)
{
80101ed3:	55                   	push   %ebp
80101ed4:	89 e5                	mov    %esp,%ebp
  ioapic->reg = reg;
80101ed6:	8b 0d 54 26 11 80    	mov    0x80112654,%ecx
80101edc:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80101ede:	a1 54 26 11 80       	mov    0x80112654,%eax
80101ee3:	89 50 10             	mov    %edx,0x10(%eax)
}
80101ee6:	5d                   	pop    %ebp
80101ee7:	c3                   	ret    

80101ee8 <ioapicinit>:

void
ioapicinit(void)
{
80101ee8:	55                   	push   %ebp
80101ee9:	89 e5                	mov    %esp,%ebp
80101eeb:	57                   	push   %edi
80101eec:	56                   	push   %esi
80101eed:	53                   	push   %ebx
80101eee:	83 ec 0c             	sub    $0xc,%esp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
80101ef1:	c7 05 54 26 11 80 00 	movl   $0xfec00000,0x80112654
80101ef8:	00 c0 fe 
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80101efb:	b8 01 00 00 00       	mov    $0x1,%eax
80101f00:	e8 b9 ff ff ff       	call   80101ebe <ioapicread>
80101f05:	c1 e8 10             	shr    $0x10,%eax
80101f08:	0f b6 f8             	movzbl %al,%edi
  id = ioapicread(REG_ID) >> 24;
80101f0b:	b8 00 00 00 00       	mov    $0x0,%eax
80101f10:	e8 a9 ff ff ff       	call   80101ebe <ioapicread>
80101f15:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
80101f18:	0f b6 15 80 27 11 80 	movzbl 0x80112780,%edx
80101f1f:	39 c2                	cmp    %eax,%edx
80101f21:	75 07                	jne    80101f2a <ioapicinit+0x42>
{
80101f23:	bb 00 00 00 00       	mov    $0x0,%ebx
80101f28:	eb 36                	jmp    80101f60 <ioapicinit+0x78>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80101f2a:	83 ec 0c             	sub    $0xc,%esp
80101f2d:	68 f4 6e 10 80       	push   $0x80106ef4
80101f32:	e8 d4 e6 ff ff       	call   8010060b <cprintf>
80101f37:	83 c4 10             	add    $0x10,%esp
80101f3a:	eb e7                	jmp    80101f23 <ioapicinit+0x3b>

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
80101f3c:	8d 53 20             	lea    0x20(%ebx),%edx
80101f3f:	81 ca 00 00 01 00    	or     $0x10000,%edx
80101f45:	8d 74 1b 10          	lea    0x10(%ebx,%ebx,1),%esi
80101f49:	89 f0                	mov    %esi,%eax
80101f4b:	e8 83 ff ff ff       	call   80101ed3 <ioapicwrite>
    ioapicwrite(REG_TABLE+2*i+1, 0);
80101f50:	8d 46 01             	lea    0x1(%esi),%eax
80101f53:	ba 00 00 00 00       	mov    $0x0,%edx
80101f58:	e8 76 ff ff ff       	call   80101ed3 <ioapicwrite>
  for(i = 0; i <= maxintr; i++){
80101f5d:	83 c3 01             	add    $0x1,%ebx
80101f60:	39 fb                	cmp    %edi,%ebx
80101f62:	7e d8                	jle    80101f3c <ioapicinit+0x54>
  }
}
80101f64:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101f67:	5b                   	pop    %ebx
80101f68:	5e                   	pop    %esi
80101f69:	5f                   	pop    %edi
80101f6a:	5d                   	pop    %ebp
80101f6b:	c3                   	ret    

80101f6c <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
80101f6c:	55                   	push   %ebp
80101f6d:	89 e5                	mov    %esp,%ebp
80101f6f:	53                   	push   %ebx
80101f70:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
80101f73:	8d 50 20             	lea    0x20(%eax),%edx
80101f76:	8d 5c 00 10          	lea    0x10(%eax,%eax,1),%ebx
80101f7a:	89 d8                	mov    %ebx,%eax
80101f7c:	e8 52 ff ff ff       	call   80101ed3 <ioapicwrite>
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80101f81:	8b 55 0c             	mov    0xc(%ebp),%edx
80101f84:	c1 e2 18             	shl    $0x18,%edx
80101f87:	8d 43 01             	lea    0x1(%ebx),%eax
80101f8a:	e8 44 ff ff ff       	call   80101ed3 <ioapicwrite>
}
80101f8f:	5b                   	pop    %ebx
80101f90:	5d                   	pop    %ebp
80101f91:	c3                   	ret    

80101f92 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80101f92:	55                   	push   %ebp
80101f93:	89 e5                	mov    %esp,%ebp
80101f95:	53                   	push   %ebx
80101f96:	83 ec 04             	sub    $0x4,%esp
80101f99:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
80101f9c:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
80101fa2:	75 61                	jne    80102005 <kfree+0x73>
80101fa4:	81 fb 18 37 12 80    	cmp    $0x80123718,%ebx
80101faa:	72 59                	jb     80102005 <kfree+0x73>

// Convert kernel virtual address to physical address
static inline uint V2P(void *a) {
    // define panic() here because memlayout.h is included before defs.h
    extern void panic(char*) __attribute__((noreturn));
    if (a < (void*) KERNBASE)
80101fac:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
80101fb2:	76 44                	jbe    80101ff8 <kfree+0x66>
        panic("V2P on address < KERNBASE "
              "(not a kernel virtual address; consider walking page "
              "table to determine physical address of a user virtual address)");
    return (uint)a - KERNBASE;
80101fb4:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80101fba:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80101fbf:	77 44                	ja     80102005 <kfree+0x73>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80101fc1:	83 ec 04             	sub    $0x4,%esp
80101fc4:	68 00 10 00 00       	push   $0x1000
80101fc9:	6a 01                	push   $0x1
80101fcb:	53                   	push   %ebx
80101fcc:	e8 f6 1f 00 00       	call   80103fc7 <memset>

  if(kmem.use_lock)
80101fd1:	83 c4 10             	add    $0x10,%esp
80101fd4:	83 3d 94 26 11 80 00 	cmpl   $0x0,0x80112694
80101fdb:	75 35                	jne    80102012 <kfree+0x80>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
80101fdd:	a1 98 26 11 80       	mov    0x80112698,%eax
80101fe2:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
80101fe4:	89 1d 98 26 11 80    	mov    %ebx,0x80112698
  if(kmem.use_lock)
80101fea:	83 3d 94 26 11 80 00 	cmpl   $0x0,0x80112694
80101ff1:	75 31                	jne    80102024 <kfree+0x92>
    release(&kmem.lock);
}
80101ff3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101ff6:	c9                   	leave  
80101ff7:	c3                   	ret    
        panic("V2P on address < KERNBASE "
80101ff8:	83 ec 0c             	sub    $0xc,%esp
80101ffb:	68 28 6f 10 80       	push   $0x80106f28
80102000:	e8 43 e3 ff ff       	call   80100348 <panic>
    panic("kfree");
80102005:	83 ec 0c             	sub    $0xc,%esp
80102008:	68 b6 6f 10 80       	push   $0x80106fb6
8010200d:	e8 36 e3 ff ff       	call   80100348 <panic>
    acquire(&kmem.lock);
80102012:	83 ec 0c             	sub    $0xc,%esp
80102015:	68 60 26 11 80       	push   $0x80112660
8010201a:	e8 fc 1e 00 00       	call   80103f1b <acquire>
8010201f:	83 c4 10             	add    $0x10,%esp
80102022:	eb b9                	jmp    80101fdd <kfree+0x4b>
    release(&kmem.lock);
80102024:	83 ec 0c             	sub    $0xc,%esp
80102027:	68 60 26 11 80       	push   $0x80112660
8010202c:	e8 4f 1f 00 00       	call   80103f80 <release>
80102031:	83 c4 10             	add    $0x10,%esp
}
80102034:	eb bd                	jmp    80101ff3 <kfree+0x61>

80102036 <freerange>:
{
80102036:	55                   	push   %ebp
80102037:	89 e5                	mov    %esp,%ebp
80102039:	56                   	push   %esi
8010203a:	53                   	push   %ebx
8010203b:	8b 45 08             	mov    0x8(%ebp),%eax
8010203e:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  if (vend < vstart) panic("freerange");
80102041:	39 c3                	cmp    %eax,%ebx
80102043:	72 0c                	jb     80102051 <freerange+0x1b>
  p = (char*)PGROUNDUP((uint)vstart);
80102045:	05 ff 0f 00 00       	add    $0xfff,%eax
8010204a:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010204f:	eb 1b                	jmp    8010206c <freerange+0x36>
  if (vend < vstart) panic("freerange");
80102051:	83 ec 0c             	sub    $0xc,%esp
80102054:	68 bc 6f 10 80       	push   $0x80106fbc
80102059:	e8 ea e2 ff ff       	call   80100348 <panic>
    kfree(p);
8010205e:	83 ec 0c             	sub    $0xc,%esp
80102061:	50                   	push   %eax
80102062:	e8 2b ff ff ff       	call   80101f92 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102067:	83 c4 10             	add    $0x10,%esp
8010206a:	89 f0                	mov    %esi,%eax
8010206c:	8d b0 00 10 00 00    	lea    0x1000(%eax),%esi
80102072:	39 de                	cmp    %ebx,%esi
80102074:	76 e8                	jbe    8010205e <freerange+0x28>
}
80102076:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102079:	5b                   	pop    %ebx
8010207a:	5e                   	pop    %esi
8010207b:	5d                   	pop    %ebp
8010207c:	c3                   	ret    

8010207d <kinit1>:
{
8010207d:	55                   	push   %ebp
8010207e:	89 e5                	mov    %esp,%ebp
80102080:	83 ec 10             	sub    $0x10,%esp
  initlock(&kmem.lock, "kmem");
80102083:	68 c6 6f 10 80       	push   $0x80106fc6
80102088:	68 60 26 11 80       	push   $0x80112660
8010208d:	e8 4d 1d 00 00       	call   80103ddf <initlock>
  kmem.use_lock = 0;
80102092:	c7 05 94 26 11 80 00 	movl   $0x0,0x80112694
80102099:	00 00 00 
  freerange(vstart, vend);
8010209c:	83 c4 08             	add    $0x8,%esp
8010209f:	ff 75 0c             	pushl  0xc(%ebp)
801020a2:	ff 75 08             	pushl  0x8(%ebp)
801020a5:	e8 8c ff ff ff       	call   80102036 <freerange>
}
801020aa:	83 c4 10             	add    $0x10,%esp
801020ad:	c9                   	leave  
801020ae:	c3                   	ret    

801020af <kinit2>:
{
801020af:	55                   	push   %ebp
801020b0:	89 e5                	mov    %esp,%ebp
801020b2:	83 ec 10             	sub    $0x10,%esp
  freerange(vstart, vend);
801020b5:	ff 75 0c             	pushl  0xc(%ebp)
801020b8:	ff 75 08             	pushl  0x8(%ebp)
801020bb:	e8 76 ff ff ff       	call   80102036 <freerange>
  kmem.use_lock = 1;
801020c0:	c7 05 94 26 11 80 01 	movl   $0x1,0x80112694
801020c7:	00 00 00 
}
801020ca:	83 c4 10             	add    $0x10,%esp
801020cd:	c9                   	leave  
801020ce:	c3                   	ret    

801020cf <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
801020cf:	55                   	push   %ebp
801020d0:	89 e5                	mov    %esp,%ebp
801020d2:	53                   	push   %ebx
801020d3:	83 ec 04             	sub    $0x4,%esp
  struct run *r;

  if(kmem.use_lock)
801020d6:	83 3d 94 26 11 80 00 	cmpl   $0x0,0x80112694
801020dd:	75 21                	jne    80102100 <kalloc+0x31>
    acquire(&kmem.lock);
  r = kmem.freelist;
801020df:	8b 1d 98 26 11 80    	mov    0x80112698,%ebx
  if(r)
801020e5:	85 db                	test   %ebx,%ebx
801020e7:	74 07                	je     801020f0 <kalloc+0x21>
    kmem.freelist = r->next;
801020e9:	8b 03                	mov    (%ebx),%eax
801020eb:	a3 98 26 11 80       	mov    %eax,0x80112698
  if(kmem.use_lock)
801020f0:	83 3d 94 26 11 80 00 	cmpl   $0x0,0x80112694
801020f7:	75 19                	jne    80102112 <kalloc+0x43>
    release(&kmem.lock);
  return (char*)r;
}
801020f9:	89 d8                	mov    %ebx,%eax
801020fb:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801020fe:	c9                   	leave  
801020ff:	c3                   	ret    
    acquire(&kmem.lock);
80102100:	83 ec 0c             	sub    $0xc,%esp
80102103:	68 60 26 11 80       	push   $0x80112660
80102108:	e8 0e 1e 00 00       	call   80103f1b <acquire>
8010210d:	83 c4 10             	add    $0x10,%esp
80102110:	eb cd                	jmp    801020df <kalloc+0x10>
    release(&kmem.lock);
80102112:	83 ec 0c             	sub    $0xc,%esp
80102115:	68 60 26 11 80       	push   $0x80112660
8010211a:	e8 61 1e 00 00       	call   80103f80 <release>
8010211f:	83 c4 10             	add    $0x10,%esp
  return (char*)r;
80102122:	eb d5                	jmp    801020f9 <kalloc+0x2a>

80102124 <kbdgetc>:
#include "defs.h"
#include "kbd.h"

int
kbdgetc(void)
{
80102124:	55                   	push   %ebp
80102125:	89 e5                	mov    %esp,%ebp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102127:	ba 64 00 00 00       	mov    $0x64,%edx
8010212c:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
8010212d:	a8 01                	test   $0x1,%al
8010212f:	0f 84 b5 00 00 00    	je     801021ea <kbdgetc+0xc6>
80102135:	ba 60 00 00 00       	mov    $0x60,%edx
8010213a:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
8010213b:	0f b6 d0             	movzbl %al,%edx

  if(data == 0xE0){
8010213e:	81 fa e0 00 00 00    	cmp    $0xe0,%edx
80102144:	74 5c                	je     801021a2 <kbdgetc+0x7e>
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
80102146:	84 c0                	test   %al,%al
80102148:	78 66                	js     801021b0 <kbdgetc+0x8c>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
8010214a:	8b 0d b4 a5 10 80    	mov    0x8010a5b4,%ecx
80102150:	f6 c1 40             	test   $0x40,%cl
80102153:	74 0f                	je     80102164 <kbdgetc+0x40>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102155:	83 c8 80             	or     $0xffffff80,%eax
80102158:	0f b6 d0             	movzbl %al,%edx
    shift &= ~E0ESC;
8010215b:	83 e1 bf             	and    $0xffffffbf,%ecx
8010215e:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
  }

  shift |= shiftcode[data];
80102164:	0f b6 8a 00 71 10 80 	movzbl -0x7fef8f00(%edx),%ecx
8010216b:	0b 0d b4 a5 10 80    	or     0x8010a5b4,%ecx
  shift ^= togglecode[data];
80102171:	0f b6 82 00 70 10 80 	movzbl -0x7fef9000(%edx),%eax
80102178:	31 c1                	xor    %eax,%ecx
8010217a:	89 0d b4 a5 10 80    	mov    %ecx,0x8010a5b4
  c = charcode[shift & (CTL | SHIFT)][data];
80102180:	89 c8                	mov    %ecx,%eax
80102182:	83 e0 03             	and    $0x3,%eax
80102185:	8b 04 85 e0 6f 10 80 	mov    -0x7fef9020(,%eax,4),%eax
8010218c:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
80102190:	f6 c1 08             	test   $0x8,%cl
80102193:	74 19                	je     801021ae <kbdgetc+0x8a>
    if('a' <= c && c <= 'z')
80102195:	8d 50 9f             	lea    -0x61(%eax),%edx
80102198:	83 fa 19             	cmp    $0x19,%edx
8010219b:	77 40                	ja     801021dd <kbdgetc+0xb9>
      c += 'A' - 'a';
8010219d:	83 e8 20             	sub    $0x20,%eax
801021a0:	eb 0c                	jmp    801021ae <kbdgetc+0x8a>
    shift |= E0ESC;
801021a2:	83 0d b4 a5 10 80 40 	orl    $0x40,0x8010a5b4
    return 0;
801021a9:	b8 00 00 00 00       	mov    $0x0,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
801021ae:	5d                   	pop    %ebp
801021af:	c3                   	ret    
    data = (shift & E0ESC ? data : data & 0x7F);
801021b0:	8b 0d b4 a5 10 80    	mov    0x8010a5b4,%ecx
801021b6:	f6 c1 40             	test   $0x40,%cl
801021b9:	75 05                	jne    801021c0 <kbdgetc+0x9c>
801021bb:	89 c2                	mov    %eax,%edx
801021bd:	83 e2 7f             	and    $0x7f,%edx
    shift &= ~(shiftcode[data] | E0ESC);
801021c0:	0f b6 82 00 71 10 80 	movzbl -0x7fef8f00(%edx),%eax
801021c7:	83 c8 40             	or     $0x40,%eax
801021ca:	0f b6 c0             	movzbl %al,%eax
801021cd:	f7 d0                	not    %eax
801021cf:	21 c8                	and    %ecx,%eax
801021d1:	a3 b4 a5 10 80       	mov    %eax,0x8010a5b4
    return 0;
801021d6:	b8 00 00 00 00       	mov    $0x0,%eax
801021db:	eb d1                	jmp    801021ae <kbdgetc+0x8a>
    else if('A' <= c && c <= 'Z')
801021dd:	8d 50 bf             	lea    -0x41(%eax),%edx
801021e0:	83 fa 19             	cmp    $0x19,%edx
801021e3:	77 c9                	ja     801021ae <kbdgetc+0x8a>
      c += 'a' - 'A';
801021e5:	83 c0 20             	add    $0x20,%eax
  return c;
801021e8:	eb c4                	jmp    801021ae <kbdgetc+0x8a>
    return -1;
801021ea:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801021ef:	eb bd                	jmp    801021ae <kbdgetc+0x8a>

801021f1 <kbdintr>:

void
kbdintr(void)
{
801021f1:	55                   	push   %ebp
801021f2:	89 e5                	mov    %esp,%ebp
801021f4:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
801021f7:	68 24 21 10 80       	push   $0x80102124
801021fc:	e8 3d e5 ff ff       	call   8010073e <consoleintr>
}
80102201:	83 c4 10             	add    $0x10,%esp
80102204:	c9                   	leave  
80102205:	c3                   	ret    

80102206 <shutdown>:
#include "types.h"
#include "x86.h"

void
shutdown(void)
{
80102206:	55                   	push   %ebp
80102207:	89 e5                	mov    %esp,%ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102209:	b8 00 00 00 00       	mov    $0x0,%eax
8010220e:	ba 01 05 00 00       	mov    $0x501,%edx
80102213:	ee                   	out    %al,(%dx)
  /*
     This only works in QEMU and assumes QEMU was run 
     with -device isa-debug-exit
   */
  outb(0x501, 0x0);
}
80102214:	5d                   	pop    %ebp
80102215:	c3                   	ret    

80102216 <lapicw>:
volatile uint *lapic;  // Initialized in mp.c

//PAGEBREAK!
static void
lapicw(int index, int value)
{
80102216:	55                   	push   %ebp
80102217:	89 e5                	mov    %esp,%ebp
  lapic[index] = value;
80102219:	8b 0d 9c 26 11 80    	mov    0x8011269c,%ecx
8010221f:	8d 04 81             	lea    (%ecx,%eax,4),%eax
80102222:	89 10                	mov    %edx,(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102224:	a1 9c 26 11 80       	mov    0x8011269c,%eax
80102229:	8b 40 20             	mov    0x20(%eax),%eax
}
8010222c:	5d                   	pop    %ebp
8010222d:	c3                   	ret    

8010222e <cmos_read>:
#define MONTH   0x08
#define YEAR    0x09

static uint
cmos_read(uint reg)
{
8010222e:	55                   	push   %ebp
8010222f:	89 e5                	mov    %esp,%ebp
80102231:	ba 70 00 00 00       	mov    $0x70,%edx
80102236:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102237:	ba 71 00 00 00       	mov    $0x71,%edx
8010223c:	ec                   	in     (%dx),%al
  outb(CMOS_PORT,  reg);
  microdelay(200);

  return inb(CMOS_RETURN);
8010223d:	0f b6 c0             	movzbl %al,%eax
}
80102240:	5d                   	pop    %ebp
80102241:	c3                   	ret    

80102242 <fill_rtcdate>:

static void
fill_rtcdate(struct rtcdate *r)
{
80102242:	55                   	push   %ebp
80102243:	89 e5                	mov    %esp,%ebp
80102245:	53                   	push   %ebx
80102246:	89 c3                	mov    %eax,%ebx
  r->second = cmos_read(SECS);
80102248:	b8 00 00 00 00       	mov    $0x0,%eax
8010224d:	e8 dc ff ff ff       	call   8010222e <cmos_read>
80102252:	89 03                	mov    %eax,(%ebx)
  r->minute = cmos_read(MINS);
80102254:	b8 02 00 00 00       	mov    $0x2,%eax
80102259:	e8 d0 ff ff ff       	call   8010222e <cmos_read>
8010225e:	89 43 04             	mov    %eax,0x4(%ebx)
  r->hour   = cmos_read(HOURS);
80102261:	b8 04 00 00 00       	mov    $0x4,%eax
80102266:	e8 c3 ff ff ff       	call   8010222e <cmos_read>
8010226b:	89 43 08             	mov    %eax,0x8(%ebx)
  r->day    = cmos_read(DAY);
8010226e:	b8 07 00 00 00       	mov    $0x7,%eax
80102273:	e8 b6 ff ff ff       	call   8010222e <cmos_read>
80102278:	89 43 0c             	mov    %eax,0xc(%ebx)
  r->month  = cmos_read(MONTH);
8010227b:	b8 08 00 00 00       	mov    $0x8,%eax
80102280:	e8 a9 ff ff ff       	call   8010222e <cmos_read>
80102285:	89 43 10             	mov    %eax,0x10(%ebx)
  r->year   = cmos_read(YEAR);
80102288:	b8 09 00 00 00       	mov    $0x9,%eax
8010228d:	e8 9c ff ff ff       	call   8010222e <cmos_read>
80102292:	89 43 14             	mov    %eax,0x14(%ebx)
}
80102295:	5b                   	pop    %ebx
80102296:	5d                   	pop    %ebp
80102297:	c3                   	ret    

80102298 <lapicinit>:
  if(!lapic)
80102298:	83 3d 9c 26 11 80 00 	cmpl   $0x0,0x8011269c
8010229f:	0f 84 fb 00 00 00    	je     801023a0 <lapicinit+0x108>
{
801022a5:	55                   	push   %ebp
801022a6:	89 e5                	mov    %esp,%ebp
  lapicw(SVR, ENABLE | (T_IRQ0 + IRQ_SPURIOUS));
801022a8:	ba 3f 01 00 00       	mov    $0x13f,%edx
801022ad:	b8 3c 00 00 00       	mov    $0x3c,%eax
801022b2:	e8 5f ff ff ff       	call   80102216 <lapicw>
  lapicw(TDCR, X1);
801022b7:	ba 0b 00 00 00       	mov    $0xb,%edx
801022bc:	b8 f8 00 00 00       	mov    $0xf8,%eax
801022c1:	e8 50 ff ff ff       	call   80102216 <lapicw>
  lapicw(TIMER, PERIODIC | (T_IRQ0 + IRQ_TIMER));
801022c6:	ba 20 00 02 00       	mov    $0x20020,%edx
801022cb:	b8 c8 00 00 00       	mov    $0xc8,%eax
801022d0:	e8 41 ff ff ff       	call   80102216 <lapicw>
  lapicw(TICR, 10000000);
801022d5:	ba 80 96 98 00       	mov    $0x989680,%edx
801022da:	b8 e0 00 00 00       	mov    $0xe0,%eax
801022df:	e8 32 ff ff ff       	call   80102216 <lapicw>
  lapicw(LINT0, MASKED);
801022e4:	ba 00 00 01 00       	mov    $0x10000,%edx
801022e9:	b8 d4 00 00 00       	mov    $0xd4,%eax
801022ee:	e8 23 ff ff ff       	call   80102216 <lapicw>
  lapicw(LINT1, MASKED);
801022f3:	ba 00 00 01 00       	mov    $0x10000,%edx
801022f8:	b8 d8 00 00 00       	mov    $0xd8,%eax
801022fd:	e8 14 ff ff ff       	call   80102216 <lapicw>
  if(((lapic[VER]>>16) & 0xFF) >= 4)
80102302:	a1 9c 26 11 80       	mov    0x8011269c,%eax
80102307:	8b 40 30             	mov    0x30(%eax),%eax
8010230a:	c1 e8 10             	shr    $0x10,%eax
8010230d:	3c 03                	cmp    $0x3,%al
8010230f:	77 7b                	ja     8010238c <lapicinit+0xf4>
  lapicw(ERROR, T_IRQ0 + IRQ_ERROR);
80102311:	ba 33 00 00 00       	mov    $0x33,%edx
80102316:	b8 dc 00 00 00       	mov    $0xdc,%eax
8010231b:	e8 f6 fe ff ff       	call   80102216 <lapicw>
  lapicw(ESR, 0);
80102320:	ba 00 00 00 00       	mov    $0x0,%edx
80102325:	b8 a0 00 00 00       	mov    $0xa0,%eax
8010232a:	e8 e7 fe ff ff       	call   80102216 <lapicw>
  lapicw(ESR, 0);
8010232f:	ba 00 00 00 00       	mov    $0x0,%edx
80102334:	b8 a0 00 00 00       	mov    $0xa0,%eax
80102339:	e8 d8 fe ff ff       	call   80102216 <lapicw>
  lapicw(EOI, 0);
8010233e:	ba 00 00 00 00       	mov    $0x0,%edx
80102343:	b8 2c 00 00 00       	mov    $0x2c,%eax
80102348:	e8 c9 fe ff ff       	call   80102216 <lapicw>
  lapicw(ICRHI, 0);
8010234d:	ba 00 00 00 00       	mov    $0x0,%edx
80102352:	b8 c4 00 00 00       	mov    $0xc4,%eax
80102357:	e8 ba fe ff ff       	call   80102216 <lapicw>
  lapicw(ICRLO, BCAST | INIT | LEVEL);
8010235c:	ba 00 85 08 00       	mov    $0x88500,%edx
80102361:	b8 c0 00 00 00       	mov    $0xc0,%eax
80102366:	e8 ab fe ff ff       	call   80102216 <lapicw>
  while(lapic[ICRLO] & DELIVS)
8010236b:	a1 9c 26 11 80       	mov    0x8011269c,%eax
80102370:	8b 80 00 03 00 00    	mov    0x300(%eax),%eax
80102376:	f6 c4 10             	test   $0x10,%ah
80102379:	75 f0                	jne    8010236b <lapicinit+0xd3>
  lapicw(TPR, 0);
8010237b:	ba 00 00 00 00       	mov    $0x0,%edx
80102380:	b8 20 00 00 00       	mov    $0x20,%eax
80102385:	e8 8c fe ff ff       	call   80102216 <lapicw>
}
8010238a:	5d                   	pop    %ebp
8010238b:	c3                   	ret    
    lapicw(PCINT, MASKED);
8010238c:	ba 00 00 01 00       	mov    $0x10000,%edx
80102391:	b8 d0 00 00 00       	mov    $0xd0,%eax
80102396:	e8 7b fe ff ff       	call   80102216 <lapicw>
8010239b:	e9 71 ff ff ff       	jmp    80102311 <lapicinit+0x79>
801023a0:	f3 c3                	repz ret 

801023a2 <lapicid>:
{
801023a2:	55                   	push   %ebp
801023a3:	89 e5                	mov    %esp,%ebp
  if (!lapic)
801023a5:	a1 9c 26 11 80       	mov    0x8011269c,%eax
801023aa:	85 c0                	test   %eax,%eax
801023ac:	74 08                	je     801023b6 <lapicid+0x14>
  return lapic[ID] >> 24;
801023ae:	8b 40 20             	mov    0x20(%eax),%eax
801023b1:	c1 e8 18             	shr    $0x18,%eax
}
801023b4:	5d                   	pop    %ebp
801023b5:	c3                   	ret    
    return 0;
801023b6:	b8 00 00 00 00       	mov    $0x0,%eax
801023bb:	eb f7                	jmp    801023b4 <lapicid+0x12>

801023bd <lapiceoi>:
  if(lapic)
801023bd:	83 3d 9c 26 11 80 00 	cmpl   $0x0,0x8011269c
801023c4:	74 14                	je     801023da <lapiceoi+0x1d>
{
801023c6:	55                   	push   %ebp
801023c7:	89 e5                	mov    %esp,%ebp
    lapicw(EOI, 0);
801023c9:	ba 00 00 00 00       	mov    $0x0,%edx
801023ce:	b8 2c 00 00 00       	mov    $0x2c,%eax
801023d3:	e8 3e fe ff ff       	call   80102216 <lapicw>
}
801023d8:	5d                   	pop    %ebp
801023d9:	c3                   	ret    
801023da:	f3 c3                	repz ret 

801023dc <microdelay>:
{
801023dc:	55                   	push   %ebp
801023dd:	89 e5                	mov    %esp,%ebp
}
801023df:	5d                   	pop    %ebp
801023e0:	c3                   	ret    

801023e1 <lapicstartap>:
{
801023e1:	55                   	push   %ebp
801023e2:	89 e5                	mov    %esp,%ebp
801023e4:	57                   	push   %edi
801023e5:	56                   	push   %esi
801023e6:	53                   	push   %ebx
801023e7:	8b 75 08             	mov    0x8(%ebp),%esi
801023ea:	8b 7d 0c             	mov    0xc(%ebp),%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801023ed:	b8 0f 00 00 00       	mov    $0xf,%eax
801023f2:	ba 70 00 00 00       	mov    $0x70,%edx
801023f7:	ee                   	out    %al,(%dx)
801023f8:	b8 0a 00 00 00       	mov    $0xa,%eax
801023fd:	ba 71 00 00 00       	mov    $0x71,%edx
80102402:	ee                   	out    %al,(%dx)
  wrv[0] = 0;
80102403:	66 c7 05 67 04 00 80 	movw   $0x0,0x80000467
8010240a:	00 00 
  wrv[1] = addr >> 4;
8010240c:	89 f8                	mov    %edi,%eax
8010240e:	c1 e8 04             	shr    $0x4,%eax
80102411:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapicw(ICRHI, apicid<<24);
80102417:	c1 e6 18             	shl    $0x18,%esi
8010241a:	89 f2                	mov    %esi,%edx
8010241c:	b8 c4 00 00 00       	mov    $0xc4,%eax
80102421:	e8 f0 fd ff ff       	call   80102216 <lapicw>
  lapicw(ICRLO, INIT | LEVEL | ASSERT);
80102426:	ba 00 c5 00 00       	mov    $0xc500,%edx
8010242b:	b8 c0 00 00 00       	mov    $0xc0,%eax
80102430:	e8 e1 fd ff ff       	call   80102216 <lapicw>
  lapicw(ICRLO, INIT | LEVEL);
80102435:	ba 00 85 00 00       	mov    $0x8500,%edx
8010243a:	b8 c0 00 00 00       	mov    $0xc0,%eax
8010243f:	e8 d2 fd ff ff       	call   80102216 <lapicw>
  for(i = 0; i < 2; i++){
80102444:	bb 00 00 00 00       	mov    $0x0,%ebx
80102449:	eb 21                	jmp    8010246c <lapicstartap+0x8b>
    lapicw(ICRHI, apicid<<24);
8010244b:	89 f2                	mov    %esi,%edx
8010244d:	b8 c4 00 00 00       	mov    $0xc4,%eax
80102452:	e8 bf fd ff ff       	call   80102216 <lapicw>
    lapicw(ICRLO, STARTUP | (addr>>12));
80102457:	89 fa                	mov    %edi,%edx
80102459:	c1 ea 0c             	shr    $0xc,%edx
8010245c:	80 ce 06             	or     $0x6,%dh
8010245f:	b8 c0 00 00 00       	mov    $0xc0,%eax
80102464:	e8 ad fd ff ff       	call   80102216 <lapicw>
  for(i = 0; i < 2; i++){
80102469:	83 c3 01             	add    $0x1,%ebx
8010246c:	83 fb 01             	cmp    $0x1,%ebx
8010246f:	7e da                	jle    8010244b <lapicstartap+0x6a>
}
80102471:	5b                   	pop    %ebx
80102472:	5e                   	pop    %esi
80102473:	5f                   	pop    %edi
80102474:	5d                   	pop    %ebp
80102475:	c3                   	ret    

80102476 <cmostime>:

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80102476:	55                   	push   %ebp
80102477:	89 e5                	mov    %esp,%ebp
80102479:	57                   	push   %edi
8010247a:	56                   	push   %esi
8010247b:	53                   	push   %ebx
8010247c:	83 ec 3c             	sub    $0x3c,%esp
8010247f:	8b 75 08             	mov    0x8(%ebp),%esi
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);
80102482:	b8 0b 00 00 00       	mov    $0xb,%eax
80102487:	e8 a2 fd ff ff       	call   8010222e <cmos_read>

  bcd = (sb & (1 << 2)) == 0;
8010248c:	83 e0 04             	and    $0x4,%eax
8010248f:	89 c7                	mov    %eax,%edi

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
80102491:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102494:	e8 a9 fd ff ff       	call   80102242 <fill_rtcdate>
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
80102499:	b8 0a 00 00 00       	mov    $0xa,%eax
8010249e:	e8 8b fd ff ff       	call   8010222e <cmos_read>
801024a3:	a8 80                	test   $0x80,%al
801024a5:	75 ea                	jne    80102491 <cmostime+0x1b>
        continue;
    fill_rtcdate(&t2);
801024a7:	8d 5d b8             	lea    -0x48(%ebp),%ebx
801024aa:	89 d8                	mov    %ebx,%eax
801024ac:	e8 91 fd ff ff       	call   80102242 <fill_rtcdate>
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
801024b1:	83 ec 04             	sub    $0x4,%esp
801024b4:	6a 18                	push   $0x18
801024b6:	53                   	push   %ebx
801024b7:	8d 45 d0             	lea    -0x30(%ebp),%eax
801024ba:	50                   	push   %eax
801024bb:	e8 4d 1b 00 00       	call   8010400d <memcmp>
801024c0:	83 c4 10             	add    $0x10,%esp
801024c3:	85 c0                	test   %eax,%eax
801024c5:	75 ca                	jne    80102491 <cmostime+0x1b>
      break;
  }

  // convert
  if(bcd) {
801024c7:	85 ff                	test   %edi,%edi
801024c9:	0f 85 84 00 00 00    	jne    80102553 <cmostime+0xdd>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
801024cf:	8b 55 d0             	mov    -0x30(%ebp),%edx
801024d2:	89 d0                	mov    %edx,%eax
801024d4:	c1 e8 04             	shr    $0x4,%eax
801024d7:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
801024da:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
801024dd:	83 e2 0f             	and    $0xf,%edx
801024e0:	01 d0                	add    %edx,%eax
801024e2:	89 45 d0             	mov    %eax,-0x30(%ebp)
    CONV(minute);
801024e5:	8b 55 d4             	mov    -0x2c(%ebp),%edx
801024e8:	89 d0                	mov    %edx,%eax
801024ea:	c1 e8 04             	shr    $0x4,%eax
801024ed:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
801024f0:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
801024f3:	83 e2 0f             	and    $0xf,%edx
801024f6:	01 d0                	add    %edx,%eax
801024f8:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    CONV(hour  );
801024fb:	8b 55 d8             	mov    -0x28(%ebp),%edx
801024fe:	89 d0                	mov    %edx,%eax
80102500:	c1 e8 04             	shr    $0x4,%eax
80102503:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102506:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
80102509:	83 e2 0f             	and    $0xf,%edx
8010250c:	01 d0                	add    %edx,%eax
8010250e:	89 45 d8             	mov    %eax,-0x28(%ebp)
    CONV(day   );
80102511:	8b 55 dc             	mov    -0x24(%ebp),%edx
80102514:	89 d0                	mov    %edx,%eax
80102516:	c1 e8 04             	shr    $0x4,%eax
80102519:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
8010251c:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
8010251f:	83 e2 0f             	and    $0xf,%edx
80102522:	01 d0                	add    %edx,%eax
80102524:	89 45 dc             	mov    %eax,-0x24(%ebp)
    CONV(month );
80102527:	8b 55 e0             	mov    -0x20(%ebp),%edx
8010252a:	89 d0                	mov    %edx,%eax
8010252c:	c1 e8 04             	shr    $0x4,%eax
8010252f:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102532:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
80102535:	83 e2 0f             	and    $0xf,%edx
80102538:	01 d0                	add    %edx,%eax
8010253a:	89 45 e0             	mov    %eax,-0x20(%ebp)
    CONV(year  );
8010253d:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80102540:	89 d0                	mov    %edx,%eax
80102542:	c1 e8 04             	shr    $0x4,%eax
80102545:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102548:	8d 04 09             	lea    (%ecx,%ecx,1),%eax
8010254b:	83 e2 0f             	and    $0xf,%edx
8010254e:	01 d0                	add    %edx,%eax
80102550:	89 45 e4             	mov    %eax,-0x1c(%ebp)
#undef     CONV
  }

  *r = t1;
80102553:	8b 45 d0             	mov    -0x30(%ebp),%eax
80102556:	89 06                	mov    %eax,(%esi)
80102558:	8b 45 d4             	mov    -0x2c(%ebp),%eax
8010255b:	89 46 04             	mov    %eax,0x4(%esi)
8010255e:	8b 45 d8             	mov    -0x28(%ebp),%eax
80102561:	89 46 08             	mov    %eax,0x8(%esi)
80102564:	8b 45 dc             	mov    -0x24(%ebp),%eax
80102567:	89 46 0c             	mov    %eax,0xc(%esi)
8010256a:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010256d:	89 46 10             	mov    %eax,0x10(%esi)
80102570:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80102573:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102576:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
8010257d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102580:	5b                   	pop    %ebx
80102581:	5e                   	pop    %esi
80102582:	5f                   	pop    %edi
80102583:	5d                   	pop    %ebp
80102584:	c3                   	ret    

80102585 <read_head>:
}

// Read the log header from disk into the in-memory log header
static void
read_head(void)
{
80102585:	55                   	push   %ebp
80102586:	89 e5                	mov    %esp,%ebp
80102588:	53                   	push   %ebx
80102589:	83 ec 0c             	sub    $0xc,%esp
  struct buf *buf = bread(log.dev, log.start);
8010258c:	ff 35 d4 26 11 80    	pushl  0x801126d4
80102592:	ff 35 e4 26 11 80    	pushl  0x801126e4
80102598:	e8 cf db ff ff       	call   8010016c <bread>
  struct logheader *lh = (struct logheader *) (buf->data);
  int i;
  log.lh.n = lh->n;
8010259d:	8b 58 5c             	mov    0x5c(%eax),%ebx
801025a0:	89 1d e8 26 11 80    	mov    %ebx,0x801126e8
  for (i = 0; i < log.lh.n; i++) {
801025a6:	83 c4 10             	add    $0x10,%esp
801025a9:	ba 00 00 00 00       	mov    $0x0,%edx
801025ae:	eb 0e                	jmp    801025be <read_head+0x39>
    log.lh.block[i] = lh->block[i];
801025b0:	8b 4c 90 60          	mov    0x60(%eax,%edx,4),%ecx
801025b4:	89 0c 95 ec 26 11 80 	mov    %ecx,-0x7feed914(,%edx,4)
  for (i = 0; i < log.lh.n; i++) {
801025bb:	83 c2 01             	add    $0x1,%edx
801025be:	39 d3                	cmp    %edx,%ebx
801025c0:	7f ee                	jg     801025b0 <read_head+0x2b>
  }
  brelse(buf);
801025c2:	83 ec 0c             	sub    $0xc,%esp
801025c5:	50                   	push   %eax
801025c6:	e8 0a dc ff ff       	call   801001d5 <brelse>
}
801025cb:	83 c4 10             	add    $0x10,%esp
801025ce:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801025d1:	c9                   	leave  
801025d2:	c3                   	ret    

801025d3 <install_trans>:
{
801025d3:	55                   	push   %ebp
801025d4:	89 e5                	mov    %esp,%ebp
801025d6:	57                   	push   %edi
801025d7:	56                   	push   %esi
801025d8:	53                   	push   %ebx
801025d9:	83 ec 0c             	sub    $0xc,%esp
  for (tail = 0; tail < log.lh.n; tail++) {
801025dc:	bb 00 00 00 00       	mov    $0x0,%ebx
801025e1:	eb 66                	jmp    80102649 <install_trans+0x76>
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
801025e3:	89 d8                	mov    %ebx,%eax
801025e5:	03 05 d4 26 11 80    	add    0x801126d4,%eax
801025eb:	83 c0 01             	add    $0x1,%eax
801025ee:	83 ec 08             	sub    $0x8,%esp
801025f1:	50                   	push   %eax
801025f2:	ff 35 e4 26 11 80    	pushl  0x801126e4
801025f8:	e8 6f db ff ff       	call   8010016c <bread>
801025fd:	89 c7                	mov    %eax,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
801025ff:	83 c4 08             	add    $0x8,%esp
80102602:	ff 34 9d ec 26 11 80 	pushl  -0x7feed914(,%ebx,4)
80102609:	ff 35 e4 26 11 80    	pushl  0x801126e4
8010260f:	e8 58 db ff ff       	call   8010016c <bread>
80102614:	89 c6                	mov    %eax,%esi
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102616:	8d 57 5c             	lea    0x5c(%edi),%edx
80102619:	8d 40 5c             	lea    0x5c(%eax),%eax
8010261c:	83 c4 0c             	add    $0xc,%esp
8010261f:	68 00 02 00 00       	push   $0x200
80102624:	52                   	push   %edx
80102625:	50                   	push   %eax
80102626:	e8 17 1a 00 00       	call   80104042 <memmove>
    bwrite(dbuf);  // write dst to disk
8010262b:	89 34 24             	mov    %esi,(%esp)
8010262e:	e8 67 db ff ff       	call   8010019a <bwrite>
    brelse(lbuf);
80102633:	89 3c 24             	mov    %edi,(%esp)
80102636:	e8 9a db ff ff       	call   801001d5 <brelse>
    brelse(dbuf);
8010263b:	89 34 24             	mov    %esi,(%esp)
8010263e:	e8 92 db ff ff       	call   801001d5 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102643:	83 c3 01             	add    $0x1,%ebx
80102646:	83 c4 10             	add    $0x10,%esp
80102649:	39 1d e8 26 11 80    	cmp    %ebx,0x801126e8
8010264f:	7f 92                	jg     801025e3 <install_trans+0x10>
}
80102651:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102654:	5b                   	pop    %ebx
80102655:	5e                   	pop    %esi
80102656:	5f                   	pop    %edi
80102657:	5d                   	pop    %ebp
80102658:	c3                   	ret    

80102659 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102659:	55                   	push   %ebp
8010265a:	89 e5                	mov    %esp,%ebp
8010265c:	53                   	push   %ebx
8010265d:	83 ec 0c             	sub    $0xc,%esp
  struct buf *buf = bread(log.dev, log.start);
80102660:	ff 35 d4 26 11 80    	pushl  0x801126d4
80102666:	ff 35 e4 26 11 80    	pushl  0x801126e4
8010266c:	e8 fb da ff ff       	call   8010016c <bread>
80102671:	89 c3                	mov    %eax,%ebx
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
80102673:	8b 0d e8 26 11 80    	mov    0x801126e8,%ecx
80102679:	89 48 5c             	mov    %ecx,0x5c(%eax)
  for (i = 0; i < log.lh.n; i++) {
8010267c:	83 c4 10             	add    $0x10,%esp
8010267f:	b8 00 00 00 00       	mov    $0x0,%eax
80102684:	eb 0e                	jmp    80102694 <write_head+0x3b>
    hb->block[i] = log.lh.block[i];
80102686:	8b 14 85 ec 26 11 80 	mov    -0x7feed914(,%eax,4),%edx
8010268d:	89 54 83 60          	mov    %edx,0x60(%ebx,%eax,4)
  for (i = 0; i < log.lh.n; i++) {
80102691:	83 c0 01             	add    $0x1,%eax
80102694:	39 c1                	cmp    %eax,%ecx
80102696:	7f ee                	jg     80102686 <write_head+0x2d>
  }
  bwrite(buf);
80102698:	83 ec 0c             	sub    $0xc,%esp
8010269b:	53                   	push   %ebx
8010269c:	e8 f9 da ff ff       	call   8010019a <bwrite>
  brelse(buf);
801026a1:	89 1c 24             	mov    %ebx,(%esp)
801026a4:	e8 2c db ff ff       	call   801001d5 <brelse>
}
801026a9:	83 c4 10             	add    $0x10,%esp
801026ac:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801026af:	c9                   	leave  
801026b0:	c3                   	ret    

801026b1 <recover_from_log>:

static void
recover_from_log(void)
{
801026b1:	55                   	push   %ebp
801026b2:	89 e5                	mov    %esp,%ebp
801026b4:	83 ec 08             	sub    $0x8,%esp
  read_head();
801026b7:	e8 c9 fe ff ff       	call   80102585 <read_head>
  install_trans(); // if committed, copy from log to disk
801026bc:	e8 12 ff ff ff       	call   801025d3 <install_trans>
  log.lh.n = 0;
801026c1:	c7 05 e8 26 11 80 00 	movl   $0x0,0x801126e8
801026c8:	00 00 00 
  write_head(); // clear the log
801026cb:	e8 89 ff ff ff       	call   80102659 <write_head>
}
801026d0:	c9                   	leave  
801026d1:	c3                   	ret    

801026d2 <write_log>:
}

// Copy modified blocks from cache to log.
static void
write_log(void)
{
801026d2:	55                   	push   %ebp
801026d3:	89 e5                	mov    %esp,%ebp
801026d5:	57                   	push   %edi
801026d6:	56                   	push   %esi
801026d7:	53                   	push   %ebx
801026d8:	83 ec 0c             	sub    $0xc,%esp
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
801026db:	bb 00 00 00 00       	mov    $0x0,%ebx
801026e0:	eb 66                	jmp    80102748 <write_log+0x76>
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
801026e2:	89 d8                	mov    %ebx,%eax
801026e4:	03 05 d4 26 11 80    	add    0x801126d4,%eax
801026ea:	83 c0 01             	add    $0x1,%eax
801026ed:	83 ec 08             	sub    $0x8,%esp
801026f0:	50                   	push   %eax
801026f1:	ff 35 e4 26 11 80    	pushl  0x801126e4
801026f7:	e8 70 da ff ff       	call   8010016c <bread>
801026fc:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
801026fe:	83 c4 08             	add    $0x8,%esp
80102701:	ff 34 9d ec 26 11 80 	pushl  -0x7feed914(,%ebx,4)
80102708:	ff 35 e4 26 11 80    	pushl  0x801126e4
8010270e:	e8 59 da ff ff       	call   8010016c <bread>
80102713:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
80102715:	8d 50 5c             	lea    0x5c(%eax),%edx
80102718:	8d 46 5c             	lea    0x5c(%esi),%eax
8010271b:	83 c4 0c             	add    $0xc,%esp
8010271e:	68 00 02 00 00       	push   $0x200
80102723:	52                   	push   %edx
80102724:	50                   	push   %eax
80102725:	e8 18 19 00 00       	call   80104042 <memmove>
    bwrite(to);  // write the log
8010272a:	89 34 24             	mov    %esi,(%esp)
8010272d:	e8 68 da ff ff       	call   8010019a <bwrite>
    brelse(from);
80102732:	89 3c 24             	mov    %edi,(%esp)
80102735:	e8 9b da ff ff       	call   801001d5 <brelse>
    brelse(to);
8010273a:	89 34 24             	mov    %esi,(%esp)
8010273d:	e8 93 da ff ff       	call   801001d5 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102742:	83 c3 01             	add    $0x1,%ebx
80102745:	83 c4 10             	add    $0x10,%esp
80102748:	39 1d e8 26 11 80    	cmp    %ebx,0x801126e8
8010274e:	7f 92                	jg     801026e2 <write_log+0x10>
  }
}
80102750:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102753:	5b                   	pop    %ebx
80102754:	5e                   	pop    %esi
80102755:	5f                   	pop    %edi
80102756:	5d                   	pop    %ebp
80102757:	c3                   	ret    

80102758 <commit>:

static void
commit()
{
  if (log.lh.n > 0) {
80102758:	83 3d e8 26 11 80 00 	cmpl   $0x0,0x801126e8
8010275f:	7e 26                	jle    80102787 <commit+0x2f>
{
80102761:	55                   	push   %ebp
80102762:	89 e5                	mov    %esp,%ebp
80102764:	83 ec 08             	sub    $0x8,%esp
    write_log();     // Write modified blocks from cache to log
80102767:	e8 66 ff ff ff       	call   801026d2 <write_log>
    write_head();    // Write header to disk -- the real commit
8010276c:	e8 e8 fe ff ff       	call   80102659 <write_head>
    install_trans(); // Now install writes to home locations
80102771:	e8 5d fe ff ff       	call   801025d3 <install_trans>
    log.lh.n = 0;
80102776:	c7 05 e8 26 11 80 00 	movl   $0x0,0x801126e8
8010277d:	00 00 00 
    write_head();    // Erase the transaction from the log
80102780:	e8 d4 fe ff ff       	call   80102659 <write_head>
  }
}
80102785:	c9                   	leave  
80102786:	c3                   	ret    
80102787:	f3 c3                	repz ret 

80102789 <initlog>:
{
80102789:	55                   	push   %ebp
8010278a:	89 e5                	mov    %esp,%ebp
8010278c:	53                   	push   %ebx
8010278d:	83 ec 2c             	sub    $0x2c,%esp
80102790:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102793:	68 00 72 10 80       	push   $0x80107200
80102798:	68 a0 26 11 80       	push   $0x801126a0
8010279d:	e8 3d 16 00 00       	call   80103ddf <initlock>
  readsb(dev, &sb);
801027a2:	83 c4 08             	add    $0x8,%esp
801027a5:	8d 45 dc             	lea    -0x24(%ebp),%eax
801027a8:	50                   	push   %eax
801027a9:	53                   	push   %ebx
801027aa:	e8 f7 ea ff ff       	call   801012a6 <readsb>
  log.start = sb.logstart;
801027af:	8b 45 ec             	mov    -0x14(%ebp),%eax
801027b2:	a3 d4 26 11 80       	mov    %eax,0x801126d4
  log.size = sb.nlog;
801027b7:	8b 45 e8             	mov    -0x18(%ebp),%eax
801027ba:	a3 d8 26 11 80       	mov    %eax,0x801126d8
  log.dev = dev;
801027bf:	89 1d e4 26 11 80    	mov    %ebx,0x801126e4
  recover_from_log();
801027c5:	e8 e7 fe ff ff       	call   801026b1 <recover_from_log>
}
801027ca:	83 c4 10             	add    $0x10,%esp
801027cd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801027d0:	c9                   	leave  
801027d1:	c3                   	ret    

801027d2 <begin_op>:
{
801027d2:	55                   	push   %ebp
801027d3:	89 e5                	mov    %esp,%ebp
801027d5:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
801027d8:	68 a0 26 11 80       	push   $0x801126a0
801027dd:	e8 39 17 00 00       	call   80103f1b <acquire>
801027e2:	83 c4 10             	add    $0x10,%esp
801027e5:	eb 15                	jmp    801027fc <begin_op+0x2a>
      sleep(&log, &log.lock);
801027e7:	83 ec 08             	sub    $0x8,%esp
801027ea:	68 a0 26 11 80       	push   $0x801126a0
801027ef:	68 a0 26 11 80       	push   $0x801126a0
801027f4:	e8 9a 0e 00 00       	call   80103693 <sleep>
801027f9:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
801027fc:	83 3d e0 26 11 80 00 	cmpl   $0x0,0x801126e0
80102803:	75 e2                	jne    801027e7 <begin_op+0x15>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102805:	a1 dc 26 11 80       	mov    0x801126dc,%eax
8010280a:	83 c0 01             	add    $0x1,%eax
8010280d:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102810:	8d 14 09             	lea    (%ecx,%ecx,1),%edx
80102813:	03 15 e8 26 11 80    	add    0x801126e8,%edx
80102819:	83 fa 1e             	cmp    $0x1e,%edx
8010281c:	7e 17                	jle    80102835 <begin_op+0x63>
      sleep(&log, &log.lock);
8010281e:	83 ec 08             	sub    $0x8,%esp
80102821:	68 a0 26 11 80       	push   $0x801126a0
80102826:	68 a0 26 11 80       	push   $0x801126a0
8010282b:	e8 63 0e 00 00       	call   80103693 <sleep>
80102830:	83 c4 10             	add    $0x10,%esp
80102833:	eb c7                	jmp    801027fc <begin_op+0x2a>
      log.outstanding += 1;
80102835:	a3 dc 26 11 80       	mov    %eax,0x801126dc
      release(&log.lock);
8010283a:	83 ec 0c             	sub    $0xc,%esp
8010283d:	68 a0 26 11 80       	push   $0x801126a0
80102842:	e8 39 17 00 00       	call   80103f80 <release>
}
80102847:	83 c4 10             	add    $0x10,%esp
8010284a:	c9                   	leave  
8010284b:	c3                   	ret    

8010284c <end_op>:
{
8010284c:	55                   	push   %ebp
8010284d:	89 e5                	mov    %esp,%ebp
8010284f:	53                   	push   %ebx
80102850:	83 ec 10             	sub    $0x10,%esp
  acquire(&log.lock);
80102853:	68 a0 26 11 80       	push   $0x801126a0
80102858:	e8 be 16 00 00       	call   80103f1b <acquire>
  log.outstanding -= 1;
8010285d:	a1 dc 26 11 80       	mov    0x801126dc,%eax
80102862:	83 e8 01             	sub    $0x1,%eax
80102865:	a3 dc 26 11 80       	mov    %eax,0x801126dc
  if(log.committing)
8010286a:	8b 1d e0 26 11 80    	mov    0x801126e0,%ebx
80102870:	83 c4 10             	add    $0x10,%esp
80102873:	85 db                	test   %ebx,%ebx
80102875:	75 2c                	jne    801028a3 <end_op+0x57>
  if(log.outstanding == 0){
80102877:	85 c0                	test   %eax,%eax
80102879:	75 35                	jne    801028b0 <end_op+0x64>
    log.committing = 1;
8010287b:	c7 05 e0 26 11 80 01 	movl   $0x1,0x801126e0
80102882:	00 00 00 
    do_commit = 1;
80102885:	bb 01 00 00 00       	mov    $0x1,%ebx
  release(&log.lock);
8010288a:	83 ec 0c             	sub    $0xc,%esp
8010288d:	68 a0 26 11 80       	push   $0x801126a0
80102892:	e8 e9 16 00 00       	call   80103f80 <release>
  if(do_commit){
80102897:	83 c4 10             	add    $0x10,%esp
8010289a:	85 db                	test   %ebx,%ebx
8010289c:	75 24                	jne    801028c2 <end_op+0x76>
}
8010289e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801028a1:	c9                   	leave  
801028a2:	c3                   	ret    
    panic("log.committing");
801028a3:	83 ec 0c             	sub    $0xc,%esp
801028a6:	68 04 72 10 80       	push   $0x80107204
801028ab:	e8 98 da ff ff       	call   80100348 <panic>
    wakeup(&log);
801028b0:	83 ec 0c             	sub    $0xc,%esp
801028b3:	68 a0 26 11 80       	push   $0x801126a0
801028b8:	e8 3e 0f 00 00       	call   801037fb <wakeup>
801028bd:	83 c4 10             	add    $0x10,%esp
801028c0:	eb c8                	jmp    8010288a <end_op+0x3e>
    commit();
801028c2:	e8 91 fe ff ff       	call   80102758 <commit>
    acquire(&log.lock);
801028c7:	83 ec 0c             	sub    $0xc,%esp
801028ca:	68 a0 26 11 80       	push   $0x801126a0
801028cf:	e8 47 16 00 00       	call   80103f1b <acquire>
    log.committing = 0;
801028d4:	c7 05 e0 26 11 80 00 	movl   $0x0,0x801126e0
801028db:	00 00 00 
    wakeup(&log);
801028de:	c7 04 24 a0 26 11 80 	movl   $0x801126a0,(%esp)
801028e5:	e8 11 0f 00 00       	call   801037fb <wakeup>
    release(&log.lock);
801028ea:	c7 04 24 a0 26 11 80 	movl   $0x801126a0,(%esp)
801028f1:	e8 8a 16 00 00       	call   80103f80 <release>
801028f6:	83 c4 10             	add    $0x10,%esp
}
801028f9:	eb a3                	jmp    8010289e <end_op+0x52>

801028fb <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
801028fb:	55                   	push   %ebp
801028fc:	89 e5                	mov    %esp,%ebp
801028fe:	53                   	push   %ebx
801028ff:	83 ec 04             	sub    $0x4,%esp
80102902:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80102905:	8b 15 e8 26 11 80    	mov    0x801126e8,%edx
8010290b:	83 fa 1d             	cmp    $0x1d,%edx
8010290e:	7f 45                	jg     80102955 <log_write+0x5a>
80102910:	a1 d8 26 11 80       	mov    0x801126d8,%eax
80102915:	83 e8 01             	sub    $0x1,%eax
80102918:	39 c2                	cmp    %eax,%edx
8010291a:	7d 39                	jge    80102955 <log_write+0x5a>
    panic("too big a transaction");
  if (log.outstanding < 1)
8010291c:	83 3d dc 26 11 80 00 	cmpl   $0x0,0x801126dc
80102923:	7e 3d                	jle    80102962 <log_write+0x67>
    panic("log_write outside of trans");

  acquire(&log.lock);
80102925:	83 ec 0c             	sub    $0xc,%esp
80102928:	68 a0 26 11 80       	push   $0x801126a0
8010292d:	e8 e9 15 00 00       	call   80103f1b <acquire>
  for (i = 0; i < log.lh.n; i++) {
80102932:	83 c4 10             	add    $0x10,%esp
80102935:	b8 00 00 00 00       	mov    $0x0,%eax
8010293a:	8b 15 e8 26 11 80    	mov    0x801126e8,%edx
80102940:	39 c2                	cmp    %eax,%edx
80102942:	7e 2b                	jle    8010296f <log_write+0x74>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80102944:	8b 4b 08             	mov    0x8(%ebx),%ecx
80102947:	39 0c 85 ec 26 11 80 	cmp    %ecx,-0x7feed914(,%eax,4)
8010294e:	74 1f                	je     8010296f <log_write+0x74>
  for (i = 0; i < log.lh.n; i++) {
80102950:	83 c0 01             	add    $0x1,%eax
80102953:	eb e5                	jmp    8010293a <log_write+0x3f>
    panic("too big a transaction");
80102955:	83 ec 0c             	sub    $0xc,%esp
80102958:	68 13 72 10 80       	push   $0x80107213
8010295d:	e8 e6 d9 ff ff       	call   80100348 <panic>
    panic("log_write outside of trans");
80102962:	83 ec 0c             	sub    $0xc,%esp
80102965:	68 29 72 10 80       	push   $0x80107229
8010296a:	e8 d9 d9 ff ff       	call   80100348 <panic>
      break;
  }
  log.lh.block[i] = b->blockno;
8010296f:	8b 4b 08             	mov    0x8(%ebx),%ecx
80102972:	89 0c 85 ec 26 11 80 	mov    %ecx,-0x7feed914(,%eax,4)
  if (i == log.lh.n)
80102979:	39 c2                	cmp    %eax,%edx
8010297b:	74 18                	je     80102995 <log_write+0x9a>
    log.lh.n++;
  b->flags |= B_DIRTY; // prevent eviction
8010297d:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
80102980:	83 ec 0c             	sub    $0xc,%esp
80102983:	68 a0 26 11 80       	push   $0x801126a0
80102988:	e8 f3 15 00 00       	call   80103f80 <release>
}
8010298d:	83 c4 10             	add    $0x10,%esp
80102990:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102993:	c9                   	leave  
80102994:	c3                   	ret    
    log.lh.n++;
80102995:	83 c2 01             	add    $0x1,%edx
80102998:	89 15 e8 26 11 80    	mov    %edx,0x801126e8
8010299e:	eb dd                	jmp    8010297d <log_write+0x82>

801029a0 <startothers>:
pde_t entrypgdir[];  // For entry.S

// Start the non-boot (AP) processors.
static void
startothers(void)
{
801029a0:	55                   	push   %ebp
801029a1:	89 e5                	mov    %esp,%ebp
801029a3:	53                   	push   %ebx
801029a4:	83 ec 08             	sub    $0x8,%esp

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
801029a7:	68 8a 00 00 00       	push   $0x8a
801029ac:	68 8c a4 10 80       	push   $0x8010a48c
801029b1:	68 00 70 00 80       	push   $0x80007000
801029b6:	e8 87 16 00 00       	call   80104042 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
801029bb:	83 c4 10             	add    $0x10,%esp
801029be:	bb a0 27 11 80       	mov    $0x801127a0,%ebx
801029c3:	eb 13                	jmp    801029d8 <startothers+0x38>
801029c5:	83 ec 0c             	sub    $0xc,%esp
801029c8:	68 28 6f 10 80       	push   $0x80106f28
801029cd:	e8 76 d9 ff ff       	call   80100348 <panic>
801029d2:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
801029d8:	69 05 20 2d 11 80 b0 	imul   $0xb0,0x80112d20,%eax
801029df:	00 00 00 
801029e2:	05 a0 27 11 80       	add    $0x801127a0,%eax
801029e7:	39 d8                	cmp    %ebx,%eax
801029e9:	76 58                	jbe    80102a43 <startothers+0xa3>
    if(c == mycpu())  // We've started already.
801029eb:	e8 f0 07 00 00       	call   801031e0 <mycpu>
801029f0:	39 d8                	cmp    %ebx,%eax
801029f2:	74 de                	je     801029d2 <startothers+0x32>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
801029f4:	e8 d6 f6 ff ff       	call   801020cf <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
801029f9:	05 00 10 00 00       	add    $0x1000,%eax
801029fe:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc
    *(void(**)(void))(code-8) = mpenter;
80102a03:	c7 05 f8 6f 00 80 87 	movl   $0x80102a87,0x80006ff8
80102a0a:	2a 10 80 
    if (a < (void*) KERNBASE)
80102a0d:	b8 00 90 10 80       	mov    $0x80109000,%eax
80102a12:	3d ff ff ff 7f       	cmp    $0x7fffffff,%eax
80102a17:	76 ac                	jbe    801029c5 <startothers+0x25>
    *(int**)(code-12) = (void *) V2P(entrypgdir);
80102a19:	c7 05 f4 6f 00 80 00 	movl   $0x109000,0x80006ff4
80102a20:	90 10 00 

    lapicstartap(c->apicid, V2P(code));
80102a23:	83 ec 08             	sub    $0x8,%esp
80102a26:	68 00 70 00 00       	push   $0x7000
80102a2b:	0f b6 03             	movzbl (%ebx),%eax
80102a2e:	50                   	push   %eax
80102a2f:	e8 ad f9 ff ff       	call   801023e1 <lapicstartap>

    // wait for cpu to finish mpmain()
    while(c->started == 0)
80102a34:	83 c4 10             	add    $0x10,%esp
80102a37:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
80102a3d:	85 c0                	test   %eax,%eax
80102a3f:	74 f6                	je     80102a37 <startothers+0x97>
80102a41:	eb 8f                	jmp    801029d2 <startothers+0x32>
      ;
  }
}
80102a43:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102a46:	c9                   	leave  
80102a47:	c3                   	ret    

80102a48 <mpmain>:
{
80102a48:	55                   	push   %ebp
80102a49:	89 e5                	mov    %esp,%ebp
80102a4b:	53                   	push   %ebx
80102a4c:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80102a4f:	e8 e8 07 00 00       	call   8010323c <cpuid>
80102a54:	89 c3                	mov    %eax,%ebx
80102a56:	e8 e1 07 00 00       	call   8010323c <cpuid>
80102a5b:	83 ec 04             	sub    $0x4,%esp
80102a5e:	53                   	push   %ebx
80102a5f:	50                   	push   %eax
80102a60:	68 44 72 10 80       	push   $0x80107244
80102a65:	e8 a1 db ff ff       	call   8010060b <cprintf>
  idtinit();       // load idt register
80102a6a:	e8 6e 27 00 00       	call   801051dd <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80102a6f:	e8 6c 07 00 00       	call   801031e0 <mycpu>
80102a74:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80102a76:	b8 01 00 00 00       	mov    $0x1,%eax
80102a7b:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
80102a82:	e8 fb 0f 00 00       	call   80103a82 <scheduler>

80102a87 <mpenter>:
{
80102a87:	55                   	push   %ebp
80102a88:	89 e5                	mov    %esp,%ebp
80102a8a:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80102a8d:	e8 7b 38 00 00       	call   8010630d <switchkvm>
  seginit();
80102a92:	e8 80 36 00 00       	call   80106117 <seginit>
  lapicinit();
80102a97:	e8 fc f7 ff ff       	call   80102298 <lapicinit>
  mpmain();
80102a9c:	e8 a7 ff ff ff       	call   80102a48 <mpmain>

80102aa1 <main>:
{
80102aa1:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80102aa5:	83 e4 f0             	and    $0xfffffff0,%esp
80102aa8:	ff 71 fc             	pushl  -0x4(%ecx)
80102aab:	55                   	push   %ebp
80102aac:	89 e5                	mov    %esp,%ebp
80102aae:	51                   	push   %ecx
80102aaf:	83 ec 0c             	sub    $0xc,%esp
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80102ab2:	68 00 00 40 80       	push   $0x80400000
80102ab7:	68 18 37 12 80       	push   $0x80123718
80102abc:	e8 bc f5 ff ff       	call   8010207d <kinit1>
  kvmalloc();      // kernel page table
80102ac1:	e8 b1 3d 00 00       	call   80106877 <kvmalloc>
  mpinit();        // detect other processors
80102ac6:	e8 e7 01 00 00       	call   80102cb2 <mpinit>
  lapicinit();     // interrupt controller
80102acb:	e8 c8 f7 ff ff       	call   80102298 <lapicinit>
  seginit();       // segment descriptors
80102ad0:	e8 42 36 00 00       	call   80106117 <seginit>
  picinit();       // disable pic
80102ad5:	e8 a0 02 00 00       	call   80102d7a <picinit>
  ioapicinit();    // another interrupt controller
80102ada:	e8 09 f4 ff ff       	call   80101ee8 <ioapicinit>
  consoleinit();   // console hardware
80102adf:	e8 aa dd ff ff       	call   8010088e <consoleinit>
  uartinit();      // serial port
80102ae4:	e8 6b 2a 00 00       	call   80105554 <uartinit>
  pinit();         // process table
80102ae9:	e8 d8 06 00 00       	call   801031c6 <pinit>
  tvinit();        // trap vectors
80102aee:	e8 39 26 00 00       	call   8010512c <tvinit>
  binit();         // buffer cache
80102af3:	e8 fc d5 ff ff       	call   801000f4 <binit>
  fileinit();      // file table
80102af8:	e8 16 e1 ff ff       	call   80100c13 <fileinit>
  ideinit();       // disk 
80102afd:	e8 ec f1 ff ff       	call   80101cee <ideinit>
  startothers();   // start other processors
80102b02:	e8 99 fe ff ff       	call   801029a0 <startothers>
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
80102b07:	83 c4 08             	add    $0x8,%esp
80102b0a:	68 00 00 00 8e       	push   $0x8e000000
80102b0f:	68 00 00 40 80       	push   $0x80400000
80102b14:	e8 96 f5 ff ff       	call   801020af <kinit2>
  userinit();      // first user process
80102b19:	e8 5d 07 00 00       	call   8010327b <userinit>
  mpmain();        // finish this processor's setup
80102b1e:	e8 25 ff ff ff       	call   80102a48 <mpmain>

80102b23 <sum>:
int ncpu;
uchar ioapicid;

static uchar
sum(uchar *addr, int len)
{
80102b23:	55                   	push   %ebp
80102b24:	89 e5                	mov    %esp,%ebp
80102b26:	56                   	push   %esi
80102b27:	53                   	push   %ebx
  int i, sum;

  sum = 0;
80102b28:	bb 00 00 00 00       	mov    $0x0,%ebx
  for(i=0; i<len; i++)
80102b2d:	b9 00 00 00 00       	mov    $0x0,%ecx
80102b32:	eb 09                	jmp    80102b3d <sum+0x1a>
    sum += addr[i];
80102b34:	0f b6 34 08          	movzbl (%eax,%ecx,1),%esi
80102b38:	01 f3                	add    %esi,%ebx
  for(i=0; i<len; i++)
80102b3a:	83 c1 01             	add    $0x1,%ecx
80102b3d:	39 d1                	cmp    %edx,%ecx
80102b3f:	7c f3                	jl     80102b34 <sum+0x11>
  return sum;
}
80102b41:	89 d8                	mov    %ebx,%eax
80102b43:	5b                   	pop    %ebx
80102b44:	5e                   	pop    %esi
80102b45:	5d                   	pop    %ebp
80102b46:	c3                   	ret    

80102b47 <mpsearch1>:

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80102b47:	55                   	push   %ebp
80102b48:	89 e5                	mov    %esp,%ebp
80102b4a:	56                   	push   %esi
80102b4b:	53                   	push   %ebx
}

// Convert physical address to kernel virtual address
static inline void *P2V(uint a) {
    extern void panic(char*) __attribute__((noreturn));
    if (a > KERNBASE)
80102b4c:	3d 00 00 00 80       	cmp    $0x80000000,%eax
80102b51:	77 0b                	ja     80102b5e <mpsearch1+0x17>
        panic("P2V on address > KERNBASE");
    return (char*)a + KERNBASE;
80102b53:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
  uchar *e, *p, *addr;

  addr = P2V(a);
  e = addr+len;
80102b59:	8d 34 13             	lea    (%ebx,%edx,1),%esi
  for(p = addr; p < e; p += sizeof(struct mp))
80102b5c:	eb 10                	jmp    80102b6e <mpsearch1+0x27>
        panic("P2V on address > KERNBASE");
80102b5e:	83 ec 0c             	sub    $0xc,%esp
80102b61:	68 58 72 10 80       	push   $0x80107258
80102b66:	e8 dd d7 ff ff       	call   80100348 <panic>
80102b6b:	83 c3 10             	add    $0x10,%ebx
80102b6e:	39 f3                	cmp    %esi,%ebx
80102b70:	73 29                	jae    80102b9b <mpsearch1+0x54>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80102b72:	83 ec 04             	sub    $0x4,%esp
80102b75:	6a 04                	push   $0x4
80102b77:	68 72 72 10 80       	push   $0x80107272
80102b7c:	53                   	push   %ebx
80102b7d:	e8 8b 14 00 00       	call   8010400d <memcmp>
80102b82:	83 c4 10             	add    $0x10,%esp
80102b85:	85 c0                	test   %eax,%eax
80102b87:	75 e2                	jne    80102b6b <mpsearch1+0x24>
80102b89:	ba 10 00 00 00       	mov    $0x10,%edx
80102b8e:	89 d8                	mov    %ebx,%eax
80102b90:	e8 8e ff ff ff       	call   80102b23 <sum>
80102b95:	84 c0                	test   %al,%al
80102b97:	75 d2                	jne    80102b6b <mpsearch1+0x24>
80102b99:	eb 05                	jmp    80102ba0 <mpsearch1+0x59>
      return (struct mp*)p;
  return 0;
80102b9b:	bb 00 00 00 00       	mov    $0x0,%ebx
}
80102ba0:	89 d8                	mov    %ebx,%eax
80102ba2:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102ba5:	5b                   	pop    %ebx
80102ba6:	5e                   	pop    %esi
80102ba7:	5d                   	pop    %ebp
80102ba8:	c3                   	ret    

80102ba9 <mpsearch>:
// 1) in the first KB of the EBDA;
// 2) in the last KB of system base memory;
// 3) in the BIOS ROM between 0xE0000 and 0xFFFFF.
static struct mp*
mpsearch(void)
{
80102ba9:	55                   	push   %ebp
80102baa:	89 e5                	mov    %esp,%ebp
80102bac:	83 ec 08             	sub    $0x8,%esp
  uchar *bda;
  uint p;
  struct mp *mp;

  bda = (uchar *) P2V(0x400);
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80102baf:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
80102bb6:	c1 e0 08             	shl    $0x8,%eax
80102bb9:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
80102bc0:	09 d0                	or     %edx,%eax
80102bc2:	c1 e0 04             	shl    $0x4,%eax
80102bc5:	85 c0                	test   %eax,%eax
80102bc7:	74 1f                	je     80102be8 <mpsearch+0x3f>
    if((mp = mpsearch1(p, 1024)))
80102bc9:	ba 00 04 00 00       	mov    $0x400,%edx
80102bce:	e8 74 ff ff ff       	call   80102b47 <mpsearch1>
80102bd3:	85 c0                	test   %eax,%eax
80102bd5:	75 0f                	jne    80102be6 <mpsearch+0x3d>
  } else {
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
    if((mp = mpsearch1(p-1024, 1024)))
      return mp;
  }
  return mpsearch1(0xF0000, 0x10000);
80102bd7:	ba 00 00 01 00       	mov    $0x10000,%edx
80102bdc:	b8 00 00 0f 00       	mov    $0xf0000,%eax
80102be1:	e8 61 ff ff ff       	call   80102b47 <mpsearch1>
}
80102be6:	c9                   	leave  
80102be7:	c3                   	ret    
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
80102be8:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
80102bef:	c1 e0 08             	shl    $0x8,%eax
80102bf2:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
80102bf9:	09 d0                	or     %edx,%eax
80102bfb:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
80102bfe:	2d 00 04 00 00       	sub    $0x400,%eax
80102c03:	ba 00 04 00 00       	mov    $0x400,%edx
80102c08:	e8 3a ff ff ff       	call   80102b47 <mpsearch1>
80102c0d:	85 c0                	test   %eax,%eax
80102c0f:	75 d5                	jne    80102be6 <mpsearch+0x3d>
80102c11:	eb c4                	jmp    80102bd7 <mpsearch+0x2e>

80102c13 <mpconfig>:
// Check for correct signature, calculate the checksum and,
// if correct, check the version.
// To do: check extended table checksum.
static struct mpconf*
mpconfig(struct mp **pmp)
{
80102c13:	55                   	push   %ebp
80102c14:	89 e5                	mov    %esp,%ebp
80102c16:	57                   	push   %edi
80102c17:	56                   	push   %esi
80102c18:	53                   	push   %ebx
80102c19:	83 ec 0c             	sub    $0xc,%esp
80102c1c:	89 c7                	mov    %eax,%edi
  struct mpconf *conf;
  struct mp *mp;

  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
80102c1e:	e8 86 ff ff ff       	call   80102ba9 <mpsearch>
80102c23:	85 c0                	test   %eax,%eax
80102c25:	74 68                	je     80102c8f <mpconfig+0x7c>
80102c27:	89 c6                	mov    %eax,%esi
80102c29:	8b 58 04             	mov    0x4(%eax),%ebx
80102c2c:	85 db                	test   %ebx,%ebx
80102c2e:	74 66                	je     80102c96 <mpconfig+0x83>
    if (a > KERNBASE)
80102c30:	81 fb 00 00 00 80    	cmp    $0x80000000,%ebx
80102c36:	77 4a                	ja     80102c82 <mpconfig+0x6f>
    return (char*)a + KERNBASE;
80102c38:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
    return 0;
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
  if(memcmp(conf, "PCMP", 4) != 0)
80102c3e:	83 ec 04             	sub    $0x4,%esp
80102c41:	6a 04                	push   $0x4
80102c43:	68 77 72 10 80       	push   $0x80107277
80102c48:	53                   	push   %ebx
80102c49:	e8 bf 13 00 00       	call   8010400d <memcmp>
80102c4e:	83 c4 10             	add    $0x10,%esp
80102c51:	85 c0                	test   %eax,%eax
80102c53:	75 48                	jne    80102c9d <mpconfig+0x8a>
    return 0;
  if(conf->version != 1 && conf->version != 4)
80102c55:	0f b6 43 06          	movzbl 0x6(%ebx),%eax
80102c59:	3c 01                	cmp    $0x1,%al
80102c5b:	0f 95 c2             	setne  %dl
80102c5e:	3c 04                	cmp    $0x4,%al
80102c60:	0f 95 c0             	setne  %al
80102c63:	84 c2                	test   %al,%dl
80102c65:	75 3d                	jne    80102ca4 <mpconfig+0x91>
    return 0;
  if(sum((uchar*)conf, conf->length) != 0)
80102c67:	0f b7 53 04          	movzwl 0x4(%ebx),%edx
80102c6b:	89 d8                	mov    %ebx,%eax
80102c6d:	e8 b1 fe ff ff       	call   80102b23 <sum>
80102c72:	84 c0                	test   %al,%al
80102c74:	75 35                	jne    80102cab <mpconfig+0x98>
    return 0;
  *pmp = mp;
80102c76:	89 37                	mov    %esi,(%edi)
  return conf;
}
80102c78:	89 d8                	mov    %ebx,%eax
80102c7a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102c7d:	5b                   	pop    %ebx
80102c7e:	5e                   	pop    %esi
80102c7f:	5f                   	pop    %edi
80102c80:	5d                   	pop    %ebp
80102c81:	c3                   	ret    
        panic("P2V on address > KERNBASE");
80102c82:	83 ec 0c             	sub    $0xc,%esp
80102c85:	68 58 72 10 80       	push   $0x80107258
80102c8a:	e8 b9 d6 ff ff       	call   80100348 <panic>
    return 0;
80102c8f:	bb 00 00 00 00       	mov    $0x0,%ebx
80102c94:	eb e2                	jmp    80102c78 <mpconfig+0x65>
80102c96:	bb 00 00 00 00       	mov    $0x0,%ebx
80102c9b:	eb db                	jmp    80102c78 <mpconfig+0x65>
    return 0;
80102c9d:	bb 00 00 00 00       	mov    $0x0,%ebx
80102ca2:	eb d4                	jmp    80102c78 <mpconfig+0x65>
    return 0;
80102ca4:	bb 00 00 00 00       	mov    $0x0,%ebx
80102ca9:	eb cd                	jmp    80102c78 <mpconfig+0x65>
    return 0;
80102cab:	bb 00 00 00 00       	mov    $0x0,%ebx
80102cb0:	eb c6                	jmp    80102c78 <mpconfig+0x65>

80102cb2 <mpinit>:

void
mpinit(void)
{
80102cb2:	55                   	push   %ebp
80102cb3:	89 e5                	mov    %esp,%ebp
80102cb5:	57                   	push   %edi
80102cb6:	56                   	push   %esi
80102cb7:	53                   	push   %ebx
80102cb8:	83 ec 1c             	sub    $0x1c,%esp
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
80102cbb:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80102cbe:	e8 50 ff ff ff       	call   80102c13 <mpconfig>
80102cc3:	85 c0                	test   %eax,%eax
80102cc5:	74 19                	je     80102ce0 <mpinit+0x2e>
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
80102cc7:	8b 50 24             	mov    0x24(%eax),%edx
80102cca:	89 15 9c 26 11 80    	mov    %edx,0x8011269c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80102cd0:	8d 50 2c             	lea    0x2c(%eax),%edx
80102cd3:	0f b7 48 04          	movzwl 0x4(%eax),%ecx
80102cd7:	01 c1                	add    %eax,%ecx
  ismp = 1;
80102cd9:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80102cde:	eb 34                	jmp    80102d14 <mpinit+0x62>
    panic("Expect to run on an SMP");
80102ce0:	83 ec 0c             	sub    $0xc,%esp
80102ce3:	68 7c 72 10 80       	push   $0x8010727c
80102ce8:	e8 5b d6 ff ff       	call   80100348 <panic>
    switch(*p){
    case MPPROC:
      proc = (struct mpproc*)p;
      if(ncpu < NCPU) {
80102ced:	8b 35 20 2d 11 80    	mov    0x80112d20,%esi
80102cf3:	83 fe 07             	cmp    $0x7,%esi
80102cf6:	7f 19                	jg     80102d11 <mpinit+0x5f>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
80102cf8:	0f b6 42 01          	movzbl 0x1(%edx),%eax
80102cfc:	69 fe b0 00 00 00    	imul   $0xb0,%esi,%edi
80102d02:	88 87 a0 27 11 80    	mov    %al,-0x7feed860(%edi)
        ncpu++;
80102d08:	83 c6 01             	add    $0x1,%esi
80102d0b:	89 35 20 2d 11 80    	mov    %esi,0x80112d20
      }
      p += sizeof(struct mpproc);
80102d11:	83 c2 14             	add    $0x14,%edx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
80102d14:	39 ca                	cmp    %ecx,%edx
80102d16:	73 2b                	jae    80102d43 <mpinit+0x91>
    switch(*p){
80102d18:	0f b6 02             	movzbl (%edx),%eax
80102d1b:	3c 04                	cmp    $0x4,%al
80102d1d:	77 1d                	ja     80102d3c <mpinit+0x8a>
80102d1f:	0f b6 c0             	movzbl %al,%eax
80102d22:	ff 24 85 b4 72 10 80 	jmp    *-0x7fef8d4c(,%eax,4)
      continue;
    case MPIOAPIC:
      ioapic = (struct mpioapic*)p;
      ioapicid = ioapic->apicno;
80102d29:	0f b6 42 01          	movzbl 0x1(%edx),%eax
80102d2d:	a2 80 27 11 80       	mov    %al,0x80112780
      p += sizeof(struct mpioapic);
80102d32:	83 c2 08             	add    $0x8,%edx
      continue;
80102d35:	eb dd                	jmp    80102d14 <mpinit+0x62>
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
80102d37:	83 c2 08             	add    $0x8,%edx
      continue;
80102d3a:	eb d8                	jmp    80102d14 <mpinit+0x62>
    default:
      ismp = 0;
80102d3c:	bb 00 00 00 00       	mov    $0x0,%ebx
80102d41:	eb d1                	jmp    80102d14 <mpinit+0x62>
      break;
    }
  }
  if(!ismp)
80102d43:	85 db                	test   %ebx,%ebx
80102d45:	74 26                	je     80102d6d <mpinit+0xbb>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
80102d47:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80102d4a:	80 78 0c 00          	cmpb   $0x0,0xc(%eax)
80102d4e:	74 15                	je     80102d65 <mpinit+0xb3>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102d50:	b8 70 00 00 00       	mov    $0x70,%eax
80102d55:	ba 22 00 00 00       	mov    $0x22,%edx
80102d5a:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102d5b:	ba 23 00 00 00       	mov    $0x23,%edx
80102d60:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
80102d61:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102d64:	ee                   	out    %al,(%dx)
  }
}
80102d65:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102d68:	5b                   	pop    %ebx
80102d69:	5e                   	pop    %esi
80102d6a:	5f                   	pop    %edi
80102d6b:	5d                   	pop    %ebp
80102d6c:	c3                   	ret    
    panic("Didn't find a suitable machine");
80102d6d:	83 ec 0c             	sub    $0xc,%esp
80102d70:	68 94 72 10 80       	push   $0x80107294
80102d75:	e8 ce d5 ff ff       	call   80100348 <panic>

80102d7a <picinit>:
#define IO_PIC2         0xA0    // Slave (IRQs 8-15)

// Don't use the 8259A interrupt controllers.  Xv6 assumes SMP hardware.
void
picinit(void)
{
80102d7a:	55                   	push   %ebp
80102d7b:	89 e5                	mov    %esp,%ebp
80102d7d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102d82:	ba 21 00 00 00       	mov    $0x21,%edx
80102d87:	ee                   	out    %al,(%dx)
80102d88:	ba a1 00 00 00       	mov    $0xa1,%edx
80102d8d:	ee                   	out    %al,(%dx)
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
80102d8e:	5d                   	pop    %ebp
80102d8f:	c3                   	ret    

80102d90 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80102d90:	55                   	push   %ebp
80102d91:	89 e5                	mov    %esp,%ebp
80102d93:	57                   	push   %edi
80102d94:	56                   	push   %esi
80102d95:	53                   	push   %ebx
80102d96:	83 ec 0c             	sub    $0xc,%esp
80102d99:	8b 5d 08             	mov    0x8(%ebp),%ebx
80102d9c:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
80102d9f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80102da5:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
80102dab:	e8 7d de ff ff       	call   80100c2d <filealloc>
80102db0:	89 03                	mov    %eax,(%ebx)
80102db2:	85 c0                	test   %eax,%eax
80102db4:	74 16                	je     80102dcc <pipealloc+0x3c>
80102db6:	e8 72 de ff ff       	call   80100c2d <filealloc>
80102dbb:	89 06                	mov    %eax,(%esi)
80102dbd:	85 c0                	test   %eax,%eax
80102dbf:	74 0b                	je     80102dcc <pipealloc+0x3c>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
80102dc1:	e8 09 f3 ff ff       	call   801020cf <kalloc>
80102dc6:	89 c7                	mov    %eax,%edi
80102dc8:	85 c0                	test   %eax,%eax
80102dca:	75 35                	jne    80102e01 <pipealloc+0x71>

//PAGEBREAK: 20
 bad:
  if(p)
    kfree((char*)p);
  if(*f0)
80102dcc:	8b 03                	mov    (%ebx),%eax
80102dce:	85 c0                	test   %eax,%eax
80102dd0:	74 0c                	je     80102dde <pipealloc+0x4e>
    fileclose(*f0);
80102dd2:	83 ec 0c             	sub    $0xc,%esp
80102dd5:	50                   	push   %eax
80102dd6:	e8 f8 de ff ff       	call   80100cd3 <fileclose>
80102ddb:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80102dde:	8b 06                	mov    (%esi),%eax
80102de0:	85 c0                	test   %eax,%eax
80102de2:	0f 84 8b 00 00 00    	je     80102e73 <pipealloc+0xe3>
    fileclose(*f1);
80102de8:	83 ec 0c             	sub    $0xc,%esp
80102deb:	50                   	push   %eax
80102dec:	e8 e2 de ff ff       	call   80100cd3 <fileclose>
80102df1:	83 c4 10             	add    $0x10,%esp
  return -1;
80102df4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102df9:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102dfc:	5b                   	pop    %ebx
80102dfd:	5e                   	pop    %esi
80102dfe:	5f                   	pop    %edi
80102dff:	5d                   	pop    %ebp
80102e00:	c3                   	ret    
  p->readopen = 1;
80102e01:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
80102e08:	00 00 00 
  p->writeopen = 1;
80102e0b:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
80102e12:	00 00 00 
  p->nwrite = 0;
80102e15:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
80102e1c:	00 00 00 
  p->nread = 0;
80102e1f:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
80102e26:	00 00 00 
  initlock(&p->lock, "pipe");
80102e29:	83 ec 08             	sub    $0x8,%esp
80102e2c:	68 c8 72 10 80       	push   $0x801072c8
80102e31:	50                   	push   %eax
80102e32:	e8 a8 0f 00 00       	call   80103ddf <initlock>
  (*f0)->type = FD_PIPE;
80102e37:	8b 03                	mov    (%ebx),%eax
80102e39:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80102e3f:	8b 03                	mov    (%ebx),%eax
80102e41:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
80102e45:	8b 03                	mov    (%ebx),%eax
80102e47:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
80102e4b:	8b 03                	mov    (%ebx),%eax
80102e4d:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
80102e50:	8b 06                	mov    (%esi),%eax
80102e52:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
80102e58:	8b 06                	mov    (%esi),%eax
80102e5a:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80102e5e:	8b 06                	mov    (%esi),%eax
80102e60:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
80102e64:	8b 06                	mov    (%esi),%eax
80102e66:	89 78 0c             	mov    %edi,0xc(%eax)
  return 0;
80102e69:	83 c4 10             	add    $0x10,%esp
80102e6c:	b8 00 00 00 00       	mov    $0x0,%eax
80102e71:	eb 86                	jmp    80102df9 <pipealloc+0x69>
  return -1;
80102e73:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80102e78:	e9 7c ff ff ff       	jmp    80102df9 <pipealloc+0x69>

80102e7d <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80102e7d:	55                   	push   %ebp
80102e7e:	89 e5                	mov    %esp,%ebp
80102e80:	53                   	push   %ebx
80102e81:	83 ec 10             	sub    $0x10,%esp
80102e84:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&p->lock);
80102e87:	53                   	push   %ebx
80102e88:	e8 8e 10 00 00       	call   80103f1b <acquire>
  if(writable){
80102e8d:	83 c4 10             	add    $0x10,%esp
80102e90:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
80102e94:	74 3f                	je     80102ed5 <pipeclose+0x58>
    p->writeopen = 0;
80102e96:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
80102e9d:	00 00 00 
    wakeup(&p->nread);
80102ea0:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80102ea6:	83 ec 0c             	sub    $0xc,%esp
80102ea9:	50                   	push   %eax
80102eaa:	e8 4c 09 00 00       	call   801037fb <wakeup>
80102eaf:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
80102eb2:	83 bb 3c 02 00 00 00 	cmpl   $0x0,0x23c(%ebx)
80102eb9:	75 09                	jne    80102ec4 <pipeclose+0x47>
80102ebb:	83 bb 40 02 00 00 00 	cmpl   $0x0,0x240(%ebx)
80102ec2:	74 2f                	je     80102ef3 <pipeclose+0x76>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
80102ec4:	83 ec 0c             	sub    $0xc,%esp
80102ec7:	53                   	push   %ebx
80102ec8:	e8 b3 10 00 00       	call   80103f80 <release>
80102ecd:	83 c4 10             	add    $0x10,%esp
}
80102ed0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102ed3:	c9                   	leave  
80102ed4:	c3                   	ret    
    p->readopen = 0;
80102ed5:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
80102edc:	00 00 00 
    wakeup(&p->nwrite);
80102edf:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
80102ee5:	83 ec 0c             	sub    $0xc,%esp
80102ee8:	50                   	push   %eax
80102ee9:	e8 0d 09 00 00       	call   801037fb <wakeup>
80102eee:	83 c4 10             	add    $0x10,%esp
80102ef1:	eb bf                	jmp    80102eb2 <pipeclose+0x35>
    release(&p->lock);
80102ef3:	83 ec 0c             	sub    $0xc,%esp
80102ef6:	53                   	push   %ebx
80102ef7:	e8 84 10 00 00       	call   80103f80 <release>
    kfree((char*)p);
80102efc:	89 1c 24             	mov    %ebx,(%esp)
80102eff:	e8 8e f0 ff ff       	call   80101f92 <kfree>
80102f04:	83 c4 10             	add    $0x10,%esp
80102f07:	eb c7                	jmp    80102ed0 <pipeclose+0x53>

80102f09 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80102f09:	55                   	push   %ebp
80102f0a:	89 e5                	mov    %esp,%ebp
80102f0c:	57                   	push   %edi
80102f0d:	56                   	push   %esi
80102f0e:	53                   	push   %ebx
80102f0f:	83 ec 18             	sub    $0x18,%esp
80102f12:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
80102f15:	89 de                	mov    %ebx,%esi
80102f17:	53                   	push   %ebx
80102f18:	e8 fe 0f 00 00       	call   80103f1b <acquire>
  for(i = 0; i < n; i++){
80102f1d:	83 c4 10             	add    $0x10,%esp
80102f20:	bf 00 00 00 00       	mov    $0x0,%edi
80102f25:	3b 7d 10             	cmp    0x10(%ebp),%edi
80102f28:	0f 8d 88 00 00 00    	jge    80102fb6 <pipewrite+0xad>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80102f2e:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
80102f34:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80102f3a:	05 00 02 00 00       	add    $0x200,%eax
80102f3f:	39 c2                	cmp    %eax,%edx
80102f41:	75 51                	jne    80102f94 <pipewrite+0x8b>
      if(p->readopen == 0 || myproc()->killed){
80102f43:	83 bb 3c 02 00 00 00 	cmpl   $0x0,0x23c(%ebx)
80102f4a:	74 2f                	je     80102f7b <pipewrite+0x72>
80102f4c:	e8 06 03 00 00       	call   80103257 <myproc>
80102f51:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
80102f55:	75 24                	jne    80102f7b <pipewrite+0x72>
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
80102f57:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80102f5d:	83 ec 0c             	sub    $0xc,%esp
80102f60:	50                   	push   %eax
80102f61:	e8 95 08 00 00       	call   801037fb <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80102f66:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
80102f6c:	83 c4 08             	add    $0x8,%esp
80102f6f:	56                   	push   %esi
80102f70:	50                   	push   %eax
80102f71:	e8 1d 07 00 00       	call   80103693 <sleep>
80102f76:	83 c4 10             	add    $0x10,%esp
80102f79:	eb b3                	jmp    80102f2e <pipewrite+0x25>
        release(&p->lock);
80102f7b:	83 ec 0c             	sub    $0xc,%esp
80102f7e:	53                   	push   %ebx
80102f7f:	e8 fc 0f 00 00       	call   80103f80 <release>
        return -1;
80102f84:	83 c4 10             	add    $0x10,%esp
80102f87:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
80102f8c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102f8f:	5b                   	pop    %ebx
80102f90:	5e                   	pop    %esi
80102f91:	5f                   	pop    %edi
80102f92:	5d                   	pop    %ebp
80102f93:	c3                   	ret    
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
80102f94:	8d 42 01             	lea    0x1(%edx),%eax
80102f97:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
80102f9d:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
80102fa3:	8b 45 0c             	mov    0xc(%ebp),%eax
80102fa6:	0f b6 04 38          	movzbl (%eax,%edi,1),%eax
80102faa:	88 44 13 34          	mov    %al,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
80102fae:	83 c7 01             	add    $0x1,%edi
80102fb1:	e9 6f ff ff ff       	jmp    80102f25 <pipewrite+0x1c>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
80102fb6:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80102fbc:	83 ec 0c             	sub    $0xc,%esp
80102fbf:	50                   	push   %eax
80102fc0:	e8 36 08 00 00       	call   801037fb <wakeup>
  release(&p->lock);
80102fc5:	89 1c 24             	mov    %ebx,(%esp)
80102fc8:	e8 b3 0f 00 00       	call   80103f80 <release>
  return n;
80102fcd:	83 c4 10             	add    $0x10,%esp
80102fd0:	8b 45 10             	mov    0x10(%ebp),%eax
80102fd3:	eb b7                	jmp    80102f8c <pipewrite+0x83>

80102fd5 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80102fd5:	55                   	push   %ebp
80102fd6:	89 e5                	mov    %esp,%ebp
80102fd8:	57                   	push   %edi
80102fd9:	56                   	push   %esi
80102fda:	53                   	push   %ebx
80102fdb:	83 ec 18             	sub    $0x18,%esp
80102fde:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
80102fe1:	89 df                	mov    %ebx,%edi
80102fe3:	53                   	push   %ebx
80102fe4:	e8 32 0f 00 00       	call   80103f1b <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80102fe9:	83 c4 10             	add    $0x10,%esp
80102fec:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
80102ff2:	39 83 34 02 00 00    	cmp    %eax,0x234(%ebx)
80102ff8:	75 3d                	jne    80103037 <piperead+0x62>
80102ffa:	8b b3 40 02 00 00    	mov    0x240(%ebx),%esi
80103000:	85 f6                	test   %esi,%esi
80103002:	74 38                	je     8010303c <piperead+0x67>
    if(myproc()->killed){
80103004:	e8 4e 02 00 00       	call   80103257 <myproc>
80103009:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
8010300d:	75 15                	jne    80103024 <piperead+0x4f>
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
8010300f:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103015:	83 ec 08             	sub    $0x8,%esp
80103018:	57                   	push   %edi
80103019:	50                   	push   %eax
8010301a:	e8 74 06 00 00       	call   80103693 <sleep>
8010301f:	83 c4 10             	add    $0x10,%esp
80103022:	eb c8                	jmp    80102fec <piperead+0x17>
      release(&p->lock);
80103024:	83 ec 0c             	sub    $0xc,%esp
80103027:	53                   	push   %ebx
80103028:	e8 53 0f 00 00       	call   80103f80 <release>
      return -1;
8010302d:	83 c4 10             	add    $0x10,%esp
80103030:	be ff ff ff ff       	mov    $0xffffffff,%esi
80103035:	eb 50                	jmp    80103087 <piperead+0xb2>
80103037:	be 00 00 00 00       	mov    $0x0,%esi
  }
  for(i = 0; i < n; i++){  //DOC: piperead-copy
8010303c:	3b 75 10             	cmp    0x10(%ebp),%esi
8010303f:	7d 2c                	jge    8010306d <piperead+0x98>
    if(p->nread == p->nwrite)
80103041:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80103047:	3b 83 38 02 00 00    	cmp    0x238(%ebx),%eax
8010304d:	74 1e                	je     8010306d <piperead+0x98>
      break;
    addr[i] = p->data[p->nread++ % PIPESIZE];
8010304f:	8d 50 01             	lea    0x1(%eax),%edx
80103052:	89 93 34 02 00 00    	mov    %edx,0x234(%ebx)
80103058:	25 ff 01 00 00       	and    $0x1ff,%eax
8010305d:	0f b6 44 03 34       	movzbl 0x34(%ebx,%eax,1),%eax
80103062:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80103065:	88 04 31             	mov    %al,(%ecx,%esi,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
80103068:	83 c6 01             	add    $0x1,%esi
8010306b:	eb cf                	jmp    8010303c <piperead+0x67>
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
8010306d:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
80103073:	83 ec 0c             	sub    $0xc,%esp
80103076:	50                   	push   %eax
80103077:	e8 7f 07 00 00       	call   801037fb <wakeup>
  release(&p->lock);
8010307c:	89 1c 24             	mov    %ebx,(%esp)
8010307f:	e8 fc 0e 00 00       	call   80103f80 <release>
  return i;
80103084:	83 c4 10             	add    $0x10,%esp
}
80103087:	89 f0                	mov    %esi,%eax
80103089:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010308c:	5b                   	pop    %ebx
8010308d:	5e                   	pop    %esi
8010308e:	5f                   	pop    %edi
8010308f:	5d                   	pop    %ebp
80103090:	c3                   	ret    

80103091 <wakeup1>:
//PAGEBREAK!
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
80103091:	55                   	push   %ebp
80103092:	89 e5                	mov    %esp,%ebp
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103094:	ba 74 2d 11 80       	mov    $0x80112d74,%edx
80103099:	eb 06                	jmp    801030a1 <wakeup1+0x10>
8010309b:	81 c2 84 00 00 00    	add    $0x84,%edx
801030a1:	81 fa 74 4e 11 80    	cmp    $0x80114e74,%edx
801030a7:	73 14                	jae    801030bd <wakeup1+0x2c>
    if(p->state == SLEEPING && p->chan == chan)
801030a9:	83 7a 0c 02          	cmpl   $0x2,0xc(%edx)
801030ad:	75 ec                	jne    8010309b <wakeup1+0xa>
801030af:	39 42 20             	cmp    %eax,0x20(%edx)
801030b2:	75 e7                	jne    8010309b <wakeup1+0xa>
      p->state = RUNNABLE;
801030b4:	c7 42 0c 03 00 00 00 	movl   $0x3,0xc(%edx)
801030bb:	eb de                	jmp    8010309b <wakeup1+0xa>
}
801030bd:	5d                   	pop    %ebp
801030be:	c3                   	ret    

801030bf <allocproc>:
{
801030bf:	55                   	push   %ebp
801030c0:	89 e5                	mov    %esp,%ebp
801030c2:	53                   	push   %ebx
801030c3:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
801030c6:	68 40 2d 11 80       	push   $0x80112d40
801030cb:	e8 4b 0e 00 00       	call   80103f1b <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801030d0:	83 c4 10             	add    $0x10,%esp
801030d3:	bb 74 2d 11 80       	mov    $0x80112d74,%ebx
801030d8:	81 fb 74 4e 11 80    	cmp    $0x80114e74,%ebx
801030de:	73 0e                	jae    801030ee <allocproc+0x2f>
    if(p->state == UNUSED)
801030e0:	83 7b 0c 00          	cmpl   $0x0,0xc(%ebx)
801030e4:	74 1f                	je     80103105 <allocproc+0x46>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
801030e6:	81 c3 84 00 00 00    	add    $0x84,%ebx
801030ec:	eb ea                	jmp    801030d8 <allocproc+0x19>
  release(&ptable.lock);
801030ee:	83 ec 0c             	sub    $0xc,%esp
801030f1:	68 40 2d 11 80       	push   $0x80112d40
801030f6:	e8 85 0e 00 00       	call   80103f80 <release>
  return 0;
801030fb:	83 c4 10             	add    $0x10,%esp
801030fe:	bb 00 00 00 00       	mov    $0x0,%ebx
80103103:	eb 69                	jmp    8010316e <allocproc+0xaf>
  p->state = EMBRYO;
80103105:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
8010310c:	a1 08 a0 10 80       	mov    0x8010a008,%eax
80103111:	8d 50 01             	lea    0x1(%eax),%edx
80103114:	89 15 08 a0 10 80    	mov    %edx,0x8010a008
8010311a:	89 43 10             	mov    %eax,0x10(%ebx)
  release(&ptable.lock);
8010311d:	83 ec 0c             	sub    $0xc,%esp
80103120:	68 40 2d 11 80       	push   $0x80112d40
80103125:	e8 56 0e 00 00       	call   80103f80 <release>
  if((p->kstack = kalloc()) == 0){
8010312a:	e8 a0 ef ff ff       	call   801020cf <kalloc>
8010312f:	89 43 08             	mov    %eax,0x8(%ebx)
80103132:	83 c4 10             	add    $0x10,%esp
80103135:	85 c0                	test   %eax,%eax
80103137:	74 3c                	je     80103175 <allocproc+0xb6>
  sp -= sizeof *p->tf;
80103139:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  p->tf = (struct trapframe*)sp;
8010313f:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
80103142:	c7 80 b0 0f 00 00 21 	movl   $0x80105121,0xfb0(%eax)
80103149:	51 10 80 
  sp -= sizeof *p->context;
8010314c:	05 9c 0f 00 00       	add    $0xf9c,%eax
  p->context = (struct context*)sp;
80103151:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
80103154:	83 ec 04             	sub    $0x4,%esp
80103157:	6a 14                	push   $0x14
80103159:	6a 00                	push   $0x0
8010315b:	50                   	push   %eax
8010315c:	e8 66 0e 00 00       	call   80103fc7 <memset>
  p->context->eip = (uint)forkret;
80103161:	8b 43 1c             	mov    0x1c(%ebx),%eax
80103164:	c7 40 10 83 31 10 80 	movl   $0x80103183,0x10(%eax)
  return p;
8010316b:	83 c4 10             	add    $0x10,%esp
}
8010316e:	89 d8                	mov    %ebx,%eax
80103170:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103173:	c9                   	leave  
80103174:	c3                   	ret    
    p->state = UNUSED;
80103175:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
8010317c:	bb 00 00 00 00       	mov    $0x0,%ebx
80103181:	eb eb                	jmp    8010316e <allocproc+0xaf>

80103183 <forkret>:
{
80103183:	55                   	push   %ebp
80103184:	89 e5                	mov    %esp,%ebp
80103186:	83 ec 14             	sub    $0x14,%esp
  release(&ptable.lock);
80103189:	68 40 2d 11 80       	push   $0x80112d40
8010318e:	e8 ed 0d 00 00       	call   80103f80 <release>
  if (first) {
80103193:	83 c4 10             	add    $0x10,%esp
80103196:	83 3d 00 a0 10 80 00 	cmpl   $0x0,0x8010a000
8010319d:	75 02                	jne    801031a1 <forkret+0x1e>
}
8010319f:	c9                   	leave  
801031a0:	c3                   	ret    
    first = 0;
801031a1:	c7 05 00 a0 10 80 00 	movl   $0x0,0x8010a000
801031a8:	00 00 00 
    iinit(ROOTDEV);
801031ab:	83 ec 0c             	sub    $0xc,%esp
801031ae:	6a 01                	push   $0x1
801031b0:	e8 25 e1 ff ff       	call   801012da <iinit>
    initlog(ROOTDEV);
801031b5:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
801031bc:	e8 c8 f5 ff ff       	call   80102789 <initlog>
801031c1:	83 c4 10             	add    $0x10,%esp
}
801031c4:	eb d9                	jmp    8010319f <forkret+0x1c>

801031c6 <pinit>:
{
801031c6:	55                   	push   %ebp
801031c7:	89 e5                	mov    %esp,%ebp
801031c9:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
801031cc:	68 cd 72 10 80       	push   $0x801072cd
801031d1:	68 40 2d 11 80       	push   $0x80112d40
801031d6:	e8 04 0c 00 00       	call   80103ddf <initlock>
}
801031db:	83 c4 10             	add    $0x10,%esp
801031de:	c9                   	leave  
801031df:	c3                   	ret    

801031e0 <mycpu>:
{
801031e0:	55                   	push   %ebp
801031e1:	89 e5                	mov    %esp,%ebp
801031e3:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801031e6:	9c                   	pushf  
801031e7:	58                   	pop    %eax
  if(readeflags()&FL_IF)
801031e8:	f6 c4 02             	test   $0x2,%ah
801031eb:	75 28                	jne    80103215 <mycpu+0x35>
  apicid = lapicid();
801031ed:	e8 b0 f1 ff ff       	call   801023a2 <lapicid>
  for (i = 0; i < ncpu; ++i) {
801031f2:	ba 00 00 00 00       	mov    $0x0,%edx
801031f7:	39 15 20 2d 11 80    	cmp    %edx,0x80112d20
801031fd:	7e 23                	jle    80103222 <mycpu+0x42>
    if (cpus[i].apicid == apicid)
801031ff:	69 ca b0 00 00 00    	imul   $0xb0,%edx,%ecx
80103205:	0f b6 89 a0 27 11 80 	movzbl -0x7feed860(%ecx),%ecx
8010320c:	39 c1                	cmp    %eax,%ecx
8010320e:	74 1f                	je     8010322f <mycpu+0x4f>
  for (i = 0; i < ncpu; ++i) {
80103210:	83 c2 01             	add    $0x1,%edx
80103213:	eb e2                	jmp    801031f7 <mycpu+0x17>
    panic("mycpu called with interrupts enabled\n");
80103215:	83 ec 0c             	sub    $0xc,%esp
80103218:	68 ec 73 10 80       	push   $0x801073ec
8010321d:	e8 26 d1 ff ff       	call   80100348 <panic>
  panic("unknown apicid\n");
80103222:	83 ec 0c             	sub    $0xc,%esp
80103225:	68 d4 72 10 80       	push   $0x801072d4
8010322a:	e8 19 d1 ff ff       	call   80100348 <panic>
      return &cpus[i];
8010322f:	69 c2 b0 00 00 00    	imul   $0xb0,%edx,%eax
80103235:	05 a0 27 11 80       	add    $0x801127a0,%eax
}
8010323a:	c9                   	leave  
8010323b:	c3                   	ret    

8010323c <cpuid>:
cpuid() {
8010323c:	55                   	push   %ebp
8010323d:	89 e5                	mov    %esp,%ebp
8010323f:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
80103242:	e8 99 ff ff ff       	call   801031e0 <mycpu>
80103247:	2d a0 27 11 80       	sub    $0x801127a0,%eax
8010324c:	c1 f8 04             	sar    $0x4,%eax
8010324f:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
80103255:	c9                   	leave  
80103256:	c3                   	ret    

80103257 <myproc>:
myproc(void) {
80103257:	55                   	push   %ebp
80103258:	89 e5                	mov    %esp,%ebp
8010325a:	53                   	push   %ebx
8010325b:	83 ec 04             	sub    $0x4,%esp
  pushcli();
8010325e:	e8 db 0b 00 00       	call   80103e3e <pushcli>
  c = mycpu();
80103263:	e8 78 ff ff ff       	call   801031e0 <mycpu>
  p = c->proc;
80103268:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
8010326e:	e8 08 0c 00 00       	call   80103e7b <popcli>
}
80103273:	89 d8                	mov    %ebx,%eax
80103275:	83 c4 04             	add    $0x4,%esp
80103278:	5b                   	pop    %ebx
80103279:	5d                   	pop    %ebp
8010327a:	c3                   	ret    

8010327b <userinit>:
{
8010327b:	55                   	push   %ebp
8010327c:	89 e5                	mov    %esp,%ebp
8010327e:	53                   	push   %ebx
8010327f:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103282:	e8 38 fe ff ff       	call   801030bf <allocproc>
80103287:	89 c3                	mov    %eax,%ebx
  initproc = p;
80103289:	a3 b8 a5 10 80       	mov    %eax,0x8010a5b8
  if((p->pgdir = setupkvm()) == 0)
8010328e:	e8 76 35 00 00       	call   80106809 <setupkvm>
80103293:	89 43 04             	mov    %eax,0x4(%ebx)
80103296:	85 c0                	test   %eax,%eax
80103298:	0f 84 c8 00 00 00    	je     80103366 <userinit+0xeb>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
8010329e:	83 ec 04             	sub    $0x4,%esp
801032a1:	68 2c 00 00 00       	push   $0x2c
801032a6:	68 60 a4 10 80       	push   $0x8010a460
801032ab:	50                   	push   %eax
801032ac:	e8 ad 31 00 00       	call   8010645e <inituvm>
  p->sz = PGSIZE;
801032b1:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
801032b7:	83 c4 0c             	add    $0xc,%esp
801032ba:	6a 4c                	push   $0x4c
801032bc:	6a 00                	push   $0x0
801032be:	ff 73 18             	pushl  0x18(%ebx)
801032c1:	e8 01 0d 00 00       	call   80103fc7 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
801032c6:	8b 43 18             	mov    0x18(%ebx),%eax
801032c9:	66 c7 40 3c 1b 00    	movw   $0x1b,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
801032cf:	8b 43 18             	mov    0x18(%ebx),%eax
801032d2:	66 c7 40 2c 23 00    	movw   $0x23,0x2c(%eax)
  p->tf->es = p->tf->ds;
801032d8:	8b 43 18             	mov    0x18(%ebx),%eax
801032db:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801032df:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
801032e3:	8b 43 18             	mov    0x18(%ebx),%eax
801032e6:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801032ea:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
801032ee:	8b 43 18             	mov    0x18(%ebx),%eax
801032f1:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
801032f8:	8b 43 18             	mov    0x18(%ebx),%eax
801032fb:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
80103302:	8b 43 18             	mov    0x18(%ebx),%eax
80103305:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  p->tickets = 10;
8010330c:	c7 43 7c 0a 00 00 00 	movl   $0xa,0x7c(%ebx)
  p->ticks = 0;
80103313:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
8010331a:	00 00 00 
  safestrcpy(p->name, "initcode", sizeof(p->name));
8010331d:	8d 43 6c             	lea    0x6c(%ebx),%eax
80103320:	83 c4 0c             	add    $0xc,%esp
80103323:	6a 10                	push   $0x10
80103325:	68 fd 72 10 80       	push   $0x801072fd
8010332a:	50                   	push   %eax
8010332b:	e8 fe 0d 00 00       	call   8010412e <safestrcpy>
  p->cwd = namei("/");
80103330:	c7 04 24 06 73 10 80 	movl   $0x80107306,(%esp)
80103337:	e8 93 e8 ff ff       	call   80101bcf <namei>
8010333c:	89 43 68             	mov    %eax,0x68(%ebx)
  acquire(&ptable.lock);
8010333f:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103346:	e8 d0 0b 00 00       	call   80103f1b <acquire>
  p->state = RUNNABLE;
8010334b:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80103352:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103359:	e8 22 0c 00 00       	call   80103f80 <release>
}
8010335e:	83 c4 10             	add    $0x10,%esp
80103361:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103364:	c9                   	leave  
80103365:	c3                   	ret    
    panic("userinit: out of memory?");
80103366:	83 ec 0c             	sub    $0xc,%esp
80103369:	68 e4 72 10 80       	push   $0x801072e4
8010336e:	e8 d5 cf ff ff       	call   80100348 <panic>

80103373 <growproc>:
{
80103373:	55                   	push   %ebp
80103374:	89 e5                	mov    %esp,%ebp
80103376:	56                   	push   %esi
80103377:	53                   	push   %ebx
80103378:	8b 75 08             	mov    0x8(%ebp),%esi
  struct proc *curproc = myproc();
8010337b:	e8 d7 fe ff ff       	call   80103257 <myproc>
80103380:	89 c3                	mov    %eax,%ebx
  sz = curproc->sz;
80103382:	8b 00                	mov    (%eax),%eax
  if(n > 0){
80103384:	85 f6                	test   %esi,%esi
80103386:	7f 21                	jg     801033a9 <growproc+0x36>
  } else if(n < 0){
80103388:	85 f6                	test   %esi,%esi
8010338a:	79 33                	jns    801033bf <growproc+0x4c>
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
8010338c:	83 ec 04             	sub    $0x4,%esp
8010338f:	01 c6                	add    %eax,%esi
80103391:	56                   	push   %esi
80103392:	50                   	push   %eax
80103393:	ff 73 04             	pushl  0x4(%ebx)
80103396:	e8 f5 31 00 00       	call   80106590 <deallocuvm>
8010339b:	83 c4 10             	add    $0x10,%esp
8010339e:	85 c0                	test   %eax,%eax
801033a0:	75 1d                	jne    801033bf <growproc+0x4c>
      return -1;
801033a2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801033a7:	eb 29                	jmp    801033d2 <growproc+0x5f>
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
801033a9:	83 ec 04             	sub    $0x4,%esp
801033ac:	01 c6                	add    %eax,%esi
801033ae:	56                   	push   %esi
801033af:	50                   	push   %eax
801033b0:	ff 73 04             	pushl  0x4(%ebx)
801033b3:	e8 cc 32 00 00       	call   80106684 <allocuvm>
801033b8:	83 c4 10             	add    $0x10,%esp
801033bb:	85 c0                	test   %eax,%eax
801033bd:	74 1a                	je     801033d9 <growproc+0x66>
  curproc->sz = sz;
801033bf:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
801033c1:	83 ec 0c             	sub    $0xc,%esp
801033c4:	53                   	push   %ebx
801033c5:	e8 68 2f 00 00       	call   80106332 <switchuvm>
  return 0;
801033ca:	83 c4 10             	add    $0x10,%esp
801033cd:	b8 00 00 00 00       	mov    $0x0,%eax
}
801033d2:	8d 65 f8             	lea    -0x8(%ebp),%esp
801033d5:	5b                   	pop    %ebx
801033d6:	5e                   	pop    %esi
801033d7:	5d                   	pop    %ebp
801033d8:	c3                   	ret    
      return -1;
801033d9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801033de:	eb f2                	jmp    801033d2 <growproc+0x5f>

801033e0 <fork>:
{
801033e0:	55                   	push   %ebp
801033e1:	89 e5                	mov    %esp,%ebp
801033e3:	57                   	push   %edi
801033e4:	56                   	push   %esi
801033e5:	53                   	push   %ebx
801033e6:	83 ec 1c             	sub    $0x1c,%esp
  struct proc *curproc = myproc();
801033e9:	e8 69 fe ff ff       	call   80103257 <myproc>
801033ee:	89 c3                	mov    %eax,%ebx
  if((np = allocproc()) == 0){
801033f0:	e8 ca fc ff ff       	call   801030bf <allocproc>
801033f5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801033f8:	85 c0                	test   %eax,%eax
801033fa:	0f 84 ee 00 00 00    	je     801034ee <fork+0x10e>
80103400:	89 c7                	mov    %eax,%edi
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103402:	83 ec 08             	sub    $0x8,%esp
80103405:	ff 33                	pushl  (%ebx)
80103407:	ff 73 04             	pushl  0x4(%ebx)
8010340a:	e8 db 36 00 00       	call   80106aea <copyuvm>
8010340f:	89 47 04             	mov    %eax,0x4(%edi)
80103412:	83 c4 10             	add    $0x10,%esp
80103415:	85 c0                	test   %eax,%eax
80103417:	74 38                	je     80103451 <fork+0x71>
  np->sz = curproc->sz;
80103419:	8b 03                	mov    (%ebx),%eax
8010341b:	8b 55 e4             	mov    -0x1c(%ebp),%edx
8010341e:	89 02                	mov    %eax,(%edx)
  np->parent = curproc;
80103420:	89 5a 14             	mov    %ebx,0x14(%edx)
  *np->tf = *curproc->tf;
80103423:	8b 73 18             	mov    0x18(%ebx),%esi
80103426:	8b 7a 18             	mov    0x18(%edx),%edi
80103429:	b9 13 00 00 00       	mov    $0x13,%ecx
8010342e:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  np->tickets = curproc->tickets;
80103430:	8b 43 7c             	mov    0x7c(%ebx),%eax
80103433:	89 42 7c             	mov    %eax,0x7c(%edx)
  np->ticks = 0;
80103436:	c7 82 80 00 00 00 00 	movl   $0x0,0x80(%edx)
8010343d:	00 00 00 
  np->tf->eax = 0;
80103440:	8b 42 18             	mov    0x18(%edx),%eax
80103443:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
  for(i = 0; i < NOFILE; i++)
8010344a:	be 00 00 00 00       	mov    $0x0,%esi
8010344f:	eb 29                	jmp    8010347a <fork+0x9a>
    kfree(np->kstack);
80103451:	83 ec 0c             	sub    $0xc,%esp
80103454:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103457:	ff 73 08             	pushl  0x8(%ebx)
8010345a:	e8 33 eb ff ff       	call   80101f92 <kfree>
    np->kstack = 0;
8010345f:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    np->state = UNUSED;
80103466:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
8010346d:	83 c4 10             	add    $0x10,%esp
80103470:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103475:	eb 6d                	jmp    801034e4 <fork+0x104>
  for(i = 0; i < NOFILE; i++)
80103477:	83 c6 01             	add    $0x1,%esi
8010347a:	83 fe 0f             	cmp    $0xf,%esi
8010347d:	7f 1d                	jg     8010349c <fork+0xbc>
    if(curproc->ofile[i])
8010347f:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103483:	85 c0                	test   %eax,%eax
80103485:	74 f0                	je     80103477 <fork+0x97>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103487:	83 ec 0c             	sub    $0xc,%esp
8010348a:	50                   	push   %eax
8010348b:	e8 fe d7 ff ff       	call   80100c8e <filedup>
80103490:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103493:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
80103497:	83 c4 10             	add    $0x10,%esp
8010349a:	eb db                	jmp    80103477 <fork+0x97>
  np->cwd = idup(curproc->cwd);
8010349c:	83 ec 0c             	sub    $0xc,%esp
8010349f:	ff 73 68             	pushl  0x68(%ebx)
801034a2:	e8 98 e0 ff ff       	call   8010153f <idup>
801034a7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
801034aa:	89 47 68             	mov    %eax,0x68(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
801034ad:	83 c3 6c             	add    $0x6c,%ebx
801034b0:	8d 47 6c             	lea    0x6c(%edi),%eax
801034b3:	83 c4 0c             	add    $0xc,%esp
801034b6:	6a 10                	push   $0x10
801034b8:	53                   	push   %ebx
801034b9:	50                   	push   %eax
801034ba:	e8 6f 0c 00 00       	call   8010412e <safestrcpy>
  pid = np->pid;
801034bf:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
801034c2:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
801034c9:	e8 4d 0a 00 00       	call   80103f1b <acquire>
  np->state = RUNNABLE;
801034ce:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
801034d5:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
801034dc:	e8 9f 0a 00 00       	call   80103f80 <release>
  return pid;
801034e1:	83 c4 10             	add    $0x10,%esp
}
801034e4:	89 d8                	mov    %ebx,%eax
801034e6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801034e9:	5b                   	pop    %ebx
801034ea:	5e                   	pop    %esi
801034eb:	5f                   	pop    %edi
801034ec:	5d                   	pop    %ebp
801034ed:	c3                   	ret    
    return -1;
801034ee:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801034f3:	eb ef                	jmp    801034e4 <fork+0x104>

801034f5 <sched>:
{
801034f5:	55                   	push   %ebp
801034f6:	89 e5                	mov    %esp,%ebp
801034f8:	56                   	push   %esi
801034f9:	53                   	push   %ebx
  struct proc *p = myproc();
801034fa:	e8 58 fd ff ff       	call   80103257 <myproc>
801034ff:	89 c3                	mov    %eax,%ebx
  if(!holding(&ptable.lock))
80103501:	83 ec 0c             	sub    $0xc,%esp
80103504:	68 40 2d 11 80       	push   $0x80112d40
80103509:	e8 cd 09 00 00       	call   80103edb <holding>
8010350e:	83 c4 10             	add    $0x10,%esp
80103511:	85 c0                	test   %eax,%eax
80103513:	74 4f                	je     80103564 <sched+0x6f>
  if(mycpu()->ncli != 1)
80103515:	e8 c6 fc ff ff       	call   801031e0 <mycpu>
8010351a:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103521:	75 4e                	jne    80103571 <sched+0x7c>
  if(p->state == RUNNING)
80103523:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103527:	74 55                	je     8010357e <sched+0x89>
80103529:	9c                   	pushf  
8010352a:	58                   	pop    %eax
  if(readeflags()&FL_IF)
8010352b:	f6 c4 02             	test   $0x2,%ah
8010352e:	75 5b                	jne    8010358b <sched+0x96>
  intena = mycpu()->intena;
80103530:	e8 ab fc ff ff       	call   801031e0 <mycpu>
80103535:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
8010353b:	e8 a0 fc ff ff       	call   801031e0 <mycpu>
80103540:	83 ec 08             	sub    $0x8,%esp
80103543:	ff 70 04             	pushl  0x4(%eax)
80103546:	83 c3 1c             	add    $0x1c,%ebx
80103549:	53                   	push   %ebx
8010354a:	e8 32 0c 00 00       	call   80104181 <swtch>
  mycpu()->intena = intena;
8010354f:	e8 8c fc ff ff       	call   801031e0 <mycpu>
80103554:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
8010355a:	83 c4 10             	add    $0x10,%esp
8010355d:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103560:	5b                   	pop    %ebx
80103561:	5e                   	pop    %esi
80103562:	5d                   	pop    %ebp
80103563:	c3                   	ret    
    panic("sched ptable.lock");
80103564:	83 ec 0c             	sub    $0xc,%esp
80103567:	68 08 73 10 80       	push   $0x80107308
8010356c:	e8 d7 cd ff ff       	call   80100348 <panic>
    panic("sched locks");
80103571:	83 ec 0c             	sub    $0xc,%esp
80103574:	68 1a 73 10 80       	push   $0x8010731a
80103579:	e8 ca cd ff ff       	call   80100348 <panic>
    panic("sched running");
8010357e:	83 ec 0c             	sub    $0xc,%esp
80103581:	68 26 73 10 80       	push   $0x80107326
80103586:	e8 bd cd ff ff       	call   80100348 <panic>
    panic("sched interruptible");
8010358b:	83 ec 0c             	sub    $0xc,%esp
8010358e:	68 34 73 10 80       	push   $0x80107334
80103593:	e8 b0 cd ff ff       	call   80100348 <panic>

80103598 <exit>:
{
80103598:	55                   	push   %ebp
80103599:	89 e5                	mov    %esp,%ebp
8010359b:	56                   	push   %esi
8010359c:	53                   	push   %ebx
  struct proc *curproc = myproc();
8010359d:	e8 b5 fc ff ff       	call   80103257 <myproc>
  if(curproc == initproc)
801035a2:	39 05 b8 a5 10 80    	cmp    %eax,0x8010a5b8
801035a8:	74 09                	je     801035b3 <exit+0x1b>
801035aa:	89 c6                	mov    %eax,%esi
  for(fd = 0; fd < NOFILE; fd++){
801035ac:	bb 00 00 00 00       	mov    $0x0,%ebx
801035b1:	eb 10                	jmp    801035c3 <exit+0x2b>
    panic("init exiting");
801035b3:	83 ec 0c             	sub    $0xc,%esp
801035b6:	68 48 73 10 80       	push   $0x80107348
801035bb:	e8 88 cd ff ff       	call   80100348 <panic>
  for(fd = 0; fd < NOFILE; fd++){
801035c0:	83 c3 01             	add    $0x1,%ebx
801035c3:	83 fb 0f             	cmp    $0xf,%ebx
801035c6:	7f 1e                	jg     801035e6 <exit+0x4e>
    if(curproc->ofile[fd]){
801035c8:	8b 44 9e 28          	mov    0x28(%esi,%ebx,4),%eax
801035cc:	85 c0                	test   %eax,%eax
801035ce:	74 f0                	je     801035c0 <exit+0x28>
      fileclose(curproc->ofile[fd]);
801035d0:	83 ec 0c             	sub    $0xc,%esp
801035d3:	50                   	push   %eax
801035d4:	e8 fa d6 ff ff       	call   80100cd3 <fileclose>
      curproc->ofile[fd] = 0;
801035d9:	c7 44 9e 28 00 00 00 	movl   $0x0,0x28(%esi,%ebx,4)
801035e0:	00 
801035e1:	83 c4 10             	add    $0x10,%esp
801035e4:	eb da                	jmp    801035c0 <exit+0x28>
  begin_op();
801035e6:	e8 e7 f1 ff ff       	call   801027d2 <begin_op>
  iput(curproc->cwd);
801035eb:	83 ec 0c             	sub    $0xc,%esp
801035ee:	ff 76 68             	pushl  0x68(%esi)
801035f1:	e8 80 e0 ff ff       	call   80101676 <iput>
  end_op();
801035f6:	e8 51 f2 ff ff       	call   8010284c <end_op>
  curproc->cwd = 0;
801035fb:	c7 46 68 00 00 00 00 	movl   $0x0,0x68(%esi)
  acquire(&ptable.lock);
80103602:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103609:	e8 0d 09 00 00       	call   80103f1b <acquire>
  wakeup1(curproc->parent);
8010360e:	8b 46 14             	mov    0x14(%esi),%eax
80103611:	e8 7b fa ff ff       	call   80103091 <wakeup1>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103616:	83 c4 10             	add    $0x10,%esp
80103619:	bb 74 2d 11 80       	mov    $0x80112d74,%ebx
8010361e:	eb 06                	jmp    80103626 <exit+0x8e>
80103620:	81 c3 84 00 00 00    	add    $0x84,%ebx
80103626:	81 fb 74 4e 11 80    	cmp    $0x80114e74,%ebx
8010362c:	73 1a                	jae    80103648 <exit+0xb0>
    if(p->parent == curproc){
8010362e:	39 73 14             	cmp    %esi,0x14(%ebx)
80103631:	75 ed                	jne    80103620 <exit+0x88>
      p->parent = initproc;
80103633:	a1 b8 a5 10 80       	mov    0x8010a5b8,%eax
80103638:	89 43 14             	mov    %eax,0x14(%ebx)
      if(p->state == ZOMBIE)
8010363b:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
8010363f:	75 df                	jne    80103620 <exit+0x88>
        wakeup1(initproc);
80103641:	e8 4b fa ff ff       	call   80103091 <wakeup1>
80103646:	eb d8                	jmp    80103620 <exit+0x88>
  curproc->state = ZOMBIE;
80103648:	c7 46 0c 05 00 00 00 	movl   $0x5,0xc(%esi)
  sched();
8010364f:	e8 a1 fe ff ff       	call   801034f5 <sched>
  panic("zombie exit");
80103654:	83 ec 0c             	sub    $0xc,%esp
80103657:	68 55 73 10 80       	push   $0x80107355
8010365c:	e8 e7 cc ff ff       	call   80100348 <panic>

80103661 <yield>:
{
80103661:	55                   	push   %ebp
80103662:	89 e5                	mov    %esp,%ebp
80103664:	83 ec 14             	sub    $0x14,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80103667:	68 40 2d 11 80       	push   $0x80112d40
8010366c:	e8 aa 08 00 00       	call   80103f1b <acquire>
  myproc()->state = RUNNABLE;
80103671:	e8 e1 fb ff ff       	call   80103257 <myproc>
80103676:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  sched();
8010367d:	e8 73 fe ff ff       	call   801034f5 <sched>
  release(&ptable.lock);
80103682:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103689:	e8 f2 08 00 00       	call   80103f80 <release>
}
8010368e:	83 c4 10             	add    $0x10,%esp
80103691:	c9                   	leave  
80103692:	c3                   	ret    

80103693 <sleep>:
{
80103693:	55                   	push   %ebp
80103694:	89 e5                	mov    %esp,%ebp
80103696:	56                   	push   %esi
80103697:	53                   	push   %ebx
80103698:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  struct proc *p = myproc();
8010369b:	e8 b7 fb ff ff       	call   80103257 <myproc>
  if(p == 0)
801036a0:	85 c0                	test   %eax,%eax
801036a2:	74 66                	je     8010370a <sleep+0x77>
801036a4:	89 c6                	mov    %eax,%esi
  if(lk == 0)
801036a6:	85 db                	test   %ebx,%ebx
801036a8:	74 6d                	je     80103717 <sleep+0x84>
  if(lk != &ptable.lock){  //DOC: sleeplock0
801036aa:	81 fb 40 2d 11 80    	cmp    $0x80112d40,%ebx
801036b0:	74 18                	je     801036ca <sleep+0x37>
    acquire(&ptable.lock);  //DOC: sleeplock1
801036b2:	83 ec 0c             	sub    $0xc,%esp
801036b5:	68 40 2d 11 80       	push   $0x80112d40
801036ba:	e8 5c 08 00 00       	call   80103f1b <acquire>
    release(lk);
801036bf:	89 1c 24             	mov    %ebx,(%esp)
801036c2:	e8 b9 08 00 00       	call   80103f80 <release>
801036c7:	83 c4 10             	add    $0x10,%esp
  p->chan = chan;
801036ca:	8b 45 08             	mov    0x8(%ebp),%eax
801036cd:	89 46 20             	mov    %eax,0x20(%esi)
  p->state = SLEEPING;
801036d0:	c7 46 0c 02 00 00 00 	movl   $0x2,0xc(%esi)
  sched();
801036d7:	e8 19 fe ff ff       	call   801034f5 <sched>
  p->chan = 0;
801036dc:	c7 46 20 00 00 00 00 	movl   $0x0,0x20(%esi)
  if(lk != &ptable.lock){  //DOC: sleeplock2
801036e3:	81 fb 40 2d 11 80    	cmp    $0x80112d40,%ebx
801036e9:	74 18                	je     80103703 <sleep+0x70>
    release(&ptable.lock);
801036eb:	83 ec 0c             	sub    $0xc,%esp
801036ee:	68 40 2d 11 80       	push   $0x80112d40
801036f3:	e8 88 08 00 00       	call   80103f80 <release>
    acquire(lk);
801036f8:	89 1c 24             	mov    %ebx,(%esp)
801036fb:	e8 1b 08 00 00       	call   80103f1b <acquire>
80103700:	83 c4 10             	add    $0x10,%esp
}
80103703:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103706:	5b                   	pop    %ebx
80103707:	5e                   	pop    %esi
80103708:	5d                   	pop    %ebp
80103709:	c3                   	ret    
    panic("sleep");
8010370a:	83 ec 0c             	sub    $0xc,%esp
8010370d:	68 61 73 10 80       	push   $0x80107361
80103712:	e8 31 cc ff ff       	call   80100348 <panic>
    panic("sleep without lk");
80103717:	83 ec 0c             	sub    $0xc,%esp
8010371a:	68 67 73 10 80       	push   $0x80107367
8010371f:	e8 24 cc ff ff       	call   80100348 <panic>

80103724 <wait>:
{
80103724:	55                   	push   %ebp
80103725:	89 e5                	mov    %esp,%ebp
80103727:	56                   	push   %esi
80103728:	53                   	push   %ebx
  struct proc *curproc = myproc();
80103729:	e8 29 fb ff ff       	call   80103257 <myproc>
8010372e:	89 c6                	mov    %eax,%esi
  acquire(&ptable.lock);
80103730:	83 ec 0c             	sub    $0xc,%esp
80103733:	68 40 2d 11 80       	push   $0x80112d40
80103738:	e8 de 07 00 00       	call   80103f1b <acquire>
8010373d:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
80103740:	b8 00 00 00 00       	mov    $0x0,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103745:	bb 74 2d 11 80       	mov    $0x80112d74,%ebx
8010374a:	eb 5e                	jmp    801037aa <wait+0x86>
        pid = p->pid;
8010374c:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
8010374f:	83 ec 0c             	sub    $0xc,%esp
80103752:	ff 73 08             	pushl  0x8(%ebx)
80103755:	e8 38 e8 ff ff       	call   80101f92 <kfree>
        p->kstack = 0;
8010375a:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80103761:	83 c4 04             	add    $0x4,%esp
80103764:	ff 73 04             	pushl  0x4(%ebx)
80103767:	e8 19 30 00 00       	call   80106785 <freevm>
        p->pid = 0;
8010376c:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
80103773:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
8010377a:	c6 43 6c 00          	movb   $0x0,0x6c(%ebx)
        p->killed = 0;
8010377e:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
80103785:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
8010378c:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103793:	e8 e8 07 00 00       	call   80103f80 <release>
        return pid;
80103798:	83 c4 10             	add    $0x10,%esp
}
8010379b:	89 f0                	mov    %esi,%eax
8010379d:	8d 65 f8             	lea    -0x8(%ebp),%esp
801037a0:	5b                   	pop    %ebx
801037a1:	5e                   	pop    %esi
801037a2:	5d                   	pop    %ebp
801037a3:	c3                   	ret    
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801037a4:	81 c3 84 00 00 00    	add    $0x84,%ebx
801037aa:	81 fb 74 4e 11 80    	cmp    $0x80114e74,%ebx
801037b0:	73 12                	jae    801037c4 <wait+0xa0>
      if(p->parent != curproc)
801037b2:	39 73 14             	cmp    %esi,0x14(%ebx)
801037b5:	75 ed                	jne    801037a4 <wait+0x80>
      if(p->state == ZOMBIE){
801037b7:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
801037bb:	74 8f                	je     8010374c <wait+0x28>
      havekids = 1;
801037bd:	b8 01 00 00 00       	mov    $0x1,%eax
801037c2:	eb e0                	jmp    801037a4 <wait+0x80>
    if(!havekids || curproc->killed){
801037c4:	85 c0                	test   %eax,%eax
801037c6:	74 06                	je     801037ce <wait+0xaa>
801037c8:	83 7e 24 00          	cmpl   $0x0,0x24(%esi)
801037cc:	74 17                	je     801037e5 <wait+0xc1>
      release(&ptable.lock);
801037ce:	83 ec 0c             	sub    $0xc,%esp
801037d1:	68 40 2d 11 80       	push   $0x80112d40
801037d6:	e8 a5 07 00 00       	call   80103f80 <release>
      return -1;
801037db:	83 c4 10             	add    $0x10,%esp
801037de:	be ff ff ff ff       	mov    $0xffffffff,%esi
801037e3:	eb b6                	jmp    8010379b <wait+0x77>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
801037e5:	83 ec 08             	sub    $0x8,%esp
801037e8:	68 40 2d 11 80       	push   $0x80112d40
801037ed:	56                   	push   %esi
801037ee:	e8 a0 fe ff ff       	call   80103693 <sleep>
    havekids = 0;
801037f3:	83 c4 10             	add    $0x10,%esp
801037f6:	e9 45 ff ff ff       	jmp    80103740 <wait+0x1c>

801037fb <wakeup>:

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801037fb:	55                   	push   %ebp
801037fc:	89 e5                	mov    %esp,%ebp
801037fe:	83 ec 14             	sub    $0x14,%esp
  acquire(&ptable.lock);
80103801:	68 40 2d 11 80       	push   $0x80112d40
80103806:	e8 10 07 00 00       	call   80103f1b <acquire>
  wakeup1(chan);
8010380b:	8b 45 08             	mov    0x8(%ebp),%eax
8010380e:	e8 7e f8 ff ff       	call   80103091 <wakeup1>
  release(&ptable.lock);
80103813:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
8010381a:	e8 61 07 00 00       	call   80103f80 <release>
}
8010381f:	83 c4 10             	add    $0x10,%esp
80103822:	c9                   	leave  
80103823:	c3                   	ret    

80103824 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80103824:	55                   	push   %ebp
80103825:	89 e5                	mov    %esp,%ebp
80103827:	53                   	push   %ebx
80103828:	83 ec 10             	sub    $0x10,%esp
8010382b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010382e:	68 40 2d 11 80       	push   $0x80112d40
80103833:	e8 e3 06 00 00       	call   80103f1b <acquire>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103838:	83 c4 10             	add    $0x10,%esp
8010383b:	b8 74 2d 11 80       	mov    $0x80112d74,%eax
80103840:	3d 74 4e 11 80       	cmp    $0x80114e74,%eax
80103845:	73 3c                	jae    80103883 <kill+0x5f>
    if(p->pid == pid){
80103847:	39 58 10             	cmp    %ebx,0x10(%eax)
8010384a:	74 07                	je     80103853 <kill+0x2f>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010384c:	05 84 00 00 00       	add    $0x84,%eax
80103851:	eb ed                	jmp    80103840 <kill+0x1c>
      p->killed = 1;
80103853:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
8010385a:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
8010385e:	74 1a                	je     8010387a <kill+0x56>
        p->state = RUNNABLE;
      release(&ptable.lock);
80103860:	83 ec 0c             	sub    $0xc,%esp
80103863:	68 40 2d 11 80       	push   $0x80112d40
80103868:	e8 13 07 00 00       	call   80103f80 <release>
      return 0;
8010386d:	83 c4 10             	add    $0x10,%esp
80103870:	b8 00 00 00 00       	mov    $0x0,%eax
    }
  }
  release(&ptable.lock);
  return -1;
}
80103875:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103878:	c9                   	leave  
80103879:	c3                   	ret    
        p->state = RUNNABLE;
8010387a:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80103881:	eb dd                	jmp    80103860 <kill+0x3c>
  release(&ptable.lock);
80103883:	83 ec 0c             	sub    $0xc,%esp
80103886:	68 40 2d 11 80       	push   $0x80112d40
8010388b:	e8 f0 06 00 00       	call   80103f80 <release>
  return -1;
80103890:	83 c4 10             	add    $0x10,%esp
80103893:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103898:	eb db                	jmp    80103875 <kill+0x51>

8010389a <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
8010389a:	55                   	push   %ebp
8010389b:	89 e5                	mov    %esp,%ebp
8010389d:	56                   	push   %esi
8010389e:	53                   	push   %ebx
8010389f:	83 ec 30             	sub    $0x30,%esp
  int i;
  struct proc *p;
  char *state;
  uint pc[10];

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801038a2:	bb 74 2d 11 80       	mov    $0x80112d74,%ebx
801038a7:	eb 36                	jmp    801038df <procdump+0x45>
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
801038a9:	b8 78 73 10 80       	mov    $0x80107378,%eax
    cprintf("%d %s %s", p->pid, state, p->name);
801038ae:	8d 53 6c             	lea    0x6c(%ebx),%edx
801038b1:	52                   	push   %edx
801038b2:	50                   	push   %eax
801038b3:	ff 73 10             	pushl  0x10(%ebx)
801038b6:	68 7c 73 10 80       	push   $0x8010737c
801038bb:	e8 4b cd ff ff       	call   8010060b <cprintf>
    if(p->state == SLEEPING){
801038c0:	83 c4 10             	add    $0x10,%esp
801038c3:	83 7b 0c 02          	cmpl   $0x2,0xc(%ebx)
801038c7:	74 3c                	je     80103905 <procdump+0x6b>
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
801038c9:	83 ec 0c             	sub    $0xc,%esp
801038cc:	68 91 75 10 80       	push   $0x80107591
801038d1:	e8 35 cd ff ff       	call   8010060b <cprintf>
801038d6:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801038d9:	81 c3 84 00 00 00    	add    $0x84,%ebx
801038df:	81 fb 74 4e 11 80    	cmp    $0x80114e74,%ebx
801038e5:	73 61                	jae    80103948 <procdump+0xae>
    if(p->state == UNUSED)
801038e7:	8b 43 0c             	mov    0xc(%ebx),%eax
801038ea:	85 c0                	test   %eax,%eax
801038ec:	74 eb                	je     801038d9 <procdump+0x3f>
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
801038ee:	83 f8 05             	cmp    $0x5,%eax
801038f1:	77 b6                	ja     801038a9 <procdump+0xf>
801038f3:	8b 04 85 14 74 10 80 	mov    -0x7fef8bec(,%eax,4),%eax
801038fa:	85 c0                	test   %eax,%eax
801038fc:	75 b0                	jne    801038ae <procdump+0x14>
      state = "???";
801038fe:	b8 78 73 10 80       	mov    $0x80107378,%eax
80103903:	eb a9                	jmp    801038ae <procdump+0x14>
      getcallerpcs((uint*)p->context->ebp+2, pc);
80103905:	8b 43 1c             	mov    0x1c(%ebx),%eax
80103908:	8b 40 0c             	mov    0xc(%eax),%eax
8010390b:	83 c0 08             	add    $0x8,%eax
8010390e:	83 ec 08             	sub    $0x8,%esp
80103911:	8d 55 d0             	lea    -0x30(%ebp),%edx
80103914:	52                   	push   %edx
80103915:	50                   	push   %eax
80103916:	e8 df 04 00 00       	call   80103dfa <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
8010391b:	83 c4 10             	add    $0x10,%esp
8010391e:	be 00 00 00 00       	mov    $0x0,%esi
80103923:	eb 14                	jmp    80103939 <procdump+0x9f>
        cprintf(" %p", pc[i]);
80103925:	83 ec 08             	sub    $0x8,%esp
80103928:	50                   	push   %eax
80103929:	68 01 6d 10 80       	push   $0x80106d01
8010392e:	e8 d8 cc ff ff       	call   8010060b <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
80103933:	83 c6 01             	add    $0x1,%esi
80103936:	83 c4 10             	add    $0x10,%esp
80103939:	83 fe 09             	cmp    $0x9,%esi
8010393c:	7f 8b                	jg     801038c9 <procdump+0x2f>
8010393e:	8b 44 b5 d0          	mov    -0x30(%ebp,%esi,4),%eax
80103942:	85 c0                	test   %eax,%eax
80103944:	75 df                	jne    80103925 <procdump+0x8b>
80103946:	eb 81                	jmp    801038c9 <procdump+0x2f>
  }
}
80103948:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010394b:	5b                   	pop    %ebx
8010394c:	5e                   	pop    %esi
8010394d:	5d                   	pop    %ebp
8010394e:	c3                   	ret    

8010394f <getprocessesinfo>:

int getprocessesinfo(void){
8010394f:	55                   	push   %ebp
80103950:	89 e5                	mov    %esp,%ebp
80103952:	53                   	push   %ebx
80103953:	83 ec 18             	sub    $0x18,%esp
  int nump = 0;
  struct processes_info *pinfo;
  argptr(0, (char**)&pinfo, sizeof(struct processes_info));
80103956:	68 04 03 00 00       	push   $0x304
8010395b:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010395e:	50                   	push   %eax
8010395f:	6a 00                	push   $0x0
80103961:	e8 d4 08 00 00       	call   8010423a <argptr>
  acquire(&ptable.lock);
80103966:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
8010396d:	e8 a9 05 00 00       	call   80103f1b <acquire>
  for(int i=0; i<NPROC; i++){
80103972:	83 c4 10             	add    $0x10,%esp
80103975:	b8 00 00 00 00       	mov    $0x0,%eax
8010397a:	eb 03                	jmp    8010397f <getprocessesinfo+0x30>
8010397c:	83 c0 01             	add    $0x1,%eax
8010397f:	83 f8 3f             	cmp    $0x3f,%eax
80103982:	7f 3e                	jg     801039c2 <getprocessesinfo+0x73>
    if(ptable.proc[i].state!=UNUSED){
80103984:	69 d0 84 00 00 00    	imul   $0x84,%eax,%edx
8010398a:	83 ba 80 2d 11 80 00 	cmpl   $0x0,-0x7feed280(%edx)
80103991:	74 e9                	je     8010397c <getprocessesinfo+0x2d>
      pinfo->pids[i] = ptable.proc[i].pid;
80103993:	8b 4d f4             	mov    -0xc(%ebp),%ecx
80103996:	8b 9a 84 2d 11 80    	mov    -0x7feed27c(%edx),%ebx
8010399c:	89 5c 81 04          	mov    %ebx,0x4(%ecx,%eax,4)
      pinfo->tickets[i] = ptable.proc[i].tickets;
801039a0:	8b 9a f0 2d 11 80    	mov    -0x7feed210(%edx),%ebx
      pinfo->pids[i] = ptable.proc[i].pid;
801039a6:	81 c2 40 2d 11 80    	add    $0x80112d40,%edx
      pinfo->tickets[i] = ptable.proc[i].tickets;
801039ac:	89 9c 81 04 02 00 00 	mov    %ebx,0x204(%ecx,%eax,4)
      pinfo->times_scheduled[i] = ptable.proc[i].ticks;
801039b3:	8b 92 b4 00 00 00    	mov    0xb4(%edx),%edx
801039b9:	89 94 81 04 01 00 00 	mov    %edx,0x104(%ecx,%eax,4)
801039c0:	eb ba                	jmp    8010397c <getprocessesinfo+0x2d>
    }
  }
  for(int i=0; i<NPROC; i++){
801039c2:	b8 00 00 00 00       	mov    $0x0,%eax
  int nump = 0;
801039c7:	b9 00 00 00 00       	mov    $0x0,%ecx
801039cc:	eb 03                	jmp    801039d1 <getprocessesinfo+0x82>
  for(int i=0; i<NPROC; i++){
801039ce:	83 c0 01             	add    $0x1,%eax
801039d1:	83 f8 3f             	cmp    $0x3f,%eax
801039d4:	7f 14                	jg     801039ea <getprocessesinfo+0x9b>
    if(ptable.proc[i].state!=UNUSED){
801039d6:	69 d0 84 00 00 00    	imul   $0x84,%eax,%edx
801039dc:	83 ba 80 2d 11 80 00 	cmpl   $0x0,-0x7feed280(%edx)
801039e3:	74 e9                	je     801039ce <getprocessesinfo+0x7f>
       nump++;
801039e5:	83 c1 01             	add    $0x1,%ecx
801039e8:	eb e4                	jmp    801039ce <getprocessesinfo+0x7f>
    }}
  pinfo->num_processes = nump;
801039ea:	8b 45 f4             	mov    -0xc(%ebp),%eax
801039ed:	89 08                	mov    %ecx,(%eax)
  release(&ptable.lock);
801039ef:	83 ec 0c             	sub    $0xc,%esp
801039f2:	68 40 2d 11 80       	push   $0x80112d40
801039f7:	e8 84 05 00 00       	call   80103f80 <release>
  return 0;
}
801039fc:	b8 00 00 00 00       	mov    $0x0,%eax
80103a01:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103a04:	c9                   	leave  
80103a05:	c3                   	ret    

80103a06 <totalNonUnusedTickets>:

unsigned totalNonUnusedTickets(void){
80103a06:	55                   	push   %ebp
80103a07:	89 e5                	mov    %esp,%ebp
  unsigned t = 0;
  for(int i=0; i<NPROC; i++){
80103a09:	ba 00 00 00 00       	mov    $0x0,%edx
  unsigned t = 0;
80103a0e:	b8 00 00 00 00       	mov    $0x0,%eax
  for(int i=0; i<NPROC; i++){
80103a13:	eb 03                	jmp    80103a18 <totalNonUnusedTickets+0x12>
80103a15:	83 c2 01             	add    $0x1,%edx
80103a18:	83 fa 3f             	cmp    $0x3f,%edx
80103a1b:	7f 17                	jg     80103a34 <totalNonUnusedTickets+0x2e>
    if(ptable.proc[i].state!=UNUSED){
80103a1d:	69 ca 84 00 00 00    	imul   $0x84,%edx,%ecx
80103a23:	83 b9 80 2d 11 80 00 	cmpl   $0x0,-0x7feed280(%ecx)
80103a2a:	74 e9                	je     80103a15 <totalNonUnusedTickets+0xf>
      t+=ptable.proc[i].tickets;
80103a2c:	03 81 f0 2d 11 80    	add    -0x7feed210(%ecx),%eax
80103a32:	eb e1                	jmp    80103a15 <totalNonUnusedTickets+0xf>
    }
  }
  return t;
}
80103a34:	5d                   	pop    %ebp
80103a35:	c3                   	ret    

80103a36 <lcg_parkmiller>:

static unsigned random_seed = 1;

#define RANDOM_MAX ((1u << 31u) - 1u)
unsigned lcg_parkmiller(unsigned *state)
{
80103a36:	55                   	push   %ebp
80103a37:	89 e5                	mov    %esp,%ebp
80103a39:	53                   	push   %ebx
80103a3a:	8b 4d 08             	mov    0x8(%ebp),%ecx
        Therefore:
          rem*G - div*(N%G) === state*G  (mod N)

        Add N if necessary so that the result is between 1 and N-1.
    */
    unsigned div = *state / (N / G);  /* max : 2,147,483,646 / 44,488 = 48,271 */
80103a3d:	8b 19                	mov    (%ecx),%ebx
80103a3f:	ba 91 13 8f bc       	mov    $0xbc8f1391,%edx
80103a44:	89 d8                	mov    %ebx,%eax
80103a46:	f7 e2                	mul    %edx
80103a48:	c1 ea 0f             	shr    $0xf,%edx
    unsigned rem = *state % (N / G);  /* max : 2,147,483,646 % 44,488 = 44,487 */
80103a4b:	69 c2 c8 ad 00 00    	imul   $0xadc8,%edx,%eax
80103a51:	29 c3                	sub    %eax,%ebx

    unsigned a = rem * G;        /* max : 44,487 * 48,271 = 2,147,431,977 */
80103a53:	69 c3 8f bc 00 00    	imul   $0xbc8f,%ebx,%eax
    unsigned b = div * (N % G);  /* max : 48,271 * 3,399 = 164,073,129 */
80103a59:	69 d2 47 0d 00 00    	imul   $0xd47,%edx,%edx

    return *state = (a > b) ? (a - b) : (a + (N - b));
80103a5f:	39 d0                	cmp    %edx,%eax
80103a61:	77 0c                	ja     80103a6f <lcg_parkmiller+0x39>
80103a63:	29 d0                	sub    %edx,%eax
80103a65:	05 ff ff ff 7f       	add    $0x7fffffff,%eax
80103a6a:	89 01                	mov    %eax,(%ecx)
}
80103a6c:	5b                   	pop    %ebx
80103a6d:	5d                   	pop    %ebp
80103a6e:	c3                   	ret    
    return *state = (a > b) ? (a - b) : (a + (N - b));
80103a6f:	29 d0                	sub    %edx,%eax
80103a71:	eb f7                	jmp    80103a6a <lcg_parkmiller+0x34>

80103a73 <next_random>:

unsigned next_random() {
80103a73:	55                   	push   %ebp
80103a74:	89 e5                	mov    %esp,%ebp
    return lcg_parkmiller(&random_seed);
80103a76:	68 04 a0 10 80       	push   $0x8010a004
80103a7b:	e8 b6 ff ff ff       	call   80103a36 <lcg_parkmiller>
}
80103a80:	c9                   	leave  
80103a81:	c3                   	ret    

80103a82 <scheduler>:
{
80103a82:	55                   	push   %ebp
80103a83:	89 e5                	mov    %esp,%ebp
80103a85:	56                   	push   %esi
80103a86:	53                   	push   %ebx
  struct cpu *c = mycpu();
80103a87:	e8 54 f7 ff ff       	call   801031e0 <mycpu>
80103a8c:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103a8e:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103a95:	00 00 00 
80103a98:	e9 83 00 00 00       	jmp    80103b20 <scheduler+0x9e>
      release(&ptable.lock);
80103a9d:	83 ec 0c             	sub    $0xc,%esp
80103aa0:	68 40 2d 11 80       	push   $0x80112d40
80103aa5:	e8 d6 04 00 00       	call   80103f80 <release>
      continue;
80103aaa:	83 c4 10             	add    $0x10,%esp
80103aad:	eb 71                	jmp    80103b20 <scheduler+0x9e>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103aaf:	81 c3 84 00 00 00    	add    $0x84,%ebx
80103ab5:	81 fb 74 4e 11 80    	cmp    $0x80114e74,%ebx
80103abb:	73 53                	jae    80103b10 <scheduler+0x8e>
      if(p->state != RUNNABLE)
80103abd:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103ac1:	75 ec                	jne    80103aaf <scheduler+0x2d>
      counter+=p->tickets;
80103ac3:	03 4b 7c             	add    0x7c(%ebx),%ecx
      if(counter>picked){
80103ac6:	39 d1                	cmp    %edx,%ecx
80103ac8:	76 e5                	jbe    80103aaf <scheduler+0x2d>
        p -> ticks++;
80103aca:	8b 83 80 00 00 00    	mov    0x80(%ebx),%eax
80103ad0:	83 c0 01             	add    $0x1,%eax
80103ad3:	89 83 80 00 00 00    	mov    %eax,0x80(%ebx)
      c->proc = p;
80103ad9:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103adf:	83 ec 0c             	sub    $0xc,%esp
80103ae2:	53                   	push   %ebx
80103ae3:	e8 4a 28 00 00       	call   80106332 <switchuvm>
      p->state = RUNNING;
80103ae8:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
      swtch(&(c->scheduler), p->context);
80103aef:	83 c4 08             	add    $0x8,%esp
80103af2:	ff 73 1c             	pushl  0x1c(%ebx)
80103af5:	8d 46 04             	lea    0x4(%esi),%eax
80103af8:	50                   	push   %eax
80103af9:	e8 83 06 00 00       	call   80104181 <swtch>
      switchkvm();
80103afe:	e8 0a 28 00 00       	call   8010630d <switchkvm>
      c->proc = 0;
80103b03:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103b0a:	00 00 00 
      break;
80103b0d:	83 c4 10             	add    $0x10,%esp
    release(&ptable.lock);
80103b10:	83 ec 0c             	sub    $0xc,%esp
80103b13:	68 40 2d 11 80       	push   $0x80112d40
80103b18:	e8 63 04 00 00       	call   80103f80 <release>
80103b1d:	83 c4 10             	add    $0x10,%esp
  asm volatile("sti");
80103b20:	fb                   	sti    
    acquire(&ptable.lock);
80103b21:	83 ec 0c             	sub    $0xc,%esp
80103b24:	68 40 2d 11 80       	push   $0x80112d40
80103b29:	e8 ed 03 00 00       	call   80103f1b <acquire>
    unsigned tickets = totalNonUnusedTickets();
80103b2e:	e8 d3 fe ff ff       	call   80103a06 <totalNonUnusedTickets>
80103b33:	89 c3                	mov    %eax,%ebx
    if(tickets==0){
80103b35:	83 c4 10             	add    $0x10,%esp
80103b38:	85 c0                	test   %eax,%eax
80103b3a:	0f 84 5d ff ff ff    	je     80103a9d <scheduler+0x1b>
    unsigned picked = next_random()%tickets;
80103b40:	e8 2e ff ff ff       	call   80103a73 <next_random>
80103b45:	ba 00 00 00 00       	mov    $0x0,%edx
80103b4a:	f7 f3                	div    %ebx
    int counter = 0;
80103b4c:	b9 00 00 00 00       	mov    $0x0,%ecx
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103b51:	bb 74 2d 11 80       	mov    $0x80112d74,%ebx
80103b56:	e9 5a ff ff ff       	jmp    80103ab5 <scheduler+0x33>

80103b5b <dumppagetable>:
int dumppagetable(int pid){
80103b5b:	55                   	push   %ebp
80103b5c:	89 e5                	mov    %esp,%ebp
80103b5e:	57                   	push   %edi
80103b5f:	56                   	push   %esi
80103b60:	53                   	push   %ebx
80103b61:	83 ec 1c             	sub    $0x1c,%esp
  cprintf("%s %d%s\n", "START PAGE TABLE (pid", pid, ")");
80103b64:	68 1d 6d 10 80       	push   $0x80106d1d
80103b69:	ff 75 08             	pushl  0x8(%ebp)
80103b6c:	68 85 73 10 80       	push   $0x80107385
80103b71:	68 9b 73 10 80       	push   $0x8010739b
80103b76:	e8 90 ca ff ff       	call   8010060b <cprintf>
  pde_t *p, *v, *pa;
  acquire(&ptable.lock);
80103b7b:	c7 04 24 40 2d 11 80 	movl   $0x80112d40,(%esp)
80103b82:	e8 94 03 00 00       	call   80103f1b <acquire>
  for (int i = 0; i < NPROC; i++){
80103b87:	83 c4 10             	add    $0x10,%esp
80103b8a:	be 00 00 00 00       	mov    $0x0,%esi
80103b8f:	e9 eb 00 00 00       	jmp    80103c7f <dumppagetable+0x124>
          cprintf("%x", ((uint)(v) >> PTXSHIFT) & 0xFFFFF);
          cprintf(" %s", "P");
        }
        else{break;}
        if(*v & PTE_U){cprintf(" %s", "U");}
        else{cprintf(" %s", "-");}
80103b94:	83 ec 08             	sub    $0x8,%esp
80103b97:	68 a9 73 10 80       	push   $0x801073a9
80103b9c:	68 81 73 10 80       	push   $0x80107381
80103ba1:	e8 65 ca ff ff       	call   8010060b <cprintf>
80103ba6:	83 c4 10             	add    $0x10,%esp
80103ba9:	e9 ab 00 00 00       	jmp    80103c59 <dumppagetable+0xfe>
        if(*v & PTE_W){cprintf(" %s", "W");}
        else{cprintf(" %s", "-");}
80103bae:	83 ec 08             	sub    $0x8,%esp
80103bb1:	68 a9 73 10 80       	push   $0x801073a9
80103bb6:	68 81 73 10 80       	push   $0x80107381
80103bbb:	e8 4b ca ff ff       	call   8010060b <cprintf>
80103bc0:	83 c4 10             	add    $0x10,%esp
        if(((void*)v >= (void*)KERNBASE)){
80103bc3:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
80103bc9:	0f 86 ad 00 00 00    	jbe    80103c7c <dumppagetable+0x121>
    return (uint)a - KERNBASE;
80103bcf:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
          pa = (pde_t *)(V2P(v));
          cprintf(" %x\n", ((uint)(pa) >> PTXSHIFT) & 0xFFFFF);
80103bd5:	83 ec 08             	sub    $0x8,%esp
80103bd8:	c1 eb 0c             	shr    $0xc,%ebx
80103bdb:	53                   	push   %ebx
80103bdc:	68 ad 73 10 80       	push   $0x801073ad
80103be1:	e8 25 ca ff ff       	call   8010060b <cprintf>
      for (int j = 0; j < ptable.proc[i].sz; j += PGSIZE){
80103be6:	81 c7 00 10 00 00    	add    $0x1000,%edi
80103bec:	83 c4 10             	add    $0x10,%esp
80103bef:	69 c6 84 00 00 00    	imul   $0x84,%esi,%eax
80103bf5:	39 b8 74 2d 11 80    	cmp    %edi,-0x7feed28c(%eax)
80103bfb:	76 7f                	jbe    80103c7c <dumppagetable+0x121>
        v = walkpgdirhelper(p, (void *)j, 0);
80103bfd:	83 ec 04             	sub    $0x4,%esp
80103c00:	6a 00                	push   $0x0
80103c02:	57                   	push   %edi
80103c03:	ff 75 e4             	pushl  -0x1c(%ebp)
80103c06:	e8 58 26 00 00       	call   80106263 <walkpgdirhelper>
80103c0b:	89 c3                	mov    %eax,%ebx
        if (*v & PTE_P){
80103c0d:	83 c4 10             	add    $0x10,%esp
80103c10:	f6 00 01             	testb  $0x1,(%eax)
80103c13:	74 67                	je     80103c7c <dumppagetable+0x121>
          cprintf("%x", ((uint)(v) >> PTXSHIFT) & 0xFFFFF);
80103c15:	83 ec 08             	sub    $0x8,%esp
80103c18:	c1 e8 0c             	shr    $0xc,%eax
80103c1b:	50                   	push   %eax
80103c1c:	68 a4 73 10 80       	push   $0x801073a4
80103c21:	e8 e5 c9 ff ff       	call   8010060b <cprintf>
          cprintf(" %s", "P");
80103c26:	83 c4 08             	add    $0x8,%esp
80103c29:	68 7a 72 10 80       	push   $0x8010727a
80103c2e:	68 81 73 10 80       	push   $0x80107381
80103c33:	e8 d3 c9 ff ff       	call   8010060b <cprintf>
        if(*v & PTE_U){cprintf(" %s", "U");}
80103c38:	83 c4 10             	add    $0x10,%esp
80103c3b:	f6 03 04             	testb  $0x4,(%ebx)
80103c3e:	0f 84 50 ff ff ff    	je     80103b94 <dumppagetable+0x39>
80103c44:	83 ec 08             	sub    $0x8,%esp
80103c47:	68 a7 73 10 80       	push   $0x801073a7
80103c4c:	68 81 73 10 80       	push   $0x80107381
80103c51:	e8 b5 c9 ff ff       	call   8010060b <cprintf>
80103c56:	83 c4 10             	add    $0x10,%esp
        if(*v & PTE_W){cprintf(" %s", "W");}
80103c59:	f6 03 02             	testb  $0x2,(%ebx)
80103c5c:	0f 84 4c ff ff ff    	je     80103bae <dumppagetable+0x53>
80103c62:	83 ec 08             	sub    $0x8,%esp
80103c65:	68 ab 73 10 80       	push   $0x801073ab
80103c6a:	68 81 73 10 80       	push   $0x80107381
80103c6f:	e8 97 c9 ff ff       	call   8010060b <cprintf>
80103c74:	83 c4 10             	add    $0x10,%esp
80103c77:	e9 47 ff ff ff       	jmp    80103bc3 <dumppagetable+0x68>
  for (int i = 0; i < NPROC; i++){
80103c7c:	83 c6 01             	add    $0x1,%esi
80103c7f:	83 fe 3f             	cmp    $0x3f,%esi
80103c82:	7f 24                	jg     80103ca8 <dumppagetable+0x14d>
    if (pid == ptable.proc[i].pid){
80103c84:	69 c6 84 00 00 00    	imul   $0x84,%esi,%eax
80103c8a:	8b 55 08             	mov    0x8(%ebp),%edx
80103c8d:	39 90 84 2d 11 80    	cmp    %edx,-0x7feed27c(%eax)
80103c93:	75 e7                	jne    80103c7c <dumppagetable+0x121>
      p = ptable.proc[i].pgdir;
80103c95:	8b 80 78 2d 11 80    	mov    -0x7feed288(%eax),%eax
80103c9b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      for (int j = 0; j < ptable.proc[i].sz; j += PGSIZE){
80103c9e:	bf 00 00 00 00       	mov    $0x0,%edi
80103ca3:	e9 47 ff ff ff       	jmp    80103bef <dumppagetable+0x94>
        }
        else{break;}
      }
    }
  }
  release(&ptable.lock);
80103ca8:	83 ec 0c             	sub    $0xc,%esp
80103cab:	68 40 2d 11 80       	push   $0x80112d40
80103cb0:	e8 cb 02 00 00       	call   80103f80 <release>
  cprintf("%s", "END PAGE TABLE\n");
80103cb5:	83 c4 08             	add    $0x8,%esp
80103cb8:	68 b2 73 10 80       	push   $0x801073b2
80103cbd:	68 82 73 10 80       	push   $0x80107382
80103cc2:	e8 44 c9 ff ff       	call   8010060b <cprintf>
  return 0;
80103cc7:	b8 00 00 00 00       	mov    $0x0,%eax
80103ccc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103ccf:	5b                   	pop    %ebx
80103cd0:	5e                   	pop    %esi
80103cd1:	5f                   	pop    %edi
80103cd2:	5d                   	pop    %ebp
80103cd3:	c3                   	ret    

80103cd4 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
80103cd4:	55                   	push   %ebp
80103cd5:	89 e5                	mov    %esp,%ebp
80103cd7:	53                   	push   %ebx
80103cd8:	83 ec 0c             	sub    $0xc,%esp
80103cdb:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
80103cde:	68 2c 74 10 80       	push   $0x8010742c
80103ce3:	8d 43 04             	lea    0x4(%ebx),%eax
80103ce6:	50                   	push   %eax
80103ce7:	e8 f3 00 00 00       	call   80103ddf <initlock>
  lk->name = name;
80103cec:	8b 45 0c             	mov    0xc(%ebp),%eax
80103cef:	89 43 38             	mov    %eax,0x38(%ebx)
  lk->locked = 0;
80103cf2:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
80103cf8:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
}
80103cff:	83 c4 10             	add    $0x10,%esp
80103d02:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103d05:	c9                   	leave  
80103d06:	c3                   	ret    

80103d07 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
80103d07:	55                   	push   %ebp
80103d08:	89 e5                	mov    %esp,%ebp
80103d0a:	56                   	push   %esi
80103d0b:	53                   	push   %ebx
80103d0c:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80103d0f:	8d 73 04             	lea    0x4(%ebx),%esi
80103d12:	83 ec 0c             	sub    $0xc,%esp
80103d15:	56                   	push   %esi
80103d16:	e8 00 02 00 00       	call   80103f1b <acquire>
  while (lk->locked) {
80103d1b:	83 c4 10             	add    $0x10,%esp
80103d1e:	eb 0d                	jmp    80103d2d <acquiresleep+0x26>
    sleep(lk, &lk->lk);
80103d20:	83 ec 08             	sub    $0x8,%esp
80103d23:	56                   	push   %esi
80103d24:	53                   	push   %ebx
80103d25:	e8 69 f9 ff ff       	call   80103693 <sleep>
80103d2a:	83 c4 10             	add    $0x10,%esp
  while (lk->locked) {
80103d2d:	83 3b 00             	cmpl   $0x0,(%ebx)
80103d30:	75 ee                	jne    80103d20 <acquiresleep+0x19>
  }
  lk->locked = 1;
80103d32:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
80103d38:	e8 1a f5 ff ff       	call   80103257 <myproc>
80103d3d:	8b 40 10             	mov    0x10(%eax),%eax
80103d40:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
80103d43:	83 ec 0c             	sub    $0xc,%esp
80103d46:	56                   	push   %esi
80103d47:	e8 34 02 00 00       	call   80103f80 <release>
}
80103d4c:	83 c4 10             	add    $0x10,%esp
80103d4f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103d52:	5b                   	pop    %ebx
80103d53:	5e                   	pop    %esi
80103d54:	5d                   	pop    %ebp
80103d55:	c3                   	ret    

80103d56 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80103d56:	55                   	push   %ebp
80103d57:	89 e5                	mov    %esp,%ebp
80103d59:	56                   	push   %esi
80103d5a:	53                   	push   %ebx
80103d5b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80103d5e:	8d 73 04             	lea    0x4(%ebx),%esi
80103d61:	83 ec 0c             	sub    $0xc,%esp
80103d64:	56                   	push   %esi
80103d65:	e8 b1 01 00 00       	call   80103f1b <acquire>
  lk->locked = 0;
80103d6a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
80103d70:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80103d77:	89 1c 24             	mov    %ebx,(%esp)
80103d7a:	e8 7c fa ff ff       	call   801037fb <wakeup>
  release(&lk->lk);
80103d7f:	89 34 24             	mov    %esi,(%esp)
80103d82:	e8 f9 01 00 00       	call   80103f80 <release>
}
80103d87:	83 c4 10             	add    $0x10,%esp
80103d8a:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103d8d:	5b                   	pop    %ebx
80103d8e:	5e                   	pop    %esi
80103d8f:	5d                   	pop    %ebp
80103d90:	c3                   	ret    

80103d91 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80103d91:	55                   	push   %ebp
80103d92:	89 e5                	mov    %esp,%ebp
80103d94:	56                   	push   %esi
80103d95:	53                   	push   %ebx
80103d96:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
80103d99:	8d 73 04             	lea    0x4(%ebx),%esi
80103d9c:	83 ec 0c             	sub    $0xc,%esp
80103d9f:	56                   	push   %esi
80103da0:	e8 76 01 00 00       	call   80103f1b <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
80103da5:	83 c4 10             	add    $0x10,%esp
80103da8:	83 3b 00             	cmpl   $0x0,(%ebx)
80103dab:	75 17                	jne    80103dc4 <holdingsleep+0x33>
80103dad:	bb 00 00 00 00       	mov    $0x0,%ebx
  release(&lk->lk);
80103db2:	83 ec 0c             	sub    $0xc,%esp
80103db5:	56                   	push   %esi
80103db6:	e8 c5 01 00 00       	call   80103f80 <release>
  return r;
}
80103dbb:	89 d8                	mov    %ebx,%eax
80103dbd:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103dc0:	5b                   	pop    %ebx
80103dc1:	5e                   	pop    %esi
80103dc2:	5d                   	pop    %ebp
80103dc3:	c3                   	ret    
  r = lk->locked && (lk->pid == myproc()->pid);
80103dc4:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
80103dc7:	e8 8b f4 ff ff       	call   80103257 <myproc>
80103dcc:	3b 58 10             	cmp    0x10(%eax),%ebx
80103dcf:	74 07                	je     80103dd8 <holdingsleep+0x47>
80103dd1:	bb 00 00 00 00       	mov    $0x0,%ebx
80103dd6:	eb da                	jmp    80103db2 <holdingsleep+0x21>
80103dd8:	bb 01 00 00 00       	mov    $0x1,%ebx
80103ddd:	eb d3                	jmp    80103db2 <holdingsleep+0x21>

80103ddf <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
80103ddf:	55                   	push   %ebp
80103de0:	89 e5                	mov    %esp,%ebp
80103de2:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
80103de5:	8b 55 0c             	mov    0xc(%ebp),%edx
80103de8:	89 50 04             	mov    %edx,0x4(%eax)
  lk->locked = 0;
80103deb:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->cpu = 0;
80103df1:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
80103df8:	5d                   	pop    %ebp
80103df9:	c3                   	ret    

80103dfa <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80103dfa:	55                   	push   %ebp
80103dfb:	89 e5                	mov    %esp,%ebp
80103dfd:	53                   	push   %ebx
80103dfe:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
80103e01:	8b 45 08             	mov    0x8(%ebp),%eax
80103e04:	8d 50 f8             	lea    -0x8(%eax),%edx
  for(i = 0; i < 10; i++){
80103e07:	b8 00 00 00 00       	mov    $0x0,%eax
80103e0c:	83 f8 09             	cmp    $0x9,%eax
80103e0f:	7f 25                	jg     80103e36 <getcallerpcs+0x3c>
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80103e11:	8d 9a 00 00 00 80    	lea    -0x80000000(%edx),%ebx
80103e17:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
80103e1d:	77 17                	ja     80103e36 <getcallerpcs+0x3c>
      break;
    pcs[i] = ebp[1];     // saved %eip
80103e1f:	8b 5a 04             	mov    0x4(%edx),%ebx
80103e22:	89 1c 81             	mov    %ebx,(%ecx,%eax,4)
    ebp = (uint*)ebp[0]; // saved %ebp
80103e25:	8b 12                	mov    (%edx),%edx
  for(i = 0; i < 10; i++){
80103e27:	83 c0 01             	add    $0x1,%eax
80103e2a:	eb e0                	jmp    80103e0c <getcallerpcs+0x12>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
80103e2c:	c7 04 81 00 00 00 00 	movl   $0x0,(%ecx,%eax,4)
  for(; i < 10; i++)
80103e33:	83 c0 01             	add    $0x1,%eax
80103e36:	83 f8 09             	cmp    $0x9,%eax
80103e39:	7e f1                	jle    80103e2c <getcallerpcs+0x32>
}
80103e3b:	5b                   	pop    %ebx
80103e3c:	5d                   	pop    %ebp
80103e3d:	c3                   	ret    

80103e3e <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80103e3e:	55                   	push   %ebp
80103e3f:	89 e5                	mov    %esp,%ebp
80103e41:	53                   	push   %ebx
80103e42:	83 ec 04             	sub    $0x4,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103e45:	9c                   	pushf  
80103e46:	5b                   	pop    %ebx
  asm volatile("cli");
80103e47:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
80103e48:	e8 93 f3 ff ff       	call   801031e0 <mycpu>
80103e4d:	83 b8 a4 00 00 00 00 	cmpl   $0x0,0xa4(%eax)
80103e54:	74 12                	je     80103e68 <pushcli+0x2a>
    mycpu()->intena = eflags & FL_IF;
  mycpu()->ncli += 1;
80103e56:	e8 85 f3 ff ff       	call   801031e0 <mycpu>
80103e5b:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80103e62:	83 c4 04             	add    $0x4,%esp
80103e65:	5b                   	pop    %ebx
80103e66:	5d                   	pop    %ebp
80103e67:	c3                   	ret    
    mycpu()->intena = eflags & FL_IF;
80103e68:	e8 73 f3 ff ff       	call   801031e0 <mycpu>
80103e6d:	81 e3 00 02 00 00    	and    $0x200,%ebx
80103e73:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
80103e79:	eb db                	jmp    80103e56 <pushcli+0x18>

80103e7b <popcli>:

void
popcli(void)
{
80103e7b:	55                   	push   %ebp
80103e7c:	89 e5                	mov    %esp,%ebp
80103e7e:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103e81:	9c                   	pushf  
80103e82:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103e83:	f6 c4 02             	test   $0x2,%ah
80103e86:	75 28                	jne    80103eb0 <popcli+0x35>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
80103e88:	e8 53 f3 ff ff       	call   801031e0 <mycpu>
80103e8d:	8b 88 a4 00 00 00    	mov    0xa4(%eax),%ecx
80103e93:	8d 51 ff             	lea    -0x1(%ecx),%edx
80103e96:	89 90 a4 00 00 00    	mov    %edx,0xa4(%eax)
80103e9c:	85 d2                	test   %edx,%edx
80103e9e:	78 1d                	js     80103ebd <popcli+0x42>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
80103ea0:	e8 3b f3 ff ff       	call   801031e0 <mycpu>
80103ea5:	83 b8 a4 00 00 00 00 	cmpl   $0x0,0xa4(%eax)
80103eac:	74 1c                	je     80103eca <popcli+0x4f>
    sti();
}
80103eae:	c9                   	leave  
80103eaf:	c3                   	ret    
    panic("popcli - interruptible");
80103eb0:	83 ec 0c             	sub    $0xc,%esp
80103eb3:	68 37 74 10 80       	push   $0x80107437
80103eb8:	e8 8b c4 ff ff       	call   80100348 <panic>
    panic("popcli");
80103ebd:	83 ec 0c             	sub    $0xc,%esp
80103ec0:	68 4e 74 10 80       	push   $0x8010744e
80103ec5:	e8 7e c4 ff ff       	call   80100348 <panic>
  if(mycpu()->ncli == 0 && mycpu()->intena)
80103eca:	e8 11 f3 ff ff       	call   801031e0 <mycpu>
80103ecf:	83 b8 a8 00 00 00 00 	cmpl   $0x0,0xa8(%eax)
80103ed6:	74 d6                	je     80103eae <popcli+0x33>
  asm volatile("sti");
80103ed8:	fb                   	sti    
}
80103ed9:	eb d3                	jmp    80103eae <popcli+0x33>

80103edb <holding>:
{
80103edb:	55                   	push   %ebp
80103edc:	89 e5                	mov    %esp,%ebp
80103ede:	53                   	push   %ebx
80103edf:	83 ec 04             	sub    $0x4,%esp
80103ee2:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pushcli();
80103ee5:	e8 54 ff ff ff       	call   80103e3e <pushcli>
  r = lock->locked && lock->cpu == mycpu();
80103eea:	83 3b 00             	cmpl   $0x0,(%ebx)
80103eed:	75 12                	jne    80103f01 <holding+0x26>
80103eef:	bb 00 00 00 00       	mov    $0x0,%ebx
  popcli();
80103ef4:	e8 82 ff ff ff       	call   80103e7b <popcli>
}
80103ef9:	89 d8                	mov    %ebx,%eax
80103efb:	83 c4 04             	add    $0x4,%esp
80103efe:	5b                   	pop    %ebx
80103eff:	5d                   	pop    %ebp
80103f00:	c3                   	ret    
  r = lock->locked && lock->cpu == mycpu();
80103f01:	8b 5b 08             	mov    0x8(%ebx),%ebx
80103f04:	e8 d7 f2 ff ff       	call   801031e0 <mycpu>
80103f09:	39 c3                	cmp    %eax,%ebx
80103f0b:	74 07                	je     80103f14 <holding+0x39>
80103f0d:	bb 00 00 00 00       	mov    $0x0,%ebx
80103f12:	eb e0                	jmp    80103ef4 <holding+0x19>
80103f14:	bb 01 00 00 00       	mov    $0x1,%ebx
80103f19:	eb d9                	jmp    80103ef4 <holding+0x19>

80103f1b <acquire>:
{
80103f1b:	55                   	push   %ebp
80103f1c:	89 e5                	mov    %esp,%ebp
80103f1e:	53                   	push   %ebx
80103f1f:	83 ec 04             	sub    $0x4,%esp
  pushcli(); // disable interrupts to avoid deadlock.
80103f22:	e8 17 ff ff ff       	call   80103e3e <pushcli>
  if(holding(lk))
80103f27:	83 ec 0c             	sub    $0xc,%esp
80103f2a:	ff 75 08             	pushl  0x8(%ebp)
80103f2d:	e8 a9 ff ff ff       	call   80103edb <holding>
80103f32:	83 c4 10             	add    $0x10,%esp
80103f35:	85 c0                	test   %eax,%eax
80103f37:	75 3a                	jne    80103f73 <acquire+0x58>
  while(xchg(&lk->locked, 1) != 0)
80103f39:	8b 55 08             	mov    0x8(%ebp),%edx
  asm volatile("lock; xchgl %0, %1" :
80103f3c:	b8 01 00 00 00       	mov    $0x1,%eax
80103f41:	f0 87 02             	lock xchg %eax,(%edx)
80103f44:	85 c0                	test   %eax,%eax
80103f46:	75 f1                	jne    80103f39 <acquire+0x1e>
  __sync_synchronize();
80103f48:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
80103f4d:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103f50:	e8 8b f2 ff ff       	call   801031e0 <mycpu>
80103f55:	89 43 08             	mov    %eax,0x8(%ebx)
  getcallerpcs(&lk, lk->pcs);
80103f58:	8b 45 08             	mov    0x8(%ebp),%eax
80103f5b:	83 c0 0c             	add    $0xc,%eax
80103f5e:	83 ec 08             	sub    $0x8,%esp
80103f61:	50                   	push   %eax
80103f62:	8d 45 08             	lea    0x8(%ebp),%eax
80103f65:	50                   	push   %eax
80103f66:	e8 8f fe ff ff       	call   80103dfa <getcallerpcs>
}
80103f6b:	83 c4 10             	add    $0x10,%esp
80103f6e:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103f71:	c9                   	leave  
80103f72:	c3                   	ret    
    panic("acquire");
80103f73:	83 ec 0c             	sub    $0xc,%esp
80103f76:	68 55 74 10 80       	push   $0x80107455
80103f7b:	e8 c8 c3 ff ff       	call   80100348 <panic>

80103f80 <release>:
{
80103f80:	55                   	push   %ebp
80103f81:	89 e5                	mov    %esp,%ebp
80103f83:	53                   	push   %ebx
80103f84:	83 ec 10             	sub    $0x10,%esp
80103f87:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
80103f8a:	53                   	push   %ebx
80103f8b:	e8 4b ff ff ff       	call   80103edb <holding>
80103f90:	83 c4 10             	add    $0x10,%esp
80103f93:	85 c0                	test   %eax,%eax
80103f95:	74 23                	je     80103fba <release+0x3a>
  lk->pcs[0] = 0;
80103f97:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
80103f9e:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
80103fa5:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
80103faa:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  popcli();
80103fb0:	e8 c6 fe ff ff       	call   80103e7b <popcli>
}
80103fb5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103fb8:	c9                   	leave  
80103fb9:	c3                   	ret    
    panic("release");
80103fba:	83 ec 0c             	sub    $0xc,%esp
80103fbd:	68 5d 74 10 80       	push   $0x8010745d
80103fc2:	e8 81 c3 ff ff       	call   80100348 <panic>

80103fc7 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80103fc7:	55                   	push   %ebp
80103fc8:	89 e5                	mov    %esp,%ebp
80103fca:	57                   	push   %edi
80103fcb:	53                   	push   %ebx
80103fcc:	8b 55 08             	mov    0x8(%ebp),%edx
80103fcf:	8b 4d 10             	mov    0x10(%ebp),%ecx
  if ((int)dst%4 == 0 && n%4 == 0){
80103fd2:	f6 c2 03             	test   $0x3,%dl
80103fd5:	75 05                	jne    80103fdc <memset+0x15>
80103fd7:	f6 c1 03             	test   $0x3,%cl
80103fda:	74 0e                	je     80103fea <memset+0x23>
  asm volatile("cld; rep stosb" :
80103fdc:	89 d7                	mov    %edx,%edi
80103fde:	8b 45 0c             	mov    0xc(%ebp),%eax
80103fe1:	fc                   	cld    
80103fe2:	f3 aa                	rep stos %al,%es:(%edi)
    c &= 0xFF;
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
  } else
    stosb(dst, c, n);
  return dst;
}
80103fe4:	89 d0                	mov    %edx,%eax
80103fe6:	5b                   	pop    %ebx
80103fe7:	5f                   	pop    %edi
80103fe8:	5d                   	pop    %ebp
80103fe9:	c3                   	ret    
    c &= 0xFF;
80103fea:	0f b6 7d 0c          	movzbl 0xc(%ebp),%edi
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80103fee:	c1 e9 02             	shr    $0x2,%ecx
80103ff1:	89 f8                	mov    %edi,%eax
80103ff3:	c1 e0 18             	shl    $0x18,%eax
80103ff6:	89 fb                	mov    %edi,%ebx
80103ff8:	c1 e3 10             	shl    $0x10,%ebx
80103ffb:	09 d8                	or     %ebx,%eax
80103ffd:	89 fb                	mov    %edi,%ebx
80103fff:	c1 e3 08             	shl    $0x8,%ebx
80104002:	09 d8                	or     %ebx,%eax
80104004:	09 f8                	or     %edi,%eax
  asm volatile("cld; rep stosl" :
80104006:	89 d7                	mov    %edx,%edi
80104008:	fc                   	cld    
80104009:	f3 ab                	rep stos %eax,%es:(%edi)
8010400b:	eb d7                	jmp    80103fe4 <memset+0x1d>

8010400d <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
8010400d:	55                   	push   %ebp
8010400e:	89 e5                	mov    %esp,%ebp
80104010:	56                   	push   %esi
80104011:	53                   	push   %ebx
80104012:	8b 4d 08             	mov    0x8(%ebp),%ecx
80104015:	8b 55 0c             	mov    0xc(%ebp),%edx
80104018:	8b 45 10             	mov    0x10(%ebp),%eax
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
8010401b:	8d 70 ff             	lea    -0x1(%eax),%esi
8010401e:	85 c0                	test   %eax,%eax
80104020:	74 1c                	je     8010403e <memcmp+0x31>
    if(*s1 != *s2)
80104022:	0f b6 01             	movzbl (%ecx),%eax
80104025:	0f b6 1a             	movzbl (%edx),%ebx
80104028:	38 d8                	cmp    %bl,%al
8010402a:	75 0a                	jne    80104036 <memcmp+0x29>
      return *s1 - *s2;
    s1++, s2++;
8010402c:	83 c1 01             	add    $0x1,%ecx
8010402f:	83 c2 01             	add    $0x1,%edx
  while(n-- > 0){
80104032:	89 f0                	mov    %esi,%eax
80104034:	eb e5                	jmp    8010401b <memcmp+0xe>
      return *s1 - *s2;
80104036:	0f b6 c0             	movzbl %al,%eax
80104039:	0f b6 db             	movzbl %bl,%ebx
8010403c:	29 d8                	sub    %ebx,%eax
  }

  return 0;
}
8010403e:	5b                   	pop    %ebx
8010403f:	5e                   	pop    %esi
80104040:	5d                   	pop    %ebp
80104041:	c3                   	ret    

80104042 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
80104042:	55                   	push   %ebp
80104043:	89 e5                	mov    %esp,%ebp
80104045:	56                   	push   %esi
80104046:	53                   	push   %ebx
80104047:	8b 45 08             	mov    0x8(%ebp),%eax
8010404a:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010404d:	8b 55 10             	mov    0x10(%ebp),%edx
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
80104050:	39 c1                	cmp    %eax,%ecx
80104052:	73 3a                	jae    8010408e <memmove+0x4c>
80104054:	8d 1c 11             	lea    (%ecx,%edx,1),%ebx
80104057:	39 c3                	cmp    %eax,%ebx
80104059:	76 37                	jbe    80104092 <memmove+0x50>
    s += n;
    d += n;
8010405b:	8d 0c 10             	lea    (%eax,%edx,1),%ecx
    while(n-- > 0)
8010405e:	eb 0d                	jmp    8010406d <memmove+0x2b>
      *--d = *--s;
80104060:	83 eb 01             	sub    $0x1,%ebx
80104063:	83 e9 01             	sub    $0x1,%ecx
80104066:	0f b6 13             	movzbl (%ebx),%edx
80104069:	88 11                	mov    %dl,(%ecx)
    while(n-- > 0)
8010406b:	89 f2                	mov    %esi,%edx
8010406d:	8d 72 ff             	lea    -0x1(%edx),%esi
80104070:	85 d2                	test   %edx,%edx
80104072:	75 ec                	jne    80104060 <memmove+0x1e>
80104074:	eb 14                	jmp    8010408a <memmove+0x48>
  } else
    while(n-- > 0)
      *d++ = *s++;
80104076:	0f b6 11             	movzbl (%ecx),%edx
80104079:	88 13                	mov    %dl,(%ebx)
8010407b:	8d 5b 01             	lea    0x1(%ebx),%ebx
8010407e:	8d 49 01             	lea    0x1(%ecx),%ecx
    while(n-- > 0)
80104081:	89 f2                	mov    %esi,%edx
80104083:	8d 72 ff             	lea    -0x1(%edx),%esi
80104086:	85 d2                	test   %edx,%edx
80104088:	75 ec                	jne    80104076 <memmove+0x34>

  return dst;
}
8010408a:	5b                   	pop    %ebx
8010408b:	5e                   	pop    %esi
8010408c:	5d                   	pop    %ebp
8010408d:	c3                   	ret    
8010408e:	89 c3                	mov    %eax,%ebx
80104090:	eb f1                	jmp    80104083 <memmove+0x41>
80104092:	89 c3                	mov    %eax,%ebx
80104094:	eb ed                	jmp    80104083 <memmove+0x41>

80104096 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
80104096:	55                   	push   %ebp
80104097:	89 e5                	mov    %esp,%ebp
  return memmove(dst, src, n);
80104099:	ff 75 10             	pushl  0x10(%ebp)
8010409c:	ff 75 0c             	pushl  0xc(%ebp)
8010409f:	ff 75 08             	pushl  0x8(%ebp)
801040a2:	e8 9b ff ff ff       	call   80104042 <memmove>
}
801040a7:	c9                   	leave  
801040a8:	c3                   	ret    

801040a9 <strncmp>:

int
strncmp(const char *p, const char *q, uint n)
{
801040a9:	55                   	push   %ebp
801040aa:	89 e5                	mov    %esp,%ebp
801040ac:	53                   	push   %ebx
801040ad:	8b 55 08             	mov    0x8(%ebp),%edx
801040b0:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801040b3:	8b 45 10             	mov    0x10(%ebp),%eax
  while(n > 0 && *p && *p == *q)
801040b6:	eb 09                	jmp    801040c1 <strncmp+0x18>
    n--, p++, q++;
801040b8:	83 e8 01             	sub    $0x1,%eax
801040bb:	83 c2 01             	add    $0x1,%edx
801040be:	83 c1 01             	add    $0x1,%ecx
  while(n > 0 && *p && *p == *q)
801040c1:	85 c0                	test   %eax,%eax
801040c3:	74 0b                	je     801040d0 <strncmp+0x27>
801040c5:	0f b6 1a             	movzbl (%edx),%ebx
801040c8:	84 db                	test   %bl,%bl
801040ca:	74 04                	je     801040d0 <strncmp+0x27>
801040cc:	3a 19                	cmp    (%ecx),%bl
801040ce:	74 e8                	je     801040b8 <strncmp+0xf>
  if(n == 0)
801040d0:	85 c0                	test   %eax,%eax
801040d2:	74 0b                	je     801040df <strncmp+0x36>
    return 0;
  return (uchar)*p - (uchar)*q;
801040d4:	0f b6 02             	movzbl (%edx),%eax
801040d7:	0f b6 11             	movzbl (%ecx),%edx
801040da:	29 d0                	sub    %edx,%eax
}
801040dc:	5b                   	pop    %ebx
801040dd:	5d                   	pop    %ebp
801040de:	c3                   	ret    
    return 0;
801040df:	b8 00 00 00 00       	mov    $0x0,%eax
801040e4:	eb f6                	jmp    801040dc <strncmp+0x33>

801040e6 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
801040e6:	55                   	push   %ebp
801040e7:	89 e5                	mov    %esp,%ebp
801040e9:	57                   	push   %edi
801040ea:	56                   	push   %esi
801040eb:	53                   	push   %ebx
801040ec:	8b 5d 0c             	mov    0xc(%ebp),%ebx
801040ef:	8b 4d 10             	mov    0x10(%ebp),%ecx
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
801040f2:	8b 45 08             	mov    0x8(%ebp),%eax
801040f5:	eb 04                	jmp    801040fb <strncpy+0x15>
801040f7:	89 fb                	mov    %edi,%ebx
801040f9:	89 f0                	mov    %esi,%eax
801040fb:	8d 51 ff             	lea    -0x1(%ecx),%edx
801040fe:	85 c9                	test   %ecx,%ecx
80104100:	7e 1d                	jle    8010411f <strncpy+0x39>
80104102:	8d 7b 01             	lea    0x1(%ebx),%edi
80104105:	8d 70 01             	lea    0x1(%eax),%esi
80104108:	0f b6 1b             	movzbl (%ebx),%ebx
8010410b:	88 18                	mov    %bl,(%eax)
8010410d:	89 d1                	mov    %edx,%ecx
8010410f:	84 db                	test   %bl,%bl
80104111:	75 e4                	jne    801040f7 <strncpy+0x11>
80104113:	89 f0                	mov    %esi,%eax
80104115:	eb 08                	jmp    8010411f <strncpy+0x39>
    ;
  while(n-- > 0)
    *s++ = 0;
80104117:	c6 00 00             	movb   $0x0,(%eax)
  while(n-- > 0)
8010411a:	89 ca                	mov    %ecx,%edx
    *s++ = 0;
8010411c:	8d 40 01             	lea    0x1(%eax),%eax
  while(n-- > 0)
8010411f:	8d 4a ff             	lea    -0x1(%edx),%ecx
80104122:	85 d2                	test   %edx,%edx
80104124:	7f f1                	jg     80104117 <strncpy+0x31>
  return os;
}
80104126:	8b 45 08             	mov    0x8(%ebp),%eax
80104129:	5b                   	pop    %ebx
8010412a:	5e                   	pop    %esi
8010412b:	5f                   	pop    %edi
8010412c:	5d                   	pop    %ebp
8010412d:	c3                   	ret    

8010412e <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
8010412e:	55                   	push   %ebp
8010412f:	89 e5                	mov    %esp,%ebp
80104131:	57                   	push   %edi
80104132:	56                   	push   %esi
80104133:	53                   	push   %ebx
80104134:	8b 45 08             	mov    0x8(%ebp),%eax
80104137:	8b 5d 0c             	mov    0xc(%ebp),%ebx
8010413a:	8b 55 10             	mov    0x10(%ebp),%edx
  char *os;

  os = s;
  if(n <= 0)
8010413d:	85 d2                	test   %edx,%edx
8010413f:	7e 23                	jle    80104164 <safestrcpy+0x36>
80104141:	89 c1                	mov    %eax,%ecx
80104143:	eb 04                	jmp    80104149 <safestrcpy+0x1b>
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
80104145:	89 fb                	mov    %edi,%ebx
80104147:	89 f1                	mov    %esi,%ecx
80104149:	83 ea 01             	sub    $0x1,%edx
8010414c:	85 d2                	test   %edx,%edx
8010414e:	7e 11                	jle    80104161 <safestrcpy+0x33>
80104150:	8d 7b 01             	lea    0x1(%ebx),%edi
80104153:	8d 71 01             	lea    0x1(%ecx),%esi
80104156:	0f b6 1b             	movzbl (%ebx),%ebx
80104159:	88 19                	mov    %bl,(%ecx)
8010415b:	84 db                	test   %bl,%bl
8010415d:	75 e6                	jne    80104145 <safestrcpy+0x17>
8010415f:	89 f1                	mov    %esi,%ecx
    ;
  *s = 0;
80104161:	c6 01 00             	movb   $0x0,(%ecx)
  return os;
}
80104164:	5b                   	pop    %ebx
80104165:	5e                   	pop    %esi
80104166:	5f                   	pop    %edi
80104167:	5d                   	pop    %ebp
80104168:	c3                   	ret    

80104169 <strlen>:

int
strlen(const char *s)
{
80104169:	55                   	push   %ebp
8010416a:	89 e5                	mov    %esp,%ebp
8010416c:	8b 55 08             	mov    0x8(%ebp),%edx
  int n;

  for(n = 0; s[n]; n++)
8010416f:	b8 00 00 00 00       	mov    $0x0,%eax
80104174:	eb 03                	jmp    80104179 <strlen+0x10>
80104176:	83 c0 01             	add    $0x1,%eax
80104179:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
8010417d:	75 f7                	jne    80104176 <strlen+0xd>
    ;
  return n;
}
8010417f:	5d                   	pop    %ebp
80104180:	c3                   	ret    

80104181 <swtch>:
# a struct context, and save its address in *old.
# Switch stacks to new and pop previously-saved registers.

.globl swtch
swtch:
  movl 4(%esp), %eax
80104181:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
80104185:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-saved registers
  pushl %ebp
80104189:	55                   	push   %ebp
  pushl %ebx
8010418a:	53                   	push   %ebx
  pushl %esi
8010418b:	56                   	push   %esi
  pushl %edi
8010418c:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
8010418d:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
8010418f:	89 d4                	mov    %edx,%esp

  # Load new callee-saved registers
  popl %edi
80104191:	5f                   	pop    %edi
  popl %esi
80104192:	5e                   	pop    %esi
  popl %ebx
80104193:	5b                   	pop    %ebx
  popl %ebp
80104194:	5d                   	pop    %ebp
  ret
80104195:	c3                   	ret    

80104196 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80104196:	55                   	push   %ebp
80104197:	89 e5                	mov    %esp,%ebp
80104199:	53                   	push   %ebx
8010419a:	83 ec 04             	sub    $0x4,%esp
8010419d:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
801041a0:	e8 b2 f0 ff ff       	call   80103257 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
801041a5:	8b 00                	mov    (%eax),%eax
801041a7:	39 d8                	cmp    %ebx,%eax
801041a9:	76 19                	jbe    801041c4 <fetchint+0x2e>
801041ab:	8d 53 04             	lea    0x4(%ebx),%edx
801041ae:	39 d0                	cmp    %edx,%eax
801041b0:	72 19                	jb     801041cb <fetchint+0x35>
    return -1;
  *ip = *(int*)(addr);
801041b2:	8b 13                	mov    (%ebx),%edx
801041b4:	8b 45 0c             	mov    0xc(%ebp),%eax
801041b7:	89 10                	mov    %edx,(%eax)
  return 0;
801041b9:	b8 00 00 00 00       	mov    $0x0,%eax
}
801041be:	83 c4 04             	add    $0x4,%esp
801041c1:	5b                   	pop    %ebx
801041c2:	5d                   	pop    %ebp
801041c3:	c3                   	ret    
    return -1;
801041c4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801041c9:	eb f3                	jmp    801041be <fetchint+0x28>
801041cb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801041d0:	eb ec                	jmp    801041be <fetchint+0x28>

801041d2 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
801041d2:	55                   	push   %ebp
801041d3:	89 e5                	mov    %esp,%ebp
801041d5:	53                   	push   %ebx
801041d6:	83 ec 04             	sub    $0x4,%esp
801041d9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
801041dc:	e8 76 f0 ff ff       	call   80103257 <myproc>

  if(addr >= curproc->sz)
801041e1:	39 18                	cmp    %ebx,(%eax)
801041e3:	76 26                	jbe    8010420b <fetchstr+0x39>
    return -1;
  *pp = (char*)addr;
801041e5:	8b 55 0c             	mov    0xc(%ebp),%edx
801041e8:	89 1a                	mov    %ebx,(%edx)
  ep = (char*)curproc->sz;
801041ea:	8b 10                	mov    (%eax),%edx
  for(s = *pp; s < ep; s++){
801041ec:	89 d8                	mov    %ebx,%eax
801041ee:	39 d0                	cmp    %edx,%eax
801041f0:	73 0e                	jae    80104200 <fetchstr+0x2e>
    if(*s == 0)
801041f2:	80 38 00             	cmpb   $0x0,(%eax)
801041f5:	74 05                	je     801041fc <fetchstr+0x2a>
  for(s = *pp; s < ep; s++){
801041f7:	83 c0 01             	add    $0x1,%eax
801041fa:	eb f2                	jmp    801041ee <fetchstr+0x1c>
      return s - *pp;
801041fc:	29 d8                	sub    %ebx,%eax
801041fe:	eb 05                	jmp    80104205 <fetchstr+0x33>
  }
  return -1;
80104200:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104205:	83 c4 04             	add    $0x4,%esp
80104208:	5b                   	pop    %ebx
80104209:	5d                   	pop    %ebp
8010420a:	c3                   	ret    
    return -1;
8010420b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104210:	eb f3                	jmp    80104205 <fetchstr+0x33>

80104212 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80104212:	55                   	push   %ebp
80104213:	89 e5                	mov    %esp,%ebp
80104215:	83 ec 08             	sub    $0x8,%esp
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104218:	e8 3a f0 ff ff       	call   80103257 <myproc>
8010421d:	8b 50 18             	mov    0x18(%eax),%edx
80104220:	8b 45 08             	mov    0x8(%ebp),%eax
80104223:	c1 e0 02             	shl    $0x2,%eax
80104226:	03 42 44             	add    0x44(%edx),%eax
80104229:	83 ec 08             	sub    $0x8,%esp
8010422c:	ff 75 0c             	pushl  0xc(%ebp)
8010422f:	83 c0 04             	add    $0x4,%eax
80104232:	50                   	push   %eax
80104233:	e8 5e ff ff ff       	call   80104196 <fetchint>
}
80104238:	c9                   	leave  
80104239:	c3                   	ret    

8010423a <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
8010423a:	55                   	push   %ebp
8010423b:	89 e5                	mov    %esp,%ebp
8010423d:	56                   	push   %esi
8010423e:	53                   	push   %ebx
8010423f:	83 ec 10             	sub    $0x10,%esp
80104242:	8b 5d 10             	mov    0x10(%ebp),%ebx
  int i;
  struct proc *curproc = myproc();
80104245:	e8 0d f0 ff ff       	call   80103257 <myproc>
8010424a:	89 c6                	mov    %eax,%esi
 
  if(argint(n, &i) < 0)
8010424c:	83 ec 08             	sub    $0x8,%esp
8010424f:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104252:	50                   	push   %eax
80104253:	ff 75 08             	pushl  0x8(%ebp)
80104256:	e8 b7 ff ff ff       	call   80104212 <argint>
8010425b:	83 c4 10             	add    $0x10,%esp
8010425e:	85 c0                	test   %eax,%eax
80104260:	78 24                	js     80104286 <argptr+0x4c>
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
80104262:	85 db                	test   %ebx,%ebx
80104264:	78 27                	js     8010428d <argptr+0x53>
80104266:	8b 16                	mov    (%esi),%edx
80104268:	8b 45 f4             	mov    -0xc(%ebp),%eax
8010426b:	39 c2                	cmp    %eax,%edx
8010426d:	76 25                	jbe    80104294 <argptr+0x5a>
8010426f:	01 c3                	add    %eax,%ebx
80104271:	39 da                	cmp    %ebx,%edx
80104273:	72 26                	jb     8010429b <argptr+0x61>
    return -1;
  *pp = (char*)i;
80104275:	8b 55 0c             	mov    0xc(%ebp),%edx
80104278:	89 02                	mov    %eax,(%edx)
  return 0;
8010427a:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010427f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104282:	5b                   	pop    %ebx
80104283:	5e                   	pop    %esi
80104284:	5d                   	pop    %ebp
80104285:	c3                   	ret    
    return -1;
80104286:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010428b:	eb f2                	jmp    8010427f <argptr+0x45>
    return -1;
8010428d:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104292:	eb eb                	jmp    8010427f <argptr+0x45>
80104294:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104299:	eb e4                	jmp    8010427f <argptr+0x45>
8010429b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801042a0:	eb dd                	jmp    8010427f <argptr+0x45>

801042a2 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
801042a2:	55                   	push   %ebp
801042a3:	89 e5                	mov    %esp,%ebp
801042a5:	83 ec 20             	sub    $0x20,%esp
  int addr;
  if(argint(n, &addr) < 0)
801042a8:	8d 45 f4             	lea    -0xc(%ebp),%eax
801042ab:	50                   	push   %eax
801042ac:	ff 75 08             	pushl  0x8(%ebp)
801042af:	e8 5e ff ff ff       	call   80104212 <argint>
801042b4:	83 c4 10             	add    $0x10,%esp
801042b7:	85 c0                	test   %eax,%eax
801042b9:	78 13                	js     801042ce <argstr+0x2c>
    return -1;
  return fetchstr(addr, pp);
801042bb:	83 ec 08             	sub    $0x8,%esp
801042be:	ff 75 0c             	pushl  0xc(%ebp)
801042c1:	ff 75 f4             	pushl  -0xc(%ebp)
801042c4:	e8 09 ff ff ff       	call   801041d2 <fetchstr>
801042c9:	83 c4 10             	add    $0x10,%esp
}
801042cc:	c9                   	leave  
801042cd:	c3                   	ret    
    return -1;
801042ce:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801042d3:	eb f7                	jmp    801042cc <argstr+0x2a>

801042d5 <syscall>:
[SYS_dumppagetable] sys_dumppagetable,
};

void
syscall(void)
{
801042d5:	55                   	push   %ebp
801042d6:	89 e5                	mov    %esp,%ebp
801042d8:	53                   	push   %ebx
801042d9:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
801042dc:	e8 76 ef ff ff       	call   80103257 <myproc>
801042e1:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
801042e3:	8b 40 18             	mov    0x18(%eax),%eax
801042e6:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
801042e9:	8d 50 ff             	lea    -0x1(%eax),%edx
801042ec:	83 fa 1b             	cmp    $0x1b,%edx
801042ef:	77 18                	ja     80104309 <syscall+0x34>
801042f1:	8b 14 85 a0 74 10 80 	mov    -0x7fef8b60(,%eax,4),%edx
801042f8:	85 d2                	test   %edx,%edx
801042fa:	74 0d                	je     80104309 <syscall+0x34>
    curproc->tf->eax = syscalls[num]();
801042fc:	ff d2                	call   *%edx
801042fe:	8b 53 18             	mov    0x18(%ebx),%edx
80104301:	89 42 1c             	mov    %eax,0x1c(%edx)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80104304:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104307:	c9                   	leave  
80104308:	c3                   	ret    
            curproc->pid, curproc->name, num);
80104309:	8d 53 6c             	lea    0x6c(%ebx),%edx
    cprintf("%d %s: unknown sys call %d\n",
8010430c:	50                   	push   %eax
8010430d:	52                   	push   %edx
8010430e:	ff 73 10             	pushl  0x10(%ebx)
80104311:	68 65 74 10 80       	push   $0x80107465
80104316:	e8 f0 c2 ff ff       	call   8010060b <cprintf>
    curproc->tf->eax = -1;
8010431b:	8b 43 18             	mov    0x18(%ebx),%eax
8010431e:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
80104325:	83 c4 10             	add    $0x10,%esp
}
80104328:	eb da                	jmp    80104304 <syscall+0x2f>

8010432a <argfd>:

// Fetch the nth word-sized system call argument as a file descriptor
// and return both the descriptor and the corresponding struct file.
int counter = 0;
static int argfd(int n, int *pfd, struct file **pf)
{
8010432a:	55                   	push   %ebp
8010432b:	89 e5                	mov    %esp,%ebp
8010432d:	56                   	push   %esi
8010432e:	53                   	push   %ebx
8010432f:	83 ec 18             	sub    $0x18,%esp
80104332:	89 d6                	mov    %edx,%esi
80104334:	89 cb                	mov    %ecx,%ebx
  int fd;
  struct file *f;

  if(argint(n, &fd) < 0)
80104336:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104339:	52                   	push   %edx
8010433a:	50                   	push   %eax
8010433b:	e8 d2 fe ff ff       	call   80104212 <argint>
80104340:	83 c4 10             	add    $0x10,%esp
80104343:	85 c0                	test   %eax,%eax
80104345:	78 2e                	js     80104375 <argfd+0x4b>
    return -1;
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80104347:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
8010434b:	77 2f                	ja     8010437c <argfd+0x52>
8010434d:	e8 05 ef ff ff       	call   80103257 <myproc>
80104352:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104355:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104359:	85 c0                	test   %eax,%eax
8010435b:	74 26                	je     80104383 <argfd+0x59>
    return -1;
  if(pfd)
8010435d:	85 f6                	test   %esi,%esi
8010435f:	74 02                	je     80104363 <argfd+0x39>
    *pfd = fd;
80104361:	89 16                	mov    %edx,(%esi)
  if(pf)
80104363:	85 db                	test   %ebx,%ebx
80104365:	74 23                	je     8010438a <argfd+0x60>
    *pf = f;
80104367:	89 03                	mov    %eax,(%ebx)
  return 0;
80104369:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010436e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104371:	5b                   	pop    %ebx
80104372:	5e                   	pop    %esi
80104373:	5d                   	pop    %ebp
80104374:	c3                   	ret    
    return -1;
80104375:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010437a:	eb f2                	jmp    8010436e <argfd+0x44>
    return -1;
8010437c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104381:	eb eb                	jmp    8010436e <argfd+0x44>
80104383:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104388:	eb e4                	jmp    8010436e <argfd+0x44>
  return 0;
8010438a:	b8 00 00 00 00       	mov    $0x0,%eax
8010438f:	eb dd                	jmp    8010436e <argfd+0x44>

80104391 <fdalloc>:

// Allocate a file descriptor for the given file.
// Takes over file reference from caller on success.
static int
fdalloc(struct file *f)
{
80104391:	55                   	push   %ebp
80104392:	89 e5                	mov    %esp,%ebp
80104394:	53                   	push   %ebx
80104395:	83 ec 04             	sub    $0x4,%esp
80104398:	89 c3                	mov    %eax,%ebx
  int fd;
  struct proc *curproc = myproc();
8010439a:	e8 b8 ee ff ff       	call   80103257 <myproc>

  for(fd = 0; fd < NOFILE; fd++){
8010439f:	ba 00 00 00 00       	mov    $0x0,%edx
801043a4:	83 fa 0f             	cmp    $0xf,%edx
801043a7:	7f 18                	jg     801043c1 <fdalloc+0x30>
    if(curproc->ofile[fd] == 0){
801043a9:	83 7c 90 28 00       	cmpl   $0x0,0x28(%eax,%edx,4)
801043ae:	74 05                	je     801043b5 <fdalloc+0x24>
  for(fd = 0; fd < NOFILE; fd++){
801043b0:	83 c2 01             	add    $0x1,%edx
801043b3:	eb ef                	jmp    801043a4 <fdalloc+0x13>
      curproc->ofile[fd] = f;
801043b5:	89 5c 90 28          	mov    %ebx,0x28(%eax,%edx,4)
      return fd;
    }
  }
  return -1;
}
801043b9:	89 d0                	mov    %edx,%eax
801043bb:	83 c4 04             	add    $0x4,%esp
801043be:	5b                   	pop    %ebx
801043bf:	5d                   	pop    %ebp
801043c0:	c3                   	ret    
  return -1;
801043c1:	ba ff ff ff ff       	mov    $0xffffffff,%edx
801043c6:	eb f1                	jmp    801043b9 <fdalloc+0x28>

801043c8 <isdirempty>:
}

// Is the directory dp empty except for "." and ".." ?
static int
isdirempty(struct inode *dp)
{
801043c8:	55                   	push   %ebp
801043c9:	89 e5                	mov    %esp,%ebp
801043cb:	56                   	push   %esi
801043cc:	53                   	push   %ebx
801043cd:	83 ec 10             	sub    $0x10,%esp
801043d0:	89 c3                	mov    %eax,%ebx
  int off;
  struct dirent de;

  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801043d2:	b8 20 00 00 00       	mov    $0x20,%eax
801043d7:	89 c6                	mov    %eax,%esi
801043d9:	39 43 58             	cmp    %eax,0x58(%ebx)
801043dc:	76 2e                	jbe    8010440c <isdirempty+0x44>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801043de:	6a 10                	push   $0x10
801043e0:	50                   	push   %eax
801043e1:	8d 45 e8             	lea    -0x18(%ebp),%eax
801043e4:	50                   	push   %eax
801043e5:	53                   	push   %ebx
801043e6:	e8 76 d3 ff ff       	call   80101761 <readi>
801043eb:	83 c4 10             	add    $0x10,%esp
801043ee:	83 f8 10             	cmp    $0x10,%eax
801043f1:	75 0c                	jne    801043ff <isdirempty+0x37>
      panic("isdirempty: readi");
    if(de.inum != 0)
801043f3:	66 83 7d e8 00       	cmpw   $0x0,-0x18(%ebp)
801043f8:	75 1e                	jne    80104418 <isdirempty+0x50>
  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801043fa:	8d 46 10             	lea    0x10(%esi),%eax
801043fd:	eb d8                	jmp    801043d7 <isdirempty+0xf>
      panic("isdirempty: readi");
801043ff:	83 ec 0c             	sub    $0xc,%esp
80104402:	68 14 75 10 80       	push   $0x80107514
80104407:	e8 3c bf ff ff       	call   80100348 <panic>
      return 0;
  }
  return 1;
8010440c:	b8 01 00 00 00       	mov    $0x1,%eax
}
80104411:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104414:	5b                   	pop    %ebx
80104415:	5e                   	pop    %esi
80104416:	5d                   	pop    %ebp
80104417:	c3                   	ret    
      return 0;
80104418:	b8 00 00 00 00       	mov    $0x0,%eax
8010441d:	eb f2                	jmp    80104411 <isdirempty+0x49>

8010441f <create>:
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
8010441f:	55                   	push   %ebp
80104420:	89 e5                	mov    %esp,%ebp
80104422:	57                   	push   %edi
80104423:	56                   	push   %esi
80104424:	53                   	push   %ebx
80104425:	83 ec 34             	sub    $0x34,%esp
80104428:	89 55 d4             	mov    %edx,-0x2c(%ebp)
8010442b:	89 4d d0             	mov    %ecx,-0x30(%ebp)
8010442e:	8b 7d 08             	mov    0x8(%ebp),%edi
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80104431:	8d 55 da             	lea    -0x26(%ebp),%edx
80104434:	52                   	push   %edx
80104435:	50                   	push   %eax
80104436:	e8 ac d7 ff ff       	call   80101be7 <nameiparent>
8010443b:	89 c6                	mov    %eax,%esi
8010443d:	83 c4 10             	add    $0x10,%esp
80104440:	85 c0                	test   %eax,%eax
80104442:	0f 84 38 01 00 00    	je     80104580 <create+0x161>
    return 0;
  ilock(dp);
80104448:	83 ec 0c             	sub    $0xc,%esp
8010444b:	50                   	push   %eax
8010444c:	e8 1e d1 ff ff       	call   8010156f <ilock>

  if((ip = dirlookup(dp, name, 0)) != 0){
80104451:	83 c4 0c             	add    $0xc,%esp
80104454:	6a 00                	push   $0x0
80104456:	8d 45 da             	lea    -0x26(%ebp),%eax
80104459:	50                   	push   %eax
8010445a:	56                   	push   %esi
8010445b:	e8 3e d5 ff ff       	call   8010199e <dirlookup>
80104460:	89 c3                	mov    %eax,%ebx
80104462:	83 c4 10             	add    $0x10,%esp
80104465:	85 c0                	test   %eax,%eax
80104467:	74 3f                	je     801044a8 <create+0x89>
    iunlockput(dp);
80104469:	83 ec 0c             	sub    $0xc,%esp
8010446c:	56                   	push   %esi
8010446d:	e8 a4 d2 ff ff       	call   80101716 <iunlockput>
    ilock(ip);
80104472:	89 1c 24             	mov    %ebx,(%esp)
80104475:	e8 f5 d0 ff ff       	call   8010156f <ilock>
    if(type == T_FILE && ip->type == T_FILE)
8010447a:	83 c4 10             	add    $0x10,%esp
8010447d:	66 83 7d d4 02       	cmpw   $0x2,-0x2c(%ebp)
80104482:	75 11                	jne    80104495 <create+0x76>
80104484:	66 83 7b 50 02       	cmpw   $0x2,0x50(%ebx)
80104489:	75 0a                	jne    80104495 <create+0x76>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
8010448b:	89 d8                	mov    %ebx,%eax
8010448d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104490:	5b                   	pop    %ebx
80104491:	5e                   	pop    %esi
80104492:	5f                   	pop    %edi
80104493:	5d                   	pop    %ebp
80104494:	c3                   	ret    
    iunlockput(ip);
80104495:	83 ec 0c             	sub    $0xc,%esp
80104498:	53                   	push   %ebx
80104499:	e8 78 d2 ff ff       	call   80101716 <iunlockput>
    return 0;
8010449e:	83 c4 10             	add    $0x10,%esp
801044a1:	bb 00 00 00 00       	mov    $0x0,%ebx
801044a6:	eb e3                	jmp    8010448b <create+0x6c>
  if((ip = ialloc(dp->dev, type)) == 0)
801044a8:	0f bf 45 d4          	movswl -0x2c(%ebp),%eax
801044ac:	83 ec 08             	sub    $0x8,%esp
801044af:	50                   	push   %eax
801044b0:	ff 36                	pushl  (%esi)
801044b2:	e8 b5 ce ff ff       	call   8010136c <ialloc>
801044b7:	89 c3                	mov    %eax,%ebx
801044b9:	83 c4 10             	add    $0x10,%esp
801044bc:	85 c0                	test   %eax,%eax
801044be:	74 55                	je     80104515 <create+0xf6>
  ilock(ip);
801044c0:	83 ec 0c             	sub    $0xc,%esp
801044c3:	50                   	push   %eax
801044c4:	e8 a6 d0 ff ff       	call   8010156f <ilock>
  ip->major = major;
801044c9:	0f b7 45 d0          	movzwl -0x30(%ebp),%eax
801044cd:	66 89 43 52          	mov    %ax,0x52(%ebx)
  ip->minor = minor;
801044d1:	66 89 7b 54          	mov    %di,0x54(%ebx)
  ip->nlink = 1;
801044d5:	66 c7 43 56 01 00    	movw   $0x1,0x56(%ebx)
  iupdate(ip);
801044db:	89 1c 24             	mov    %ebx,(%esp)
801044de:	e8 2b cf ff ff       	call   8010140e <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
801044e3:	83 c4 10             	add    $0x10,%esp
801044e6:	66 83 7d d4 01       	cmpw   $0x1,-0x2c(%ebp)
801044eb:	74 35                	je     80104522 <create+0x103>
  if(dirlink(dp, name, ip->inum) < 0)
801044ed:	83 ec 04             	sub    $0x4,%esp
801044f0:	ff 73 04             	pushl  0x4(%ebx)
801044f3:	8d 45 da             	lea    -0x26(%ebp),%eax
801044f6:	50                   	push   %eax
801044f7:	56                   	push   %esi
801044f8:	e8 21 d6 ff ff       	call   80101b1e <dirlink>
801044fd:	83 c4 10             	add    $0x10,%esp
80104500:	85 c0                	test   %eax,%eax
80104502:	78 6f                	js     80104573 <create+0x154>
  iunlockput(dp);
80104504:	83 ec 0c             	sub    $0xc,%esp
80104507:	56                   	push   %esi
80104508:	e8 09 d2 ff ff       	call   80101716 <iunlockput>
  return ip;
8010450d:	83 c4 10             	add    $0x10,%esp
80104510:	e9 76 ff ff ff       	jmp    8010448b <create+0x6c>
    panic("create: ialloc");
80104515:	83 ec 0c             	sub    $0xc,%esp
80104518:	68 26 75 10 80       	push   $0x80107526
8010451d:	e8 26 be ff ff       	call   80100348 <panic>
    dp->nlink++;  // for ".."
80104522:	0f b7 46 56          	movzwl 0x56(%esi),%eax
80104526:	83 c0 01             	add    $0x1,%eax
80104529:	66 89 46 56          	mov    %ax,0x56(%esi)
    iupdate(dp);
8010452d:	83 ec 0c             	sub    $0xc,%esp
80104530:	56                   	push   %esi
80104531:	e8 d8 ce ff ff       	call   8010140e <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80104536:	83 c4 0c             	add    $0xc,%esp
80104539:	ff 73 04             	pushl  0x4(%ebx)
8010453c:	68 a0 75 10 80       	push   $0x801075a0
80104541:	53                   	push   %ebx
80104542:	e8 d7 d5 ff ff       	call   80101b1e <dirlink>
80104547:	83 c4 10             	add    $0x10,%esp
8010454a:	85 c0                	test   %eax,%eax
8010454c:	78 18                	js     80104566 <create+0x147>
8010454e:	83 ec 04             	sub    $0x4,%esp
80104551:	ff 76 04             	pushl  0x4(%esi)
80104554:	68 9f 75 10 80       	push   $0x8010759f
80104559:	53                   	push   %ebx
8010455a:	e8 bf d5 ff ff       	call   80101b1e <dirlink>
8010455f:	83 c4 10             	add    $0x10,%esp
80104562:	85 c0                	test   %eax,%eax
80104564:	79 87                	jns    801044ed <create+0xce>
      panic("create dots");
80104566:	83 ec 0c             	sub    $0xc,%esp
80104569:	68 35 75 10 80       	push   $0x80107535
8010456e:	e8 d5 bd ff ff       	call   80100348 <panic>
    panic("create: dirlink");
80104573:	83 ec 0c             	sub    $0xc,%esp
80104576:	68 41 75 10 80       	push   $0x80107541
8010457b:	e8 c8 bd ff ff       	call   80100348 <panic>
    return 0;
80104580:	89 c3                	mov    %eax,%ebx
80104582:	e9 04 ff ff ff       	jmp    8010448b <create+0x6c>

80104587 <sys_dup>:
{
80104587:	55                   	push   %ebp
80104588:	89 e5                	mov    %esp,%ebp
8010458a:	53                   	push   %ebx
8010458b:	83 ec 14             	sub    $0x14,%esp
  if(argfd(0, 0, &f) < 0)
8010458e:	8d 4d f4             	lea    -0xc(%ebp),%ecx
80104591:	ba 00 00 00 00       	mov    $0x0,%edx
80104596:	b8 00 00 00 00       	mov    $0x0,%eax
8010459b:	e8 8a fd ff ff       	call   8010432a <argfd>
801045a0:	85 c0                	test   %eax,%eax
801045a2:	78 23                	js     801045c7 <sys_dup+0x40>
  if((fd=fdalloc(f)) < 0)
801045a4:	8b 45 f4             	mov    -0xc(%ebp),%eax
801045a7:	e8 e5 fd ff ff       	call   80104391 <fdalloc>
801045ac:	89 c3                	mov    %eax,%ebx
801045ae:	85 c0                	test   %eax,%eax
801045b0:	78 1c                	js     801045ce <sys_dup+0x47>
  filedup(f);
801045b2:	83 ec 0c             	sub    $0xc,%esp
801045b5:	ff 75 f4             	pushl  -0xc(%ebp)
801045b8:	e8 d1 c6 ff ff       	call   80100c8e <filedup>
  return fd;
801045bd:	83 c4 10             	add    $0x10,%esp
}
801045c0:	89 d8                	mov    %ebx,%eax
801045c2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801045c5:	c9                   	leave  
801045c6:	c3                   	ret    
    return -1;
801045c7:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801045cc:	eb f2                	jmp    801045c0 <sys_dup+0x39>
    return -1;
801045ce:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801045d3:	eb eb                	jmp    801045c0 <sys_dup+0x39>

801045d5 <sys_read>:
{
801045d5:	55                   	push   %ebp
801045d6:	89 e5                	mov    %esp,%ebp
801045d8:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
801045db:	8d 4d f4             	lea    -0xc(%ebp),%ecx
801045de:	ba 00 00 00 00       	mov    $0x0,%edx
801045e3:	b8 00 00 00 00       	mov    $0x0,%eax
801045e8:	e8 3d fd ff ff       	call   8010432a <argfd>
801045ed:	85 c0                	test   %eax,%eax
801045ef:	78 43                	js     80104634 <sys_read+0x5f>
801045f1:	83 ec 08             	sub    $0x8,%esp
801045f4:	8d 45 f0             	lea    -0x10(%ebp),%eax
801045f7:	50                   	push   %eax
801045f8:	6a 02                	push   $0x2
801045fa:	e8 13 fc ff ff       	call   80104212 <argint>
801045ff:	83 c4 10             	add    $0x10,%esp
80104602:	85 c0                	test   %eax,%eax
80104604:	78 35                	js     8010463b <sys_read+0x66>
80104606:	83 ec 04             	sub    $0x4,%esp
80104609:	ff 75 f0             	pushl  -0x10(%ebp)
8010460c:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010460f:	50                   	push   %eax
80104610:	6a 01                	push   $0x1
80104612:	e8 23 fc ff ff       	call   8010423a <argptr>
80104617:	83 c4 10             	add    $0x10,%esp
8010461a:	85 c0                	test   %eax,%eax
8010461c:	78 24                	js     80104642 <sys_read+0x6d>
  return fileread(f, p, n);
8010461e:	83 ec 04             	sub    $0x4,%esp
80104621:	ff 75 f0             	pushl  -0x10(%ebp)
80104624:	ff 75 ec             	pushl  -0x14(%ebp)
80104627:	ff 75 f4             	pushl  -0xc(%ebp)
8010462a:	e8 a8 c7 ff ff       	call   80100dd7 <fileread>
8010462f:	83 c4 10             	add    $0x10,%esp
}
80104632:	c9                   	leave  
80104633:	c3                   	ret    
    return -1;
80104634:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104639:	eb f7                	jmp    80104632 <sys_read+0x5d>
8010463b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104640:	eb f0                	jmp    80104632 <sys_read+0x5d>
80104642:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104647:	eb e9                	jmp    80104632 <sys_read+0x5d>

80104649 <sys_write>:
{
80104649:	55                   	push   %ebp
8010464a:	89 e5                	mov    %esp,%ebp
8010464c:	83 ec 18             	sub    $0x18,%esp
  counter += 1;
8010464f:	83 05 bc a5 10 80 01 	addl   $0x1,0x8010a5bc
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104656:	8d 4d f4             	lea    -0xc(%ebp),%ecx
80104659:	ba 00 00 00 00       	mov    $0x0,%edx
8010465e:	b8 00 00 00 00       	mov    $0x0,%eax
80104663:	e8 c2 fc ff ff       	call   8010432a <argfd>
80104668:	85 c0                	test   %eax,%eax
8010466a:	78 43                	js     801046af <sys_write+0x66>
8010466c:	83 ec 08             	sub    $0x8,%esp
8010466f:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104672:	50                   	push   %eax
80104673:	6a 02                	push   $0x2
80104675:	e8 98 fb ff ff       	call   80104212 <argint>
8010467a:	83 c4 10             	add    $0x10,%esp
8010467d:	85 c0                	test   %eax,%eax
8010467f:	78 35                	js     801046b6 <sys_write+0x6d>
80104681:	83 ec 04             	sub    $0x4,%esp
80104684:	ff 75 f0             	pushl  -0x10(%ebp)
80104687:	8d 45 ec             	lea    -0x14(%ebp),%eax
8010468a:	50                   	push   %eax
8010468b:	6a 01                	push   $0x1
8010468d:	e8 a8 fb ff ff       	call   8010423a <argptr>
80104692:	83 c4 10             	add    $0x10,%esp
80104695:	85 c0                	test   %eax,%eax
80104697:	78 24                	js     801046bd <sys_write+0x74>
  return filewrite(f, p, n);
80104699:	83 ec 04             	sub    $0x4,%esp
8010469c:	ff 75 f0             	pushl  -0x10(%ebp)
8010469f:	ff 75 ec             	pushl  -0x14(%ebp)
801046a2:	ff 75 f4             	pushl  -0xc(%ebp)
801046a5:	e8 b2 c7 ff ff       	call   80100e5c <filewrite>
801046aa:	83 c4 10             	add    $0x10,%esp
}
801046ad:	c9                   	leave  
801046ae:	c3                   	ret    
    return -1;
801046af:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801046b4:	eb f7                	jmp    801046ad <sys_write+0x64>
801046b6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801046bb:	eb f0                	jmp    801046ad <sys_write+0x64>
801046bd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801046c2:	eb e9                	jmp    801046ad <sys_write+0x64>

801046c4 <sys_writecount>:
{
801046c4:	55                   	push   %ebp
801046c5:	89 e5                	mov    %esp,%ebp
}
801046c7:	a1 bc a5 10 80       	mov    0x8010a5bc,%eax
801046cc:	5d                   	pop    %ebp
801046cd:	c3                   	ret    

801046ce <sys_setwritecount>:
{
801046ce:	55                   	push   %ebp
801046cf:	89 e5                	mov    %esp,%ebp
801046d1:	83 ec 20             	sub    $0x20,%esp
argint(0, &pid);
801046d4:	8d 45 f4             	lea    -0xc(%ebp),%eax
801046d7:	50                   	push   %eax
801046d8:	6a 00                	push   $0x0
801046da:	e8 33 fb ff ff       	call   80104212 <argint>
counter = pid;
801046df:	8b 45 f4             	mov    -0xc(%ebp),%eax
801046e2:	a3 bc a5 10 80       	mov    %eax,0x8010a5bc
}
801046e7:	83 c4 10             	add    $0x10,%esp
801046ea:	c9                   	leave  
801046eb:	c3                   	ret    

801046ec <sys_close>:
{
801046ec:	55                   	push   %ebp
801046ed:	89 e5                	mov    %esp,%ebp
801046ef:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, &fd, &f) < 0)
801046f2:	8d 4d f0             	lea    -0x10(%ebp),%ecx
801046f5:	8d 55 f4             	lea    -0xc(%ebp),%edx
801046f8:	b8 00 00 00 00       	mov    $0x0,%eax
801046fd:	e8 28 fc ff ff       	call   8010432a <argfd>
80104702:	85 c0                	test   %eax,%eax
80104704:	78 25                	js     8010472b <sys_close+0x3f>
  myproc()->ofile[fd] = 0;
80104706:	e8 4c eb ff ff       	call   80103257 <myproc>
8010470b:	8b 55 f4             	mov    -0xc(%ebp),%edx
8010470e:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104715:	00 
  fileclose(f);
80104716:	83 ec 0c             	sub    $0xc,%esp
80104719:	ff 75 f0             	pushl  -0x10(%ebp)
8010471c:	e8 b2 c5 ff ff       	call   80100cd3 <fileclose>
  return 0;
80104721:	83 c4 10             	add    $0x10,%esp
80104724:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104729:	c9                   	leave  
8010472a:	c3                   	ret    
    return -1;
8010472b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104730:	eb f7                	jmp    80104729 <sys_close+0x3d>

80104732 <sys_fstat>:
{
80104732:	55                   	push   %ebp
80104733:	89 e5                	mov    %esp,%ebp
80104735:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104738:	8d 4d f4             	lea    -0xc(%ebp),%ecx
8010473b:	ba 00 00 00 00       	mov    $0x0,%edx
80104740:	b8 00 00 00 00       	mov    $0x0,%eax
80104745:	e8 e0 fb ff ff       	call   8010432a <argfd>
8010474a:	85 c0                	test   %eax,%eax
8010474c:	78 2a                	js     80104778 <sys_fstat+0x46>
8010474e:	83 ec 04             	sub    $0x4,%esp
80104751:	6a 14                	push   $0x14
80104753:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104756:	50                   	push   %eax
80104757:	6a 01                	push   $0x1
80104759:	e8 dc fa ff ff       	call   8010423a <argptr>
8010475e:	83 c4 10             	add    $0x10,%esp
80104761:	85 c0                	test   %eax,%eax
80104763:	78 1a                	js     8010477f <sys_fstat+0x4d>
  return filestat(f, st);
80104765:	83 ec 08             	sub    $0x8,%esp
80104768:	ff 75 f0             	pushl  -0x10(%ebp)
8010476b:	ff 75 f4             	pushl  -0xc(%ebp)
8010476e:	e8 1d c6 ff ff       	call   80100d90 <filestat>
80104773:	83 c4 10             	add    $0x10,%esp
}
80104776:	c9                   	leave  
80104777:	c3                   	ret    
    return -1;
80104778:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010477d:	eb f7                	jmp    80104776 <sys_fstat+0x44>
8010477f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104784:	eb f0                	jmp    80104776 <sys_fstat+0x44>

80104786 <sys_link>:
{
80104786:	55                   	push   %ebp
80104787:	89 e5                	mov    %esp,%ebp
80104789:	56                   	push   %esi
8010478a:	53                   	push   %ebx
8010478b:	83 ec 28             	sub    $0x28,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
8010478e:	8d 45 e0             	lea    -0x20(%ebp),%eax
80104791:	50                   	push   %eax
80104792:	6a 00                	push   $0x0
80104794:	e8 09 fb ff ff       	call   801042a2 <argstr>
80104799:	83 c4 10             	add    $0x10,%esp
8010479c:	85 c0                	test   %eax,%eax
8010479e:	0f 88 32 01 00 00    	js     801048d6 <sys_link+0x150>
801047a4:	83 ec 08             	sub    $0x8,%esp
801047a7:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801047aa:	50                   	push   %eax
801047ab:	6a 01                	push   $0x1
801047ad:	e8 f0 fa ff ff       	call   801042a2 <argstr>
801047b2:	83 c4 10             	add    $0x10,%esp
801047b5:	85 c0                	test   %eax,%eax
801047b7:	0f 88 20 01 00 00    	js     801048dd <sys_link+0x157>
  begin_op();
801047bd:	e8 10 e0 ff ff       	call   801027d2 <begin_op>
  if((ip = namei(old)) == 0){
801047c2:	83 ec 0c             	sub    $0xc,%esp
801047c5:	ff 75 e0             	pushl  -0x20(%ebp)
801047c8:	e8 02 d4 ff ff       	call   80101bcf <namei>
801047cd:	89 c3                	mov    %eax,%ebx
801047cf:	83 c4 10             	add    $0x10,%esp
801047d2:	85 c0                	test   %eax,%eax
801047d4:	0f 84 99 00 00 00    	je     80104873 <sys_link+0xed>
  ilock(ip);
801047da:	83 ec 0c             	sub    $0xc,%esp
801047dd:	50                   	push   %eax
801047de:	e8 8c cd ff ff       	call   8010156f <ilock>
  if(ip->type == T_DIR){
801047e3:	83 c4 10             	add    $0x10,%esp
801047e6:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801047eb:	0f 84 8e 00 00 00    	je     8010487f <sys_link+0xf9>
  ip->nlink++;
801047f1:	0f b7 43 56          	movzwl 0x56(%ebx),%eax
801047f5:	83 c0 01             	add    $0x1,%eax
801047f8:	66 89 43 56          	mov    %ax,0x56(%ebx)
  iupdate(ip);
801047fc:	83 ec 0c             	sub    $0xc,%esp
801047ff:	53                   	push   %ebx
80104800:	e8 09 cc ff ff       	call   8010140e <iupdate>
  iunlock(ip);
80104805:	89 1c 24             	mov    %ebx,(%esp)
80104808:	e8 24 ce ff ff       	call   80101631 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
8010480d:	83 c4 08             	add    $0x8,%esp
80104810:	8d 45 ea             	lea    -0x16(%ebp),%eax
80104813:	50                   	push   %eax
80104814:	ff 75 e4             	pushl  -0x1c(%ebp)
80104817:	e8 cb d3 ff ff       	call   80101be7 <nameiparent>
8010481c:	89 c6                	mov    %eax,%esi
8010481e:	83 c4 10             	add    $0x10,%esp
80104821:	85 c0                	test   %eax,%eax
80104823:	74 7e                	je     801048a3 <sys_link+0x11d>
  ilock(dp);
80104825:	83 ec 0c             	sub    $0xc,%esp
80104828:	50                   	push   %eax
80104829:	e8 41 cd ff ff       	call   8010156f <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
8010482e:	83 c4 10             	add    $0x10,%esp
80104831:	8b 03                	mov    (%ebx),%eax
80104833:	39 06                	cmp    %eax,(%esi)
80104835:	75 60                	jne    80104897 <sys_link+0x111>
80104837:	83 ec 04             	sub    $0x4,%esp
8010483a:	ff 73 04             	pushl  0x4(%ebx)
8010483d:	8d 45 ea             	lea    -0x16(%ebp),%eax
80104840:	50                   	push   %eax
80104841:	56                   	push   %esi
80104842:	e8 d7 d2 ff ff       	call   80101b1e <dirlink>
80104847:	83 c4 10             	add    $0x10,%esp
8010484a:	85 c0                	test   %eax,%eax
8010484c:	78 49                	js     80104897 <sys_link+0x111>
  iunlockput(dp);
8010484e:	83 ec 0c             	sub    $0xc,%esp
80104851:	56                   	push   %esi
80104852:	e8 bf ce ff ff       	call   80101716 <iunlockput>
  iput(ip);
80104857:	89 1c 24             	mov    %ebx,(%esp)
8010485a:	e8 17 ce ff ff       	call   80101676 <iput>
  end_op();
8010485f:	e8 e8 df ff ff       	call   8010284c <end_op>
  return 0;
80104864:	83 c4 10             	add    $0x10,%esp
80104867:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010486c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010486f:	5b                   	pop    %ebx
80104870:	5e                   	pop    %esi
80104871:	5d                   	pop    %ebp
80104872:	c3                   	ret    
    end_op();
80104873:	e8 d4 df ff ff       	call   8010284c <end_op>
    return -1;
80104878:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010487d:	eb ed                	jmp    8010486c <sys_link+0xe6>
    iunlockput(ip);
8010487f:	83 ec 0c             	sub    $0xc,%esp
80104882:	53                   	push   %ebx
80104883:	e8 8e ce ff ff       	call   80101716 <iunlockput>
    end_op();
80104888:	e8 bf df ff ff       	call   8010284c <end_op>
    return -1;
8010488d:	83 c4 10             	add    $0x10,%esp
80104890:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104895:	eb d5                	jmp    8010486c <sys_link+0xe6>
    iunlockput(dp);
80104897:	83 ec 0c             	sub    $0xc,%esp
8010489a:	56                   	push   %esi
8010489b:	e8 76 ce ff ff       	call   80101716 <iunlockput>
    goto bad;
801048a0:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
801048a3:	83 ec 0c             	sub    $0xc,%esp
801048a6:	53                   	push   %ebx
801048a7:	e8 c3 cc ff ff       	call   8010156f <ilock>
  ip->nlink--;
801048ac:	0f b7 43 56          	movzwl 0x56(%ebx),%eax
801048b0:	83 e8 01             	sub    $0x1,%eax
801048b3:	66 89 43 56          	mov    %ax,0x56(%ebx)
  iupdate(ip);
801048b7:	89 1c 24             	mov    %ebx,(%esp)
801048ba:	e8 4f cb ff ff       	call   8010140e <iupdate>
  iunlockput(ip);
801048bf:	89 1c 24             	mov    %ebx,(%esp)
801048c2:	e8 4f ce ff ff       	call   80101716 <iunlockput>
  end_op();
801048c7:	e8 80 df ff ff       	call   8010284c <end_op>
  return -1;
801048cc:	83 c4 10             	add    $0x10,%esp
801048cf:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801048d4:	eb 96                	jmp    8010486c <sys_link+0xe6>
    return -1;
801048d6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801048db:	eb 8f                	jmp    8010486c <sys_link+0xe6>
801048dd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801048e2:	eb 88                	jmp    8010486c <sys_link+0xe6>

801048e4 <sys_unlink>:
{
801048e4:	55                   	push   %ebp
801048e5:	89 e5                	mov    %esp,%ebp
801048e7:	57                   	push   %edi
801048e8:	56                   	push   %esi
801048e9:	53                   	push   %ebx
801048ea:	83 ec 44             	sub    $0x44,%esp
  if(argstr(0, &path) < 0)
801048ed:	8d 45 c4             	lea    -0x3c(%ebp),%eax
801048f0:	50                   	push   %eax
801048f1:	6a 00                	push   $0x0
801048f3:	e8 aa f9 ff ff       	call   801042a2 <argstr>
801048f8:	83 c4 10             	add    $0x10,%esp
801048fb:	85 c0                	test   %eax,%eax
801048fd:	0f 88 83 01 00 00    	js     80104a86 <sys_unlink+0x1a2>
  begin_op();
80104903:	e8 ca de ff ff       	call   801027d2 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80104908:	83 ec 08             	sub    $0x8,%esp
8010490b:	8d 45 ca             	lea    -0x36(%ebp),%eax
8010490e:	50                   	push   %eax
8010490f:	ff 75 c4             	pushl  -0x3c(%ebp)
80104912:	e8 d0 d2 ff ff       	call   80101be7 <nameiparent>
80104917:	89 c6                	mov    %eax,%esi
80104919:	83 c4 10             	add    $0x10,%esp
8010491c:	85 c0                	test   %eax,%eax
8010491e:	0f 84 ed 00 00 00    	je     80104a11 <sys_unlink+0x12d>
  ilock(dp);
80104924:	83 ec 0c             	sub    $0xc,%esp
80104927:	50                   	push   %eax
80104928:	e8 42 cc ff ff       	call   8010156f <ilock>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
8010492d:	83 c4 08             	add    $0x8,%esp
80104930:	68 a0 75 10 80       	push   $0x801075a0
80104935:	8d 45 ca             	lea    -0x36(%ebp),%eax
80104938:	50                   	push   %eax
80104939:	e8 4b d0 ff ff       	call   80101989 <namecmp>
8010493e:	83 c4 10             	add    $0x10,%esp
80104941:	85 c0                	test   %eax,%eax
80104943:	0f 84 fc 00 00 00    	je     80104a45 <sys_unlink+0x161>
80104949:	83 ec 08             	sub    $0x8,%esp
8010494c:	68 9f 75 10 80       	push   $0x8010759f
80104951:	8d 45 ca             	lea    -0x36(%ebp),%eax
80104954:	50                   	push   %eax
80104955:	e8 2f d0 ff ff       	call   80101989 <namecmp>
8010495a:	83 c4 10             	add    $0x10,%esp
8010495d:	85 c0                	test   %eax,%eax
8010495f:	0f 84 e0 00 00 00    	je     80104a45 <sys_unlink+0x161>
  if((ip = dirlookup(dp, name, &off)) == 0)
80104965:	83 ec 04             	sub    $0x4,%esp
80104968:	8d 45 c0             	lea    -0x40(%ebp),%eax
8010496b:	50                   	push   %eax
8010496c:	8d 45 ca             	lea    -0x36(%ebp),%eax
8010496f:	50                   	push   %eax
80104970:	56                   	push   %esi
80104971:	e8 28 d0 ff ff       	call   8010199e <dirlookup>
80104976:	89 c3                	mov    %eax,%ebx
80104978:	83 c4 10             	add    $0x10,%esp
8010497b:	85 c0                	test   %eax,%eax
8010497d:	0f 84 c2 00 00 00    	je     80104a45 <sys_unlink+0x161>
  ilock(ip);
80104983:	83 ec 0c             	sub    $0xc,%esp
80104986:	50                   	push   %eax
80104987:	e8 e3 cb ff ff       	call   8010156f <ilock>
  if(ip->nlink < 1)
8010498c:	83 c4 10             	add    $0x10,%esp
8010498f:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80104994:	0f 8e 83 00 00 00    	jle    80104a1d <sys_unlink+0x139>
  if(ip->type == T_DIR && !isdirempty(ip)){
8010499a:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010499f:	0f 84 85 00 00 00    	je     80104a2a <sys_unlink+0x146>
  memset(&de, 0, sizeof(de));
801049a5:	83 ec 04             	sub    $0x4,%esp
801049a8:	6a 10                	push   $0x10
801049aa:	6a 00                	push   $0x0
801049ac:	8d 7d d8             	lea    -0x28(%ebp),%edi
801049af:	57                   	push   %edi
801049b0:	e8 12 f6 ff ff       	call   80103fc7 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801049b5:	6a 10                	push   $0x10
801049b7:	ff 75 c0             	pushl  -0x40(%ebp)
801049ba:	57                   	push   %edi
801049bb:	56                   	push   %esi
801049bc:	e8 9d ce ff ff       	call   8010185e <writei>
801049c1:	83 c4 20             	add    $0x20,%esp
801049c4:	83 f8 10             	cmp    $0x10,%eax
801049c7:	0f 85 90 00 00 00    	jne    80104a5d <sys_unlink+0x179>
  if(ip->type == T_DIR){
801049cd:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
801049d2:	0f 84 92 00 00 00    	je     80104a6a <sys_unlink+0x186>
  iunlockput(dp);
801049d8:	83 ec 0c             	sub    $0xc,%esp
801049db:	56                   	push   %esi
801049dc:	e8 35 cd ff ff       	call   80101716 <iunlockput>
  ip->nlink--;
801049e1:	0f b7 43 56          	movzwl 0x56(%ebx),%eax
801049e5:	83 e8 01             	sub    $0x1,%eax
801049e8:	66 89 43 56          	mov    %ax,0x56(%ebx)
  iupdate(ip);
801049ec:	89 1c 24             	mov    %ebx,(%esp)
801049ef:	e8 1a ca ff ff       	call   8010140e <iupdate>
  iunlockput(ip);
801049f4:	89 1c 24             	mov    %ebx,(%esp)
801049f7:	e8 1a cd ff ff       	call   80101716 <iunlockput>
  end_op();
801049fc:	e8 4b de ff ff       	call   8010284c <end_op>
  return 0;
80104a01:	83 c4 10             	add    $0x10,%esp
80104a04:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104a09:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104a0c:	5b                   	pop    %ebx
80104a0d:	5e                   	pop    %esi
80104a0e:	5f                   	pop    %edi
80104a0f:	5d                   	pop    %ebp
80104a10:	c3                   	ret    
    end_op();
80104a11:	e8 36 de ff ff       	call   8010284c <end_op>
    return -1;
80104a16:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a1b:	eb ec                	jmp    80104a09 <sys_unlink+0x125>
    panic("unlink: nlink < 1");
80104a1d:	83 ec 0c             	sub    $0xc,%esp
80104a20:	68 51 75 10 80       	push   $0x80107551
80104a25:	e8 1e b9 ff ff       	call   80100348 <panic>
  if(ip->type == T_DIR && !isdirempty(ip)){
80104a2a:	89 d8                	mov    %ebx,%eax
80104a2c:	e8 97 f9 ff ff       	call   801043c8 <isdirempty>
80104a31:	85 c0                	test   %eax,%eax
80104a33:	0f 85 6c ff ff ff    	jne    801049a5 <sys_unlink+0xc1>
    iunlockput(ip);
80104a39:	83 ec 0c             	sub    $0xc,%esp
80104a3c:	53                   	push   %ebx
80104a3d:	e8 d4 cc ff ff       	call   80101716 <iunlockput>
    goto bad;
80104a42:	83 c4 10             	add    $0x10,%esp
  iunlockput(dp);
80104a45:	83 ec 0c             	sub    $0xc,%esp
80104a48:	56                   	push   %esi
80104a49:	e8 c8 cc ff ff       	call   80101716 <iunlockput>
  end_op();
80104a4e:	e8 f9 dd ff ff       	call   8010284c <end_op>
  return -1;
80104a53:	83 c4 10             	add    $0x10,%esp
80104a56:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a5b:	eb ac                	jmp    80104a09 <sys_unlink+0x125>
    panic("unlink: writei");
80104a5d:	83 ec 0c             	sub    $0xc,%esp
80104a60:	68 63 75 10 80       	push   $0x80107563
80104a65:	e8 de b8 ff ff       	call   80100348 <panic>
    dp->nlink--;
80104a6a:	0f b7 46 56          	movzwl 0x56(%esi),%eax
80104a6e:	83 e8 01             	sub    $0x1,%eax
80104a71:	66 89 46 56          	mov    %ax,0x56(%esi)
    iupdate(dp);
80104a75:	83 ec 0c             	sub    $0xc,%esp
80104a78:	56                   	push   %esi
80104a79:	e8 90 c9 ff ff       	call   8010140e <iupdate>
80104a7e:	83 c4 10             	add    $0x10,%esp
80104a81:	e9 52 ff ff ff       	jmp    801049d8 <sys_unlink+0xf4>
    return -1;
80104a86:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104a8b:	e9 79 ff ff ff       	jmp    80104a09 <sys_unlink+0x125>

80104a90 <sys_open>:

int
sys_open(void)
{
80104a90:	55                   	push   %ebp
80104a91:	89 e5                	mov    %esp,%ebp
80104a93:	57                   	push   %edi
80104a94:	56                   	push   %esi
80104a95:	53                   	push   %ebx
80104a96:	83 ec 24             	sub    $0x24,%esp
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80104a99:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80104a9c:	50                   	push   %eax
80104a9d:	6a 00                	push   $0x0
80104a9f:	e8 fe f7 ff ff       	call   801042a2 <argstr>
80104aa4:	83 c4 10             	add    $0x10,%esp
80104aa7:	85 c0                	test   %eax,%eax
80104aa9:	0f 88 30 01 00 00    	js     80104bdf <sys_open+0x14f>
80104aaf:	83 ec 08             	sub    $0x8,%esp
80104ab2:	8d 45 e0             	lea    -0x20(%ebp),%eax
80104ab5:	50                   	push   %eax
80104ab6:	6a 01                	push   $0x1
80104ab8:	e8 55 f7 ff ff       	call   80104212 <argint>
80104abd:	83 c4 10             	add    $0x10,%esp
80104ac0:	85 c0                	test   %eax,%eax
80104ac2:	0f 88 21 01 00 00    	js     80104be9 <sys_open+0x159>
    return -1;

  begin_op();
80104ac8:	e8 05 dd ff ff       	call   801027d2 <begin_op>

  if(omode & O_CREATE){
80104acd:	f6 45 e1 02          	testb  $0x2,-0x1f(%ebp)
80104ad1:	0f 84 84 00 00 00    	je     80104b5b <sys_open+0xcb>
    ip = create(path, T_FILE, 0, 0);
80104ad7:	83 ec 0c             	sub    $0xc,%esp
80104ada:	6a 00                	push   $0x0
80104adc:	b9 00 00 00 00       	mov    $0x0,%ecx
80104ae1:	ba 02 00 00 00       	mov    $0x2,%edx
80104ae6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80104ae9:	e8 31 f9 ff ff       	call   8010441f <create>
80104aee:	89 c6                	mov    %eax,%esi
    if(ip == 0){
80104af0:	83 c4 10             	add    $0x10,%esp
80104af3:	85 c0                	test   %eax,%eax
80104af5:	74 58                	je     80104b4f <sys_open+0xbf>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
80104af7:	e8 31 c1 ff ff       	call   80100c2d <filealloc>
80104afc:	89 c3                	mov    %eax,%ebx
80104afe:	85 c0                	test   %eax,%eax
80104b00:	0f 84 ae 00 00 00    	je     80104bb4 <sys_open+0x124>
80104b06:	e8 86 f8 ff ff       	call   80104391 <fdalloc>
80104b0b:	89 c7                	mov    %eax,%edi
80104b0d:	85 c0                	test   %eax,%eax
80104b0f:	0f 88 9f 00 00 00    	js     80104bb4 <sys_open+0x124>
      fileclose(f);
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80104b15:	83 ec 0c             	sub    $0xc,%esp
80104b18:	56                   	push   %esi
80104b19:	e8 13 cb ff ff       	call   80101631 <iunlock>
  end_op();
80104b1e:	e8 29 dd ff ff       	call   8010284c <end_op>

  f->type = FD_INODE;
80104b23:	c7 03 02 00 00 00    	movl   $0x2,(%ebx)
  f->ip = ip;
80104b29:	89 73 10             	mov    %esi,0x10(%ebx)
  f->off = 0;
80104b2c:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
  f->readable = !(omode & O_WRONLY);
80104b33:	8b 45 e0             	mov    -0x20(%ebp),%eax
80104b36:	83 c4 10             	add    $0x10,%esp
80104b39:	a8 01                	test   $0x1,%al
80104b3b:	0f 94 43 08          	sete   0x8(%ebx)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80104b3f:	a8 03                	test   $0x3,%al
80104b41:	0f 95 43 09          	setne  0x9(%ebx)
  return fd;
}
80104b45:	89 f8                	mov    %edi,%eax
80104b47:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104b4a:	5b                   	pop    %ebx
80104b4b:	5e                   	pop    %esi
80104b4c:	5f                   	pop    %edi
80104b4d:	5d                   	pop    %ebp
80104b4e:	c3                   	ret    
      end_op();
80104b4f:	e8 f8 dc ff ff       	call   8010284c <end_op>
      return -1;
80104b54:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104b59:	eb ea                	jmp    80104b45 <sys_open+0xb5>
    if((ip = namei(path)) == 0){
80104b5b:	83 ec 0c             	sub    $0xc,%esp
80104b5e:	ff 75 e4             	pushl  -0x1c(%ebp)
80104b61:	e8 69 d0 ff ff       	call   80101bcf <namei>
80104b66:	89 c6                	mov    %eax,%esi
80104b68:	83 c4 10             	add    $0x10,%esp
80104b6b:	85 c0                	test   %eax,%eax
80104b6d:	74 39                	je     80104ba8 <sys_open+0x118>
    ilock(ip);
80104b6f:	83 ec 0c             	sub    $0xc,%esp
80104b72:	50                   	push   %eax
80104b73:	e8 f7 c9 ff ff       	call   8010156f <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
80104b78:	83 c4 10             	add    $0x10,%esp
80104b7b:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80104b80:	0f 85 71 ff ff ff    	jne    80104af7 <sys_open+0x67>
80104b86:	83 7d e0 00          	cmpl   $0x0,-0x20(%ebp)
80104b8a:	0f 84 67 ff ff ff    	je     80104af7 <sys_open+0x67>
      iunlockput(ip);
80104b90:	83 ec 0c             	sub    $0xc,%esp
80104b93:	56                   	push   %esi
80104b94:	e8 7d cb ff ff       	call   80101716 <iunlockput>
      end_op();
80104b99:	e8 ae dc ff ff       	call   8010284c <end_op>
      return -1;
80104b9e:	83 c4 10             	add    $0x10,%esp
80104ba1:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104ba6:	eb 9d                	jmp    80104b45 <sys_open+0xb5>
      end_op();
80104ba8:	e8 9f dc ff ff       	call   8010284c <end_op>
      return -1;
80104bad:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104bb2:	eb 91                	jmp    80104b45 <sys_open+0xb5>
    if(f)
80104bb4:	85 db                	test   %ebx,%ebx
80104bb6:	74 0c                	je     80104bc4 <sys_open+0x134>
      fileclose(f);
80104bb8:	83 ec 0c             	sub    $0xc,%esp
80104bbb:	53                   	push   %ebx
80104bbc:	e8 12 c1 ff ff       	call   80100cd3 <fileclose>
80104bc1:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
80104bc4:	83 ec 0c             	sub    $0xc,%esp
80104bc7:	56                   	push   %esi
80104bc8:	e8 49 cb ff ff       	call   80101716 <iunlockput>
    end_op();
80104bcd:	e8 7a dc ff ff       	call   8010284c <end_op>
    return -1;
80104bd2:	83 c4 10             	add    $0x10,%esp
80104bd5:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104bda:	e9 66 ff ff ff       	jmp    80104b45 <sys_open+0xb5>
    return -1;
80104bdf:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104be4:	e9 5c ff ff ff       	jmp    80104b45 <sys_open+0xb5>
80104be9:	bf ff ff ff ff       	mov    $0xffffffff,%edi
80104bee:	e9 52 ff ff ff       	jmp    80104b45 <sys_open+0xb5>

80104bf3 <sys_mkdir>:

int
sys_mkdir(void)
{
80104bf3:	55                   	push   %ebp
80104bf4:	89 e5                	mov    %esp,%ebp
80104bf6:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
80104bf9:	e8 d4 db ff ff       	call   801027d2 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
80104bfe:	83 ec 08             	sub    $0x8,%esp
80104c01:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104c04:	50                   	push   %eax
80104c05:	6a 00                	push   $0x0
80104c07:	e8 96 f6 ff ff       	call   801042a2 <argstr>
80104c0c:	83 c4 10             	add    $0x10,%esp
80104c0f:	85 c0                	test   %eax,%eax
80104c11:	78 36                	js     80104c49 <sys_mkdir+0x56>
80104c13:	83 ec 0c             	sub    $0xc,%esp
80104c16:	6a 00                	push   $0x0
80104c18:	b9 00 00 00 00       	mov    $0x0,%ecx
80104c1d:	ba 01 00 00 00       	mov    $0x1,%edx
80104c22:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104c25:	e8 f5 f7 ff ff       	call   8010441f <create>
80104c2a:	83 c4 10             	add    $0x10,%esp
80104c2d:	85 c0                	test   %eax,%eax
80104c2f:	74 18                	je     80104c49 <sys_mkdir+0x56>
    end_op();
    return -1;
  }
  iunlockput(ip);
80104c31:	83 ec 0c             	sub    $0xc,%esp
80104c34:	50                   	push   %eax
80104c35:	e8 dc ca ff ff       	call   80101716 <iunlockput>
  end_op();
80104c3a:	e8 0d dc ff ff       	call   8010284c <end_op>
  return 0;
80104c3f:	83 c4 10             	add    $0x10,%esp
80104c42:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104c47:	c9                   	leave  
80104c48:	c3                   	ret    
    end_op();
80104c49:	e8 fe db ff ff       	call   8010284c <end_op>
    return -1;
80104c4e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c53:	eb f2                	jmp    80104c47 <sys_mkdir+0x54>

80104c55 <sys_mknod>:

int
sys_mknod(void)
{
80104c55:	55                   	push   %ebp
80104c56:	89 e5                	mov    %esp,%ebp
80104c58:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80104c5b:	e8 72 db ff ff       	call   801027d2 <begin_op>
  if((argstr(0, &path)) < 0 ||
80104c60:	83 ec 08             	sub    $0x8,%esp
80104c63:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104c66:	50                   	push   %eax
80104c67:	6a 00                	push   $0x0
80104c69:	e8 34 f6 ff ff       	call   801042a2 <argstr>
80104c6e:	83 c4 10             	add    $0x10,%esp
80104c71:	85 c0                	test   %eax,%eax
80104c73:	78 62                	js     80104cd7 <sys_mknod+0x82>
     argint(1, &major) < 0 ||
80104c75:	83 ec 08             	sub    $0x8,%esp
80104c78:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104c7b:	50                   	push   %eax
80104c7c:	6a 01                	push   $0x1
80104c7e:	e8 8f f5 ff ff       	call   80104212 <argint>
  if((argstr(0, &path)) < 0 ||
80104c83:	83 c4 10             	add    $0x10,%esp
80104c86:	85 c0                	test   %eax,%eax
80104c88:	78 4d                	js     80104cd7 <sys_mknod+0x82>
     argint(2, &minor) < 0 ||
80104c8a:	83 ec 08             	sub    $0x8,%esp
80104c8d:	8d 45 ec             	lea    -0x14(%ebp),%eax
80104c90:	50                   	push   %eax
80104c91:	6a 02                	push   $0x2
80104c93:	e8 7a f5 ff ff       	call   80104212 <argint>
     argint(1, &major) < 0 ||
80104c98:	83 c4 10             	add    $0x10,%esp
80104c9b:	85 c0                	test   %eax,%eax
80104c9d:	78 38                	js     80104cd7 <sys_mknod+0x82>
     (ip = create(path, T_DEV, major, minor)) == 0){
80104c9f:	0f bf 45 ec          	movswl -0x14(%ebp),%eax
80104ca3:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
     argint(2, &minor) < 0 ||
80104ca7:	83 ec 0c             	sub    $0xc,%esp
80104caa:	50                   	push   %eax
80104cab:	ba 03 00 00 00       	mov    $0x3,%edx
80104cb0:	8b 45 f4             	mov    -0xc(%ebp),%eax
80104cb3:	e8 67 f7 ff ff       	call   8010441f <create>
80104cb8:	83 c4 10             	add    $0x10,%esp
80104cbb:	85 c0                	test   %eax,%eax
80104cbd:	74 18                	je     80104cd7 <sys_mknod+0x82>
    end_op();
    return -1;
  }
  iunlockput(ip);
80104cbf:	83 ec 0c             	sub    $0xc,%esp
80104cc2:	50                   	push   %eax
80104cc3:	e8 4e ca ff ff       	call   80101716 <iunlockput>
  end_op();
80104cc8:	e8 7f db ff ff       	call   8010284c <end_op>
  return 0;
80104ccd:	83 c4 10             	add    $0x10,%esp
80104cd0:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104cd5:	c9                   	leave  
80104cd6:	c3                   	ret    
    end_op();
80104cd7:	e8 70 db ff ff       	call   8010284c <end_op>
    return -1;
80104cdc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104ce1:	eb f2                	jmp    80104cd5 <sys_mknod+0x80>

80104ce3 <sys_chdir>:

int
sys_chdir(void)
{
80104ce3:	55                   	push   %ebp
80104ce4:	89 e5                	mov    %esp,%ebp
80104ce6:	56                   	push   %esi
80104ce7:	53                   	push   %ebx
80104ce8:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
80104ceb:	e8 67 e5 ff ff       	call   80103257 <myproc>
80104cf0:	89 c6                	mov    %eax,%esi
  
  begin_op();
80104cf2:	e8 db da ff ff       	call   801027d2 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
80104cf7:	83 ec 08             	sub    $0x8,%esp
80104cfa:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104cfd:	50                   	push   %eax
80104cfe:	6a 00                	push   $0x0
80104d00:	e8 9d f5 ff ff       	call   801042a2 <argstr>
80104d05:	83 c4 10             	add    $0x10,%esp
80104d08:	85 c0                	test   %eax,%eax
80104d0a:	78 52                	js     80104d5e <sys_chdir+0x7b>
80104d0c:	83 ec 0c             	sub    $0xc,%esp
80104d0f:	ff 75 f4             	pushl  -0xc(%ebp)
80104d12:	e8 b8 ce ff ff       	call   80101bcf <namei>
80104d17:	89 c3                	mov    %eax,%ebx
80104d19:	83 c4 10             	add    $0x10,%esp
80104d1c:	85 c0                	test   %eax,%eax
80104d1e:	74 3e                	je     80104d5e <sys_chdir+0x7b>
    end_op();
    return -1;
  }
  ilock(ip);
80104d20:	83 ec 0c             	sub    $0xc,%esp
80104d23:	50                   	push   %eax
80104d24:	e8 46 c8 ff ff       	call   8010156f <ilock>
  if(ip->type != T_DIR){
80104d29:	83 c4 10             	add    $0x10,%esp
80104d2c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80104d31:	75 37                	jne    80104d6a <sys_chdir+0x87>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80104d33:	83 ec 0c             	sub    $0xc,%esp
80104d36:	53                   	push   %ebx
80104d37:	e8 f5 c8 ff ff       	call   80101631 <iunlock>
  iput(curproc->cwd);
80104d3c:	83 c4 04             	add    $0x4,%esp
80104d3f:	ff 76 68             	pushl  0x68(%esi)
80104d42:	e8 2f c9 ff ff       	call   80101676 <iput>
  end_op();
80104d47:	e8 00 db ff ff       	call   8010284c <end_op>
  curproc->cwd = ip;
80104d4c:	89 5e 68             	mov    %ebx,0x68(%esi)
  return 0;
80104d4f:	83 c4 10             	add    $0x10,%esp
80104d52:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104d57:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104d5a:	5b                   	pop    %ebx
80104d5b:	5e                   	pop    %esi
80104d5c:	5d                   	pop    %ebp
80104d5d:	c3                   	ret    
    end_op();
80104d5e:	e8 e9 da ff ff       	call   8010284c <end_op>
    return -1;
80104d63:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d68:	eb ed                	jmp    80104d57 <sys_chdir+0x74>
    iunlockput(ip);
80104d6a:	83 ec 0c             	sub    $0xc,%esp
80104d6d:	53                   	push   %ebx
80104d6e:	e8 a3 c9 ff ff       	call   80101716 <iunlockput>
    end_op();
80104d73:	e8 d4 da ff ff       	call   8010284c <end_op>
    return -1;
80104d78:	83 c4 10             	add    $0x10,%esp
80104d7b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104d80:	eb d5                	jmp    80104d57 <sys_chdir+0x74>

80104d82 <sys_exec>:

int
sys_exec(void)
{
80104d82:	55                   	push   %ebp
80104d83:	89 e5                	mov    %esp,%ebp
80104d85:	53                   	push   %ebx
80104d86:	81 ec 9c 00 00 00    	sub    $0x9c,%esp
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80104d8c:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104d8f:	50                   	push   %eax
80104d90:	6a 00                	push   $0x0
80104d92:	e8 0b f5 ff ff       	call   801042a2 <argstr>
80104d97:	83 c4 10             	add    $0x10,%esp
80104d9a:	85 c0                	test   %eax,%eax
80104d9c:	0f 88 a8 00 00 00    	js     80104e4a <sys_exec+0xc8>
80104da2:	83 ec 08             	sub    $0x8,%esp
80104da5:	8d 85 70 ff ff ff    	lea    -0x90(%ebp),%eax
80104dab:	50                   	push   %eax
80104dac:	6a 01                	push   $0x1
80104dae:	e8 5f f4 ff ff       	call   80104212 <argint>
80104db3:	83 c4 10             	add    $0x10,%esp
80104db6:	85 c0                	test   %eax,%eax
80104db8:	0f 88 93 00 00 00    	js     80104e51 <sys_exec+0xcf>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
80104dbe:	83 ec 04             	sub    $0x4,%esp
80104dc1:	68 80 00 00 00       	push   $0x80
80104dc6:	6a 00                	push   $0x0
80104dc8:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
80104dce:	50                   	push   %eax
80104dcf:	e8 f3 f1 ff ff       	call   80103fc7 <memset>
80104dd4:	83 c4 10             	add    $0x10,%esp
  for(i=0;; i++){
80104dd7:	bb 00 00 00 00       	mov    $0x0,%ebx
    if(i >= NELEM(argv))
80104ddc:	83 fb 1f             	cmp    $0x1f,%ebx
80104ddf:	77 77                	ja     80104e58 <sys_exec+0xd6>
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
80104de1:	83 ec 08             	sub    $0x8,%esp
80104de4:	8d 85 6c ff ff ff    	lea    -0x94(%ebp),%eax
80104dea:	50                   	push   %eax
80104deb:	8b 85 70 ff ff ff    	mov    -0x90(%ebp),%eax
80104df1:	8d 04 98             	lea    (%eax,%ebx,4),%eax
80104df4:	50                   	push   %eax
80104df5:	e8 9c f3 ff ff       	call   80104196 <fetchint>
80104dfa:	83 c4 10             	add    $0x10,%esp
80104dfd:	85 c0                	test   %eax,%eax
80104dff:	78 5e                	js     80104e5f <sys_exec+0xdd>
      return -1;
    if(uarg == 0){
80104e01:	8b 85 6c ff ff ff    	mov    -0x94(%ebp),%eax
80104e07:	85 c0                	test   %eax,%eax
80104e09:	74 1d                	je     80104e28 <sys_exec+0xa6>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
80104e0b:	83 ec 08             	sub    $0x8,%esp
80104e0e:	8d 94 9d 74 ff ff ff 	lea    -0x8c(%ebp,%ebx,4),%edx
80104e15:	52                   	push   %edx
80104e16:	50                   	push   %eax
80104e17:	e8 b6 f3 ff ff       	call   801041d2 <fetchstr>
80104e1c:	83 c4 10             	add    $0x10,%esp
80104e1f:	85 c0                	test   %eax,%eax
80104e21:	78 46                	js     80104e69 <sys_exec+0xe7>
  for(i=0;; i++){
80104e23:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
80104e26:	eb b4                	jmp    80104ddc <sys_exec+0x5a>
      argv[i] = 0;
80104e28:	c7 84 9d 74 ff ff ff 	movl   $0x0,-0x8c(%ebp,%ebx,4)
80104e2f:	00 00 00 00 
      return -1;
  }
  return exec(path, argv);
80104e33:	83 ec 08             	sub    $0x8,%esp
80104e36:	8d 85 74 ff ff ff    	lea    -0x8c(%ebp),%eax
80104e3c:	50                   	push   %eax
80104e3d:	ff 75 f4             	pushl  -0xc(%ebp)
80104e40:	e8 8d ba ff ff       	call   801008d2 <exec>
80104e45:	83 c4 10             	add    $0x10,%esp
80104e48:	eb 1a                	jmp    80104e64 <sys_exec+0xe2>
    return -1;
80104e4a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104e4f:	eb 13                	jmp    80104e64 <sys_exec+0xe2>
80104e51:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104e56:	eb 0c                	jmp    80104e64 <sys_exec+0xe2>
      return -1;
80104e58:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104e5d:	eb 05                	jmp    80104e64 <sys_exec+0xe2>
      return -1;
80104e5f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104e64:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104e67:	c9                   	leave  
80104e68:	c3                   	ret    
      return -1;
80104e69:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104e6e:	eb f4                	jmp    80104e64 <sys_exec+0xe2>

80104e70 <sys_pipe>:

int
sys_pipe(void)
{
80104e70:	55                   	push   %ebp
80104e71:	89 e5                	mov    %esp,%ebp
80104e73:	53                   	push   %ebx
80104e74:	83 ec 18             	sub    $0x18,%esp
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80104e77:	6a 08                	push   $0x8
80104e79:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e7c:	50                   	push   %eax
80104e7d:	6a 00                	push   $0x0
80104e7f:	e8 b6 f3 ff ff       	call   8010423a <argptr>
80104e84:	83 c4 10             	add    $0x10,%esp
80104e87:	85 c0                	test   %eax,%eax
80104e89:	78 77                	js     80104f02 <sys_pipe+0x92>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
80104e8b:	83 ec 08             	sub    $0x8,%esp
80104e8e:	8d 45 ec             	lea    -0x14(%ebp),%eax
80104e91:	50                   	push   %eax
80104e92:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104e95:	50                   	push   %eax
80104e96:	e8 f5 de ff ff       	call   80102d90 <pipealloc>
80104e9b:	83 c4 10             	add    $0x10,%esp
80104e9e:	85 c0                	test   %eax,%eax
80104ea0:	78 67                	js     80104f09 <sys_pipe+0x99>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80104ea2:	8b 45 f0             	mov    -0x10(%ebp),%eax
80104ea5:	e8 e7 f4 ff ff       	call   80104391 <fdalloc>
80104eaa:	89 c3                	mov    %eax,%ebx
80104eac:	85 c0                	test   %eax,%eax
80104eae:	78 21                	js     80104ed1 <sys_pipe+0x61>
80104eb0:	8b 45 ec             	mov    -0x14(%ebp),%eax
80104eb3:	e8 d9 f4 ff ff       	call   80104391 <fdalloc>
80104eb8:	85 c0                	test   %eax,%eax
80104eba:	78 15                	js     80104ed1 <sys_pipe+0x61>
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
    fileclose(wf);
    return -1;
  }
  fd[0] = fd0;
80104ebc:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ebf:	89 1a                	mov    %ebx,(%edx)
  fd[1] = fd1;
80104ec1:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104ec4:	89 42 04             	mov    %eax,0x4(%edx)
  return 0;
80104ec7:	b8 00 00 00 00       	mov    $0x0,%eax
}
80104ecc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104ecf:	c9                   	leave  
80104ed0:	c3                   	ret    
    if(fd0 >= 0)
80104ed1:	85 db                	test   %ebx,%ebx
80104ed3:	78 0d                	js     80104ee2 <sys_pipe+0x72>
      myproc()->ofile[fd0] = 0;
80104ed5:	e8 7d e3 ff ff       	call   80103257 <myproc>
80104eda:	c7 44 98 28 00 00 00 	movl   $0x0,0x28(%eax,%ebx,4)
80104ee1:	00 
    fileclose(rf);
80104ee2:	83 ec 0c             	sub    $0xc,%esp
80104ee5:	ff 75 f0             	pushl  -0x10(%ebp)
80104ee8:	e8 e6 bd ff ff       	call   80100cd3 <fileclose>
    fileclose(wf);
80104eed:	83 c4 04             	add    $0x4,%esp
80104ef0:	ff 75 ec             	pushl  -0x14(%ebp)
80104ef3:	e8 db bd ff ff       	call   80100cd3 <fileclose>
    return -1;
80104ef8:	83 c4 10             	add    $0x10,%esp
80104efb:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f00:	eb ca                	jmp    80104ecc <sys_pipe+0x5c>
    return -1;
80104f02:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f07:	eb c3                	jmp    80104ecc <sys_pipe+0x5c>
    return -1;
80104f09:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f0e:	eb bc                	jmp    80104ecc <sys_pipe+0x5c>

80104f10 <sys_fork>:
#include "mmu.h"
#include "proc.h"

int
sys_fork(void)
{
80104f10:	55                   	push   %ebp
80104f11:	89 e5                	mov    %esp,%ebp
80104f13:	83 ec 08             	sub    $0x8,%esp
  return fork();
80104f16:	e8 c5 e4 ff ff       	call   801033e0 <fork>
}
80104f1b:	c9                   	leave  
80104f1c:	c3                   	ret    

80104f1d <sys_exit>:

int
sys_exit(void)
{
80104f1d:	55                   	push   %ebp
80104f1e:	89 e5                	mov    %esp,%ebp
80104f20:	83 ec 08             	sub    $0x8,%esp
  exit();
80104f23:	e8 70 e6 ff ff       	call   80103598 <exit>
  return 0;  // not reached
}
80104f28:	b8 00 00 00 00       	mov    $0x0,%eax
80104f2d:	c9                   	leave  
80104f2e:	c3                   	ret    

80104f2f <sys_wait>:

int
sys_wait(void)
{
80104f2f:	55                   	push   %ebp
80104f30:	89 e5                	mov    %esp,%ebp
80104f32:	83 ec 08             	sub    $0x8,%esp
  return wait();
80104f35:	e8 ea e7 ff ff       	call   80103724 <wait>
}
80104f3a:	c9                   	leave  
80104f3b:	c3                   	ret    

80104f3c <sys_kill>:

int
sys_kill(void)
{
80104f3c:	55                   	push   %ebp
80104f3d:	89 e5                	mov    %esp,%ebp
80104f3f:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
80104f42:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104f45:	50                   	push   %eax
80104f46:	6a 00                	push   $0x0
80104f48:	e8 c5 f2 ff ff       	call   80104212 <argint>
80104f4d:	83 c4 10             	add    $0x10,%esp
80104f50:	85 c0                	test   %eax,%eax
80104f52:	78 10                	js     80104f64 <sys_kill+0x28>
    return -1;
  return kill(pid);
80104f54:	83 ec 0c             	sub    $0xc,%esp
80104f57:	ff 75 f4             	pushl  -0xc(%ebp)
80104f5a:	e8 c5 e8 ff ff       	call   80103824 <kill>
80104f5f:	83 c4 10             	add    $0x10,%esp
}
80104f62:	c9                   	leave  
80104f63:	c3                   	ret    
    return -1;
80104f64:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f69:	eb f7                	jmp    80104f62 <sys_kill+0x26>

80104f6b <sys_getpid>:

int
sys_getpid(void)
{
80104f6b:	55                   	push   %ebp
80104f6c:	89 e5                	mov    %esp,%ebp
80104f6e:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
80104f71:	e8 e1 e2 ff ff       	call   80103257 <myproc>
80104f76:	8b 40 10             	mov    0x10(%eax),%eax
}
80104f79:	c9                   	leave  
80104f7a:	c3                   	ret    

80104f7b <sys_sbrk>:

int
sys_sbrk(void)
{
80104f7b:	55                   	push   %ebp
80104f7c:	89 e5                	mov    %esp,%ebp
80104f7e:	53                   	push   %ebx
80104f7f:	83 ec 1c             	sub    $0x1c,%esp
  int addr;
  int n;

  if(argint(0, &n) < 0)
80104f82:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104f85:	50                   	push   %eax
80104f86:	6a 00                	push   $0x0
80104f88:	e8 85 f2 ff ff       	call   80104212 <argint>
80104f8d:	83 c4 10             	add    $0x10,%esp
80104f90:	85 c0                	test   %eax,%eax
80104f92:	78 18                	js     80104fac <sys_sbrk+0x31>
    return -1;
  addr = myproc()->sz;
80104f94:	e8 be e2 ff ff       	call   80103257 <myproc>
80104f99:	8b 18                	mov    (%eax),%ebx
  myproc()->sz += n;
80104f9b:	e8 b7 e2 ff ff       	call   80103257 <myproc>
80104fa0:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104fa3:	01 10                	add    %edx,(%eax)
  //if(growproc(n) < 0)
    //return -1;
  return addr;
}
80104fa5:	89 d8                	mov    %ebx,%eax
80104fa7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104faa:	c9                   	leave  
80104fab:	c3                   	ret    
    return -1;
80104fac:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80104fb1:	eb f2                	jmp    80104fa5 <sys_sbrk+0x2a>

80104fb3 <sys_sleep>:

int
sys_sleep(void)
{
80104fb3:	55                   	push   %ebp
80104fb4:	89 e5                	mov    %esp,%ebp
80104fb6:	53                   	push   %ebx
80104fb7:	83 ec 1c             	sub    $0x1c,%esp
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80104fba:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104fbd:	50                   	push   %eax
80104fbe:	6a 00                	push   $0x0
80104fc0:	e8 4d f2 ff ff       	call   80104212 <argint>
80104fc5:	83 c4 10             	add    $0x10,%esp
80104fc8:	85 c0                	test   %eax,%eax
80104fca:	78 75                	js     80105041 <sys_sleep+0x8e>
    return -1;
  acquire(&tickslock);
80104fcc:	83 ec 0c             	sub    $0xc,%esp
80104fcf:	68 80 4e 11 80       	push   $0x80114e80
80104fd4:	e8 42 ef ff ff       	call   80103f1b <acquire>
  ticks0 = ticks;
80104fd9:	8b 1d c0 56 11 80    	mov    0x801156c0,%ebx
  while(ticks - ticks0 < n){
80104fdf:	83 c4 10             	add    $0x10,%esp
80104fe2:	a1 c0 56 11 80       	mov    0x801156c0,%eax
80104fe7:	29 d8                	sub    %ebx,%eax
80104fe9:	3b 45 f4             	cmp    -0xc(%ebp),%eax
80104fec:	73 39                	jae    80105027 <sys_sleep+0x74>
    if(myproc()->killed){
80104fee:	e8 64 e2 ff ff       	call   80103257 <myproc>
80104ff3:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
80104ff7:	75 17                	jne    80105010 <sys_sleep+0x5d>
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
80104ff9:	83 ec 08             	sub    $0x8,%esp
80104ffc:	68 80 4e 11 80       	push   $0x80114e80
80105001:	68 c0 56 11 80       	push   $0x801156c0
80105006:	e8 88 e6 ff ff       	call   80103693 <sleep>
8010500b:	83 c4 10             	add    $0x10,%esp
8010500e:	eb d2                	jmp    80104fe2 <sys_sleep+0x2f>
      release(&tickslock);
80105010:	83 ec 0c             	sub    $0xc,%esp
80105013:	68 80 4e 11 80       	push   $0x80114e80
80105018:	e8 63 ef ff ff       	call   80103f80 <release>
      return -1;
8010501d:	83 c4 10             	add    $0x10,%esp
80105020:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105025:	eb 15                	jmp    8010503c <sys_sleep+0x89>
  }
  release(&tickslock);
80105027:	83 ec 0c             	sub    $0xc,%esp
8010502a:	68 80 4e 11 80       	push   $0x80114e80
8010502f:	e8 4c ef ff ff       	call   80103f80 <release>
  return 0;
80105034:	83 c4 10             	add    $0x10,%esp
80105037:	b8 00 00 00 00       	mov    $0x0,%eax
}
8010503c:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010503f:	c9                   	leave  
80105040:	c3                   	ret    
    return -1;
80105041:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105046:	eb f4                	jmp    8010503c <sys_sleep+0x89>

80105048 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
80105048:	55                   	push   %ebp
80105049:	89 e5                	mov    %esp,%ebp
8010504b:	53                   	push   %ebx
8010504c:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
8010504f:	68 80 4e 11 80       	push   $0x80114e80
80105054:	e8 c2 ee ff ff       	call   80103f1b <acquire>
  xticks = ticks;
80105059:	8b 1d c0 56 11 80    	mov    0x801156c0,%ebx
  release(&tickslock);
8010505f:	c7 04 24 80 4e 11 80 	movl   $0x80114e80,(%esp)
80105066:	e8 15 ef ff ff       	call   80103f80 <release>
  return xticks;
}
8010506b:	89 d8                	mov    %ebx,%eax
8010506d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105070:	c9                   	leave  
80105071:	c3                   	ret    

80105072 <sys_yield>:

int
sys_yield(void)
{
80105072:	55                   	push   %ebp
80105073:	89 e5                	mov    %esp,%ebp
80105075:	83 ec 08             	sub    $0x8,%esp
  yield();
80105078:	e8 e4 e5 ff ff       	call   80103661 <yield>
  return 0;
}
8010507d:	b8 00 00 00 00       	mov    $0x0,%eax
80105082:	c9                   	leave  
80105083:	c3                   	ret    

80105084 <sys_shutdown>:

int sys_shutdown(void)
{
80105084:	55                   	push   %ebp
80105085:	89 e5                	mov    %esp,%ebp
80105087:	83 ec 08             	sub    $0x8,%esp
  shutdown();
8010508a:	e8 77 d1 ff ff       	call   80102206 <shutdown>
  return 0;
}
8010508f:	b8 00 00 00 00       	mov    $0x0,%eax
80105094:	c9                   	leave  
80105095:	c3                   	ret    

80105096 <sys_getprocessesinfo>:
int sys_getprocessesinfo(void){
80105096:	55                   	push   %ebp
80105097:	89 e5                	mov    %esp,%ebp
80105099:	83 ec 08             	sub    $0x8,%esp
  return getprocessesinfo();
8010509c:	e8 ae e8 ff ff       	call   8010394f <getprocessesinfo>
}
801050a1:	c9                   	leave  
801050a2:	c3                   	ret    

801050a3 <sys_settickets>:
int sys_settickets(void){
801050a3:	55                   	push   %ebp
801050a4:	89 e5                	mov    %esp,%ebp
801050a6:	83 ec 20             	sub    $0x20,%esp
  int ticket_number;
  if (argint(0, &ticket_number) < 0)
801050a9:	8d 45 f4             	lea    -0xc(%ebp),%eax
801050ac:	50                   	push   %eax
801050ad:	6a 00                	push   $0x0
801050af:	e8 5e f1 ff ff       	call   80104212 <argint>
801050b4:	83 c4 10             	add    $0x10,%esp
801050b7:	85 c0                	test   %eax,%eax
801050b9:	78 1d                	js     801050d8 <sys_settickets+0x35>
  {
     myproc()->tickets = 10;}
  else{
     argint(0,&myproc()->tickets);
801050bb:	e8 97 e1 ff ff       	call   80103257 <myproc>
801050c0:	83 ec 08             	sub    $0x8,%esp
801050c3:	83 c0 7c             	add    $0x7c,%eax
801050c6:	50                   	push   %eax
801050c7:	6a 00                	push   $0x0
801050c9:	e8 44 f1 ff ff       	call   80104212 <argint>
801050ce:	83 c4 10             	add    $0x10,%esp
  }
  
  return 0;
} 
801050d1:	b8 00 00 00 00       	mov    $0x0,%eax
801050d6:	c9                   	leave  
801050d7:	c3                   	ret    
     myproc()->tickets = 10;}
801050d8:	e8 7a e1 ff ff       	call   80103257 <myproc>
801050dd:	c7 40 7c 0a 00 00 00 	movl   $0xa,0x7c(%eax)
801050e4:	eb eb                	jmp    801050d1 <sys_settickets+0x2e>

801050e6 <sys_dumppagetable>:
int sys_dumppagetable(void){
801050e6:	55                   	push   %ebp
801050e7:	89 e5                	mov    %esp,%ebp
801050e9:	83 ec 20             	sub    $0x20,%esp
  int pid;
  argint(0, &pid);
801050ec:	8d 45 f4             	lea    -0xc(%ebp),%eax
801050ef:	50                   	push   %eax
801050f0:	6a 00                	push   $0x0
801050f2:	e8 1b f1 ff ff       	call   80104212 <argint>
  dumppagetable(pid);
801050f7:	83 c4 04             	add    $0x4,%esp
801050fa:	ff 75 f4             	pushl  -0xc(%ebp)
801050fd:	e8 59 ea ff ff       	call   80103b5b <dumppagetable>
  return 0;
80105102:	b8 00 00 00 00       	mov    $0x0,%eax
80105107:	c9                   	leave  
80105108:	c3                   	ret    

80105109 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80105109:	1e                   	push   %ds
  pushl %es
8010510a:	06                   	push   %es
  pushl %fs
8010510b:	0f a0                	push   %fs
  pushl %gs
8010510d:	0f a8                	push   %gs
  pushal
8010510f:	60                   	pusha  
  
  # Set up data segments.
  movw $(SEG_KDATA<<3), %ax
80105110:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80105114:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80105116:	8e c0                	mov    %eax,%es

  # Call trap(tf), where tf=%esp
  pushl %esp
80105118:	54                   	push   %esp
  call trap
80105119:	e8 e3 00 00 00       	call   80105201 <trap>
  addl $4, %esp
8010511e:	83 c4 04             	add    $0x4,%esp

80105121 <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
80105121:	61                   	popa   
  popl %gs
80105122:	0f a9                	pop    %gs
  popl %fs
80105124:	0f a1                	pop    %fs
  popl %es
80105126:	07                   	pop    %es
  popl %ds
80105127:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80105128:	83 c4 08             	add    $0x8,%esp
  iret
8010512b:	cf                   	iret   

8010512c <tvinit>:
extern uint vectors[]; // in vectors.S: array of 256 entry pointers
struct spinlock tickslock;
uint ticks;

void tvinit(void)
{
8010512c:	55                   	push   %ebp
8010512d:	89 e5                	mov    %esp,%ebp
8010512f:	83 ec 08             	sub    $0x8,%esp
  int i;

  for (i = 0; i < 256; i++)
80105132:	b8 00 00 00 00       	mov    $0x0,%eax
80105137:	eb 4a                	jmp    80105183 <tvinit+0x57>
    SETGATE(idt[i], 0, SEG_KCODE << 3, vectors[i], 0);
80105139:	8b 0c 85 0c a0 10 80 	mov    -0x7fef5ff4(,%eax,4),%ecx
80105140:	66 89 0c c5 c0 4e 11 	mov    %cx,-0x7feeb140(,%eax,8)
80105147:	80 
80105148:	66 c7 04 c5 c2 4e 11 	movw   $0x8,-0x7feeb13e(,%eax,8)
8010514f:	80 08 00 
80105152:	c6 04 c5 c4 4e 11 80 	movb   $0x0,-0x7feeb13c(,%eax,8)
80105159:	00 
8010515a:	0f b6 14 c5 c5 4e 11 	movzbl -0x7feeb13b(,%eax,8),%edx
80105161:	80 
80105162:	83 e2 f0             	and    $0xfffffff0,%edx
80105165:	83 ca 0e             	or     $0xe,%edx
80105168:	83 e2 8f             	and    $0xffffff8f,%edx
8010516b:	83 ca 80             	or     $0xffffff80,%edx
8010516e:	88 14 c5 c5 4e 11 80 	mov    %dl,-0x7feeb13b(,%eax,8)
80105175:	c1 e9 10             	shr    $0x10,%ecx
80105178:	66 89 0c c5 c6 4e 11 	mov    %cx,-0x7feeb13a(,%eax,8)
8010517f:	80 
  for (i = 0; i < 256; i++)
80105180:	83 c0 01             	add    $0x1,%eax
80105183:	3d ff 00 00 00       	cmp    $0xff,%eax
80105188:	7e af                	jle    80105139 <tvinit+0xd>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE << 3, vectors[T_SYSCALL], DPL_USER);
8010518a:	8b 15 0c a1 10 80    	mov    0x8010a10c,%edx
80105190:	66 89 15 c0 50 11 80 	mov    %dx,0x801150c0
80105197:	66 c7 05 c2 50 11 80 	movw   $0x8,0x801150c2
8010519e:	08 00 
801051a0:	c6 05 c4 50 11 80 00 	movb   $0x0,0x801150c4
801051a7:	0f b6 05 c5 50 11 80 	movzbl 0x801150c5,%eax
801051ae:	83 c8 0f             	or     $0xf,%eax
801051b1:	83 e0 ef             	and    $0xffffffef,%eax
801051b4:	83 c8 e0             	or     $0xffffffe0,%eax
801051b7:	a2 c5 50 11 80       	mov    %al,0x801150c5
801051bc:	c1 ea 10             	shr    $0x10,%edx
801051bf:	66 89 15 c6 50 11 80 	mov    %dx,0x801150c6

  initlock(&tickslock, "time");
801051c6:	83 ec 08             	sub    $0x8,%esp
801051c9:	68 72 75 10 80       	push   $0x80107572
801051ce:	68 80 4e 11 80       	push   $0x80114e80
801051d3:	e8 07 ec ff ff       	call   80103ddf <initlock>
}
801051d8:	83 c4 10             	add    $0x10,%esp
801051db:	c9                   	leave  
801051dc:	c3                   	ret    

801051dd <idtinit>:

void idtinit(void)
{
801051dd:	55                   	push   %ebp
801051de:	89 e5                	mov    %esp,%ebp
801051e0:	83 ec 10             	sub    $0x10,%esp
  pd[0] = size-1;
801051e3:	66 c7 45 fa ff 07    	movw   $0x7ff,-0x6(%ebp)
  pd[1] = (uint)p;
801051e9:	b8 c0 4e 11 80       	mov    $0x80114ec0,%eax
801051ee:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801051f2:	c1 e8 10             	shr    $0x10,%eax
801051f5:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
801051f9:	8d 45 fa             	lea    -0x6(%ebp),%eax
801051fc:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
801051ff:	c9                   	leave  
80105200:	c3                   	ret    

80105201 <trap>:

//PAGEBREAK: 41
void trap(struct trapframe *tf)
{
80105201:	55                   	push   %ebp
80105202:	89 e5                	mov    %esp,%ebp
80105204:	57                   	push   %edi
80105205:	56                   	push   %esi
80105206:	53                   	push   %ebx
80105207:	83 ec 1c             	sub    $0x1c,%esp
8010520a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (tf->trapno == T_SYSCALL)
8010520d:	8b 43 30             	mov    0x30(%ebx),%eax
80105210:	83 f8 40             	cmp    $0x40,%eax
80105213:	74 41                	je     80105256 <trap+0x55>
    if (myproc()->killed)
      exit();
    return;
  }

  switch (tf->trapno)
80105215:	83 f8 27             	cmp    $0x27,%eax
80105218:	0f 84 b1 01 00 00    	je     801053cf <trap+0x1ce>
8010521e:	83 f8 27             	cmp    $0x27,%eax
80105221:	0f 87 a8 00 00 00    	ja     801052cf <trap+0xce>
80105227:	83 f8 20             	cmp    $0x20,%eax
8010522a:	0f 84 e4 00 00 00    	je     80105314 <trap+0x113>
80105230:	83 f8 20             	cmp    $0x20,%eax
80105233:	76 59                	jbe    8010528e <trap+0x8d>
80105235:	83 f8 21             	cmp    $0x21,%eax
80105238:	0f 84 82 01 00 00    	je     801053c0 <trap+0x1bf>
8010523e:	83 f8 24             	cmp    $0x24,%eax
80105241:	0f 85 dd 01 00 00    	jne    80105424 <trap+0x223>
  case T_IRQ0 + IRQ_KBD:
    kbdintr();
    lapiceoi();
    break;
  case T_IRQ0 + IRQ_COM1:
    uartintr();
80105247:	e8 aa 03 00 00       	call   801055f6 <uartintr>
    lapiceoi();
8010524c:	e8 6c d1 ff ff       	call   801023bd <lapiceoi>
    break;
80105251:	e9 cc 00 00 00       	jmp    80105322 <trap+0x121>
    if (myproc()->killed)
80105256:	e8 fc df ff ff       	call   80103257 <myproc>
8010525b:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
8010525f:	75 26                	jne    80105287 <trap+0x86>
    myproc()->tf = tf;
80105261:	e8 f1 df ff ff       	call   80103257 <myproc>
80105266:	89 58 18             	mov    %ebx,0x18(%eax)
    syscall();
80105269:	e8 67 f0 ff ff       	call   801042d5 <syscall>
    if (myproc()->killed)
8010526e:	e8 e4 df ff ff       	call   80103257 <myproc>
80105273:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
80105277:	0f 84 07 01 00 00    	je     80105384 <trap+0x183>
      exit();
8010527d:	e8 16 e3 ff ff       	call   80103598 <exit>
80105282:	e9 fd 00 00 00       	jmp    80105384 <trap+0x183>
      exit();
80105287:	e8 0c e3 ff ff       	call   80103598 <exit>
8010528c:	eb d3                	jmp    80105261 <trap+0x60>
  switch (tf->trapno)
8010528e:	83 f8 0e             	cmp    $0xe,%eax
80105291:	0f 85 8d 01 00 00    	jne    80105424 <trap+0x223>
    break;
  case T_USER_SYSCALL:
    cprintf("user interrupt 128 called!\n");
    break;
  case T_PGFLT:
    cprintf("here we goo...");
80105297:	83 ec 0c             	sub    $0xc,%esp
8010529a:	68 93 75 10 80       	push   $0x80107593
8010529f:	e8 67 b3 ff ff       	call   8010060b <cprintf>

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
801052a4:	0f 20 d6             	mov    %cr2,%esi
    if ((rcr2() >= myproc()->sz || myproc()->sz >= KERNBASE) || (pfh(myproc()->pgdir, myproc()->sz, rcr2())))
801052a7:	e8 ab df ff ff       	call   80103257 <myproc>
801052ac:	83 c4 10             	add    $0x10,%esp
801052af:	39 30                	cmp    %esi,(%eax)
801052b1:	76 0e                	jbe    801052c1 <trap+0xc0>
801052b3:	e8 9f df ff ff       	call   80103257 <myproc>
801052b8:	83 38 00             	cmpl   $0x0,(%eax)
801052bb:	0f 89 37 01 00 00    	jns    801053f8 <trap+0x1f7>
    {
      myproc()->killed = 1;
801052c1:	e8 91 df ff ff       	call   80103257 <myproc>
801052c6:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      break;
801052cd:	eb 53                	jmp    80105322 <trap+0x121>
  switch (tf->trapno)
801052cf:	83 f8 2f             	cmp    $0x2f,%eax
801052d2:	74 4e                	je     80105322 <trap+0x121>
801052d4:	83 f8 2f             	cmp    $0x2f,%eax
801052d7:	76 26                	jbe    801052ff <trap+0xfe>
801052d9:	83 f8 3f             	cmp    $0x3f,%eax
801052dc:	0f 84 ed 00 00 00    	je     801053cf <trap+0x1ce>
801052e2:	3d 80 00 00 00       	cmp    $0x80,%eax
801052e7:	0f 85 37 01 00 00    	jne    80105424 <trap+0x223>
    cprintf("user interrupt 128 called!\n");
801052ed:	83 ec 0c             	sub    $0xc,%esp
801052f0:	68 77 75 10 80       	push   $0x80107577
801052f5:	e8 11 b3 ff ff       	call   8010060b <cprintf>
    break;
801052fa:	83 c4 10             	add    $0x10,%esp
801052fd:	eb 23                	jmp    80105322 <trap+0x121>
  switch (tf->trapno)
801052ff:	83 f8 2e             	cmp    $0x2e,%eax
80105302:	0f 85 1c 01 00 00    	jne    80105424 <trap+0x223>
    ideintr();
80105308:	e8 54 ca ff ff       	call   80101d61 <ideintr>
    lapiceoi();
8010530d:	e8 ab d0 ff ff       	call   801023bd <lapiceoi>
    break;
80105312:	eb 0e                	jmp    80105322 <trap+0x121>
    if (cpuid() == 0)
80105314:	e8 23 df ff ff       	call   8010323c <cpuid>
80105319:	85 c0                	test   %eax,%eax
8010531b:	74 6f                	je     8010538c <trap+0x18b>
    lapiceoi();
8010531d:	e8 9b d0 ff ff       	call   801023bd <lapiceoi>
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if (myproc() && myproc()->killed && (tf->cs & 3) == DPL_USER)
80105322:	e8 30 df ff ff       	call   80103257 <myproc>
80105327:	85 c0                	test   %eax,%eax
80105329:	74 1c                	je     80105347 <trap+0x146>
8010532b:	e8 27 df ff ff       	call   80103257 <myproc>
80105330:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
80105334:	74 11                	je     80105347 <trap+0x146>
80105336:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
8010533a:	83 e0 03             	and    $0x3,%eax
8010533d:	66 83 f8 03          	cmp    $0x3,%ax
80105341:	0f 84 70 01 00 00    	je     801054b7 <trap+0x2b6>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if (myproc() && myproc()->state == RUNNING &&
80105347:	e8 0b df ff ff       	call   80103257 <myproc>
8010534c:	85 c0                	test   %eax,%eax
8010534e:	74 0f                	je     8010535f <trap+0x15e>
80105350:	e8 02 df ff ff       	call   80103257 <myproc>
80105355:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105359:	0f 84 62 01 00 00    	je     801054c1 <trap+0x2c0>
      tf->trapno == T_IRQ0 + IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if (myproc() && myproc()->killed && (tf->cs & 3) == DPL_USER)
8010535f:	e8 f3 de ff ff       	call   80103257 <myproc>
80105364:	85 c0                	test   %eax,%eax
80105366:	74 1c                	je     80105384 <trap+0x183>
80105368:	e8 ea de ff ff       	call   80103257 <myproc>
8010536d:	83 78 24 00          	cmpl   $0x0,0x24(%eax)
80105371:	74 11                	je     80105384 <trap+0x183>
80105373:	0f b7 43 3c          	movzwl 0x3c(%ebx),%eax
80105377:	83 e0 03             	and    $0x3,%eax
8010537a:	66 83 f8 03          	cmp    $0x3,%ax
8010537e:	0f 84 51 01 00 00    	je     801054d5 <trap+0x2d4>
    exit();
}
80105384:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105387:	5b                   	pop    %ebx
80105388:	5e                   	pop    %esi
80105389:	5f                   	pop    %edi
8010538a:	5d                   	pop    %ebp
8010538b:	c3                   	ret    
      acquire(&tickslock);
8010538c:	83 ec 0c             	sub    $0xc,%esp
8010538f:	68 80 4e 11 80       	push   $0x80114e80
80105394:	e8 82 eb ff ff       	call   80103f1b <acquire>
      ticks++;
80105399:	83 05 c0 56 11 80 01 	addl   $0x1,0x801156c0
      wakeup(&ticks);
801053a0:	c7 04 24 c0 56 11 80 	movl   $0x801156c0,(%esp)
801053a7:	e8 4f e4 ff ff       	call   801037fb <wakeup>
      release(&tickslock);
801053ac:	c7 04 24 80 4e 11 80 	movl   $0x80114e80,(%esp)
801053b3:	e8 c8 eb ff ff       	call   80103f80 <release>
801053b8:	83 c4 10             	add    $0x10,%esp
801053bb:	e9 5d ff ff ff       	jmp    8010531d <trap+0x11c>
    kbdintr();
801053c0:	e8 2c ce ff ff       	call   801021f1 <kbdintr>
    lapiceoi();
801053c5:	e8 f3 cf ff ff       	call   801023bd <lapiceoi>
    break;
801053ca:	e9 53 ff ff ff       	jmp    80105322 <trap+0x121>
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801053cf:	8b 7b 38             	mov    0x38(%ebx),%edi
            cpuid(), tf->cs, tf->eip);
801053d2:	0f b7 73 3c          	movzwl 0x3c(%ebx),%esi
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
801053d6:	e8 61 de ff ff       	call   8010323c <cpuid>
801053db:	57                   	push   %edi
801053dc:	0f b7 f6             	movzwl %si,%esi
801053df:	56                   	push   %esi
801053e0:	50                   	push   %eax
801053e1:	68 a8 75 10 80       	push   $0x801075a8
801053e6:	e8 20 b2 ff ff       	call   8010060b <cprintf>
    lapiceoi();
801053eb:	e8 cd cf ff ff       	call   801023bd <lapiceoi>
    break;
801053f0:	83 c4 10             	add    $0x10,%esp
801053f3:	e9 2a ff ff ff       	jmp    80105322 <trap+0x121>
801053f8:	0f 20 d7             	mov    %cr2,%edi
    if ((rcr2() >= myproc()->sz || myproc()->sz >= KERNBASE) || (pfh(myproc()->pgdir, myproc()->sz, rcr2())))
801053fb:	e8 57 de ff ff       	call   80103257 <myproc>
80105400:	8b 30                	mov    (%eax),%esi
80105402:	e8 50 de ff ff       	call   80103257 <myproc>
80105407:	83 ec 04             	sub    $0x4,%esp
8010540a:	57                   	push   %edi
8010540b:	56                   	push   %esi
8010540c:	ff 70 04             	pushl  0x4(%eax)
8010540f:	e8 a6 14 00 00       	call   801068ba <pfh>
80105414:	83 c4 10             	add    $0x10,%esp
80105417:	85 c0                	test   %eax,%eax
80105419:	0f 84 03 ff ff ff    	je     80105322 <trap+0x121>
8010541f:	e9 9d fe ff ff       	jmp    801052c1 <trap+0xc0>
    if (myproc() == 0 || (tf->cs & 3) == 0)
80105424:	e8 2e de ff ff       	call   80103257 <myproc>
80105429:	85 c0                	test   %eax,%eax
8010542b:	74 5f                	je     8010548c <trap+0x28b>
8010542d:	f6 43 3c 03          	testb  $0x3,0x3c(%ebx)
80105431:	74 59                	je     8010548c <trap+0x28b>
80105433:	0f 20 d7             	mov    %cr2,%edi
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105436:	8b 43 38             	mov    0x38(%ebx),%eax
80105439:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010543c:	e8 fb dd ff ff       	call   8010323c <cpuid>
80105441:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105444:	8b 53 34             	mov    0x34(%ebx),%edx
80105447:	89 55 dc             	mov    %edx,-0x24(%ebp)
8010544a:	8b 73 30             	mov    0x30(%ebx),%esi
            myproc()->pid, myproc()->name, tf->trapno, tf->err, cpuid(), tf->eip, rcr2());
8010544d:	e8 05 de ff ff       	call   80103257 <myproc>
80105452:	8d 48 6c             	lea    0x6c(%eax),%ecx
80105455:	89 4d d8             	mov    %ecx,-0x28(%ebp)
80105458:	e8 fa dd ff ff       	call   80103257 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
8010545d:	57                   	push   %edi
8010545e:	ff 75 e4             	pushl  -0x1c(%ebp)
80105461:	ff 75 e0             	pushl  -0x20(%ebp)
80105464:	ff 75 dc             	pushl  -0x24(%ebp)
80105467:	56                   	push   %esi
80105468:	ff 75 d8             	pushl  -0x28(%ebp)
8010546b:	ff 70 10             	pushl  0x10(%eax)
8010546e:	68 00 76 10 80       	push   $0x80107600
80105473:	e8 93 b1 ff ff       	call   8010060b <cprintf>
    myproc()->killed = 1;
80105478:	83 c4 20             	add    $0x20,%esp
8010547b:	e8 d7 dd ff ff       	call   80103257 <myproc>
80105480:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80105487:	e9 96 fe ff ff       	jmp    80105322 <trap+0x121>
8010548c:	0f 20 d7             	mov    %cr2,%edi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
8010548f:	8b 73 38             	mov    0x38(%ebx),%esi
80105492:	e8 a5 dd ff ff       	call   8010323c <cpuid>
80105497:	83 ec 0c             	sub    $0xc,%esp
8010549a:	57                   	push   %edi
8010549b:	56                   	push   %esi
8010549c:	50                   	push   %eax
8010549d:	ff 73 30             	pushl  0x30(%ebx)
801054a0:	68 cc 75 10 80       	push   $0x801075cc
801054a5:	e8 61 b1 ff ff       	call   8010060b <cprintf>
      panic("trap");
801054aa:	83 c4 14             	add    $0x14,%esp
801054ad:	68 a2 75 10 80       	push   $0x801075a2
801054b2:	e8 91 ae ff ff       	call   80100348 <panic>
    exit();
801054b7:	e8 dc e0 ff ff       	call   80103598 <exit>
801054bc:	e9 86 fe ff ff       	jmp    80105347 <trap+0x146>
  if (myproc() && myproc()->state == RUNNING &&
801054c1:	83 7b 30 20          	cmpl   $0x20,0x30(%ebx)
801054c5:	0f 85 94 fe ff ff    	jne    8010535f <trap+0x15e>
    yield();
801054cb:	e8 91 e1 ff ff       	call   80103661 <yield>
801054d0:	e9 8a fe ff ff       	jmp    8010535f <trap+0x15e>
    exit();
801054d5:	e8 be e0 ff ff       	call   80103598 <exit>
801054da:	e9 a5 fe ff ff       	jmp    80105384 <trap+0x183>

801054df <uartgetc>:
  outb(COM1+0, c);
}

static int
uartgetc(void)
{
801054df:	55                   	push   %ebp
801054e0:	89 e5                	mov    %esp,%ebp
  if(!uart)
801054e2:	83 3d c0 a5 10 80 00 	cmpl   $0x0,0x8010a5c0
801054e9:	74 15                	je     80105500 <uartgetc+0x21>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801054eb:	ba fd 03 00 00       	mov    $0x3fd,%edx
801054f0:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
801054f1:	a8 01                	test   $0x1,%al
801054f3:	74 12                	je     80105507 <uartgetc+0x28>
801054f5:	ba f8 03 00 00       	mov    $0x3f8,%edx
801054fa:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
801054fb:	0f b6 c0             	movzbl %al,%eax
}
801054fe:	5d                   	pop    %ebp
801054ff:	c3                   	ret    
    return -1;
80105500:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105505:	eb f7                	jmp    801054fe <uartgetc+0x1f>
    return -1;
80105507:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010550c:	eb f0                	jmp    801054fe <uartgetc+0x1f>

8010550e <uartputc>:
  if(!uart)
8010550e:	83 3d c0 a5 10 80 00 	cmpl   $0x0,0x8010a5c0
80105515:	74 3b                	je     80105552 <uartputc+0x44>
{
80105517:	55                   	push   %ebp
80105518:	89 e5                	mov    %esp,%ebp
8010551a:	53                   	push   %ebx
8010551b:	83 ec 04             	sub    $0x4,%esp
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
8010551e:	bb 00 00 00 00       	mov    $0x0,%ebx
80105523:	eb 10                	jmp    80105535 <uartputc+0x27>
    microdelay(10);
80105525:	83 ec 0c             	sub    $0xc,%esp
80105528:	6a 0a                	push   $0xa
8010552a:	e8 ad ce ff ff       	call   801023dc <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
8010552f:	83 c3 01             	add    $0x1,%ebx
80105532:	83 c4 10             	add    $0x10,%esp
80105535:	83 fb 7f             	cmp    $0x7f,%ebx
80105538:	7f 0a                	jg     80105544 <uartputc+0x36>
8010553a:	ba fd 03 00 00       	mov    $0x3fd,%edx
8010553f:	ec                   	in     (%dx),%al
80105540:	a8 20                	test   $0x20,%al
80105542:	74 e1                	je     80105525 <uartputc+0x17>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80105544:	8b 45 08             	mov    0x8(%ebp),%eax
80105547:	ba f8 03 00 00       	mov    $0x3f8,%edx
8010554c:	ee                   	out    %al,(%dx)
}
8010554d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105550:	c9                   	leave  
80105551:	c3                   	ret    
80105552:	f3 c3                	repz ret 

80105554 <uartinit>:
{
80105554:	55                   	push   %ebp
80105555:	89 e5                	mov    %esp,%ebp
80105557:	56                   	push   %esi
80105558:	53                   	push   %ebx
80105559:	b9 00 00 00 00       	mov    $0x0,%ecx
8010555e:	ba fa 03 00 00       	mov    $0x3fa,%edx
80105563:	89 c8                	mov    %ecx,%eax
80105565:	ee                   	out    %al,(%dx)
80105566:	be fb 03 00 00       	mov    $0x3fb,%esi
8010556b:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105570:	89 f2                	mov    %esi,%edx
80105572:	ee                   	out    %al,(%dx)
80105573:	b8 0c 00 00 00       	mov    $0xc,%eax
80105578:	ba f8 03 00 00       	mov    $0x3f8,%edx
8010557d:	ee                   	out    %al,(%dx)
8010557e:	bb f9 03 00 00       	mov    $0x3f9,%ebx
80105583:	89 c8                	mov    %ecx,%eax
80105585:	89 da                	mov    %ebx,%edx
80105587:	ee                   	out    %al,(%dx)
80105588:	b8 03 00 00 00       	mov    $0x3,%eax
8010558d:	89 f2                	mov    %esi,%edx
8010558f:	ee                   	out    %al,(%dx)
80105590:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105595:	89 c8                	mov    %ecx,%eax
80105597:	ee                   	out    %al,(%dx)
80105598:	b8 01 00 00 00       	mov    $0x1,%eax
8010559d:	89 da                	mov    %ebx,%edx
8010559f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801055a0:	ba fd 03 00 00       	mov    $0x3fd,%edx
801055a5:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
801055a6:	3c ff                	cmp    $0xff,%al
801055a8:	74 45                	je     801055ef <uartinit+0x9b>
  uart = 1;
801055aa:	c7 05 c0 a5 10 80 01 	movl   $0x1,0x8010a5c0
801055b1:	00 00 00 
801055b4:	ba fa 03 00 00       	mov    $0x3fa,%edx
801055b9:	ec                   	in     (%dx),%al
801055ba:	ba f8 03 00 00       	mov    $0x3f8,%edx
801055bf:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
801055c0:	83 ec 08             	sub    $0x8,%esp
801055c3:	6a 00                	push   $0x0
801055c5:	6a 04                	push   $0x4
801055c7:	e8 a0 c9 ff ff       	call   80101f6c <ioapicenable>
  for(p="xv6...\n"; *p; p++)
801055cc:	83 c4 10             	add    $0x10,%esp
801055cf:	bb 43 76 10 80       	mov    $0x80107643,%ebx
801055d4:	eb 12                	jmp    801055e8 <uartinit+0x94>
    uartputc(*p);
801055d6:	83 ec 0c             	sub    $0xc,%esp
801055d9:	0f be c0             	movsbl %al,%eax
801055dc:	50                   	push   %eax
801055dd:	e8 2c ff ff ff       	call   8010550e <uartputc>
  for(p="xv6...\n"; *p; p++)
801055e2:	83 c3 01             	add    $0x1,%ebx
801055e5:	83 c4 10             	add    $0x10,%esp
801055e8:	0f b6 03             	movzbl (%ebx),%eax
801055eb:	84 c0                	test   %al,%al
801055ed:	75 e7                	jne    801055d6 <uartinit+0x82>
}
801055ef:	8d 65 f8             	lea    -0x8(%ebp),%esp
801055f2:	5b                   	pop    %ebx
801055f3:	5e                   	pop    %esi
801055f4:	5d                   	pop    %ebp
801055f5:	c3                   	ret    

801055f6 <uartintr>:

void
uartintr(void)
{
801055f6:	55                   	push   %ebp
801055f7:	89 e5                	mov    %esp,%ebp
801055f9:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
801055fc:	68 df 54 10 80       	push   $0x801054df
80105601:	e8 38 b1 ff ff       	call   8010073e <consoleintr>
}
80105606:	83 c4 10             	add    $0x10,%esp
80105609:	c9                   	leave  
8010560a:	c3                   	ret    

8010560b <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
8010560b:	6a 00                	push   $0x0
  pushl $0
8010560d:	6a 00                	push   $0x0
  jmp alltraps
8010560f:	e9 f5 fa ff ff       	jmp    80105109 <alltraps>

80105614 <vector1>:
.globl vector1
vector1:
  pushl $0
80105614:	6a 00                	push   $0x0
  pushl $1
80105616:	6a 01                	push   $0x1
  jmp alltraps
80105618:	e9 ec fa ff ff       	jmp    80105109 <alltraps>

8010561d <vector2>:
.globl vector2
vector2:
  pushl $0
8010561d:	6a 00                	push   $0x0
  pushl $2
8010561f:	6a 02                	push   $0x2
  jmp alltraps
80105621:	e9 e3 fa ff ff       	jmp    80105109 <alltraps>

80105626 <vector3>:
.globl vector3
vector3:
  pushl $0
80105626:	6a 00                	push   $0x0
  pushl $3
80105628:	6a 03                	push   $0x3
  jmp alltraps
8010562a:	e9 da fa ff ff       	jmp    80105109 <alltraps>

8010562f <vector4>:
.globl vector4
vector4:
  pushl $0
8010562f:	6a 00                	push   $0x0
  pushl $4
80105631:	6a 04                	push   $0x4
  jmp alltraps
80105633:	e9 d1 fa ff ff       	jmp    80105109 <alltraps>

80105638 <vector5>:
.globl vector5
vector5:
  pushl $0
80105638:	6a 00                	push   $0x0
  pushl $5
8010563a:	6a 05                	push   $0x5
  jmp alltraps
8010563c:	e9 c8 fa ff ff       	jmp    80105109 <alltraps>

80105641 <vector6>:
.globl vector6
vector6:
  pushl $0
80105641:	6a 00                	push   $0x0
  pushl $6
80105643:	6a 06                	push   $0x6
  jmp alltraps
80105645:	e9 bf fa ff ff       	jmp    80105109 <alltraps>

8010564a <vector7>:
.globl vector7
vector7:
  pushl $0
8010564a:	6a 00                	push   $0x0
  pushl $7
8010564c:	6a 07                	push   $0x7
  jmp alltraps
8010564e:	e9 b6 fa ff ff       	jmp    80105109 <alltraps>

80105653 <vector8>:
.globl vector8
vector8:
  pushl $8
80105653:	6a 08                	push   $0x8
  jmp alltraps
80105655:	e9 af fa ff ff       	jmp    80105109 <alltraps>

8010565a <vector9>:
.globl vector9
vector9:
  pushl $0
8010565a:	6a 00                	push   $0x0
  pushl $9
8010565c:	6a 09                	push   $0x9
  jmp alltraps
8010565e:	e9 a6 fa ff ff       	jmp    80105109 <alltraps>

80105663 <vector10>:
.globl vector10
vector10:
  pushl $10
80105663:	6a 0a                	push   $0xa
  jmp alltraps
80105665:	e9 9f fa ff ff       	jmp    80105109 <alltraps>

8010566a <vector11>:
.globl vector11
vector11:
  pushl $11
8010566a:	6a 0b                	push   $0xb
  jmp alltraps
8010566c:	e9 98 fa ff ff       	jmp    80105109 <alltraps>

80105671 <vector12>:
.globl vector12
vector12:
  pushl $12
80105671:	6a 0c                	push   $0xc
  jmp alltraps
80105673:	e9 91 fa ff ff       	jmp    80105109 <alltraps>

80105678 <vector13>:
.globl vector13
vector13:
  pushl $13
80105678:	6a 0d                	push   $0xd
  jmp alltraps
8010567a:	e9 8a fa ff ff       	jmp    80105109 <alltraps>

8010567f <vector14>:
.globl vector14
vector14:
  pushl $14
8010567f:	6a 0e                	push   $0xe
  jmp alltraps
80105681:	e9 83 fa ff ff       	jmp    80105109 <alltraps>

80105686 <vector15>:
.globl vector15
vector15:
  pushl $0
80105686:	6a 00                	push   $0x0
  pushl $15
80105688:	6a 0f                	push   $0xf
  jmp alltraps
8010568a:	e9 7a fa ff ff       	jmp    80105109 <alltraps>

8010568f <vector16>:
.globl vector16
vector16:
  pushl $0
8010568f:	6a 00                	push   $0x0
  pushl $16
80105691:	6a 10                	push   $0x10
  jmp alltraps
80105693:	e9 71 fa ff ff       	jmp    80105109 <alltraps>

80105698 <vector17>:
.globl vector17
vector17:
  pushl $17
80105698:	6a 11                	push   $0x11
  jmp alltraps
8010569a:	e9 6a fa ff ff       	jmp    80105109 <alltraps>

8010569f <vector18>:
.globl vector18
vector18:
  pushl $0
8010569f:	6a 00                	push   $0x0
  pushl $18
801056a1:	6a 12                	push   $0x12
  jmp alltraps
801056a3:	e9 61 fa ff ff       	jmp    80105109 <alltraps>

801056a8 <vector19>:
.globl vector19
vector19:
  pushl $0
801056a8:	6a 00                	push   $0x0
  pushl $19
801056aa:	6a 13                	push   $0x13
  jmp alltraps
801056ac:	e9 58 fa ff ff       	jmp    80105109 <alltraps>

801056b1 <vector20>:
.globl vector20
vector20:
  pushl $0
801056b1:	6a 00                	push   $0x0
  pushl $20
801056b3:	6a 14                	push   $0x14
  jmp alltraps
801056b5:	e9 4f fa ff ff       	jmp    80105109 <alltraps>

801056ba <vector21>:
.globl vector21
vector21:
  pushl $0
801056ba:	6a 00                	push   $0x0
  pushl $21
801056bc:	6a 15                	push   $0x15
  jmp alltraps
801056be:	e9 46 fa ff ff       	jmp    80105109 <alltraps>

801056c3 <vector22>:
.globl vector22
vector22:
  pushl $0
801056c3:	6a 00                	push   $0x0
  pushl $22
801056c5:	6a 16                	push   $0x16
  jmp alltraps
801056c7:	e9 3d fa ff ff       	jmp    80105109 <alltraps>

801056cc <vector23>:
.globl vector23
vector23:
  pushl $0
801056cc:	6a 00                	push   $0x0
  pushl $23
801056ce:	6a 17                	push   $0x17
  jmp alltraps
801056d0:	e9 34 fa ff ff       	jmp    80105109 <alltraps>

801056d5 <vector24>:
.globl vector24
vector24:
  pushl $0
801056d5:	6a 00                	push   $0x0
  pushl $24
801056d7:	6a 18                	push   $0x18
  jmp alltraps
801056d9:	e9 2b fa ff ff       	jmp    80105109 <alltraps>

801056de <vector25>:
.globl vector25
vector25:
  pushl $0
801056de:	6a 00                	push   $0x0
  pushl $25
801056e0:	6a 19                	push   $0x19
  jmp alltraps
801056e2:	e9 22 fa ff ff       	jmp    80105109 <alltraps>

801056e7 <vector26>:
.globl vector26
vector26:
  pushl $0
801056e7:	6a 00                	push   $0x0
  pushl $26
801056e9:	6a 1a                	push   $0x1a
  jmp alltraps
801056eb:	e9 19 fa ff ff       	jmp    80105109 <alltraps>

801056f0 <vector27>:
.globl vector27
vector27:
  pushl $0
801056f0:	6a 00                	push   $0x0
  pushl $27
801056f2:	6a 1b                	push   $0x1b
  jmp alltraps
801056f4:	e9 10 fa ff ff       	jmp    80105109 <alltraps>

801056f9 <vector28>:
.globl vector28
vector28:
  pushl $0
801056f9:	6a 00                	push   $0x0
  pushl $28
801056fb:	6a 1c                	push   $0x1c
  jmp alltraps
801056fd:	e9 07 fa ff ff       	jmp    80105109 <alltraps>

80105702 <vector29>:
.globl vector29
vector29:
  pushl $0
80105702:	6a 00                	push   $0x0
  pushl $29
80105704:	6a 1d                	push   $0x1d
  jmp alltraps
80105706:	e9 fe f9 ff ff       	jmp    80105109 <alltraps>

8010570b <vector30>:
.globl vector30
vector30:
  pushl $0
8010570b:	6a 00                	push   $0x0
  pushl $30
8010570d:	6a 1e                	push   $0x1e
  jmp alltraps
8010570f:	e9 f5 f9 ff ff       	jmp    80105109 <alltraps>

80105714 <vector31>:
.globl vector31
vector31:
  pushl $0
80105714:	6a 00                	push   $0x0
  pushl $31
80105716:	6a 1f                	push   $0x1f
  jmp alltraps
80105718:	e9 ec f9 ff ff       	jmp    80105109 <alltraps>

8010571d <vector32>:
.globl vector32
vector32:
  pushl $0
8010571d:	6a 00                	push   $0x0
  pushl $32
8010571f:	6a 20                	push   $0x20
  jmp alltraps
80105721:	e9 e3 f9 ff ff       	jmp    80105109 <alltraps>

80105726 <vector33>:
.globl vector33
vector33:
  pushl $0
80105726:	6a 00                	push   $0x0
  pushl $33
80105728:	6a 21                	push   $0x21
  jmp alltraps
8010572a:	e9 da f9 ff ff       	jmp    80105109 <alltraps>

8010572f <vector34>:
.globl vector34
vector34:
  pushl $0
8010572f:	6a 00                	push   $0x0
  pushl $34
80105731:	6a 22                	push   $0x22
  jmp alltraps
80105733:	e9 d1 f9 ff ff       	jmp    80105109 <alltraps>

80105738 <vector35>:
.globl vector35
vector35:
  pushl $0
80105738:	6a 00                	push   $0x0
  pushl $35
8010573a:	6a 23                	push   $0x23
  jmp alltraps
8010573c:	e9 c8 f9 ff ff       	jmp    80105109 <alltraps>

80105741 <vector36>:
.globl vector36
vector36:
  pushl $0
80105741:	6a 00                	push   $0x0
  pushl $36
80105743:	6a 24                	push   $0x24
  jmp alltraps
80105745:	e9 bf f9 ff ff       	jmp    80105109 <alltraps>

8010574a <vector37>:
.globl vector37
vector37:
  pushl $0
8010574a:	6a 00                	push   $0x0
  pushl $37
8010574c:	6a 25                	push   $0x25
  jmp alltraps
8010574e:	e9 b6 f9 ff ff       	jmp    80105109 <alltraps>

80105753 <vector38>:
.globl vector38
vector38:
  pushl $0
80105753:	6a 00                	push   $0x0
  pushl $38
80105755:	6a 26                	push   $0x26
  jmp alltraps
80105757:	e9 ad f9 ff ff       	jmp    80105109 <alltraps>

8010575c <vector39>:
.globl vector39
vector39:
  pushl $0
8010575c:	6a 00                	push   $0x0
  pushl $39
8010575e:	6a 27                	push   $0x27
  jmp alltraps
80105760:	e9 a4 f9 ff ff       	jmp    80105109 <alltraps>

80105765 <vector40>:
.globl vector40
vector40:
  pushl $0
80105765:	6a 00                	push   $0x0
  pushl $40
80105767:	6a 28                	push   $0x28
  jmp alltraps
80105769:	e9 9b f9 ff ff       	jmp    80105109 <alltraps>

8010576e <vector41>:
.globl vector41
vector41:
  pushl $0
8010576e:	6a 00                	push   $0x0
  pushl $41
80105770:	6a 29                	push   $0x29
  jmp alltraps
80105772:	e9 92 f9 ff ff       	jmp    80105109 <alltraps>

80105777 <vector42>:
.globl vector42
vector42:
  pushl $0
80105777:	6a 00                	push   $0x0
  pushl $42
80105779:	6a 2a                	push   $0x2a
  jmp alltraps
8010577b:	e9 89 f9 ff ff       	jmp    80105109 <alltraps>

80105780 <vector43>:
.globl vector43
vector43:
  pushl $0
80105780:	6a 00                	push   $0x0
  pushl $43
80105782:	6a 2b                	push   $0x2b
  jmp alltraps
80105784:	e9 80 f9 ff ff       	jmp    80105109 <alltraps>

80105789 <vector44>:
.globl vector44
vector44:
  pushl $0
80105789:	6a 00                	push   $0x0
  pushl $44
8010578b:	6a 2c                	push   $0x2c
  jmp alltraps
8010578d:	e9 77 f9 ff ff       	jmp    80105109 <alltraps>

80105792 <vector45>:
.globl vector45
vector45:
  pushl $0
80105792:	6a 00                	push   $0x0
  pushl $45
80105794:	6a 2d                	push   $0x2d
  jmp alltraps
80105796:	e9 6e f9 ff ff       	jmp    80105109 <alltraps>

8010579b <vector46>:
.globl vector46
vector46:
  pushl $0
8010579b:	6a 00                	push   $0x0
  pushl $46
8010579d:	6a 2e                	push   $0x2e
  jmp alltraps
8010579f:	e9 65 f9 ff ff       	jmp    80105109 <alltraps>

801057a4 <vector47>:
.globl vector47
vector47:
  pushl $0
801057a4:	6a 00                	push   $0x0
  pushl $47
801057a6:	6a 2f                	push   $0x2f
  jmp alltraps
801057a8:	e9 5c f9 ff ff       	jmp    80105109 <alltraps>

801057ad <vector48>:
.globl vector48
vector48:
  pushl $0
801057ad:	6a 00                	push   $0x0
  pushl $48
801057af:	6a 30                	push   $0x30
  jmp alltraps
801057b1:	e9 53 f9 ff ff       	jmp    80105109 <alltraps>

801057b6 <vector49>:
.globl vector49
vector49:
  pushl $0
801057b6:	6a 00                	push   $0x0
  pushl $49
801057b8:	6a 31                	push   $0x31
  jmp alltraps
801057ba:	e9 4a f9 ff ff       	jmp    80105109 <alltraps>

801057bf <vector50>:
.globl vector50
vector50:
  pushl $0
801057bf:	6a 00                	push   $0x0
  pushl $50
801057c1:	6a 32                	push   $0x32
  jmp alltraps
801057c3:	e9 41 f9 ff ff       	jmp    80105109 <alltraps>

801057c8 <vector51>:
.globl vector51
vector51:
  pushl $0
801057c8:	6a 00                	push   $0x0
  pushl $51
801057ca:	6a 33                	push   $0x33
  jmp alltraps
801057cc:	e9 38 f9 ff ff       	jmp    80105109 <alltraps>

801057d1 <vector52>:
.globl vector52
vector52:
  pushl $0
801057d1:	6a 00                	push   $0x0
  pushl $52
801057d3:	6a 34                	push   $0x34
  jmp alltraps
801057d5:	e9 2f f9 ff ff       	jmp    80105109 <alltraps>

801057da <vector53>:
.globl vector53
vector53:
  pushl $0
801057da:	6a 00                	push   $0x0
  pushl $53
801057dc:	6a 35                	push   $0x35
  jmp alltraps
801057de:	e9 26 f9 ff ff       	jmp    80105109 <alltraps>

801057e3 <vector54>:
.globl vector54
vector54:
  pushl $0
801057e3:	6a 00                	push   $0x0
  pushl $54
801057e5:	6a 36                	push   $0x36
  jmp alltraps
801057e7:	e9 1d f9 ff ff       	jmp    80105109 <alltraps>

801057ec <vector55>:
.globl vector55
vector55:
  pushl $0
801057ec:	6a 00                	push   $0x0
  pushl $55
801057ee:	6a 37                	push   $0x37
  jmp alltraps
801057f0:	e9 14 f9 ff ff       	jmp    80105109 <alltraps>

801057f5 <vector56>:
.globl vector56
vector56:
  pushl $0
801057f5:	6a 00                	push   $0x0
  pushl $56
801057f7:	6a 38                	push   $0x38
  jmp alltraps
801057f9:	e9 0b f9 ff ff       	jmp    80105109 <alltraps>

801057fe <vector57>:
.globl vector57
vector57:
  pushl $0
801057fe:	6a 00                	push   $0x0
  pushl $57
80105800:	6a 39                	push   $0x39
  jmp alltraps
80105802:	e9 02 f9 ff ff       	jmp    80105109 <alltraps>

80105807 <vector58>:
.globl vector58
vector58:
  pushl $0
80105807:	6a 00                	push   $0x0
  pushl $58
80105809:	6a 3a                	push   $0x3a
  jmp alltraps
8010580b:	e9 f9 f8 ff ff       	jmp    80105109 <alltraps>

80105810 <vector59>:
.globl vector59
vector59:
  pushl $0
80105810:	6a 00                	push   $0x0
  pushl $59
80105812:	6a 3b                	push   $0x3b
  jmp alltraps
80105814:	e9 f0 f8 ff ff       	jmp    80105109 <alltraps>

80105819 <vector60>:
.globl vector60
vector60:
  pushl $0
80105819:	6a 00                	push   $0x0
  pushl $60
8010581b:	6a 3c                	push   $0x3c
  jmp alltraps
8010581d:	e9 e7 f8 ff ff       	jmp    80105109 <alltraps>

80105822 <vector61>:
.globl vector61
vector61:
  pushl $0
80105822:	6a 00                	push   $0x0
  pushl $61
80105824:	6a 3d                	push   $0x3d
  jmp alltraps
80105826:	e9 de f8 ff ff       	jmp    80105109 <alltraps>

8010582b <vector62>:
.globl vector62
vector62:
  pushl $0
8010582b:	6a 00                	push   $0x0
  pushl $62
8010582d:	6a 3e                	push   $0x3e
  jmp alltraps
8010582f:	e9 d5 f8 ff ff       	jmp    80105109 <alltraps>

80105834 <vector63>:
.globl vector63
vector63:
  pushl $0
80105834:	6a 00                	push   $0x0
  pushl $63
80105836:	6a 3f                	push   $0x3f
  jmp alltraps
80105838:	e9 cc f8 ff ff       	jmp    80105109 <alltraps>

8010583d <vector64>:
.globl vector64
vector64:
  pushl $0
8010583d:	6a 00                	push   $0x0
  pushl $64
8010583f:	6a 40                	push   $0x40
  jmp alltraps
80105841:	e9 c3 f8 ff ff       	jmp    80105109 <alltraps>

80105846 <vector65>:
.globl vector65
vector65:
  pushl $0
80105846:	6a 00                	push   $0x0
  pushl $65
80105848:	6a 41                	push   $0x41
  jmp alltraps
8010584a:	e9 ba f8 ff ff       	jmp    80105109 <alltraps>

8010584f <vector66>:
.globl vector66
vector66:
  pushl $0
8010584f:	6a 00                	push   $0x0
  pushl $66
80105851:	6a 42                	push   $0x42
  jmp alltraps
80105853:	e9 b1 f8 ff ff       	jmp    80105109 <alltraps>

80105858 <vector67>:
.globl vector67
vector67:
  pushl $0
80105858:	6a 00                	push   $0x0
  pushl $67
8010585a:	6a 43                	push   $0x43
  jmp alltraps
8010585c:	e9 a8 f8 ff ff       	jmp    80105109 <alltraps>

80105861 <vector68>:
.globl vector68
vector68:
  pushl $0
80105861:	6a 00                	push   $0x0
  pushl $68
80105863:	6a 44                	push   $0x44
  jmp alltraps
80105865:	e9 9f f8 ff ff       	jmp    80105109 <alltraps>

8010586a <vector69>:
.globl vector69
vector69:
  pushl $0
8010586a:	6a 00                	push   $0x0
  pushl $69
8010586c:	6a 45                	push   $0x45
  jmp alltraps
8010586e:	e9 96 f8 ff ff       	jmp    80105109 <alltraps>

80105873 <vector70>:
.globl vector70
vector70:
  pushl $0
80105873:	6a 00                	push   $0x0
  pushl $70
80105875:	6a 46                	push   $0x46
  jmp alltraps
80105877:	e9 8d f8 ff ff       	jmp    80105109 <alltraps>

8010587c <vector71>:
.globl vector71
vector71:
  pushl $0
8010587c:	6a 00                	push   $0x0
  pushl $71
8010587e:	6a 47                	push   $0x47
  jmp alltraps
80105880:	e9 84 f8 ff ff       	jmp    80105109 <alltraps>

80105885 <vector72>:
.globl vector72
vector72:
  pushl $0
80105885:	6a 00                	push   $0x0
  pushl $72
80105887:	6a 48                	push   $0x48
  jmp alltraps
80105889:	e9 7b f8 ff ff       	jmp    80105109 <alltraps>

8010588e <vector73>:
.globl vector73
vector73:
  pushl $0
8010588e:	6a 00                	push   $0x0
  pushl $73
80105890:	6a 49                	push   $0x49
  jmp alltraps
80105892:	e9 72 f8 ff ff       	jmp    80105109 <alltraps>

80105897 <vector74>:
.globl vector74
vector74:
  pushl $0
80105897:	6a 00                	push   $0x0
  pushl $74
80105899:	6a 4a                	push   $0x4a
  jmp alltraps
8010589b:	e9 69 f8 ff ff       	jmp    80105109 <alltraps>

801058a0 <vector75>:
.globl vector75
vector75:
  pushl $0
801058a0:	6a 00                	push   $0x0
  pushl $75
801058a2:	6a 4b                	push   $0x4b
  jmp alltraps
801058a4:	e9 60 f8 ff ff       	jmp    80105109 <alltraps>

801058a9 <vector76>:
.globl vector76
vector76:
  pushl $0
801058a9:	6a 00                	push   $0x0
  pushl $76
801058ab:	6a 4c                	push   $0x4c
  jmp alltraps
801058ad:	e9 57 f8 ff ff       	jmp    80105109 <alltraps>

801058b2 <vector77>:
.globl vector77
vector77:
  pushl $0
801058b2:	6a 00                	push   $0x0
  pushl $77
801058b4:	6a 4d                	push   $0x4d
  jmp alltraps
801058b6:	e9 4e f8 ff ff       	jmp    80105109 <alltraps>

801058bb <vector78>:
.globl vector78
vector78:
  pushl $0
801058bb:	6a 00                	push   $0x0
  pushl $78
801058bd:	6a 4e                	push   $0x4e
  jmp alltraps
801058bf:	e9 45 f8 ff ff       	jmp    80105109 <alltraps>

801058c4 <vector79>:
.globl vector79
vector79:
  pushl $0
801058c4:	6a 00                	push   $0x0
  pushl $79
801058c6:	6a 4f                	push   $0x4f
  jmp alltraps
801058c8:	e9 3c f8 ff ff       	jmp    80105109 <alltraps>

801058cd <vector80>:
.globl vector80
vector80:
  pushl $0
801058cd:	6a 00                	push   $0x0
  pushl $80
801058cf:	6a 50                	push   $0x50
  jmp alltraps
801058d1:	e9 33 f8 ff ff       	jmp    80105109 <alltraps>

801058d6 <vector81>:
.globl vector81
vector81:
  pushl $0
801058d6:	6a 00                	push   $0x0
  pushl $81
801058d8:	6a 51                	push   $0x51
  jmp alltraps
801058da:	e9 2a f8 ff ff       	jmp    80105109 <alltraps>

801058df <vector82>:
.globl vector82
vector82:
  pushl $0
801058df:	6a 00                	push   $0x0
  pushl $82
801058e1:	6a 52                	push   $0x52
  jmp alltraps
801058e3:	e9 21 f8 ff ff       	jmp    80105109 <alltraps>

801058e8 <vector83>:
.globl vector83
vector83:
  pushl $0
801058e8:	6a 00                	push   $0x0
  pushl $83
801058ea:	6a 53                	push   $0x53
  jmp alltraps
801058ec:	e9 18 f8 ff ff       	jmp    80105109 <alltraps>

801058f1 <vector84>:
.globl vector84
vector84:
  pushl $0
801058f1:	6a 00                	push   $0x0
  pushl $84
801058f3:	6a 54                	push   $0x54
  jmp alltraps
801058f5:	e9 0f f8 ff ff       	jmp    80105109 <alltraps>

801058fa <vector85>:
.globl vector85
vector85:
  pushl $0
801058fa:	6a 00                	push   $0x0
  pushl $85
801058fc:	6a 55                	push   $0x55
  jmp alltraps
801058fe:	e9 06 f8 ff ff       	jmp    80105109 <alltraps>

80105903 <vector86>:
.globl vector86
vector86:
  pushl $0
80105903:	6a 00                	push   $0x0
  pushl $86
80105905:	6a 56                	push   $0x56
  jmp alltraps
80105907:	e9 fd f7 ff ff       	jmp    80105109 <alltraps>

8010590c <vector87>:
.globl vector87
vector87:
  pushl $0
8010590c:	6a 00                	push   $0x0
  pushl $87
8010590e:	6a 57                	push   $0x57
  jmp alltraps
80105910:	e9 f4 f7 ff ff       	jmp    80105109 <alltraps>

80105915 <vector88>:
.globl vector88
vector88:
  pushl $0
80105915:	6a 00                	push   $0x0
  pushl $88
80105917:	6a 58                	push   $0x58
  jmp alltraps
80105919:	e9 eb f7 ff ff       	jmp    80105109 <alltraps>

8010591e <vector89>:
.globl vector89
vector89:
  pushl $0
8010591e:	6a 00                	push   $0x0
  pushl $89
80105920:	6a 59                	push   $0x59
  jmp alltraps
80105922:	e9 e2 f7 ff ff       	jmp    80105109 <alltraps>

80105927 <vector90>:
.globl vector90
vector90:
  pushl $0
80105927:	6a 00                	push   $0x0
  pushl $90
80105929:	6a 5a                	push   $0x5a
  jmp alltraps
8010592b:	e9 d9 f7 ff ff       	jmp    80105109 <alltraps>

80105930 <vector91>:
.globl vector91
vector91:
  pushl $0
80105930:	6a 00                	push   $0x0
  pushl $91
80105932:	6a 5b                	push   $0x5b
  jmp alltraps
80105934:	e9 d0 f7 ff ff       	jmp    80105109 <alltraps>

80105939 <vector92>:
.globl vector92
vector92:
  pushl $0
80105939:	6a 00                	push   $0x0
  pushl $92
8010593b:	6a 5c                	push   $0x5c
  jmp alltraps
8010593d:	e9 c7 f7 ff ff       	jmp    80105109 <alltraps>

80105942 <vector93>:
.globl vector93
vector93:
  pushl $0
80105942:	6a 00                	push   $0x0
  pushl $93
80105944:	6a 5d                	push   $0x5d
  jmp alltraps
80105946:	e9 be f7 ff ff       	jmp    80105109 <alltraps>

8010594b <vector94>:
.globl vector94
vector94:
  pushl $0
8010594b:	6a 00                	push   $0x0
  pushl $94
8010594d:	6a 5e                	push   $0x5e
  jmp alltraps
8010594f:	e9 b5 f7 ff ff       	jmp    80105109 <alltraps>

80105954 <vector95>:
.globl vector95
vector95:
  pushl $0
80105954:	6a 00                	push   $0x0
  pushl $95
80105956:	6a 5f                	push   $0x5f
  jmp alltraps
80105958:	e9 ac f7 ff ff       	jmp    80105109 <alltraps>

8010595d <vector96>:
.globl vector96
vector96:
  pushl $0
8010595d:	6a 00                	push   $0x0
  pushl $96
8010595f:	6a 60                	push   $0x60
  jmp alltraps
80105961:	e9 a3 f7 ff ff       	jmp    80105109 <alltraps>

80105966 <vector97>:
.globl vector97
vector97:
  pushl $0
80105966:	6a 00                	push   $0x0
  pushl $97
80105968:	6a 61                	push   $0x61
  jmp alltraps
8010596a:	e9 9a f7 ff ff       	jmp    80105109 <alltraps>

8010596f <vector98>:
.globl vector98
vector98:
  pushl $0
8010596f:	6a 00                	push   $0x0
  pushl $98
80105971:	6a 62                	push   $0x62
  jmp alltraps
80105973:	e9 91 f7 ff ff       	jmp    80105109 <alltraps>

80105978 <vector99>:
.globl vector99
vector99:
  pushl $0
80105978:	6a 00                	push   $0x0
  pushl $99
8010597a:	6a 63                	push   $0x63
  jmp alltraps
8010597c:	e9 88 f7 ff ff       	jmp    80105109 <alltraps>

80105981 <vector100>:
.globl vector100
vector100:
  pushl $0
80105981:	6a 00                	push   $0x0
  pushl $100
80105983:	6a 64                	push   $0x64
  jmp alltraps
80105985:	e9 7f f7 ff ff       	jmp    80105109 <alltraps>

8010598a <vector101>:
.globl vector101
vector101:
  pushl $0
8010598a:	6a 00                	push   $0x0
  pushl $101
8010598c:	6a 65                	push   $0x65
  jmp alltraps
8010598e:	e9 76 f7 ff ff       	jmp    80105109 <alltraps>

80105993 <vector102>:
.globl vector102
vector102:
  pushl $0
80105993:	6a 00                	push   $0x0
  pushl $102
80105995:	6a 66                	push   $0x66
  jmp alltraps
80105997:	e9 6d f7 ff ff       	jmp    80105109 <alltraps>

8010599c <vector103>:
.globl vector103
vector103:
  pushl $0
8010599c:	6a 00                	push   $0x0
  pushl $103
8010599e:	6a 67                	push   $0x67
  jmp alltraps
801059a0:	e9 64 f7 ff ff       	jmp    80105109 <alltraps>

801059a5 <vector104>:
.globl vector104
vector104:
  pushl $0
801059a5:	6a 00                	push   $0x0
  pushl $104
801059a7:	6a 68                	push   $0x68
  jmp alltraps
801059a9:	e9 5b f7 ff ff       	jmp    80105109 <alltraps>

801059ae <vector105>:
.globl vector105
vector105:
  pushl $0
801059ae:	6a 00                	push   $0x0
  pushl $105
801059b0:	6a 69                	push   $0x69
  jmp alltraps
801059b2:	e9 52 f7 ff ff       	jmp    80105109 <alltraps>

801059b7 <vector106>:
.globl vector106
vector106:
  pushl $0
801059b7:	6a 00                	push   $0x0
  pushl $106
801059b9:	6a 6a                	push   $0x6a
  jmp alltraps
801059bb:	e9 49 f7 ff ff       	jmp    80105109 <alltraps>

801059c0 <vector107>:
.globl vector107
vector107:
  pushl $0
801059c0:	6a 00                	push   $0x0
  pushl $107
801059c2:	6a 6b                	push   $0x6b
  jmp alltraps
801059c4:	e9 40 f7 ff ff       	jmp    80105109 <alltraps>

801059c9 <vector108>:
.globl vector108
vector108:
  pushl $0
801059c9:	6a 00                	push   $0x0
  pushl $108
801059cb:	6a 6c                	push   $0x6c
  jmp alltraps
801059cd:	e9 37 f7 ff ff       	jmp    80105109 <alltraps>

801059d2 <vector109>:
.globl vector109
vector109:
  pushl $0
801059d2:	6a 00                	push   $0x0
  pushl $109
801059d4:	6a 6d                	push   $0x6d
  jmp alltraps
801059d6:	e9 2e f7 ff ff       	jmp    80105109 <alltraps>

801059db <vector110>:
.globl vector110
vector110:
  pushl $0
801059db:	6a 00                	push   $0x0
  pushl $110
801059dd:	6a 6e                	push   $0x6e
  jmp alltraps
801059df:	e9 25 f7 ff ff       	jmp    80105109 <alltraps>

801059e4 <vector111>:
.globl vector111
vector111:
  pushl $0
801059e4:	6a 00                	push   $0x0
  pushl $111
801059e6:	6a 6f                	push   $0x6f
  jmp alltraps
801059e8:	e9 1c f7 ff ff       	jmp    80105109 <alltraps>

801059ed <vector112>:
.globl vector112
vector112:
  pushl $0
801059ed:	6a 00                	push   $0x0
  pushl $112
801059ef:	6a 70                	push   $0x70
  jmp alltraps
801059f1:	e9 13 f7 ff ff       	jmp    80105109 <alltraps>

801059f6 <vector113>:
.globl vector113
vector113:
  pushl $0
801059f6:	6a 00                	push   $0x0
  pushl $113
801059f8:	6a 71                	push   $0x71
  jmp alltraps
801059fa:	e9 0a f7 ff ff       	jmp    80105109 <alltraps>

801059ff <vector114>:
.globl vector114
vector114:
  pushl $0
801059ff:	6a 00                	push   $0x0
  pushl $114
80105a01:	6a 72                	push   $0x72
  jmp alltraps
80105a03:	e9 01 f7 ff ff       	jmp    80105109 <alltraps>

80105a08 <vector115>:
.globl vector115
vector115:
  pushl $0
80105a08:	6a 00                	push   $0x0
  pushl $115
80105a0a:	6a 73                	push   $0x73
  jmp alltraps
80105a0c:	e9 f8 f6 ff ff       	jmp    80105109 <alltraps>

80105a11 <vector116>:
.globl vector116
vector116:
  pushl $0
80105a11:	6a 00                	push   $0x0
  pushl $116
80105a13:	6a 74                	push   $0x74
  jmp alltraps
80105a15:	e9 ef f6 ff ff       	jmp    80105109 <alltraps>

80105a1a <vector117>:
.globl vector117
vector117:
  pushl $0
80105a1a:	6a 00                	push   $0x0
  pushl $117
80105a1c:	6a 75                	push   $0x75
  jmp alltraps
80105a1e:	e9 e6 f6 ff ff       	jmp    80105109 <alltraps>

80105a23 <vector118>:
.globl vector118
vector118:
  pushl $0
80105a23:	6a 00                	push   $0x0
  pushl $118
80105a25:	6a 76                	push   $0x76
  jmp alltraps
80105a27:	e9 dd f6 ff ff       	jmp    80105109 <alltraps>

80105a2c <vector119>:
.globl vector119
vector119:
  pushl $0
80105a2c:	6a 00                	push   $0x0
  pushl $119
80105a2e:	6a 77                	push   $0x77
  jmp alltraps
80105a30:	e9 d4 f6 ff ff       	jmp    80105109 <alltraps>

80105a35 <vector120>:
.globl vector120
vector120:
  pushl $0
80105a35:	6a 00                	push   $0x0
  pushl $120
80105a37:	6a 78                	push   $0x78
  jmp alltraps
80105a39:	e9 cb f6 ff ff       	jmp    80105109 <alltraps>

80105a3e <vector121>:
.globl vector121
vector121:
  pushl $0
80105a3e:	6a 00                	push   $0x0
  pushl $121
80105a40:	6a 79                	push   $0x79
  jmp alltraps
80105a42:	e9 c2 f6 ff ff       	jmp    80105109 <alltraps>

80105a47 <vector122>:
.globl vector122
vector122:
  pushl $0
80105a47:	6a 00                	push   $0x0
  pushl $122
80105a49:	6a 7a                	push   $0x7a
  jmp alltraps
80105a4b:	e9 b9 f6 ff ff       	jmp    80105109 <alltraps>

80105a50 <vector123>:
.globl vector123
vector123:
  pushl $0
80105a50:	6a 00                	push   $0x0
  pushl $123
80105a52:	6a 7b                	push   $0x7b
  jmp alltraps
80105a54:	e9 b0 f6 ff ff       	jmp    80105109 <alltraps>

80105a59 <vector124>:
.globl vector124
vector124:
  pushl $0
80105a59:	6a 00                	push   $0x0
  pushl $124
80105a5b:	6a 7c                	push   $0x7c
  jmp alltraps
80105a5d:	e9 a7 f6 ff ff       	jmp    80105109 <alltraps>

80105a62 <vector125>:
.globl vector125
vector125:
  pushl $0
80105a62:	6a 00                	push   $0x0
  pushl $125
80105a64:	6a 7d                	push   $0x7d
  jmp alltraps
80105a66:	e9 9e f6 ff ff       	jmp    80105109 <alltraps>

80105a6b <vector126>:
.globl vector126
vector126:
  pushl $0
80105a6b:	6a 00                	push   $0x0
  pushl $126
80105a6d:	6a 7e                	push   $0x7e
  jmp alltraps
80105a6f:	e9 95 f6 ff ff       	jmp    80105109 <alltraps>

80105a74 <vector127>:
.globl vector127
vector127:
  pushl $0
80105a74:	6a 00                	push   $0x0
  pushl $127
80105a76:	6a 7f                	push   $0x7f
  jmp alltraps
80105a78:	e9 8c f6 ff ff       	jmp    80105109 <alltraps>

80105a7d <vector128>:
.globl vector128
vector128:
  pushl $0
80105a7d:	6a 00                	push   $0x0
  pushl $128
80105a7f:	68 80 00 00 00       	push   $0x80
  jmp alltraps
80105a84:	e9 80 f6 ff ff       	jmp    80105109 <alltraps>

80105a89 <vector129>:
.globl vector129
vector129:
  pushl $0
80105a89:	6a 00                	push   $0x0
  pushl $129
80105a8b:	68 81 00 00 00       	push   $0x81
  jmp alltraps
80105a90:	e9 74 f6 ff ff       	jmp    80105109 <alltraps>

80105a95 <vector130>:
.globl vector130
vector130:
  pushl $0
80105a95:	6a 00                	push   $0x0
  pushl $130
80105a97:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80105a9c:	e9 68 f6 ff ff       	jmp    80105109 <alltraps>

80105aa1 <vector131>:
.globl vector131
vector131:
  pushl $0
80105aa1:	6a 00                	push   $0x0
  pushl $131
80105aa3:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80105aa8:	e9 5c f6 ff ff       	jmp    80105109 <alltraps>

80105aad <vector132>:
.globl vector132
vector132:
  pushl $0
80105aad:	6a 00                	push   $0x0
  pushl $132
80105aaf:	68 84 00 00 00       	push   $0x84
  jmp alltraps
80105ab4:	e9 50 f6 ff ff       	jmp    80105109 <alltraps>

80105ab9 <vector133>:
.globl vector133
vector133:
  pushl $0
80105ab9:	6a 00                	push   $0x0
  pushl $133
80105abb:	68 85 00 00 00       	push   $0x85
  jmp alltraps
80105ac0:	e9 44 f6 ff ff       	jmp    80105109 <alltraps>

80105ac5 <vector134>:
.globl vector134
vector134:
  pushl $0
80105ac5:	6a 00                	push   $0x0
  pushl $134
80105ac7:	68 86 00 00 00       	push   $0x86
  jmp alltraps
80105acc:	e9 38 f6 ff ff       	jmp    80105109 <alltraps>

80105ad1 <vector135>:
.globl vector135
vector135:
  pushl $0
80105ad1:	6a 00                	push   $0x0
  pushl $135
80105ad3:	68 87 00 00 00       	push   $0x87
  jmp alltraps
80105ad8:	e9 2c f6 ff ff       	jmp    80105109 <alltraps>

80105add <vector136>:
.globl vector136
vector136:
  pushl $0
80105add:	6a 00                	push   $0x0
  pushl $136
80105adf:	68 88 00 00 00       	push   $0x88
  jmp alltraps
80105ae4:	e9 20 f6 ff ff       	jmp    80105109 <alltraps>

80105ae9 <vector137>:
.globl vector137
vector137:
  pushl $0
80105ae9:	6a 00                	push   $0x0
  pushl $137
80105aeb:	68 89 00 00 00       	push   $0x89
  jmp alltraps
80105af0:	e9 14 f6 ff ff       	jmp    80105109 <alltraps>

80105af5 <vector138>:
.globl vector138
vector138:
  pushl $0
80105af5:	6a 00                	push   $0x0
  pushl $138
80105af7:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
80105afc:	e9 08 f6 ff ff       	jmp    80105109 <alltraps>

80105b01 <vector139>:
.globl vector139
vector139:
  pushl $0
80105b01:	6a 00                	push   $0x0
  pushl $139
80105b03:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
80105b08:	e9 fc f5 ff ff       	jmp    80105109 <alltraps>

80105b0d <vector140>:
.globl vector140
vector140:
  pushl $0
80105b0d:	6a 00                	push   $0x0
  pushl $140
80105b0f:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
80105b14:	e9 f0 f5 ff ff       	jmp    80105109 <alltraps>

80105b19 <vector141>:
.globl vector141
vector141:
  pushl $0
80105b19:	6a 00                	push   $0x0
  pushl $141
80105b1b:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
80105b20:	e9 e4 f5 ff ff       	jmp    80105109 <alltraps>

80105b25 <vector142>:
.globl vector142
vector142:
  pushl $0
80105b25:	6a 00                	push   $0x0
  pushl $142
80105b27:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80105b2c:	e9 d8 f5 ff ff       	jmp    80105109 <alltraps>

80105b31 <vector143>:
.globl vector143
vector143:
  pushl $0
80105b31:	6a 00                	push   $0x0
  pushl $143
80105b33:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80105b38:	e9 cc f5 ff ff       	jmp    80105109 <alltraps>

80105b3d <vector144>:
.globl vector144
vector144:
  pushl $0
80105b3d:	6a 00                	push   $0x0
  pushl $144
80105b3f:	68 90 00 00 00       	push   $0x90
  jmp alltraps
80105b44:	e9 c0 f5 ff ff       	jmp    80105109 <alltraps>

80105b49 <vector145>:
.globl vector145
vector145:
  pushl $0
80105b49:	6a 00                	push   $0x0
  pushl $145
80105b4b:	68 91 00 00 00       	push   $0x91
  jmp alltraps
80105b50:	e9 b4 f5 ff ff       	jmp    80105109 <alltraps>

80105b55 <vector146>:
.globl vector146
vector146:
  pushl $0
80105b55:	6a 00                	push   $0x0
  pushl $146
80105b57:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80105b5c:	e9 a8 f5 ff ff       	jmp    80105109 <alltraps>

80105b61 <vector147>:
.globl vector147
vector147:
  pushl $0
80105b61:	6a 00                	push   $0x0
  pushl $147
80105b63:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80105b68:	e9 9c f5 ff ff       	jmp    80105109 <alltraps>

80105b6d <vector148>:
.globl vector148
vector148:
  pushl $0
80105b6d:	6a 00                	push   $0x0
  pushl $148
80105b6f:	68 94 00 00 00       	push   $0x94
  jmp alltraps
80105b74:	e9 90 f5 ff ff       	jmp    80105109 <alltraps>

80105b79 <vector149>:
.globl vector149
vector149:
  pushl $0
80105b79:	6a 00                	push   $0x0
  pushl $149
80105b7b:	68 95 00 00 00       	push   $0x95
  jmp alltraps
80105b80:	e9 84 f5 ff ff       	jmp    80105109 <alltraps>

80105b85 <vector150>:
.globl vector150
vector150:
  pushl $0
80105b85:	6a 00                	push   $0x0
  pushl $150
80105b87:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80105b8c:	e9 78 f5 ff ff       	jmp    80105109 <alltraps>

80105b91 <vector151>:
.globl vector151
vector151:
  pushl $0
80105b91:	6a 00                	push   $0x0
  pushl $151
80105b93:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80105b98:	e9 6c f5 ff ff       	jmp    80105109 <alltraps>

80105b9d <vector152>:
.globl vector152
vector152:
  pushl $0
80105b9d:	6a 00                	push   $0x0
  pushl $152
80105b9f:	68 98 00 00 00       	push   $0x98
  jmp alltraps
80105ba4:	e9 60 f5 ff ff       	jmp    80105109 <alltraps>

80105ba9 <vector153>:
.globl vector153
vector153:
  pushl $0
80105ba9:	6a 00                	push   $0x0
  pushl $153
80105bab:	68 99 00 00 00       	push   $0x99
  jmp alltraps
80105bb0:	e9 54 f5 ff ff       	jmp    80105109 <alltraps>

80105bb5 <vector154>:
.globl vector154
vector154:
  pushl $0
80105bb5:	6a 00                	push   $0x0
  pushl $154
80105bb7:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
80105bbc:	e9 48 f5 ff ff       	jmp    80105109 <alltraps>

80105bc1 <vector155>:
.globl vector155
vector155:
  pushl $0
80105bc1:	6a 00                	push   $0x0
  pushl $155
80105bc3:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
80105bc8:	e9 3c f5 ff ff       	jmp    80105109 <alltraps>

80105bcd <vector156>:
.globl vector156
vector156:
  pushl $0
80105bcd:	6a 00                	push   $0x0
  pushl $156
80105bcf:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
80105bd4:	e9 30 f5 ff ff       	jmp    80105109 <alltraps>

80105bd9 <vector157>:
.globl vector157
vector157:
  pushl $0
80105bd9:	6a 00                	push   $0x0
  pushl $157
80105bdb:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
80105be0:	e9 24 f5 ff ff       	jmp    80105109 <alltraps>

80105be5 <vector158>:
.globl vector158
vector158:
  pushl $0
80105be5:	6a 00                	push   $0x0
  pushl $158
80105be7:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
80105bec:	e9 18 f5 ff ff       	jmp    80105109 <alltraps>

80105bf1 <vector159>:
.globl vector159
vector159:
  pushl $0
80105bf1:	6a 00                	push   $0x0
  pushl $159
80105bf3:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
80105bf8:	e9 0c f5 ff ff       	jmp    80105109 <alltraps>

80105bfd <vector160>:
.globl vector160
vector160:
  pushl $0
80105bfd:	6a 00                	push   $0x0
  pushl $160
80105bff:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
80105c04:	e9 00 f5 ff ff       	jmp    80105109 <alltraps>

80105c09 <vector161>:
.globl vector161
vector161:
  pushl $0
80105c09:	6a 00                	push   $0x0
  pushl $161
80105c0b:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
80105c10:	e9 f4 f4 ff ff       	jmp    80105109 <alltraps>

80105c15 <vector162>:
.globl vector162
vector162:
  pushl $0
80105c15:	6a 00                	push   $0x0
  pushl $162
80105c17:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80105c1c:	e9 e8 f4 ff ff       	jmp    80105109 <alltraps>

80105c21 <vector163>:
.globl vector163
vector163:
  pushl $0
80105c21:	6a 00                	push   $0x0
  pushl $163
80105c23:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80105c28:	e9 dc f4 ff ff       	jmp    80105109 <alltraps>

80105c2d <vector164>:
.globl vector164
vector164:
  pushl $0
80105c2d:	6a 00                	push   $0x0
  pushl $164
80105c2f:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
80105c34:	e9 d0 f4 ff ff       	jmp    80105109 <alltraps>

80105c39 <vector165>:
.globl vector165
vector165:
  pushl $0
80105c39:	6a 00                	push   $0x0
  pushl $165
80105c3b:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
80105c40:	e9 c4 f4 ff ff       	jmp    80105109 <alltraps>

80105c45 <vector166>:
.globl vector166
vector166:
  pushl $0
80105c45:	6a 00                	push   $0x0
  pushl $166
80105c47:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80105c4c:	e9 b8 f4 ff ff       	jmp    80105109 <alltraps>

80105c51 <vector167>:
.globl vector167
vector167:
  pushl $0
80105c51:	6a 00                	push   $0x0
  pushl $167
80105c53:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80105c58:	e9 ac f4 ff ff       	jmp    80105109 <alltraps>

80105c5d <vector168>:
.globl vector168
vector168:
  pushl $0
80105c5d:	6a 00                	push   $0x0
  pushl $168
80105c5f:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
80105c64:	e9 a0 f4 ff ff       	jmp    80105109 <alltraps>

80105c69 <vector169>:
.globl vector169
vector169:
  pushl $0
80105c69:	6a 00                	push   $0x0
  pushl $169
80105c6b:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
80105c70:	e9 94 f4 ff ff       	jmp    80105109 <alltraps>

80105c75 <vector170>:
.globl vector170
vector170:
  pushl $0
80105c75:	6a 00                	push   $0x0
  pushl $170
80105c77:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80105c7c:	e9 88 f4 ff ff       	jmp    80105109 <alltraps>

80105c81 <vector171>:
.globl vector171
vector171:
  pushl $0
80105c81:	6a 00                	push   $0x0
  pushl $171
80105c83:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80105c88:	e9 7c f4 ff ff       	jmp    80105109 <alltraps>

80105c8d <vector172>:
.globl vector172
vector172:
  pushl $0
80105c8d:	6a 00                	push   $0x0
  pushl $172
80105c8f:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
80105c94:	e9 70 f4 ff ff       	jmp    80105109 <alltraps>

80105c99 <vector173>:
.globl vector173
vector173:
  pushl $0
80105c99:	6a 00                	push   $0x0
  pushl $173
80105c9b:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
80105ca0:	e9 64 f4 ff ff       	jmp    80105109 <alltraps>

80105ca5 <vector174>:
.globl vector174
vector174:
  pushl $0
80105ca5:	6a 00                	push   $0x0
  pushl $174
80105ca7:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80105cac:	e9 58 f4 ff ff       	jmp    80105109 <alltraps>

80105cb1 <vector175>:
.globl vector175
vector175:
  pushl $0
80105cb1:	6a 00                	push   $0x0
  pushl $175
80105cb3:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
80105cb8:	e9 4c f4 ff ff       	jmp    80105109 <alltraps>

80105cbd <vector176>:
.globl vector176
vector176:
  pushl $0
80105cbd:	6a 00                	push   $0x0
  pushl $176
80105cbf:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
80105cc4:	e9 40 f4 ff ff       	jmp    80105109 <alltraps>

80105cc9 <vector177>:
.globl vector177
vector177:
  pushl $0
80105cc9:	6a 00                	push   $0x0
  pushl $177
80105ccb:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
80105cd0:	e9 34 f4 ff ff       	jmp    80105109 <alltraps>

80105cd5 <vector178>:
.globl vector178
vector178:
  pushl $0
80105cd5:	6a 00                	push   $0x0
  pushl $178
80105cd7:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
80105cdc:	e9 28 f4 ff ff       	jmp    80105109 <alltraps>

80105ce1 <vector179>:
.globl vector179
vector179:
  pushl $0
80105ce1:	6a 00                	push   $0x0
  pushl $179
80105ce3:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
80105ce8:	e9 1c f4 ff ff       	jmp    80105109 <alltraps>

80105ced <vector180>:
.globl vector180
vector180:
  pushl $0
80105ced:	6a 00                	push   $0x0
  pushl $180
80105cef:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
80105cf4:	e9 10 f4 ff ff       	jmp    80105109 <alltraps>

80105cf9 <vector181>:
.globl vector181
vector181:
  pushl $0
80105cf9:	6a 00                	push   $0x0
  pushl $181
80105cfb:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
80105d00:	e9 04 f4 ff ff       	jmp    80105109 <alltraps>

80105d05 <vector182>:
.globl vector182
vector182:
  pushl $0
80105d05:	6a 00                	push   $0x0
  pushl $182
80105d07:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
80105d0c:	e9 f8 f3 ff ff       	jmp    80105109 <alltraps>

80105d11 <vector183>:
.globl vector183
vector183:
  pushl $0
80105d11:	6a 00                	push   $0x0
  pushl $183
80105d13:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80105d18:	e9 ec f3 ff ff       	jmp    80105109 <alltraps>

80105d1d <vector184>:
.globl vector184
vector184:
  pushl $0
80105d1d:	6a 00                	push   $0x0
  pushl $184
80105d1f:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
80105d24:	e9 e0 f3 ff ff       	jmp    80105109 <alltraps>

80105d29 <vector185>:
.globl vector185
vector185:
  pushl $0
80105d29:	6a 00                	push   $0x0
  pushl $185
80105d2b:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
80105d30:	e9 d4 f3 ff ff       	jmp    80105109 <alltraps>

80105d35 <vector186>:
.globl vector186
vector186:
  pushl $0
80105d35:	6a 00                	push   $0x0
  pushl $186
80105d37:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80105d3c:	e9 c8 f3 ff ff       	jmp    80105109 <alltraps>

80105d41 <vector187>:
.globl vector187
vector187:
  pushl $0
80105d41:	6a 00                	push   $0x0
  pushl $187
80105d43:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80105d48:	e9 bc f3 ff ff       	jmp    80105109 <alltraps>

80105d4d <vector188>:
.globl vector188
vector188:
  pushl $0
80105d4d:	6a 00                	push   $0x0
  pushl $188
80105d4f:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
80105d54:	e9 b0 f3 ff ff       	jmp    80105109 <alltraps>

80105d59 <vector189>:
.globl vector189
vector189:
  pushl $0
80105d59:	6a 00                	push   $0x0
  pushl $189
80105d5b:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
80105d60:	e9 a4 f3 ff ff       	jmp    80105109 <alltraps>

80105d65 <vector190>:
.globl vector190
vector190:
  pushl $0
80105d65:	6a 00                	push   $0x0
  pushl $190
80105d67:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80105d6c:	e9 98 f3 ff ff       	jmp    80105109 <alltraps>

80105d71 <vector191>:
.globl vector191
vector191:
  pushl $0
80105d71:	6a 00                	push   $0x0
  pushl $191
80105d73:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80105d78:	e9 8c f3 ff ff       	jmp    80105109 <alltraps>

80105d7d <vector192>:
.globl vector192
vector192:
  pushl $0
80105d7d:	6a 00                	push   $0x0
  pushl $192
80105d7f:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
80105d84:	e9 80 f3 ff ff       	jmp    80105109 <alltraps>

80105d89 <vector193>:
.globl vector193
vector193:
  pushl $0
80105d89:	6a 00                	push   $0x0
  pushl $193
80105d8b:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
80105d90:	e9 74 f3 ff ff       	jmp    80105109 <alltraps>

80105d95 <vector194>:
.globl vector194
vector194:
  pushl $0
80105d95:	6a 00                	push   $0x0
  pushl $194
80105d97:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80105d9c:	e9 68 f3 ff ff       	jmp    80105109 <alltraps>

80105da1 <vector195>:
.globl vector195
vector195:
  pushl $0
80105da1:	6a 00                	push   $0x0
  pushl $195
80105da3:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80105da8:	e9 5c f3 ff ff       	jmp    80105109 <alltraps>

80105dad <vector196>:
.globl vector196
vector196:
  pushl $0
80105dad:	6a 00                	push   $0x0
  pushl $196
80105daf:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
80105db4:	e9 50 f3 ff ff       	jmp    80105109 <alltraps>

80105db9 <vector197>:
.globl vector197
vector197:
  pushl $0
80105db9:	6a 00                	push   $0x0
  pushl $197
80105dbb:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
80105dc0:	e9 44 f3 ff ff       	jmp    80105109 <alltraps>

80105dc5 <vector198>:
.globl vector198
vector198:
  pushl $0
80105dc5:	6a 00                	push   $0x0
  pushl $198
80105dc7:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
80105dcc:	e9 38 f3 ff ff       	jmp    80105109 <alltraps>

80105dd1 <vector199>:
.globl vector199
vector199:
  pushl $0
80105dd1:	6a 00                	push   $0x0
  pushl $199
80105dd3:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
80105dd8:	e9 2c f3 ff ff       	jmp    80105109 <alltraps>

80105ddd <vector200>:
.globl vector200
vector200:
  pushl $0
80105ddd:	6a 00                	push   $0x0
  pushl $200
80105ddf:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
80105de4:	e9 20 f3 ff ff       	jmp    80105109 <alltraps>

80105de9 <vector201>:
.globl vector201
vector201:
  pushl $0
80105de9:	6a 00                	push   $0x0
  pushl $201
80105deb:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
80105df0:	e9 14 f3 ff ff       	jmp    80105109 <alltraps>

80105df5 <vector202>:
.globl vector202
vector202:
  pushl $0
80105df5:	6a 00                	push   $0x0
  pushl $202
80105df7:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
80105dfc:	e9 08 f3 ff ff       	jmp    80105109 <alltraps>

80105e01 <vector203>:
.globl vector203
vector203:
  pushl $0
80105e01:	6a 00                	push   $0x0
  pushl $203
80105e03:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
80105e08:	e9 fc f2 ff ff       	jmp    80105109 <alltraps>

80105e0d <vector204>:
.globl vector204
vector204:
  pushl $0
80105e0d:	6a 00                	push   $0x0
  pushl $204
80105e0f:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
80105e14:	e9 f0 f2 ff ff       	jmp    80105109 <alltraps>

80105e19 <vector205>:
.globl vector205
vector205:
  pushl $0
80105e19:	6a 00                	push   $0x0
  pushl $205
80105e1b:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
80105e20:	e9 e4 f2 ff ff       	jmp    80105109 <alltraps>

80105e25 <vector206>:
.globl vector206
vector206:
  pushl $0
80105e25:	6a 00                	push   $0x0
  pushl $206
80105e27:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80105e2c:	e9 d8 f2 ff ff       	jmp    80105109 <alltraps>

80105e31 <vector207>:
.globl vector207
vector207:
  pushl $0
80105e31:	6a 00                	push   $0x0
  pushl $207
80105e33:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80105e38:	e9 cc f2 ff ff       	jmp    80105109 <alltraps>

80105e3d <vector208>:
.globl vector208
vector208:
  pushl $0
80105e3d:	6a 00                	push   $0x0
  pushl $208
80105e3f:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
80105e44:	e9 c0 f2 ff ff       	jmp    80105109 <alltraps>

80105e49 <vector209>:
.globl vector209
vector209:
  pushl $0
80105e49:	6a 00                	push   $0x0
  pushl $209
80105e4b:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
80105e50:	e9 b4 f2 ff ff       	jmp    80105109 <alltraps>

80105e55 <vector210>:
.globl vector210
vector210:
  pushl $0
80105e55:	6a 00                	push   $0x0
  pushl $210
80105e57:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80105e5c:	e9 a8 f2 ff ff       	jmp    80105109 <alltraps>

80105e61 <vector211>:
.globl vector211
vector211:
  pushl $0
80105e61:	6a 00                	push   $0x0
  pushl $211
80105e63:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80105e68:	e9 9c f2 ff ff       	jmp    80105109 <alltraps>

80105e6d <vector212>:
.globl vector212
vector212:
  pushl $0
80105e6d:	6a 00                	push   $0x0
  pushl $212
80105e6f:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
80105e74:	e9 90 f2 ff ff       	jmp    80105109 <alltraps>

80105e79 <vector213>:
.globl vector213
vector213:
  pushl $0
80105e79:	6a 00                	push   $0x0
  pushl $213
80105e7b:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
80105e80:	e9 84 f2 ff ff       	jmp    80105109 <alltraps>

80105e85 <vector214>:
.globl vector214
vector214:
  pushl $0
80105e85:	6a 00                	push   $0x0
  pushl $214
80105e87:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80105e8c:	e9 78 f2 ff ff       	jmp    80105109 <alltraps>

80105e91 <vector215>:
.globl vector215
vector215:
  pushl $0
80105e91:	6a 00                	push   $0x0
  pushl $215
80105e93:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80105e98:	e9 6c f2 ff ff       	jmp    80105109 <alltraps>

80105e9d <vector216>:
.globl vector216
vector216:
  pushl $0
80105e9d:	6a 00                	push   $0x0
  pushl $216
80105e9f:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
80105ea4:	e9 60 f2 ff ff       	jmp    80105109 <alltraps>

80105ea9 <vector217>:
.globl vector217
vector217:
  pushl $0
80105ea9:	6a 00                	push   $0x0
  pushl $217
80105eab:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
80105eb0:	e9 54 f2 ff ff       	jmp    80105109 <alltraps>

80105eb5 <vector218>:
.globl vector218
vector218:
  pushl $0
80105eb5:	6a 00                	push   $0x0
  pushl $218
80105eb7:	68 da 00 00 00       	push   $0xda
  jmp alltraps
80105ebc:	e9 48 f2 ff ff       	jmp    80105109 <alltraps>

80105ec1 <vector219>:
.globl vector219
vector219:
  pushl $0
80105ec1:	6a 00                	push   $0x0
  pushl $219
80105ec3:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
80105ec8:	e9 3c f2 ff ff       	jmp    80105109 <alltraps>

80105ecd <vector220>:
.globl vector220
vector220:
  pushl $0
80105ecd:	6a 00                	push   $0x0
  pushl $220
80105ecf:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
80105ed4:	e9 30 f2 ff ff       	jmp    80105109 <alltraps>

80105ed9 <vector221>:
.globl vector221
vector221:
  pushl $0
80105ed9:	6a 00                	push   $0x0
  pushl $221
80105edb:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
80105ee0:	e9 24 f2 ff ff       	jmp    80105109 <alltraps>

80105ee5 <vector222>:
.globl vector222
vector222:
  pushl $0
80105ee5:	6a 00                	push   $0x0
  pushl $222
80105ee7:	68 de 00 00 00       	push   $0xde
  jmp alltraps
80105eec:	e9 18 f2 ff ff       	jmp    80105109 <alltraps>

80105ef1 <vector223>:
.globl vector223
vector223:
  pushl $0
80105ef1:	6a 00                	push   $0x0
  pushl $223
80105ef3:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
80105ef8:	e9 0c f2 ff ff       	jmp    80105109 <alltraps>

80105efd <vector224>:
.globl vector224
vector224:
  pushl $0
80105efd:	6a 00                	push   $0x0
  pushl $224
80105eff:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
80105f04:	e9 00 f2 ff ff       	jmp    80105109 <alltraps>

80105f09 <vector225>:
.globl vector225
vector225:
  pushl $0
80105f09:	6a 00                	push   $0x0
  pushl $225
80105f0b:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
80105f10:	e9 f4 f1 ff ff       	jmp    80105109 <alltraps>

80105f15 <vector226>:
.globl vector226
vector226:
  pushl $0
80105f15:	6a 00                	push   $0x0
  pushl $226
80105f17:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
80105f1c:	e9 e8 f1 ff ff       	jmp    80105109 <alltraps>

80105f21 <vector227>:
.globl vector227
vector227:
  pushl $0
80105f21:	6a 00                	push   $0x0
  pushl $227
80105f23:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80105f28:	e9 dc f1 ff ff       	jmp    80105109 <alltraps>

80105f2d <vector228>:
.globl vector228
vector228:
  pushl $0
80105f2d:	6a 00                	push   $0x0
  pushl $228
80105f2f:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
80105f34:	e9 d0 f1 ff ff       	jmp    80105109 <alltraps>

80105f39 <vector229>:
.globl vector229
vector229:
  pushl $0
80105f39:	6a 00                	push   $0x0
  pushl $229
80105f3b:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
80105f40:	e9 c4 f1 ff ff       	jmp    80105109 <alltraps>

80105f45 <vector230>:
.globl vector230
vector230:
  pushl $0
80105f45:	6a 00                	push   $0x0
  pushl $230
80105f47:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
80105f4c:	e9 b8 f1 ff ff       	jmp    80105109 <alltraps>

80105f51 <vector231>:
.globl vector231
vector231:
  pushl $0
80105f51:	6a 00                	push   $0x0
  pushl $231
80105f53:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80105f58:	e9 ac f1 ff ff       	jmp    80105109 <alltraps>

80105f5d <vector232>:
.globl vector232
vector232:
  pushl $0
80105f5d:	6a 00                	push   $0x0
  pushl $232
80105f5f:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
80105f64:	e9 a0 f1 ff ff       	jmp    80105109 <alltraps>

80105f69 <vector233>:
.globl vector233
vector233:
  pushl $0
80105f69:	6a 00                	push   $0x0
  pushl $233
80105f6b:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
80105f70:	e9 94 f1 ff ff       	jmp    80105109 <alltraps>

80105f75 <vector234>:
.globl vector234
vector234:
  pushl $0
80105f75:	6a 00                	push   $0x0
  pushl $234
80105f77:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80105f7c:	e9 88 f1 ff ff       	jmp    80105109 <alltraps>

80105f81 <vector235>:
.globl vector235
vector235:
  pushl $0
80105f81:	6a 00                	push   $0x0
  pushl $235
80105f83:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80105f88:	e9 7c f1 ff ff       	jmp    80105109 <alltraps>

80105f8d <vector236>:
.globl vector236
vector236:
  pushl $0
80105f8d:	6a 00                	push   $0x0
  pushl $236
80105f8f:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
80105f94:	e9 70 f1 ff ff       	jmp    80105109 <alltraps>

80105f99 <vector237>:
.globl vector237
vector237:
  pushl $0
80105f99:	6a 00                	push   $0x0
  pushl $237
80105f9b:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
80105fa0:	e9 64 f1 ff ff       	jmp    80105109 <alltraps>

80105fa5 <vector238>:
.globl vector238
vector238:
  pushl $0
80105fa5:	6a 00                	push   $0x0
  pushl $238
80105fa7:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80105fac:	e9 58 f1 ff ff       	jmp    80105109 <alltraps>

80105fb1 <vector239>:
.globl vector239
vector239:
  pushl $0
80105fb1:	6a 00                	push   $0x0
  pushl $239
80105fb3:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
80105fb8:	e9 4c f1 ff ff       	jmp    80105109 <alltraps>

80105fbd <vector240>:
.globl vector240
vector240:
  pushl $0
80105fbd:	6a 00                	push   $0x0
  pushl $240
80105fbf:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
80105fc4:	e9 40 f1 ff ff       	jmp    80105109 <alltraps>

80105fc9 <vector241>:
.globl vector241
vector241:
  pushl $0
80105fc9:	6a 00                	push   $0x0
  pushl $241
80105fcb:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
80105fd0:	e9 34 f1 ff ff       	jmp    80105109 <alltraps>

80105fd5 <vector242>:
.globl vector242
vector242:
  pushl $0
80105fd5:	6a 00                	push   $0x0
  pushl $242
80105fd7:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
80105fdc:	e9 28 f1 ff ff       	jmp    80105109 <alltraps>

80105fe1 <vector243>:
.globl vector243
vector243:
  pushl $0
80105fe1:	6a 00                	push   $0x0
  pushl $243
80105fe3:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
80105fe8:	e9 1c f1 ff ff       	jmp    80105109 <alltraps>

80105fed <vector244>:
.globl vector244
vector244:
  pushl $0
80105fed:	6a 00                	push   $0x0
  pushl $244
80105fef:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
80105ff4:	e9 10 f1 ff ff       	jmp    80105109 <alltraps>

80105ff9 <vector245>:
.globl vector245
vector245:
  pushl $0
80105ff9:	6a 00                	push   $0x0
  pushl $245
80105ffb:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
80106000:	e9 04 f1 ff ff       	jmp    80105109 <alltraps>

80106005 <vector246>:
.globl vector246
vector246:
  pushl $0
80106005:	6a 00                	push   $0x0
  pushl $246
80106007:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
8010600c:	e9 f8 f0 ff ff       	jmp    80105109 <alltraps>

80106011 <vector247>:
.globl vector247
vector247:
  pushl $0
80106011:	6a 00                	push   $0x0
  pushl $247
80106013:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80106018:	e9 ec f0 ff ff       	jmp    80105109 <alltraps>

8010601d <vector248>:
.globl vector248
vector248:
  pushl $0
8010601d:	6a 00                	push   $0x0
  pushl $248
8010601f:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
80106024:	e9 e0 f0 ff ff       	jmp    80105109 <alltraps>

80106029 <vector249>:
.globl vector249
vector249:
  pushl $0
80106029:	6a 00                	push   $0x0
  pushl $249
8010602b:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
80106030:	e9 d4 f0 ff ff       	jmp    80105109 <alltraps>

80106035 <vector250>:
.globl vector250
vector250:
  pushl $0
80106035:	6a 00                	push   $0x0
  pushl $250
80106037:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
8010603c:	e9 c8 f0 ff ff       	jmp    80105109 <alltraps>

80106041 <vector251>:
.globl vector251
vector251:
  pushl $0
80106041:	6a 00                	push   $0x0
  pushl $251
80106043:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80106048:	e9 bc f0 ff ff       	jmp    80105109 <alltraps>

8010604d <vector252>:
.globl vector252
vector252:
  pushl $0
8010604d:	6a 00                	push   $0x0
  pushl $252
8010604f:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
80106054:	e9 b0 f0 ff ff       	jmp    80105109 <alltraps>

80106059 <vector253>:
.globl vector253
vector253:
  pushl $0
80106059:	6a 00                	push   $0x0
  pushl $253
8010605b:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
80106060:	e9 a4 f0 ff ff       	jmp    80105109 <alltraps>

80106065 <vector254>:
.globl vector254
vector254:
  pushl $0
80106065:	6a 00                	push   $0x0
  pushl $254
80106067:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
8010606c:	e9 98 f0 ff ff       	jmp    80105109 <alltraps>

80106071 <vector255>:
.globl vector255
vector255:
  pushl $0
80106071:	6a 00                	push   $0x0
  pushl $255
80106073:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80106078:	e9 8c f0 ff ff       	jmp    80105109 <alltraps>

8010607d <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
8010607d:	55                   	push   %ebp
8010607e:	89 e5                	mov    %esp,%ebp
80106080:	57                   	push   %edi
80106081:	56                   	push   %esi
80106082:	53                   	push   %ebx
80106083:	83 ec 0c             	sub    $0xc,%esp
80106086:	89 d6                	mov    %edx,%esi
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
80106088:	c1 ea 16             	shr    $0x16,%edx
8010608b:	8d 3c 90             	lea    (%eax,%edx,4),%edi
  if (*pde & PTE_P)
8010608e:	8b 1f                	mov    (%edi),%ebx
80106090:	f6 c3 01             	test   $0x1,%bl
80106093:	74 37                	je     801060cc <walkpgdir+0x4f>

#ifndef __ASSEMBLER__
// Address in page table or page directory entry
//   I changes these from macros into inline functions to make sure we
//   consistently get an error if a pointer is erroneously passed to them.
static inline uint PTE_ADDR(uint pte)  { return pte & ~0xFFF; }
80106095:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    if (a > KERNBASE)
8010609b:	81 fb 00 00 00 80    	cmp    $0x80000000,%ebx
801060a1:	77 1c                	ja     801060bf <walkpgdir+0x42>
    return (char*)a + KERNBASE;
801060a3:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
801060a9:	c1 ee 0c             	shr    $0xc,%esi
801060ac:	81 e6 ff 03 00 00    	and    $0x3ff,%esi
801060b2:	8d 1c b3             	lea    (%ebx,%esi,4),%ebx
}
801060b5:	89 d8                	mov    %ebx,%eax
801060b7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801060ba:	5b                   	pop    %ebx
801060bb:	5e                   	pop    %esi
801060bc:	5f                   	pop    %edi
801060bd:	5d                   	pop    %ebp
801060be:	c3                   	ret    
        panic("P2V on address > KERNBASE");
801060bf:	83 ec 0c             	sub    $0xc,%esp
801060c2:	68 58 72 10 80       	push   $0x80107258
801060c7:	e8 7c a2 ff ff       	call   80100348 <panic>
    if (!alloc || (pgtab = (pte_t *)kalloc()) == 0)
801060cc:	85 c9                	test   %ecx,%ecx
801060ce:	74 40                	je     80106110 <walkpgdir+0x93>
801060d0:	e8 fa bf ff ff       	call   801020cf <kalloc>
801060d5:	89 c3                	mov    %eax,%ebx
801060d7:	85 c0                	test   %eax,%eax
801060d9:	74 da                	je     801060b5 <walkpgdir+0x38>
    memset(pgtab, 0, PGSIZE);
801060db:	83 ec 04             	sub    $0x4,%esp
801060de:	68 00 10 00 00       	push   $0x1000
801060e3:	6a 00                	push   $0x0
801060e5:	50                   	push   %eax
801060e6:	e8 dc de ff ff       	call   80103fc7 <memset>
    if (a < (void*) KERNBASE)
801060eb:	83 c4 10             	add    $0x10,%esp
801060ee:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
801060f4:	76 0d                	jbe    80106103 <walkpgdir+0x86>
    return (uint)a - KERNBASE;
801060f6:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
801060fc:	83 c8 07             	or     $0x7,%eax
801060ff:	89 07                	mov    %eax,(%edi)
80106101:	eb a6                	jmp    801060a9 <walkpgdir+0x2c>
        panic("V2P on address < KERNBASE "
80106103:	83 ec 0c             	sub    $0xc,%esp
80106106:	68 28 6f 10 80       	push   $0x80106f28
8010610b:	e8 38 a2 ff ff       	call   80100348 <panic>
      return 0;
80106110:	bb 00 00 00 00       	mov    $0x0,%ebx
80106115:	eb 9e                	jmp    801060b5 <walkpgdir+0x38>

80106117 <seginit>:
{
80106117:	55                   	push   %ebp
80106118:	89 e5                	mov    %esp,%ebp
8010611a:	53                   	push   %ebx
8010611b:	83 ec 14             	sub    $0x14,%esp
  c = &cpus[cpuid()];
8010611e:	e8 19 d1 ff ff       	call   8010323c <cpuid>
  c->gdt[SEG_KCODE] = SEG(STA_X | STA_R, 0, 0xffffffff, 0);
80106123:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
80106129:	66 c7 80 18 28 11 80 	movw   $0xffff,-0x7feed7e8(%eax)
80106130:	ff ff 
80106132:	66 c7 80 1a 28 11 80 	movw   $0x0,-0x7feed7e6(%eax)
80106139:	00 00 
8010613b:	c6 80 1c 28 11 80 00 	movb   $0x0,-0x7feed7e4(%eax)
80106142:	0f b6 88 1d 28 11 80 	movzbl -0x7feed7e3(%eax),%ecx
80106149:	83 e1 f0             	and    $0xfffffff0,%ecx
8010614c:	83 c9 1a             	or     $0x1a,%ecx
8010614f:	83 e1 9f             	and    $0xffffff9f,%ecx
80106152:	83 c9 80             	or     $0xffffff80,%ecx
80106155:	88 88 1d 28 11 80    	mov    %cl,-0x7feed7e3(%eax)
8010615b:	0f b6 88 1e 28 11 80 	movzbl -0x7feed7e2(%eax),%ecx
80106162:	83 c9 0f             	or     $0xf,%ecx
80106165:	83 e1 cf             	and    $0xffffffcf,%ecx
80106168:	83 c9 c0             	or     $0xffffffc0,%ecx
8010616b:	88 88 1e 28 11 80    	mov    %cl,-0x7feed7e2(%eax)
80106171:	c6 80 1f 28 11 80 00 	movb   $0x0,-0x7feed7e1(%eax)
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80106178:	66 c7 80 20 28 11 80 	movw   $0xffff,-0x7feed7e0(%eax)
8010617f:	ff ff 
80106181:	66 c7 80 22 28 11 80 	movw   $0x0,-0x7feed7de(%eax)
80106188:	00 00 
8010618a:	c6 80 24 28 11 80 00 	movb   $0x0,-0x7feed7dc(%eax)
80106191:	0f b6 88 25 28 11 80 	movzbl -0x7feed7db(%eax),%ecx
80106198:	83 e1 f0             	and    $0xfffffff0,%ecx
8010619b:	83 c9 12             	or     $0x12,%ecx
8010619e:	83 e1 9f             	and    $0xffffff9f,%ecx
801061a1:	83 c9 80             	or     $0xffffff80,%ecx
801061a4:	88 88 25 28 11 80    	mov    %cl,-0x7feed7db(%eax)
801061aa:	0f b6 88 26 28 11 80 	movzbl -0x7feed7da(%eax),%ecx
801061b1:	83 c9 0f             	or     $0xf,%ecx
801061b4:	83 e1 cf             	and    $0xffffffcf,%ecx
801061b7:	83 c9 c0             	or     $0xffffffc0,%ecx
801061ba:	88 88 26 28 11 80    	mov    %cl,-0x7feed7da(%eax)
801061c0:	c6 80 27 28 11 80 00 	movb   $0x0,-0x7feed7d9(%eax)
  c->gdt[SEG_UCODE] = SEG(STA_X | STA_R, 0, 0xffffffff, DPL_USER);
801061c7:	66 c7 80 28 28 11 80 	movw   $0xffff,-0x7feed7d8(%eax)
801061ce:	ff ff 
801061d0:	66 c7 80 2a 28 11 80 	movw   $0x0,-0x7feed7d6(%eax)
801061d7:	00 00 
801061d9:	c6 80 2c 28 11 80 00 	movb   $0x0,-0x7feed7d4(%eax)
801061e0:	c6 80 2d 28 11 80 fa 	movb   $0xfa,-0x7feed7d3(%eax)
801061e7:	0f b6 88 2e 28 11 80 	movzbl -0x7feed7d2(%eax),%ecx
801061ee:	83 c9 0f             	or     $0xf,%ecx
801061f1:	83 e1 cf             	and    $0xffffffcf,%ecx
801061f4:	83 c9 c0             	or     $0xffffffc0,%ecx
801061f7:	88 88 2e 28 11 80    	mov    %cl,-0x7feed7d2(%eax)
801061fd:	c6 80 2f 28 11 80 00 	movb   $0x0,-0x7feed7d1(%eax)
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80106204:	66 c7 80 30 28 11 80 	movw   $0xffff,-0x7feed7d0(%eax)
8010620b:	ff ff 
8010620d:	66 c7 80 32 28 11 80 	movw   $0x0,-0x7feed7ce(%eax)
80106214:	00 00 
80106216:	c6 80 34 28 11 80 00 	movb   $0x0,-0x7feed7cc(%eax)
8010621d:	c6 80 35 28 11 80 f2 	movb   $0xf2,-0x7feed7cb(%eax)
80106224:	0f b6 88 36 28 11 80 	movzbl -0x7feed7ca(%eax),%ecx
8010622b:	83 c9 0f             	or     $0xf,%ecx
8010622e:	83 e1 cf             	and    $0xffffffcf,%ecx
80106231:	83 c9 c0             	or     $0xffffffc0,%ecx
80106234:	88 88 36 28 11 80    	mov    %cl,-0x7feed7ca(%eax)
8010623a:	c6 80 37 28 11 80 00 	movb   $0x0,-0x7feed7c9(%eax)
  lgdt(c->gdt, sizeof(c->gdt));
80106241:	05 10 28 11 80       	add    $0x80112810,%eax
  pd[0] = size-1;
80106246:	66 c7 45 f2 2f 00    	movw   $0x2f,-0xe(%ebp)
  pd[1] = (uint)p;
8010624c:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80106250:	c1 e8 10             	shr    $0x10,%eax
80106253:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
80106257:	8d 45 f2             	lea    -0xe(%ebp),%eax
8010625a:	0f 01 10             	lgdtl  (%eax)
}
8010625d:	83 c4 14             	add    $0x14,%esp
80106260:	5b                   	pop    %ebx
80106261:	5d                   	pop    %ebp
80106262:	c3                   	ret    

80106263 <walkpgdirhelper>:
pte_t *walkpgdirhelper(pde_t *pgdir, const void *va, int alloc)
{
80106263:	55                   	push   %ebp
80106264:	89 e5                	mov    %esp,%ebp
80106266:	83 ec 08             	sub    $0x8,%esp
  return walkpgdir(pgdir, va, alloc);
80106269:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010626c:	8b 55 0c             	mov    0xc(%ebp),%edx
8010626f:	8b 45 08             	mov    0x8(%ebp),%eax
80106272:	e8 06 fe ff ff       	call   8010607d <walkpgdir>
}
80106277:	c9                   	leave  
80106278:	c3                   	ret    

80106279 <mappages>:
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned.
int mappages(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
80106279:	55                   	push   %ebp
8010627a:	89 e5                	mov    %esp,%ebp
8010627c:	57                   	push   %edi
8010627d:	56                   	push   %esi
8010627e:	53                   	push   %ebx
8010627f:	83 ec 0c             	sub    $0xc,%esp
80106282:	8b 7d 0c             	mov    0xc(%ebp),%edi
80106285:	8b 75 14             	mov    0x14(%ebp),%esi
  char *a, *last;
  pte_t *pte;

  a = (char *)PGROUNDDOWN((uint)va);
80106288:	89 fb                	mov    %edi,%ebx
8010628a:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  last = (char *)PGROUNDDOWN(((uint)va) + size - 1);
80106290:	03 7d 10             	add    0x10(%ebp),%edi
80106293:	83 ef 01             	sub    $0x1,%edi
80106296:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
  for (;;)
  {
    if ((pte = walkpgdir(pgdir, a, 1)) == 0)
8010629c:	b9 01 00 00 00       	mov    $0x1,%ecx
801062a1:	89 da                	mov    %ebx,%edx
801062a3:	8b 45 08             	mov    0x8(%ebp),%eax
801062a6:	e8 d2 fd ff ff       	call   8010607d <walkpgdir>
801062ab:	85 c0                	test   %eax,%eax
801062ad:	74 2e                	je     801062dd <mappages+0x64>
      return -1;
    if (*pte & PTE_P)
801062af:	f6 00 01             	testb  $0x1,(%eax)
801062b2:	75 1c                	jne    801062d0 <mappages+0x57>
      panic("remap");
    *pte = pa | perm | PTE_P;
801062b4:	89 f2                	mov    %esi,%edx
801062b6:	0b 55 18             	or     0x18(%ebp),%edx
801062b9:	83 ca 01             	or     $0x1,%edx
801062bc:	89 10                	mov    %edx,(%eax)
    if (a == last)
801062be:	39 fb                	cmp    %edi,%ebx
801062c0:	74 28                	je     801062ea <mappages+0x71>
      break;
    a += PGSIZE;
801062c2:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    pa += PGSIZE;
801062c8:	81 c6 00 10 00 00    	add    $0x1000,%esi
    if ((pte = walkpgdir(pgdir, a, 1)) == 0)
801062ce:	eb cc                	jmp    8010629c <mappages+0x23>
      panic("remap");
801062d0:	83 ec 0c             	sub    $0xc,%esp
801062d3:	68 4b 76 10 80       	push   $0x8010764b
801062d8:	e8 6b a0 ff ff       	call   80100348 <panic>
      return -1;
801062dd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  return 0;
}
801062e2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801062e5:	5b                   	pop    %ebx
801062e6:	5e                   	pop    %esi
801062e7:	5f                   	pop    %edi
801062e8:	5d                   	pop    %ebp
801062e9:	c3                   	ret    
  return 0;
801062ea:	b8 00 00 00 00       	mov    $0x0,%eax
801062ef:	eb f1                	jmp    801062e2 <mappages+0x69>

801062f1 <mappageshelper>:
int mappageshelper(pde_t *pgdir, void *va, uint size, uint pa, int perm)
{
801062f1:	55                   	push   %ebp
801062f2:	89 e5                	mov    %esp,%ebp
801062f4:	83 ec 14             	sub    $0x14,%esp
  return mappages(pgdir, va, size, pa, perm);
801062f7:	ff 75 18             	pushl  0x18(%ebp)
801062fa:	ff 75 14             	pushl  0x14(%ebp)
801062fd:	ff 75 10             	pushl  0x10(%ebp)
80106300:	ff 75 0c             	pushl  0xc(%ebp)
80106303:	ff 75 08             	pushl  0x8(%ebp)
80106306:	e8 6e ff ff ff       	call   80106279 <mappages>
}
8010630b:	c9                   	leave  
8010630c:	c3                   	ret    

8010630d <switchkvm>:

// Switch h/w page table register to the kernel-only page table,
// for when no process is running.
void switchkvm(void)
{
  lcr3(V2P(kpgdir)); // switch to the kernel page table
8010630d:	a1 14 37 12 80       	mov    0x80123714,%eax
    if (a < (void*) KERNBASE)
80106312:	3d ff ff ff 7f       	cmp    $0x7fffffff,%eax
80106317:	76 09                	jbe    80106322 <switchkvm+0x15>
    return (uint)a - KERNBASE;
80106319:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
8010631e:	0f 22 d8             	mov    %eax,%cr3
80106321:	c3                   	ret    
{
80106322:	55                   	push   %ebp
80106323:	89 e5                	mov    %esp,%ebp
80106325:	83 ec 14             	sub    $0x14,%esp
        panic("V2P on address < KERNBASE "
80106328:	68 28 6f 10 80       	push   $0x80106f28
8010632d:	e8 16 a0 ff ff       	call   80100348 <panic>

80106332 <switchuvm>:
}

// Switch TSS and h/w page table to correspond to process p.
void switchuvm(struct proc *p)
{
80106332:	55                   	push   %ebp
80106333:	89 e5                	mov    %esp,%ebp
80106335:	57                   	push   %edi
80106336:	56                   	push   %esi
80106337:	53                   	push   %ebx
80106338:	83 ec 1c             	sub    $0x1c,%esp
8010633b:	8b 75 08             	mov    0x8(%ebp),%esi
  if (p == 0)
8010633e:	85 f6                	test   %esi,%esi
80106340:	0f 84 e4 00 00 00    	je     8010642a <switchuvm+0xf8>
    panic("switchuvm: no process");
  if (p->kstack == 0)
80106346:	83 7e 08 00          	cmpl   $0x0,0x8(%esi)
8010634a:	0f 84 e7 00 00 00    	je     80106437 <switchuvm+0x105>
    panic("switchuvm: no kstack");
  if (p->pgdir == 0)
80106350:	83 7e 04 00          	cmpl   $0x0,0x4(%esi)
80106354:	0f 84 ea 00 00 00    	je     80106444 <switchuvm+0x112>
    panic("switchuvm: no pgdir");

  pushcli();
8010635a:	e8 df da ff ff       	call   80103e3e <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
8010635f:	e8 7c ce ff ff       	call   801031e0 <mycpu>
80106364:	89 c3                	mov    %eax,%ebx
80106366:	e8 75 ce ff ff       	call   801031e0 <mycpu>
8010636b:	8d 78 08             	lea    0x8(%eax),%edi
8010636e:	e8 6d ce ff ff       	call   801031e0 <mycpu>
80106373:	83 c0 08             	add    $0x8,%eax
80106376:	c1 e8 10             	shr    $0x10,%eax
80106379:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010637c:	e8 5f ce ff ff       	call   801031e0 <mycpu>
80106381:	83 c0 08             	add    $0x8,%eax
80106384:	c1 e8 18             	shr    $0x18,%eax
80106387:	66 c7 83 98 00 00 00 	movw   $0x67,0x98(%ebx)
8010638e:	67 00 
80106390:	66 89 bb 9a 00 00 00 	mov    %di,0x9a(%ebx)
80106397:	0f b6 4d e4          	movzbl -0x1c(%ebp),%ecx
8010639b:	88 8b 9c 00 00 00    	mov    %cl,0x9c(%ebx)
801063a1:	0f b6 93 9d 00 00 00 	movzbl 0x9d(%ebx),%edx
801063a8:	83 e2 f0             	and    $0xfffffff0,%edx
801063ab:	83 ca 19             	or     $0x19,%edx
801063ae:	83 e2 9f             	and    $0xffffff9f,%edx
801063b1:	83 ca 80             	or     $0xffffff80,%edx
801063b4:	88 93 9d 00 00 00    	mov    %dl,0x9d(%ebx)
801063ba:	c6 83 9e 00 00 00 40 	movb   $0x40,0x9e(%ebx)
801063c1:	88 83 9f 00 00 00    	mov    %al,0x9f(%ebx)
                                sizeof(mycpu()->ts) - 1, 0);
  mycpu()->gdt[SEG_TSS].s = 0;
801063c7:	e8 14 ce ff ff       	call   801031e0 <mycpu>
801063cc:	0f b6 90 9d 00 00 00 	movzbl 0x9d(%eax),%edx
801063d3:	83 e2 ef             	and    $0xffffffef,%edx
801063d6:	88 90 9d 00 00 00    	mov    %dl,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
801063dc:	e8 ff cd ff ff       	call   801031e0 <mycpu>
801063e1:	66 c7 40 10 10 00    	movw   $0x10,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
801063e7:	8b 5e 08             	mov    0x8(%esi),%ebx
801063ea:	e8 f1 cd ff ff       	call   801031e0 <mycpu>
801063ef:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801063f5:	89 58 0c             	mov    %ebx,0xc(%eax)
  // setting IOPL=0 in eflags *and* iomb beyond the tss segment limit
  // forbids I/O instructions (e.g., inb and outb) from user space
  mycpu()->ts.iomb = (ushort)0xFFFF;
801063f8:	e8 e3 cd ff ff       	call   801031e0 <mycpu>
801063fd:	66 c7 40 6e ff ff    	movw   $0xffff,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80106403:	b8 28 00 00 00       	mov    $0x28,%eax
80106408:	0f 00 d8             	ltr    %ax
  ltr(SEG_TSS << 3);
  lcr3(V2P(p->pgdir)); // switch to process's address space
8010640b:	8b 46 04             	mov    0x4(%esi),%eax
    if (a < (void*) KERNBASE)
8010640e:	3d ff ff ff 7f       	cmp    $0x7fffffff,%eax
80106413:	76 3c                	jbe    80106451 <switchuvm+0x11f>
    return (uint)a - KERNBASE;
80106415:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
8010641a:	0f 22 d8             	mov    %eax,%cr3
  popcli();
8010641d:	e8 59 da ff ff       	call   80103e7b <popcli>
}
80106422:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106425:	5b                   	pop    %ebx
80106426:	5e                   	pop    %esi
80106427:	5f                   	pop    %edi
80106428:	5d                   	pop    %ebp
80106429:	c3                   	ret    
    panic("switchuvm: no process");
8010642a:	83 ec 0c             	sub    $0xc,%esp
8010642d:	68 51 76 10 80       	push   $0x80107651
80106432:	e8 11 9f ff ff       	call   80100348 <panic>
    panic("switchuvm: no kstack");
80106437:	83 ec 0c             	sub    $0xc,%esp
8010643a:	68 67 76 10 80       	push   $0x80107667
8010643f:	e8 04 9f ff ff       	call   80100348 <panic>
    panic("switchuvm: no pgdir");
80106444:	83 ec 0c             	sub    $0xc,%esp
80106447:	68 7c 76 10 80       	push   $0x8010767c
8010644c:	e8 f7 9e ff ff       	call   80100348 <panic>
        panic("V2P on address < KERNBASE "
80106451:	83 ec 0c             	sub    $0xc,%esp
80106454:	68 28 6f 10 80       	push   $0x80106f28
80106459:	e8 ea 9e ff ff       	call   80100348 <panic>

8010645e <inituvm>:

// Load the initcode into address 0 of pgdir.
// sz must be less than a page.
void inituvm(pde_t *pgdir, char *init, uint sz)
{
8010645e:	55                   	push   %ebp
8010645f:	89 e5                	mov    %esp,%ebp
80106461:	56                   	push   %esi
80106462:	53                   	push   %ebx
80106463:	8b 75 10             	mov    0x10(%ebp),%esi
  char *mem;

  if (sz >= PGSIZE)
80106466:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
8010646c:	77 54                	ja     801064c2 <inituvm+0x64>
    panic("inituvm: more than a page");
  mem = kalloc();
8010646e:	e8 5c bc ff ff       	call   801020cf <kalloc>
80106473:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
80106475:	83 ec 04             	sub    $0x4,%esp
80106478:	68 00 10 00 00       	push   $0x1000
8010647d:	6a 00                	push   $0x0
8010647f:	50                   	push   %eax
80106480:	e8 42 db ff ff       	call   80103fc7 <memset>
    if (a < (void*) KERNBASE)
80106485:	83 c4 10             	add    $0x10,%esp
80106488:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
8010648e:	76 3f                	jbe    801064cf <inituvm+0x71>
    return (uint)a - KERNBASE;
80106490:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W | PTE_U);
80106496:	83 ec 0c             	sub    $0xc,%esp
80106499:	6a 06                	push   $0x6
8010649b:	50                   	push   %eax
8010649c:	68 00 10 00 00       	push   $0x1000
801064a1:	6a 00                	push   $0x0
801064a3:	ff 75 08             	pushl  0x8(%ebp)
801064a6:	e8 ce fd ff ff       	call   80106279 <mappages>
  memmove(mem, init, sz);
801064ab:	83 c4 1c             	add    $0x1c,%esp
801064ae:	56                   	push   %esi
801064af:	ff 75 0c             	pushl  0xc(%ebp)
801064b2:	53                   	push   %ebx
801064b3:	e8 8a db ff ff       	call   80104042 <memmove>
}
801064b8:	83 c4 10             	add    $0x10,%esp
801064bb:	8d 65 f8             	lea    -0x8(%ebp),%esp
801064be:	5b                   	pop    %ebx
801064bf:	5e                   	pop    %esi
801064c0:	5d                   	pop    %ebp
801064c1:	c3                   	ret    
    panic("inituvm: more than a page");
801064c2:	83 ec 0c             	sub    $0xc,%esp
801064c5:	68 90 76 10 80       	push   $0x80107690
801064ca:	e8 79 9e ff ff       	call   80100348 <panic>
        panic("V2P on address < KERNBASE "
801064cf:	83 ec 0c             	sub    $0xc,%esp
801064d2:	68 28 6f 10 80       	push   $0x80106f28
801064d7:	e8 6c 9e ff ff       	call   80100348 <panic>

801064dc <loaduvm>:

// Load a program segment into pgdir.  addr must be page-aligned
// and the pages from addr to addr+sz must already be mapped.
int loaduvm(pde_t *pgdir, char *addr, struct inode *ip, uint offset, uint sz)
{
801064dc:	55                   	push   %ebp
801064dd:	89 e5                	mov    %esp,%ebp
801064df:	57                   	push   %edi
801064e0:	56                   	push   %esi
801064e1:	53                   	push   %ebx
801064e2:	83 ec 0c             	sub    $0xc,%esp
801064e5:	8b 7d 18             	mov    0x18(%ebp),%edi
  uint i, pa, n;
  pte_t *pte;

  if ((uint)addr % PGSIZE != 0)
801064e8:	f7 45 0c ff 0f 00 00 	testl  $0xfff,0xc(%ebp)
801064ef:	75 07                	jne    801064f8 <loaduvm+0x1c>
    panic("loaduvm: addr must be page aligned");
  for (i = 0; i < sz; i += PGSIZE)
801064f1:	bb 00 00 00 00       	mov    $0x0,%ebx
801064f6:	eb 43                	jmp    8010653b <loaduvm+0x5f>
    panic("loaduvm: addr must be page aligned");
801064f8:	83 ec 0c             	sub    $0xc,%esp
801064fb:	68 18 77 10 80       	push   $0x80107718
80106500:	e8 43 9e ff ff       	call   80100348 <panic>
  {
    if ((pte = walkpgdir(pgdir, addr + i, 0)) == 0)
      panic("loaduvm: address should exist");
80106505:	83 ec 0c             	sub    $0xc,%esp
80106508:	68 aa 76 10 80       	push   $0x801076aa
8010650d:	e8 36 9e ff ff       	call   80100348 <panic>
    pa = PTE_ADDR(*pte);
    if (sz - i < PGSIZE)
      n = sz - i;
    else
      n = PGSIZE;
    if (readi(ip, P2V(pa), offset + i, n) != n)
80106512:	89 da                	mov    %ebx,%edx
80106514:	03 55 14             	add    0x14(%ebp),%edx
    if (a > KERNBASE)
80106517:	3d 00 00 00 80       	cmp    $0x80000000,%eax
8010651c:	77 51                	ja     8010656f <loaduvm+0x93>
    return (char*)a + KERNBASE;
8010651e:	05 00 00 00 80       	add    $0x80000000,%eax
80106523:	56                   	push   %esi
80106524:	52                   	push   %edx
80106525:	50                   	push   %eax
80106526:	ff 75 10             	pushl  0x10(%ebp)
80106529:	e8 33 b2 ff ff       	call   80101761 <readi>
8010652e:	83 c4 10             	add    $0x10,%esp
80106531:	39 f0                	cmp    %esi,%eax
80106533:	75 54                	jne    80106589 <loaduvm+0xad>
  for (i = 0; i < sz; i += PGSIZE)
80106535:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010653b:	39 fb                	cmp    %edi,%ebx
8010653d:	73 3d                	jae    8010657c <loaduvm+0xa0>
    if ((pte = walkpgdir(pgdir, addr + i, 0)) == 0)
8010653f:	89 da                	mov    %ebx,%edx
80106541:	03 55 0c             	add    0xc(%ebp),%edx
80106544:	b9 00 00 00 00       	mov    $0x0,%ecx
80106549:	8b 45 08             	mov    0x8(%ebp),%eax
8010654c:	e8 2c fb ff ff       	call   8010607d <walkpgdir>
80106551:	85 c0                	test   %eax,%eax
80106553:	74 b0                	je     80106505 <loaduvm+0x29>
    pa = PTE_ADDR(*pte);
80106555:	8b 00                	mov    (%eax),%eax
80106557:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if (sz - i < PGSIZE)
8010655c:	89 fe                	mov    %edi,%esi
8010655e:	29 de                	sub    %ebx,%esi
80106560:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106566:	76 aa                	jbe    80106512 <loaduvm+0x36>
      n = PGSIZE;
80106568:	be 00 10 00 00       	mov    $0x1000,%esi
8010656d:	eb a3                	jmp    80106512 <loaduvm+0x36>
        panic("P2V on address > KERNBASE");
8010656f:	83 ec 0c             	sub    $0xc,%esp
80106572:	68 58 72 10 80       	push   $0x80107258
80106577:	e8 cc 9d ff ff       	call   80100348 <panic>
      return -1;
  }
  return 0;
8010657c:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106581:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106584:	5b                   	pop    %ebx
80106585:	5e                   	pop    %esi
80106586:	5f                   	pop    %edi
80106587:	5d                   	pop    %ebp
80106588:	c3                   	ret    
      return -1;
80106589:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010658e:	eb f1                	jmp    80106581 <loaduvm+0xa5>

80106590 <deallocuvm>:
// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
{
80106590:	55                   	push   %ebp
80106591:	89 e5                	mov    %esp,%ebp
80106593:	57                   	push   %edi
80106594:	56                   	push   %esi
80106595:	53                   	push   %ebx
80106596:	83 ec 0c             	sub    $0xc,%esp
  pte_t *pte;
  uint a, pa;

  if (newsz >= oldsz)
80106599:	8b 45 0c             	mov    0xc(%ebp),%eax
8010659c:	39 45 10             	cmp    %eax,0x10(%ebp)
8010659f:	0f 83 d7 00 00 00    	jae    8010667c <deallocuvm+0xec>
    return oldsz;

  a = PGROUNDUP(newsz);
801065a5:	8b 45 10             	mov    0x10(%ebp),%eax
801065a8:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
801065ae:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  while (a < oldsz)
801065b4:	eb 58                	jmp    8010660e <deallocuvm+0x7e>
  {
    pte = walkpgdir(pgdir, (char *)a, 0);
    if (!pte)
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
801065b6:	c1 eb 16             	shr    $0x16,%ebx
801065b9:	83 c3 01             	add    $0x1,%ebx
801065bc:	c1 e3 16             	shl    $0x16,%ebx
801065bf:	81 eb 00 10 00 00    	sub    $0x1000,%ebx
801065c5:	eb 41                	jmp    80106608 <deallocuvm+0x78>
    else if ((PTE_P & *pte) != 0)
    {
      pa = PTE_ADDR(*pte);
      if (pa == 0)
        panic("kfree");
801065c7:	83 ec 0c             	sub    $0xc,%esp
801065ca:	68 b6 6f 10 80       	push   $0x80106fb6
801065cf:	e8 74 9d ff ff       	call   80100348 <panic>
    if (a > KERNBASE)
801065d4:	81 fe 00 00 00 80    	cmp    $0x80000000,%esi
801065da:	0f 87 8c 00 00 00    	ja     8010666c <deallocuvm+0xdc>
    return (char*)a + KERNBASE;
801065e0:	81 c6 00 00 00 80    	add    $0x80000000,%esi
        *pte &= ~(PTE_P);
      }
      else
      {
        char *v = P2V(pa);
        kfree(v);
801065e6:	83 ec 0c             	sub    $0xc,%esp
801065e9:	56                   	push   %esi
801065ea:	e8 a3 b9 ff ff       	call   80101f92 <kfree>
        *pte = 0;
801065ef:	c7 07 00 00 00 00    	movl   $0x0,(%edi)
801065f5:	83 c4 10             	add    $0x10,%esp
      }
      release(&pgo.l);
801065f8:	83 ec 0c             	sub    $0xc,%esp
801065fb:	68 e0 56 11 80       	push   $0x801156e0
80106600:	e8 7b d9 ff ff       	call   80103f80 <release>
80106605:	83 c4 10             	add    $0x10,%esp
    }
    a += PGSIZE;
80106608:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  while (a < oldsz)
8010660e:	3b 5d 0c             	cmp    0xc(%ebp),%ebx
80106611:	73 66                	jae    80106679 <deallocuvm+0xe9>
    pte = walkpgdir(pgdir, (char *)a, 0);
80106613:	b9 00 00 00 00       	mov    $0x0,%ecx
80106618:	89 da                	mov    %ebx,%edx
8010661a:	8b 45 08             	mov    0x8(%ebp),%eax
8010661d:	e8 5b fa ff ff       	call   8010607d <walkpgdir>
80106622:	89 c7                	mov    %eax,%edi
    if (!pte)
80106624:	85 c0                	test   %eax,%eax
80106626:	74 8e                	je     801065b6 <deallocuvm+0x26>
    else if ((PTE_P & *pte) != 0)
80106628:	8b 30                	mov    (%eax),%esi
8010662a:	f7 c6 01 00 00 00    	test   $0x1,%esi
80106630:	74 d6                	je     80106608 <deallocuvm+0x78>
      if (pa == 0)
80106632:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
80106638:	74 8d                	je     801065c7 <deallocuvm+0x37>
      acquire(&pgo.l);
8010663a:	83 ec 0c             	sub    $0xc,%esp
8010663d:	68 e0 56 11 80       	push   $0x801156e0
80106642:	e8 d4 d8 ff ff       	call   80103f1b <acquire>
      if (pgo.pgr[pa >> 12])
80106647:	89 f2                	mov    %esi,%edx
80106649:	c1 ea 0c             	shr    $0xc,%edx
8010664c:	0f b6 82 14 57 11 80 	movzbl -0x7feea8ec(%edx),%eax
80106653:	83 c4 10             	add    $0x10,%esp
80106656:	84 c0                	test   %al,%al
80106658:	0f 84 76 ff ff ff    	je     801065d4 <deallocuvm+0x44>
        pgo.pgr[pa >> 12] -= 1;
8010665e:	83 e8 01             	sub    $0x1,%eax
80106661:	88 82 14 57 11 80    	mov    %al,-0x7feea8ec(%edx)
        *pte &= ~(PTE_P);
80106667:	83 27 fe             	andl   $0xfffffffe,(%edi)
8010666a:	eb 8c                	jmp    801065f8 <deallocuvm+0x68>
        panic("P2V on address > KERNBASE");
8010666c:	83 ec 0c             	sub    $0xc,%esp
8010666f:	68 58 72 10 80       	push   $0x80107258
80106674:	e8 cf 9c ff ff       	call   80100348 <panic>
  }
  return newsz;
80106679:	8b 45 10             	mov    0x10(%ebp),%eax
}
8010667c:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010667f:	5b                   	pop    %ebx
80106680:	5e                   	pop    %esi
80106681:	5f                   	pop    %edi
80106682:	5d                   	pop    %ebp
80106683:	c3                   	ret    

80106684 <allocuvm>:
{
80106684:	55                   	push   %ebp
80106685:	89 e5                	mov    %esp,%ebp
80106687:	57                   	push   %edi
80106688:	56                   	push   %esi
80106689:	53                   	push   %ebx
8010668a:	83 ec 1c             	sub    $0x1c,%esp
8010668d:	8b 7d 10             	mov    0x10(%ebp),%edi
  if (newsz >= KERNBASE)
80106690:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80106693:	85 ff                	test   %edi,%edi
80106695:	0f 88 d8 00 00 00    	js     80106773 <allocuvm+0xef>
  if (newsz < oldsz)
8010669b:	3b 7d 0c             	cmp    0xc(%ebp),%edi
8010669e:	72 66                	jb     80106706 <allocuvm+0x82>
  a = PGROUNDUP(oldsz);
801066a0:	8b 45 0c             	mov    0xc(%ebp),%eax
801066a3:	8d b0 ff 0f 00 00    	lea    0xfff(%eax),%esi
801066a9:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
  for (; a < newsz; a += PGSIZE)
801066af:	39 fe                	cmp    %edi,%esi
801066b1:	0f 83 c3 00 00 00    	jae    8010677a <allocuvm+0xf6>
    mem = kalloc();
801066b7:	e8 13 ba ff ff       	call   801020cf <kalloc>
801066bc:	89 c3                	mov    %eax,%ebx
    if (mem == 0)
801066be:	85 c0                	test   %eax,%eax
801066c0:	74 4c                	je     8010670e <allocuvm+0x8a>
    memset(mem, 0, PGSIZE);
801066c2:	83 ec 04             	sub    $0x4,%esp
801066c5:	68 00 10 00 00       	push   $0x1000
801066ca:	6a 00                	push   $0x0
801066cc:	50                   	push   %eax
801066cd:	e8 f5 d8 ff ff       	call   80103fc7 <memset>
    if (a < (void*) KERNBASE)
801066d2:	83 c4 10             	add    $0x10,%esp
801066d5:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
801066db:	76 59                	jbe    80106736 <allocuvm+0xb2>
    return (uint)a - KERNBASE;
801066dd:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
    if (mappages(pgdir, (char *)a, PGSIZE, V2P(mem), PTE_W | PTE_U) < 0)
801066e3:	83 ec 0c             	sub    $0xc,%esp
801066e6:	6a 06                	push   $0x6
801066e8:	50                   	push   %eax
801066e9:	68 00 10 00 00       	push   $0x1000
801066ee:	56                   	push   %esi
801066ef:	ff 75 08             	pushl  0x8(%ebp)
801066f2:	e8 82 fb ff ff       	call   80106279 <mappages>
801066f7:	83 c4 20             	add    $0x20,%esp
801066fa:	85 c0                	test   %eax,%eax
801066fc:	78 45                	js     80106743 <allocuvm+0xbf>
  for (; a < newsz; a += PGSIZE)
801066fe:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106704:	eb a9                	jmp    801066af <allocuvm+0x2b>
    return oldsz;
80106706:	8b 45 0c             	mov    0xc(%ebp),%eax
80106709:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010670c:	eb 6c                	jmp    8010677a <allocuvm+0xf6>
      cprintf("allocuvm out of memory\n");
8010670e:	83 ec 0c             	sub    $0xc,%esp
80106711:	68 c8 76 10 80       	push   $0x801076c8
80106716:	e8 f0 9e ff ff       	call   8010060b <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
8010671b:	83 c4 0c             	add    $0xc,%esp
8010671e:	ff 75 0c             	pushl  0xc(%ebp)
80106721:	57                   	push   %edi
80106722:	ff 75 08             	pushl  0x8(%ebp)
80106725:	e8 66 fe ff ff       	call   80106590 <deallocuvm>
      return 0;
8010672a:	83 c4 10             	add    $0x10,%esp
8010672d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80106734:	eb 44                	jmp    8010677a <allocuvm+0xf6>
        panic("V2P on address < KERNBASE "
80106736:	83 ec 0c             	sub    $0xc,%esp
80106739:	68 28 6f 10 80       	push   $0x80106f28
8010673e:	e8 05 9c ff ff       	call   80100348 <panic>
      cprintf("allocuvm out of memory (2)\n");
80106743:	83 ec 0c             	sub    $0xc,%esp
80106746:	68 e0 76 10 80       	push   $0x801076e0
8010674b:	e8 bb 9e ff ff       	call   8010060b <cprintf>
      deallocuvm(pgdir, newsz, oldsz);
80106750:	83 c4 0c             	add    $0xc,%esp
80106753:	ff 75 0c             	pushl  0xc(%ebp)
80106756:	57                   	push   %edi
80106757:	ff 75 08             	pushl  0x8(%ebp)
8010675a:	e8 31 fe ff ff       	call   80106590 <deallocuvm>
      kfree(mem);
8010675f:	89 1c 24             	mov    %ebx,(%esp)
80106762:	e8 2b b8 ff ff       	call   80101f92 <kfree>
      return 0;
80106767:	83 c4 10             	add    $0x10,%esp
8010676a:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80106771:	eb 07                	jmp    8010677a <allocuvm+0xf6>
    return 0;
80106773:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
}
8010677a:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010677d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106780:	5b                   	pop    %ebx
80106781:	5e                   	pop    %esi
80106782:	5f                   	pop    %edi
80106783:	5d                   	pop    %ebp
80106784:	c3                   	ret    

80106785 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void freevm(pde_t *pgdir)
{
80106785:	55                   	push   %ebp
80106786:	89 e5                	mov    %esp,%ebp
80106788:	56                   	push   %esi
80106789:	53                   	push   %ebx
8010678a:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if (pgdir == 0)
8010678d:	85 f6                	test   %esi,%esi
8010678f:	74 1a                	je     801067ab <freevm+0x26>
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
80106791:	83 ec 04             	sub    $0x4,%esp
80106794:	6a 00                	push   $0x0
80106796:	68 00 00 00 80       	push   $0x80000000
8010679b:	56                   	push   %esi
8010679c:	e8 ef fd ff ff       	call   80106590 <deallocuvm>
  for (i = 0; i < NPDENTRIES; i++)
801067a1:	83 c4 10             	add    $0x10,%esp
801067a4:	bb 00 00 00 00       	mov    $0x0,%ebx
801067a9:	eb 1d                	jmp    801067c8 <freevm+0x43>
    panic("freevm: no pgdir");
801067ab:	83 ec 0c             	sub    $0xc,%esp
801067ae:	68 fc 76 10 80       	push   $0x801076fc
801067b3:	e8 90 9b ff ff       	call   80100348 <panic>
        panic("P2V on address > KERNBASE");
801067b8:	83 ec 0c             	sub    $0xc,%esp
801067bb:	68 58 72 10 80       	push   $0x80107258
801067c0:	e8 83 9b ff ff       	call   80100348 <panic>
  for (i = 0; i < NPDENTRIES; i++)
801067c5:	83 c3 01             	add    $0x1,%ebx
801067c8:	81 fb ff 03 00 00    	cmp    $0x3ff,%ebx
801067ce:	77 26                	ja     801067f6 <freevm+0x71>
  {
    if (pgdir[i] & PTE_P)
801067d0:	8b 04 9e             	mov    (%esi,%ebx,4),%eax
801067d3:	a8 01                	test   $0x1,%al
801067d5:	74 ee                	je     801067c5 <freevm+0x40>
801067d7:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if (a > KERNBASE)
801067dc:	3d 00 00 00 80       	cmp    $0x80000000,%eax
801067e1:	77 d5                	ja     801067b8 <freevm+0x33>
    return (char*)a + KERNBASE;
801067e3:	05 00 00 00 80       	add    $0x80000000,%eax
    {
      char *v = P2V(PTE_ADDR(pgdir[i]));
      kfree(v);
801067e8:	83 ec 0c             	sub    $0xc,%esp
801067eb:	50                   	push   %eax
801067ec:	e8 a1 b7 ff ff       	call   80101f92 <kfree>
801067f1:	83 c4 10             	add    $0x10,%esp
801067f4:	eb cf                	jmp    801067c5 <freevm+0x40>
    }
  }
  kfree((char *)pgdir);
801067f6:	83 ec 0c             	sub    $0xc,%esp
801067f9:	56                   	push   %esi
801067fa:	e8 93 b7 ff ff       	call   80101f92 <kfree>
}
801067ff:	83 c4 10             	add    $0x10,%esp
80106802:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106805:	5b                   	pop    %ebx
80106806:	5e                   	pop    %esi
80106807:	5d                   	pop    %ebp
80106808:	c3                   	ret    

80106809 <setupkvm>:
{
80106809:	55                   	push   %ebp
8010680a:	89 e5                	mov    %esp,%ebp
8010680c:	56                   	push   %esi
8010680d:	53                   	push   %ebx
  if ((pgdir = (pde_t *)kalloc()) == 0)
8010680e:	e8 bc b8 ff ff       	call   801020cf <kalloc>
80106813:	89 c6                	mov    %eax,%esi
80106815:	85 c0                	test   %eax,%eax
80106817:	74 55                	je     8010686e <setupkvm+0x65>
  memset(pgdir, 0, PGSIZE);
80106819:	83 ec 04             	sub    $0x4,%esp
8010681c:	68 00 10 00 00       	push   $0x1000
80106821:	6a 00                	push   $0x0
80106823:	50                   	push   %eax
80106824:	e8 9e d7 ff ff       	call   80103fc7 <memset>
  for (k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106829:	83 c4 10             	add    $0x10,%esp
8010682c:	bb 20 a4 10 80       	mov    $0x8010a420,%ebx
80106831:	81 fb 60 a4 10 80    	cmp    $0x8010a460,%ebx
80106837:	73 35                	jae    8010686e <setupkvm+0x65>
                 (uint)k->phys_start, k->perm) < 0)
80106839:	8b 43 04             	mov    0x4(%ebx),%eax
    if (mappages(pgdir, k->virt, k->phys_end - k->phys_start,
8010683c:	83 ec 0c             	sub    $0xc,%esp
8010683f:	ff 73 0c             	pushl  0xc(%ebx)
80106842:	50                   	push   %eax
80106843:	8b 53 08             	mov    0x8(%ebx),%edx
80106846:	29 c2                	sub    %eax,%edx
80106848:	52                   	push   %edx
80106849:	ff 33                	pushl  (%ebx)
8010684b:	56                   	push   %esi
8010684c:	e8 28 fa ff ff       	call   80106279 <mappages>
80106851:	83 c4 20             	add    $0x20,%esp
80106854:	85 c0                	test   %eax,%eax
80106856:	78 05                	js     8010685d <setupkvm+0x54>
  for (k = kmap; k < &kmap[NELEM(kmap)]; k++)
80106858:	83 c3 10             	add    $0x10,%ebx
8010685b:	eb d4                	jmp    80106831 <setupkvm+0x28>
      freevm(pgdir);
8010685d:	83 ec 0c             	sub    $0xc,%esp
80106860:	56                   	push   %esi
80106861:	e8 1f ff ff ff       	call   80106785 <freevm>
      return 0;
80106866:	83 c4 10             	add    $0x10,%esp
80106869:	be 00 00 00 00       	mov    $0x0,%esi
}
8010686e:	89 f0                	mov    %esi,%eax
80106870:	8d 65 f8             	lea    -0x8(%ebp),%esp
80106873:	5b                   	pop    %ebx
80106874:	5e                   	pop    %esi
80106875:	5d                   	pop    %ebp
80106876:	c3                   	ret    

80106877 <kvmalloc>:
{
80106877:	55                   	push   %ebp
80106878:	89 e5                	mov    %esp,%ebp
8010687a:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
8010687d:	e8 87 ff ff ff       	call   80106809 <setupkvm>
80106882:	a3 14 37 12 80       	mov    %eax,0x80123714
  switchkvm();
80106887:	e8 81 fa ff ff       	call   8010630d <switchkvm>
}
8010688c:	c9                   	leave  
8010688d:	c3                   	ret    

8010688e <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void clearpteu(pde_t *pgdir, char *uva)
{
8010688e:	55                   	push   %ebp
8010688f:	89 e5                	mov    %esp,%ebp
80106891:	83 ec 08             	sub    $0x8,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80106894:	b9 00 00 00 00       	mov    $0x0,%ecx
80106899:	8b 55 0c             	mov    0xc(%ebp),%edx
8010689c:	8b 45 08             	mov    0x8(%ebp),%eax
8010689f:	e8 d9 f7 ff ff       	call   8010607d <walkpgdir>
  if (pte == 0)
801068a4:	85 c0                	test   %eax,%eax
801068a6:	74 05                	je     801068ad <clearpteu+0x1f>
    panic("clearpteu");
  *pte &= ~PTE_U;
801068a8:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
801068ab:	c9                   	leave  
801068ac:	c3                   	ret    
    panic("clearpteu");
801068ad:	83 ec 0c             	sub    $0xc,%esp
801068b0:	68 0d 77 10 80       	push   $0x8010770d
801068b5:	e8 8e 9a ff ff       	call   80100348 <panic>

801068ba <pfh>:

// Given a parent process's page table, create a copy
// of it for a child.
int pfh(pde_t *pgdir, uint sz, uint _rcr2)
{
801068ba:	55                   	push   %ebp
801068bb:	89 e5                	mov    %esp,%ebp
801068bd:	57                   	push   %edi
801068be:	56                   	push   %esi
801068bf:	53                   	push   %ebx
801068c0:	83 ec 0c             	sub    $0xc,%esp
801068c3:	8b 5d 08             	mov    0x8(%ebp),%ebx
  pte_t *pte;
  char *mem;
  if ((pte = walkpgdir(pgdir, (void *)_rcr2, 0)) == 0)
801068c6:	b9 00 00 00 00       	mov    $0x0,%ecx
801068cb:	8b 55 10             	mov    0x10(%ebp),%edx
801068ce:	89 d8                	mov    %ebx,%eax
801068d0:	e8 a8 f7 ff ff       	call   8010607d <walkpgdir>
801068d5:	85 c0                	test   %eax,%eax
801068d7:	0f 84 85 00 00 00    	je     80106962 <pfh+0xa8>
801068dd:	89 c6                	mov    %eax,%esi
      return 1;
    }
  }
  else
  {
    if (!(PTE_W & *pte) && (PTE_P & *pte) && (PTE_U & *pte))
801068df:	8b 00                	mov    (%eax),%eax
801068e1:	89 c2                	mov    %eax,%edx
801068e3:	83 e2 07             	and    $0x7,%edx
801068e6:	83 fa 05             	cmp    $0x5,%edx
801068e9:	0f 84 e7 00 00 00    	je     801069d6 <pfh+0x11c>
      {
        *pte |= PTE_W;
      }
      release(&pgo.l);
    }
    else if (!(*pte & PTE_P))
801068ef:	a8 01                	test   $0x1,%al
801068f1:	0f 85 a7 01 00 00    	jne    80106a9e <pfh+0x1e4>
    {
      if ((mem = kalloc()) == 0)
801068f7:	e8 d3 b7 ff ff       	call   801020cf <kalloc>
801068fc:	89 c7                	mov    %eax,%edi
801068fe:	85 c0                	test   %eax,%eax
80106900:	0f 84 dd 01 00 00    	je     80106ae3 <pfh+0x229>
      {
        return 1;
      }
      memset(mem, 0, PGSIZE);
80106906:	83 ec 04             	sub    $0x4,%esp
80106909:	68 00 10 00 00       	push   $0x1000
8010690e:	6a 00                	push   $0x0
80106910:	50                   	push   %eax
80106911:	e8 b1 d6 ff ff       	call   80103fc7 <memset>
    if (a < (void*) KERNBASE)
80106916:	83 c4 10             	add    $0x10,%esp
80106919:	81 ff ff ff ff 7f    	cmp    $0x7fffffff,%edi
8010691f:	0f 86 6c 01 00 00    	jbe    80106a91 <pfh+0x1d7>
    return (uint)a - KERNBASE;
80106925:	81 c7 00 00 00 80    	add    $0x80000000,%edi
      if (mappages(myproc()->pgdir, (char *)PGROUNDDOWN(_rcr2), PGSIZE, V2P(mem), PTE_W | PTE_U) < 0)
8010692b:	8b 75 10             	mov    0x10(%ebp),%esi
8010692e:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
80106934:	e8 1e c9 ff ff       	call   80103257 <myproc>
80106939:	83 ec 0c             	sub    $0xc,%esp
8010693c:	6a 06                	push   $0x6
8010693e:	57                   	push   %edi
8010693f:	68 00 10 00 00       	push   $0x1000
80106944:	56                   	push   %esi
80106945:	ff 70 04             	pushl  0x4(%eax)
80106948:	e8 2c f9 ff ff       	call   80106279 <mappages>
8010694d:	83 c4 20             	add    $0x20,%esp
80106950:	85 c0                	test   %eax,%eax
80106952:	0f 89 52 01 00 00    	jns    80106aaa <pfh+0x1f0>
      {
        return 1;
80106958:	b8 01 00 00 00       	mov    $0x1,%eax
8010695d:	e9 5e 01 00 00       	jmp    80106ac0 <pfh+0x206>
    if ((mem = kalloc()) == 0)
80106962:	e8 68 b7 ff ff       	call   801020cf <kalloc>
80106967:	89 c7                	mov    %eax,%edi
80106969:	85 c0                	test   %eax,%eax
8010696b:	0f 84 64 01 00 00    	je     80106ad5 <pfh+0x21b>
    memset(mem, 0, PGSIZE);
80106971:	83 ec 04             	sub    $0x4,%esp
80106974:	68 00 10 00 00       	push   $0x1000
80106979:	6a 00                	push   $0x0
8010697b:	50                   	push   %eax
8010697c:	e8 46 d6 ff ff       	call   80103fc7 <memset>
    if (a < (void*) KERNBASE)
80106981:	83 c4 10             	add    $0x10,%esp
80106984:	81 ff ff ff ff 7f    	cmp    $0x7fffffff,%edi
8010698a:	76 3d                	jbe    801069c9 <pfh+0x10f>
    return (uint)a - KERNBASE;
8010698c:	81 c7 00 00 00 80    	add    $0x80000000,%edi
    if (mappages(myproc()->pgdir, (char *)PGROUNDDOWN(_rcr2), PGSIZE, V2P(mem), PTE_W | PTE_U) < 0)
80106992:	8b 75 10             	mov    0x10(%ebp),%esi
80106995:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
8010699b:	e8 b7 c8 ff ff       	call   80103257 <myproc>
801069a0:	83 ec 0c             	sub    $0xc,%esp
801069a3:	6a 06                	push   $0x6
801069a5:	57                   	push   %edi
801069a6:	68 00 10 00 00       	push   $0x1000
801069ab:	56                   	push   %esi
801069ac:	ff 70 04             	pushl  0x4(%eax)
801069af:	e8 c5 f8 ff ff       	call   80106279 <mappages>
801069b4:	83 c4 20             	add    $0x20,%esp
801069b7:	85 c0                	test   %eax,%eax
801069b9:	0f 89 eb 00 00 00    	jns    80106aaa <pfh+0x1f0>
      return 1;
801069bf:	b8 01 00 00 00       	mov    $0x1,%eax
801069c4:	e9 f7 00 00 00       	jmp    80106ac0 <pfh+0x206>
        panic("V2P on address < KERNBASE "
801069c9:	83 ec 0c             	sub    $0xc,%esp
801069cc:	68 28 6f 10 80       	push   $0x80106f28
801069d1:	e8 72 99 ff ff       	call   80100348 <panic>
      acquire(&pgo.l);
801069d6:	83 ec 0c             	sub    $0xc,%esp
801069d9:	68 e0 56 11 80       	push   $0x801156e0
801069de:	e8 38 d5 ff ff       	call   80103f1b <acquire>
      if (pgo.pgr[PTE_ADDR(*pte) >> 12] != 0)
801069e3:	8b 06                	mov    (%esi),%eax
801069e5:	89 c2                	mov    %eax,%edx
801069e7:	c1 ea 0c             	shr    $0xc,%edx
801069ea:	83 c4 10             	add    $0x10,%esp
801069ed:	80 ba 14 57 11 80 00 	cmpb   $0x0,-0x7feea8ec(%edx)
801069f4:	75 1a                	jne    80106a10 <pfh+0x156>
        *pte |= PTE_W;
801069f6:	83 c8 02             	or     $0x2,%eax
801069f9:	89 06                	mov    %eax,(%esi)
      release(&pgo.l);
801069fb:	83 ec 0c             	sub    $0xc,%esp
801069fe:	68 e0 56 11 80       	push   $0x801156e0
80106a03:	e8 78 d5 ff ff       	call   80103f80 <release>
80106a08:	83 c4 10             	add    $0x10,%esp
80106a0b:	e9 9a 00 00 00       	jmp    80106aaa <pfh+0x1f0>
        if ((mem = kalloc()) == 0)
80106a10:	e8 ba b6 ff ff       	call   801020cf <kalloc>
80106a15:	89 c7                	mov    %eax,%edi
80106a17:	85 c0                	test   %eax,%eax
80106a19:	0f 84 bd 00 00 00    	je     80106adc <pfh+0x222>
80106a1f:	8b 06                	mov    (%esi),%eax
80106a21:	c1 e8 0c             	shr    $0xc,%eax
        pgo.pgr[PTE_ADDR(*pte) >> 12]--;
80106a24:	0f b6 88 14 57 11 80 	movzbl -0x7feea8ec(%eax),%ecx
80106a2b:	8d 51 ff             	lea    -0x1(%ecx),%edx
80106a2e:	88 90 14 57 11 80    	mov    %dl,-0x7feea8ec(%eax)
80106a34:	8b 06                	mov    (%esi),%eax
80106a36:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if (a > KERNBASE)
80106a3b:	3d 00 00 00 80       	cmp    $0x80000000,%eax
80106a40:	77 35                	ja     80106a77 <pfh+0x1bd>
    return (char*)a + KERNBASE;
80106a42:	05 00 00 00 80       	add    $0x80000000,%eax
        memmove(mem, (char *)P2V(PTE_ADDR(*pte)), PGSIZE);
80106a47:	83 ec 04             	sub    $0x4,%esp
80106a4a:	68 00 10 00 00       	push   $0x1000
80106a4f:	50                   	push   %eax
80106a50:	57                   	push   %edi
80106a51:	e8 ec d5 ff ff       	call   80104042 <memmove>
    if (a < (void*) KERNBASE)
80106a56:	83 c4 10             	add    $0x10,%esp
80106a59:	81 ff ff ff ff 7f    	cmp    $0x7fffffff,%edi
80106a5f:	76 23                	jbe    80106a84 <pfh+0x1ca>
    return (uint)a - KERNBASE;
80106a61:	81 c7 00 00 00 80    	add    $0x80000000,%edi
static inline uint PTE_FLAGS(uint pte) { return pte & 0xFFF; }
80106a67:	8b 06                	mov    (%esi),%eax
80106a69:	25 ff 0f 00 00       	and    $0xfff,%eax
        *pte = V2P(mem) | PTE_FLAGS(*pte) | PTE_W;
80106a6e:	09 c7                	or     %eax,%edi
80106a70:	83 cf 02             	or     $0x2,%edi
80106a73:	89 3e                	mov    %edi,(%esi)
80106a75:	eb 84                	jmp    801069fb <pfh+0x141>
        panic("P2V on address > KERNBASE");
80106a77:	83 ec 0c             	sub    $0xc,%esp
80106a7a:	68 58 72 10 80       	push   $0x80107258
80106a7f:	e8 c4 98 ff ff       	call   80100348 <panic>
        panic("V2P on address < KERNBASE "
80106a84:	83 ec 0c             	sub    $0xc,%esp
80106a87:	68 28 6f 10 80       	push   $0x80106f28
80106a8c:	e8 b7 98 ff ff       	call   80100348 <panic>
80106a91:	83 ec 0c             	sub    $0xc,%esp
80106a94:	68 28 6f 10 80       	push   $0x80106f28
80106a99:	e8 aa 98 ff ff       	call   80100348 <panic>
      }
    }
    else
    {
      myproc()->killed = 1;
80106a9e:	e8 b4 c7 ff ff       	call   80103257 <myproc>
80106aa3:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
    if (a < (void*) KERNBASE)
80106aaa:	81 fb ff ff ff 7f    	cmp    $0x7fffffff,%ebx
80106ab0:	76 16                	jbe    80106ac8 <pfh+0x20e>
    return (uint)a - KERNBASE;
80106ab2:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
80106ab8:	0f 22 db             	mov    %ebx,%cr3
    }
  }
  lcr3(V2P(pgdir));
  return 0;
80106abb:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106ac0:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106ac3:	5b                   	pop    %ebx
80106ac4:	5e                   	pop    %esi
80106ac5:	5f                   	pop    %edi
80106ac6:	5d                   	pop    %ebp
80106ac7:	c3                   	ret    
        panic("V2P on address < KERNBASE "
80106ac8:	83 ec 0c             	sub    $0xc,%esp
80106acb:	68 28 6f 10 80       	push   $0x80106f28
80106ad0:	e8 73 98 ff ff       	call   80100348 <panic>
      return 1;
80106ad5:	b8 01 00 00 00       	mov    $0x1,%eax
80106ada:	eb e4                	jmp    80106ac0 <pfh+0x206>
          return 1;
80106adc:	b8 01 00 00 00       	mov    $0x1,%eax
80106ae1:	eb dd                	jmp    80106ac0 <pfh+0x206>
        return 1;
80106ae3:	b8 01 00 00 00       	mov    $0x1,%eax
80106ae8:	eb d6                	jmp    80106ac0 <pfh+0x206>

80106aea <copyuvm>:

pde_t *
copyuvm(pde_t *pgdir, uint sz)
{
80106aea:	55                   	push   %ebp
80106aeb:	89 e5                	mov    %esp,%ebp
80106aed:	57                   	push   %edi
80106aee:	56                   	push   %esi
80106aef:	53                   	push   %ebx
80106af0:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;

  if ((d = setupkvm()) == 0)
80106af3:	e8 11 fd ff ff       	call   80106809 <setupkvm>
80106af8:	89 45 dc             	mov    %eax,-0x24(%ebp)
80106afb:	85 c0                	test   %eax,%eax
80106afd:	0f 84 c6 00 00 00    	je     80106bc9 <copyuvm+0xdf>
    return 0;
  for (i = 0; i < sz; i += PGSIZE)
80106b03:	bf 00 00 00 00       	mov    $0x0,%edi
80106b08:	eb 06                	jmp    80106b10 <copyuvm+0x26>
80106b0a:	81 c7 00 10 00 00    	add    $0x1000,%edi
80106b10:	3b 7d 0c             	cmp    0xc(%ebp),%edi
80106b13:	0f 83 9c 00 00 00    	jae    80106bb5 <copyuvm+0xcb>
  {
    if ((pte = walkpgdir(pgdir, (void *)i, 0)) == 0)
80106b19:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80106b1c:	b9 00 00 00 00       	mov    $0x0,%ecx
80106b21:	89 fa                	mov    %edi,%edx
80106b23:	8b 45 08             	mov    0x8(%ebp),%eax
80106b26:	e8 52 f5 ff ff       	call   8010607d <walkpgdir>
80106b2b:	89 c3                	mov    %eax,%ebx
80106b2d:	85 c0                	test   %eax,%eax
80106b2f:	74 d9                	je     80106b0a <copyuvm+0x20>
      continue;
    //panic("copyuvm: pte should exist");
    if (!(*pte & PTE_P))
80106b31:	8b 00                	mov    (%eax),%eax
80106b33:	a8 01                	test   $0x1,%al
80106b35:	74 d3                	je     80106b0a <copyuvm+0x20>
static inline uint PTE_ADDR(uint pte)  { return pte & ~0xFFF; }
80106b37:	89 c6                	mov    %eax,%esi
80106b39:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
      continue;
    //panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte) & (~(PTE_W));
80106b3f:	25 fd 0f 00 00       	and    $0xffd,%eax
80106b44:	89 45 e0             	mov    %eax,-0x20(%ebp)
    acquire(&pgo.l);
80106b47:	83 ec 0c             	sub    $0xc,%esp
80106b4a:	68 e0 56 11 80       	push   $0x801156e0
80106b4f:	e8 c7 d3 ff ff       	call   80103f1b <acquire>
    pgo.pgr[pa >> 12] += 1;
80106b54:	89 f2                	mov    %esi,%edx
80106b56:	c1 ea 0c             	shr    $0xc,%edx
80106b59:	0f b6 8a 14 57 11 80 	movzbl -0x7feea8ec(%edx),%ecx
80106b60:	8d 41 01             	lea    0x1(%ecx),%eax
80106b63:	88 82 14 57 11 80    	mov    %al,-0x7feea8ec(%edx)
    release(&pgo.l);
80106b69:	c7 04 24 e0 56 11 80 	movl   $0x801156e0,(%esp)
80106b70:	e8 0b d4 ff ff       	call   80103f80 <release>
    *pte = *pte & (~(PTE_W));
80106b75:	8b 03                	mov    (%ebx),%eax
80106b77:	83 e0 fd             	and    $0xfffffffd,%eax
80106b7a:	89 03                	mov    %eax,(%ebx)
    if (mappages(d, (void *)i, PGSIZE, pa, flags) < 0)
80106b7c:	83 c4 04             	add    $0x4,%esp
80106b7f:	ff 75 e0             	pushl  -0x20(%ebp)
80106b82:	56                   	push   %esi
80106b83:	68 00 10 00 00       	push   $0x1000
80106b88:	ff 75 e4             	pushl  -0x1c(%ebp)
80106b8b:	ff 75 dc             	pushl  -0x24(%ebp)
80106b8e:	e8 e6 f6 ff ff       	call   80106279 <mappages>
80106b93:	83 c4 20             	add    $0x20,%esp
80106b96:	85 c0                	test   %eax,%eax
80106b98:	0f 89 6c ff ff ff    	jns    80106b0a <copyuvm+0x20>
    {
      freevm(d);
80106b9e:	83 ec 0c             	sub    $0xc,%esp
80106ba1:	ff 75 dc             	pushl  -0x24(%ebp)
80106ba4:	e8 dc fb ff ff       	call   80106785 <freevm>
      return 0;
80106ba9:	83 c4 10             	add    $0x10,%esp
80106bac:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
80106bb3:	eb 14                	jmp    80106bc9 <copyuvm+0xdf>
    if (a < (void*) KERNBASE)
80106bb5:	81 7d 08 ff ff ff 7f 	cmpl   $0x7fffffff,0x8(%ebp)
80106bbc:	76 16                	jbe    80106bd4 <copyuvm+0xea>
    return (uint)a - KERNBASE;
80106bbe:	8b 45 08             	mov    0x8(%ebp),%eax
80106bc1:	05 00 00 00 80       	add    $0x80000000,%eax
80106bc6:	0f 22 d8             	mov    %eax,%cr3
    }
  }
  lcr3(V2P(pgdir));
  return d;
}
80106bc9:	8b 45 dc             	mov    -0x24(%ebp),%eax
80106bcc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106bcf:	5b                   	pop    %ebx
80106bd0:	5e                   	pop    %esi
80106bd1:	5f                   	pop    %edi
80106bd2:	5d                   	pop    %ebp
80106bd3:	c3                   	ret    
        panic("V2P on address < KERNBASE "
80106bd4:	83 ec 0c             	sub    $0xc,%esp
80106bd7:	68 28 6f 10 80       	push   $0x80106f28
80106bdc:	e8 67 97 ff ff       	call   80100348 <panic>

80106be1 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char *
uva2ka(pde_t *pgdir, char *uva)
{
80106be1:	55                   	push   %ebp
80106be2:	89 e5                	mov    %esp,%ebp
80106be4:	83 ec 08             	sub    $0x8,%esp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80106be7:	b9 00 00 00 00       	mov    $0x0,%ecx
80106bec:	8b 55 0c             	mov    0xc(%ebp),%edx
80106bef:	8b 45 08             	mov    0x8(%ebp),%eax
80106bf2:	e8 86 f4 ff ff       	call   8010607d <walkpgdir>
  if ((*pte & PTE_P) == 0)
80106bf7:	8b 00                	mov    (%eax),%eax
80106bf9:	a8 01                	test   $0x1,%al
80106bfb:	74 24                	je     80106c21 <uva2ka+0x40>
    return 0;
  if ((*pte & PTE_U) == 0)
80106bfd:	a8 04                	test   $0x4,%al
80106bff:	74 27                	je     80106c28 <uva2ka+0x47>
80106c01:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if (a > KERNBASE)
80106c06:	3d 00 00 00 80       	cmp    $0x80000000,%eax
80106c0b:	77 07                	ja     80106c14 <uva2ka+0x33>
    return (char*)a + KERNBASE;
80106c0d:	05 00 00 00 80       	add    $0x80000000,%eax
    return 0;
  return (char *)P2V(PTE_ADDR(*pte));
}
80106c12:	c9                   	leave  
80106c13:	c3                   	ret    
        panic("P2V on address > KERNBASE");
80106c14:	83 ec 0c             	sub    $0xc,%esp
80106c17:	68 58 72 10 80       	push   $0x80107258
80106c1c:	e8 27 97 ff ff       	call   80100348 <panic>
    return 0;
80106c21:	b8 00 00 00 00       	mov    $0x0,%eax
80106c26:	eb ea                	jmp    80106c12 <uva2ka+0x31>
    return 0;
80106c28:	b8 00 00 00 00       	mov    $0x0,%eax
80106c2d:	eb e3                	jmp    80106c12 <uva2ka+0x31>

80106c2f <copyout>:

// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80106c2f:	55                   	push   %ebp
80106c30:	89 e5                	mov    %esp,%ebp
80106c32:	57                   	push   %edi
80106c33:	56                   	push   %esi
80106c34:	53                   	push   %ebx
80106c35:	83 ec 0c             	sub    $0xc,%esp
80106c38:	8b 7d 14             	mov    0x14(%ebp),%edi
  char *buf, *pa0;
  uint n, va0;

  buf = (char *)p;
  while (len > 0)
80106c3b:	eb 25                	jmp    80106c62 <copyout+0x33>
    if (pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
    if (n > len)
      n = len;
    memmove(pa0 + (va - va0), buf, n);
80106c3d:	8b 55 0c             	mov    0xc(%ebp),%edx
80106c40:	29 f2                	sub    %esi,%edx
80106c42:	01 d0                	add    %edx,%eax
80106c44:	83 ec 04             	sub    $0x4,%esp
80106c47:	53                   	push   %ebx
80106c48:	ff 75 10             	pushl  0x10(%ebp)
80106c4b:	50                   	push   %eax
80106c4c:	e8 f1 d3 ff ff       	call   80104042 <memmove>
    len -= n;
80106c51:	29 df                	sub    %ebx,%edi
    buf += n;
80106c53:	01 5d 10             	add    %ebx,0x10(%ebp)
    va = va0 + PGSIZE;
80106c56:	8d 86 00 10 00 00    	lea    0x1000(%esi),%eax
80106c5c:	89 45 0c             	mov    %eax,0xc(%ebp)
80106c5f:	83 c4 10             	add    $0x10,%esp
  while (len > 0)
80106c62:	85 ff                	test   %edi,%edi
80106c64:	74 2f                	je     80106c95 <copyout+0x66>
    va0 = (uint)PGROUNDDOWN(va);
80106c66:	8b 75 0c             	mov    0xc(%ebp),%esi
80106c69:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
    pa0 = uva2ka(pgdir, (char *)va0);
80106c6f:	83 ec 08             	sub    $0x8,%esp
80106c72:	56                   	push   %esi
80106c73:	ff 75 08             	pushl  0x8(%ebp)
80106c76:	e8 66 ff ff ff       	call   80106be1 <uva2ka>
    if (pa0 == 0)
80106c7b:	83 c4 10             	add    $0x10,%esp
80106c7e:	85 c0                	test   %eax,%eax
80106c80:	74 20                	je     80106ca2 <copyout+0x73>
    n = PGSIZE - (va - va0);
80106c82:	89 f3                	mov    %esi,%ebx
80106c84:	2b 5d 0c             	sub    0xc(%ebp),%ebx
80106c87:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if (n > len)
80106c8d:	39 df                	cmp    %ebx,%edi
80106c8f:	73 ac                	jae    80106c3d <copyout+0xe>
      n = len;
80106c91:	89 fb                	mov    %edi,%ebx
80106c93:	eb a8                	jmp    80106c3d <copyout+0xe>
  }
  return 0;
80106c95:	b8 00 00 00 00       	mov    $0x0,%eax
}
80106c9a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106c9d:	5b                   	pop    %ebx
80106c9e:	5e                   	pop    %esi
80106c9f:	5f                   	pop    %edi
80106ca0:	5d                   	pop    %ebp
80106ca1:	c3                   	ret    
      return -1;
80106ca2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80106ca7:	eb f1                	jmp    80106c9a <copyout+0x6b>
