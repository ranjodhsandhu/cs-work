#include "pagingtestlib.h"

int main() {
    setup();
    test_allocation_no_fork(700 * 1024 * 1024, "700MB", "8K",8 * 1024, 8 * 1024, 64 * 1024, 8 * 1024, 1);
    finish();
}
