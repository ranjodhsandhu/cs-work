#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  printf(1, "Write count: %d\n", writecount());
  setwritecount(21);
  printf(1, "Write count: %d\n", writecount());
  printf(1, "Write count: %d\n", writecount());
  printf(1, "Write count: %d\n", writecount());
  printf(1, "Write count: %d\n", writecount());
  exit();
}
