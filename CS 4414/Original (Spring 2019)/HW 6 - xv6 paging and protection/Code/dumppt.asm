
_dumppt:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "types.h"
#include "user.h"

int main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 04             	sub    $0x4,%esp
  11:	8b 41 04             	mov    0x4(%ecx),%eax
    int pid;
    if (argc > 1) {
  14:	83 39 01             	cmpl   $0x1,(%ecx)
  17:	7e 1c                	jle    35 <main+0x35>
        pid = atoi(argv[1]);
  19:	83 ec 0c             	sub    $0xc,%esp
  1c:	ff 70 04             	pushl  0x4(%eax)
  1f:	e8 47 01 00 00       	call   16b <atoi>
  24:	83 c4 10             	add    $0x10,%esp
    } else {
        pid = getpid();
    }
    dumppagetable(pid);
  27:	83 ec 0c             	sub    $0xc,%esp
  2a:	50                   	push   %eax
  2b:	e8 69 02 00 00       	call   299 <dumppagetable>
    exit();
  30:	e8 94 01 00 00       	call   1c9 <exit>
        pid = getpid();
  35:	e8 0f 02 00 00       	call   249 <getpid>
  3a:	eb eb                	jmp    27 <main+0x27>

0000003c <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  3c:	55                   	push   %ebp
  3d:	89 e5                	mov    %esp,%ebp
  3f:	53                   	push   %ebx
  40:	8b 45 08             	mov    0x8(%ebp),%eax
  43:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  46:	89 c2                	mov    %eax,%edx
  48:	0f b6 19             	movzbl (%ecx),%ebx
  4b:	88 1a                	mov    %bl,(%edx)
  4d:	8d 52 01             	lea    0x1(%edx),%edx
  50:	8d 49 01             	lea    0x1(%ecx),%ecx
  53:	84 db                	test   %bl,%bl
  55:	75 f1                	jne    48 <strcpy+0xc>
    ;
  return os;
}
  57:	5b                   	pop    %ebx
  58:	5d                   	pop    %ebp
  59:	c3                   	ret    

0000005a <strcmp>:

int
strcmp(const char *p, const char *q)
{
  5a:	55                   	push   %ebp
  5b:	89 e5                	mov    %esp,%ebp
  5d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  60:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  63:	eb 06                	jmp    6b <strcmp+0x11>
    p++, q++;
  65:	83 c1 01             	add    $0x1,%ecx
  68:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  6b:	0f b6 01             	movzbl (%ecx),%eax
  6e:	84 c0                	test   %al,%al
  70:	74 04                	je     76 <strcmp+0x1c>
  72:	3a 02                	cmp    (%edx),%al
  74:	74 ef                	je     65 <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
  76:	0f b6 c0             	movzbl %al,%eax
  79:	0f b6 12             	movzbl (%edx),%edx
  7c:	29 d0                	sub    %edx,%eax
}
  7e:	5d                   	pop    %ebp
  7f:	c3                   	ret    

00000080 <strlen>:

uint
strlen(const char *s)
{
  80:	55                   	push   %ebp
  81:	89 e5                	mov    %esp,%ebp
  83:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
  86:	ba 00 00 00 00       	mov    $0x0,%edx
  8b:	eb 03                	jmp    90 <strlen+0x10>
  8d:	83 c2 01             	add    $0x1,%edx
  90:	89 d0                	mov    %edx,%eax
  92:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  96:	75 f5                	jne    8d <strlen+0xd>
    ;
  return n;
}
  98:	5d                   	pop    %ebp
  99:	c3                   	ret    

0000009a <memset>:

void*
memset(void *dst, int c, uint n)
{
  9a:	55                   	push   %ebp
  9b:	89 e5                	mov    %esp,%ebp
  9d:	57                   	push   %edi
  9e:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
  a1:	89 d7                	mov    %edx,%edi
  a3:	8b 4d 10             	mov    0x10(%ebp),%ecx
  a6:	8b 45 0c             	mov    0xc(%ebp),%eax
  a9:	fc                   	cld    
  aa:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
  ac:	89 d0                	mov    %edx,%eax
  ae:	5f                   	pop    %edi
  af:	5d                   	pop    %ebp
  b0:	c3                   	ret    

000000b1 <strchr>:

char*
strchr(const char *s, char c)
{
  b1:	55                   	push   %ebp
  b2:	89 e5                	mov    %esp,%ebp
  b4:	8b 45 08             	mov    0x8(%ebp),%eax
  b7:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
  bb:	0f b6 10             	movzbl (%eax),%edx
  be:	84 d2                	test   %dl,%dl
  c0:	74 09                	je     cb <strchr+0x1a>
    if(*s == c)
  c2:	38 ca                	cmp    %cl,%dl
  c4:	74 0a                	je     d0 <strchr+0x1f>
  for(; *s; s++)
  c6:	83 c0 01             	add    $0x1,%eax
  c9:	eb f0                	jmp    bb <strchr+0xa>
      return (char*)s;
  return 0;
  cb:	b8 00 00 00 00       	mov    $0x0,%eax
}
  d0:	5d                   	pop    %ebp
  d1:	c3                   	ret    

000000d2 <gets>:

char*
gets(char *buf, int max)
{
  d2:	55                   	push   %ebp
  d3:	89 e5                	mov    %esp,%ebp
  d5:	57                   	push   %edi
  d6:	56                   	push   %esi
  d7:	53                   	push   %ebx
  d8:	83 ec 1c             	sub    $0x1c,%esp
  db:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
  de:	bb 00 00 00 00       	mov    $0x0,%ebx
  e3:	8d 73 01             	lea    0x1(%ebx),%esi
  e6:	3b 75 0c             	cmp    0xc(%ebp),%esi
  e9:	7d 2e                	jge    119 <gets+0x47>
    cc = read(0, &c, 1);
  eb:	83 ec 04             	sub    $0x4,%esp
  ee:	6a 01                	push   $0x1
  f0:	8d 45 e7             	lea    -0x19(%ebp),%eax
  f3:	50                   	push   %eax
  f4:	6a 00                	push   $0x0
  f6:	e8 e6 00 00 00       	call   1e1 <read>
    if(cc < 1)
  fb:	83 c4 10             	add    $0x10,%esp
  fe:	85 c0                	test   %eax,%eax
 100:	7e 17                	jle    119 <gets+0x47>
      break;
    buf[i++] = c;
 102:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 106:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 109:	3c 0a                	cmp    $0xa,%al
 10b:	0f 94 c2             	sete   %dl
 10e:	3c 0d                	cmp    $0xd,%al
 110:	0f 94 c0             	sete   %al
    buf[i++] = c;
 113:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 115:	08 c2                	or     %al,%dl
 117:	74 ca                	je     e3 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 119:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 11d:	89 f8                	mov    %edi,%eax
 11f:	8d 65 f4             	lea    -0xc(%ebp),%esp
 122:	5b                   	pop    %ebx
 123:	5e                   	pop    %esi
 124:	5f                   	pop    %edi
 125:	5d                   	pop    %ebp
 126:	c3                   	ret    

00000127 <stat>:

int
stat(const char *n, struct stat *st)
{
 127:	55                   	push   %ebp
 128:	89 e5                	mov    %esp,%ebp
 12a:	56                   	push   %esi
 12b:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 12c:	83 ec 08             	sub    $0x8,%esp
 12f:	6a 00                	push   $0x0
 131:	ff 75 08             	pushl  0x8(%ebp)
 134:	e8 d0 00 00 00       	call   209 <open>
  if(fd < 0)
 139:	83 c4 10             	add    $0x10,%esp
 13c:	85 c0                	test   %eax,%eax
 13e:	78 24                	js     164 <stat+0x3d>
 140:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 142:	83 ec 08             	sub    $0x8,%esp
 145:	ff 75 0c             	pushl  0xc(%ebp)
 148:	50                   	push   %eax
 149:	e8 d3 00 00 00       	call   221 <fstat>
 14e:	89 c6                	mov    %eax,%esi
  close(fd);
 150:	89 1c 24             	mov    %ebx,(%esp)
 153:	e8 99 00 00 00       	call   1f1 <close>
  return r;
 158:	83 c4 10             	add    $0x10,%esp
}
 15b:	89 f0                	mov    %esi,%eax
 15d:	8d 65 f8             	lea    -0x8(%ebp),%esp
 160:	5b                   	pop    %ebx
 161:	5e                   	pop    %esi
 162:	5d                   	pop    %ebp
 163:	c3                   	ret    
    return -1;
 164:	be ff ff ff ff       	mov    $0xffffffff,%esi
 169:	eb f0                	jmp    15b <stat+0x34>

0000016b <atoi>:

int
atoi(const char *s)
{
 16b:	55                   	push   %ebp
 16c:	89 e5                	mov    %esp,%ebp
 16e:	53                   	push   %ebx
 16f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 172:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 177:	eb 10                	jmp    189 <atoi+0x1e>
    n = n*10 + *s++ - '0';
 179:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 17c:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 17f:	83 c1 01             	add    $0x1,%ecx
 182:	0f be d2             	movsbl %dl,%edx
 185:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 189:	0f b6 11             	movzbl (%ecx),%edx
 18c:	8d 5a d0             	lea    -0x30(%edx),%ebx
 18f:	80 fb 09             	cmp    $0x9,%bl
 192:	76 e5                	jbe    179 <atoi+0xe>
  return n;
}
 194:	5b                   	pop    %ebx
 195:	5d                   	pop    %ebp
 196:	c3                   	ret    

00000197 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 197:	55                   	push   %ebp
 198:	89 e5                	mov    %esp,%ebp
 19a:	56                   	push   %esi
 19b:	53                   	push   %ebx
 19c:	8b 45 08             	mov    0x8(%ebp),%eax
 19f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 1a2:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 1a5:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 1a7:	eb 0d                	jmp    1b6 <memmove+0x1f>
    *dst++ = *src++;
 1a9:	0f b6 13             	movzbl (%ebx),%edx
 1ac:	88 11                	mov    %dl,(%ecx)
 1ae:	8d 5b 01             	lea    0x1(%ebx),%ebx
 1b1:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 1b4:	89 f2                	mov    %esi,%edx
 1b6:	8d 72 ff             	lea    -0x1(%edx),%esi
 1b9:	85 d2                	test   %edx,%edx
 1bb:	7f ec                	jg     1a9 <memmove+0x12>
  return vdst;
}
 1bd:	5b                   	pop    %ebx
 1be:	5e                   	pop    %esi
 1bf:	5d                   	pop    %ebp
 1c0:	c3                   	ret    

000001c1 <fork>:
 1c1:	b8 01 00 00 00       	mov    $0x1,%eax
 1c6:	cd 40                	int    $0x40
 1c8:	c3                   	ret    

000001c9 <exit>:
 1c9:	b8 02 00 00 00       	mov    $0x2,%eax
 1ce:	cd 40                	int    $0x40
 1d0:	c3                   	ret    

000001d1 <wait>:
 1d1:	b8 03 00 00 00       	mov    $0x3,%eax
 1d6:	cd 40                	int    $0x40
 1d8:	c3                   	ret    

000001d9 <pipe>:
 1d9:	b8 04 00 00 00       	mov    $0x4,%eax
 1de:	cd 40                	int    $0x40
 1e0:	c3                   	ret    

000001e1 <read>:
 1e1:	b8 05 00 00 00       	mov    $0x5,%eax
 1e6:	cd 40                	int    $0x40
 1e8:	c3                   	ret    

000001e9 <write>:
 1e9:	b8 10 00 00 00       	mov    $0x10,%eax
 1ee:	cd 40                	int    $0x40
 1f0:	c3                   	ret    

000001f1 <close>:
 1f1:	b8 15 00 00 00       	mov    $0x15,%eax
 1f6:	cd 40                	int    $0x40
 1f8:	c3                   	ret    

000001f9 <kill>:
 1f9:	b8 06 00 00 00       	mov    $0x6,%eax
 1fe:	cd 40                	int    $0x40
 200:	c3                   	ret    

00000201 <exec>:
 201:	b8 07 00 00 00       	mov    $0x7,%eax
 206:	cd 40                	int    $0x40
 208:	c3                   	ret    

00000209 <open>:
 209:	b8 0f 00 00 00       	mov    $0xf,%eax
 20e:	cd 40                	int    $0x40
 210:	c3                   	ret    

00000211 <mknod>:
 211:	b8 11 00 00 00       	mov    $0x11,%eax
 216:	cd 40                	int    $0x40
 218:	c3                   	ret    

00000219 <unlink>:
 219:	b8 12 00 00 00       	mov    $0x12,%eax
 21e:	cd 40                	int    $0x40
 220:	c3                   	ret    

00000221 <fstat>:
 221:	b8 08 00 00 00       	mov    $0x8,%eax
 226:	cd 40                	int    $0x40
 228:	c3                   	ret    

00000229 <link>:
 229:	b8 13 00 00 00       	mov    $0x13,%eax
 22e:	cd 40                	int    $0x40
 230:	c3                   	ret    

00000231 <mkdir>:
 231:	b8 14 00 00 00       	mov    $0x14,%eax
 236:	cd 40                	int    $0x40
 238:	c3                   	ret    

00000239 <chdir>:
 239:	b8 09 00 00 00       	mov    $0x9,%eax
 23e:	cd 40                	int    $0x40
 240:	c3                   	ret    

00000241 <dup>:
 241:	b8 0a 00 00 00       	mov    $0xa,%eax
 246:	cd 40                	int    $0x40
 248:	c3                   	ret    

00000249 <getpid>:
 249:	b8 0b 00 00 00       	mov    $0xb,%eax
 24e:	cd 40                	int    $0x40
 250:	c3                   	ret    

00000251 <sbrk>:
 251:	b8 0c 00 00 00       	mov    $0xc,%eax
 256:	cd 40                	int    $0x40
 258:	c3                   	ret    

00000259 <sleep>:
 259:	b8 0d 00 00 00       	mov    $0xd,%eax
 25e:	cd 40                	int    $0x40
 260:	c3                   	ret    

00000261 <uptime>:
 261:	b8 0e 00 00 00       	mov    $0xe,%eax
 266:	cd 40                	int    $0x40
 268:	c3                   	ret    

00000269 <yield>:
 269:	b8 16 00 00 00       	mov    $0x16,%eax
 26e:	cd 40                	int    $0x40
 270:	c3                   	ret    

00000271 <shutdown>:
 271:	b8 17 00 00 00       	mov    $0x17,%eax
 276:	cd 40                	int    $0x40
 278:	c3                   	ret    

00000279 <writecount>:
 279:	b8 18 00 00 00       	mov    $0x18,%eax
 27e:	cd 40                	int    $0x40
 280:	c3                   	ret    

00000281 <setwritecount>:
 281:	b8 19 00 00 00       	mov    $0x19,%eax
 286:	cd 40                	int    $0x40
 288:	c3                   	ret    

00000289 <settickets>:
 289:	b8 1a 00 00 00       	mov    $0x1a,%eax
 28e:	cd 40                	int    $0x40
 290:	c3                   	ret    

00000291 <getprocessesinfo>:
 291:	b8 1b 00 00 00       	mov    $0x1b,%eax
 296:	cd 40                	int    $0x40
 298:	c3                   	ret    

00000299 <dumppagetable>:
 299:	b8 1c 00 00 00       	mov    $0x1c,%eax
 29e:	cd 40                	int    $0x40
 2a0:	c3                   	ret    

000002a1 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 2a1:	55                   	push   %ebp
 2a2:	89 e5                	mov    %esp,%ebp
 2a4:	83 ec 1c             	sub    $0x1c,%esp
 2a7:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 2aa:	6a 01                	push   $0x1
 2ac:	8d 55 f4             	lea    -0xc(%ebp),%edx
 2af:	52                   	push   %edx
 2b0:	50                   	push   %eax
 2b1:	e8 33 ff ff ff       	call   1e9 <write>
}
 2b6:	83 c4 10             	add    $0x10,%esp
 2b9:	c9                   	leave  
 2ba:	c3                   	ret    

000002bb <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 2bb:	55                   	push   %ebp
 2bc:	89 e5                	mov    %esp,%ebp
 2be:	57                   	push   %edi
 2bf:	56                   	push   %esi
 2c0:	53                   	push   %ebx
 2c1:	83 ec 2c             	sub    $0x2c,%esp
 2c4:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 2c6:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 2ca:	0f 95 c3             	setne  %bl
 2cd:	89 d0                	mov    %edx,%eax
 2cf:	c1 e8 1f             	shr    $0x1f,%eax
 2d2:	84 c3                	test   %al,%bl
 2d4:	74 10                	je     2e6 <printint+0x2b>
    neg = 1;
    x = -xx;
 2d6:	f7 da                	neg    %edx
    neg = 1;
 2d8:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 2df:	be 00 00 00 00       	mov    $0x0,%esi
 2e4:	eb 0b                	jmp    2f1 <printint+0x36>
  neg = 0;
 2e6:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 2ed:	eb f0                	jmp    2df <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 2ef:	89 c6                	mov    %eax,%esi
 2f1:	89 d0                	mov    %edx,%eax
 2f3:	ba 00 00 00 00       	mov    $0x0,%edx
 2f8:	f7 f1                	div    %ecx
 2fa:	89 c3                	mov    %eax,%ebx
 2fc:	8d 46 01             	lea    0x1(%esi),%eax
 2ff:	0f b6 92 fc 05 00 00 	movzbl 0x5fc(%edx),%edx
 306:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 30a:	89 da                	mov    %ebx,%edx
 30c:	85 db                	test   %ebx,%ebx
 30e:	75 df                	jne    2ef <printint+0x34>
 310:	89 c3                	mov    %eax,%ebx
  if(neg)
 312:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 316:	74 16                	je     32e <printint+0x73>
    buf[i++] = '-';
 318:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 31d:	8d 5e 02             	lea    0x2(%esi),%ebx
 320:	eb 0c                	jmp    32e <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 322:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 327:	89 f8                	mov    %edi,%eax
 329:	e8 73 ff ff ff       	call   2a1 <putc>
  while(--i >= 0)
 32e:	83 eb 01             	sub    $0x1,%ebx
 331:	79 ef                	jns    322 <printint+0x67>
}
 333:	83 c4 2c             	add    $0x2c,%esp
 336:	5b                   	pop    %ebx
 337:	5e                   	pop    %esi
 338:	5f                   	pop    %edi
 339:	5d                   	pop    %ebp
 33a:	c3                   	ret    

0000033b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 33b:	55                   	push   %ebp
 33c:	89 e5                	mov    %esp,%ebp
 33e:	57                   	push   %edi
 33f:	56                   	push   %esi
 340:	53                   	push   %ebx
 341:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 344:	8d 45 10             	lea    0x10(%ebp),%eax
 347:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 34a:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 34f:	bb 00 00 00 00       	mov    $0x0,%ebx
 354:	eb 14                	jmp    36a <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 356:	89 fa                	mov    %edi,%edx
 358:	8b 45 08             	mov    0x8(%ebp),%eax
 35b:	e8 41 ff ff ff       	call   2a1 <putc>
 360:	eb 05                	jmp    367 <printf+0x2c>
      }
    } else if(state == '%'){
 362:	83 fe 25             	cmp    $0x25,%esi
 365:	74 25                	je     38c <printf+0x51>
  for(i = 0; fmt[i]; i++){
 367:	83 c3 01             	add    $0x1,%ebx
 36a:	8b 45 0c             	mov    0xc(%ebp),%eax
 36d:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 371:	84 c0                	test   %al,%al
 373:	0f 84 23 01 00 00    	je     49c <printf+0x161>
    c = fmt[i] & 0xff;
 379:	0f be f8             	movsbl %al,%edi
 37c:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 37f:	85 f6                	test   %esi,%esi
 381:	75 df                	jne    362 <printf+0x27>
      if(c == '%'){
 383:	83 f8 25             	cmp    $0x25,%eax
 386:	75 ce                	jne    356 <printf+0x1b>
        state = '%';
 388:	89 c6                	mov    %eax,%esi
 38a:	eb db                	jmp    367 <printf+0x2c>
      if(c == 'd'){
 38c:	83 f8 64             	cmp    $0x64,%eax
 38f:	74 49                	je     3da <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 391:	83 f8 78             	cmp    $0x78,%eax
 394:	0f 94 c1             	sete   %cl
 397:	83 f8 70             	cmp    $0x70,%eax
 39a:	0f 94 c2             	sete   %dl
 39d:	08 d1                	or     %dl,%cl
 39f:	75 63                	jne    404 <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 3a1:	83 f8 73             	cmp    $0x73,%eax
 3a4:	0f 84 84 00 00 00    	je     42e <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 3aa:	83 f8 63             	cmp    $0x63,%eax
 3ad:	0f 84 b7 00 00 00    	je     46a <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 3b3:	83 f8 25             	cmp    $0x25,%eax
 3b6:	0f 84 cc 00 00 00    	je     488 <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 3bc:	ba 25 00 00 00       	mov    $0x25,%edx
 3c1:	8b 45 08             	mov    0x8(%ebp),%eax
 3c4:	e8 d8 fe ff ff       	call   2a1 <putc>
        putc(fd, c);
 3c9:	89 fa                	mov    %edi,%edx
 3cb:	8b 45 08             	mov    0x8(%ebp),%eax
 3ce:	e8 ce fe ff ff       	call   2a1 <putc>
      }
      state = 0;
 3d3:	be 00 00 00 00       	mov    $0x0,%esi
 3d8:	eb 8d                	jmp    367 <printf+0x2c>
        printint(fd, *ap, 10, 1);
 3da:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 3dd:	8b 17                	mov    (%edi),%edx
 3df:	83 ec 0c             	sub    $0xc,%esp
 3e2:	6a 01                	push   $0x1
 3e4:	b9 0a 00 00 00       	mov    $0xa,%ecx
 3e9:	8b 45 08             	mov    0x8(%ebp),%eax
 3ec:	e8 ca fe ff ff       	call   2bb <printint>
        ap++;
 3f1:	83 c7 04             	add    $0x4,%edi
 3f4:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 3f7:	83 c4 10             	add    $0x10,%esp
      state = 0;
 3fa:	be 00 00 00 00       	mov    $0x0,%esi
 3ff:	e9 63 ff ff ff       	jmp    367 <printf+0x2c>
        printint(fd, *ap, 16, 0);
 404:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 407:	8b 17                	mov    (%edi),%edx
 409:	83 ec 0c             	sub    $0xc,%esp
 40c:	6a 00                	push   $0x0
 40e:	b9 10 00 00 00       	mov    $0x10,%ecx
 413:	8b 45 08             	mov    0x8(%ebp),%eax
 416:	e8 a0 fe ff ff       	call   2bb <printint>
        ap++;
 41b:	83 c7 04             	add    $0x4,%edi
 41e:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 421:	83 c4 10             	add    $0x10,%esp
      state = 0;
 424:	be 00 00 00 00       	mov    $0x0,%esi
 429:	e9 39 ff ff ff       	jmp    367 <printf+0x2c>
        s = (char*)*ap;
 42e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 431:	8b 30                	mov    (%eax),%esi
        ap++;
 433:	83 c0 04             	add    $0x4,%eax
 436:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 439:	85 f6                	test   %esi,%esi
 43b:	75 28                	jne    465 <printf+0x12a>
          s = "(null)";
 43d:	be f4 05 00 00       	mov    $0x5f4,%esi
 442:	8b 7d 08             	mov    0x8(%ebp),%edi
 445:	eb 0d                	jmp    454 <printf+0x119>
          putc(fd, *s);
 447:	0f be d2             	movsbl %dl,%edx
 44a:	89 f8                	mov    %edi,%eax
 44c:	e8 50 fe ff ff       	call   2a1 <putc>
          s++;
 451:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 454:	0f b6 16             	movzbl (%esi),%edx
 457:	84 d2                	test   %dl,%dl
 459:	75 ec                	jne    447 <printf+0x10c>
      state = 0;
 45b:	be 00 00 00 00       	mov    $0x0,%esi
 460:	e9 02 ff ff ff       	jmp    367 <printf+0x2c>
 465:	8b 7d 08             	mov    0x8(%ebp),%edi
 468:	eb ea                	jmp    454 <printf+0x119>
        putc(fd, *ap);
 46a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 46d:	0f be 17             	movsbl (%edi),%edx
 470:	8b 45 08             	mov    0x8(%ebp),%eax
 473:	e8 29 fe ff ff       	call   2a1 <putc>
        ap++;
 478:	83 c7 04             	add    $0x4,%edi
 47b:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 47e:	be 00 00 00 00       	mov    $0x0,%esi
 483:	e9 df fe ff ff       	jmp    367 <printf+0x2c>
        putc(fd, c);
 488:	89 fa                	mov    %edi,%edx
 48a:	8b 45 08             	mov    0x8(%ebp),%eax
 48d:	e8 0f fe ff ff       	call   2a1 <putc>
      state = 0;
 492:	be 00 00 00 00       	mov    $0x0,%esi
 497:	e9 cb fe ff ff       	jmp    367 <printf+0x2c>
    }
  }
}
 49c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 49f:	5b                   	pop    %ebx
 4a0:	5e                   	pop    %esi
 4a1:	5f                   	pop    %edi
 4a2:	5d                   	pop    %ebp
 4a3:	c3                   	ret    

000004a4 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 4a4:	55                   	push   %ebp
 4a5:	89 e5                	mov    %esp,%ebp
 4a7:	57                   	push   %edi
 4a8:	56                   	push   %esi
 4a9:	53                   	push   %ebx
 4aa:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 4ad:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 4b0:	a1 94 08 00 00       	mov    0x894,%eax
 4b5:	eb 02                	jmp    4b9 <free+0x15>
 4b7:	89 d0                	mov    %edx,%eax
 4b9:	39 c8                	cmp    %ecx,%eax
 4bb:	73 04                	jae    4c1 <free+0x1d>
 4bd:	39 08                	cmp    %ecx,(%eax)
 4bf:	77 12                	ja     4d3 <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 4c1:	8b 10                	mov    (%eax),%edx
 4c3:	39 c2                	cmp    %eax,%edx
 4c5:	77 f0                	ja     4b7 <free+0x13>
 4c7:	39 c8                	cmp    %ecx,%eax
 4c9:	72 08                	jb     4d3 <free+0x2f>
 4cb:	39 ca                	cmp    %ecx,%edx
 4cd:	77 04                	ja     4d3 <free+0x2f>
 4cf:	89 d0                	mov    %edx,%eax
 4d1:	eb e6                	jmp    4b9 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 4d3:	8b 73 fc             	mov    -0x4(%ebx),%esi
 4d6:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 4d9:	8b 10                	mov    (%eax),%edx
 4db:	39 d7                	cmp    %edx,%edi
 4dd:	74 19                	je     4f8 <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 4df:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 4e2:	8b 50 04             	mov    0x4(%eax),%edx
 4e5:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 4e8:	39 ce                	cmp    %ecx,%esi
 4ea:	74 1b                	je     507 <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 4ec:	89 08                	mov    %ecx,(%eax)
  freep = p;
 4ee:	a3 94 08 00 00       	mov    %eax,0x894
}
 4f3:	5b                   	pop    %ebx
 4f4:	5e                   	pop    %esi
 4f5:	5f                   	pop    %edi
 4f6:	5d                   	pop    %ebp
 4f7:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 4f8:	03 72 04             	add    0x4(%edx),%esi
 4fb:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 4fe:	8b 10                	mov    (%eax),%edx
 500:	8b 12                	mov    (%edx),%edx
 502:	89 53 f8             	mov    %edx,-0x8(%ebx)
 505:	eb db                	jmp    4e2 <free+0x3e>
    p->s.size += bp->s.size;
 507:	03 53 fc             	add    -0x4(%ebx),%edx
 50a:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 50d:	8b 53 f8             	mov    -0x8(%ebx),%edx
 510:	89 10                	mov    %edx,(%eax)
 512:	eb da                	jmp    4ee <free+0x4a>

00000514 <morecore>:

static Header*
morecore(uint nu)
{
 514:	55                   	push   %ebp
 515:	89 e5                	mov    %esp,%ebp
 517:	53                   	push   %ebx
 518:	83 ec 04             	sub    $0x4,%esp
 51b:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 51d:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 522:	77 05                	ja     529 <morecore+0x15>
    nu = 4096;
 524:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 529:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 530:	83 ec 0c             	sub    $0xc,%esp
 533:	50                   	push   %eax
 534:	e8 18 fd ff ff       	call   251 <sbrk>
  if(p == (char*)-1)
 539:	83 c4 10             	add    $0x10,%esp
 53c:	83 f8 ff             	cmp    $0xffffffff,%eax
 53f:	74 1c                	je     55d <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 541:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 544:	83 c0 08             	add    $0x8,%eax
 547:	83 ec 0c             	sub    $0xc,%esp
 54a:	50                   	push   %eax
 54b:	e8 54 ff ff ff       	call   4a4 <free>
  return freep;
 550:	a1 94 08 00 00       	mov    0x894,%eax
 555:	83 c4 10             	add    $0x10,%esp
}
 558:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 55b:	c9                   	leave  
 55c:	c3                   	ret    
    return 0;
 55d:	b8 00 00 00 00       	mov    $0x0,%eax
 562:	eb f4                	jmp    558 <morecore+0x44>

00000564 <malloc>:

void*
malloc(uint nbytes)
{
 564:	55                   	push   %ebp
 565:	89 e5                	mov    %esp,%ebp
 567:	53                   	push   %ebx
 568:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 56b:	8b 45 08             	mov    0x8(%ebp),%eax
 56e:	8d 58 07             	lea    0x7(%eax),%ebx
 571:	c1 eb 03             	shr    $0x3,%ebx
 574:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 577:	8b 0d 94 08 00 00    	mov    0x894,%ecx
 57d:	85 c9                	test   %ecx,%ecx
 57f:	74 04                	je     585 <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 581:	8b 01                	mov    (%ecx),%eax
 583:	eb 4d                	jmp    5d2 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 585:	c7 05 94 08 00 00 98 	movl   $0x898,0x894
 58c:	08 00 00 
 58f:	c7 05 98 08 00 00 98 	movl   $0x898,0x898
 596:	08 00 00 
    base.s.size = 0;
 599:	c7 05 9c 08 00 00 00 	movl   $0x0,0x89c
 5a0:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 5a3:	b9 98 08 00 00       	mov    $0x898,%ecx
 5a8:	eb d7                	jmp    581 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 5aa:	39 da                	cmp    %ebx,%edx
 5ac:	74 1a                	je     5c8 <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 5ae:	29 da                	sub    %ebx,%edx
 5b0:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 5b3:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 5b6:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 5b9:	89 0d 94 08 00 00    	mov    %ecx,0x894
      return (void*)(p + 1);
 5bf:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 5c2:	83 c4 04             	add    $0x4,%esp
 5c5:	5b                   	pop    %ebx
 5c6:	5d                   	pop    %ebp
 5c7:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 5c8:	8b 10                	mov    (%eax),%edx
 5ca:	89 11                	mov    %edx,(%ecx)
 5cc:	eb eb                	jmp    5b9 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 5ce:	89 c1                	mov    %eax,%ecx
 5d0:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 5d2:	8b 50 04             	mov    0x4(%eax),%edx
 5d5:	39 da                	cmp    %ebx,%edx
 5d7:	73 d1                	jae    5aa <malloc+0x46>
    if(p == freep)
 5d9:	39 05 94 08 00 00    	cmp    %eax,0x894
 5df:	75 ed                	jne    5ce <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 5e1:	89 d8                	mov    %ebx,%eax
 5e3:	e8 2c ff ff ff       	call   514 <morecore>
 5e8:	85 c0                	test   %eax,%eax
 5ea:	75 e2                	jne    5ce <malloc+0x6a>
        return 0;
 5ec:	b8 00 00 00 00       	mov    $0x0,%eax
 5f1:	eb cf                	jmp    5c2 <malloc+0x5e>
