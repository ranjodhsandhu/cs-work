
_read_guard_page:     file format elf32-i386


Disassembly of section .text:

00000000 <read_guard_page>:
#include "pagingtestlib.h"

void read_guard_page() {
       0:	55                   	push   %ebp
       1:	89 e5                	mov    %esp,%ebp
    __asm__ volatile(
       3:	89 e0                	mov    %esp,%eax
       5:	25 00 f0 ff ff       	and    $0xfffff000,%eax
       a:	2d 00 08 00 00       	sub    $0x800,%eax
       f:	8b 00                	mov    (%eax),%eax
        "and $0xFFFFF000, %%eax\n"
        "sub $0x800, %%eax\n"
        "mov (%%eax), %%eax\n"
    ::: "%eax", "memory"
    );
}
      11:	5d                   	pop    %ebp
      12:	c3                   	ret    

00000013 <test_out_of_bounds_internal>:
    }
    return 0;
}

static unsigned out_of_bounds_offset = 1;
void test_out_of_bounds_internal() {
      13:	55                   	push   %ebp
      14:	89 e5                	mov    %esp,%ebp
      16:	83 ec 14             	sub    $0x14,%esp
    volatile char *end_of_heap = sbrk(0);
      19:	6a 00                	push   $0x0
      1b:	e8 a9 1c 00 00       	call   1cc9 <sbrk>
    (void) end_of_heap[out_of_bounds_offset];
      20:	03 05 8c 35 00 00    	add    0x358c,%eax
      26:	0f b6 00             	movzbl (%eax),%eax
}
      29:	83 c4 10             	add    $0x10,%esp
      2c:	c9                   	leave  
      2d:	c3                   	ret    

0000002e <dump_for>:
void dump_for(const char *reason, int pid) {
      2e:	55                   	push   %ebp
      2f:	89 e5                	mov    %esp,%ebp
      31:	53                   	push   %ebx
      32:	83 ec 04             	sub    $0x4,%esp
      35:	8b 5d 08             	mov    0x8(%ebp),%ebx
    if (enable_dump) {
      38:	83 3d a0 35 00 00 00 	cmpl   $0x0,0x35a0
      3f:	74 42                	je     83 <dump_for+0x55>
        if (dump_count >= 0) {
      41:	a1 cc 36 00 00       	mov    0x36cc,%eax
      46:	85 c0                	test   %eax,%eax
      48:	78 3e                	js     88 <dump_for+0x5a>
            printf(1, STARTDUMP "%s#%d\n", reason, dump_count);
      4a:	50                   	push   %eax
      4b:	53                   	push   %ebx
      4c:	68 6c 20 00 00       	push   $0x206c
      51:	6a 01                	push   $0x1
      53:	e8 5b 1d 00 00       	call   1db3 <printf>
      58:	83 c4 10             	add    $0x10,%esp
        dumppagetable(pid);
      5b:	83 ec 0c             	sub    $0xc,%esp
      5e:	ff 75 0c             	pushl  0xc(%ebp)
      61:	e8 ab 1c 00 00       	call   1d11 <dumppagetable>
        if (dump_count >= 0) {
      66:	a1 cc 36 00 00       	mov    0x36cc,%eax
      6b:	83 c4 10             	add    $0x10,%esp
      6e:	85 c0                	test   %eax,%eax
      70:	78 2b                	js     9d <dump_for+0x6f>
            printf(1, ENDDUMP "%s#%d\n", reason, dump_count);
      72:	50                   	push   %eax
      73:	53                   	push   %ebx
      74:	68 c4 20 00 00       	push   $0x20c4
      79:	6a 01                	push   $0x1
      7b:	e8 33 1d 00 00       	call   1db3 <printf>
      80:	83 c4 10             	add    $0x10,%esp
}
      83:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      86:	c9                   	leave  
      87:	c3                   	ret    
            printf(1, STARTDUMP "%s\n", reason);
      88:	83 ec 04             	sub    $0x4,%esp
      8b:	53                   	push   %ebx
      8c:	68 98 20 00 00       	push   $0x2098
      91:	6a 01                	push   $0x1
      93:	e8 1b 1d 00 00       	call   1db3 <printf>
      98:	83 c4 10             	add    $0x10,%esp
      9b:	eb be                	jmp    5b <dump_for+0x2d>
            printf(1, ENDDUMP "%s\n", reason);
      9d:	83 ec 04             	sub    $0x4,%esp
      a0:	53                   	push   %ebx
      a1:	68 f4 20 00 00       	push   $0x20f4
      a6:	6a 01                	push   $0x1
      a8:	e8 06 1d 00 00       	call   1db3 <printf>
      ad:	83 c4 10             	add    $0x10,%esp
}
      b0:	eb d1                	jmp    83 <dump_for+0x55>

000000b2 <setup>:
void setup() {
      b2:	55                   	push   %ebp
      b3:	89 e5                	mov    %esp,%ebp
      b5:	83 ec 08             	sub    $0x8,%esp
    dump_count = -1;
      b8:	c7 05 cc 36 00 00 ff 	movl   $0xffffffff,0x36cc
      bf:	ff ff ff 
    if (getpid() == 1) {
      c2:	e8 fa 1b 00 00       	call   1cc1 <getpid>
      c7:	83 f8 01             	cmp    $0x1,%eax
      ca:	74 02                	je     ce <setup+0x1c>
}
      cc:	c9                   	leave  
      cd:	c3                   	ret    
        mknod("console", 1, 1);
      ce:	83 ec 04             	sub    $0x4,%esp
      d1:	6a 01                	push   $0x1
      d3:	6a 01                	push   $0x1
      d5:	68 7c 2c 00 00       	push   $0x2c7c
      da:	e8 aa 1b 00 00       	call   1c89 <mknod>
        open("console", O_RDWR);
      df:	83 c4 08             	add    $0x8,%esp
      e2:	6a 02                	push   $0x2
      e4:	68 7c 2c 00 00       	push   $0x2c7c
      e9:	e8 93 1b 00 00       	call   1c81 <open>
        dup(0);
      ee:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
      f5:	e8 bf 1b 00 00       	call   1cb9 <dup>
        dup(0);
      fa:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     101:	e8 b3 1b 00 00       	call   1cb9 <dup>
     106:	83 c4 10             	add    $0x10,%esp
}
     109:	eb c1                	jmp    cc <setup+0x1a>

0000010b <finish>:
void finish() {
     10b:	55                   	push   %ebp
     10c:	89 e5                	mov    %esp,%ebp
     10e:	83 ec 08             	sub    $0x8,%esp
    if (getpid() == 1) {
     111:	e8 ab 1b 00 00       	call   1cc1 <getpid>
     116:	83 f8 01             	cmp    $0x1,%eax
     119:	75 07                	jne    122 <finish+0x17>
        shutdown();
     11b:	e8 c9 1b 00 00       	call   1ce9 <shutdown>
}
     120:	c9                   	leave  
     121:	c3                   	ret    
        exit();
     122:	e8 1a 1b 00 00       	call   1c41 <exit>

00000127 <test_simple_crash_no_fork>:
void test_simple_crash_no_fork(void (*test_func)(), const char *no_crash_message) {
     127:	55                   	push   %ebp
     128:	89 e5                	mov    %esp,%ebp
     12a:	83 ec 08             	sub    $0x8,%esp
    test_func();
     12d:	ff 55 08             	call   *0x8(%ebp)
    printf(1, "%s\n", no_crash_message);
     130:	83 ec 04             	sub    $0x4,%esp
     133:	ff 75 0c             	pushl  0xc(%ebp)
     136:	68 84 2c 00 00       	push   $0x2c84
     13b:	6a 01                	push   $0x1
     13d:	e8 71 1c 00 00       	call   1db3 <printf>
}
     142:	83 c4 10             	add    $0x10,%esp
     145:	c9                   	leave  
     146:	c3                   	ret    

00000147 <test_simple_crash>:
int test_simple_crash(void (*test_func)(), const char *crash_message, const char *no_crash_message) {
     147:	55                   	push   %ebp
     148:	89 e5                	mov    %esp,%ebp
     14a:	53                   	push   %ebx
     14b:	83 ec 20             	sub    $0x20,%esp
    pipe(fds);
     14e:	8d 45 f0             	lea    -0x10(%ebp),%eax
     151:	50                   	push   %eax
     152:	e8 fa 1a 00 00       	call   1c51 <pipe>
    int pid = fork();
     157:	e8 dd 1a 00 00       	call   1c39 <fork>
    if (pid == -1) {
     15c:	83 c4 10             	add    $0x10,%esp
     15f:	83 f8 ff             	cmp    $0xffffffff,%eax
     162:	74 59                	je     1bd <test_simple_crash+0x76>
    } else if (pid == 0) {
     164:	85 c0                	test   %eax,%eax
     166:	74 6e                	je     1d6 <test_simple_crash+0x8f>
        close(fds[1]);
     168:	83 ec 0c             	sub    $0xc,%esp
     16b:	ff 75 f4             	pushl  -0xc(%ebp)
     16e:	e8 f6 1a 00 00       	call   1c69 <close>
        int size = read(fds[0], text, 1);
     173:	83 c4 0c             	add    $0xc,%esp
     176:	6a 01                	push   $0x1
     178:	8d 45 ef             	lea    -0x11(%ebp),%eax
     17b:	50                   	push   %eax
     17c:	ff 75 f0             	pushl  -0x10(%ebp)
     17f:	e8 d5 1a 00 00       	call   1c59 <read>
     184:	89 c3                	mov    %eax,%ebx
        wait();
     186:	e8 be 1a 00 00       	call   1c49 <wait>
        close(fds[0]);
     18b:	83 c4 04             	add    $0x4,%esp
     18e:	ff 75 f0             	pushl  -0x10(%ebp)
     191:	e8 d3 1a 00 00       	call   1c69 <close>
        if (size == 1) {
     196:	83 c4 10             	add    $0x10,%esp
     199:	83 fb 01             	cmp    $0x1,%ebx
     19c:	74 66                	je     204 <test_simple_crash+0xbd>
            printf(1, "%s\n", crash_message);
     19e:	83 ec 04             	sub    $0x4,%esp
     1a1:	ff 75 0c             	pushl  0xc(%ebp)
     1a4:	68 84 2c 00 00       	push   $0x2c84
     1a9:	6a 01                	push   $0x1
     1ab:	e8 03 1c 00 00       	call   1db3 <printf>
            return 1;
     1b0:	83 c4 10             	add    $0x10,%esp
     1b3:	b8 01 00 00 00       	mov    $0x1,%eax
}
     1b8:	8b 5d fc             	mov    -0x4(%ebp),%ebx
     1bb:	c9                   	leave  
     1bc:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
     1bd:	83 ec 08             	sub    $0x8,%esp
     1c0:	68 88 2c 00 00       	push   $0x2c88
     1c5:	6a 01                	push   $0x1
     1c7:	e8 e7 1b 00 00       	call   1db3 <printf>
    return 0;
     1cc:	83 c4 10             	add    $0x10,%esp
     1cf:	b8 00 00 00 00       	mov    $0x0,%eax
     1d4:	eb e2                	jmp    1b8 <test_simple_crash+0x71>
        close(1);
     1d6:	83 ec 0c             	sub    $0xc,%esp
     1d9:	6a 01                	push   $0x1
     1db:	e8 89 1a 00 00       	call   1c69 <close>
        dup(fds[1]);
     1e0:	83 c4 04             	add    $0x4,%esp
     1e3:	ff 75 f4             	pushl  -0xc(%ebp)
     1e6:	e8 ce 1a 00 00       	call   1cb9 <dup>
        test_func();
     1eb:	ff 55 08             	call   *0x8(%ebp)
        write(1, "X", 1);
     1ee:	83 c4 0c             	add    $0xc,%esp
     1f1:	6a 01                	push   $0x1
     1f3:	68 a1 2c 00 00       	push   $0x2ca1
     1f8:	6a 01                	push   $0x1
     1fa:	e8 62 1a 00 00       	call   1c61 <write>
        exit();
     1ff:	e8 3d 1a 00 00       	call   1c41 <exit>
            printf(1, "%s\n", no_crash_message);
     204:	83 ec 04             	sub    $0x4,%esp
     207:	ff 75 10             	pushl  0x10(%ebp)
     20a:	68 84 2c 00 00       	push   $0x2c84
     20f:	6a 01                	push   $0x1
     211:	e8 9d 1b 00 00       	call   1db3 <printf>
            return 0;
     216:	83 c4 10             	add    $0x10,%esp
     219:	b8 00 00 00 00       	mov    $0x0,%eax
     21e:	eb 98                	jmp    1b8 <test_simple_crash+0x71>

00000220 <test_out_of_bounds_fork>:

int test_out_of_bounds_fork(int offset, const char *crash_message, const char *no_crash_message) {
     220:	55                   	push   %ebp
     221:	89 e5                	mov    %esp,%ebp
     223:	83 ec 0c             	sub    $0xc,%esp
    out_of_bounds_offset = offset;
     226:	8b 45 08             	mov    0x8(%ebp),%eax
     229:	a3 8c 35 00 00       	mov    %eax,0x358c
    return test_simple_crash(test_out_of_bounds_internal, crash_message, no_crash_message);
     22e:	ff 75 10             	pushl  0x10(%ebp)
     231:	ff 75 0c             	pushl  0xc(%ebp)
     234:	68 13 00 00 00       	push   $0x13
     239:	e8 09 ff ff ff       	call   147 <test_simple_crash>
}
     23e:	c9                   	leave  
     23f:	c3                   	ret    

00000240 <test_out_of_bounds_no_fork>:

void test_out_of_bounds_no_fork(int offset, const char *no_crash_message) {
     240:	55                   	push   %ebp
     241:	89 e5                	mov    %esp,%ebp
     243:	83 ec 10             	sub    $0x10,%esp
    out_of_bounds_offset = offset;
     246:	8b 45 08             	mov    0x8(%ebp),%eax
     249:	a3 8c 35 00 00       	mov    %eax,0x358c
    test_simple_crash_no_fork(test_out_of_bounds_internal, no_crash_message);
     24e:	ff 75 0c             	pushl  0xc(%ebp)
     251:	68 13 00 00 00       	push   $0x13
     256:	e8 cc fe ff ff       	call   127 <test_simple_crash_no_fork>
}
     25b:	83 c4 10             	add    $0x10,%esp
     25e:	c9                   	leave  
     25f:	c3                   	ret    

00000260 <_allocation_failure_message>:

void _allocation_failure_message(int size, char *code) {
     260:	55                   	push   %ebp
     261:	89 e5                	mov    %esp,%ebp
     263:	83 ec 08             	sub    $0x8,%esp
     266:	8b 45 08             	mov    0x8(%ebp),%eax
    if (size == 2 && code[0] == 'N') {
     269:	83 f8 02             	cmp    $0x2,%eax
     26c:	74 1c                	je     28a <_allocation_failure_message+0x2a>
        } else if (code[1] == 'F') {
            printf(1, FAIL_MSG "fork failed\n");
        } else {
            printf(1, FAIL_MSG "unknown error\n");
        }
    } else if (size == 0) {
     26e:	85 c0                	test   %eax,%eax
     270:	0f 84 cd 00 00 00    	je     343 <_allocation_failure_message+0xe3>
        printf(1, FAIL_MSG "unknown crash?\n");
    } else {
        printf(1, FAIL_MSG "unknown error\n");
     276:	83 ec 08             	sub    $0x8,%esp
     279:	68 bd 2c 00 00       	push   $0x2cbd
     27e:	6a 01                	push   $0x1
     280:	e8 2e 1b 00 00       	call   1db3 <printf>
     285:	83 c4 10             	add    $0x10,%esp
    }
}
     288:	c9                   	leave  
     289:	c3                   	ret    
    if (size == 2 && code[0] == 'N') {
     28a:	8b 55 0c             	mov    0xc(%ebp),%edx
     28d:	80 3a 4e             	cmpb   $0x4e,(%edx)
     290:	75 dc                	jne    26e <_allocation_failure_message+0xe>
        if (code[1] == 'A') {
     292:	0f b6 42 01          	movzbl 0x1(%edx),%eax
     296:	3c 41                	cmp    $0x41,%al
     298:	74 28                	je     2c2 <_allocation_failure_message+0x62>
        } else if (code[1] == 'I') {
     29a:	3c 49                	cmp    $0x49,%al
     29c:	74 38                	je     2d6 <_allocation_failure_message+0x76>
        } else if (code[1] == 'R') {
     29e:	3c 52                	cmp    $0x52,%al
     2a0:	74 48                	je     2ea <_allocation_failure_message+0x8a>
        } else if (code[1] == 'S') {
     2a2:	3c 53                	cmp    $0x53,%al
     2a4:	74 58                	je     2fe <_allocation_failure_message+0x9e>
        } else if (code[1] == 's') {
     2a6:	3c 73                	cmp    $0x73,%al
     2a8:	74 6b                	je     315 <_allocation_failure_message+0xb5>
        } else if (code[1] == 'F') {
     2aa:	3c 46                	cmp    $0x46,%al
     2ac:	74 7e                	je     32c <_allocation_failure_message+0xcc>
            printf(1, FAIL_MSG "unknown error\n");
     2ae:	83 ec 08             	sub    $0x8,%esp
     2b1:	68 bd 2c 00 00       	push   $0x2cbd
     2b6:	6a 01                	push   $0x1
     2b8:	e8 f6 1a 00 00       	call   1db3 <printf>
     2bd:	83 c4 10             	add    $0x10,%esp
     2c0:	eb c6                	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "allocating (but not using) memory with sbrk() returned error\n");
     2c2:	83 ec 08             	sub    $0x8,%esp
     2c5:	68 20 21 00 00       	push   $0x2120
     2ca:	6a 01                	push   $0x1
     2cc:	e8 e2 1a 00 00       	call   1db3 <printf>
     2d1:	83 c4 10             	add    $0x10,%esp
     2d4:	eb b2                	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "allocation initialized to non-zero value\n");
     2d6:	83 ec 08             	sub    $0x8,%esp
     2d9:	68 6c 21 00 00       	push   $0x216c
     2de:	6a 01                	push   $0x1
     2e0:	e8 ce 1a 00 00       	call   1db3 <printf>
     2e5:	83 c4 10             	add    $0x10,%esp
     2e8:	eb 9e                	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "using parts of allocation read wrong value\n");
     2ea:	83 ec 08             	sub    $0x8,%esp
     2ed:	68 a4 21 00 00       	push   $0x21a4
     2f2:	6a 01                	push   $0x1
     2f4:	e8 ba 1a 00 00       	call   1db3 <printf>
     2f9:	83 c4 10             	add    $0x10,%esp
     2fc:	eb 8a                	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "sbrk() returned wrong value (wrong amount allocated?)\n");
     2fe:	83 ec 08             	sub    $0x8,%esp
     301:	68 e0 21 00 00       	push   $0x21e0
     306:	6a 01                	push   $0x1
     308:	e8 a6 1a 00 00       	call   1db3 <printf>
     30d:	83 c4 10             	add    $0x10,%esp
     310:	e9 73 ff ff ff       	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "sbrk() failed (returned -1)\n");
     315:	83 ec 08             	sub    $0x8,%esp
     318:	68 24 22 00 00       	push   $0x2224
     31d:	6a 01                	push   $0x1
     31f:	e8 8f 1a 00 00       	call   1db3 <printf>
     324:	83 c4 10             	add    $0x10,%esp
     327:	e9 5c ff ff ff       	jmp    288 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "fork failed\n");
     32c:	83 ec 08             	sub    $0x8,%esp
     32f:	68 a3 2c 00 00       	push   $0x2ca3
     334:	6a 01                	push   $0x1
     336:	e8 78 1a 00 00       	call   1db3 <printf>
     33b:	83 c4 10             	add    $0x10,%esp
     33e:	e9 45 ff ff ff       	jmp    288 <_allocation_failure_message+0x28>
        printf(1, FAIL_MSG "unknown crash?\n");
     343:	83 ec 08             	sub    $0x8,%esp
     346:	68 d9 2c 00 00       	push   $0x2cd9
     34b:	6a 01                	push   $0x1
     34d:	e8 61 1a 00 00       	call   1db3 <printf>
     352:	83 c4 10             	add    $0x10,%esp
     355:	e9 2e ff ff ff       	jmp    288 <_allocation_failure_message+0x28>

0000035a <_fail_allocation_test>:

void _fail_allocation_test(int pipe_fd, char reason) {
     35a:	55                   	push   %ebp
     35b:	89 e5                	mov    %esp,%ebp
     35d:	83 ec 18             	sub    $0x18,%esp
    char temp[2] = {'N', reason};
     360:	c6 45 f6 4e          	movb   $0x4e,-0xa(%ebp)
     364:	8b 45 0c             	mov    0xc(%ebp),%eax
     367:	88 45 f7             	mov    %al,-0x9(%ebp)
    if (pipe_fd == -1) {
     36a:	83 7d 08 ff          	cmpl   $0xffffffff,0x8(%ebp)
     36e:	75 10                	jne    380 <_fail_allocation_test+0x26>
      _allocation_failure_message(2, temp);
     370:	83 ec 08             	sub    $0x8,%esp
     373:	8d 45 f6             	lea    -0xa(%ebp),%eax
     376:	50                   	push   %eax
     377:	6a 02                	push   $0x2
     379:	e8 e2 fe ff ff       	call   260 <_allocation_failure_message>
    } else {
      write(pipe_fd, temp, 2);
      exit();
    }
}
     37e:	c9                   	leave  
     37f:	c3                   	ret    
      write(pipe_fd, temp, 2);
     380:	83 ec 04             	sub    $0x4,%esp
     383:	6a 02                	push   $0x2
     385:	8d 45 f6             	lea    -0xa(%ebp),%eax
     388:	50                   	push   %eax
     389:	ff 75 08             	pushl  0x8(%ebp)
     38c:	e8 d0 18 00 00       	call   1c61 <write>
      exit();
     391:	e8 ab 18 00 00       	call   1c41 <exit>

00000396 <_pass_allocation_test>:

void _pass_allocation_test(int pipe_fd, const char *message) {
     396:	55                   	push   %ebp
     397:	89 e5                	mov    %esp,%ebp
     399:	83 ec 18             	sub    $0x18,%esp
    char temp[2] = {'Y', 'Y'};
     39c:	c6 45 f6 59          	movb   $0x59,-0xa(%ebp)
     3a0:	c6 45 f7 59          	movb   $0x59,-0x9(%ebp)
    if (pipe_fd == -1) {
     3a4:	83 7d 08 ff          	cmpl   $0xffffffff,0x8(%ebp)
     3a8:	75 14                	jne    3be <_pass_allocation_test+0x28>
      printf(1, PASS_MSG "%s", message);
     3aa:	83 ec 04             	sub    $0x4,%esp
     3ad:	ff 75 0c             	pushl  0xc(%ebp)
     3b0:	68 f6 2c 00 00       	push   $0x2cf6
     3b5:	6a 01                	push   $0x1
     3b7:	e8 f7 19 00 00       	call   1db3 <printf>
    } else {
      write(pipe_fd, temp, 2);
      exit();
    }
}
     3bc:	c9                   	leave  
     3bd:	c3                   	ret    
      write(pipe_fd, temp, 2);
     3be:	83 ec 04             	sub    $0x4,%esp
     3c1:	6a 02                	push   $0x2
     3c3:	8d 45 f6             	lea    -0xa(%ebp),%eax
     3c6:	50                   	push   %eax
     3c7:	ff 75 08             	pushl  0x8(%ebp)
     3ca:	e8 92 18 00 00       	call   1c61 <write>
      exit();
     3cf:	e8 6d 18 00 00       	call   1c41 <exit>

000003d4 <_test_allocation_generic>:

int _test_allocation_generic(
    int fork_before, int fork_after,
    int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero,
    int write_after
) {
     3d4:	55                   	push   %ebp
     3d5:	89 e5                	mov    %esp,%ebp
     3d7:	57                   	push   %edi
     3d8:	56                   	push   %esi
     3d9:	53                   	push   %ebx
     3da:	83 ec 2c             	sub    $0x2c,%esp
     3dd:	8b 5d 08             	mov    0x8(%ebp),%ebx
     3e0:	8b 7d 18             	mov    0x18(%ebp),%edi
     3e3:	8b 75 2c             	mov    0x2c(%ebp),%esi
  printf(1, "testing allocating %s and reading/writing to %s segments of it\n", describe_size, describe_amount);
     3e6:	57                   	push   %edi
     3e7:	ff 75 14             	pushl  0x14(%ebp)
     3ea:	68 d4 22 00 00       	push   $0x22d4
     3ef:	6a 01                	push   $0x1
     3f1:	e8 bd 19 00 00       	call   1db3 <printf>
  if (check_zero)
     3f6:	83 c4 10             	add    $0x10,%esp
     3f9:	85 f6                	test   %esi,%esi
     3fb:	0f 85 0b 01 00 00    	jne    50c <_test_allocation_generic+0x138>
    printf(1, "... and verifying that (at least some of) the heap is initialized to zeroes\n");
  if (fork_before)
     401:	85 db                	test   %ebx,%ebx
     403:	0f 85 1a 01 00 00    	jne    523 <_test_allocation_generic+0x14f>
    printf(1, "... in a subprocess\n");
  if (fork_after)
     409:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     40d:	74 20                	je     42f <_test_allocation_generic+0x5b>
    printf(1, "... and fork'ing%s after writing to parts of the heap\n",
     40f:	85 db                	test   %ebx,%ebx
     411:	0f 84 23 01 00 00    	je     53a <_test_allocation_generic+0x166>
     417:	b8 06 2d 00 00       	mov    $0x2d06,%eax
     41c:	83 ec 04             	sub    $0x4,%esp
     41f:	50                   	push   %eax
     420:	68 64 23 00 00       	push   $0x2364
     425:	6a 01                	push   $0x1
     427:	e8 87 19 00 00       	call   1db3 <printf>
     42c:	83 c4 10             	add    $0x10,%esp
        fork_before ? " again" : "");
  if (write_after)
     42f:	83 7d 30 00          	cmpl   $0x0,0x30(%ebp)
     433:	0f 85 0b 01 00 00    	jne    544 <_test_allocation_generic+0x170>
    printf(1, "... and writing in the child process after forking and reading from the parent after that\n");
  dump_for("allocation-pre-allocate", getpid());
     439:	e8 83 18 00 00       	call   1cc1 <getpid>
     43e:	83 ec 08             	sub    $0x8,%esp
     441:	50                   	push   %eax
     442:	68 22 2d 00 00       	push   $0x2d22
     447:	e8 e2 fb ff ff       	call   2e <dump_for>
  int fds[2] = {-1, -1};
     44c:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
     453:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  int main_pid = -1;
  if (fork_before) {
     45a:	83 c4 10             	add    $0x10,%esp
     45d:	85 db                	test   %ebx,%ebx
     45f:	0f 84 9a 01 00 00    	je     5ff <_test_allocation_generic+0x22b>
    pipe(fds);
     465:	83 ec 0c             	sub    $0xc,%esp
     468:	8d 45 e0             	lea    -0x20(%ebp),%eax
     46b:	50                   	push   %eax
     46c:	e8 e0 17 00 00       	call   1c51 <pipe>
    main_pid = fork();
     471:	e8 c3 17 00 00       	call   1c39 <fork>
    if (main_pid == -1) {
     476:	83 c4 10             	add    $0x10,%esp
     479:	83 f8 ff             	cmp    $0xffffffff,%eax
     47c:	0f 84 d9 00 00 00    	je     55b <_test_allocation_generic+0x187>
      printf(1, FAIL_MSG "fork failed");
    } else if (main_pid != 0) {
     482:	85 c0                	test   %eax,%eax
     484:	0f 84 67 01 00 00    	je     5f1 <_test_allocation_generic+0x21d>
      /* parent process */
      char text[10];
      close(fds[1]);
     48a:	83 ec 0c             	sub    $0xc,%esp
     48d:	ff 75 e4             	pushl  -0x1c(%ebp)
     490:	e8 d4 17 00 00       	call   1c69 <close>
      wait();
     495:	e8 af 17 00 00       	call   1c49 <wait>
      int size = read(fds[0], text, 10);
     49a:	83 c4 0c             	add    $0xc,%esp
     49d:	6a 0a                	push   $0xa
     49f:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     4a2:	50                   	push   %eax
     4a3:	ff 75 e0             	pushl  -0x20(%ebp)
     4a6:	e8 ae 17 00 00       	call   1c59 <read>
     4ab:	89 c3                	mov    %eax,%ebx
      close(fds[0]);
     4ad:	83 c4 04             	add    $0x4,%esp
     4b0:	ff 75 e0             	pushl  -0x20(%ebp)
     4b3:	e8 b1 17 00 00       	call   1c69 <close>
      if (fork_after) {
     4b8:	83 c4 10             	add    $0x10,%esp
     4bb:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     4bf:	0f 84 f3 00 00 00    	je     5b8 <_test_allocation_generic+0x1e4>
        if (size != 4) {
     4c5:	83 fb 04             	cmp    $0x4,%ebx
     4c8:	0f 85 a4 00 00 00    	jne    572 <_test_allocation_generic+0x19e>
          printf(1, FAIL_MSG "allocation test did not return result from both processes after fork()ing after allocation?");
          return 0;
        } else if (text[0] != 'Y') {
     4ce:	80 7d d6 59          	cmpb   $0x59,-0x2a(%ebp)
     4d2:	0f 85 b6 00 00 00    	jne    58e <_test_allocation_generic+0x1ba>
          printf(1, "... test failed in child process:\n");
          _allocation_failure_message(2, text);
          return 0;
        } else if (text[2] != 'Y') {
     4d8:	80 7d d8 59          	cmpb   $0x59,-0x28(%ebp)
     4dc:	0f 84 1d 01 00 00    	je     5ff <_test_allocation_generic+0x22b>
          printf(1, "... test failed in grandchild process :\n");
     4e2:	83 ec 08             	sub    $0x8,%esp
     4e5:	68 88 24 00 00       	push   $0x2488
     4ea:	6a 01                	push   $0x1
     4ec:	e8 c2 18 00 00       	call   1db3 <printf>
          _allocation_failure_message(2, text + 2);
     4f1:	83 c4 08             	add    $0x8,%esp
     4f4:	8d 45 d8             	lea    -0x28(%ebp),%eax
     4f7:	50                   	push   %eax
     4f8:	6a 02                	push   $0x2
     4fa:	e8 61 fd ff ff       	call   260 <_allocation_failure_message>
          return 0;
     4ff:	83 c4 10             	add    $0x10,%esp
     502:	be 00 00 00 00       	mov    $0x0,%esi
     507:	e9 30 01 00 00       	jmp    63c <_test_allocation_generic+0x268>
    printf(1, "... and verifying that (at least some of) the heap is initialized to zeroes\n");
     50c:	83 ec 08             	sub    $0x8,%esp
     50f:	68 14 23 00 00       	push   $0x2314
     514:	6a 01                	push   $0x1
     516:	e8 98 18 00 00       	call   1db3 <printf>
     51b:	83 c4 10             	add    $0x10,%esp
     51e:	e9 de fe ff ff       	jmp    401 <_test_allocation_generic+0x2d>
    printf(1, "... in a subprocess\n");
     523:	83 ec 08             	sub    $0x8,%esp
     526:	68 0d 2d 00 00       	push   $0x2d0d
     52b:	6a 01                	push   $0x1
     52d:	e8 81 18 00 00       	call   1db3 <printf>
     532:	83 c4 10             	add    $0x10,%esp
     535:	e9 cf fe ff ff       	jmp    409 <_test_allocation_generic+0x35>
    printf(1, "... and fork'ing%s after writing to parts of the heap\n",
     53a:	b8 f5 2c 00 00       	mov    $0x2cf5,%eax
     53f:	e9 d8 fe ff ff       	jmp    41c <_test_allocation_generic+0x48>
    printf(1, "... and writing in the child process after forking and reading from the parent after that\n");
     544:	83 ec 08             	sub    $0x8,%esp
     547:	68 9c 23 00 00       	push   $0x239c
     54c:	6a 01                	push   $0x1
     54e:	e8 60 18 00 00       	call   1db3 <printf>
     553:	83 c4 10             	add    $0x10,%esp
     556:	e9 de fe ff ff       	jmp    439 <_test_allocation_generic+0x65>
      printf(1, FAIL_MSG "fork failed");
     55b:	83 ec 08             	sub    $0x8,%esp
     55e:	68 88 2c 00 00       	push   $0x2c88
     563:	6a 01                	push   $0x1
     565:	e8 49 18 00 00       	call   1db3 <printf>
     56a:	83 c4 10             	add    $0x10,%esp
     56d:	e9 8d 00 00 00       	jmp    5ff <_test_allocation_generic+0x22b>
          printf(1, FAIL_MSG "allocation test did not return result from both processes after fork()ing after allocation?");
     572:	83 ec 08             	sub    $0x8,%esp
     575:	68 f8 23 00 00       	push   $0x23f8
     57a:	6a 01                	push   $0x1
     57c:	e8 32 18 00 00       	call   1db3 <printf>
          return 0;
     581:	83 c4 10             	add    $0x10,%esp
     584:	be 00 00 00 00       	mov    $0x0,%esi
     589:	e9 ae 00 00 00       	jmp    63c <_test_allocation_generic+0x268>
          printf(1, "... test failed in child process:\n");
     58e:	83 ec 08             	sub    $0x8,%esp
     591:	68 64 24 00 00       	push   $0x2464
     596:	6a 01                	push   $0x1
     598:	e8 16 18 00 00       	call   1db3 <printf>
          _allocation_failure_message(2, text);
     59d:	83 c4 08             	add    $0x8,%esp
     5a0:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     5a3:	50                   	push   %eax
     5a4:	6a 02                	push   $0x2
     5a6:	e8 b5 fc ff ff       	call   260 <_allocation_failure_message>
          return 0;
     5ab:	83 c4 10             	add    $0x10,%esp
     5ae:	be 00 00 00 00       	mov    $0x0,%esi
     5b3:	e9 84 00 00 00       	jmp    63c <_test_allocation_generic+0x268>
        }
      } else if (size < 1 || text[0] != 'Y') {
     5b8:	85 db                	test   %ebx,%ebx
     5ba:	7e 06                	jle    5c2 <_test_allocation_generic+0x1ee>
     5bc:	80 7d d6 59          	cmpb   $0x59,-0x2a(%ebp)
     5c0:	74 15                	je     5d7 <_test_allocation_generic+0x203>
        _allocation_failure_message(size, text);
     5c2:	83 ec 08             	sub    $0x8,%esp
     5c5:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     5c8:	50                   	push   %eax
     5c9:	53                   	push   %ebx
     5ca:	e8 91 fc ff ff       	call   260 <_allocation_failure_message>
        return 0;
     5cf:	83 c4 10             	add    $0x10,%esp
     5d2:	8b 75 0c             	mov    0xc(%ebp),%esi
     5d5:	eb 65                	jmp    63c <_test_allocation_generic+0x268>
      } else {
        printf(1, PASS_MSG "allocating %s and using %s parts of allocation passed\n", describe_size, describe_amount);
     5d7:	57                   	push   %edi
     5d8:	ff 75 14             	pushl  0x14(%ebp)
     5db:	68 b4 24 00 00       	push   $0x24b4
     5e0:	6a 01                	push   $0x1
     5e2:	e8 cc 17 00 00       	call   1db3 <printf>
        return 1;
     5e7:	83 c4 10             	add    $0x10,%esp
     5ea:	be 01 00 00 00       	mov    $0x1,%esi
     5ef:	eb 4b                	jmp    63c <_test_allocation_generic+0x268>
      }
    } else {
      close(fds[0]);
     5f1:	83 ec 0c             	sub    $0xc,%esp
     5f4:	ff 75 e0             	pushl  -0x20(%ebp)
     5f7:	e8 6d 16 00 00       	call   1c69 <close>
     5fc:	83 c4 10             	add    $0x10,%esp
    }
  }
  char *old_end_of_heap = sbrk(size);
     5ff:	83 ec 0c             	sub    $0xc,%esp
     602:	ff 75 10             	pushl  0x10(%ebp)
     605:	e8 bf 16 00 00       	call   1cc9 <sbrk>
     60a:	89 c7                	mov    %eax,%edi
  char *new_end_of_heap = sbrk(0);
     60c:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     613:	e8 b1 16 00 00       	call   1cc9 <sbrk>
  if (old_end_of_heap == (char*) -1) {
     618:	83 c4 10             	add    $0x10,%esp
     61b:	83 ff ff             	cmp    $0xffffffff,%edi
     61e:	74 26                	je     646 <_test_allocation_generic+0x272>
    _fail_allocation_test(fds[1], 's');
    return 0;
  } else if (new_end_of_heap - old_end_of_heap != size) {
     620:	29 f8                	sub    %edi,%eax
     622:	3b 45 10             	cmp    0x10(%ebp),%eax
     625:	74 36                	je     65d <_test_allocation_generic+0x289>
    _fail_allocation_test(fds[1], 'S');
     627:	83 ec 08             	sub    $0x8,%esp
     62a:	6a 53                	push   $0x53
     62c:	ff 75 e4             	pushl  -0x1c(%ebp)
     62f:	e8 26 fd ff ff       	call   35a <_fail_allocation_test>
    return 0;
     634:	83 c4 10             	add    $0x10,%esp
     637:	be 00 00 00 00       	mov    $0x0,%esi
        "allocation passed (expand + write + read heap)\n"
      );
      return 1;
    }
  }
}
     63c:	89 f0                	mov    %esi,%eax
     63e:	8d 65 f4             	lea    -0xc(%ebp),%esp
     641:	5b                   	pop    %ebx
     642:	5e                   	pop    %esi
     643:	5f                   	pop    %edi
     644:	5d                   	pop    %ebp
     645:	c3                   	ret    
    _fail_allocation_test(fds[1], 's');
     646:	83 ec 08             	sub    $0x8,%esp
     649:	6a 73                	push   $0x73
     64b:	ff 75 e4             	pushl  -0x1c(%ebp)
     64e:	e8 07 fd ff ff       	call   35a <_fail_allocation_test>
    return 0;
     653:	83 c4 10             	add    $0x10,%esp
     656:	be 00 00 00 00       	mov    $0x0,%esi
     65b:	eb df                	jmp    63c <_test_allocation_generic+0x268>
    dump_for("allocation-pre-access", getpid());
     65d:	e8 5f 16 00 00       	call   1cc1 <getpid>
     662:	83 ec 08             	sub    $0x8,%esp
     665:	50                   	push   %eax
     666:	68 3a 2d 00 00       	push   $0x2d3a
     66b:	e8 be f9 ff ff       	call   2e <dump_for>
    char *place_one = &old_end_of_heap[offset1];
     670:	89 fb                	mov    %edi,%ebx
     672:	03 5d 1c             	add    0x1c(%ebp),%ebx
    char *place_two = &old_end_of_heap[offset2];
     675:	03 7d 24             	add    0x24(%ebp),%edi
    for (i = 0; i < count1; ++i) {
     678:	83 c4 10             	add    $0x10,%esp
     67b:	b8 00 00 00 00       	mov    $0x0,%eax
     680:	8b 55 20             	mov    0x20(%ebp),%edx
     683:	eb 07                	jmp    68c <_test_allocation_generic+0x2b8>
      place_one[i] = 'A';
     685:	c6 04 03 41          	movb   $0x41,(%ebx,%eax,1)
    for (i = 0; i < count1; ++i) {
     689:	83 c0 01             	add    $0x1,%eax
     68c:	39 d0                	cmp    %edx,%eax
     68e:	7d 21                	jge    6b1 <_test_allocation_generic+0x2dd>
      if (check_zero && place_one[i] != '\0') {
     690:	85 f6                	test   %esi,%esi
     692:	74 f1                	je     685 <_test_allocation_generic+0x2b1>
     694:	80 3c 03 00          	cmpb   $0x0,(%ebx,%eax,1)
     698:	74 eb                	je     685 <_test_allocation_generic+0x2b1>
        _fail_allocation_test(fds[1], 'I');
     69a:	83 ec 08             	sub    $0x8,%esp
     69d:	6a 49                	push   $0x49
     69f:	ff 75 e4             	pushl  -0x1c(%ebp)
     6a2:	e8 b3 fc ff ff       	call   35a <_fail_allocation_test>
        return 0;
     6a7:	83 c4 10             	add    $0x10,%esp
     6aa:	be 00 00 00 00       	mov    $0x0,%esi
     6af:	eb 8b                	jmp    63c <_test_allocation_generic+0x268>
    for (i = 0; i < count2; ++i) {
     6b1:	b8 00 00 00 00       	mov    $0x0,%eax
     6b6:	8b 55 28             	mov    0x28(%ebp),%edx
     6b9:	eb 07                	jmp    6c2 <_test_allocation_generic+0x2ee>
      place_two[i] = 'B';
     6bb:	c6 04 07 42          	movb   $0x42,(%edi,%eax,1)
    for (i = 0; i < count2; ++i) {
     6bf:	83 c0 01             	add    $0x1,%eax
     6c2:	39 d0                	cmp    %edx,%eax
     6c4:	7d 24                	jge    6ea <_test_allocation_generic+0x316>
      if (check_zero && place_two[i] != '\0') {
     6c6:	85 f6                	test   %esi,%esi
     6c8:	74 f1                	je     6bb <_test_allocation_generic+0x2e7>
     6ca:	80 3c 07 00          	cmpb   $0x0,(%edi,%eax,1)
     6ce:	74 eb                	je     6bb <_test_allocation_generic+0x2e7>
        _fail_allocation_test(fds[1], 'I');
     6d0:	83 ec 08             	sub    $0x8,%esp
     6d3:	6a 49                	push   $0x49
     6d5:	ff 75 e4             	pushl  -0x1c(%ebp)
     6d8:	e8 7d fc ff ff       	call   35a <_fail_allocation_test>
        return 0;
     6dd:	83 c4 10             	add    $0x10,%esp
     6e0:	be 00 00 00 00       	mov    $0x0,%esi
     6e5:	e9 52 ff ff ff       	jmp    63c <_test_allocation_generic+0x268>
    dump_for("allocation-post-access", getpid());
     6ea:	e8 d2 15 00 00       	call   1cc1 <getpid>
     6ef:	83 ec 08             	sub    $0x8,%esp
     6f2:	50                   	push   %eax
     6f3:	68 50 2d 00 00       	push   $0x2d50
     6f8:	e8 31 f9 ff ff       	call   2e <dump_for>
    if (fork_after) {
     6fd:	83 c4 10             	add    $0x10,%esp
     700:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     704:	75 29                	jne    72f <_test_allocation_generic+0x35b>
      if (fork_after) {
     706:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     70a:	0f 85 d9 00 00 00    	jne    7e9 <_test_allocation_generic+0x415>
      for (i = 0; i < count1; ++i) {
     710:	b8 00 00 00 00       	mov    $0x0,%eax
     715:	8b 55 20             	mov    0x20(%ebp),%edx
     718:	39 d0                	cmp    %edx,%eax
     71a:	0f 8d 03 01 00 00    	jge    823 <_test_allocation_generic+0x44f>
        if (place_one[i] != 'A') {
     720:	80 3c 03 41          	cmpb   $0x41,(%ebx,%eax,1)
     724:	0f 85 df 00 00 00    	jne    809 <_test_allocation_generic+0x435>
      for (i = 0; i < count1; ++i) {
     72a:	83 c0 01             	add    $0x1,%eax
     72d:	eb e9                	jmp    718 <_test_allocation_generic+0x344>
      pid = fork();
     72f:	e8 05 15 00 00       	call   1c39 <fork>
     734:	89 c6                	mov    %eax,%esi
      if (pid == -1) {
     736:	83 f8 ff             	cmp    $0xffffffff,%eax
     739:	74 2e                	je     769 <_test_allocation_generic+0x395>
    if (pid == 0) {
     73b:	85 c0                	test   %eax,%eax
     73d:	75 c7                	jne    706 <_test_allocation_generic+0x332>
      dump_for("allocation-post-fork-child", getpid());
     73f:	e8 7d 15 00 00       	call   1cc1 <getpid>
     744:	83 ec 08             	sub    $0x8,%esp
     747:	50                   	push   %eax
     748:	68 67 2d 00 00       	push   $0x2d67
     74d:	e8 dc f8 ff ff       	call   2e <dump_for>
      for (i = 0; i < count1; ++i) {
     752:	83 c4 10             	add    $0x10,%esp
     755:	89 f0                	mov    %esi,%eax
     757:	8b 55 20             	mov    0x20(%ebp),%edx
     75a:	39 d0                	cmp    %edx,%eax
     75c:	7d 3a                	jge    798 <_test_allocation_generic+0x3c4>
        if (place_one[i] != 'A') {
     75e:	80 3c 03 41          	cmpb   $0x41,(%ebx,%eax,1)
     762:	75 1f                	jne    783 <_test_allocation_generic+0x3af>
      for (i = 0; i < count1; ++i) {
     764:	83 c0 01             	add    $0x1,%eax
     767:	eb f1                	jmp    75a <_test_allocation_generic+0x386>
        _fail_allocation_test(fds[1], 'F');
     769:	83 ec 08             	sub    $0x8,%esp
     76c:	6a 46                	push   $0x46
     76e:	ff 75 e4             	pushl  -0x1c(%ebp)
     771:	e8 e4 fb ff ff       	call   35a <_fail_allocation_test>
        return 0;
     776:	83 c4 10             	add    $0x10,%esp
     779:	be 00 00 00 00       	mov    $0x0,%esi
     77e:	e9 b9 fe ff ff       	jmp    63c <_test_allocation_generic+0x268>
          _fail_allocation_test(fds[1], 'R');
     783:	83 ec 08             	sub    $0x8,%esp
     786:	6a 52                	push   $0x52
     788:	ff 75 e4             	pushl  -0x1c(%ebp)
     78b:	e8 ca fb ff ff       	call   35a <_fail_allocation_test>
          return 0;
     790:	83 c4 10             	add    $0x10,%esp
     793:	e9 a4 fe ff ff       	jmp    63c <_test_allocation_generic+0x268>
      for (i = 0; i < count2; ++i) {
     798:	89 f0                	mov    %esi,%eax
     79a:	8b 55 28             	mov    0x28(%ebp),%edx
     79d:	eb 03                	jmp    7a2 <_test_allocation_generic+0x3ce>
     79f:	83 c0 01             	add    $0x1,%eax
     7a2:	39 d0                	cmp    %edx,%eax
     7a4:	7d 1b                	jge    7c1 <_test_allocation_generic+0x3ed>
        if (place_two[i] != 'B') {
     7a6:	80 3c 07 42          	cmpb   $0x42,(%edi,%eax,1)
     7aa:	74 f3                	je     79f <_test_allocation_generic+0x3cb>
          _fail_allocation_test(fds[1], 'R');
     7ac:	83 ec 08             	sub    $0x8,%esp
     7af:	6a 52                	push   $0x52
     7b1:	ff 75 e4             	pushl  -0x1c(%ebp)
     7b4:	e8 a1 fb ff ff       	call   35a <_fail_allocation_test>
          return 0;
     7b9:	83 c4 10             	add    $0x10,%esp
     7bc:	e9 7b fe ff ff       	jmp    63c <_test_allocation_generic+0x268>
     7c1:	89 c6                	mov    %eax,%esi
      _pass_allocation_test(fds[1], "allocation passed in child (expand + write + fork + read heap in child)\n");
     7c3:	83 ec 08             	sub    $0x8,%esp
     7c6:	68 f8 24 00 00       	push   $0x24f8
     7cb:	ff 75 e4             	pushl  -0x1c(%ebp)
     7ce:	e8 c3 fb ff ff       	call   396 <_pass_allocation_test>
      if (write_after) {
     7d3:	83 c4 10             	add    $0x10,%esp
     7d6:	83 7d 30 00          	cmpl   $0x0,0x30(%ebp)
     7da:	74 08                	je     7e4 <_test_allocation_generic+0x410>
        place_one[i] = 'X';
     7dc:	c6 04 33 58          	movb   $0x58,(%ebx,%esi,1)
        place_two[i] = 'Y';
     7e0:	c6 04 37 59          	movb   $0x59,(%edi,%esi,1)
      exit();
     7e4:	e8 58 14 00 00       	call   1c41 <exit>
        wait();
     7e9:	e8 5b 14 00 00       	call   1c49 <wait>
        dump_for("allocation-post-fork-parent", getpid());
     7ee:	e8 ce 14 00 00       	call   1cc1 <getpid>
     7f3:	83 ec 08             	sub    $0x8,%esp
     7f6:	50                   	push   %eax
     7f7:	68 82 2d 00 00       	push   $0x2d82
     7fc:	e8 2d f8 ff ff       	call   2e <dump_for>
     801:	83 c4 10             	add    $0x10,%esp
     804:	e9 07 ff ff ff       	jmp    710 <_test_allocation_generic+0x33c>
          _fail_allocation_test(fds[1], 'R');
     809:	83 ec 08             	sub    $0x8,%esp
     80c:	6a 52                	push   $0x52
     80e:	ff 75 e4             	pushl  -0x1c(%ebp)
     811:	e8 44 fb ff ff       	call   35a <_fail_allocation_test>
          return 0;
     816:	83 c4 10             	add    $0x10,%esp
     819:	be 00 00 00 00       	mov    $0x0,%esi
     81e:	e9 19 fe ff ff       	jmp    63c <_test_allocation_generic+0x268>
      for (i = 0; i < count2; ++i) {
     823:	b8 00 00 00 00       	mov    $0x0,%eax
     828:	8b 55 28             	mov    0x28(%ebp),%edx
     82b:	39 d0                	cmp    %edx,%eax
     82d:	7d 25                	jge    854 <_test_allocation_generic+0x480>
        if (place_two[i] != 'B') {
     82f:	80 3c 07 42          	cmpb   $0x42,(%edi,%eax,1)
     833:	75 05                	jne    83a <_test_allocation_generic+0x466>
      for (i = 0; i < count2; ++i) {
     835:	83 c0 01             	add    $0x1,%eax
     838:	eb f1                	jmp    82b <_test_allocation_generic+0x457>
          _fail_allocation_test(fds[1], 'R');
     83a:	83 ec 08             	sub    $0x8,%esp
     83d:	6a 52                	push   $0x52
     83f:	ff 75 e4             	pushl  -0x1c(%ebp)
     842:	e8 13 fb ff ff       	call   35a <_fail_allocation_test>
          return 0;
     847:	83 c4 10             	add    $0x10,%esp
     84a:	be 00 00 00 00       	mov    $0x0,%esi
     84f:	e9 e8 fd ff ff       	jmp    63c <_test_allocation_generic+0x268>
      _pass_allocation_test(fds[1], fork_after ?
     854:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     858:	74 1e                	je     878 <_test_allocation_generic+0x4a4>
     85a:	b8 50 22 00 00       	mov    $0x2250,%eax
     85f:	83 ec 08             	sub    $0x8,%esp
     862:	50                   	push   %eax
     863:	ff 75 e4             	pushl  -0x1c(%ebp)
     866:	e8 2b fb ff ff       	call   396 <_pass_allocation_test>
      return 1;
     86b:	83 c4 10             	add    $0x10,%esp
     86e:	be 01 00 00 00       	mov    $0x1,%esi
     873:	e9 c4 fd ff ff       	jmp    63c <_test_allocation_generic+0x268>
      _pass_allocation_test(fds[1], fork_after ?
     878:	b8 a4 22 00 00       	mov    $0x22a4,%eax
     87d:	eb e0                	jmp    85f <_test_allocation_generic+0x48b>

0000087f <test_allocation_no_fork>:


int test_allocation_no_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero) {
     87f:	55                   	push   %ebp
     880:	89 e5                	mov    %esp,%ebp
     882:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(0, 0, size, describe_size, describe_amount, offset1, count1, offset2, count2, check_zero, 0);
     885:	6a 00                	push   $0x0
     887:	ff 75 24             	pushl  0x24(%ebp)
     88a:	ff 75 20             	pushl  0x20(%ebp)
     88d:	ff 75 1c             	pushl  0x1c(%ebp)
     890:	ff 75 18             	pushl  0x18(%ebp)
     893:	ff 75 14             	pushl  0x14(%ebp)
     896:	ff 75 10             	pushl  0x10(%ebp)
     899:	ff 75 0c             	pushl  0xc(%ebp)
     89c:	ff 75 08             	pushl  0x8(%ebp)
     89f:	6a 00                	push   $0x0
     8a1:	6a 00                	push   $0x0
     8a3:	e8 2c fb ff ff       	call   3d4 <_test_allocation_generic>
}
     8a8:	c9                   	leave  
     8a9:	c3                   	ret    

000008aa <test_allocation_then_fork>:

int test_allocation_then_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero, int write_after) {
     8aa:	55                   	push   %ebp
     8ab:	89 e5                	mov    %esp,%ebp
     8ad:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(0, 1, size, describe_size, describe_amount, offset1, count1, offset2, count2, check_zero, write_after);
     8b0:	ff 75 28             	pushl  0x28(%ebp)
     8b3:	ff 75 24             	pushl  0x24(%ebp)
     8b6:	ff 75 20             	pushl  0x20(%ebp)
     8b9:	ff 75 1c             	pushl  0x1c(%ebp)
     8bc:	ff 75 18             	pushl  0x18(%ebp)
     8bf:	ff 75 14             	pushl  0x14(%ebp)
     8c2:	ff 75 10             	pushl  0x10(%ebp)
     8c5:	ff 75 0c             	pushl  0xc(%ebp)
     8c8:	ff 75 08             	pushl  0x8(%ebp)
     8cb:	6a 01                	push   $0x1
     8cd:	6a 00                	push   $0x0
     8cf:	e8 00 fb ff ff       	call   3d4 <_test_allocation_generic>
}
     8d4:	c9                   	leave  
     8d5:	c3                   	ret    

000008d6 <test_allocation_fork>:


int test_allocation_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2) {
     8d6:	55                   	push   %ebp
     8d7:	89 e5                	mov    %esp,%ebp
     8d9:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(1, 0, size, describe_size, describe_amount, offset1, count1, offset2, count2, 1, 0);
     8dc:	6a 00                	push   $0x0
     8de:	6a 01                	push   $0x1
     8e0:	ff 75 20             	pushl  0x20(%ebp)
     8e3:	ff 75 1c             	pushl  0x1c(%ebp)
     8e6:	ff 75 18             	pushl  0x18(%ebp)
     8e9:	ff 75 14             	pushl  0x14(%ebp)
     8ec:	ff 75 10             	pushl  0x10(%ebp)
     8ef:	ff 75 0c             	pushl  0xc(%ebp)
     8f2:	ff 75 08             	pushl  0x8(%ebp)
     8f5:	6a 00                	push   $0x0
     8f7:	6a 01                	push   $0x1
     8f9:	e8 d6 fa ff ff       	call   3d4 <_test_allocation_generic>
}
     8fe:	c9                   	leave  
     8ff:	c3                   	ret    

00000900 <wait_forever>:

void wait_forever() {
     900:	55                   	push   %ebp
     901:	89 e5                	mov    %esp,%ebp
     903:	83 ec 08             	sub    $0x8,%esp
  while (1) { sleep(1000); }
     906:	83 ec 0c             	sub    $0xc,%esp
     909:	68 e8 03 00 00       	push   $0x3e8
     90e:	e8 be 13 00 00       	call   1cd1 <sleep>
     913:	83 c4 10             	add    $0x10,%esp
     916:	eb ee                	jmp    906 <wait_forever+0x6>

00000918 <test_copy_on_write_main_child>:
}

void test_copy_on_write_main_child(int result_fd, int size, const char *describe_size, int forks) {
     918:	55                   	push   %ebp
     919:	89 e5                	mov    %esp,%ebp
     91b:	57                   	push   %edi
     91c:	56                   	push   %esi
     91d:	53                   	push   %ebx
     91e:	83 ec 78             	sub    $0x78,%esp
  char *old_end_of_heap = sbrk(size);
     921:	ff 75 0c             	pushl  0xc(%ebp)
     924:	e8 a0 13 00 00       	call   1cc9 <sbrk>
     929:	89 c6                	mov    %eax,%esi
  char *new_end_of_heap = sbrk(0);
     92b:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     932:	e8 92 13 00 00       	call   1cc9 <sbrk>
     937:	89 c2                	mov    %eax,%edx
     939:	89 45 8c             	mov    %eax,-0x74(%ebp)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     93c:	83 c4 10             	add    $0x10,%esp
     93f:	89 f0                	mov    %esi,%eax
     941:	eb 06                	jmp    949 <test_copy_on_write_main_child+0x31>
      *p = 'A';
     943:	c6 00 41             	movb   $0x41,(%eax)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     946:	83 c0 01             	add    $0x1,%eax
     949:	39 d0                	cmp    %edx,%eax
     94b:	72 f6                	jb     943 <test_copy_on_write_main_child+0x2b>
  }
  int children[MAX_CHILDREN] = {0};
     94d:	8d 7d a8             	lea    -0x58(%ebp),%edi
     950:	b9 10 00 00 00       	mov    $0x10,%ecx
     955:	b8 00 00 00 00       	mov    $0x0,%eax
     95a:	f3 ab                	rep stos %eax,%es:(%edi)
  if (forks > MAX_CHILDREN) {
     95c:	83 7d 14 10          	cmpl   $0x10,0x14(%ebp)
     960:	7f 31                	jg     993 <test_copy_on_write_main_child+0x7b>
    printf(2, "unsupported number of children in test_copy_on_write\n");
  }
  int failed = 0;
  char failed_code = ' ';
  dump_for("copy-write-parent-before", getpid());
     962:	e8 5a 13 00 00       	call   1cc1 <getpid>
     967:	83 ec 08             	sub    $0x8,%esp
     96a:	50                   	push   %eax
     96b:	68 a2 2d 00 00       	push   $0x2da2
     970:	e8 b9 f6 ff ff       	call   2e <dump_for>
  for (int i = 0; i < forks; ++i) {
     975:	83 c4 10             	add    $0x10,%esp
     978:	bf 00 00 00 00       	mov    $0x0,%edi
  char failed_code = ' ';
     97d:	c6 45 90 20          	movb   $0x20,-0x70(%ebp)
  int failed = 0;
     981:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
     988:	89 75 88             	mov    %esi,-0x78(%ebp)
     98b:	8b 75 14             	mov    0x14(%ebp),%esi
  for (int i = 0; i < forks; ++i) {
     98e:	e9 28 02 00 00       	jmp    bbb <test_copy_on_write_main_child+0x2a3>
    printf(2, "unsupported number of children in test_copy_on_write\n");
     993:	83 ec 08             	sub    $0x8,%esp
     996:	68 44 25 00 00       	push   $0x2544
     99b:	6a 02                	push   $0x2
     99d:	e8 11 14 00 00       	call   1db3 <printf>
     9a2:	83 c4 10             	add    $0x10,%esp
     9a5:	eb bb                	jmp    962 <test_copy_on_write_main_child+0x4a>
     9a7:	8b 75 88             	mov    -0x78(%ebp),%esi
    int child_fds[2];
    pipe(child_fds);
    children[i] = fork();
    if (children[i] == -1) {
      printf(2, "fork failed\n");
     9aa:	83 ec 08             	sub    $0x8,%esp
     9ad:	68 b0 2c 00 00       	push   $0x2cb0
     9b2:	6a 02                	push   $0x2
     9b4:	e8 fa 13 00 00       	call   1db3 <printf>
      failed = 1;
      failed_code = 'f';
      break;
     9b9:	83 c4 10             	add    $0x10,%esp
      failed_code = 'f';
     9bc:	c6 45 90 66          	movb   $0x66,-0x70(%ebp)
      failed = 1;
     9c0:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     9c7:	e9 53 02 00 00       	jmp    c1f <test_copy_on_write_main_child+0x307>
     9cc:	8b 75 88             	mov    -0x78(%ebp),%esi
    } else if (children[i] == 0) {
      dump_for("copy-write-child-before-writes", getpid());
     9cf:	e8 ed 12 00 00       	call   1cc1 <getpid>
     9d4:	83 ec 08             	sub    $0x8,%esp
     9d7:	50                   	push   %eax
     9d8:	68 7c 25 00 00       	push   $0x257c
     9dd:	e8 4c f6 ff ff       	call   2e <dump_for>
      int found_wrong_memory = 0;
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     9e2:	83 c4 10             	add    $0x10,%esp
     9e5:	89 f0                	mov    %esi,%eax
     9e7:	eb 03                	jmp    9ec <test_copy_on_write_main_child+0xd4>
     9e9:	83 c0 01             	add    $0x1,%eax
     9ec:	3b 45 8c             	cmp    -0x74(%ebp),%eax
     9ef:	73 0c                	jae    9fd <test_copy_on_write_main_child+0xe5>
        if (*p != 'A') {
     9f1:	80 38 41             	cmpb   $0x41,(%eax)
     9f4:	74 f3                	je     9e9 <test_copy_on_write_main_child+0xd1>
          found_wrong_memory = 1;
     9f6:	bb 01 00 00 00       	mov    $0x1,%ebx
     9fb:	eb ec                	jmp    9e9 <test_copy_on_write_main_child+0xd1>
        }
      }
      int place_one = size / 2;
     9fd:	8b 45 0c             	mov    0xc(%ebp),%eax
     a00:	c1 e8 1f             	shr    $0x1f,%eax
     a03:	03 45 0c             	add    0xc(%ebp),%eax
     a06:	d1 f8                	sar    %eax
      old_end_of_heap[place_one] = 'B' + i;
     a08:	01 f0                	add    %esi,%eax
     a0a:	89 c1                	mov    %eax,%ecx
     a0c:	89 45 94             	mov    %eax,-0x6c(%ebp)
     a0f:	8d 47 42             	lea    0x42(%edi),%eax
     a12:	88 01                	mov    %al,(%ecx)
      int place_two = 4096 * i;
     a14:	89 f8                	mov    %edi,%eax
     a16:	c1 e0 0c             	shl    $0xc,%eax
     a19:	89 45 90             	mov    %eax,-0x70(%ebp)
      if (place_two >= size) {
     a1c:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a1f:	7f 09                	jg     a2a <test_copy_on_write_main_child+0x112>
          place_two = size - 1;
     a21:	8b 45 0c             	mov    0xc(%ebp),%eax
     a24:	83 e8 01             	sub    $0x1,%eax
     a27:	89 45 90             	mov    %eax,-0x70(%ebp)
      }
      if (size <= 4096) {
     a2a:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a31:	0f 8f b8 00 00 00    	jg     aef <test_copy_on_write_main_child+0x1d7>
          dump_for("copy-write-child-after-first-write", getpid());
     a37:	e8 85 12 00 00       	call   1cc1 <getpid>
     a3c:	83 ec 08             	sub    $0x8,%esp
     a3f:	50                   	push   %eax
     a40:	68 9c 25 00 00       	push   $0x259c
     a45:	e8 e4 f5 ff ff       	call   2e <dump_for>
     a4a:	83 c4 10             	add    $0x10,%esp
      } else if (size > 4096) {
          dump_for("copy-write-child-after-write-1", getpid());
      }
      old_end_of_heap[place_two] = 'C';
     a4d:	8b 45 90             	mov    -0x70(%ebp),%eax
     a50:	c6 04 06 43          	movb   $0x43,(%esi,%eax,1)
      int place_three = 4096 * (i - 1);
     a54:	8d 47 ff             	lea    -0x1(%edi),%eax
     a57:	c1 e0 0c             	shl    $0xc,%eax
     a5a:	89 45 90             	mov    %eax,-0x70(%ebp)
      if (place_three >= size || place_three < 0) {
     a5d:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a60:	0f 9e c2             	setle  %dl
     a63:	c1 e8 1f             	shr    $0x1f,%eax
     a66:	08 c2                	or     %al,%dl
     a68:	74 09                	je     a73 <test_copy_on_write_main_child+0x15b>
          place_three = size - 2;
     a6a:	8b 45 0c             	mov    0xc(%ebp),%eax
     a6d:	83 e8 02             	sub    $0x2,%eax
     a70:	89 45 90             	mov    %eax,-0x70(%ebp)
      }
      if (size > 4096) {
     a73:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a7a:	0f 8f 8a 00 00 00    	jg     b0a <test_copy_on_write_main_child+0x1f2>
          dump_for("copy-write-child-after-write-2", getpid());
      }
      int place_four = 4096 * (i + 1);
     a80:	8d 47 01             	lea    0x1(%edi),%eax
     a83:	c1 e0 0c             	shl    $0xc,%eax
     a86:	89 45 8c             	mov    %eax,-0x74(%ebp)
      if (place_four >= size) {
     a89:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a8c:	7f 09                	jg     a97 <test_copy_on_write_main_child+0x17f>
          place_four = size - 3;
     a8e:	8b 45 0c             	mov    0xc(%ebp),%eax
     a91:	83 e8 03             	sub    $0x3,%eax
     a94:	89 45 8c             	mov    %eax,-0x74(%ebp)
      }
      if (size > 4096) {
     a97:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a9e:	0f 8f 81 00 00 00    	jg     b25 <test_copy_on_write_main_child+0x20d>
          dump_for("copy-write-child-after-write-3", getpid());
      }
      printf(1, "[Debugging info: three: %c; one: %c; four: %c; already_wrong: %d; i: %d]\n",
        old_end_of_heap[place_three],
        old_end_of_heap[place_one],
        old_end_of_heap[place_four],
     aa4:	8b 45 8c             	mov    -0x74(%ebp),%eax
     aa7:	01 f0                	add    %esi,%eax
     aa9:	89 45 8c             	mov    %eax,-0x74(%ebp)
        old_end_of_heap[place_three],
     aac:	03 75 90             	add    -0x70(%ebp),%esi
      printf(1, "[Debugging info: three: %c; one: %c; four: %c; already_wrong: %d; i: %d]\n",
     aaf:	83 ec 04             	sub    $0x4,%esp
     ab2:	57                   	push   %edi
     ab3:	53                   	push   %ebx
     ab4:	0f be 00             	movsbl (%eax),%eax
     ab7:	50                   	push   %eax
     ab8:	8b 45 94             	mov    -0x6c(%ebp),%eax
     abb:	0f be 00             	movsbl (%eax),%eax
     abe:	50                   	push   %eax
     abf:	0f be 06             	movsbl (%esi),%eax
     ac2:	50                   	push   %eax
     ac3:	68 20 26 00 00       	push   $0x2620
     ac8:	6a 01                	push   $0x1
     aca:	e8 e4 12 00 00       	call   1db3 <printf>
        found_wrong_memory,
        i);
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     acf:	83 c4 20             	add    $0x20,%esp
     ad2:	80 3e 41             	cmpb   $0x41,(%esi)
     ad5:	74 69                	je     b40 <test_copy_on_write_main_child+0x228>
          old_end_of_heap[place_four] != 'A') {
          found_wrong_memory = 1;
      }
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     ad7:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     adc:	83 ec 04             	sub    $0x4,%esp
     adf:	6a 01                	push   $0x1
     ae1:	50                   	push   %eax
     ae2:	ff 75 a4             	pushl  -0x5c(%ebp)
     ae5:	e8 77 11 00 00       	call   1c61 <write>
      wait_forever();
     aea:	e8 11 fe ff ff       	call   900 <wait_forever>
          dump_for("copy-write-child-after-write-1", getpid());
     aef:	e8 cd 11 00 00       	call   1cc1 <getpid>
     af4:	83 ec 08             	sub    $0x8,%esp
     af7:	50                   	push   %eax
     af8:	68 c0 25 00 00       	push   $0x25c0
     afd:	e8 2c f5 ff ff       	call   2e <dump_for>
     b02:	83 c4 10             	add    $0x10,%esp
     b05:	e9 43 ff ff ff       	jmp    a4d <test_copy_on_write_main_child+0x135>
          dump_for("copy-write-child-after-write-2", getpid());
     b0a:	e8 b2 11 00 00       	call   1cc1 <getpid>
     b0f:	83 ec 08             	sub    $0x8,%esp
     b12:	50                   	push   %eax
     b13:	68 e0 25 00 00       	push   $0x25e0
     b18:	e8 11 f5 ff ff       	call   2e <dump_for>
     b1d:	83 c4 10             	add    $0x10,%esp
     b20:	e9 5b ff ff ff       	jmp    a80 <test_copy_on_write_main_child+0x168>
          dump_for("copy-write-child-after-write-3", getpid());
     b25:	e8 97 11 00 00       	call   1cc1 <getpid>
     b2a:	83 ec 08             	sub    $0x8,%esp
     b2d:	50                   	push   %eax
     b2e:	68 00 26 00 00       	push   $0x2600
     b33:	e8 f6 f4 ff ff       	call   2e <dump_for>
     b38:	83 c4 10             	add    $0x10,%esp
     b3b:	e9 64 ff ff ff       	jmp    aa4 <test_copy_on_write_main_child+0x18c>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     b40:	8b 45 94             	mov    -0x6c(%ebp),%eax
     b43:	0f be 00             	movsbl (%eax),%eax
     b46:	83 c7 42             	add    $0x42,%edi
     b49:	39 f8                	cmp    %edi,%eax
     b4b:	74 07                	je     b54 <test_copy_on_write_main_child+0x23c>
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     b4d:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     b52:	eb 88                	jmp    adc <test_copy_on_write_main_child+0x1c4>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     b54:	8b 45 8c             	mov    -0x74(%ebp),%eax
     b57:	80 38 41             	cmpb   $0x41,(%eax)
     b5a:	75 0e                	jne    b6a <test_copy_on_write_main_child+0x252>
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     b5c:	85 db                	test   %ebx,%ebx
     b5e:	74 14                	je     b74 <test_copy_on_write_main_child+0x25c>
     b60:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     b65:	e9 72 ff ff ff       	jmp    adc <test_copy_on_write_main_child+0x1c4>
     b6a:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     b6f:	e9 68 ff ff ff       	jmp    adc <test_copy_on_write_main_child+0x1c4>
     b74:	b8 a0 2d 00 00       	mov    $0x2da0,%eax
     b79:	e9 5e ff ff ff       	jmp    adc <test_copy_on_write_main_child+0x1c4>
      read(child_fds[0], buffer, 1);
      if (buffer[0] != '+') {
        failed = 1;
        failed_code = 'c';
      }
      close(child_fds[0]); close(child_fds[1]);
     b7e:	83 ec 0c             	sub    $0xc,%esp
     b81:	ff 75 a0             	pushl  -0x60(%ebp)
     b84:	e8 e0 10 00 00       	call   1c69 <close>
     b89:	83 c4 04             	add    $0x4,%esp
     b8c:	ff 75 a4             	pushl  -0x5c(%ebp)
     b8f:	e8 d5 10 00 00       	call   1c69 <close>
      dump_for("copy-write-parent-after", getpid());
     b94:	e8 28 11 00 00       	call   1cc1 <getpid>
     b99:	83 c4 08             	add    $0x8,%esp
     b9c:	50                   	push   %eax
     b9d:	68 bb 2d 00 00       	push   $0x2dbb
     ba2:	e8 87 f4 ff ff       	call   2e <dump_for>
      dump_for("copy-write-child-after", children[i]);
     ba7:	83 c4 08             	add    $0x8,%esp
     baa:	53                   	push   %ebx
     bab:	68 d3 2d 00 00       	push   $0x2dd3
     bb0:	e8 79 f4 ff ff       	call   2e <dump_for>
  for (int i = 0; i < forks; ++i) {
     bb5:	83 c7 01             	add    $0x1,%edi
     bb8:	83 c4 10             	add    $0x10,%esp
     bbb:	39 f7                	cmp    %esi,%edi
     bbd:	7d 5d                	jge    c1c <test_copy_on_write_main_child+0x304>
    pipe(child_fds);
     bbf:	83 ec 0c             	sub    $0xc,%esp
     bc2:	8d 45 a0             	lea    -0x60(%ebp),%eax
     bc5:	50                   	push   %eax
     bc6:	e8 86 10 00 00       	call   1c51 <pipe>
    children[i] = fork();
     bcb:	e8 69 10 00 00       	call   1c39 <fork>
     bd0:	89 c3                	mov    %eax,%ebx
     bd2:	89 44 bd a8          	mov    %eax,-0x58(%ebp,%edi,4)
    if (children[i] == -1) {
     bd6:	83 c4 10             	add    $0x10,%esp
     bd9:	83 f8 ff             	cmp    $0xffffffff,%eax
     bdc:	0f 84 c5 fd ff ff    	je     9a7 <test_copy_on_write_main_child+0x8f>
    } else if (children[i] == 0) {
     be2:	85 c0                	test   %eax,%eax
     be4:	0f 84 e2 fd ff ff    	je     9cc <test_copy_on_write_main_child+0xb4>
      char buffer[1] = {'X'};
     bea:	c6 45 9f 58          	movb   $0x58,-0x61(%ebp)
      read(child_fds[0], buffer, 1);
     bee:	83 ec 04             	sub    $0x4,%esp
     bf1:	6a 01                	push   $0x1
     bf3:	8d 45 9f             	lea    -0x61(%ebp),%eax
     bf6:	50                   	push   %eax
     bf7:	ff 75 a0             	pushl  -0x60(%ebp)
     bfa:	e8 5a 10 00 00       	call   1c59 <read>
      if (buffer[0] != '+') {
     bff:	83 c4 10             	add    $0x10,%esp
     c02:	80 7d 9f 2b          	cmpb   $0x2b,-0x61(%ebp)
     c06:	0f 84 72 ff ff ff    	je     b7e <test_copy_on_write_main_child+0x266>
        failed_code = 'c';
     c0c:	c6 45 90 63          	movb   $0x63,-0x70(%ebp)
        failed = 1;
     c10:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     c17:	e9 62 ff ff ff       	jmp    b7e <test_copy_on_write_main_child+0x266>
     c1c:	8b 75 88             	mov    -0x78(%ebp),%esi
    }
  }
  for (int i = 0; i < forks; ++i) {
     c1f:	bb 00 00 00 00       	mov    $0x0,%ebx
     c24:	8b 7d 14             	mov    0x14(%ebp),%edi
     c27:	eb 17                	jmp    c40 <test_copy_on_write_main_child+0x328>
    kill(children[i]);
     c29:	83 ec 0c             	sub    $0xc,%esp
     c2c:	ff 74 9d a8          	pushl  -0x58(%ebp,%ebx,4)
     c30:	e8 3c 10 00 00       	call   1c71 <kill>
    wait();
     c35:	e8 0f 10 00 00       	call   1c49 <wait>
  for (int i = 0; i < forks; ++i) {
     c3a:	83 c3 01             	add    $0x1,%ebx
     c3d:	83 c4 10             	add    $0x10,%esp
     c40:	39 fb                	cmp    %edi,%ebx
     c42:	7c e5                	jl     c29 <test_copy_on_write_main_child+0x311>
     c44:	8b 45 8c             	mov    -0x74(%ebp),%eax
     c47:	eb 03                	jmp    c4c <test_copy_on_write_main_child+0x334>
  }
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     c49:	83 c6 01             	add    $0x1,%esi
     c4c:	39 c6                	cmp    %eax,%esi
     c4e:	73 12                	jae    c62 <test_copy_on_write_main_child+0x34a>
    if (*p != 'A') {
     c50:	80 3e 41             	cmpb   $0x41,(%esi)
     c53:	74 f4                	je     c49 <test_copy_on_write_main_child+0x331>
      failed = 1;
      failed_code = 'p';
     c55:	c6 45 90 70          	movb   $0x70,-0x70(%ebp)
      failed = 1;
     c59:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     c60:	eb e7                	jmp    c49 <test_copy_on_write_main_child+0x331>
    }
  }
  if (failed) {
     c62:	83 7d 94 00          	cmpl   $0x0,-0x6c(%ebp)
     c66:	75 1d                	jne    c85 <test_copy_on_write_main_child+0x36d>
    char buffer[2] = {'N', ' '};
    buffer[1] = failed_code;
    write(result_fd, buffer, 2);
  } else {
    write(result_fd, "YY", 2);
     c68:	83 ec 04             	sub    $0x4,%esp
     c6b:	6a 02                	push   $0x2
     c6d:	68 ea 2d 00 00       	push   $0x2dea
     c72:	ff 75 08             	pushl  0x8(%ebp)
     c75:	e8 e7 0f 00 00       	call   1c61 <write>
     c7a:	83 c4 10             	add    $0x10,%esp
  }
}
     c7d:	8d 65 f4             	lea    -0xc(%ebp),%esp
     c80:	5b                   	pop    %ebx
     c81:	5e                   	pop    %esi
     c82:	5f                   	pop    %edi
     c83:	5d                   	pop    %ebp
     c84:	c3                   	ret    
    char buffer[2] = {'N', ' '};
     c85:	c6 45 a0 4e          	movb   $0x4e,-0x60(%ebp)
    buffer[1] = failed_code;
     c89:	0f b6 45 90          	movzbl -0x70(%ebp),%eax
     c8d:	88 45 a1             	mov    %al,-0x5f(%ebp)
    write(result_fd, buffer, 2);
     c90:	83 ec 04             	sub    $0x4,%esp
     c93:	6a 02                	push   $0x2
     c95:	8d 45 a0             	lea    -0x60(%ebp),%eax
     c98:	50                   	push   %eax
     c99:	ff 75 08             	pushl  0x8(%ebp)
     c9c:	e8 c0 0f 00 00       	call   1c61 <write>
     ca1:	83 c4 10             	add    $0x10,%esp
     ca4:	eb d7                	jmp    c7d <test_copy_on_write_main_child+0x365>

00000ca6 <test_copy_on_write_main_child_alt>:

void test_copy_on_write_main_child_alt(int result_fd, int size, const char *describe_size, int forks, int early_term) {
     ca6:	55                   	push   %ebp
     ca7:	89 e5                	mov    %esp,%ebp
     ca9:	57                   	push   %edi
     caa:	56                   	push   %esi
     cab:	53                   	push   %ebx
     cac:	81 ec f8 00 00 00    	sub    $0xf8,%esp
  char *old_end_of_heap = sbrk(size);
     cb2:	ff 75 0c             	pushl  0xc(%ebp)
     cb5:	e8 0f 10 00 00       	call   1cc9 <sbrk>
     cba:	89 c6                	mov    %eax,%esi
  char *new_end_of_heap = sbrk(0);
     cbc:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     cc3:	e8 01 10 00 00       	call   1cc9 <sbrk>
     cc8:	89 c2                	mov    %eax,%edx
     cca:	89 85 0c ff ff ff    	mov    %eax,-0xf4(%ebp)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     cd0:	83 c4 10             	add    $0x10,%esp
     cd3:	89 f0                	mov    %esi,%eax
     cd5:	eb 06                	jmp    cdd <test_copy_on_write_main_child_alt+0x37>
      *p = 'A';
     cd7:	c6 00 41             	movb   $0x41,(%eax)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     cda:	83 c0 01             	add    $0x1,%eax
     cdd:	39 d0                	cmp    %edx,%eax
     cdf:	72 f6                	jb     cd7 <test_copy_on_write_main_child_alt+0x31>
  }
  int children[MAX_CHILDREN] = {0};
     ce1:	8d 7d a8             	lea    -0x58(%ebp),%edi
     ce4:	b9 10 00 00 00       	mov    $0x10,%ecx
     ce9:	b8 00 00 00 00       	mov    $0x0,%eax
     cee:	f3 ab                	rep stos %eax,%es:(%edi)
  int child_fds[MAX_CHILDREN][2];
  if (forks > MAX_CHILDREN) {
     cf0:	83 7d 14 10          	cmpl   $0x10,0x14(%ebp)
     cf4:	7f 43                	jg     d39 <test_copy_on_write_main_child_alt+0x93>
      if (old_end_of_heap[place_three] != 'A' || 
          old_end_of_heap[place_four] != 'A' ||
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
          found_wrong_memory = 1;
      }
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     cf6:	bb 00 00 00 00       	mov    $0x0,%ebx
     cfb:	8b 7d 14             	mov    0x14(%ebp),%edi
  for (int i = 0; i < forks; ++i) {
     cfe:	39 fb                	cmp    %edi,%ebx
     d00:	0f 8d e6 01 00 00    	jge    eec <test_copy_on_write_main_child_alt+0x246>
    sleep(1);
     d06:	83 ec 0c             	sub    $0xc,%esp
     d09:	6a 01                	push   $0x1
     d0b:	e8 c1 0f 00 00       	call   1cd1 <sleep>
    pipe(child_fds[i]);
     d10:	8d 84 dd 28 ff ff ff 	lea    -0xd8(%ebp,%ebx,8),%eax
     d17:	89 04 24             	mov    %eax,(%esp)
     d1a:	e8 32 0f 00 00       	call   1c51 <pipe>
    children[i] = fork();
     d1f:	e8 15 0f 00 00       	call   1c39 <fork>
     d24:	89 44 9d a8          	mov    %eax,-0x58(%ebp,%ebx,4)
    if (children[i] == -1) {
     d28:	83 c4 10             	add    $0x10,%esp
     d2b:	83 f8 ff             	cmp    $0xffffffff,%eax
     d2e:	74 1d                	je     d4d <test_copy_on_write_main_child_alt+0xa7>
    } else if (children[i] == 0) {
     d30:	85 c0                	test   %eax,%eax
     d32:	74 54                	je     d88 <test_copy_on_write_main_child_alt+0xe2>
  for (int i = 0; i < forks; ++i) {
     d34:	83 c3 01             	add    $0x1,%ebx
     d37:	eb c5                	jmp    cfe <test_copy_on_write_main_child_alt+0x58>
    printf(2, "unsupported number of children in test_copy_on_write\n");
     d39:	83 ec 08             	sub    $0x8,%esp
     d3c:	68 44 25 00 00       	push   $0x2544
     d41:	6a 02                	push   $0x2
     d43:	e8 6b 10 00 00       	call   1db3 <printf>
     d48:	83 c4 10             	add    $0x10,%esp
     d4b:	eb a9                	jmp    cf6 <test_copy_on_write_main_child_alt+0x50>
      printf(2, "fork failed\n");
     d4d:	83 ec 08             	sub    $0x8,%esp
     d50:	68 b0 2c 00 00       	push   $0x2cb0
     d55:	6a 02                	push   $0x2
     d57:	e8 57 10 00 00       	call   1db3 <printf>
      break;
     d5c:	83 c4 10             	add    $0x10,%esp
      failed_code = 'f';
     d5f:	c6 85 10 ff ff ff 66 	movb   $0x66,-0xf0(%ebp)
      failed = 1;
     d66:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     d6d:	00 00 00 
      break;
     d70:	e9 88 01 00 00       	jmp    efd <test_copy_on_write_main_child_alt+0x257>
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     d75:	83 c0 01             	add    $0x1,%eax
     d78:	39 d0                	cmp    %edx,%eax
     d7a:	73 18                	jae    d94 <test_copy_on_write_main_child_alt+0xee>
        if (*p != 'A') {
     d7c:	80 38 41             	cmpb   $0x41,(%eax)
     d7f:	74 f4                	je     d75 <test_copy_on_write_main_child_alt+0xcf>
          found_wrong_memory = 1;
     d81:	bf 01 00 00 00       	mov    $0x1,%edi
     d86:	eb ed                	jmp    d75 <test_copy_on_write_main_child_alt+0xcf>
     d88:	89 c7                	mov    %eax,%edi
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     d8a:	89 f0                	mov    %esi,%eax
     d8c:	8b 95 0c ff ff ff    	mov    -0xf4(%ebp),%edx
     d92:	eb e4                	jmp    d78 <test_copy_on_write_main_child_alt+0xd2>
      int place_one = size / 2;
     d94:	8b 45 0c             	mov    0xc(%ebp),%eax
     d97:	c1 e8 1f             	shr    $0x1f,%eax
     d9a:	03 45 0c             	add    0xc(%ebp),%eax
     d9d:	d1 f8                	sar    %eax
      old_end_of_heap[place_one] = 'B' + i;
     d9f:	89 da                	mov    %ebx,%edx
     da1:	01 f0                	add    %esi,%eax
     da3:	89 c1                	mov    %eax,%ecx
     da5:	89 85 0c ff ff ff    	mov    %eax,-0xf4(%ebp)
     dab:	8d 43 42             	lea    0x42(%ebx),%eax
     dae:	88 01                	mov    %al,(%ecx)
      int place_two = 4096 * i;
     db0:	89 d8                	mov    %ebx,%eax
     db2:	c1 e0 0c             	shl    $0xc,%eax
      if (place_two >= size) {
     db5:	39 45 0c             	cmp    %eax,0xc(%ebp)
     db8:	7f 06                	jg     dc0 <test_copy_on_write_main_child_alt+0x11a>
          place_two = size - 1;
     dba:	8b 45 0c             	mov    0xc(%ebp),%eax
     dbd:	83 e8 01             	sub    $0x1,%eax
      old_end_of_heap[place_two] = 'C' + i;
     dc0:	01 f0                	add    %esi,%eax
     dc2:	89 85 08 ff ff ff    	mov    %eax,-0xf8(%ebp)
     dc8:	83 c2 43             	add    $0x43,%edx
     dcb:	88 10                	mov    %dl,(%eax)
      int place_three = 4096 * (i - 1);
     dcd:	8d 43 ff             	lea    -0x1(%ebx),%eax
     dd0:	c1 e0 0c             	shl    $0xc,%eax
     dd3:	89 c1                	mov    %eax,%ecx
      if (place_three >= size || place_three < 0) {
     dd5:	39 45 0c             	cmp    %eax,0xc(%ebp)
     dd8:	0f 9e c2             	setle  %dl
     ddb:	c1 e8 1f             	shr    $0x1f,%eax
     dde:	08 c2                	or     %al,%dl
     de0:	74 06                	je     de8 <test_copy_on_write_main_child_alt+0x142>
          place_three = size - 2;
     de2:	8b 45 0c             	mov    0xc(%ebp),%eax
     de5:	8d 48 fe             	lea    -0x2(%eax),%ecx
      int place_four = 4096 * (i + 1);
     de8:	8d 43 01             	lea    0x1(%ebx),%eax
     deb:	c1 e0 0c             	shl    $0xc,%eax
     dee:	89 85 10 ff ff ff    	mov    %eax,-0xf0(%ebp)
      if (place_four >= size || place_four < 0) {
     df4:	39 45 0c             	cmp    %eax,0xc(%ebp)
     df7:	0f 9e c2             	setle  %dl
     dfa:	c1 e8 1f             	shr    $0x1f,%eax
     dfd:	08 c2                	or     %al,%dl
     dff:	74 0c                	je     e0d <test_copy_on_write_main_child_alt+0x167>
          place_four = size - 3;
     e01:	8b 45 0c             	mov    0xc(%ebp),%eax
     e04:	83 e8 03             	sub    $0x3,%eax
     e07:	89 85 10 ff ff ff    	mov    %eax,-0xf0(%ebp)
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     e0d:	8d 04 0e             	lea    (%esi,%ecx,1),%eax
     e10:	89 85 14 ff ff ff    	mov    %eax,-0xec(%ebp)
     e16:	80 38 41             	cmpb   $0x41,(%eax)
     e19:	74 46                	je     e61 <test_copy_on_write_main_child_alt+0x1bb>
          found_wrong_memory = 1;
     e1b:	bf 01 00 00 00       	mov    $0x1,%edi
      sleep(5);
     e20:	83 ec 0c             	sub    $0xc,%esp
     e23:	6a 05                	push   $0x5
     e25:	e8 a7 0e 00 00       	call   1cd1 <sleep>
      if (old_end_of_heap[place_three] != 'A' || 
     e2a:	83 c4 10             	add    $0x10,%esp
     e2d:	8b 85 14 ff ff ff    	mov    -0xec(%ebp),%eax
     e33:	80 38 41             	cmpb   $0x41,(%eax)
     e36:	74 53                	je     e8b <test_copy_on_write_main_child_alt+0x1e5>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     e38:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     e3d:	83 ec 04             	sub    $0x4,%esp
     e40:	6a 01                	push   $0x1
     e42:	50                   	push   %eax
     e43:	ff b4 dd 2c ff ff ff 	pushl  -0xd4(%ebp,%ebx,8)
     e4a:	e8 12 0e 00 00       	call   1c61 <write>
      if (early_term) {
     e4f:	83 c4 10             	add    $0x10,%esp
     e52:	83 7d 18 00          	cmpl   $0x0,0x18(%ebp)
     e56:	0f 84 8b 00 00 00    	je     ee7 <test_copy_on_write_main_child_alt+0x241>
          exit();
     e5c:	e8 e0 0d 00 00       	call   1c41 <exit>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     e61:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     e67:	0f be 10             	movsbl (%eax),%edx
     e6a:	8d 43 42             	lea    0x42(%ebx),%eax
     e6d:	39 c2                	cmp    %eax,%edx
     e6f:	74 07                	je     e78 <test_copy_on_write_main_child_alt+0x1d2>
          found_wrong_memory = 1;
     e71:	bf 01 00 00 00       	mov    $0x1,%edi
     e76:	eb a8                	jmp    e20 <test_copy_on_write_main_child_alt+0x17a>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     e78:	8b 85 10 ff ff ff    	mov    -0xf0(%ebp),%eax
     e7e:	80 3c 06 41          	cmpb   $0x41,(%esi,%eax,1)
     e82:	74 9c                	je     e20 <test_copy_on_write_main_child_alt+0x17a>
          found_wrong_memory = 1;
     e84:	bf 01 00 00 00       	mov    $0x1,%edi
     e89:	eb 95                	jmp    e20 <test_copy_on_write_main_child_alt+0x17a>
      if (old_end_of_heap[place_three] != 'A' || 
     e8b:	8b 85 10 ff ff ff    	mov    -0xf0(%ebp),%eax
     e91:	80 3c 06 41          	cmpb   $0x41,(%esi,%eax,1)
     e95:	74 07                	je     e9e <test_copy_on_write_main_child_alt+0x1f8>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     e97:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     e9c:	eb 9f                	jmp    e3d <test_copy_on_write_main_child_alt+0x197>
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
     e9e:	8b 85 08 ff ff ff    	mov    -0xf8(%ebp),%eax
     ea4:	0f be 10             	movsbl (%eax),%edx
     ea7:	8d 43 43             	lea    0x43(%ebx),%eax
          old_end_of_heap[place_four] != 'A' ||
     eaa:	39 c2                	cmp    %eax,%edx
     eac:	74 07                	je     eb5 <test_copy_on_write_main_child_alt+0x20f>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     eae:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     eb3:	eb 88                	jmp    e3d <test_copy_on_write_main_child_alt+0x197>
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
     eb5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     ebb:	0f be 10             	movsbl (%eax),%edx
     ebe:	8d 43 42             	lea    0x42(%ebx),%eax
     ec1:	39 c2                	cmp    %eax,%edx
     ec3:	74 0a                	je     ecf <test_copy_on_write_main_child_alt+0x229>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     ec5:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     eca:	e9 6e ff ff ff       	jmp    e3d <test_copy_on_write_main_child_alt+0x197>
     ecf:	85 ff                	test   %edi,%edi
     ed1:	74 0a                	je     edd <test_copy_on_write_main_child_alt+0x237>
     ed3:	b8 9e 2d 00 00       	mov    $0x2d9e,%eax
     ed8:	e9 60 ff ff ff       	jmp    e3d <test_copy_on_write_main_child_alt+0x197>
     edd:	b8 a0 2d 00 00       	mov    $0x2da0,%eax
     ee2:	e9 56 ff ff ff       	jmp    e3d <test_copy_on_write_main_child_alt+0x197>
      } else {
          wait_forever();
     ee7:	e8 14 fa ff ff       	call   900 <wait_forever>
  char failed_code = ' ';
     eec:	c6 85 10 ff ff ff 20 	movb   $0x20,-0xf0(%ebp)
  int failed = 0;
     ef3:	c7 85 14 ff ff ff 00 	movl   $0x0,-0xec(%ebp)
     efa:	00 00 00 
      failed = 1;
     efd:	bb 00 00 00 00       	mov    $0x0,%ebx
     f02:	89 b5 08 ff ff ff    	mov    %esi,-0xf8(%ebp)
     f08:	8b 75 14             	mov    0x14(%ebp),%esi
     f0b:	eb 43                	jmp    f50 <test_copy_on_write_main_child_alt+0x2aa>
    if (children[i] != -1) {
      char buffer[1] = {'X'};
      read(child_fds[i][0], buffer, 1);
      if (buffer[0] == 'X') {
        failed = 1;
        failed_code = 'P';
     f0d:	c6 85 10 ff ff ff 50 	movb   $0x50,-0xf0(%ebp)
        failed = 1;
     f14:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     f1b:	00 00 00 
      } else if (buffer[0] != '+') {
        failed = 1;
        failed_code = 'c';
      }
      close(child_fds[i][0]); close(child_fds[i][1]);
     f1e:	83 ec 0c             	sub    $0xc,%esp
     f21:	ff b4 dd 28 ff ff ff 	pushl  -0xd8(%ebp,%ebx,8)
     f28:	e8 3c 0d 00 00       	call   1c69 <close>
     f2d:	83 c4 04             	add    $0x4,%esp
     f30:	ff b4 dd 2c ff ff ff 	pushl  -0xd4(%ebp,%ebx,8)
     f37:	e8 2d 0d 00 00       	call   1c69 <close>
      dump_for("copy-write-child", children[i]);
     f3c:	83 c4 08             	add    $0x8,%esp
     f3f:	57                   	push   %edi
     f40:	68 ed 2d 00 00       	push   $0x2ded
     f45:	e8 e4 f0 ff ff       	call   2e <dump_for>
     f4a:	83 c4 10             	add    $0x10,%esp
  for (int i = 0; i < forks; ++i) {
     f4d:	83 c3 01             	add    $0x1,%ebx
     f50:	39 f3                	cmp    %esi,%ebx
     f52:	7d 50                	jge    fa4 <test_copy_on_write_main_child_alt+0x2fe>
    if (children[i] != -1) {
     f54:	8b 7c 9d a8          	mov    -0x58(%ebp,%ebx,4),%edi
     f58:	83 ff ff             	cmp    $0xffffffff,%edi
     f5b:	74 f0                	je     f4d <test_copy_on_write_main_child_alt+0x2a7>
      char buffer[1] = {'X'};
     f5d:	c6 85 26 ff ff ff 58 	movb   $0x58,-0xda(%ebp)
      read(child_fds[i][0], buffer, 1);
     f64:	83 ec 04             	sub    $0x4,%esp
     f67:	6a 01                	push   $0x1
     f69:	8d 85 26 ff ff ff    	lea    -0xda(%ebp),%eax
     f6f:	50                   	push   %eax
     f70:	ff b4 dd 28 ff ff ff 	pushl  -0xd8(%ebp,%ebx,8)
     f77:	e8 dd 0c 00 00       	call   1c59 <read>
      if (buffer[0] == 'X') {
     f7c:	0f b6 85 26 ff ff ff 	movzbl -0xda(%ebp),%eax
     f83:	83 c4 10             	add    $0x10,%esp
     f86:	3c 58                	cmp    $0x58,%al
     f88:	74 83                	je     f0d <test_copy_on_write_main_child_alt+0x267>
      } else if (buffer[0] != '+') {
     f8a:	3c 2b                	cmp    $0x2b,%al
     f8c:	74 90                	je     f1e <test_copy_on_write_main_child_alt+0x278>
        failed_code = 'c';
     f8e:	c6 85 10 ff ff ff 63 	movb   $0x63,-0xf0(%ebp)
        failed = 1;
     f95:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     f9c:	00 00 00 
     f9f:	e9 7a ff ff ff       	jmp    f1e <test_copy_on_write_main_child_alt+0x278>
     fa4:	8b b5 08 ff ff ff    	mov    -0xf8(%ebp),%esi
    }
  }
  dump_for("copy-write-parent", getpid());
     faa:	e8 12 0d 00 00       	call   1cc1 <getpid>
     faf:	83 ec 08             	sub    $0x8,%esp
     fb2:	50                   	push   %eax
     fb3:	68 fe 2d 00 00       	push   $0x2dfe
     fb8:	e8 71 f0 ff ff       	call   2e <dump_for>
  for (int i = 0; i < forks; ++i) {
     fbd:	83 c4 10             	add    $0x10,%esp
     fc0:	bb 00 00 00 00       	mov    $0x0,%ebx
     fc5:	8b 7d 14             	mov    0x14(%ebp),%edi
     fc8:	eb 17                	jmp    fe1 <test_copy_on_write_main_child_alt+0x33b>
    kill(children[i]);
     fca:	83 ec 0c             	sub    $0xc,%esp
     fcd:	ff 74 9d a8          	pushl  -0x58(%ebp,%ebx,4)
     fd1:	e8 9b 0c 00 00       	call   1c71 <kill>
    wait();
     fd6:	e8 6e 0c 00 00       	call   1c49 <wait>
  for (int i = 0; i < forks; ++i) {
     fdb:	83 c3 01             	add    $0x1,%ebx
     fde:	83 c4 10             	add    $0x10,%esp
     fe1:	39 fb                	cmp    %edi,%ebx
     fe3:	7c e5                	jl     fca <test_copy_on_write_main_child_alt+0x324>
     fe5:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     feb:	eb 03                	jmp    ff0 <test_copy_on_write_main_child_alt+0x34a>
  }
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     fed:	83 c6 01             	add    $0x1,%esi
     ff0:	39 c6                	cmp    %eax,%esi
     ff2:	73 18                	jae    100c <test_copy_on_write_main_child_alt+0x366>
    if (*p != 'A') {
     ff4:	80 3e 41             	cmpb   $0x41,(%esi)
     ff7:	74 f4                	je     fed <test_copy_on_write_main_child_alt+0x347>
      failed = 1;
      failed_code = 'p';
     ff9:	c6 85 10 ff ff ff 70 	movb   $0x70,-0xf0(%ebp)
      failed = 1;
    1000:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
    1007:	00 00 00 
    100a:	eb e1                	jmp    fed <test_copy_on_write_main_child_alt+0x347>
    }
  }
  if (failed) {
    100c:	83 bd 14 ff ff ff 00 	cmpl   $0x0,-0xec(%ebp)
    1013:	75 1d                	jne    1032 <test_copy_on_write_main_child_alt+0x38c>
    char buffer[2] = {'N', ' '};
    buffer[1] = failed_code;
    write(result_fd, buffer, 2);
  } else {
    write(result_fd, "YY", 2);
    1015:	83 ec 04             	sub    $0x4,%esp
    1018:	6a 02                	push   $0x2
    101a:	68 ea 2d 00 00       	push   $0x2dea
    101f:	ff 75 08             	pushl  0x8(%ebp)
    1022:	e8 3a 0c 00 00       	call   1c61 <write>
    1027:	83 c4 10             	add    $0x10,%esp
  }
}
    102a:	8d 65 f4             	lea    -0xc(%ebp),%esp
    102d:	5b                   	pop    %ebx
    102e:	5e                   	pop    %esi
    102f:	5f                   	pop    %edi
    1030:	5d                   	pop    %ebp
    1031:	c3                   	ret    
    char buffer[2] = {'N', ' '};
    1032:	c6 85 26 ff ff ff 4e 	movb   $0x4e,-0xda(%ebp)
    buffer[1] = failed_code;
    1039:	0f b6 85 10 ff ff ff 	movzbl -0xf0(%ebp),%eax
    1040:	88 85 27 ff ff ff    	mov    %al,-0xd9(%ebp)
    write(result_fd, buffer, 2);
    1046:	83 ec 04             	sub    $0x4,%esp
    1049:	6a 02                	push   $0x2
    104b:	8d 85 26 ff ff ff    	lea    -0xda(%ebp),%eax
    1051:	50                   	push   %eax
    1052:	ff 75 08             	pushl  0x8(%ebp)
    1055:	e8 07 0c 00 00       	call   1c61 <write>
    105a:	83 c4 10             	add    $0x10,%esp
    105d:	eb cb                	jmp    102a <test_copy_on_write_main_child_alt+0x384>

0000105f <_show_cow_test_error>:

void _show_cow_test_error(char *code) {
    105f:	55                   	push   %ebp
    1060:	89 e5                	mov    %esp,%ebp
    1062:	83 ec 08             	sub    $0x8,%esp
    1065:	8b 55 08             	mov    0x8(%ebp),%edx
  if (code[0] == 'X') {
    1068:	0f b6 02             	movzbl (%edx),%eax
    106b:	3c 58                	cmp    $0x58,%al
    106d:	74 06                	je     1075 <_show_cow_test_error+0x16>
    printf(1, FAIL_MSG "copy on write test failed --- crash?\n");
  } else if (code[0] == 'N') {
    106f:	3c 4e                	cmp    $0x4e,%al
    1071:	74 16                	je     1089 <_show_cow_test_error+0x2a>
    default:
      printf(1, FAIL_MSG"copy on write test failed --- unknown reason\n");
      break;
    }
  }
}
    1073:	c9                   	leave  
    1074:	c3                   	ret    
    printf(1, FAIL_MSG "copy on write test failed --- crash?\n");
    1075:	83 ec 08             	sub    $0x8,%esp
    1078:	68 6c 26 00 00       	push   $0x266c
    107d:	6a 01                	push   $0x1
    107f:	e8 2f 0d 00 00       	call   1db3 <printf>
    1084:	83 c4 10             	add    $0x10,%esp
    1087:	eb ea                	jmp    1073 <_show_cow_test_error+0x14>
    switch (code[1]) {
    1089:	0f b6 42 01          	movzbl 0x1(%edx),%eax
    108d:	3c 63                	cmp    $0x63,%al
    108f:	74 4c                	je     10dd <_show_cow_test_error+0x7e>
    1091:	3c 63                	cmp    $0x63,%al
    1093:	7e 1c                	jle    10b1 <_show_cow_test_error+0x52>
    1095:	3c 66                	cmp    $0x66,%al
    1097:	74 30                	je     10c9 <_show_cow_test_error+0x6a>
    1099:	3c 70                	cmp    $0x70,%al
    109b:	75 54                	jne    10f1 <_show_cow_test_error+0x92>
      printf(1, FAIL_MSG "copy on write test failed --- wrong value for memory in parent\n");
    109d:	83 ec 08             	sub    $0x8,%esp
    10a0:	68 d8 26 00 00       	push   $0x26d8
    10a5:	6a 01                	push   $0x1
    10a7:	e8 07 0d 00 00       	call   1db3 <printf>
      break;
    10ac:	83 c4 10             	add    $0x10,%esp
    10af:	eb c2                	jmp    1073 <_show_cow_test_error+0x14>
    switch (code[1]) {
    10b1:	3c 50                	cmp    $0x50,%al
    10b3:	75 3c                	jne    10f1 <_show_cow_test_error+0x92>
      printf(1, FAIL_MSG "copy on write test failed --- pipe read problem\n");
    10b5:	83 ec 08             	sub    $0x8,%esp
    10b8:	68 28 27 00 00       	push   $0x2728
    10bd:	6a 01                	push   $0x1
    10bf:	e8 ef 0c 00 00       	call   1db3 <printf>
      break;
    10c4:	83 c4 10             	add    $0x10,%esp
    10c7:	eb aa                	jmp    1073 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG "copy on write test failed --- fork failed\n");
    10c9:	83 ec 08             	sub    $0x8,%esp
    10cc:	68 a0 26 00 00       	push   $0x26a0
    10d1:	6a 01                	push   $0x1
    10d3:	e8 db 0c 00 00       	call   1db3 <printf>
      break;
    10d8:	83 c4 10             	add    $0x10,%esp
    10db:	eb 96                	jmp    1073 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG "copy on write test failed --- wrong value for memory in child\n");
    10dd:	83 ec 08             	sub    $0x8,%esp
    10e0:	68 68 27 00 00       	push   $0x2768
    10e5:	6a 01                	push   $0x1
    10e7:	e8 c7 0c 00 00       	call   1db3 <printf>
      break;
    10ec:	83 c4 10             	add    $0x10,%esp
    10ef:	eb 82                	jmp    1073 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG"copy on write test failed --- unknown reason\n");
    10f1:	83 ec 08             	sub    $0x8,%esp
    10f4:	68 b4 27 00 00       	push   $0x27b4
    10f9:	6a 01                	push   $0x1
    10fb:	e8 b3 0c 00 00       	call   1db3 <printf>
      break;
    1100:	83 c4 10             	add    $0x10,%esp
}
    1103:	e9 6b ff ff ff       	jmp    1073 <_show_cow_test_error+0x14>

00001108 <test_copy_on_write_less_forks>:

int test_copy_on_write_less_forks(int size, const char *describe_size, int forks) {
    1108:	55                   	push   %ebp
    1109:	89 e5                	mov    %esp,%ebp
    110b:	56                   	push   %esi
    110c:	53                   	push   %ebx
    110d:	83 ec 1c             	sub    $0x1c,%esp
    1110:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    1113:	8b 75 10             	mov    0x10(%ebp),%esi
  int fds[2];
  pipe(fds);
    1116:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1119:	50                   	push   %eax
    111a:	e8 32 0b 00 00       	call   1c51 <pipe>
  test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    111f:	56                   	push   %esi
    1120:	53                   	push   %ebx
    1121:	ff 75 08             	pushl  0x8(%ebp)
    1124:	ff 75 f4             	pushl  -0xc(%ebp)
    1127:	e8 ec f7 ff ff       	call   918 <test_copy_on_write_main_child>
  char text[2] = {'X', 'X'};
    112c:	c6 45 ee 58          	movb   $0x58,-0x12(%ebp)
    1130:	c6 45 ef 58          	movb   $0x58,-0x11(%ebp)
  read(fds[0], text, 2);
    1134:	83 c4 1c             	add    $0x1c,%esp
    1137:	6a 02                	push   $0x2
    1139:	8d 45 ee             	lea    -0x12(%ebp),%eax
    113c:	50                   	push   %eax
    113d:	ff 75 f0             	pushl  -0x10(%ebp)
    1140:	e8 14 0b 00 00       	call   1c59 <read>
  close(fds[0]); close(fds[1]);
    1145:	83 c4 04             	add    $0x4,%esp
    1148:	ff 75 f0             	pushl  -0x10(%ebp)
    114b:	e8 19 0b 00 00       	call   1c69 <close>
    1150:	83 c4 04             	add    $0x4,%esp
    1153:	ff 75 f4             	pushl  -0xc(%ebp)
    1156:	e8 0e 0b 00 00       	call   1c69 <close>
  if (text[0] != 'Y') {
    115b:	83 c4 10             	add    $0x10,%esp
    115e:	80 7d ee 59          	cmpb   $0x59,-0x12(%ebp)
    1162:	74 1b                	je     117f <test_copy_on_write_less_forks+0x77>
    _show_cow_test_error(text);
    1164:	83 ec 0c             	sub    $0xc,%esp
    1167:	8d 45 ee             	lea    -0x12(%ebp),%eax
    116a:	50                   	push   %eax
    116b:	e8 ef fe ff ff       	call   105f <_show_cow_test_error>
    return 0;
    1170:	83 c4 10             	add    $0x10,%esp
    1173:	b8 00 00 00 00       	mov    $0x0,%eax
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
           "fork %d children; read+write small parts in each child\n",
           describe_size, forks);
    return 1;
  }
}
    1178:	8d 65 f8             	lea    -0x8(%ebp),%esp
    117b:	5b                   	pop    %ebx
    117c:	5e                   	pop    %esi
    117d:	5d                   	pop    %ebp
    117e:	c3                   	ret    
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    117f:	56                   	push   %esi
    1180:	53                   	push   %ebx
    1181:	68 f0 27 00 00       	push   $0x27f0
    1186:	6a 01                	push   $0x1
    1188:	e8 26 0c 00 00       	call   1db3 <printf>
    return 1;
    118d:	83 c4 10             	add    $0x10,%esp
    1190:	b8 01 00 00 00       	mov    $0x1,%eax
    1195:	eb e1                	jmp    1178 <test_copy_on_write_less_forks+0x70>

00001197 <test_copy_on_write_less_forks_alt>:

int test_copy_on_write_less_forks_alt(int size, const char *describe_size, int forks, int early_term) {
    1197:	55                   	push   %ebp
    1198:	89 e5                	mov    %esp,%ebp
    119a:	56                   	push   %esi
    119b:	53                   	push   %ebx
    119c:	83 ec 1c             	sub    $0x1c,%esp
    119f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    11a2:	8b 75 10             	mov    0x10(%ebp),%esi
  int fds[2];
  pipe(fds);
    11a5:	8d 45 f0             	lea    -0x10(%ebp),%eax
    11a8:	50                   	push   %eax
    11a9:	e8 a3 0a 00 00       	call   1c51 <pipe>
  test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    11ae:	83 c4 04             	add    $0x4,%esp
    11b1:	ff 75 14             	pushl  0x14(%ebp)
    11b4:	56                   	push   %esi
    11b5:	53                   	push   %ebx
    11b6:	ff 75 08             	pushl  0x8(%ebp)
    11b9:	ff 75 f4             	pushl  -0xc(%ebp)
    11bc:	e8 e5 fa ff ff       	call   ca6 <test_copy_on_write_main_child_alt>
  char text[2] = {'X', 'X'};
    11c1:	c6 45 ee 58          	movb   $0x58,-0x12(%ebp)
    11c5:	c6 45 ef 58          	movb   $0x58,-0x11(%ebp)
  read(fds[0], text, 2);
    11c9:	83 c4 1c             	add    $0x1c,%esp
    11cc:	6a 02                	push   $0x2
    11ce:	8d 45 ee             	lea    -0x12(%ebp),%eax
    11d1:	50                   	push   %eax
    11d2:	ff 75 f0             	pushl  -0x10(%ebp)
    11d5:	e8 7f 0a 00 00       	call   1c59 <read>
  close(fds[0]); close(fds[1]);
    11da:	83 c4 04             	add    $0x4,%esp
    11dd:	ff 75 f0             	pushl  -0x10(%ebp)
    11e0:	e8 84 0a 00 00       	call   1c69 <close>
    11e5:	83 c4 04             	add    $0x4,%esp
    11e8:	ff 75 f4             	pushl  -0xc(%ebp)
    11eb:	e8 79 0a 00 00       	call   1c69 <close>
  if (text[0] != 'Y') {
    11f0:	83 c4 10             	add    $0x10,%esp
    11f3:	80 7d ee 59          	cmpb   $0x59,-0x12(%ebp)
    11f7:	74 1b                	je     1214 <test_copy_on_write_less_forks_alt+0x7d>
    _show_cow_test_error(text);
    11f9:	83 ec 0c             	sub    $0xc,%esp
    11fc:	8d 45 ee             	lea    -0x12(%ebp),%eax
    11ff:	50                   	push   %eax
    1200:	e8 5a fe ff ff       	call   105f <_show_cow_test_error>
    return 0;
    1205:	83 c4 10             	add    $0x10,%esp
    1208:	b8 00 00 00 00       	mov    $0x0,%eax
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
           "fork %d children; read+write small parts in each child\n",
           describe_size, forks);
    return 1;
  }
}
    120d:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1210:	5b                   	pop    %ebx
    1211:	5e                   	pop    %esi
    1212:	5d                   	pop    %ebp
    1213:	c3                   	ret    
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    1214:	56                   	push   %esi
    1215:	53                   	push   %ebx
    1216:	68 f0 27 00 00       	push   $0x27f0
    121b:	6a 01                	push   $0x1
    121d:	e8 91 0b 00 00       	call   1db3 <printf>
    return 1;
    1222:	83 c4 10             	add    $0x10,%esp
    1225:	b8 01 00 00 00       	mov    $0x1,%eax
    122a:	eb e1                	jmp    120d <test_copy_on_write_less_forks_alt+0x76>

0000122c <_test_copy_on_write>:

int _test_copy_on_write(int size,  const char *describe_size, int forks, int use_alt, int early_term, int pre_alloc, const char* describe_prealloc) {
    122c:	55                   	push   %ebp
    122d:	89 e5                	mov    %esp,%ebp
    122f:	83 ec 34             	sub    $0x34,%esp
  int fds[2];
  pipe(fds);
    1232:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1235:	50                   	push   %eax
    1236:	e8 16 0a 00 00       	call   1c51 <pipe>
  int pid = fork();
    123b:	e8 f9 09 00 00       	call   1c39 <fork>
  if (pid == -1) {
    1240:	83 c4 10             	add    $0x10,%esp
    1243:	83 f8 ff             	cmp    $0xffffffff,%eax
    1246:	0f 84 c2 00 00 00    	je     130e <_test_copy_on_write+0xe2>
    printf(1, FAIL_MSG "fork failed");
  } else if (pid == 0) {
    124c:	85 c0                	test   %eax,%eax
    124e:	0f 84 d6 00 00 00    	je     132a <_test_copy_on_write+0xfe>
      test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    } else {
      test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    }
    exit();
  } else if (pid > 0) {
    1254:	85 c0                	test   %eax,%eax
    1256:	0f 8e 6c 01 00 00    	jle    13c8 <_test_copy_on_write+0x19c>
    printf(1, "running copy on write test: ");
    125c:	83 ec 08             	sub    $0x8,%esp
    125f:	68 10 2e 00 00       	push   $0x2e10
    1264:	6a 01                	push   $0x1
    1266:	e8 48 0b 00 00       	call   1db3 <printf>
    if (pre_alloc > 0) {
    126b:	83 c4 10             	add    $0x10,%esp
    126e:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
    1272:	0f 8f 03 01 00 00    	jg     137b <_test_copy_on_write+0x14f>
      printf(1, "allocate but do not use %s; ", describe_prealloc);
    }
    printf(1, "allocate and use %s; fork %d children; read+write small parts in each child",
    1278:	ff 75 10             	pushl  0x10(%ebp)
    127b:	ff 75 0c             	pushl  0xc(%ebp)
    127e:	68 60 28 00 00       	push   $0x2860
    1283:	6a 01                	push   $0x1
    1285:	e8 29 0b 00 00       	call   1db3 <printf>
        describe_size, forks);
    if (use_alt) {
    128a:	83 c4 10             	add    $0x10,%esp
    128d:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
    1291:	0f 85 fe 00 00 00    	jne    1395 <_test_copy_on_write+0x169>
      printf(1, " [and try to keep children running in parallel]");
    }
    printf(1, "\n");
    1297:	83 ec 08             	sub    $0x8,%esp
    129a:	68 f4 2c 00 00       	push   $0x2cf4
    129f:	6a 01                	push   $0x1
    12a1:	e8 0d 0b 00 00       	call   1db3 <printf>
    char text[10] = {'X', 'X'};
    12a6:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
    12ad:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
    12b4:	c6 45 e6 58          	movb   $0x58,-0x1a(%ebp)
    12b8:	c6 45 e7 58          	movb   $0x58,-0x19(%ebp)
    close(fds[1]);
    12bc:	83 c4 04             	add    $0x4,%esp
    12bf:	ff 75 f4             	pushl  -0xc(%ebp)
    12c2:	e8 a2 09 00 00       	call   1c69 <close>
    read(fds[0], text, 10);
    12c7:	83 c4 0c             	add    $0xc,%esp
    12ca:	6a 0a                	push   $0xa
    12cc:	8d 45 e6             	lea    -0x1a(%ebp),%eax
    12cf:	50                   	push   %eax
    12d0:	ff 75 f0             	pushl  -0x10(%ebp)
    12d3:	e8 81 09 00 00       	call   1c59 <read>
    wait();
    12d8:	e8 6c 09 00 00       	call   1c49 <wait>
    close(fds[0]);
    12dd:	83 c4 04             	add    $0x4,%esp
    12e0:	ff 75 f0             	pushl  -0x10(%ebp)
    12e3:	e8 81 09 00 00       	call   1c69 <close>
    if (text[0] != 'Y') {
    12e8:	83 c4 10             	add    $0x10,%esp
    12eb:	80 7d e6 59          	cmpb   $0x59,-0x1a(%ebp)
    12ef:	0f 84 b7 00 00 00    	je     13ac <_test_copy_on_write+0x180>
      _show_cow_test_error(text);
    12f5:	83 ec 0c             	sub    $0xc,%esp
    12f8:	8d 45 e6             	lea    -0x1a(%ebp),%eax
    12fb:	50                   	push   %eax
    12fc:	e8 5e fd ff ff       	call   105f <_show_cow_test_error>
      return 0;
    1301:	83 c4 10             	add    $0x10,%esp
    1304:	b8 00 00 00 00       	mov    $0x0,%eax
    1309:	e9 bf 00 00 00       	jmp    13cd <_test_copy_on_write+0x1a1>
    printf(1, FAIL_MSG "fork failed");
    130e:	83 ec 08             	sub    $0x8,%esp
    1311:	68 88 2c 00 00       	push   $0x2c88
    1316:	6a 01                	push   $0x1
    1318:	e8 96 0a 00 00       	call   1db3 <printf>
    131d:	83 c4 10             	add    $0x10,%esp
      return 1;
    }
  } else if (pid == -1) {
     printf(1, FAIL_MSG "copy on write test failed --- first fork failed\n");
  }
  return 0;
    1320:	b8 00 00 00 00       	mov    $0x0,%eax
    1325:	e9 a3 00 00 00       	jmp    13cd <_test_copy_on_write+0x1a1>
    if (pre_alloc > 0) {
    132a:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
    132e:	7f 25                	jg     1355 <_test_copy_on_write+0x129>
    if (use_alt) {
    1330:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
    1334:	74 2f                	je     1365 <_test_copy_on_write+0x139>
      test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    1336:	83 ec 0c             	sub    $0xc,%esp
    1339:	ff 75 18             	pushl  0x18(%ebp)
    133c:	ff 75 10             	pushl  0x10(%ebp)
    133f:	ff 75 0c             	pushl  0xc(%ebp)
    1342:	ff 75 08             	pushl  0x8(%ebp)
    1345:	ff 75 f4             	pushl  -0xc(%ebp)
    1348:	e8 59 f9 ff ff       	call   ca6 <test_copy_on_write_main_child_alt>
    134d:	83 c4 20             	add    $0x20,%esp
    exit();
    1350:	e8 ec 08 00 00       	call   1c41 <exit>
      sbrk(pre_alloc);
    1355:	83 ec 0c             	sub    $0xc,%esp
    1358:	ff 75 1c             	pushl  0x1c(%ebp)
    135b:	e8 69 09 00 00       	call   1cc9 <sbrk>
    1360:	83 c4 10             	add    $0x10,%esp
    1363:	eb cb                	jmp    1330 <_test_copy_on_write+0x104>
      test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    1365:	ff 75 10             	pushl  0x10(%ebp)
    1368:	ff 75 0c             	pushl  0xc(%ebp)
    136b:	ff 75 08             	pushl  0x8(%ebp)
    136e:	ff 75 f4             	pushl  -0xc(%ebp)
    1371:	e8 a2 f5 ff ff       	call   918 <test_copy_on_write_main_child>
    1376:	83 c4 10             	add    $0x10,%esp
    1379:	eb d5                	jmp    1350 <_test_copy_on_write+0x124>
      printf(1, "allocate but do not use %s; ", describe_prealloc);
    137b:	83 ec 04             	sub    $0x4,%esp
    137e:	ff 75 20             	pushl  0x20(%ebp)
    1381:	68 2d 2e 00 00       	push   $0x2e2d
    1386:	6a 01                	push   $0x1
    1388:	e8 26 0a 00 00       	call   1db3 <printf>
    138d:	83 c4 10             	add    $0x10,%esp
    1390:	e9 e3 fe ff ff       	jmp    1278 <_test_copy_on_write+0x4c>
      printf(1, " [and try to keep children running in parallel]");
    1395:	83 ec 08             	sub    $0x8,%esp
    1398:	68 ac 28 00 00       	push   $0x28ac
    139d:	6a 01                	push   $0x1
    139f:	e8 0f 0a 00 00       	call   1db3 <printf>
    13a4:	83 c4 10             	add    $0x10,%esp
    13a7:	e9 eb fe ff ff       	jmp    1297 <_test_copy_on_write+0x6b>
      printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    13ac:	ff 75 10             	pushl  0x10(%ebp)
    13af:	ff 75 0c             	pushl  0xc(%ebp)
    13b2:	68 f0 27 00 00       	push   $0x27f0
    13b7:	6a 01                	push   $0x1
    13b9:	e8 f5 09 00 00       	call   1db3 <printf>
      return 1;
    13be:	83 c4 10             	add    $0x10,%esp
    13c1:	b8 01 00 00 00       	mov    $0x1,%eax
    13c6:	eb 05                	jmp    13cd <_test_copy_on_write+0x1a1>
  return 0;
    13c8:	b8 00 00 00 00       	mov    $0x0,%eax
}
    13cd:	c9                   	leave  
    13ce:	c3                   	ret    

000013cf <test_copy_on_write>:

int test_copy_on_write(int size, const char *describe_size, int forks) {
    13cf:	55                   	push   %ebp
    13d0:	89 e5                	mov    %esp,%ebp
    13d2:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 0, 0, 0, "");
    13d5:	68 f5 2c 00 00       	push   $0x2cf5
    13da:	6a 00                	push   $0x0
    13dc:	6a 00                	push   $0x0
    13de:	6a 00                	push   $0x0
    13e0:	ff 75 10             	pushl  0x10(%ebp)
    13e3:	ff 75 0c             	pushl  0xc(%ebp)
    13e6:	ff 75 08             	pushl  0x8(%ebp)
    13e9:	e8 3e fe ff ff       	call   122c <_test_copy_on_write>
}
    13ee:	c9                   	leave  
    13ef:	c3                   	ret    

000013f0 <test_copy_on_write_alloc_unused>:

int test_copy_on_write_alloc_unused(int unused_size, const char *describe_unused_size, int size, const char *describe_size, int forks) {
    13f0:	55                   	push   %ebp
    13f1:	89 e5                	mov    %esp,%ebp
    13f3:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 0, 0, unused_size, describe_unused_size);
    13f6:	ff 75 0c             	pushl  0xc(%ebp)
    13f9:	ff 75 08             	pushl  0x8(%ebp)
    13fc:	6a 00                	push   $0x0
    13fe:	6a 00                	push   $0x0
    1400:	ff 75 18             	pushl  0x18(%ebp)
    1403:	ff 75 14             	pushl  0x14(%ebp)
    1406:	ff 75 10             	pushl  0x10(%ebp)
    1409:	e8 1e fe ff ff       	call   122c <_test_copy_on_write>
}
    140e:	c9                   	leave  
    140f:	c3                   	ret    

00001410 <test_copy_on_write_alt>:

int test_copy_on_write_alt(int size, const char *describe_size, int forks) {
    1410:	55                   	push   %ebp
    1411:	89 e5                	mov    %esp,%ebp
    1413:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 1, 0, 0, "");
    1416:	68 f5 2c 00 00       	push   $0x2cf5
    141b:	6a 00                	push   $0x0
    141d:	6a 00                	push   $0x0
    141f:	6a 01                	push   $0x1
    1421:	ff 75 10             	pushl  0x10(%ebp)
    1424:	ff 75 0c             	pushl  0xc(%ebp)
    1427:	ff 75 08             	pushl  0x8(%ebp)
    142a:	e8 fd fd ff ff       	call   122c <_test_copy_on_write>
}
    142f:	c9                   	leave  
    1430:	c3                   	ret    

00001431 <test_read_into_alloc_no_fork>:

int test_read_into_alloc_no_fork(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    1431:	55                   	push   %ebp
    1432:	89 e5                	mov    %esp,%ebp
    1434:	57                   	push   %edi
    1435:	56                   	push   %esi
    1436:	53                   	push   %ebx
    1437:	83 ec 28             	sub    $0x28,%esp
    143a:	8b 5d 10             	mov    0x10(%ebp),%ebx
    printf(1, "testing read(), writing %d bytes to a location %s into a %s allocation\n",
    143d:	ff 75 14             	pushl  0x14(%ebp)
    1440:	ff 75 18             	pushl  0x18(%ebp)
    1443:	53                   	push   %ebx
    1444:	68 dc 28 00 00       	push   $0x28dc
    1449:	6a 01                	push   $0x1
    144b:	e8 63 09 00 00       	call   1db3 <printf>
        read_count, describe_offset, describe_size);
    int fd = open("tempfile", O_WRONLY | O_CREATE);
    1450:	83 c4 18             	add    $0x18,%esp
    1453:	68 01 02 00 00       	push   $0x201
    1458:	68 58 2e 00 00       	push   $0x2e58
    145d:	e8 1f 08 00 00       	call   1c81 <open>
    1462:	89 c6                	mov    %eax,%esi
    static char buffer[128]; // static to avoid running out of stack space
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1464:	83 c4 10             	add    $0x10,%esp
    1467:	b8 00 00 00 00       	mov    $0x0,%eax
    146c:	eb 0a                	jmp    1478 <test_read_into_alloc_no_fork+0x47>
        buffer[i] = 'X';
    146e:	c6 80 40 36 00 00 58 	movb   $0x58,0x3640(%eax)
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1475:	83 c0 01             	add    $0x1,%eax
    1478:	83 f8 7f             	cmp    $0x7f,%eax
    147b:	76 f1                	jbe    146e <test_read_into_alloc_no_fork+0x3d>
    }
    for (int i = 0; i < read_count; i += sizeof buffer) {
    147d:	bf 00 00 00 00       	mov    $0x0,%edi
    1482:	eb 19                	jmp    149d <test_read_into_alloc_no_fork+0x6c>
        write(fd, buffer, sizeof buffer);
    1484:	83 ec 04             	sub    $0x4,%esp
    1487:	68 80 00 00 00       	push   $0x80
    148c:	68 40 36 00 00       	push   $0x3640
    1491:	56                   	push   %esi
    1492:	e8 ca 07 00 00       	call   1c61 <write>
    for (int i = 0; i < read_count; i += sizeof buffer) {
    1497:	83 ef 80             	sub    $0xffffff80,%edi
    149a:	83 c4 10             	add    $0x10,%esp
    149d:	39 df                	cmp    %ebx,%edi
    149f:	7c e3                	jl     1484 <test_read_into_alloc_no_fork+0x53>
    }
    close(fd);
    14a1:	83 ec 0c             	sub    $0xc,%esp
    14a4:	56                   	push   %esi
    14a5:	e8 bf 07 00 00       	call   1c69 <close>
    fd = open("tempfile", O_RDONLY);
    14aa:	83 c4 08             	add    $0x8,%esp
    14ad:	6a 00                	push   $0x0
    14af:	68 58 2e 00 00       	push   $0x2e58
    14b4:	e8 c8 07 00 00       	call   1c81 <open>
    14b9:	89 c7                	mov    %eax,%edi
    if (fd == -1) {
    14bb:	83 c4 10             	add    $0x10,%esp
    14be:	83 f8 ff             	cmp    $0xffffffff,%eax
    14c1:	74 3f                	je     1502 <test_read_into_alloc_no_fork+0xd1>
        printf(2, "error opening tempfile");
    }
    char *heap = sbrk(0);
    14c3:	83 ec 0c             	sub    $0xc,%esp
    14c6:	6a 00                	push   $0x0
    14c8:	e8 fc 07 00 00       	call   1cc9 <sbrk>
    14cd:	89 c6                	mov    %eax,%esi
    sbrk(size);
    14cf:	83 c4 04             	add    $0x4,%esp
    14d2:	ff 75 08             	pushl  0x8(%ebp)
    14d5:	e8 ef 07 00 00       	call   1cc9 <sbrk>
    char *loc = heap + offset;
    14da:	03 75 0c             	add    0xc(%ebp),%esi
    int count = read(fd, loc, read_count);
    14dd:	83 c4 0c             	add    $0xc,%esp
    14e0:	53                   	push   %ebx
    14e1:	56                   	push   %esi
    14e2:	57                   	push   %edi
    14e3:	e8 71 07 00 00       	call   1c59 <read>
    14e8:	89 45 e0             	mov    %eax,-0x20(%ebp)
    int failed_value = 0;
    failed_value = loc[-1] != '\0';
    14eb:	83 c4 10             	add    $0x10,%esp
    14ee:	80 7e ff 00          	cmpb   $0x0,-0x1(%esi)
    14f2:	0f 95 c0             	setne  %al
    14f5:	0f b6 c0             	movzbl %al,%eax
    14f8:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for (int i = 0; i < read_count; ++i) {
    14fb:	b8 00 00 00 00       	mov    $0x0,%eax
    1500:	eb 17                	jmp    1519 <test_read_into_alloc_no_fork+0xe8>
        printf(2, "error opening tempfile");
    1502:	83 ec 08             	sub    $0x8,%esp
    1505:	68 4a 2e 00 00       	push   $0x2e4a
    150a:	6a 02                	push   $0x2
    150c:	e8 a2 08 00 00       	call   1db3 <printf>
    1511:	83 c4 10             	add    $0x10,%esp
    1514:	eb ad                	jmp    14c3 <test_read_into_alloc_no_fork+0x92>
    for (int i = 0; i < read_count; ++i) {
    1516:	83 c0 01             	add    $0x1,%eax
    1519:	39 d8                	cmp    %ebx,%eax
    151b:	7d 0f                	jge    152c <test_read_into_alloc_no_fork+0xfb>
        if (loc[i] != 'X') {
    151d:	80 3c 06 58          	cmpb   $0x58,(%esi,%eax,1)
    1521:	74 f3                	je     1516 <test_read_into_alloc_no_fork+0xe5>
            failed_value = 1;
    1523:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
    152a:	eb ea                	jmp    1516 <test_read_into_alloc_no_fork+0xe5>
        }
    }
    if (loc[read_count] != '\0') {
    152c:	80 3c 1e 00          	cmpb   $0x0,(%esi,%ebx,1)
    1530:	74 07                	je     1539 <test_read_into_alloc_no_fork+0x108>
        failed_value = 1;
    1532:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
    }
    close(fd);
    1539:	83 ec 0c             	sub    $0xc,%esp
    153c:	57                   	push   %edi
    153d:	e8 27 07 00 00       	call   1c69 <close>
    unlink("tempfile");
    1542:	c7 04 24 58 2e 00 00 	movl   $0x2e58,(%esp)
    1549:	e8 43 07 00 00       	call   1c91 <unlink>
    if (count != read_count) {
    154e:	83 c4 10             	add    $0x10,%esp
    1551:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
    1554:	75 1f                	jne    1575 <test_read_into_alloc_no_fork+0x144>
        printf(1, FAIL_MSG "wrong return value from read()\n");
        return 0;
    } else if (failed_value) {
    1556:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
    155a:	75 38                	jne    1594 <test_read_into_alloc_no_fork+0x163>
        printf(1, FAIL_MSG "wrong value written to memory by read()\n");
        return 0;
    } else {
        printf(1, PASS_MSG "read() into heap allocation\n");
    155c:	83 ec 08             	sub    $0x8,%esp
    155f:	68 8c 29 00 00       	push   $0x298c
    1564:	6a 01                	push   $0x1
    1566:	e8 48 08 00 00       	call   1db3 <printf>
        return 1;
    156b:	83 c4 10             	add    $0x10,%esp
    156e:	b8 01 00 00 00       	mov    $0x1,%eax
    1573:	eb 17                	jmp    158c <test_read_into_alloc_no_fork+0x15b>
        printf(1, FAIL_MSG "wrong return value from read()\n");
    1575:	83 ec 08             	sub    $0x8,%esp
    1578:	68 24 29 00 00       	push   $0x2924
    157d:	6a 01                	push   $0x1
    157f:	e8 2f 08 00 00       	call   1db3 <printf>
        return 0;
    1584:	83 c4 10             	add    $0x10,%esp
    1587:	b8 00 00 00 00       	mov    $0x0,%eax
    }
}
    158c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    158f:	5b                   	pop    %ebx
    1590:	5e                   	pop    %esi
    1591:	5f                   	pop    %edi
    1592:	5d                   	pop    %ebp
    1593:	c3                   	ret    
        printf(1, FAIL_MSG "wrong value written to memory by read()\n");
    1594:	83 ec 08             	sub    $0x8,%esp
    1597:	68 54 29 00 00       	push   $0x2954
    159c:	6a 01                	push   $0x1
    159e:	e8 10 08 00 00       	call   1db3 <printf>
        return 0;
    15a3:	83 c4 10             	add    $0x10,%esp
    15a6:	b8 00 00 00 00       	mov    $0x0,%eax
    15ab:	eb df                	jmp    158c <test_read_into_alloc_no_fork+0x15b>

000015ad <test_read_into_alloc>:

int test_read_into_alloc(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    15ad:	55                   	push   %ebp
    15ae:	89 e5                	mov    %esp,%ebp
    15b0:	83 ec 24             	sub    $0x24,%esp
    int pipe_fds[2];
    pipe(pipe_fds);
    15b3:	8d 45 f0             	lea    -0x10(%ebp),%eax
    15b6:	50                   	push   %eax
    15b7:	e8 95 06 00 00       	call   1c51 <pipe>
    int pid = fork();
    15bc:	e8 78 06 00 00       	call   1c39 <fork>
    if (pid == -1) {
    15c1:	83 c4 10             	add    $0x10,%esp
    15c4:	83 f8 ff             	cmp    $0xffffffff,%eax
    15c7:	74 38                	je     1601 <test_read_into_alloc+0x54>
        printf(1, FAIL_MSG "fork failed");
    } else if (pid == 0) {
    15c9:	85 c0                	test   %eax,%eax
    15cb:	74 4d                	je     161a <test_read_into_alloc+0x6d>
            result_str[0] = 'Y';
        }
        write(pipe_fds[1], result_str, 1);
        exit();
    } else {
        close(pipe_fds[1]);
    15cd:	83 ec 0c             	sub    $0xc,%esp
    15d0:	ff 75 f4             	pushl  -0xc(%ebp)
    15d3:	e8 91 06 00 00       	call   1c69 <close>
        char result_str[1] = {'N'};
    15d8:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        read(pipe_fds[0], result_str, 1);
    15dc:	83 c4 0c             	add    $0xc,%esp
    15df:	6a 01                	push   $0x1
    15e1:	8d 45 ef             	lea    -0x11(%ebp),%eax
    15e4:	50                   	push   %eax
    15e5:	ff 75 f0             	pushl  -0x10(%ebp)
    15e8:	e8 6c 06 00 00       	call   1c59 <read>
        wait();
    15ed:	e8 57 06 00 00       	call   1c49 <wait>
        return result_str[0] == 'Y';
    15f2:	83 c4 10             	add    $0x10,%esp
    15f5:	80 7d ef 59          	cmpb   $0x59,-0x11(%ebp)
    15f9:	0f 94 c0             	sete   %al
    15fc:	0f b6 c0             	movzbl %al,%eax
    }
    return 0;
}
    15ff:	c9                   	leave  
    1600:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
    1601:	83 ec 08             	sub    $0x8,%esp
    1604:	68 88 2c 00 00       	push   $0x2c88
    1609:	6a 01                	push   $0x1
    160b:	e8 a3 07 00 00       	call   1db3 <printf>
    return 0;
    1610:	83 c4 10             	add    $0x10,%esp
    1613:	b8 00 00 00 00       	mov    $0x0,%eax
    1618:	eb e5                	jmp    15ff <test_read_into_alloc+0x52>
        close(pipe_fds[0]);
    161a:	83 ec 0c             	sub    $0xc,%esp
    161d:	ff 75 f0             	pushl  -0x10(%ebp)
    1620:	e8 44 06 00 00       	call   1c69 <close>
        char result_str[1] = {'N'};
    1625:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        if (test_read_into_alloc_no_fork(size, offset, read_count, describe_size, describe_offset)) {
    1629:	83 c4 04             	add    $0x4,%esp
    162c:	ff 75 18             	pushl  0x18(%ebp)
    162f:	ff 75 14             	pushl  0x14(%ebp)
    1632:	ff 75 10             	pushl  0x10(%ebp)
    1635:	ff 75 0c             	pushl  0xc(%ebp)
    1638:	ff 75 08             	pushl  0x8(%ebp)
    163b:	e8 f1 fd ff ff       	call   1431 <test_read_into_alloc_no_fork>
    1640:	83 c4 20             	add    $0x20,%esp
    1643:	85 c0                	test   %eax,%eax
    1645:	74 04                	je     164b <test_read_into_alloc+0x9e>
            result_str[0] = 'Y';
    1647:	c6 45 ef 59          	movb   $0x59,-0x11(%ebp)
        write(pipe_fds[1], result_str, 1);
    164b:	83 ec 04             	sub    $0x4,%esp
    164e:	6a 01                	push   $0x1
    1650:	8d 45 ef             	lea    -0x11(%ebp),%eax
    1653:	50                   	push   %eax
    1654:	ff 75 f4             	pushl  -0xc(%ebp)
    1657:	e8 05 06 00 00       	call   1c61 <write>
        exit();
    165c:	e8 e0 05 00 00       	call   1c41 <exit>

00001661 <test_read_into_cow_less_forks>:

int test_read_into_cow_less_forks(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    1661:	55                   	push   %ebp
    1662:	89 e5                	mov    %esp,%ebp
    1664:	57                   	push   %edi
    1665:	56                   	push   %esi
    1666:	53                   	push   %ebx
    1667:	83 ec 38             	sub    $0x38,%esp
    166a:	8b 75 08             	mov    0x8(%ebp),%esi
    printf(1, "testing read(), writing %d bytes to a location %s into a %s copy-on-write allocation\n",
    166d:	ff 75 14             	pushl  0x14(%ebp)
    1670:	ff 75 18             	pushl  0x18(%ebp)
    1673:	ff 75 10             	pushl  0x10(%ebp)
    1676:	68 b8 29 00 00       	push   $0x29b8
    167b:	6a 01                	push   $0x1
    167d:	e8 31 07 00 00       	call   1db3 <printf>
        read_count, describe_offset, describe_size);
    int fd = open("tempfile", O_WRONLY | O_CREATE);
    1682:	83 c4 18             	add    $0x18,%esp
    1685:	68 01 02 00 00       	push   $0x201
    168a:	68 58 2e 00 00       	push   $0x2e58
    168f:	e8 ed 05 00 00       	call   1c81 <open>
    1694:	89 c7                	mov    %eax,%edi
    static char buffer[128]; // static to avoid running out of stack space
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1696:	83 c4 10             	add    $0x10,%esp
    1699:	b8 00 00 00 00       	mov    $0x0,%eax
    169e:	eb 0a                	jmp    16aa <test_read_into_cow_less_forks+0x49>
        buffer[i] = 'X';
    16a0:	c6 80 c0 35 00 00 58 	movb   $0x58,0x35c0(%eax)
    for (int i = 0 ; i < sizeof buffer; ++i) {
    16a7:	83 c0 01             	add    $0x1,%eax
    16aa:	83 f8 7f             	cmp    $0x7f,%eax
    16ad:	76 f1                	jbe    16a0 <test_read_into_cow_less_forks+0x3f>
    }
    for (int i = 0; i < read_count; i += sizeof buffer) {
    16af:	bb 00 00 00 00       	mov    $0x0,%ebx
    16b4:	eb 19                	jmp    16cf <test_read_into_cow_less_forks+0x6e>
        write(fd, buffer, sizeof buffer);
    16b6:	83 ec 04             	sub    $0x4,%esp
    16b9:	68 80 00 00 00       	push   $0x80
    16be:	68 c0 35 00 00       	push   $0x35c0
    16c3:	57                   	push   %edi
    16c4:	e8 98 05 00 00       	call   1c61 <write>
    for (int i = 0; i < read_count; i += sizeof buffer) {
    16c9:	83 eb 80             	sub    $0xffffff80,%ebx
    16cc:	83 c4 10             	add    $0x10,%esp
    16cf:	3b 5d 10             	cmp    0x10(%ebp),%ebx
    16d2:	7c e2                	jl     16b6 <test_read_into_cow_less_forks+0x55>
    }
    close(fd);
    16d4:	83 ec 0c             	sub    $0xc,%esp
    16d7:	57                   	push   %edi
    16d8:	e8 8c 05 00 00       	call   1c69 <close>
    fd = open("tempfile", O_RDONLY);
    16dd:	83 c4 08             	add    $0x8,%esp
    16e0:	6a 00                	push   $0x0
    16e2:	68 58 2e 00 00       	push   $0x2e58
    16e7:	e8 95 05 00 00       	call   1c81 <open>
    16ec:	89 45 d0             	mov    %eax,-0x30(%ebp)
    if (fd == -1) {
    16ef:	83 c4 10             	add    $0x10,%esp
    16f2:	83 f8 ff             	cmp    $0xffffffff,%eax
    16f5:	74 1e                	je     1715 <test_read_into_cow_less_forks+0xb4>
        printf(2, "error opening tempfile");
    }
    char *heap = sbrk(0);
    16f7:	83 ec 0c             	sub    $0xc,%esp
    16fa:	6a 00                	push   $0x0
    16fc:	e8 c8 05 00 00       	call   1cc9 <sbrk>
    1701:	89 c3                	mov    %eax,%ebx
    sbrk(size);
    1703:	89 34 24             	mov    %esi,(%esp)
    1706:	e8 be 05 00 00       	call   1cc9 <sbrk>
    for (int i = 0; i < size; ++i) {
    170b:	83 c4 10             	add    $0x10,%esp
    170e:	b8 00 00 00 00       	mov    $0x0,%eax
    1713:	eb 1b                	jmp    1730 <test_read_into_cow_less_forks+0xcf>
        printf(2, "error opening tempfile");
    1715:	83 ec 08             	sub    $0x8,%esp
    1718:	68 4a 2e 00 00       	push   $0x2e4a
    171d:	6a 02                	push   $0x2
    171f:	e8 8f 06 00 00       	call   1db3 <printf>
    1724:	83 c4 10             	add    $0x10,%esp
    1727:	eb ce                	jmp    16f7 <test_read_into_cow_less_forks+0x96>
        heap[i] = 'Y';
    1729:	c6 04 03 59          	movb   $0x59,(%ebx,%eax,1)
    for (int i = 0; i < size; ++i) {
    172d:	83 c0 01             	add    $0x1,%eax
    1730:	39 f0                	cmp    %esi,%eax
    1732:	7c f5                	jl     1729 <test_read_into_cow_less_forks+0xc8>
    }
    char *loc = heap + offset;
    1734:	89 d8                	mov    %ebx,%eax
    1736:	03 45 0c             	add    0xc(%ebp),%eax
    1739:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    int pipe_fds[2];
    pipe(pipe_fds);
    173c:	83 ec 0c             	sub    $0xc,%esp
    173f:	8d 45 e0             	lea    -0x20(%ebp),%eax
    1742:	50                   	push   %eax
    1743:	e8 09 05 00 00       	call   1c51 <pipe>
    int pid = fork();
    1748:	e8 ec 04 00 00       	call   1c39 <fork>
    174d:	89 c7                	mov    %eax,%edi
    if (pid == -1) {
    174f:	83 c4 10             	add    $0x10,%esp
    1752:	83 f8 ff             	cmp    $0xffffffff,%eax
    1755:	74 55                	je     17ac <test_read_into_cow_less_forks+0x14b>
        printf(1, FAIL_MSG "fork failed");
        exit();
    } else if (pid == 0) {
    1757:	85 c0                	test   %eax,%eax
    1759:	74 65                	je     17c0 <test_read_into_cow_less_forks+0x15f>
            write(pipe_fds[1], "Y", 1);
        }
        close(pipe_fds[1]);
        exit();
    } else {
        close(pipe_fds[1]);
    175b:	83 ec 0c             	sub    $0xc,%esp
    175e:	ff 75 e4             	pushl  -0x1c(%ebp)
    1761:	e8 03 05 00 00       	call   1c69 <close>
        char result_buf[1] = {'N'};
    1766:	c6 45 df 4e          	movb   $0x4e,-0x21(%ebp)
        read(pipe_fds[0], result_buf, 1);
    176a:	83 c4 0c             	add    $0xc,%esp
    176d:	6a 01                	push   $0x1
    176f:	8d 45 df             	lea    -0x21(%ebp),%eax
    1772:	50                   	push   %eax
    1773:	ff 75 e0             	pushl  -0x20(%ebp)
    1776:	e8 de 04 00 00       	call   1c59 <read>
        close(pipe_fds[0]);
    177b:	83 c4 04             	add    $0x4,%esp
    177e:	ff 75 e0             	pushl  -0x20(%ebp)
    1781:	e8 e3 04 00 00       	call   1c69 <close>
        wait();
    1786:	e8 be 04 00 00       	call   1c49 <wait>
        printf(1, "testing correct value for heap in parent after read() in child\n");
    178b:	83 c4 08             	add    $0x8,%esp
    178e:	68 50 2a 00 00       	push   $0x2a50
    1793:	6a 01                	push   $0x1
    1795:	e8 19 06 00 00       	call   1db3 <printf>
        int found_wrong = 0;
        for (int i = 0; i < size; ++i) {
    179a:	83 c4 10             	add    $0x10,%esp
    179d:	b8 00 00 00 00       	mov    $0x0,%eax
        int found_wrong = 0;
    17a2:	bf 00 00 00 00       	mov    $0x0,%edi
        for (int i = 0; i < size; ++i) {
    17a7:	e9 0f 01 00 00       	jmp    18bb <test_read_into_cow_less_forks+0x25a>
        printf(1, FAIL_MSG "fork failed");
    17ac:	83 ec 08             	sub    $0x8,%esp
    17af:	68 88 2c 00 00       	push   $0x2c88
    17b4:	6a 01                	push   $0x1
    17b6:	e8 f8 05 00 00       	call   1db3 <printf>
        exit();
    17bb:	e8 81 04 00 00       	call   1c41 <exit>
        close(pipe_fds[0]);
    17c0:	83 ec 0c             	sub    $0xc,%esp
    17c3:	ff 75 e0             	pushl  -0x20(%ebp)
    17c6:	e8 9e 04 00 00       	call   1c69 <close>
        int count = read(fd, loc, read_count);
    17cb:	83 c4 0c             	add    $0xc,%esp
    17ce:	ff 75 10             	pushl  0x10(%ebp)
    17d1:	8b 5d d4             	mov    -0x2c(%ebp),%ebx
    17d4:	53                   	push   %ebx
    17d5:	ff 75 d0             	pushl  -0x30(%ebp)
    17d8:	e8 7c 04 00 00       	call   1c59 <read>
    17dd:	89 c6                	mov    %eax,%esi
        failed_value = loc[-1] != 'Y';
    17df:	83 c4 10             	add    $0x10,%esp
    17e2:	80 7b ff 59          	cmpb   $0x59,-0x1(%ebx)
    17e6:	0f 95 c3             	setne  %bl
    17e9:	0f b6 db             	movzbl %bl,%ebx
        for (int i = 0; i < read_count; ++i) {
    17ec:	3b 7d 10             	cmp    0x10(%ebp),%edi
    17ef:	7c 64                	jl     1855 <test_read_into_cow_less_forks+0x1f4>
        if (loc[read_count] != 'Y') {
    17f1:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    17f4:	8b 55 10             	mov    0x10(%ebp),%edx
    17f7:	80 3c 10 59          	cmpb   $0x59,(%eax,%edx,1)
    17fb:	74 05                	je     1802 <test_read_into_cow_less_forks+0x1a1>
            failed_value = 1;
    17fd:	bb 01 00 00 00       	mov    $0x1,%ebx
        close(fd);
    1802:	83 ec 0c             	sub    $0xc,%esp
    1805:	ff 75 d0             	pushl  -0x30(%ebp)
    1808:	e8 5c 04 00 00       	call   1c69 <close>
        unlink("tempfile");
    180d:	c7 04 24 58 2e 00 00 	movl   $0x2e58,(%esp)
    1814:	e8 78 04 00 00       	call   1c91 <unlink>
        if (count != read_count) {
    1819:	83 c4 10             	add    $0x10,%esp
    181c:	39 75 10             	cmp    %esi,0x10(%ebp)
    181f:	74 47                	je     1868 <test_read_into_cow_less_forks+0x207>
            printf(1, FAIL_MSG "wrong return value from read()\n");
    1821:	83 ec 08             	sub    $0x8,%esp
    1824:	68 24 29 00 00       	push   $0x2924
    1829:	6a 01                	push   $0x1
    182b:	e8 83 05 00 00       	call   1db3 <printf>
            write(pipe_fds[1], "N", 1);
    1830:	83 c4 0c             	add    $0xc,%esp
    1833:	6a 01                	push   $0x1
    1835:	68 61 2e 00 00       	push   $0x2e61
    183a:	ff 75 e4             	pushl  -0x1c(%ebp)
    183d:	e8 1f 04 00 00       	call   1c61 <write>
    1842:	83 c4 10             	add    $0x10,%esp
        close(pipe_fds[1]);
    1845:	83 ec 0c             	sub    $0xc,%esp
    1848:	ff 75 e4             	pushl  -0x1c(%ebp)
    184b:	e8 19 04 00 00       	call   1c69 <close>
        exit();
    1850:	e8 ec 03 00 00       	call   1c41 <exit>
            if (loc[i] != 'X') {
    1855:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    1858:	80 3c 38 58          	cmpb   $0x58,(%eax,%edi,1)
    185c:	74 05                	je     1863 <test_read_into_cow_less_forks+0x202>
                failed_value = 1;
    185e:	bb 01 00 00 00       	mov    $0x1,%ebx
        for (int i = 0; i < read_count; ++i) {
    1863:	83 c7 01             	add    $0x1,%edi
    1866:	eb 84                	jmp    17ec <test_read_into_cow_less_forks+0x18b>
        } else if (failed_value) {
    1868:	85 db                	test   %ebx,%ebx
    186a:	74 26                	je     1892 <test_read_into_cow_less_forks+0x231>
            printf(1, FAIL_MSG "wrong value written to memory by read()\n");
    186c:	83 ec 08             	sub    $0x8,%esp
    186f:	68 54 29 00 00       	push   $0x2954
    1874:	6a 01                	push   $0x1
    1876:	e8 38 05 00 00       	call   1db3 <printf>
            write(pipe_fds[1], "N", 1);
    187b:	83 c4 0c             	add    $0xc,%esp
    187e:	6a 01                	push   $0x1
    1880:	68 61 2e 00 00       	push   $0x2e61
    1885:	ff 75 e4             	pushl  -0x1c(%ebp)
    1888:	e8 d4 03 00 00       	call   1c61 <write>
    188d:	83 c4 10             	add    $0x10,%esp
    1890:	eb b3                	jmp    1845 <test_read_into_cow_less_forks+0x1e4>
            printf(1, PASS_MSG "correct value read into copy-on-write allocation\n");
    1892:	83 ec 08             	sub    $0x8,%esp
    1895:	68 10 2a 00 00       	push   $0x2a10
    189a:	6a 01                	push   $0x1
    189c:	e8 12 05 00 00       	call   1db3 <printf>
            write(pipe_fds[1], "Y", 1);
    18a1:	83 c4 0c             	add    $0xc,%esp
    18a4:	6a 01                	push   $0x1
    18a6:	68 eb 2d 00 00       	push   $0x2deb
    18ab:	ff 75 e4             	pushl  -0x1c(%ebp)
    18ae:	e8 ae 03 00 00       	call   1c61 <write>
    18b3:	83 c4 10             	add    $0x10,%esp
    18b6:	eb 8d                	jmp    1845 <test_read_into_cow_less_forks+0x1e4>
        for (int i = 0; i < size; ++i) {
    18b8:	83 c0 01             	add    $0x1,%eax
    18bb:	39 f0                	cmp    %esi,%eax
    18bd:	7d 0d                	jge    18cc <test_read_into_cow_less_forks+0x26b>
            if (heap[i] != 'Y') {
    18bf:	80 3c 03 59          	cmpb   $0x59,(%ebx,%eax,1)
    18c3:	74 f3                	je     18b8 <test_read_into_cow_less_forks+0x257>
                found_wrong = 1;
    18c5:	bf 01 00 00 00       	mov    $0x1,%edi
    18ca:	eb ec                	jmp    18b8 <test_read_into_cow_less_forks+0x257>
            }
        }
        if (found_wrong) { 
    18cc:	85 ff                	test   %edi,%edi
    18ce:	75 29                	jne    18f9 <test_read_into_cow_less_forks+0x298>
            printf(1, FAIL_MSG "wrong value in parent after read() in child\n");
        } else {
            printf(1, PASS_MSG "correct value in parent after read into copy-on-write allocation\n");
    18d0:	83 ec 08             	sub    $0x8,%esp
    18d3:	68 cc 2a 00 00       	push   $0x2acc
    18d8:	6a 01                	push   $0x1
    18da:	e8 d4 04 00 00       	call   1db3 <printf>
    18df:	83 c4 10             	add    $0x10,%esp
        }
        return (found_wrong == 0) + (result_buf[0] == 'Y');
    18e2:	83 f7 01             	xor    $0x1,%edi
    18e5:	80 7d df 59          	cmpb   $0x59,-0x21(%ebp)
    18e9:	0f 94 c0             	sete   %al
    18ec:	0f b6 c0             	movzbl %al,%eax
    18ef:	01 f8                	add    %edi,%eax
    }
}
    18f1:	8d 65 f4             	lea    -0xc(%ebp),%esp
    18f4:	5b                   	pop    %ebx
    18f5:	5e                   	pop    %esi
    18f6:	5f                   	pop    %edi
    18f7:	5d                   	pop    %ebp
    18f8:	c3                   	ret    
            printf(1, FAIL_MSG "wrong value in parent after read() in child\n");
    18f9:	83 ec 08             	sub    $0x8,%esp
    18fc:	68 90 2a 00 00       	push   $0x2a90
    1901:	6a 01                	push   $0x1
    1903:	e8 ab 04 00 00       	call   1db3 <printf>
    1908:	83 c4 10             	add    $0x10,%esp
    190b:	eb d5                	jmp    18e2 <test_read_into_cow_less_forks+0x281>

0000190d <test_read_into_cow>:

int test_read_into_cow(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    190d:	55                   	push   %ebp
    190e:	89 e5                	mov    %esp,%ebp
    1910:	83 ec 24             	sub    $0x24,%esp
    int pipe_fds[2];
    pipe(pipe_fds);
    1913:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1916:	50                   	push   %eax
    1917:	e8 35 03 00 00       	call   1c51 <pipe>
    int pid = fork();
    191c:	e8 18 03 00 00       	call   1c39 <fork>
    if (pid == -1) {
    1921:	83 c4 10             	add    $0x10,%esp
    1924:	83 f8 ff             	cmp    $0xffffffff,%eax
    1927:	74 38                	je     1961 <test_read_into_cow+0x54>
        printf(1, FAIL_MSG "fork failed");
        exit();
    } else if (pid == 0) {
    1929:	85 c0                	test   %eax,%eax
    192b:	74 48                	je     1975 <test_read_into_cow+0x68>
            result_str[0] = 'Y';
        }
        write(pipe_fds[1], result_str, 1);
        exit();
    } else {
        close(pipe_fds[1]);
    192d:	83 ec 0c             	sub    $0xc,%esp
    1930:	ff 75 f4             	pushl  -0xc(%ebp)
    1933:	e8 31 03 00 00       	call   1c69 <close>
        char result_str[1] = {'N'};
    1938:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        read(pipe_fds[0], result_str, 1);
    193c:	83 c4 0c             	add    $0xc,%esp
    193f:	6a 01                	push   $0x1
    1941:	8d 45 ef             	lea    -0x11(%ebp),%eax
    1944:	50                   	push   %eax
    1945:	ff 75 f0             	pushl  -0x10(%ebp)
    1948:	e8 0c 03 00 00       	call   1c59 <read>
        wait();
    194d:	e8 f7 02 00 00       	call   1c49 <wait>
        return result_str[0] == 'Y';
    1952:	83 c4 10             	add    $0x10,%esp
    1955:	80 7d ef 59          	cmpb   $0x59,-0x11(%ebp)
    1959:	0f 94 c0             	sete   %al
    195c:	0f b6 c0             	movzbl %al,%eax
    }
}
    195f:	c9                   	leave  
    1960:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
    1961:	83 ec 08             	sub    $0x8,%esp
    1964:	68 88 2c 00 00       	push   $0x2c88
    1969:	6a 01                	push   $0x1
    196b:	e8 43 04 00 00       	call   1db3 <printf>
        exit();
    1970:	e8 cc 02 00 00       	call   1c41 <exit>
        close(pipe_fds[0]);
    1975:	83 ec 0c             	sub    $0xc,%esp
    1978:	ff 75 f0             	pushl  -0x10(%ebp)
    197b:	e8 e9 02 00 00       	call   1c69 <close>
        char result_str[1] = {'N'};
    1980:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        if (test_read_into_cow_less_forks(size, offset, read_count, describe_size, describe_offset)) {
    1984:	83 c4 04             	add    $0x4,%esp
    1987:	ff 75 18             	pushl  0x18(%ebp)
    198a:	ff 75 14             	pushl  0x14(%ebp)
    198d:	ff 75 10             	pushl  0x10(%ebp)
    1990:	ff 75 0c             	pushl  0xc(%ebp)
    1993:	ff 75 08             	pushl  0x8(%ebp)
    1996:	e8 c6 fc ff ff       	call   1661 <test_read_into_cow_less_forks>
    199b:	83 c4 20             	add    $0x20,%esp
    199e:	85 c0                	test   %eax,%eax
    19a0:	74 04                	je     19a6 <test_read_into_cow+0x99>
            result_str[0] = 'Y';
    19a2:	c6 45 ef 59          	movb   $0x59,-0x11(%ebp)
        write(pipe_fds[1], result_str, 1);
    19a6:	83 ec 04             	sub    $0x4,%esp
    19a9:	6a 01                	push   $0x1
    19ab:	8d 45 ef             	lea    -0x11(%ebp),%eax
    19ae:	50                   	push   %eax
    19af:	ff 75 f4             	pushl  -0xc(%ebp)
    19b2:	e8 aa 02 00 00       	call   1c61 <write>
        exit();
    19b7:	e8 85 02 00 00       	call   1c41 <exit>

000019bc <test_dealloc_cow_less_forks>:

int test_dealloc_cow_less_forks(int size) {
    19bc:	55                   	push   %ebp
    19bd:	89 e5                	mov    %esp,%ebp
    19bf:	56                   	push   %esi
    19c0:	53                   	push   %ebx
    19c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
    char *heap = sbrk(0);
    19c4:	83 ec 0c             	sub    $0xc,%esp
    19c7:	6a 00                	push   $0x0
    19c9:	e8 fb 02 00 00       	call   1cc9 <sbrk>
    19ce:	89 c6                	mov    %eax,%esi
    sbrk(size);
    19d0:	89 1c 24             	mov    %ebx,(%esp)
    19d3:	e8 f1 02 00 00       	call   1cc9 <sbrk>
    printf(1, "testing that deallocating (with sbrk()) shared copy-on-write memory in child does not change it in parent\n");
    19d8:	83 c4 08             	add    $0x8,%esp
    19db:	68 1c 2b 00 00       	push   $0x2b1c
    19e0:	6a 01                	push   $0x1
    19e2:	e8 cc 03 00 00       	call   1db3 <printf>
    for (int i = 0; i < size; ++i) {
    19e7:	83 c4 10             	add    $0x10,%esp
    19ea:	b8 00 00 00 00       	mov    $0x0,%eax
    19ef:	eb 07                	jmp    19f8 <test_dealloc_cow_less_forks+0x3c>
        heap[i] = 'Y';
    19f1:	c6 04 06 59          	movb   $0x59,(%esi,%eax,1)
    for (int i = 0; i < size; ++i) {
    19f5:	83 c0 01             	add    $0x1,%eax
    19f8:	39 d8                	cmp    %ebx,%eax
    19fa:	7c f5                	jl     19f1 <test_dealloc_cow_less_forks+0x35>
    }
    int pid = fork();
    19fc:	e8 38 02 00 00       	call   1c39 <fork>
    if (pid == 0) {
    1a01:	85 c0                	test   %eax,%eax
    1a03:	74 11                	je     1a16 <test_dealloc_cow_less_forks+0x5a>
        sbrk(-size);
        exit();
    } else {
        wait();
    1a05:	e8 3f 02 00 00       	call   1c49 <wait>
        int found_wrong = 0;
        for (int i = 0; i < size; ++i) {
    1a0a:	b8 00 00 00 00       	mov    $0x0,%eax
        int found_wrong = 0;
    1a0f:	ba 00 00 00 00       	mov    $0x0,%edx
        for (int i = 0; i < size; ++i) {
    1a14:	eb 13                	jmp    1a29 <test_dealloc_cow_less_forks+0x6d>
        sbrk(-size);
    1a16:	83 ec 0c             	sub    $0xc,%esp
    1a19:	f7 db                	neg    %ebx
    1a1b:	53                   	push   %ebx
    1a1c:	e8 a8 02 00 00       	call   1cc9 <sbrk>
        exit();
    1a21:	e8 1b 02 00 00       	call   1c41 <exit>
        for (int i = 0; i < size; ++i) {
    1a26:	83 c0 01             	add    $0x1,%eax
    1a29:	39 d8                	cmp    %ebx,%eax
    1a2b:	7d 0d                	jge    1a3a <test_dealloc_cow_less_forks+0x7e>
            if (heap[i] != 'Y') {
    1a2d:	80 3c 06 59          	cmpb   $0x59,(%esi,%eax,1)
    1a31:	74 f3                	je     1a26 <test_dealloc_cow_less_forks+0x6a>
                found_wrong = 1;
    1a33:	ba 01 00 00 00       	mov    $0x1,%edx
    1a38:	eb ec                	jmp    1a26 <test_dealloc_cow_less_forks+0x6a>
            }
        }
        if (found_wrong) {
    1a3a:	85 d2                	test   %edx,%edx
    1a3c:	75 1e                	jne    1a5c <test_dealloc_cow_less_forks+0xa0>
            printf(1, FAIL_MSG "wrong value in parent after sbrk(-size) in child\n");
            return 0;
        } else {
            printf(1, PASS_MSG "correct values in parent after sbrk(-size) in child\n");
    1a3e:	83 ec 08             	sub    $0x8,%esp
    1a41:	68 c8 2b 00 00       	push   $0x2bc8
    1a46:	6a 01                	push   $0x1
    1a48:	e8 66 03 00 00       	call   1db3 <printf>
            return 1;
    1a4d:	83 c4 10             	add    $0x10,%esp
    1a50:	b8 01 00 00 00       	mov    $0x1,%eax
        }
    }
}
    1a55:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1a58:	5b                   	pop    %ebx
    1a59:	5e                   	pop    %esi
    1a5a:	5d                   	pop    %ebp
    1a5b:	c3                   	ret    
            printf(1, FAIL_MSG "wrong value in parent after sbrk(-size) in child\n");
    1a5c:	83 ec 08             	sub    $0x8,%esp
    1a5f:	68 88 2b 00 00       	push   $0x2b88
    1a64:	6a 01                	push   $0x1
    1a66:	e8 48 03 00 00       	call   1db3 <printf>
            return 0;
    1a6b:	83 c4 10             	add    $0x10,%esp
    1a6e:	b8 00 00 00 00       	mov    $0x0,%eax
    1a73:	eb e0                	jmp    1a55 <test_dealloc_cow_less_forks+0x99>

00001a75 <main>:

int main(void) {
    1a75:	8d 4c 24 04          	lea    0x4(%esp),%ecx
    1a79:	83 e4 f0             	and    $0xfffffff0,%esp
    1a7c:	ff 71 fc             	pushl  -0x4(%ecx)
    1a7f:	55                   	push   %ebp
    1a80:	89 e5                	mov    %esp,%ebp
    1a82:	51                   	push   %ecx
    1a83:	83 ec 04             	sub    $0x4,%esp
    setup();
    1a86:	e8 27 e6 ff ff       	call   b2 <setup>
    test_simple_crash(read_guard_page,
    1a8b:	83 ec 04             	sub    $0x4,%esp
    1a8e:	68 0c 2c 00 00       	push   $0x2c0c
    1a93:	68 48 2c 00 00       	push   $0x2c48
    1a98:	68 00 00 00 00       	push   $0x0
    1a9d:	e8 a5 e6 ff ff       	call   147 <test_simple_crash>
        PASS_MSG "reading from guard page killed process",
        FAIL_MSG "reading from guard page did not kill process?");
    finish();
    1aa2:	e8 64 e6 ff ff       	call   10b <finish>
}
    1aa7:	b8 00 00 00 00       	mov    $0x0,%eax
    1aac:	8b 4d fc             	mov    -0x4(%ebp),%ecx
    1aaf:	c9                   	leave  
    1ab0:	8d 61 fc             	lea    -0x4(%ecx),%esp
    1ab3:	c3                   	ret    

00001ab4 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
    1ab4:	55                   	push   %ebp
    1ab5:	89 e5                	mov    %esp,%ebp
    1ab7:	53                   	push   %ebx
    1ab8:	8b 45 08             	mov    0x8(%ebp),%eax
    1abb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
    1abe:	89 c2                	mov    %eax,%edx
    1ac0:	0f b6 19             	movzbl (%ecx),%ebx
    1ac3:	88 1a                	mov    %bl,(%edx)
    1ac5:	8d 52 01             	lea    0x1(%edx),%edx
    1ac8:	8d 49 01             	lea    0x1(%ecx),%ecx
    1acb:	84 db                	test   %bl,%bl
    1acd:	75 f1                	jne    1ac0 <strcpy+0xc>
    ;
  return os;
}
    1acf:	5b                   	pop    %ebx
    1ad0:	5d                   	pop    %ebp
    1ad1:	c3                   	ret    

00001ad2 <strcmp>:

int
strcmp(const char *p, const char *q)
{
    1ad2:	55                   	push   %ebp
    1ad3:	89 e5                	mov    %esp,%ebp
    1ad5:	8b 4d 08             	mov    0x8(%ebp),%ecx
    1ad8:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
    1adb:	eb 06                	jmp    1ae3 <strcmp+0x11>
    p++, q++;
    1add:	83 c1 01             	add    $0x1,%ecx
    1ae0:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
    1ae3:	0f b6 01             	movzbl (%ecx),%eax
    1ae6:	84 c0                	test   %al,%al
    1ae8:	74 04                	je     1aee <strcmp+0x1c>
    1aea:	3a 02                	cmp    (%edx),%al
    1aec:	74 ef                	je     1add <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
    1aee:	0f b6 c0             	movzbl %al,%eax
    1af1:	0f b6 12             	movzbl (%edx),%edx
    1af4:	29 d0                	sub    %edx,%eax
}
    1af6:	5d                   	pop    %ebp
    1af7:	c3                   	ret    

00001af8 <strlen>:

uint
strlen(const char *s)
{
    1af8:	55                   	push   %ebp
    1af9:	89 e5                	mov    %esp,%ebp
    1afb:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
    1afe:	ba 00 00 00 00       	mov    $0x0,%edx
    1b03:	eb 03                	jmp    1b08 <strlen+0x10>
    1b05:	83 c2 01             	add    $0x1,%edx
    1b08:	89 d0                	mov    %edx,%eax
    1b0a:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
    1b0e:	75 f5                	jne    1b05 <strlen+0xd>
    ;
  return n;
}
    1b10:	5d                   	pop    %ebp
    1b11:	c3                   	ret    

00001b12 <memset>:

void*
memset(void *dst, int c, uint n)
{
    1b12:	55                   	push   %ebp
    1b13:	89 e5                	mov    %esp,%ebp
    1b15:	57                   	push   %edi
    1b16:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
    1b19:	89 d7                	mov    %edx,%edi
    1b1b:	8b 4d 10             	mov    0x10(%ebp),%ecx
    1b1e:	8b 45 0c             	mov    0xc(%ebp),%eax
    1b21:	fc                   	cld    
    1b22:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
    1b24:	89 d0                	mov    %edx,%eax
    1b26:	5f                   	pop    %edi
    1b27:	5d                   	pop    %ebp
    1b28:	c3                   	ret    

00001b29 <strchr>:

char*
strchr(const char *s, char c)
{
    1b29:	55                   	push   %ebp
    1b2a:	89 e5                	mov    %esp,%ebp
    1b2c:	8b 45 08             	mov    0x8(%ebp),%eax
    1b2f:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
    1b33:	0f b6 10             	movzbl (%eax),%edx
    1b36:	84 d2                	test   %dl,%dl
    1b38:	74 09                	je     1b43 <strchr+0x1a>
    if(*s == c)
    1b3a:	38 ca                	cmp    %cl,%dl
    1b3c:	74 0a                	je     1b48 <strchr+0x1f>
  for(; *s; s++)
    1b3e:	83 c0 01             	add    $0x1,%eax
    1b41:	eb f0                	jmp    1b33 <strchr+0xa>
      return (char*)s;
  return 0;
    1b43:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1b48:	5d                   	pop    %ebp
    1b49:	c3                   	ret    

00001b4a <gets>:

char*
gets(char *buf, int max)
{
    1b4a:	55                   	push   %ebp
    1b4b:	89 e5                	mov    %esp,%ebp
    1b4d:	57                   	push   %edi
    1b4e:	56                   	push   %esi
    1b4f:	53                   	push   %ebx
    1b50:	83 ec 1c             	sub    $0x1c,%esp
    1b53:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    1b56:	bb 00 00 00 00       	mov    $0x0,%ebx
    1b5b:	8d 73 01             	lea    0x1(%ebx),%esi
    1b5e:	3b 75 0c             	cmp    0xc(%ebp),%esi
    1b61:	7d 2e                	jge    1b91 <gets+0x47>
    cc = read(0, &c, 1);
    1b63:	83 ec 04             	sub    $0x4,%esp
    1b66:	6a 01                	push   $0x1
    1b68:	8d 45 e7             	lea    -0x19(%ebp),%eax
    1b6b:	50                   	push   %eax
    1b6c:	6a 00                	push   $0x0
    1b6e:	e8 e6 00 00 00       	call   1c59 <read>
    if(cc < 1)
    1b73:	83 c4 10             	add    $0x10,%esp
    1b76:	85 c0                	test   %eax,%eax
    1b78:	7e 17                	jle    1b91 <gets+0x47>
      break;
    buf[i++] = c;
    1b7a:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
    1b7e:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
    1b81:	3c 0a                	cmp    $0xa,%al
    1b83:	0f 94 c2             	sete   %dl
    1b86:	3c 0d                	cmp    $0xd,%al
    1b88:	0f 94 c0             	sete   %al
    buf[i++] = c;
    1b8b:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
    1b8d:	08 c2                	or     %al,%dl
    1b8f:	74 ca                	je     1b5b <gets+0x11>
      break;
  }
  buf[i] = '\0';
    1b91:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
    1b95:	89 f8                	mov    %edi,%eax
    1b97:	8d 65 f4             	lea    -0xc(%ebp),%esp
    1b9a:	5b                   	pop    %ebx
    1b9b:	5e                   	pop    %esi
    1b9c:	5f                   	pop    %edi
    1b9d:	5d                   	pop    %ebp
    1b9e:	c3                   	ret    

00001b9f <stat>:

int
stat(const char *n, struct stat *st)
{
    1b9f:	55                   	push   %ebp
    1ba0:	89 e5                	mov    %esp,%ebp
    1ba2:	56                   	push   %esi
    1ba3:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    1ba4:	83 ec 08             	sub    $0x8,%esp
    1ba7:	6a 00                	push   $0x0
    1ba9:	ff 75 08             	pushl  0x8(%ebp)
    1bac:	e8 d0 00 00 00       	call   1c81 <open>
  if(fd < 0)
    1bb1:	83 c4 10             	add    $0x10,%esp
    1bb4:	85 c0                	test   %eax,%eax
    1bb6:	78 24                	js     1bdc <stat+0x3d>
    1bb8:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
    1bba:	83 ec 08             	sub    $0x8,%esp
    1bbd:	ff 75 0c             	pushl  0xc(%ebp)
    1bc0:	50                   	push   %eax
    1bc1:	e8 d3 00 00 00       	call   1c99 <fstat>
    1bc6:	89 c6                	mov    %eax,%esi
  close(fd);
    1bc8:	89 1c 24             	mov    %ebx,(%esp)
    1bcb:	e8 99 00 00 00       	call   1c69 <close>
  return r;
    1bd0:	83 c4 10             	add    $0x10,%esp
}
    1bd3:	89 f0                	mov    %esi,%eax
    1bd5:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1bd8:	5b                   	pop    %ebx
    1bd9:	5e                   	pop    %esi
    1bda:	5d                   	pop    %ebp
    1bdb:	c3                   	ret    
    return -1;
    1bdc:	be ff ff ff ff       	mov    $0xffffffff,%esi
    1be1:	eb f0                	jmp    1bd3 <stat+0x34>

00001be3 <atoi>:

int
atoi(const char *s)
{
    1be3:	55                   	push   %ebp
    1be4:	89 e5                	mov    %esp,%ebp
    1be6:	53                   	push   %ebx
    1be7:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
    1bea:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
    1bef:	eb 10                	jmp    1c01 <atoi+0x1e>
    n = n*10 + *s++ - '0';
    1bf1:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
    1bf4:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
    1bf7:	83 c1 01             	add    $0x1,%ecx
    1bfa:	0f be d2             	movsbl %dl,%edx
    1bfd:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
    1c01:	0f b6 11             	movzbl (%ecx),%edx
    1c04:	8d 5a d0             	lea    -0x30(%edx),%ebx
    1c07:	80 fb 09             	cmp    $0x9,%bl
    1c0a:	76 e5                	jbe    1bf1 <atoi+0xe>
  return n;
}
    1c0c:	5b                   	pop    %ebx
    1c0d:	5d                   	pop    %ebp
    1c0e:	c3                   	ret    

00001c0f <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
    1c0f:	55                   	push   %ebp
    1c10:	89 e5                	mov    %esp,%ebp
    1c12:	56                   	push   %esi
    1c13:	53                   	push   %ebx
    1c14:	8b 45 08             	mov    0x8(%ebp),%eax
    1c17:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    1c1a:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
    1c1d:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
    1c1f:	eb 0d                	jmp    1c2e <memmove+0x1f>
    *dst++ = *src++;
    1c21:	0f b6 13             	movzbl (%ebx),%edx
    1c24:	88 11                	mov    %dl,(%ecx)
    1c26:	8d 5b 01             	lea    0x1(%ebx),%ebx
    1c29:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
    1c2c:	89 f2                	mov    %esi,%edx
    1c2e:	8d 72 ff             	lea    -0x1(%edx),%esi
    1c31:	85 d2                	test   %edx,%edx
    1c33:	7f ec                	jg     1c21 <memmove+0x12>
  return vdst;
}
    1c35:	5b                   	pop    %ebx
    1c36:	5e                   	pop    %esi
    1c37:	5d                   	pop    %ebp
    1c38:	c3                   	ret    

00001c39 <fork>:
    1c39:	b8 01 00 00 00       	mov    $0x1,%eax
    1c3e:	cd 40                	int    $0x40
    1c40:	c3                   	ret    

00001c41 <exit>:
    1c41:	b8 02 00 00 00       	mov    $0x2,%eax
    1c46:	cd 40                	int    $0x40
    1c48:	c3                   	ret    

00001c49 <wait>:
    1c49:	b8 03 00 00 00       	mov    $0x3,%eax
    1c4e:	cd 40                	int    $0x40
    1c50:	c3                   	ret    

00001c51 <pipe>:
    1c51:	b8 04 00 00 00       	mov    $0x4,%eax
    1c56:	cd 40                	int    $0x40
    1c58:	c3                   	ret    

00001c59 <read>:
    1c59:	b8 05 00 00 00       	mov    $0x5,%eax
    1c5e:	cd 40                	int    $0x40
    1c60:	c3                   	ret    

00001c61 <write>:
    1c61:	b8 10 00 00 00       	mov    $0x10,%eax
    1c66:	cd 40                	int    $0x40
    1c68:	c3                   	ret    

00001c69 <close>:
    1c69:	b8 15 00 00 00       	mov    $0x15,%eax
    1c6e:	cd 40                	int    $0x40
    1c70:	c3                   	ret    

00001c71 <kill>:
    1c71:	b8 06 00 00 00       	mov    $0x6,%eax
    1c76:	cd 40                	int    $0x40
    1c78:	c3                   	ret    

00001c79 <exec>:
    1c79:	b8 07 00 00 00       	mov    $0x7,%eax
    1c7e:	cd 40                	int    $0x40
    1c80:	c3                   	ret    

00001c81 <open>:
    1c81:	b8 0f 00 00 00       	mov    $0xf,%eax
    1c86:	cd 40                	int    $0x40
    1c88:	c3                   	ret    

00001c89 <mknod>:
    1c89:	b8 11 00 00 00       	mov    $0x11,%eax
    1c8e:	cd 40                	int    $0x40
    1c90:	c3                   	ret    

00001c91 <unlink>:
    1c91:	b8 12 00 00 00       	mov    $0x12,%eax
    1c96:	cd 40                	int    $0x40
    1c98:	c3                   	ret    

00001c99 <fstat>:
    1c99:	b8 08 00 00 00       	mov    $0x8,%eax
    1c9e:	cd 40                	int    $0x40
    1ca0:	c3                   	ret    

00001ca1 <link>:
    1ca1:	b8 13 00 00 00       	mov    $0x13,%eax
    1ca6:	cd 40                	int    $0x40
    1ca8:	c3                   	ret    

00001ca9 <mkdir>:
    1ca9:	b8 14 00 00 00       	mov    $0x14,%eax
    1cae:	cd 40                	int    $0x40
    1cb0:	c3                   	ret    

00001cb1 <chdir>:
    1cb1:	b8 09 00 00 00       	mov    $0x9,%eax
    1cb6:	cd 40                	int    $0x40
    1cb8:	c3                   	ret    

00001cb9 <dup>:
    1cb9:	b8 0a 00 00 00       	mov    $0xa,%eax
    1cbe:	cd 40                	int    $0x40
    1cc0:	c3                   	ret    

00001cc1 <getpid>:
    1cc1:	b8 0b 00 00 00       	mov    $0xb,%eax
    1cc6:	cd 40                	int    $0x40
    1cc8:	c3                   	ret    

00001cc9 <sbrk>:
    1cc9:	b8 0c 00 00 00       	mov    $0xc,%eax
    1cce:	cd 40                	int    $0x40
    1cd0:	c3                   	ret    

00001cd1 <sleep>:
    1cd1:	b8 0d 00 00 00       	mov    $0xd,%eax
    1cd6:	cd 40                	int    $0x40
    1cd8:	c3                   	ret    

00001cd9 <uptime>:
    1cd9:	b8 0e 00 00 00       	mov    $0xe,%eax
    1cde:	cd 40                	int    $0x40
    1ce0:	c3                   	ret    

00001ce1 <yield>:
    1ce1:	b8 16 00 00 00       	mov    $0x16,%eax
    1ce6:	cd 40                	int    $0x40
    1ce8:	c3                   	ret    

00001ce9 <shutdown>:
    1ce9:	b8 17 00 00 00       	mov    $0x17,%eax
    1cee:	cd 40                	int    $0x40
    1cf0:	c3                   	ret    

00001cf1 <writecount>:
    1cf1:	b8 18 00 00 00       	mov    $0x18,%eax
    1cf6:	cd 40                	int    $0x40
    1cf8:	c3                   	ret    

00001cf9 <setwritecount>:
    1cf9:	b8 19 00 00 00       	mov    $0x19,%eax
    1cfe:	cd 40                	int    $0x40
    1d00:	c3                   	ret    

00001d01 <settickets>:
    1d01:	b8 1a 00 00 00       	mov    $0x1a,%eax
    1d06:	cd 40                	int    $0x40
    1d08:	c3                   	ret    

00001d09 <getprocessesinfo>:
    1d09:	b8 1b 00 00 00       	mov    $0x1b,%eax
    1d0e:	cd 40                	int    $0x40
    1d10:	c3                   	ret    

00001d11 <dumppagetable>:
    1d11:	b8 1c 00 00 00       	mov    $0x1c,%eax
    1d16:	cd 40                	int    $0x40
    1d18:	c3                   	ret    

00001d19 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
    1d19:	55                   	push   %ebp
    1d1a:	89 e5                	mov    %esp,%ebp
    1d1c:	83 ec 1c             	sub    $0x1c,%esp
    1d1f:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
    1d22:	6a 01                	push   $0x1
    1d24:	8d 55 f4             	lea    -0xc(%ebp),%edx
    1d27:	52                   	push   %edx
    1d28:	50                   	push   %eax
    1d29:	e8 33 ff ff ff       	call   1c61 <write>
}
    1d2e:	83 c4 10             	add    $0x10,%esp
    1d31:	c9                   	leave  
    1d32:	c3                   	ret    

00001d33 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
    1d33:	55                   	push   %ebp
    1d34:	89 e5                	mov    %esp,%ebp
    1d36:	57                   	push   %edi
    1d37:	56                   	push   %esi
    1d38:	53                   	push   %ebx
    1d39:	83 ec 2c             	sub    $0x2c,%esp
    1d3c:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    1d3e:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
    1d42:	0f 95 c3             	setne  %bl
    1d45:	89 d0                	mov    %edx,%eax
    1d47:	c1 e8 1f             	shr    $0x1f,%eax
    1d4a:	84 c3                	test   %al,%bl
    1d4c:	74 10                	je     1d5e <printint+0x2b>
    neg = 1;
    x = -xx;
    1d4e:	f7 da                	neg    %edx
    neg = 1;
    1d50:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
    1d57:	be 00 00 00 00       	mov    $0x0,%esi
    1d5c:	eb 0b                	jmp    1d69 <printint+0x36>
  neg = 0;
    1d5e:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
    1d65:	eb f0                	jmp    1d57 <printint+0x24>
  do{
    buf[i++] = digits[x % base];
    1d67:	89 c6                	mov    %eax,%esi
    1d69:	89 d0                	mov    %edx,%eax
    1d6b:	ba 00 00 00 00       	mov    $0x0,%edx
    1d70:	f7 f1                	div    %ecx
    1d72:	89 c3                	mov    %eax,%ebx
    1d74:	8d 46 01             	lea    0x1(%esi),%eax
    1d77:	0f b6 92 6c 2e 00 00 	movzbl 0x2e6c(%edx),%edx
    1d7e:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
    1d82:	89 da                	mov    %ebx,%edx
    1d84:	85 db                	test   %ebx,%ebx
    1d86:	75 df                	jne    1d67 <printint+0x34>
    1d88:	89 c3                	mov    %eax,%ebx
  if(neg)
    1d8a:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
    1d8e:	74 16                	je     1da6 <printint+0x73>
    buf[i++] = '-';
    1d90:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
    1d95:	8d 5e 02             	lea    0x2(%esi),%ebx
    1d98:	eb 0c                	jmp    1da6 <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
    1d9a:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
    1d9f:	89 f8                	mov    %edi,%eax
    1da1:	e8 73 ff ff ff       	call   1d19 <putc>
  while(--i >= 0)
    1da6:	83 eb 01             	sub    $0x1,%ebx
    1da9:	79 ef                	jns    1d9a <printint+0x67>
}
    1dab:	83 c4 2c             	add    $0x2c,%esp
    1dae:	5b                   	pop    %ebx
    1daf:	5e                   	pop    %esi
    1db0:	5f                   	pop    %edi
    1db1:	5d                   	pop    %ebp
    1db2:	c3                   	ret    

00001db3 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
    1db3:	55                   	push   %ebp
    1db4:	89 e5                	mov    %esp,%ebp
    1db6:	57                   	push   %edi
    1db7:	56                   	push   %esi
    1db8:	53                   	push   %ebx
    1db9:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
    1dbc:	8d 45 10             	lea    0x10(%ebp),%eax
    1dbf:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
    1dc2:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
    1dc7:	bb 00 00 00 00       	mov    $0x0,%ebx
    1dcc:	eb 14                	jmp    1de2 <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
    1dce:	89 fa                	mov    %edi,%edx
    1dd0:	8b 45 08             	mov    0x8(%ebp),%eax
    1dd3:	e8 41 ff ff ff       	call   1d19 <putc>
    1dd8:	eb 05                	jmp    1ddf <printf+0x2c>
      }
    } else if(state == '%'){
    1dda:	83 fe 25             	cmp    $0x25,%esi
    1ddd:	74 25                	je     1e04 <printf+0x51>
  for(i = 0; fmt[i]; i++){
    1ddf:	83 c3 01             	add    $0x1,%ebx
    1de2:	8b 45 0c             	mov    0xc(%ebp),%eax
    1de5:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
    1de9:	84 c0                	test   %al,%al
    1deb:	0f 84 23 01 00 00    	je     1f14 <printf+0x161>
    c = fmt[i] & 0xff;
    1df1:	0f be f8             	movsbl %al,%edi
    1df4:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
    1df7:	85 f6                	test   %esi,%esi
    1df9:	75 df                	jne    1dda <printf+0x27>
      if(c == '%'){
    1dfb:	83 f8 25             	cmp    $0x25,%eax
    1dfe:	75 ce                	jne    1dce <printf+0x1b>
        state = '%';
    1e00:	89 c6                	mov    %eax,%esi
    1e02:	eb db                	jmp    1ddf <printf+0x2c>
      if(c == 'd'){
    1e04:	83 f8 64             	cmp    $0x64,%eax
    1e07:	74 49                	je     1e52 <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
    1e09:	83 f8 78             	cmp    $0x78,%eax
    1e0c:	0f 94 c1             	sete   %cl
    1e0f:	83 f8 70             	cmp    $0x70,%eax
    1e12:	0f 94 c2             	sete   %dl
    1e15:	08 d1                	or     %dl,%cl
    1e17:	75 63                	jne    1e7c <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
    1e19:	83 f8 73             	cmp    $0x73,%eax
    1e1c:	0f 84 84 00 00 00    	je     1ea6 <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    1e22:	83 f8 63             	cmp    $0x63,%eax
    1e25:	0f 84 b7 00 00 00    	je     1ee2 <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
    1e2b:	83 f8 25             	cmp    $0x25,%eax
    1e2e:	0f 84 cc 00 00 00    	je     1f00 <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    1e34:	ba 25 00 00 00       	mov    $0x25,%edx
    1e39:	8b 45 08             	mov    0x8(%ebp),%eax
    1e3c:	e8 d8 fe ff ff       	call   1d19 <putc>
        putc(fd, c);
    1e41:	89 fa                	mov    %edi,%edx
    1e43:	8b 45 08             	mov    0x8(%ebp),%eax
    1e46:	e8 ce fe ff ff       	call   1d19 <putc>
      }
      state = 0;
    1e4b:	be 00 00 00 00       	mov    $0x0,%esi
    1e50:	eb 8d                	jmp    1ddf <printf+0x2c>
        printint(fd, *ap, 10, 1);
    1e52:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1e55:	8b 17                	mov    (%edi),%edx
    1e57:	83 ec 0c             	sub    $0xc,%esp
    1e5a:	6a 01                	push   $0x1
    1e5c:	b9 0a 00 00 00       	mov    $0xa,%ecx
    1e61:	8b 45 08             	mov    0x8(%ebp),%eax
    1e64:	e8 ca fe ff ff       	call   1d33 <printint>
        ap++;
    1e69:	83 c7 04             	add    $0x4,%edi
    1e6c:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    1e6f:	83 c4 10             	add    $0x10,%esp
      state = 0;
    1e72:	be 00 00 00 00       	mov    $0x0,%esi
    1e77:	e9 63 ff ff ff       	jmp    1ddf <printf+0x2c>
        printint(fd, *ap, 16, 0);
    1e7c:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1e7f:	8b 17                	mov    (%edi),%edx
    1e81:	83 ec 0c             	sub    $0xc,%esp
    1e84:	6a 00                	push   $0x0
    1e86:	b9 10 00 00 00       	mov    $0x10,%ecx
    1e8b:	8b 45 08             	mov    0x8(%ebp),%eax
    1e8e:	e8 a0 fe ff ff       	call   1d33 <printint>
        ap++;
    1e93:	83 c7 04             	add    $0x4,%edi
    1e96:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    1e99:	83 c4 10             	add    $0x10,%esp
      state = 0;
    1e9c:	be 00 00 00 00       	mov    $0x0,%esi
    1ea1:	e9 39 ff ff ff       	jmp    1ddf <printf+0x2c>
        s = (char*)*ap;
    1ea6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    1ea9:	8b 30                	mov    (%eax),%esi
        ap++;
    1eab:	83 c0 04             	add    $0x4,%eax
    1eae:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
    1eb1:	85 f6                	test   %esi,%esi
    1eb3:	75 28                	jne    1edd <printf+0x12a>
          s = "(null)";
    1eb5:	be 63 2e 00 00       	mov    $0x2e63,%esi
    1eba:	8b 7d 08             	mov    0x8(%ebp),%edi
    1ebd:	eb 0d                	jmp    1ecc <printf+0x119>
          putc(fd, *s);
    1ebf:	0f be d2             	movsbl %dl,%edx
    1ec2:	89 f8                	mov    %edi,%eax
    1ec4:	e8 50 fe ff ff       	call   1d19 <putc>
          s++;
    1ec9:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
    1ecc:	0f b6 16             	movzbl (%esi),%edx
    1ecf:	84 d2                	test   %dl,%dl
    1ed1:	75 ec                	jne    1ebf <printf+0x10c>
      state = 0;
    1ed3:	be 00 00 00 00       	mov    $0x0,%esi
    1ed8:	e9 02 ff ff ff       	jmp    1ddf <printf+0x2c>
    1edd:	8b 7d 08             	mov    0x8(%ebp),%edi
    1ee0:	eb ea                	jmp    1ecc <printf+0x119>
        putc(fd, *ap);
    1ee2:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1ee5:	0f be 17             	movsbl (%edi),%edx
    1ee8:	8b 45 08             	mov    0x8(%ebp),%eax
    1eeb:	e8 29 fe ff ff       	call   1d19 <putc>
        ap++;
    1ef0:	83 c7 04             	add    $0x4,%edi
    1ef3:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
    1ef6:	be 00 00 00 00       	mov    $0x0,%esi
    1efb:	e9 df fe ff ff       	jmp    1ddf <printf+0x2c>
        putc(fd, c);
    1f00:	89 fa                	mov    %edi,%edx
    1f02:	8b 45 08             	mov    0x8(%ebp),%eax
    1f05:	e8 0f fe ff ff       	call   1d19 <putc>
      state = 0;
    1f0a:	be 00 00 00 00       	mov    $0x0,%esi
    1f0f:	e9 cb fe ff ff       	jmp    1ddf <printf+0x2c>
    }
  }
}
    1f14:	8d 65 f4             	lea    -0xc(%ebp),%esp
    1f17:	5b                   	pop    %ebx
    1f18:	5e                   	pop    %esi
    1f19:	5f                   	pop    %edi
    1f1a:	5d                   	pop    %ebp
    1f1b:	c3                   	ret    

00001f1c <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
    1f1c:	55                   	push   %ebp
    1f1d:	89 e5                	mov    %esp,%ebp
    1f1f:	57                   	push   %edi
    1f20:	56                   	push   %esi
    1f21:	53                   	push   %ebx
    1f22:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
    1f25:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1f28:	a1 c0 36 00 00       	mov    0x36c0,%eax
    1f2d:	eb 02                	jmp    1f31 <free+0x15>
    1f2f:	89 d0                	mov    %edx,%eax
    1f31:	39 c8                	cmp    %ecx,%eax
    1f33:	73 04                	jae    1f39 <free+0x1d>
    1f35:	39 08                	cmp    %ecx,(%eax)
    1f37:	77 12                	ja     1f4b <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    1f39:	8b 10                	mov    (%eax),%edx
    1f3b:	39 c2                	cmp    %eax,%edx
    1f3d:	77 f0                	ja     1f2f <free+0x13>
    1f3f:	39 c8                	cmp    %ecx,%eax
    1f41:	72 08                	jb     1f4b <free+0x2f>
    1f43:	39 ca                	cmp    %ecx,%edx
    1f45:	77 04                	ja     1f4b <free+0x2f>
    1f47:	89 d0                	mov    %edx,%eax
    1f49:	eb e6                	jmp    1f31 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
    1f4b:	8b 73 fc             	mov    -0x4(%ebx),%esi
    1f4e:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
    1f51:	8b 10                	mov    (%eax),%edx
    1f53:	39 d7                	cmp    %edx,%edi
    1f55:	74 19                	je     1f70 <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
    1f57:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
    1f5a:	8b 50 04             	mov    0x4(%eax),%edx
    1f5d:	8d 34 d0             	lea    (%eax,%edx,8),%esi
    1f60:	39 ce                	cmp    %ecx,%esi
    1f62:	74 1b                	je     1f7f <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
    1f64:	89 08                	mov    %ecx,(%eax)
  freep = p;
    1f66:	a3 c0 36 00 00       	mov    %eax,0x36c0
}
    1f6b:	5b                   	pop    %ebx
    1f6c:	5e                   	pop    %esi
    1f6d:	5f                   	pop    %edi
    1f6e:	5d                   	pop    %ebp
    1f6f:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
    1f70:	03 72 04             	add    0x4(%edx),%esi
    1f73:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
    1f76:	8b 10                	mov    (%eax),%edx
    1f78:	8b 12                	mov    (%edx),%edx
    1f7a:	89 53 f8             	mov    %edx,-0x8(%ebx)
    1f7d:	eb db                	jmp    1f5a <free+0x3e>
    p->s.size += bp->s.size;
    1f7f:	03 53 fc             	add    -0x4(%ebx),%edx
    1f82:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
    1f85:	8b 53 f8             	mov    -0x8(%ebx),%edx
    1f88:	89 10                	mov    %edx,(%eax)
    1f8a:	eb da                	jmp    1f66 <free+0x4a>

00001f8c <morecore>:

static Header*
morecore(uint nu)
{
    1f8c:	55                   	push   %ebp
    1f8d:	89 e5                	mov    %esp,%ebp
    1f8f:	53                   	push   %ebx
    1f90:	83 ec 04             	sub    $0x4,%esp
    1f93:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
    1f95:	3d ff 0f 00 00       	cmp    $0xfff,%eax
    1f9a:	77 05                	ja     1fa1 <morecore+0x15>
    nu = 4096;
    1f9c:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
    1fa1:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
    1fa8:	83 ec 0c             	sub    $0xc,%esp
    1fab:	50                   	push   %eax
    1fac:	e8 18 fd ff ff       	call   1cc9 <sbrk>
  if(p == (char*)-1)
    1fb1:	83 c4 10             	add    $0x10,%esp
    1fb4:	83 f8 ff             	cmp    $0xffffffff,%eax
    1fb7:	74 1c                	je     1fd5 <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
    1fb9:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
    1fbc:	83 c0 08             	add    $0x8,%eax
    1fbf:	83 ec 0c             	sub    $0xc,%esp
    1fc2:	50                   	push   %eax
    1fc3:	e8 54 ff ff ff       	call   1f1c <free>
  return freep;
    1fc8:	a1 c0 36 00 00       	mov    0x36c0,%eax
    1fcd:	83 c4 10             	add    $0x10,%esp
}
    1fd0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    1fd3:	c9                   	leave  
    1fd4:	c3                   	ret    
    return 0;
    1fd5:	b8 00 00 00 00       	mov    $0x0,%eax
    1fda:	eb f4                	jmp    1fd0 <morecore+0x44>

00001fdc <malloc>:

void*
malloc(uint nbytes)
{
    1fdc:	55                   	push   %ebp
    1fdd:	89 e5                	mov    %esp,%ebp
    1fdf:	53                   	push   %ebx
    1fe0:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
    1fe3:	8b 45 08             	mov    0x8(%ebp),%eax
    1fe6:	8d 58 07             	lea    0x7(%eax),%ebx
    1fe9:	c1 eb 03             	shr    $0x3,%ebx
    1fec:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
    1fef:	8b 0d c0 36 00 00    	mov    0x36c0,%ecx
    1ff5:	85 c9                	test   %ecx,%ecx
    1ff7:	74 04                	je     1ffd <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    1ff9:	8b 01                	mov    (%ecx),%eax
    1ffb:	eb 4d                	jmp    204a <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
    1ffd:	c7 05 c0 36 00 00 c4 	movl   $0x36c4,0x36c0
    2004:	36 00 00 
    2007:	c7 05 c4 36 00 00 c4 	movl   $0x36c4,0x36c4
    200e:	36 00 00 
    base.s.size = 0;
    2011:	c7 05 c8 36 00 00 00 	movl   $0x0,0x36c8
    2018:	00 00 00 
    base.s.ptr = freep = prevp = &base;
    201b:	b9 c4 36 00 00       	mov    $0x36c4,%ecx
    2020:	eb d7                	jmp    1ff9 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
    2022:	39 da                	cmp    %ebx,%edx
    2024:	74 1a                	je     2040 <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
    2026:	29 da                	sub    %ebx,%edx
    2028:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
    202b:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
    202e:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
    2031:	89 0d c0 36 00 00    	mov    %ecx,0x36c0
      return (void*)(p + 1);
    2037:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
    203a:	83 c4 04             	add    $0x4,%esp
    203d:	5b                   	pop    %ebx
    203e:	5d                   	pop    %ebp
    203f:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
    2040:	8b 10                	mov    (%eax),%edx
    2042:	89 11                	mov    %edx,(%ecx)
    2044:	eb eb                	jmp    2031 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    2046:	89 c1                	mov    %eax,%ecx
    2048:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
    204a:	8b 50 04             	mov    0x4(%eax),%edx
    204d:	39 da                	cmp    %ebx,%edx
    204f:	73 d1                	jae    2022 <malloc+0x46>
    if(p == freep)
    2051:	39 05 c0 36 00 00    	cmp    %eax,0x36c0
    2057:	75 ed                	jne    2046 <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
    2059:	89 d8                	mov    %ebx,%eax
    205b:	e8 2c ff ff ff       	call   1f8c <morecore>
    2060:	85 c0                	test   %eax,%eax
    2062:	75 e2                	jne    2046 <malloc+0x6a>
        return 0;
    2064:	b8 00 00 00 00       	mov    $0x0,%eax
    2069:	eb cf                	jmp    203a <malloc+0x5e>
