
_stressfs:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "fs.h"
#include "fcntl.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	56                   	push   %esi
   e:	53                   	push   %ebx
   f:	51                   	push   %ecx
  10:	81 ec 24 02 00 00    	sub    $0x224,%esp
  int fd, i;
  char path[] = "stressfs0";
  16:	c7 45 de 73 74 72 65 	movl   $0x65727473,-0x22(%ebp)
  1d:	c7 45 e2 73 73 66 73 	movl   $0x73667373,-0x1e(%ebp)
  24:	66 c7 45 e6 30 00    	movw   $0x30,-0x1a(%ebp)
  char data[512];

  printf(1, "stressfs starting\n");
  2a:	68 d4 06 00 00       	push   $0x6d4
  2f:	6a 01                	push   $0x1
  31:	e8 e5 03 00 00       	call   41b <printf>
  memset(data, 'a', sizeof(data));
  36:	83 c4 0c             	add    $0xc,%esp
  39:	68 00 02 00 00       	push   $0x200
  3e:	6a 61                	push   $0x61
  40:	8d 85 de fd ff ff    	lea    -0x222(%ebp),%eax
  46:	50                   	push   %eax
  47:	e8 2e 01 00 00       	call   17a <memset>

  for(i = 0; i < 4; i++)
  4c:	83 c4 10             	add    $0x10,%esp
  4f:	bb 00 00 00 00       	mov    $0x0,%ebx
  54:	83 fb 03             	cmp    $0x3,%ebx
  57:	7f 0e                	jg     67 <main+0x67>
    if(fork() > 0)
  59:	e8 43 02 00 00       	call   2a1 <fork>
  5e:	85 c0                	test   %eax,%eax
  60:	7f 05                	jg     67 <main+0x67>
  for(i = 0; i < 4; i++)
  62:	83 c3 01             	add    $0x1,%ebx
  65:	eb ed                	jmp    54 <main+0x54>
      break;

  printf(1, "write %d\n", i);
  67:	83 ec 04             	sub    $0x4,%esp
  6a:	53                   	push   %ebx
  6b:	68 e7 06 00 00       	push   $0x6e7
  70:	6a 01                	push   $0x1
  72:	e8 a4 03 00 00       	call   41b <printf>

  path[8] += i;
  77:	00 5d e6             	add    %bl,-0x1a(%ebp)
  fd = open(path, O_CREATE | O_RDWR);
  7a:	83 c4 08             	add    $0x8,%esp
  7d:	68 02 02 00 00       	push   $0x202
  82:	8d 45 de             	lea    -0x22(%ebp),%eax
  85:	50                   	push   %eax
  86:	e8 5e 02 00 00       	call   2e9 <open>
  8b:	89 c6                	mov    %eax,%esi
  for(i = 0; i < 20; i++)
  8d:	83 c4 10             	add    $0x10,%esp
  90:	bb 00 00 00 00       	mov    $0x0,%ebx
  95:	eb 1b                	jmp    b2 <main+0xb2>
//    printf(fd, "%d\n", i);
    write(fd, data, sizeof(data));
  97:	83 ec 04             	sub    $0x4,%esp
  9a:	68 00 02 00 00       	push   $0x200
  9f:	8d 85 de fd ff ff    	lea    -0x222(%ebp),%eax
  a5:	50                   	push   %eax
  a6:	56                   	push   %esi
  a7:	e8 1d 02 00 00       	call   2c9 <write>
  for(i = 0; i < 20; i++)
  ac:	83 c3 01             	add    $0x1,%ebx
  af:	83 c4 10             	add    $0x10,%esp
  b2:	83 fb 13             	cmp    $0x13,%ebx
  b5:	7e e0                	jle    97 <main+0x97>
  close(fd);
  b7:	83 ec 0c             	sub    $0xc,%esp
  ba:	56                   	push   %esi
  bb:	e8 11 02 00 00       	call   2d1 <close>

  printf(1, "read\n");
  c0:	83 c4 08             	add    $0x8,%esp
  c3:	68 f1 06 00 00       	push   $0x6f1
  c8:	6a 01                	push   $0x1
  ca:	e8 4c 03 00 00       	call   41b <printf>

  fd = open(path, O_RDONLY);
  cf:	83 c4 08             	add    $0x8,%esp
  d2:	6a 00                	push   $0x0
  d4:	8d 45 de             	lea    -0x22(%ebp),%eax
  d7:	50                   	push   %eax
  d8:	e8 0c 02 00 00       	call   2e9 <open>
  dd:	89 c6                	mov    %eax,%esi
  for (i = 0; i < 20; i++)
  df:	83 c4 10             	add    $0x10,%esp
  e2:	bb 00 00 00 00       	mov    $0x0,%ebx
  e7:	eb 1b                	jmp    104 <main+0x104>
    read(fd, data, sizeof(data));
  e9:	83 ec 04             	sub    $0x4,%esp
  ec:	68 00 02 00 00       	push   $0x200
  f1:	8d 85 de fd ff ff    	lea    -0x222(%ebp),%eax
  f7:	50                   	push   %eax
  f8:	56                   	push   %esi
  f9:	e8 c3 01 00 00       	call   2c1 <read>
  for (i = 0; i < 20; i++)
  fe:	83 c3 01             	add    $0x1,%ebx
 101:	83 c4 10             	add    $0x10,%esp
 104:	83 fb 13             	cmp    $0x13,%ebx
 107:	7e e0                	jle    e9 <main+0xe9>
  close(fd);
 109:	83 ec 0c             	sub    $0xc,%esp
 10c:	56                   	push   %esi
 10d:	e8 bf 01 00 00       	call   2d1 <close>

  wait();
 112:	e8 9a 01 00 00       	call   2b1 <wait>

  exit();
 117:	e8 8d 01 00 00       	call   2a9 <exit>

0000011c <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
 11c:	55                   	push   %ebp
 11d:	89 e5                	mov    %esp,%ebp
 11f:	53                   	push   %ebx
 120:	8b 45 08             	mov    0x8(%ebp),%eax
 123:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
 126:	89 c2                	mov    %eax,%edx
 128:	0f b6 19             	movzbl (%ecx),%ebx
 12b:	88 1a                	mov    %bl,(%edx)
 12d:	8d 52 01             	lea    0x1(%edx),%edx
 130:	8d 49 01             	lea    0x1(%ecx),%ecx
 133:	84 db                	test   %bl,%bl
 135:	75 f1                	jne    128 <strcpy+0xc>
    ;
  return os;
}
 137:	5b                   	pop    %ebx
 138:	5d                   	pop    %ebp
 139:	c3                   	ret    

0000013a <strcmp>:

int
strcmp(const char *p, const char *q)
{
 13a:	55                   	push   %ebp
 13b:	89 e5                	mov    %esp,%ebp
 13d:	8b 4d 08             	mov    0x8(%ebp),%ecx
 140:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
 143:	eb 06                	jmp    14b <strcmp+0x11>
    p++, q++;
 145:	83 c1 01             	add    $0x1,%ecx
 148:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
 14b:	0f b6 01             	movzbl (%ecx),%eax
 14e:	84 c0                	test   %al,%al
 150:	74 04                	je     156 <strcmp+0x1c>
 152:	3a 02                	cmp    (%edx),%al
 154:	74 ef                	je     145 <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
 156:	0f b6 c0             	movzbl %al,%eax
 159:	0f b6 12             	movzbl (%edx),%edx
 15c:	29 d0                	sub    %edx,%eax
}
 15e:	5d                   	pop    %ebp
 15f:	c3                   	ret    

00000160 <strlen>:

uint
strlen(const char *s)
{
 160:	55                   	push   %ebp
 161:	89 e5                	mov    %esp,%ebp
 163:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
 166:	ba 00 00 00 00       	mov    $0x0,%edx
 16b:	eb 03                	jmp    170 <strlen+0x10>
 16d:	83 c2 01             	add    $0x1,%edx
 170:	89 d0                	mov    %edx,%eax
 172:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
 176:	75 f5                	jne    16d <strlen+0xd>
    ;
  return n;
}
 178:	5d                   	pop    %ebp
 179:	c3                   	ret    

0000017a <memset>:

void*
memset(void *dst, int c, uint n)
{
 17a:	55                   	push   %ebp
 17b:	89 e5                	mov    %esp,%ebp
 17d:	57                   	push   %edi
 17e:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
 181:	89 d7                	mov    %edx,%edi
 183:	8b 4d 10             	mov    0x10(%ebp),%ecx
 186:	8b 45 0c             	mov    0xc(%ebp),%eax
 189:	fc                   	cld    
 18a:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
 18c:	89 d0                	mov    %edx,%eax
 18e:	5f                   	pop    %edi
 18f:	5d                   	pop    %ebp
 190:	c3                   	ret    

00000191 <strchr>:

char*
strchr(const char *s, char c)
{
 191:	55                   	push   %ebp
 192:	89 e5                	mov    %esp,%ebp
 194:	8b 45 08             	mov    0x8(%ebp),%eax
 197:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 19b:	0f b6 10             	movzbl (%eax),%edx
 19e:	84 d2                	test   %dl,%dl
 1a0:	74 09                	je     1ab <strchr+0x1a>
    if(*s == c)
 1a2:	38 ca                	cmp    %cl,%dl
 1a4:	74 0a                	je     1b0 <strchr+0x1f>
  for(; *s; s++)
 1a6:	83 c0 01             	add    $0x1,%eax
 1a9:	eb f0                	jmp    19b <strchr+0xa>
      return (char*)s;
  return 0;
 1ab:	b8 00 00 00 00       	mov    $0x0,%eax
}
 1b0:	5d                   	pop    %ebp
 1b1:	c3                   	ret    

000001b2 <gets>:

char*
gets(char *buf, int max)
{
 1b2:	55                   	push   %ebp
 1b3:	89 e5                	mov    %esp,%ebp
 1b5:	57                   	push   %edi
 1b6:	56                   	push   %esi
 1b7:	53                   	push   %ebx
 1b8:	83 ec 1c             	sub    $0x1c,%esp
 1bb:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 1be:	bb 00 00 00 00       	mov    $0x0,%ebx
 1c3:	8d 73 01             	lea    0x1(%ebx),%esi
 1c6:	3b 75 0c             	cmp    0xc(%ebp),%esi
 1c9:	7d 2e                	jge    1f9 <gets+0x47>
    cc = read(0, &c, 1);
 1cb:	83 ec 04             	sub    $0x4,%esp
 1ce:	6a 01                	push   $0x1
 1d0:	8d 45 e7             	lea    -0x19(%ebp),%eax
 1d3:	50                   	push   %eax
 1d4:	6a 00                	push   $0x0
 1d6:	e8 e6 00 00 00       	call   2c1 <read>
    if(cc < 1)
 1db:	83 c4 10             	add    $0x10,%esp
 1de:	85 c0                	test   %eax,%eax
 1e0:	7e 17                	jle    1f9 <gets+0x47>
      break;
    buf[i++] = c;
 1e2:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 1e6:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 1e9:	3c 0a                	cmp    $0xa,%al
 1eb:	0f 94 c2             	sete   %dl
 1ee:	3c 0d                	cmp    $0xd,%al
 1f0:	0f 94 c0             	sete   %al
    buf[i++] = c;
 1f3:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 1f5:	08 c2                	or     %al,%dl
 1f7:	74 ca                	je     1c3 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 1f9:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 1fd:	89 f8                	mov    %edi,%eax
 1ff:	8d 65 f4             	lea    -0xc(%ebp),%esp
 202:	5b                   	pop    %ebx
 203:	5e                   	pop    %esi
 204:	5f                   	pop    %edi
 205:	5d                   	pop    %ebp
 206:	c3                   	ret    

00000207 <stat>:

int
stat(const char *n, struct stat *st)
{
 207:	55                   	push   %ebp
 208:	89 e5                	mov    %esp,%ebp
 20a:	56                   	push   %esi
 20b:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 20c:	83 ec 08             	sub    $0x8,%esp
 20f:	6a 00                	push   $0x0
 211:	ff 75 08             	pushl  0x8(%ebp)
 214:	e8 d0 00 00 00       	call   2e9 <open>
  if(fd < 0)
 219:	83 c4 10             	add    $0x10,%esp
 21c:	85 c0                	test   %eax,%eax
 21e:	78 24                	js     244 <stat+0x3d>
 220:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 222:	83 ec 08             	sub    $0x8,%esp
 225:	ff 75 0c             	pushl  0xc(%ebp)
 228:	50                   	push   %eax
 229:	e8 d3 00 00 00       	call   301 <fstat>
 22e:	89 c6                	mov    %eax,%esi
  close(fd);
 230:	89 1c 24             	mov    %ebx,(%esp)
 233:	e8 99 00 00 00       	call   2d1 <close>
  return r;
 238:	83 c4 10             	add    $0x10,%esp
}
 23b:	89 f0                	mov    %esi,%eax
 23d:	8d 65 f8             	lea    -0x8(%ebp),%esp
 240:	5b                   	pop    %ebx
 241:	5e                   	pop    %esi
 242:	5d                   	pop    %ebp
 243:	c3                   	ret    
    return -1;
 244:	be ff ff ff ff       	mov    $0xffffffff,%esi
 249:	eb f0                	jmp    23b <stat+0x34>

0000024b <atoi>:

int
atoi(const char *s)
{
 24b:	55                   	push   %ebp
 24c:	89 e5                	mov    %esp,%ebp
 24e:	53                   	push   %ebx
 24f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 252:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 257:	eb 10                	jmp    269 <atoi+0x1e>
    n = n*10 + *s++ - '0';
 259:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 25c:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 25f:	83 c1 01             	add    $0x1,%ecx
 262:	0f be d2             	movsbl %dl,%edx
 265:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 269:	0f b6 11             	movzbl (%ecx),%edx
 26c:	8d 5a d0             	lea    -0x30(%edx),%ebx
 26f:	80 fb 09             	cmp    $0x9,%bl
 272:	76 e5                	jbe    259 <atoi+0xe>
  return n;
}
 274:	5b                   	pop    %ebx
 275:	5d                   	pop    %ebp
 276:	c3                   	ret    

00000277 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 277:	55                   	push   %ebp
 278:	89 e5                	mov    %esp,%ebp
 27a:	56                   	push   %esi
 27b:	53                   	push   %ebx
 27c:	8b 45 08             	mov    0x8(%ebp),%eax
 27f:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 282:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 285:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 287:	eb 0d                	jmp    296 <memmove+0x1f>
    *dst++ = *src++;
 289:	0f b6 13             	movzbl (%ebx),%edx
 28c:	88 11                	mov    %dl,(%ecx)
 28e:	8d 5b 01             	lea    0x1(%ebx),%ebx
 291:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 294:	89 f2                	mov    %esi,%edx
 296:	8d 72 ff             	lea    -0x1(%edx),%esi
 299:	85 d2                	test   %edx,%edx
 29b:	7f ec                	jg     289 <memmove+0x12>
  return vdst;
}
 29d:	5b                   	pop    %ebx
 29e:	5e                   	pop    %esi
 29f:	5d                   	pop    %ebp
 2a0:	c3                   	ret    

000002a1 <fork>:
 2a1:	b8 01 00 00 00       	mov    $0x1,%eax
 2a6:	cd 40                	int    $0x40
 2a8:	c3                   	ret    

000002a9 <exit>:
 2a9:	b8 02 00 00 00       	mov    $0x2,%eax
 2ae:	cd 40                	int    $0x40
 2b0:	c3                   	ret    

000002b1 <wait>:
 2b1:	b8 03 00 00 00       	mov    $0x3,%eax
 2b6:	cd 40                	int    $0x40
 2b8:	c3                   	ret    

000002b9 <pipe>:
 2b9:	b8 04 00 00 00       	mov    $0x4,%eax
 2be:	cd 40                	int    $0x40
 2c0:	c3                   	ret    

000002c1 <read>:
 2c1:	b8 05 00 00 00       	mov    $0x5,%eax
 2c6:	cd 40                	int    $0x40
 2c8:	c3                   	ret    

000002c9 <write>:
 2c9:	b8 10 00 00 00       	mov    $0x10,%eax
 2ce:	cd 40                	int    $0x40
 2d0:	c3                   	ret    

000002d1 <close>:
 2d1:	b8 15 00 00 00       	mov    $0x15,%eax
 2d6:	cd 40                	int    $0x40
 2d8:	c3                   	ret    

000002d9 <kill>:
 2d9:	b8 06 00 00 00       	mov    $0x6,%eax
 2de:	cd 40                	int    $0x40
 2e0:	c3                   	ret    

000002e1 <exec>:
 2e1:	b8 07 00 00 00       	mov    $0x7,%eax
 2e6:	cd 40                	int    $0x40
 2e8:	c3                   	ret    

000002e9 <open>:
 2e9:	b8 0f 00 00 00       	mov    $0xf,%eax
 2ee:	cd 40                	int    $0x40
 2f0:	c3                   	ret    

000002f1 <mknod>:
 2f1:	b8 11 00 00 00       	mov    $0x11,%eax
 2f6:	cd 40                	int    $0x40
 2f8:	c3                   	ret    

000002f9 <unlink>:
 2f9:	b8 12 00 00 00       	mov    $0x12,%eax
 2fe:	cd 40                	int    $0x40
 300:	c3                   	ret    

00000301 <fstat>:
 301:	b8 08 00 00 00       	mov    $0x8,%eax
 306:	cd 40                	int    $0x40
 308:	c3                   	ret    

00000309 <link>:
 309:	b8 13 00 00 00       	mov    $0x13,%eax
 30e:	cd 40                	int    $0x40
 310:	c3                   	ret    

00000311 <mkdir>:
 311:	b8 14 00 00 00       	mov    $0x14,%eax
 316:	cd 40                	int    $0x40
 318:	c3                   	ret    

00000319 <chdir>:
 319:	b8 09 00 00 00       	mov    $0x9,%eax
 31e:	cd 40                	int    $0x40
 320:	c3                   	ret    

00000321 <dup>:
 321:	b8 0a 00 00 00       	mov    $0xa,%eax
 326:	cd 40                	int    $0x40
 328:	c3                   	ret    

00000329 <getpid>:
 329:	b8 0b 00 00 00       	mov    $0xb,%eax
 32e:	cd 40                	int    $0x40
 330:	c3                   	ret    

00000331 <sbrk>:
 331:	b8 0c 00 00 00       	mov    $0xc,%eax
 336:	cd 40                	int    $0x40
 338:	c3                   	ret    

00000339 <sleep>:
 339:	b8 0d 00 00 00       	mov    $0xd,%eax
 33e:	cd 40                	int    $0x40
 340:	c3                   	ret    

00000341 <uptime>:
 341:	b8 0e 00 00 00       	mov    $0xe,%eax
 346:	cd 40                	int    $0x40
 348:	c3                   	ret    

00000349 <yield>:
 349:	b8 16 00 00 00       	mov    $0x16,%eax
 34e:	cd 40                	int    $0x40
 350:	c3                   	ret    

00000351 <shutdown>:
 351:	b8 17 00 00 00       	mov    $0x17,%eax
 356:	cd 40                	int    $0x40
 358:	c3                   	ret    

00000359 <writecount>:
 359:	b8 18 00 00 00       	mov    $0x18,%eax
 35e:	cd 40                	int    $0x40
 360:	c3                   	ret    

00000361 <setwritecount>:
 361:	b8 19 00 00 00       	mov    $0x19,%eax
 366:	cd 40                	int    $0x40
 368:	c3                   	ret    

00000369 <settickets>:
 369:	b8 1a 00 00 00       	mov    $0x1a,%eax
 36e:	cd 40                	int    $0x40
 370:	c3                   	ret    

00000371 <getprocessesinfo>:
 371:	b8 1b 00 00 00       	mov    $0x1b,%eax
 376:	cd 40                	int    $0x40
 378:	c3                   	ret    

00000379 <dumppagetable>:
 379:	b8 1c 00 00 00       	mov    $0x1c,%eax
 37e:	cd 40                	int    $0x40
 380:	c3                   	ret    

00000381 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 381:	55                   	push   %ebp
 382:	89 e5                	mov    %esp,%ebp
 384:	83 ec 1c             	sub    $0x1c,%esp
 387:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 38a:	6a 01                	push   $0x1
 38c:	8d 55 f4             	lea    -0xc(%ebp),%edx
 38f:	52                   	push   %edx
 390:	50                   	push   %eax
 391:	e8 33 ff ff ff       	call   2c9 <write>
}
 396:	83 c4 10             	add    $0x10,%esp
 399:	c9                   	leave  
 39a:	c3                   	ret    

0000039b <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 39b:	55                   	push   %ebp
 39c:	89 e5                	mov    %esp,%ebp
 39e:	57                   	push   %edi
 39f:	56                   	push   %esi
 3a0:	53                   	push   %ebx
 3a1:	83 ec 2c             	sub    $0x2c,%esp
 3a4:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 3a6:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 3aa:	0f 95 c3             	setne  %bl
 3ad:	89 d0                	mov    %edx,%eax
 3af:	c1 e8 1f             	shr    $0x1f,%eax
 3b2:	84 c3                	test   %al,%bl
 3b4:	74 10                	je     3c6 <printint+0x2b>
    neg = 1;
    x = -xx;
 3b6:	f7 da                	neg    %edx
    neg = 1;
 3b8:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 3bf:	be 00 00 00 00       	mov    $0x0,%esi
 3c4:	eb 0b                	jmp    3d1 <printint+0x36>
  neg = 0;
 3c6:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 3cd:	eb f0                	jmp    3bf <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 3cf:	89 c6                	mov    %eax,%esi
 3d1:	89 d0                	mov    %edx,%eax
 3d3:	ba 00 00 00 00       	mov    $0x0,%edx
 3d8:	f7 f1                	div    %ecx
 3da:	89 c3                	mov    %eax,%ebx
 3dc:	8d 46 01             	lea    0x1(%esi),%eax
 3df:	0f b6 92 00 07 00 00 	movzbl 0x700(%edx),%edx
 3e6:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 3ea:	89 da                	mov    %ebx,%edx
 3ec:	85 db                	test   %ebx,%ebx
 3ee:	75 df                	jne    3cf <printint+0x34>
 3f0:	89 c3                	mov    %eax,%ebx
  if(neg)
 3f2:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 3f6:	74 16                	je     40e <printint+0x73>
    buf[i++] = '-';
 3f8:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 3fd:	8d 5e 02             	lea    0x2(%esi),%ebx
 400:	eb 0c                	jmp    40e <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 402:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 407:	89 f8                	mov    %edi,%eax
 409:	e8 73 ff ff ff       	call   381 <putc>
  while(--i >= 0)
 40e:	83 eb 01             	sub    $0x1,%ebx
 411:	79 ef                	jns    402 <printint+0x67>
}
 413:	83 c4 2c             	add    $0x2c,%esp
 416:	5b                   	pop    %ebx
 417:	5e                   	pop    %esi
 418:	5f                   	pop    %edi
 419:	5d                   	pop    %ebp
 41a:	c3                   	ret    

0000041b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 41b:	55                   	push   %ebp
 41c:	89 e5                	mov    %esp,%ebp
 41e:	57                   	push   %edi
 41f:	56                   	push   %esi
 420:	53                   	push   %ebx
 421:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 424:	8d 45 10             	lea    0x10(%ebp),%eax
 427:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 42a:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 42f:	bb 00 00 00 00       	mov    $0x0,%ebx
 434:	eb 14                	jmp    44a <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 436:	89 fa                	mov    %edi,%edx
 438:	8b 45 08             	mov    0x8(%ebp),%eax
 43b:	e8 41 ff ff ff       	call   381 <putc>
 440:	eb 05                	jmp    447 <printf+0x2c>
      }
    } else if(state == '%'){
 442:	83 fe 25             	cmp    $0x25,%esi
 445:	74 25                	je     46c <printf+0x51>
  for(i = 0; fmt[i]; i++){
 447:	83 c3 01             	add    $0x1,%ebx
 44a:	8b 45 0c             	mov    0xc(%ebp),%eax
 44d:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 451:	84 c0                	test   %al,%al
 453:	0f 84 23 01 00 00    	je     57c <printf+0x161>
    c = fmt[i] & 0xff;
 459:	0f be f8             	movsbl %al,%edi
 45c:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 45f:	85 f6                	test   %esi,%esi
 461:	75 df                	jne    442 <printf+0x27>
      if(c == '%'){
 463:	83 f8 25             	cmp    $0x25,%eax
 466:	75 ce                	jne    436 <printf+0x1b>
        state = '%';
 468:	89 c6                	mov    %eax,%esi
 46a:	eb db                	jmp    447 <printf+0x2c>
      if(c == 'd'){
 46c:	83 f8 64             	cmp    $0x64,%eax
 46f:	74 49                	je     4ba <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 471:	83 f8 78             	cmp    $0x78,%eax
 474:	0f 94 c1             	sete   %cl
 477:	83 f8 70             	cmp    $0x70,%eax
 47a:	0f 94 c2             	sete   %dl
 47d:	08 d1                	or     %dl,%cl
 47f:	75 63                	jne    4e4 <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 481:	83 f8 73             	cmp    $0x73,%eax
 484:	0f 84 84 00 00 00    	je     50e <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 48a:	83 f8 63             	cmp    $0x63,%eax
 48d:	0f 84 b7 00 00 00    	je     54a <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 493:	83 f8 25             	cmp    $0x25,%eax
 496:	0f 84 cc 00 00 00    	je     568 <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 49c:	ba 25 00 00 00       	mov    $0x25,%edx
 4a1:	8b 45 08             	mov    0x8(%ebp),%eax
 4a4:	e8 d8 fe ff ff       	call   381 <putc>
        putc(fd, c);
 4a9:	89 fa                	mov    %edi,%edx
 4ab:	8b 45 08             	mov    0x8(%ebp),%eax
 4ae:	e8 ce fe ff ff       	call   381 <putc>
      }
      state = 0;
 4b3:	be 00 00 00 00       	mov    $0x0,%esi
 4b8:	eb 8d                	jmp    447 <printf+0x2c>
        printint(fd, *ap, 10, 1);
 4ba:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4bd:	8b 17                	mov    (%edi),%edx
 4bf:	83 ec 0c             	sub    $0xc,%esp
 4c2:	6a 01                	push   $0x1
 4c4:	b9 0a 00 00 00       	mov    $0xa,%ecx
 4c9:	8b 45 08             	mov    0x8(%ebp),%eax
 4cc:	e8 ca fe ff ff       	call   39b <printint>
        ap++;
 4d1:	83 c7 04             	add    $0x4,%edi
 4d4:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 4d7:	83 c4 10             	add    $0x10,%esp
      state = 0;
 4da:	be 00 00 00 00       	mov    $0x0,%esi
 4df:	e9 63 ff ff ff       	jmp    447 <printf+0x2c>
        printint(fd, *ap, 16, 0);
 4e4:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4e7:	8b 17                	mov    (%edi),%edx
 4e9:	83 ec 0c             	sub    $0xc,%esp
 4ec:	6a 00                	push   $0x0
 4ee:	b9 10 00 00 00       	mov    $0x10,%ecx
 4f3:	8b 45 08             	mov    0x8(%ebp),%eax
 4f6:	e8 a0 fe ff ff       	call   39b <printint>
        ap++;
 4fb:	83 c7 04             	add    $0x4,%edi
 4fe:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 501:	83 c4 10             	add    $0x10,%esp
      state = 0;
 504:	be 00 00 00 00       	mov    $0x0,%esi
 509:	e9 39 ff ff ff       	jmp    447 <printf+0x2c>
        s = (char*)*ap;
 50e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 511:	8b 30                	mov    (%eax),%esi
        ap++;
 513:	83 c0 04             	add    $0x4,%eax
 516:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 519:	85 f6                	test   %esi,%esi
 51b:	75 28                	jne    545 <printf+0x12a>
          s = "(null)";
 51d:	be f7 06 00 00       	mov    $0x6f7,%esi
 522:	8b 7d 08             	mov    0x8(%ebp),%edi
 525:	eb 0d                	jmp    534 <printf+0x119>
          putc(fd, *s);
 527:	0f be d2             	movsbl %dl,%edx
 52a:	89 f8                	mov    %edi,%eax
 52c:	e8 50 fe ff ff       	call   381 <putc>
          s++;
 531:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 534:	0f b6 16             	movzbl (%esi),%edx
 537:	84 d2                	test   %dl,%dl
 539:	75 ec                	jne    527 <printf+0x10c>
      state = 0;
 53b:	be 00 00 00 00       	mov    $0x0,%esi
 540:	e9 02 ff ff ff       	jmp    447 <printf+0x2c>
 545:	8b 7d 08             	mov    0x8(%ebp),%edi
 548:	eb ea                	jmp    534 <printf+0x119>
        putc(fd, *ap);
 54a:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 54d:	0f be 17             	movsbl (%edi),%edx
 550:	8b 45 08             	mov    0x8(%ebp),%eax
 553:	e8 29 fe ff ff       	call   381 <putc>
        ap++;
 558:	83 c7 04             	add    $0x4,%edi
 55b:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 55e:	be 00 00 00 00       	mov    $0x0,%esi
 563:	e9 df fe ff ff       	jmp    447 <printf+0x2c>
        putc(fd, c);
 568:	89 fa                	mov    %edi,%edx
 56a:	8b 45 08             	mov    0x8(%ebp),%eax
 56d:	e8 0f fe ff ff       	call   381 <putc>
      state = 0;
 572:	be 00 00 00 00       	mov    $0x0,%esi
 577:	e9 cb fe ff ff       	jmp    447 <printf+0x2c>
    }
  }
}
 57c:	8d 65 f4             	lea    -0xc(%ebp),%esp
 57f:	5b                   	pop    %ebx
 580:	5e                   	pop    %esi
 581:	5f                   	pop    %edi
 582:	5d                   	pop    %ebp
 583:	c3                   	ret    

00000584 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 584:	55                   	push   %ebp
 585:	89 e5                	mov    %esp,%ebp
 587:	57                   	push   %edi
 588:	56                   	push   %esi
 589:	53                   	push   %ebx
 58a:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 58d:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 590:	a1 a0 09 00 00       	mov    0x9a0,%eax
 595:	eb 02                	jmp    599 <free+0x15>
 597:	89 d0                	mov    %edx,%eax
 599:	39 c8                	cmp    %ecx,%eax
 59b:	73 04                	jae    5a1 <free+0x1d>
 59d:	39 08                	cmp    %ecx,(%eax)
 59f:	77 12                	ja     5b3 <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 5a1:	8b 10                	mov    (%eax),%edx
 5a3:	39 c2                	cmp    %eax,%edx
 5a5:	77 f0                	ja     597 <free+0x13>
 5a7:	39 c8                	cmp    %ecx,%eax
 5a9:	72 08                	jb     5b3 <free+0x2f>
 5ab:	39 ca                	cmp    %ecx,%edx
 5ad:	77 04                	ja     5b3 <free+0x2f>
 5af:	89 d0                	mov    %edx,%eax
 5b1:	eb e6                	jmp    599 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 5b3:	8b 73 fc             	mov    -0x4(%ebx),%esi
 5b6:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 5b9:	8b 10                	mov    (%eax),%edx
 5bb:	39 d7                	cmp    %edx,%edi
 5bd:	74 19                	je     5d8 <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 5bf:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 5c2:	8b 50 04             	mov    0x4(%eax),%edx
 5c5:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 5c8:	39 ce                	cmp    %ecx,%esi
 5ca:	74 1b                	je     5e7 <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 5cc:	89 08                	mov    %ecx,(%eax)
  freep = p;
 5ce:	a3 a0 09 00 00       	mov    %eax,0x9a0
}
 5d3:	5b                   	pop    %ebx
 5d4:	5e                   	pop    %esi
 5d5:	5f                   	pop    %edi
 5d6:	5d                   	pop    %ebp
 5d7:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 5d8:	03 72 04             	add    0x4(%edx),%esi
 5db:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 5de:	8b 10                	mov    (%eax),%edx
 5e0:	8b 12                	mov    (%edx),%edx
 5e2:	89 53 f8             	mov    %edx,-0x8(%ebx)
 5e5:	eb db                	jmp    5c2 <free+0x3e>
    p->s.size += bp->s.size;
 5e7:	03 53 fc             	add    -0x4(%ebx),%edx
 5ea:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 5ed:	8b 53 f8             	mov    -0x8(%ebx),%edx
 5f0:	89 10                	mov    %edx,(%eax)
 5f2:	eb da                	jmp    5ce <free+0x4a>

000005f4 <morecore>:

static Header*
morecore(uint nu)
{
 5f4:	55                   	push   %ebp
 5f5:	89 e5                	mov    %esp,%ebp
 5f7:	53                   	push   %ebx
 5f8:	83 ec 04             	sub    $0x4,%esp
 5fb:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 5fd:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 602:	77 05                	ja     609 <morecore+0x15>
    nu = 4096;
 604:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 609:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 610:	83 ec 0c             	sub    $0xc,%esp
 613:	50                   	push   %eax
 614:	e8 18 fd ff ff       	call   331 <sbrk>
  if(p == (char*)-1)
 619:	83 c4 10             	add    $0x10,%esp
 61c:	83 f8 ff             	cmp    $0xffffffff,%eax
 61f:	74 1c                	je     63d <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 621:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 624:	83 c0 08             	add    $0x8,%eax
 627:	83 ec 0c             	sub    $0xc,%esp
 62a:	50                   	push   %eax
 62b:	e8 54 ff ff ff       	call   584 <free>
  return freep;
 630:	a1 a0 09 00 00       	mov    0x9a0,%eax
 635:	83 c4 10             	add    $0x10,%esp
}
 638:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 63b:	c9                   	leave  
 63c:	c3                   	ret    
    return 0;
 63d:	b8 00 00 00 00       	mov    $0x0,%eax
 642:	eb f4                	jmp    638 <morecore+0x44>

00000644 <malloc>:

void*
malloc(uint nbytes)
{
 644:	55                   	push   %ebp
 645:	89 e5                	mov    %esp,%ebp
 647:	53                   	push   %ebx
 648:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 64b:	8b 45 08             	mov    0x8(%ebp),%eax
 64e:	8d 58 07             	lea    0x7(%eax),%ebx
 651:	c1 eb 03             	shr    $0x3,%ebx
 654:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 657:	8b 0d a0 09 00 00    	mov    0x9a0,%ecx
 65d:	85 c9                	test   %ecx,%ecx
 65f:	74 04                	je     665 <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 661:	8b 01                	mov    (%ecx),%eax
 663:	eb 4d                	jmp    6b2 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 665:	c7 05 a0 09 00 00 a4 	movl   $0x9a4,0x9a0
 66c:	09 00 00 
 66f:	c7 05 a4 09 00 00 a4 	movl   $0x9a4,0x9a4
 676:	09 00 00 
    base.s.size = 0;
 679:	c7 05 a8 09 00 00 00 	movl   $0x0,0x9a8
 680:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 683:	b9 a4 09 00 00       	mov    $0x9a4,%ecx
 688:	eb d7                	jmp    661 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 68a:	39 da                	cmp    %ebx,%edx
 68c:	74 1a                	je     6a8 <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 68e:	29 da                	sub    %ebx,%edx
 690:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 693:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 696:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 699:	89 0d a0 09 00 00    	mov    %ecx,0x9a0
      return (void*)(p + 1);
 69f:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 6a2:	83 c4 04             	add    $0x4,%esp
 6a5:	5b                   	pop    %ebx
 6a6:	5d                   	pop    %ebp
 6a7:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 6a8:	8b 10                	mov    (%eax),%edx
 6aa:	89 11                	mov    %edx,(%ecx)
 6ac:	eb eb                	jmp    699 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 6ae:	89 c1                	mov    %eax,%ecx
 6b0:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 6b2:	8b 50 04             	mov    0x4(%eax),%edx
 6b5:	39 da                	cmp    %ebx,%edx
 6b7:	73 d1                	jae    68a <malloc+0x46>
    if(p == freep)
 6b9:	39 05 a0 09 00 00    	cmp    %eax,0x9a0
 6bf:	75 ed                	jne    6ae <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 6c1:	89 d8                	mov    %ebx,%eax
 6c3:	e8 2c ff ff ff       	call   5f4 <morecore>
 6c8:	85 c0                	test   %eax,%eax
 6ca:	75 e2                	jne    6ae <malloc+0x6a>
        return 0;
 6cc:	b8 00 00 00 00       	mov    $0x0,%eax
 6d1:	eb cf                	jmp    6a2 <malloc+0x5e>
