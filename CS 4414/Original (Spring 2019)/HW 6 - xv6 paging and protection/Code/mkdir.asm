
_mkdir:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	57                   	push   %edi
   e:	56                   	push   %esi
   f:	53                   	push   %ebx
  10:	51                   	push   %ecx
  11:	83 ec 18             	sub    $0x18,%esp
  14:	8b 01                	mov    (%ecx),%eax
  16:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  19:	8b 79 04             	mov    0x4(%ecx),%edi
  int i;

  if(argc < 2){
  1c:	83 f8 01             	cmp    $0x1,%eax
  1f:	7e 23                	jle    44 <main+0x44>
    printf(2, "Usage: mkdir files...\n");
    exit();
  }

  for(i = 1; i < argc; i++){
  21:	bb 01 00 00 00       	mov    $0x1,%ebx
  26:	3b 5d e4             	cmp    -0x1c(%ebp),%ebx
  29:	7d 41                	jge    6c <main+0x6c>
    if(mkdir(argv[i]) < 0){
  2b:	8d 34 9f             	lea    (%edi,%ebx,4),%esi
  2e:	83 ec 0c             	sub    $0xc,%esp
  31:	ff 36                	pushl  (%esi)
  33:	e8 2e 02 00 00       	call   266 <mkdir>
  38:	83 c4 10             	add    $0x10,%esp
  3b:	85 c0                	test   %eax,%eax
  3d:	78 19                	js     58 <main+0x58>
  for(i = 1; i < argc; i++){
  3f:	83 c3 01             	add    $0x1,%ebx
  42:	eb e2                	jmp    26 <main+0x26>
    printf(2, "Usage: mkdir files...\n");
  44:	83 ec 08             	sub    $0x8,%esp
  47:	68 28 06 00 00       	push   $0x628
  4c:	6a 02                	push   $0x2
  4e:	e8 1d 03 00 00       	call   370 <printf>
    exit();
  53:	e8 a6 01 00 00       	call   1fe <exit>
      printf(2, "mkdir: %s failed to create\n", argv[i]);
  58:	83 ec 04             	sub    $0x4,%esp
  5b:	ff 36                	pushl  (%esi)
  5d:	68 3f 06 00 00       	push   $0x63f
  62:	6a 02                	push   $0x2
  64:	e8 07 03 00 00       	call   370 <printf>
      break;
  69:	83 c4 10             	add    $0x10,%esp
    }
  }

  exit();
  6c:	e8 8d 01 00 00       	call   1fe <exit>

00000071 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  71:	55                   	push   %ebp
  72:	89 e5                	mov    %esp,%ebp
  74:	53                   	push   %ebx
  75:	8b 45 08             	mov    0x8(%ebp),%eax
  78:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  7b:	89 c2                	mov    %eax,%edx
  7d:	0f b6 19             	movzbl (%ecx),%ebx
  80:	88 1a                	mov    %bl,(%edx)
  82:	8d 52 01             	lea    0x1(%edx),%edx
  85:	8d 49 01             	lea    0x1(%ecx),%ecx
  88:	84 db                	test   %bl,%bl
  8a:	75 f1                	jne    7d <strcpy+0xc>
    ;
  return os;
}
  8c:	5b                   	pop    %ebx
  8d:	5d                   	pop    %ebp
  8e:	c3                   	ret    

0000008f <strcmp>:

int
strcmp(const char *p, const char *q)
{
  8f:	55                   	push   %ebp
  90:	89 e5                	mov    %esp,%ebp
  92:	8b 4d 08             	mov    0x8(%ebp),%ecx
  95:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  98:	eb 06                	jmp    a0 <strcmp+0x11>
    p++, q++;
  9a:	83 c1 01             	add    $0x1,%ecx
  9d:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  a0:	0f b6 01             	movzbl (%ecx),%eax
  a3:	84 c0                	test   %al,%al
  a5:	74 04                	je     ab <strcmp+0x1c>
  a7:	3a 02                	cmp    (%edx),%al
  a9:	74 ef                	je     9a <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
  ab:	0f b6 c0             	movzbl %al,%eax
  ae:	0f b6 12             	movzbl (%edx),%edx
  b1:	29 d0                	sub    %edx,%eax
}
  b3:	5d                   	pop    %ebp
  b4:	c3                   	ret    

000000b5 <strlen>:

uint
strlen(const char *s)
{
  b5:	55                   	push   %ebp
  b6:	89 e5                	mov    %esp,%ebp
  b8:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
  bb:	ba 00 00 00 00       	mov    $0x0,%edx
  c0:	eb 03                	jmp    c5 <strlen+0x10>
  c2:	83 c2 01             	add    $0x1,%edx
  c5:	89 d0                	mov    %edx,%eax
  c7:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  cb:	75 f5                	jne    c2 <strlen+0xd>
    ;
  return n;
}
  cd:	5d                   	pop    %ebp
  ce:	c3                   	ret    

000000cf <memset>:

void*
memset(void *dst, int c, uint n)
{
  cf:	55                   	push   %ebp
  d0:	89 e5                	mov    %esp,%ebp
  d2:	57                   	push   %edi
  d3:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
  d6:	89 d7                	mov    %edx,%edi
  d8:	8b 4d 10             	mov    0x10(%ebp),%ecx
  db:	8b 45 0c             	mov    0xc(%ebp),%eax
  de:	fc                   	cld    
  df:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
  e1:	89 d0                	mov    %edx,%eax
  e3:	5f                   	pop    %edi
  e4:	5d                   	pop    %ebp
  e5:	c3                   	ret    

000000e6 <strchr>:

char*
strchr(const char *s, char c)
{
  e6:	55                   	push   %ebp
  e7:	89 e5                	mov    %esp,%ebp
  e9:	8b 45 08             	mov    0x8(%ebp),%eax
  ec:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
  f0:	0f b6 10             	movzbl (%eax),%edx
  f3:	84 d2                	test   %dl,%dl
  f5:	74 09                	je     100 <strchr+0x1a>
    if(*s == c)
  f7:	38 ca                	cmp    %cl,%dl
  f9:	74 0a                	je     105 <strchr+0x1f>
  for(; *s; s++)
  fb:	83 c0 01             	add    $0x1,%eax
  fe:	eb f0                	jmp    f0 <strchr+0xa>
      return (char*)s;
  return 0;
 100:	b8 00 00 00 00       	mov    $0x0,%eax
}
 105:	5d                   	pop    %ebp
 106:	c3                   	ret    

00000107 <gets>:

char*
gets(char *buf, int max)
{
 107:	55                   	push   %ebp
 108:	89 e5                	mov    %esp,%ebp
 10a:	57                   	push   %edi
 10b:	56                   	push   %esi
 10c:	53                   	push   %ebx
 10d:	83 ec 1c             	sub    $0x1c,%esp
 110:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 113:	bb 00 00 00 00       	mov    $0x0,%ebx
 118:	8d 73 01             	lea    0x1(%ebx),%esi
 11b:	3b 75 0c             	cmp    0xc(%ebp),%esi
 11e:	7d 2e                	jge    14e <gets+0x47>
    cc = read(0, &c, 1);
 120:	83 ec 04             	sub    $0x4,%esp
 123:	6a 01                	push   $0x1
 125:	8d 45 e7             	lea    -0x19(%ebp),%eax
 128:	50                   	push   %eax
 129:	6a 00                	push   $0x0
 12b:	e8 e6 00 00 00       	call   216 <read>
    if(cc < 1)
 130:	83 c4 10             	add    $0x10,%esp
 133:	85 c0                	test   %eax,%eax
 135:	7e 17                	jle    14e <gets+0x47>
      break;
    buf[i++] = c;
 137:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 13b:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 13e:	3c 0a                	cmp    $0xa,%al
 140:	0f 94 c2             	sete   %dl
 143:	3c 0d                	cmp    $0xd,%al
 145:	0f 94 c0             	sete   %al
    buf[i++] = c;
 148:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 14a:	08 c2                	or     %al,%dl
 14c:	74 ca                	je     118 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 14e:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 152:	89 f8                	mov    %edi,%eax
 154:	8d 65 f4             	lea    -0xc(%ebp),%esp
 157:	5b                   	pop    %ebx
 158:	5e                   	pop    %esi
 159:	5f                   	pop    %edi
 15a:	5d                   	pop    %ebp
 15b:	c3                   	ret    

0000015c <stat>:

int
stat(const char *n, struct stat *st)
{
 15c:	55                   	push   %ebp
 15d:	89 e5                	mov    %esp,%ebp
 15f:	56                   	push   %esi
 160:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 161:	83 ec 08             	sub    $0x8,%esp
 164:	6a 00                	push   $0x0
 166:	ff 75 08             	pushl  0x8(%ebp)
 169:	e8 d0 00 00 00       	call   23e <open>
  if(fd < 0)
 16e:	83 c4 10             	add    $0x10,%esp
 171:	85 c0                	test   %eax,%eax
 173:	78 24                	js     199 <stat+0x3d>
 175:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 177:	83 ec 08             	sub    $0x8,%esp
 17a:	ff 75 0c             	pushl  0xc(%ebp)
 17d:	50                   	push   %eax
 17e:	e8 d3 00 00 00       	call   256 <fstat>
 183:	89 c6                	mov    %eax,%esi
  close(fd);
 185:	89 1c 24             	mov    %ebx,(%esp)
 188:	e8 99 00 00 00       	call   226 <close>
  return r;
 18d:	83 c4 10             	add    $0x10,%esp
}
 190:	89 f0                	mov    %esi,%eax
 192:	8d 65 f8             	lea    -0x8(%ebp),%esp
 195:	5b                   	pop    %ebx
 196:	5e                   	pop    %esi
 197:	5d                   	pop    %ebp
 198:	c3                   	ret    
    return -1;
 199:	be ff ff ff ff       	mov    $0xffffffff,%esi
 19e:	eb f0                	jmp    190 <stat+0x34>

000001a0 <atoi>:

int
atoi(const char *s)
{
 1a0:	55                   	push   %ebp
 1a1:	89 e5                	mov    %esp,%ebp
 1a3:	53                   	push   %ebx
 1a4:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 1a7:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 1ac:	eb 10                	jmp    1be <atoi+0x1e>
    n = n*10 + *s++ - '0';
 1ae:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 1b1:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 1b4:	83 c1 01             	add    $0x1,%ecx
 1b7:	0f be d2             	movsbl %dl,%edx
 1ba:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 1be:	0f b6 11             	movzbl (%ecx),%edx
 1c1:	8d 5a d0             	lea    -0x30(%edx),%ebx
 1c4:	80 fb 09             	cmp    $0x9,%bl
 1c7:	76 e5                	jbe    1ae <atoi+0xe>
  return n;
}
 1c9:	5b                   	pop    %ebx
 1ca:	5d                   	pop    %ebp
 1cb:	c3                   	ret    

000001cc <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 1cc:	55                   	push   %ebp
 1cd:	89 e5                	mov    %esp,%ebp
 1cf:	56                   	push   %esi
 1d0:	53                   	push   %ebx
 1d1:	8b 45 08             	mov    0x8(%ebp),%eax
 1d4:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 1d7:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 1da:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 1dc:	eb 0d                	jmp    1eb <memmove+0x1f>
    *dst++ = *src++;
 1de:	0f b6 13             	movzbl (%ebx),%edx
 1e1:	88 11                	mov    %dl,(%ecx)
 1e3:	8d 5b 01             	lea    0x1(%ebx),%ebx
 1e6:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 1e9:	89 f2                	mov    %esi,%edx
 1eb:	8d 72 ff             	lea    -0x1(%edx),%esi
 1ee:	85 d2                	test   %edx,%edx
 1f0:	7f ec                	jg     1de <memmove+0x12>
  return vdst;
}
 1f2:	5b                   	pop    %ebx
 1f3:	5e                   	pop    %esi
 1f4:	5d                   	pop    %ebp
 1f5:	c3                   	ret    

000001f6 <fork>:
 1f6:	b8 01 00 00 00       	mov    $0x1,%eax
 1fb:	cd 40                	int    $0x40
 1fd:	c3                   	ret    

000001fe <exit>:
 1fe:	b8 02 00 00 00       	mov    $0x2,%eax
 203:	cd 40                	int    $0x40
 205:	c3                   	ret    

00000206 <wait>:
 206:	b8 03 00 00 00       	mov    $0x3,%eax
 20b:	cd 40                	int    $0x40
 20d:	c3                   	ret    

0000020e <pipe>:
 20e:	b8 04 00 00 00       	mov    $0x4,%eax
 213:	cd 40                	int    $0x40
 215:	c3                   	ret    

00000216 <read>:
 216:	b8 05 00 00 00       	mov    $0x5,%eax
 21b:	cd 40                	int    $0x40
 21d:	c3                   	ret    

0000021e <write>:
 21e:	b8 10 00 00 00       	mov    $0x10,%eax
 223:	cd 40                	int    $0x40
 225:	c3                   	ret    

00000226 <close>:
 226:	b8 15 00 00 00       	mov    $0x15,%eax
 22b:	cd 40                	int    $0x40
 22d:	c3                   	ret    

0000022e <kill>:
 22e:	b8 06 00 00 00       	mov    $0x6,%eax
 233:	cd 40                	int    $0x40
 235:	c3                   	ret    

00000236 <exec>:
 236:	b8 07 00 00 00       	mov    $0x7,%eax
 23b:	cd 40                	int    $0x40
 23d:	c3                   	ret    

0000023e <open>:
 23e:	b8 0f 00 00 00       	mov    $0xf,%eax
 243:	cd 40                	int    $0x40
 245:	c3                   	ret    

00000246 <mknod>:
 246:	b8 11 00 00 00       	mov    $0x11,%eax
 24b:	cd 40                	int    $0x40
 24d:	c3                   	ret    

0000024e <unlink>:
 24e:	b8 12 00 00 00       	mov    $0x12,%eax
 253:	cd 40                	int    $0x40
 255:	c3                   	ret    

00000256 <fstat>:
 256:	b8 08 00 00 00       	mov    $0x8,%eax
 25b:	cd 40                	int    $0x40
 25d:	c3                   	ret    

0000025e <link>:
 25e:	b8 13 00 00 00       	mov    $0x13,%eax
 263:	cd 40                	int    $0x40
 265:	c3                   	ret    

00000266 <mkdir>:
 266:	b8 14 00 00 00       	mov    $0x14,%eax
 26b:	cd 40                	int    $0x40
 26d:	c3                   	ret    

0000026e <chdir>:
 26e:	b8 09 00 00 00       	mov    $0x9,%eax
 273:	cd 40                	int    $0x40
 275:	c3                   	ret    

00000276 <dup>:
 276:	b8 0a 00 00 00       	mov    $0xa,%eax
 27b:	cd 40                	int    $0x40
 27d:	c3                   	ret    

0000027e <getpid>:
 27e:	b8 0b 00 00 00       	mov    $0xb,%eax
 283:	cd 40                	int    $0x40
 285:	c3                   	ret    

00000286 <sbrk>:
 286:	b8 0c 00 00 00       	mov    $0xc,%eax
 28b:	cd 40                	int    $0x40
 28d:	c3                   	ret    

0000028e <sleep>:
 28e:	b8 0d 00 00 00       	mov    $0xd,%eax
 293:	cd 40                	int    $0x40
 295:	c3                   	ret    

00000296 <uptime>:
 296:	b8 0e 00 00 00       	mov    $0xe,%eax
 29b:	cd 40                	int    $0x40
 29d:	c3                   	ret    

0000029e <yield>:
 29e:	b8 16 00 00 00       	mov    $0x16,%eax
 2a3:	cd 40                	int    $0x40
 2a5:	c3                   	ret    

000002a6 <shutdown>:
 2a6:	b8 17 00 00 00       	mov    $0x17,%eax
 2ab:	cd 40                	int    $0x40
 2ad:	c3                   	ret    

000002ae <writecount>:
 2ae:	b8 18 00 00 00       	mov    $0x18,%eax
 2b3:	cd 40                	int    $0x40
 2b5:	c3                   	ret    

000002b6 <setwritecount>:
 2b6:	b8 19 00 00 00       	mov    $0x19,%eax
 2bb:	cd 40                	int    $0x40
 2bd:	c3                   	ret    

000002be <settickets>:
 2be:	b8 1a 00 00 00       	mov    $0x1a,%eax
 2c3:	cd 40                	int    $0x40
 2c5:	c3                   	ret    

000002c6 <getprocessesinfo>:
 2c6:	b8 1b 00 00 00       	mov    $0x1b,%eax
 2cb:	cd 40                	int    $0x40
 2cd:	c3                   	ret    

000002ce <dumppagetable>:
 2ce:	b8 1c 00 00 00       	mov    $0x1c,%eax
 2d3:	cd 40                	int    $0x40
 2d5:	c3                   	ret    

000002d6 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 2d6:	55                   	push   %ebp
 2d7:	89 e5                	mov    %esp,%ebp
 2d9:	83 ec 1c             	sub    $0x1c,%esp
 2dc:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 2df:	6a 01                	push   $0x1
 2e1:	8d 55 f4             	lea    -0xc(%ebp),%edx
 2e4:	52                   	push   %edx
 2e5:	50                   	push   %eax
 2e6:	e8 33 ff ff ff       	call   21e <write>
}
 2eb:	83 c4 10             	add    $0x10,%esp
 2ee:	c9                   	leave  
 2ef:	c3                   	ret    

000002f0 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 2f0:	55                   	push   %ebp
 2f1:	89 e5                	mov    %esp,%ebp
 2f3:	57                   	push   %edi
 2f4:	56                   	push   %esi
 2f5:	53                   	push   %ebx
 2f6:	83 ec 2c             	sub    $0x2c,%esp
 2f9:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 2fb:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 2ff:	0f 95 c3             	setne  %bl
 302:	89 d0                	mov    %edx,%eax
 304:	c1 e8 1f             	shr    $0x1f,%eax
 307:	84 c3                	test   %al,%bl
 309:	74 10                	je     31b <printint+0x2b>
    neg = 1;
    x = -xx;
 30b:	f7 da                	neg    %edx
    neg = 1;
 30d:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 314:	be 00 00 00 00       	mov    $0x0,%esi
 319:	eb 0b                	jmp    326 <printint+0x36>
  neg = 0;
 31b:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 322:	eb f0                	jmp    314 <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 324:	89 c6                	mov    %eax,%esi
 326:	89 d0                	mov    %edx,%eax
 328:	ba 00 00 00 00       	mov    $0x0,%edx
 32d:	f7 f1                	div    %ecx
 32f:	89 c3                	mov    %eax,%ebx
 331:	8d 46 01             	lea    0x1(%esi),%eax
 334:	0f b6 92 64 06 00 00 	movzbl 0x664(%edx),%edx
 33b:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 33f:	89 da                	mov    %ebx,%edx
 341:	85 db                	test   %ebx,%ebx
 343:	75 df                	jne    324 <printint+0x34>
 345:	89 c3                	mov    %eax,%ebx
  if(neg)
 347:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 34b:	74 16                	je     363 <printint+0x73>
    buf[i++] = '-';
 34d:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 352:	8d 5e 02             	lea    0x2(%esi),%ebx
 355:	eb 0c                	jmp    363 <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 357:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 35c:	89 f8                	mov    %edi,%eax
 35e:	e8 73 ff ff ff       	call   2d6 <putc>
  while(--i >= 0)
 363:	83 eb 01             	sub    $0x1,%ebx
 366:	79 ef                	jns    357 <printint+0x67>
}
 368:	83 c4 2c             	add    $0x2c,%esp
 36b:	5b                   	pop    %ebx
 36c:	5e                   	pop    %esi
 36d:	5f                   	pop    %edi
 36e:	5d                   	pop    %ebp
 36f:	c3                   	ret    

00000370 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 370:	55                   	push   %ebp
 371:	89 e5                	mov    %esp,%ebp
 373:	57                   	push   %edi
 374:	56                   	push   %esi
 375:	53                   	push   %ebx
 376:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 379:	8d 45 10             	lea    0x10(%ebp),%eax
 37c:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 37f:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 384:	bb 00 00 00 00       	mov    $0x0,%ebx
 389:	eb 14                	jmp    39f <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 38b:	89 fa                	mov    %edi,%edx
 38d:	8b 45 08             	mov    0x8(%ebp),%eax
 390:	e8 41 ff ff ff       	call   2d6 <putc>
 395:	eb 05                	jmp    39c <printf+0x2c>
      }
    } else if(state == '%'){
 397:	83 fe 25             	cmp    $0x25,%esi
 39a:	74 25                	je     3c1 <printf+0x51>
  for(i = 0; fmt[i]; i++){
 39c:	83 c3 01             	add    $0x1,%ebx
 39f:	8b 45 0c             	mov    0xc(%ebp),%eax
 3a2:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 3a6:	84 c0                	test   %al,%al
 3a8:	0f 84 23 01 00 00    	je     4d1 <printf+0x161>
    c = fmt[i] & 0xff;
 3ae:	0f be f8             	movsbl %al,%edi
 3b1:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 3b4:	85 f6                	test   %esi,%esi
 3b6:	75 df                	jne    397 <printf+0x27>
      if(c == '%'){
 3b8:	83 f8 25             	cmp    $0x25,%eax
 3bb:	75 ce                	jne    38b <printf+0x1b>
        state = '%';
 3bd:	89 c6                	mov    %eax,%esi
 3bf:	eb db                	jmp    39c <printf+0x2c>
      if(c == 'd'){
 3c1:	83 f8 64             	cmp    $0x64,%eax
 3c4:	74 49                	je     40f <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 3c6:	83 f8 78             	cmp    $0x78,%eax
 3c9:	0f 94 c1             	sete   %cl
 3cc:	83 f8 70             	cmp    $0x70,%eax
 3cf:	0f 94 c2             	sete   %dl
 3d2:	08 d1                	or     %dl,%cl
 3d4:	75 63                	jne    439 <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 3d6:	83 f8 73             	cmp    $0x73,%eax
 3d9:	0f 84 84 00 00 00    	je     463 <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 3df:	83 f8 63             	cmp    $0x63,%eax
 3e2:	0f 84 b7 00 00 00    	je     49f <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 3e8:	83 f8 25             	cmp    $0x25,%eax
 3eb:	0f 84 cc 00 00 00    	je     4bd <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 3f1:	ba 25 00 00 00       	mov    $0x25,%edx
 3f6:	8b 45 08             	mov    0x8(%ebp),%eax
 3f9:	e8 d8 fe ff ff       	call   2d6 <putc>
        putc(fd, c);
 3fe:	89 fa                	mov    %edi,%edx
 400:	8b 45 08             	mov    0x8(%ebp),%eax
 403:	e8 ce fe ff ff       	call   2d6 <putc>
      }
      state = 0;
 408:	be 00 00 00 00       	mov    $0x0,%esi
 40d:	eb 8d                	jmp    39c <printf+0x2c>
        printint(fd, *ap, 10, 1);
 40f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 412:	8b 17                	mov    (%edi),%edx
 414:	83 ec 0c             	sub    $0xc,%esp
 417:	6a 01                	push   $0x1
 419:	b9 0a 00 00 00       	mov    $0xa,%ecx
 41e:	8b 45 08             	mov    0x8(%ebp),%eax
 421:	e8 ca fe ff ff       	call   2f0 <printint>
        ap++;
 426:	83 c7 04             	add    $0x4,%edi
 429:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 42c:	83 c4 10             	add    $0x10,%esp
      state = 0;
 42f:	be 00 00 00 00       	mov    $0x0,%esi
 434:	e9 63 ff ff ff       	jmp    39c <printf+0x2c>
        printint(fd, *ap, 16, 0);
 439:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 43c:	8b 17                	mov    (%edi),%edx
 43e:	83 ec 0c             	sub    $0xc,%esp
 441:	6a 00                	push   $0x0
 443:	b9 10 00 00 00       	mov    $0x10,%ecx
 448:	8b 45 08             	mov    0x8(%ebp),%eax
 44b:	e8 a0 fe ff ff       	call   2f0 <printint>
        ap++;
 450:	83 c7 04             	add    $0x4,%edi
 453:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 456:	83 c4 10             	add    $0x10,%esp
      state = 0;
 459:	be 00 00 00 00       	mov    $0x0,%esi
 45e:	e9 39 ff ff ff       	jmp    39c <printf+0x2c>
        s = (char*)*ap;
 463:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 466:	8b 30                	mov    (%eax),%esi
        ap++;
 468:	83 c0 04             	add    $0x4,%eax
 46b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 46e:	85 f6                	test   %esi,%esi
 470:	75 28                	jne    49a <printf+0x12a>
          s = "(null)";
 472:	be 5b 06 00 00       	mov    $0x65b,%esi
 477:	8b 7d 08             	mov    0x8(%ebp),%edi
 47a:	eb 0d                	jmp    489 <printf+0x119>
          putc(fd, *s);
 47c:	0f be d2             	movsbl %dl,%edx
 47f:	89 f8                	mov    %edi,%eax
 481:	e8 50 fe ff ff       	call   2d6 <putc>
          s++;
 486:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 489:	0f b6 16             	movzbl (%esi),%edx
 48c:	84 d2                	test   %dl,%dl
 48e:	75 ec                	jne    47c <printf+0x10c>
      state = 0;
 490:	be 00 00 00 00       	mov    $0x0,%esi
 495:	e9 02 ff ff ff       	jmp    39c <printf+0x2c>
 49a:	8b 7d 08             	mov    0x8(%ebp),%edi
 49d:	eb ea                	jmp    489 <printf+0x119>
        putc(fd, *ap);
 49f:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4a2:	0f be 17             	movsbl (%edi),%edx
 4a5:	8b 45 08             	mov    0x8(%ebp),%eax
 4a8:	e8 29 fe ff ff       	call   2d6 <putc>
        ap++;
 4ad:	83 c7 04             	add    $0x4,%edi
 4b0:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 4b3:	be 00 00 00 00       	mov    $0x0,%esi
 4b8:	e9 df fe ff ff       	jmp    39c <printf+0x2c>
        putc(fd, c);
 4bd:	89 fa                	mov    %edi,%edx
 4bf:	8b 45 08             	mov    0x8(%ebp),%eax
 4c2:	e8 0f fe ff ff       	call   2d6 <putc>
      state = 0;
 4c7:	be 00 00 00 00       	mov    $0x0,%esi
 4cc:	e9 cb fe ff ff       	jmp    39c <printf+0x2c>
    }
  }
}
 4d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4d4:	5b                   	pop    %ebx
 4d5:	5e                   	pop    %esi
 4d6:	5f                   	pop    %edi
 4d7:	5d                   	pop    %ebp
 4d8:	c3                   	ret    

000004d9 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 4d9:	55                   	push   %ebp
 4da:	89 e5                	mov    %esp,%ebp
 4dc:	57                   	push   %edi
 4dd:	56                   	push   %esi
 4de:	53                   	push   %ebx
 4df:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 4e2:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 4e5:	a1 08 09 00 00       	mov    0x908,%eax
 4ea:	eb 02                	jmp    4ee <free+0x15>
 4ec:	89 d0                	mov    %edx,%eax
 4ee:	39 c8                	cmp    %ecx,%eax
 4f0:	73 04                	jae    4f6 <free+0x1d>
 4f2:	39 08                	cmp    %ecx,(%eax)
 4f4:	77 12                	ja     508 <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 4f6:	8b 10                	mov    (%eax),%edx
 4f8:	39 c2                	cmp    %eax,%edx
 4fa:	77 f0                	ja     4ec <free+0x13>
 4fc:	39 c8                	cmp    %ecx,%eax
 4fe:	72 08                	jb     508 <free+0x2f>
 500:	39 ca                	cmp    %ecx,%edx
 502:	77 04                	ja     508 <free+0x2f>
 504:	89 d0                	mov    %edx,%eax
 506:	eb e6                	jmp    4ee <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 508:	8b 73 fc             	mov    -0x4(%ebx),%esi
 50b:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 50e:	8b 10                	mov    (%eax),%edx
 510:	39 d7                	cmp    %edx,%edi
 512:	74 19                	je     52d <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 514:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 517:	8b 50 04             	mov    0x4(%eax),%edx
 51a:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 51d:	39 ce                	cmp    %ecx,%esi
 51f:	74 1b                	je     53c <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 521:	89 08                	mov    %ecx,(%eax)
  freep = p;
 523:	a3 08 09 00 00       	mov    %eax,0x908
}
 528:	5b                   	pop    %ebx
 529:	5e                   	pop    %esi
 52a:	5f                   	pop    %edi
 52b:	5d                   	pop    %ebp
 52c:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 52d:	03 72 04             	add    0x4(%edx),%esi
 530:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 533:	8b 10                	mov    (%eax),%edx
 535:	8b 12                	mov    (%edx),%edx
 537:	89 53 f8             	mov    %edx,-0x8(%ebx)
 53a:	eb db                	jmp    517 <free+0x3e>
    p->s.size += bp->s.size;
 53c:	03 53 fc             	add    -0x4(%ebx),%edx
 53f:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 542:	8b 53 f8             	mov    -0x8(%ebx),%edx
 545:	89 10                	mov    %edx,(%eax)
 547:	eb da                	jmp    523 <free+0x4a>

00000549 <morecore>:

static Header*
morecore(uint nu)
{
 549:	55                   	push   %ebp
 54a:	89 e5                	mov    %esp,%ebp
 54c:	53                   	push   %ebx
 54d:	83 ec 04             	sub    $0x4,%esp
 550:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 552:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 557:	77 05                	ja     55e <morecore+0x15>
    nu = 4096;
 559:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 55e:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 565:	83 ec 0c             	sub    $0xc,%esp
 568:	50                   	push   %eax
 569:	e8 18 fd ff ff       	call   286 <sbrk>
  if(p == (char*)-1)
 56e:	83 c4 10             	add    $0x10,%esp
 571:	83 f8 ff             	cmp    $0xffffffff,%eax
 574:	74 1c                	je     592 <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 576:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 579:	83 c0 08             	add    $0x8,%eax
 57c:	83 ec 0c             	sub    $0xc,%esp
 57f:	50                   	push   %eax
 580:	e8 54 ff ff ff       	call   4d9 <free>
  return freep;
 585:	a1 08 09 00 00       	mov    0x908,%eax
 58a:	83 c4 10             	add    $0x10,%esp
}
 58d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 590:	c9                   	leave  
 591:	c3                   	ret    
    return 0;
 592:	b8 00 00 00 00       	mov    $0x0,%eax
 597:	eb f4                	jmp    58d <morecore+0x44>

00000599 <malloc>:

void*
malloc(uint nbytes)
{
 599:	55                   	push   %ebp
 59a:	89 e5                	mov    %esp,%ebp
 59c:	53                   	push   %ebx
 59d:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 5a0:	8b 45 08             	mov    0x8(%ebp),%eax
 5a3:	8d 58 07             	lea    0x7(%eax),%ebx
 5a6:	c1 eb 03             	shr    $0x3,%ebx
 5a9:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 5ac:	8b 0d 08 09 00 00    	mov    0x908,%ecx
 5b2:	85 c9                	test   %ecx,%ecx
 5b4:	74 04                	je     5ba <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 5b6:	8b 01                	mov    (%ecx),%eax
 5b8:	eb 4d                	jmp    607 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 5ba:	c7 05 08 09 00 00 0c 	movl   $0x90c,0x908
 5c1:	09 00 00 
 5c4:	c7 05 0c 09 00 00 0c 	movl   $0x90c,0x90c
 5cb:	09 00 00 
    base.s.size = 0;
 5ce:	c7 05 10 09 00 00 00 	movl   $0x0,0x910
 5d5:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 5d8:	b9 0c 09 00 00       	mov    $0x90c,%ecx
 5dd:	eb d7                	jmp    5b6 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 5df:	39 da                	cmp    %ebx,%edx
 5e1:	74 1a                	je     5fd <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 5e3:	29 da                	sub    %ebx,%edx
 5e5:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 5e8:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 5eb:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 5ee:	89 0d 08 09 00 00    	mov    %ecx,0x908
      return (void*)(p + 1);
 5f4:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 5f7:	83 c4 04             	add    $0x4,%esp
 5fa:	5b                   	pop    %ebx
 5fb:	5d                   	pop    %ebp
 5fc:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 5fd:	8b 10                	mov    (%eax),%edx
 5ff:	89 11                	mov    %edx,(%ecx)
 601:	eb eb                	jmp    5ee <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 603:	89 c1                	mov    %eax,%ecx
 605:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 607:	8b 50 04             	mov    0x4(%eax),%edx
 60a:	39 da                	cmp    %ebx,%edx
 60c:	73 d1                	jae    5df <malloc+0x46>
    if(p == freep)
 60e:	39 05 08 09 00 00    	cmp    %eax,0x908
 614:	75 ed                	jne    603 <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 616:	89 d8                	mov    %ebx,%eax
 618:	e8 2c ff ff ff       	call   549 <morecore>
 61d:	85 c0                	test   %eax,%eax
 61f:	75 e2                	jne    603 <malloc+0x6a>
        return 0;
 621:	b8 00 00 00 00       	mov    $0x0,%eax
 626:	eb cf                	jmp    5f7 <malloc+0x5e>
