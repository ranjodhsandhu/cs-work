
_printtest:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 04             	sub    $0x4,%esp
  printf(1, "Write count: %d\n", writecount());
  11:	e8 b2 02 00 00       	call   2c8 <writecount>
  16:	83 ec 04             	sub    $0x4,%esp
  19:	50                   	push   %eax
  1a:	68 44 06 00 00       	push   $0x644
  1f:	6a 01                	push   $0x1
  21:	e8 64 03 00 00       	call   38a <printf>
  setwritecount(21);
  26:	c7 04 24 15 00 00 00 	movl   $0x15,(%esp)
  2d:	e8 9e 02 00 00       	call   2d0 <setwritecount>
  printf(1, "Write count: %d\n", writecount());
  32:	e8 91 02 00 00       	call   2c8 <writecount>
  37:	83 c4 0c             	add    $0xc,%esp
  3a:	50                   	push   %eax
  3b:	68 44 06 00 00       	push   $0x644
  40:	6a 01                	push   $0x1
  42:	e8 43 03 00 00       	call   38a <printf>
  printf(1, "Write count: %d\n", writecount());
  47:	e8 7c 02 00 00       	call   2c8 <writecount>
  4c:	83 c4 0c             	add    $0xc,%esp
  4f:	50                   	push   %eax
  50:	68 44 06 00 00       	push   $0x644
  55:	6a 01                	push   $0x1
  57:	e8 2e 03 00 00       	call   38a <printf>
  printf(1, "Write count: %d\n", writecount());
  5c:	e8 67 02 00 00       	call   2c8 <writecount>
  61:	83 c4 0c             	add    $0xc,%esp
  64:	50                   	push   %eax
  65:	68 44 06 00 00       	push   $0x644
  6a:	6a 01                	push   $0x1
  6c:	e8 19 03 00 00       	call   38a <printf>
  printf(1, "Write count: %d\n", writecount());
  71:	e8 52 02 00 00       	call   2c8 <writecount>
  76:	83 c4 0c             	add    $0xc,%esp
  79:	50                   	push   %eax
  7a:	68 44 06 00 00       	push   $0x644
  7f:	6a 01                	push   $0x1
  81:	e8 04 03 00 00       	call   38a <printf>
  exit();
  86:	e8 8d 01 00 00       	call   218 <exit>

0000008b <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  8b:	55                   	push   %ebp
  8c:	89 e5                	mov    %esp,%ebp
  8e:	53                   	push   %ebx
  8f:	8b 45 08             	mov    0x8(%ebp),%eax
  92:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  95:	89 c2                	mov    %eax,%edx
  97:	0f b6 19             	movzbl (%ecx),%ebx
  9a:	88 1a                	mov    %bl,(%edx)
  9c:	8d 52 01             	lea    0x1(%edx),%edx
  9f:	8d 49 01             	lea    0x1(%ecx),%ecx
  a2:	84 db                	test   %bl,%bl
  a4:	75 f1                	jne    97 <strcpy+0xc>
    ;
  return os;
}
  a6:	5b                   	pop    %ebx
  a7:	5d                   	pop    %ebp
  a8:	c3                   	ret    

000000a9 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  a9:	55                   	push   %ebp
  aa:	89 e5                	mov    %esp,%ebp
  ac:	8b 4d 08             	mov    0x8(%ebp),%ecx
  af:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  b2:	eb 06                	jmp    ba <strcmp+0x11>
    p++, q++;
  b4:	83 c1 01             	add    $0x1,%ecx
  b7:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  ba:	0f b6 01             	movzbl (%ecx),%eax
  bd:	84 c0                	test   %al,%al
  bf:	74 04                	je     c5 <strcmp+0x1c>
  c1:	3a 02                	cmp    (%edx),%al
  c3:	74 ef                	je     b4 <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
  c5:	0f b6 c0             	movzbl %al,%eax
  c8:	0f b6 12             	movzbl (%edx),%edx
  cb:	29 d0                	sub    %edx,%eax
}
  cd:	5d                   	pop    %ebp
  ce:	c3                   	ret    

000000cf <strlen>:

uint
strlen(const char *s)
{
  cf:	55                   	push   %ebp
  d0:	89 e5                	mov    %esp,%ebp
  d2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
  d5:	ba 00 00 00 00       	mov    $0x0,%edx
  da:	eb 03                	jmp    df <strlen+0x10>
  dc:	83 c2 01             	add    $0x1,%edx
  df:	89 d0                	mov    %edx,%eax
  e1:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  e5:	75 f5                	jne    dc <strlen+0xd>
    ;
  return n;
}
  e7:	5d                   	pop    %ebp
  e8:	c3                   	ret    

000000e9 <memset>:

void*
memset(void *dst, int c, uint n)
{
  e9:	55                   	push   %ebp
  ea:	89 e5                	mov    %esp,%ebp
  ec:	57                   	push   %edi
  ed:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
  f0:	89 d7                	mov    %edx,%edi
  f2:	8b 4d 10             	mov    0x10(%ebp),%ecx
  f5:	8b 45 0c             	mov    0xc(%ebp),%eax
  f8:	fc                   	cld    
  f9:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
  fb:	89 d0                	mov    %edx,%eax
  fd:	5f                   	pop    %edi
  fe:	5d                   	pop    %ebp
  ff:	c3                   	ret    

00000100 <strchr>:

char*
strchr(const char *s, char c)
{
 100:	55                   	push   %ebp
 101:	89 e5                	mov    %esp,%ebp
 103:	8b 45 08             	mov    0x8(%ebp),%eax
 106:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 10a:	0f b6 10             	movzbl (%eax),%edx
 10d:	84 d2                	test   %dl,%dl
 10f:	74 09                	je     11a <strchr+0x1a>
    if(*s == c)
 111:	38 ca                	cmp    %cl,%dl
 113:	74 0a                	je     11f <strchr+0x1f>
  for(; *s; s++)
 115:	83 c0 01             	add    $0x1,%eax
 118:	eb f0                	jmp    10a <strchr+0xa>
      return (char*)s;
  return 0;
 11a:	b8 00 00 00 00       	mov    $0x0,%eax
}
 11f:	5d                   	pop    %ebp
 120:	c3                   	ret    

00000121 <gets>:

char*
gets(char *buf, int max)
{
 121:	55                   	push   %ebp
 122:	89 e5                	mov    %esp,%ebp
 124:	57                   	push   %edi
 125:	56                   	push   %esi
 126:	53                   	push   %ebx
 127:	83 ec 1c             	sub    $0x1c,%esp
 12a:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 12d:	bb 00 00 00 00       	mov    $0x0,%ebx
 132:	8d 73 01             	lea    0x1(%ebx),%esi
 135:	3b 75 0c             	cmp    0xc(%ebp),%esi
 138:	7d 2e                	jge    168 <gets+0x47>
    cc = read(0, &c, 1);
 13a:	83 ec 04             	sub    $0x4,%esp
 13d:	6a 01                	push   $0x1
 13f:	8d 45 e7             	lea    -0x19(%ebp),%eax
 142:	50                   	push   %eax
 143:	6a 00                	push   $0x0
 145:	e8 e6 00 00 00       	call   230 <read>
    if(cc < 1)
 14a:	83 c4 10             	add    $0x10,%esp
 14d:	85 c0                	test   %eax,%eax
 14f:	7e 17                	jle    168 <gets+0x47>
      break;
    buf[i++] = c;
 151:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 155:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 158:	3c 0a                	cmp    $0xa,%al
 15a:	0f 94 c2             	sete   %dl
 15d:	3c 0d                	cmp    $0xd,%al
 15f:	0f 94 c0             	sete   %al
    buf[i++] = c;
 162:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 164:	08 c2                	or     %al,%dl
 166:	74 ca                	je     132 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 168:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 16c:	89 f8                	mov    %edi,%eax
 16e:	8d 65 f4             	lea    -0xc(%ebp),%esp
 171:	5b                   	pop    %ebx
 172:	5e                   	pop    %esi
 173:	5f                   	pop    %edi
 174:	5d                   	pop    %ebp
 175:	c3                   	ret    

00000176 <stat>:

int
stat(const char *n, struct stat *st)
{
 176:	55                   	push   %ebp
 177:	89 e5                	mov    %esp,%ebp
 179:	56                   	push   %esi
 17a:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 17b:	83 ec 08             	sub    $0x8,%esp
 17e:	6a 00                	push   $0x0
 180:	ff 75 08             	pushl  0x8(%ebp)
 183:	e8 d0 00 00 00       	call   258 <open>
  if(fd < 0)
 188:	83 c4 10             	add    $0x10,%esp
 18b:	85 c0                	test   %eax,%eax
 18d:	78 24                	js     1b3 <stat+0x3d>
 18f:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 191:	83 ec 08             	sub    $0x8,%esp
 194:	ff 75 0c             	pushl  0xc(%ebp)
 197:	50                   	push   %eax
 198:	e8 d3 00 00 00       	call   270 <fstat>
 19d:	89 c6                	mov    %eax,%esi
  close(fd);
 19f:	89 1c 24             	mov    %ebx,(%esp)
 1a2:	e8 99 00 00 00       	call   240 <close>
  return r;
 1a7:	83 c4 10             	add    $0x10,%esp
}
 1aa:	89 f0                	mov    %esi,%eax
 1ac:	8d 65 f8             	lea    -0x8(%ebp),%esp
 1af:	5b                   	pop    %ebx
 1b0:	5e                   	pop    %esi
 1b1:	5d                   	pop    %ebp
 1b2:	c3                   	ret    
    return -1;
 1b3:	be ff ff ff ff       	mov    $0xffffffff,%esi
 1b8:	eb f0                	jmp    1aa <stat+0x34>

000001ba <atoi>:

int
atoi(const char *s)
{
 1ba:	55                   	push   %ebp
 1bb:	89 e5                	mov    %esp,%ebp
 1bd:	53                   	push   %ebx
 1be:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 1c1:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 1c6:	eb 10                	jmp    1d8 <atoi+0x1e>
    n = n*10 + *s++ - '0';
 1c8:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 1cb:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 1ce:	83 c1 01             	add    $0x1,%ecx
 1d1:	0f be d2             	movsbl %dl,%edx
 1d4:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 1d8:	0f b6 11             	movzbl (%ecx),%edx
 1db:	8d 5a d0             	lea    -0x30(%edx),%ebx
 1de:	80 fb 09             	cmp    $0x9,%bl
 1e1:	76 e5                	jbe    1c8 <atoi+0xe>
  return n;
}
 1e3:	5b                   	pop    %ebx
 1e4:	5d                   	pop    %ebp
 1e5:	c3                   	ret    

000001e6 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 1e6:	55                   	push   %ebp
 1e7:	89 e5                	mov    %esp,%ebp
 1e9:	56                   	push   %esi
 1ea:	53                   	push   %ebx
 1eb:	8b 45 08             	mov    0x8(%ebp),%eax
 1ee:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 1f1:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 1f4:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 1f6:	eb 0d                	jmp    205 <memmove+0x1f>
    *dst++ = *src++;
 1f8:	0f b6 13             	movzbl (%ebx),%edx
 1fb:	88 11                	mov    %dl,(%ecx)
 1fd:	8d 5b 01             	lea    0x1(%ebx),%ebx
 200:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 203:	89 f2                	mov    %esi,%edx
 205:	8d 72 ff             	lea    -0x1(%edx),%esi
 208:	85 d2                	test   %edx,%edx
 20a:	7f ec                	jg     1f8 <memmove+0x12>
  return vdst;
}
 20c:	5b                   	pop    %ebx
 20d:	5e                   	pop    %esi
 20e:	5d                   	pop    %ebp
 20f:	c3                   	ret    

00000210 <fork>:
 210:	b8 01 00 00 00       	mov    $0x1,%eax
 215:	cd 40                	int    $0x40
 217:	c3                   	ret    

00000218 <exit>:
 218:	b8 02 00 00 00       	mov    $0x2,%eax
 21d:	cd 40                	int    $0x40
 21f:	c3                   	ret    

00000220 <wait>:
 220:	b8 03 00 00 00       	mov    $0x3,%eax
 225:	cd 40                	int    $0x40
 227:	c3                   	ret    

00000228 <pipe>:
 228:	b8 04 00 00 00       	mov    $0x4,%eax
 22d:	cd 40                	int    $0x40
 22f:	c3                   	ret    

00000230 <read>:
 230:	b8 05 00 00 00       	mov    $0x5,%eax
 235:	cd 40                	int    $0x40
 237:	c3                   	ret    

00000238 <write>:
 238:	b8 10 00 00 00       	mov    $0x10,%eax
 23d:	cd 40                	int    $0x40
 23f:	c3                   	ret    

00000240 <close>:
 240:	b8 15 00 00 00       	mov    $0x15,%eax
 245:	cd 40                	int    $0x40
 247:	c3                   	ret    

00000248 <kill>:
 248:	b8 06 00 00 00       	mov    $0x6,%eax
 24d:	cd 40                	int    $0x40
 24f:	c3                   	ret    

00000250 <exec>:
 250:	b8 07 00 00 00       	mov    $0x7,%eax
 255:	cd 40                	int    $0x40
 257:	c3                   	ret    

00000258 <open>:
 258:	b8 0f 00 00 00       	mov    $0xf,%eax
 25d:	cd 40                	int    $0x40
 25f:	c3                   	ret    

00000260 <mknod>:
 260:	b8 11 00 00 00       	mov    $0x11,%eax
 265:	cd 40                	int    $0x40
 267:	c3                   	ret    

00000268 <unlink>:
 268:	b8 12 00 00 00       	mov    $0x12,%eax
 26d:	cd 40                	int    $0x40
 26f:	c3                   	ret    

00000270 <fstat>:
 270:	b8 08 00 00 00       	mov    $0x8,%eax
 275:	cd 40                	int    $0x40
 277:	c3                   	ret    

00000278 <link>:
 278:	b8 13 00 00 00       	mov    $0x13,%eax
 27d:	cd 40                	int    $0x40
 27f:	c3                   	ret    

00000280 <mkdir>:
 280:	b8 14 00 00 00       	mov    $0x14,%eax
 285:	cd 40                	int    $0x40
 287:	c3                   	ret    

00000288 <chdir>:
 288:	b8 09 00 00 00       	mov    $0x9,%eax
 28d:	cd 40                	int    $0x40
 28f:	c3                   	ret    

00000290 <dup>:
 290:	b8 0a 00 00 00       	mov    $0xa,%eax
 295:	cd 40                	int    $0x40
 297:	c3                   	ret    

00000298 <getpid>:
 298:	b8 0b 00 00 00       	mov    $0xb,%eax
 29d:	cd 40                	int    $0x40
 29f:	c3                   	ret    

000002a0 <sbrk>:
 2a0:	b8 0c 00 00 00       	mov    $0xc,%eax
 2a5:	cd 40                	int    $0x40
 2a7:	c3                   	ret    

000002a8 <sleep>:
 2a8:	b8 0d 00 00 00       	mov    $0xd,%eax
 2ad:	cd 40                	int    $0x40
 2af:	c3                   	ret    

000002b0 <uptime>:
 2b0:	b8 0e 00 00 00       	mov    $0xe,%eax
 2b5:	cd 40                	int    $0x40
 2b7:	c3                   	ret    

000002b8 <yield>:
 2b8:	b8 16 00 00 00       	mov    $0x16,%eax
 2bd:	cd 40                	int    $0x40
 2bf:	c3                   	ret    

000002c0 <shutdown>:
 2c0:	b8 17 00 00 00       	mov    $0x17,%eax
 2c5:	cd 40                	int    $0x40
 2c7:	c3                   	ret    

000002c8 <writecount>:
 2c8:	b8 18 00 00 00       	mov    $0x18,%eax
 2cd:	cd 40                	int    $0x40
 2cf:	c3                   	ret    

000002d0 <setwritecount>:
 2d0:	b8 19 00 00 00       	mov    $0x19,%eax
 2d5:	cd 40                	int    $0x40
 2d7:	c3                   	ret    

000002d8 <settickets>:
 2d8:	b8 1a 00 00 00       	mov    $0x1a,%eax
 2dd:	cd 40                	int    $0x40
 2df:	c3                   	ret    

000002e0 <getprocessesinfo>:
 2e0:	b8 1b 00 00 00       	mov    $0x1b,%eax
 2e5:	cd 40                	int    $0x40
 2e7:	c3                   	ret    

000002e8 <dumppagetable>:
 2e8:	b8 1c 00 00 00       	mov    $0x1c,%eax
 2ed:	cd 40                	int    $0x40
 2ef:	c3                   	ret    

000002f0 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 2f0:	55                   	push   %ebp
 2f1:	89 e5                	mov    %esp,%ebp
 2f3:	83 ec 1c             	sub    $0x1c,%esp
 2f6:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 2f9:	6a 01                	push   $0x1
 2fb:	8d 55 f4             	lea    -0xc(%ebp),%edx
 2fe:	52                   	push   %edx
 2ff:	50                   	push   %eax
 300:	e8 33 ff ff ff       	call   238 <write>
}
 305:	83 c4 10             	add    $0x10,%esp
 308:	c9                   	leave  
 309:	c3                   	ret    

0000030a <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 30a:	55                   	push   %ebp
 30b:	89 e5                	mov    %esp,%ebp
 30d:	57                   	push   %edi
 30e:	56                   	push   %esi
 30f:	53                   	push   %ebx
 310:	83 ec 2c             	sub    $0x2c,%esp
 313:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 315:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 319:	0f 95 c3             	setne  %bl
 31c:	89 d0                	mov    %edx,%eax
 31e:	c1 e8 1f             	shr    $0x1f,%eax
 321:	84 c3                	test   %al,%bl
 323:	74 10                	je     335 <printint+0x2b>
    neg = 1;
    x = -xx;
 325:	f7 da                	neg    %edx
    neg = 1;
 327:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 32e:	be 00 00 00 00       	mov    $0x0,%esi
 333:	eb 0b                	jmp    340 <printint+0x36>
  neg = 0;
 335:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 33c:	eb f0                	jmp    32e <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 33e:	89 c6                	mov    %eax,%esi
 340:	89 d0                	mov    %edx,%eax
 342:	ba 00 00 00 00       	mov    $0x0,%edx
 347:	f7 f1                	div    %ecx
 349:	89 c3                	mov    %eax,%ebx
 34b:	8d 46 01             	lea    0x1(%esi),%eax
 34e:	0f b6 92 5c 06 00 00 	movzbl 0x65c(%edx),%edx
 355:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 359:	89 da                	mov    %ebx,%edx
 35b:	85 db                	test   %ebx,%ebx
 35d:	75 df                	jne    33e <printint+0x34>
 35f:	89 c3                	mov    %eax,%ebx
  if(neg)
 361:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 365:	74 16                	je     37d <printint+0x73>
    buf[i++] = '-';
 367:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 36c:	8d 5e 02             	lea    0x2(%esi),%ebx
 36f:	eb 0c                	jmp    37d <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 371:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 376:	89 f8                	mov    %edi,%eax
 378:	e8 73 ff ff ff       	call   2f0 <putc>
  while(--i >= 0)
 37d:	83 eb 01             	sub    $0x1,%ebx
 380:	79 ef                	jns    371 <printint+0x67>
}
 382:	83 c4 2c             	add    $0x2c,%esp
 385:	5b                   	pop    %ebx
 386:	5e                   	pop    %esi
 387:	5f                   	pop    %edi
 388:	5d                   	pop    %ebp
 389:	c3                   	ret    

0000038a <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 38a:	55                   	push   %ebp
 38b:	89 e5                	mov    %esp,%ebp
 38d:	57                   	push   %edi
 38e:	56                   	push   %esi
 38f:	53                   	push   %ebx
 390:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 393:	8d 45 10             	lea    0x10(%ebp),%eax
 396:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 399:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 39e:	bb 00 00 00 00       	mov    $0x0,%ebx
 3a3:	eb 14                	jmp    3b9 <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 3a5:	89 fa                	mov    %edi,%edx
 3a7:	8b 45 08             	mov    0x8(%ebp),%eax
 3aa:	e8 41 ff ff ff       	call   2f0 <putc>
 3af:	eb 05                	jmp    3b6 <printf+0x2c>
      }
    } else if(state == '%'){
 3b1:	83 fe 25             	cmp    $0x25,%esi
 3b4:	74 25                	je     3db <printf+0x51>
  for(i = 0; fmt[i]; i++){
 3b6:	83 c3 01             	add    $0x1,%ebx
 3b9:	8b 45 0c             	mov    0xc(%ebp),%eax
 3bc:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 3c0:	84 c0                	test   %al,%al
 3c2:	0f 84 23 01 00 00    	je     4eb <printf+0x161>
    c = fmt[i] & 0xff;
 3c8:	0f be f8             	movsbl %al,%edi
 3cb:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 3ce:	85 f6                	test   %esi,%esi
 3d0:	75 df                	jne    3b1 <printf+0x27>
      if(c == '%'){
 3d2:	83 f8 25             	cmp    $0x25,%eax
 3d5:	75 ce                	jne    3a5 <printf+0x1b>
        state = '%';
 3d7:	89 c6                	mov    %eax,%esi
 3d9:	eb db                	jmp    3b6 <printf+0x2c>
      if(c == 'd'){
 3db:	83 f8 64             	cmp    $0x64,%eax
 3de:	74 49                	je     429 <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 3e0:	83 f8 78             	cmp    $0x78,%eax
 3e3:	0f 94 c1             	sete   %cl
 3e6:	83 f8 70             	cmp    $0x70,%eax
 3e9:	0f 94 c2             	sete   %dl
 3ec:	08 d1                	or     %dl,%cl
 3ee:	75 63                	jne    453 <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 3f0:	83 f8 73             	cmp    $0x73,%eax
 3f3:	0f 84 84 00 00 00    	je     47d <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 3f9:	83 f8 63             	cmp    $0x63,%eax
 3fc:	0f 84 b7 00 00 00    	je     4b9 <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 402:	83 f8 25             	cmp    $0x25,%eax
 405:	0f 84 cc 00 00 00    	je     4d7 <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 40b:	ba 25 00 00 00       	mov    $0x25,%edx
 410:	8b 45 08             	mov    0x8(%ebp),%eax
 413:	e8 d8 fe ff ff       	call   2f0 <putc>
        putc(fd, c);
 418:	89 fa                	mov    %edi,%edx
 41a:	8b 45 08             	mov    0x8(%ebp),%eax
 41d:	e8 ce fe ff ff       	call   2f0 <putc>
      }
      state = 0;
 422:	be 00 00 00 00       	mov    $0x0,%esi
 427:	eb 8d                	jmp    3b6 <printf+0x2c>
        printint(fd, *ap, 10, 1);
 429:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 42c:	8b 17                	mov    (%edi),%edx
 42e:	83 ec 0c             	sub    $0xc,%esp
 431:	6a 01                	push   $0x1
 433:	b9 0a 00 00 00       	mov    $0xa,%ecx
 438:	8b 45 08             	mov    0x8(%ebp),%eax
 43b:	e8 ca fe ff ff       	call   30a <printint>
        ap++;
 440:	83 c7 04             	add    $0x4,%edi
 443:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 446:	83 c4 10             	add    $0x10,%esp
      state = 0;
 449:	be 00 00 00 00       	mov    $0x0,%esi
 44e:	e9 63 ff ff ff       	jmp    3b6 <printf+0x2c>
        printint(fd, *ap, 16, 0);
 453:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 456:	8b 17                	mov    (%edi),%edx
 458:	83 ec 0c             	sub    $0xc,%esp
 45b:	6a 00                	push   $0x0
 45d:	b9 10 00 00 00       	mov    $0x10,%ecx
 462:	8b 45 08             	mov    0x8(%ebp),%eax
 465:	e8 a0 fe ff ff       	call   30a <printint>
        ap++;
 46a:	83 c7 04             	add    $0x4,%edi
 46d:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 470:	83 c4 10             	add    $0x10,%esp
      state = 0;
 473:	be 00 00 00 00       	mov    $0x0,%esi
 478:	e9 39 ff ff ff       	jmp    3b6 <printf+0x2c>
        s = (char*)*ap;
 47d:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 480:	8b 30                	mov    (%eax),%esi
        ap++;
 482:	83 c0 04             	add    $0x4,%eax
 485:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 488:	85 f6                	test   %esi,%esi
 48a:	75 28                	jne    4b4 <printf+0x12a>
          s = "(null)";
 48c:	be 55 06 00 00       	mov    $0x655,%esi
 491:	8b 7d 08             	mov    0x8(%ebp),%edi
 494:	eb 0d                	jmp    4a3 <printf+0x119>
          putc(fd, *s);
 496:	0f be d2             	movsbl %dl,%edx
 499:	89 f8                	mov    %edi,%eax
 49b:	e8 50 fe ff ff       	call   2f0 <putc>
          s++;
 4a0:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 4a3:	0f b6 16             	movzbl (%esi),%edx
 4a6:	84 d2                	test   %dl,%dl
 4a8:	75 ec                	jne    496 <printf+0x10c>
      state = 0;
 4aa:	be 00 00 00 00       	mov    $0x0,%esi
 4af:	e9 02 ff ff ff       	jmp    3b6 <printf+0x2c>
 4b4:	8b 7d 08             	mov    0x8(%ebp),%edi
 4b7:	eb ea                	jmp    4a3 <printf+0x119>
        putc(fd, *ap);
 4b9:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4bc:	0f be 17             	movsbl (%edi),%edx
 4bf:	8b 45 08             	mov    0x8(%ebp),%eax
 4c2:	e8 29 fe ff ff       	call   2f0 <putc>
        ap++;
 4c7:	83 c7 04             	add    $0x4,%edi
 4ca:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 4cd:	be 00 00 00 00       	mov    $0x0,%esi
 4d2:	e9 df fe ff ff       	jmp    3b6 <printf+0x2c>
        putc(fd, c);
 4d7:	89 fa                	mov    %edi,%edx
 4d9:	8b 45 08             	mov    0x8(%ebp),%eax
 4dc:	e8 0f fe ff ff       	call   2f0 <putc>
      state = 0;
 4e1:	be 00 00 00 00       	mov    $0x0,%esi
 4e6:	e9 cb fe ff ff       	jmp    3b6 <printf+0x2c>
    }
  }
}
 4eb:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4ee:	5b                   	pop    %ebx
 4ef:	5e                   	pop    %esi
 4f0:	5f                   	pop    %edi
 4f1:	5d                   	pop    %ebp
 4f2:	c3                   	ret    

000004f3 <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 4f3:	55                   	push   %ebp
 4f4:	89 e5                	mov    %esp,%ebp
 4f6:	57                   	push   %edi
 4f7:	56                   	push   %esi
 4f8:	53                   	push   %ebx
 4f9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 4fc:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 4ff:	a1 f4 08 00 00       	mov    0x8f4,%eax
 504:	eb 02                	jmp    508 <free+0x15>
 506:	89 d0                	mov    %edx,%eax
 508:	39 c8                	cmp    %ecx,%eax
 50a:	73 04                	jae    510 <free+0x1d>
 50c:	39 08                	cmp    %ecx,(%eax)
 50e:	77 12                	ja     522 <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 510:	8b 10                	mov    (%eax),%edx
 512:	39 c2                	cmp    %eax,%edx
 514:	77 f0                	ja     506 <free+0x13>
 516:	39 c8                	cmp    %ecx,%eax
 518:	72 08                	jb     522 <free+0x2f>
 51a:	39 ca                	cmp    %ecx,%edx
 51c:	77 04                	ja     522 <free+0x2f>
 51e:	89 d0                	mov    %edx,%eax
 520:	eb e6                	jmp    508 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 522:	8b 73 fc             	mov    -0x4(%ebx),%esi
 525:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 528:	8b 10                	mov    (%eax),%edx
 52a:	39 d7                	cmp    %edx,%edi
 52c:	74 19                	je     547 <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 52e:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 531:	8b 50 04             	mov    0x4(%eax),%edx
 534:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 537:	39 ce                	cmp    %ecx,%esi
 539:	74 1b                	je     556 <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 53b:	89 08                	mov    %ecx,(%eax)
  freep = p;
 53d:	a3 f4 08 00 00       	mov    %eax,0x8f4
}
 542:	5b                   	pop    %ebx
 543:	5e                   	pop    %esi
 544:	5f                   	pop    %edi
 545:	5d                   	pop    %ebp
 546:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 547:	03 72 04             	add    0x4(%edx),%esi
 54a:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 54d:	8b 10                	mov    (%eax),%edx
 54f:	8b 12                	mov    (%edx),%edx
 551:	89 53 f8             	mov    %edx,-0x8(%ebx)
 554:	eb db                	jmp    531 <free+0x3e>
    p->s.size += bp->s.size;
 556:	03 53 fc             	add    -0x4(%ebx),%edx
 559:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 55c:	8b 53 f8             	mov    -0x8(%ebx),%edx
 55f:	89 10                	mov    %edx,(%eax)
 561:	eb da                	jmp    53d <free+0x4a>

00000563 <morecore>:

static Header*
morecore(uint nu)
{
 563:	55                   	push   %ebp
 564:	89 e5                	mov    %esp,%ebp
 566:	53                   	push   %ebx
 567:	83 ec 04             	sub    $0x4,%esp
 56a:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 56c:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 571:	77 05                	ja     578 <morecore+0x15>
    nu = 4096;
 573:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 578:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 57f:	83 ec 0c             	sub    $0xc,%esp
 582:	50                   	push   %eax
 583:	e8 18 fd ff ff       	call   2a0 <sbrk>
  if(p == (char*)-1)
 588:	83 c4 10             	add    $0x10,%esp
 58b:	83 f8 ff             	cmp    $0xffffffff,%eax
 58e:	74 1c                	je     5ac <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 590:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 593:	83 c0 08             	add    $0x8,%eax
 596:	83 ec 0c             	sub    $0xc,%esp
 599:	50                   	push   %eax
 59a:	e8 54 ff ff ff       	call   4f3 <free>
  return freep;
 59f:	a1 f4 08 00 00       	mov    0x8f4,%eax
 5a4:	83 c4 10             	add    $0x10,%esp
}
 5a7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 5aa:	c9                   	leave  
 5ab:	c3                   	ret    
    return 0;
 5ac:	b8 00 00 00 00       	mov    $0x0,%eax
 5b1:	eb f4                	jmp    5a7 <morecore+0x44>

000005b3 <malloc>:

void*
malloc(uint nbytes)
{
 5b3:	55                   	push   %ebp
 5b4:	89 e5                	mov    %esp,%ebp
 5b6:	53                   	push   %ebx
 5b7:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 5ba:	8b 45 08             	mov    0x8(%ebp),%eax
 5bd:	8d 58 07             	lea    0x7(%eax),%ebx
 5c0:	c1 eb 03             	shr    $0x3,%ebx
 5c3:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 5c6:	8b 0d f4 08 00 00    	mov    0x8f4,%ecx
 5cc:	85 c9                	test   %ecx,%ecx
 5ce:	74 04                	je     5d4 <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 5d0:	8b 01                	mov    (%ecx),%eax
 5d2:	eb 4d                	jmp    621 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 5d4:	c7 05 f4 08 00 00 f8 	movl   $0x8f8,0x8f4
 5db:	08 00 00 
 5de:	c7 05 f8 08 00 00 f8 	movl   $0x8f8,0x8f8
 5e5:	08 00 00 
    base.s.size = 0;
 5e8:	c7 05 fc 08 00 00 00 	movl   $0x0,0x8fc
 5ef:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 5f2:	b9 f8 08 00 00       	mov    $0x8f8,%ecx
 5f7:	eb d7                	jmp    5d0 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 5f9:	39 da                	cmp    %ebx,%edx
 5fb:	74 1a                	je     617 <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 5fd:	29 da                	sub    %ebx,%edx
 5ff:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 602:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 605:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 608:	89 0d f4 08 00 00    	mov    %ecx,0x8f4
      return (void*)(p + 1);
 60e:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 611:	83 c4 04             	add    $0x4,%esp
 614:	5b                   	pop    %ebx
 615:	5d                   	pop    %ebp
 616:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 617:	8b 10                	mov    (%eax),%edx
 619:	89 11                	mov    %edx,(%ecx)
 61b:	eb eb                	jmp    608 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 61d:	89 c1                	mov    %eax,%ecx
 61f:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 621:	8b 50 04             	mov    0x4(%eax),%edx
 624:	39 da                	cmp    %ebx,%edx
 626:	73 d1                	jae    5f9 <malloc+0x46>
    if(p == freep)
 628:	39 05 f4 08 00 00    	cmp    %eax,0x8f4
 62e:	75 ed                	jne    61d <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 630:	89 d8                	mov    %ebx,%eax
 632:	e8 2c ff ff ff       	call   563 <morecore>
 637:	85 c0                	test   %eax,%eax
 639:	75 e2                	jne    61d <malloc+0x6a>
        return 0;
 63b:	b8 00 00 00 00       	mov    $0x0,%eax
 640:	eb cf                	jmp    611 <malloc+0x5e>
