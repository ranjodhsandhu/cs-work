
_copy_on_write_large_repeat_plus_alloc:     file format elf32-i386


Disassembly of section .text:

00000000 <test_out_of_bounds_internal>:
    }
    return 0;
}

static unsigned out_of_bounds_offset = 1;
void test_out_of_bounds_internal() {
       0:	55                   	push   %ebp
       1:	89 e5                	mov    %esp,%ebp
       3:	83 ec 14             	sub    $0x14,%esp
    volatile char *end_of_heap = sbrk(0);
       6:	6a 00                	push   $0x0
       8:	e8 fe 1c 00 00       	call   1d0b <sbrk>
    (void) end_of_heap[out_of_bounds_offset];
       d:	03 05 ac 35 00 00    	add    0x35ac,%eax
      13:	0f b6 00             	movzbl (%eax),%eax
}
      16:	83 c4 10             	add    $0x10,%esp
      19:	c9                   	leave  
      1a:	c3                   	ret    

0000001b <dump_for>:
void dump_for(const char *reason, int pid) {
      1b:	55                   	push   %ebp
      1c:	89 e5                	mov    %esp,%ebp
      1e:	53                   	push   %ebx
      1f:	83 ec 04             	sub    $0x4,%esp
      22:	8b 5d 08             	mov    0x8(%ebp),%ebx
    if (enable_dump) {
      25:	83 3d c0 35 00 00 00 	cmpl   $0x0,0x35c0
      2c:	74 42                	je     70 <dump_for+0x55>
        if (dump_count >= 0) {
      2e:	a1 ec 36 00 00       	mov    0x36ec,%eax
      33:	85 c0                	test   %eax,%eax
      35:	78 3e                	js     75 <dump_for+0x5a>
            printf(1, STARTDUMP "%s#%d\n", reason, dump_count);
      37:	50                   	push   %eax
      38:	53                   	push   %ebx
      39:	68 b0 20 00 00       	push   $0x20b0
      3e:	6a 01                	push   $0x1
      40:	e8 b0 1d 00 00       	call   1df5 <printf>
      45:	83 c4 10             	add    $0x10,%esp
        dumppagetable(pid);
      48:	83 ec 0c             	sub    $0xc,%esp
      4b:	ff 75 0c             	pushl  0xc(%ebp)
      4e:	e8 00 1d 00 00       	call   1d53 <dumppagetable>
        if (dump_count >= 0) {
      53:	a1 ec 36 00 00       	mov    0x36ec,%eax
      58:	83 c4 10             	add    $0x10,%esp
      5b:	85 c0                	test   %eax,%eax
      5d:	78 2b                	js     8a <dump_for+0x6f>
            printf(1, ENDDUMP "%s#%d\n", reason, dump_count);
      5f:	50                   	push   %eax
      60:	53                   	push   %ebx
      61:	68 08 21 00 00       	push   $0x2108
      66:	6a 01                	push   $0x1
      68:	e8 88 1d 00 00       	call   1df5 <printf>
      6d:	83 c4 10             	add    $0x10,%esp
}
      70:	8b 5d fc             	mov    -0x4(%ebp),%ebx
      73:	c9                   	leave  
      74:	c3                   	ret    
            printf(1, STARTDUMP "%s\n", reason);
      75:	83 ec 04             	sub    $0x4,%esp
      78:	53                   	push   %ebx
      79:	68 dc 20 00 00       	push   $0x20dc
      7e:	6a 01                	push   $0x1
      80:	e8 70 1d 00 00       	call   1df5 <printf>
      85:	83 c4 10             	add    $0x10,%esp
      88:	eb be                	jmp    48 <dump_for+0x2d>
            printf(1, ENDDUMP "%s\n", reason);
      8a:	83 ec 04             	sub    $0x4,%esp
      8d:	53                   	push   %ebx
      8e:	68 38 21 00 00       	push   $0x2138
      93:	6a 01                	push   $0x1
      95:	e8 5b 1d 00 00       	call   1df5 <printf>
      9a:	83 c4 10             	add    $0x10,%esp
}
      9d:	eb d1                	jmp    70 <dump_for+0x55>

0000009f <setup>:
void setup() {
      9f:	55                   	push   %ebp
      a0:	89 e5                	mov    %esp,%ebp
      a2:	83 ec 08             	sub    $0x8,%esp
    dump_count = -1;
      a5:	c7 05 ec 36 00 00 ff 	movl   $0xffffffff,0x36ec
      ac:	ff ff ff 
    if (getpid() == 1) {
      af:	e8 4f 1c 00 00       	call   1d03 <getpid>
      b4:	83 f8 01             	cmp    $0x1,%eax
      b7:	74 02                	je     bb <setup+0x1c>
}
      b9:	c9                   	leave  
      ba:	c3                   	ret    
        mknod("console", 1, 1);
      bb:	83 ec 04             	sub    $0x4,%esp
      be:	6a 01                	push   $0x1
      c0:	6a 01                	push   $0x1
      c2:	68 9c 2c 00 00       	push   $0x2c9c
      c7:	e8 ff 1b 00 00       	call   1ccb <mknod>
        open("console", O_RDWR);
      cc:	83 c4 08             	add    $0x8,%esp
      cf:	6a 02                	push   $0x2
      d1:	68 9c 2c 00 00       	push   $0x2c9c
      d6:	e8 e8 1b 00 00       	call   1cc3 <open>
        dup(0);
      db:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
      e2:	e8 14 1c 00 00       	call   1cfb <dup>
        dup(0);
      e7:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
      ee:	e8 08 1c 00 00       	call   1cfb <dup>
      f3:	83 c4 10             	add    $0x10,%esp
}
      f6:	eb c1                	jmp    b9 <setup+0x1a>

000000f8 <finish>:
void finish() {
      f8:	55                   	push   %ebp
      f9:	89 e5                	mov    %esp,%ebp
      fb:	83 ec 08             	sub    $0x8,%esp
    if (getpid() == 1) {
      fe:	e8 00 1c 00 00       	call   1d03 <getpid>
     103:	83 f8 01             	cmp    $0x1,%eax
     106:	75 07                	jne    10f <finish+0x17>
        shutdown();
     108:	e8 1e 1c 00 00       	call   1d2b <shutdown>
}
     10d:	c9                   	leave  
     10e:	c3                   	ret    
        exit();
     10f:	e8 6f 1b 00 00       	call   1c83 <exit>

00000114 <test_simple_crash_no_fork>:
void test_simple_crash_no_fork(void (*test_func)(), const char *no_crash_message) {
     114:	55                   	push   %ebp
     115:	89 e5                	mov    %esp,%ebp
     117:	83 ec 08             	sub    $0x8,%esp
    test_func();
     11a:	ff 55 08             	call   *0x8(%ebp)
    printf(1, "%s\n", no_crash_message);
     11d:	83 ec 04             	sub    $0x4,%esp
     120:	ff 75 0c             	pushl  0xc(%ebp)
     123:	68 a4 2c 00 00       	push   $0x2ca4
     128:	6a 01                	push   $0x1
     12a:	e8 c6 1c 00 00       	call   1df5 <printf>
}
     12f:	83 c4 10             	add    $0x10,%esp
     132:	c9                   	leave  
     133:	c3                   	ret    

00000134 <test_simple_crash>:
int test_simple_crash(void (*test_func)(), const char *crash_message, const char *no_crash_message) {
     134:	55                   	push   %ebp
     135:	89 e5                	mov    %esp,%ebp
     137:	53                   	push   %ebx
     138:	83 ec 20             	sub    $0x20,%esp
    pipe(fds);
     13b:	8d 45 f0             	lea    -0x10(%ebp),%eax
     13e:	50                   	push   %eax
     13f:	e8 4f 1b 00 00       	call   1c93 <pipe>
    int pid = fork();
     144:	e8 32 1b 00 00       	call   1c7b <fork>
    if (pid == -1) {
     149:	83 c4 10             	add    $0x10,%esp
     14c:	83 f8 ff             	cmp    $0xffffffff,%eax
     14f:	74 59                	je     1aa <test_simple_crash+0x76>
    } else if (pid == 0) {
     151:	85 c0                	test   %eax,%eax
     153:	74 6e                	je     1c3 <test_simple_crash+0x8f>
        close(fds[1]);
     155:	83 ec 0c             	sub    $0xc,%esp
     158:	ff 75 f4             	pushl  -0xc(%ebp)
     15b:	e8 4b 1b 00 00       	call   1cab <close>
        int size = read(fds[0], text, 1);
     160:	83 c4 0c             	add    $0xc,%esp
     163:	6a 01                	push   $0x1
     165:	8d 45 ef             	lea    -0x11(%ebp),%eax
     168:	50                   	push   %eax
     169:	ff 75 f0             	pushl  -0x10(%ebp)
     16c:	e8 2a 1b 00 00       	call   1c9b <read>
     171:	89 c3                	mov    %eax,%ebx
        wait();
     173:	e8 13 1b 00 00       	call   1c8b <wait>
        close(fds[0]);
     178:	83 c4 04             	add    $0x4,%esp
     17b:	ff 75 f0             	pushl  -0x10(%ebp)
     17e:	e8 28 1b 00 00       	call   1cab <close>
        if (size == 1) {
     183:	83 c4 10             	add    $0x10,%esp
     186:	83 fb 01             	cmp    $0x1,%ebx
     189:	74 66                	je     1f1 <test_simple_crash+0xbd>
            printf(1, "%s\n", crash_message);
     18b:	83 ec 04             	sub    $0x4,%esp
     18e:	ff 75 0c             	pushl  0xc(%ebp)
     191:	68 a4 2c 00 00       	push   $0x2ca4
     196:	6a 01                	push   $0x1
     198:	e8 58 1c 00 00       	call   1df5 <printf>
            return 1;
     19d:	83 c4 10             	add    $0x10,%esp
     1a0:	b8 01 00 00 00       	mov    $0x1,%eax
}
     1a5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
     1a8:	c9                   	leave  
     1a9:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
     1aa:	83 ec 08             	sub    $0x8,%esp
     1ad:	68 a8 2c 00 00       	push   $0x2ca8
     1b2:	6a 01                	push   $0x1
     1b4:	e8 3c 1c 00 00       	call   1df5 <printf>
    return 0;
     1b9:	83 c4 10             	add    $0x10,%esp
     1bc:	b8 00 00 00 00       	mov    $0x0,%eax
     1c1:	eb e2                	jmp    1a5 <test_simple_crash+0x71>
        close(1);
     1c3:	83 ec 0c             	sub    $0xc,%esp
     1c6:	6a 01                	push   $0x1
     1c8:	e8 de 1a 00 00       	call   1cab <close>
        dup(fds[1]);
     1cd:	83 c4 04             	add    $0x4,%esp
     1d0:	ff 75 f4             	pushl  -0xc(%ebp)
     1d3:	e8 23 1b 00 00       	call   1cfb <dup>
        test_func();
     1d8:	ff 55 08             	call   *0x8(%ebp)
        write(1, "X", 1);
     1db:	83 c4 0c             	add    $0xc,%esp
     1de:	6a 01                	push   $0x1
     1e0:	68 c1 2c 00 00       	push   $0x2cc1
     1e5:	6a 01                	push   $0x1
     1e7:	e8 b7 1a 00 00       	call   1ca3 <write>
        exit();
     1ec:	e8 92 1a 00 00       	call   1c83 <exit>
            printf(1, "%s\n", no_crash_message);
     1f1:	83 ec 04             	sub    $0x4,%esp
     1f4:	ff 75 10             	pushl  0x10(%ebp)
     1f7:	68 a4 2c 00 00       	push   $0x2ca4
     1fc:	6a 01                	push   $0x1
     1fe:	e8 f2 1b 00 00       	call   1df5 <printf>
            return 0;
     203:	83 c4 10             	add    $0x10,%esp
     206:	b8 00 00 00 00       	mov    $0x0,%eax
     20b:	eb 98                	jmp    1a5 <test_simple_crash+0x71>

0000020d <test_out_of_bounds_fork>:

int test_out_of_bounds_fork(int offset, const char *crash_message, const char *no_crash_message) {
     20d:	55                   	push   %ebp
     20e:	89 e5                	mov    %esp,%ebp
     210:	83 ec 0c             	sub    $0xc,%esp
    out_of_bounds_offset = offset;
     213:	8b 45 08             	mov    0x8(%ebp),%eax
     216:	a3 ac 35 00 00       	mov    %eax,0x35ac
    return test_simple_crash(test_out_of_bounds_internal, crash_message, no_crash_message);
     21b:	ff 75 10             	pushl  0x10(%ebp)
     21e:	ff 75 0c             	pushl  0xc(%ebp)
     221:	68 00 00 00 00       	push   $0x0
     226:	e8 09 ff ff ff       	call   134 <test_simple_crash>
}
     22b:	c9                   	leave  
     22c:	c3                   	ret    

0000022d <test_out_of_bounds_no_fork>:

void test_out_of_bounds_no_fork(int offset, const char *no_crash_message) {
     22d:	55                   	push   %ebp
     22e:	89 e5                	mov    %esp,%ebp
     230:	83 ec 10             	sub    $0x10,%esp
    out_of_bounds_offset = offset;
     233:	8b 45 08             	mov    0x8(%ebp),%eax
     236:	a3 ac 35 00 00       	mov    %eax,0x35ac
    test_simple_crash_no_fork(test_out_of_bounds_internal, no_crash_message);
     23b:	ff 75 0c             	pushl  0xc(%ebp)
     23e:	68 00 00 00 00       	push   $0x0
     243:	e8 cc fe ff ff       	call   114 <test_simple_crash_no_fork>
}
     248:	83 c4 10             	add    $0x10,%esp
     24b:	c9                   	leave  
     24c:	c3                   	ret    

0000024d <_allocation_failure_message>:

void _allocation_failure_message(int size, char *code) {
     24d:	55                   	push   %ebp
     24e:	89 e5                	mov    %esp,%ebp
     250:	83 ec 08             	sub    $0x8,%esp
     253:	8b 45 08             	mov    0x8(%ebp),%eax
    if (size == 2 && code[0] == 'N') {
     256:	83 f8 02             	cmp    $0x2,%eax
     259:	74 1c                	je     277 <_allocation_failure_message+0x2a>
        } else if (code[1] == 'F') {
            printf(1, FAIL_MSG "fork failed\n");
        } else {
            printf(1, FAIL_MSG "unknown error\n");
        }
    } else if (size == 0) {
     25b:	85 c0                	test   %eax,%eax
     25d:	0f 84 cd 00 00 00    	je     330 <_allocation_failure_message+0xe3>
        printf(1, FAIL_MSG "unknown crash?\n");
    } else {
        printf(1, FAIL_MSG "unknown error\n");
     263:	83 ec 08             	sub    $0x8,%esp
     266:	68 dd 2c 00 00       	push   $0x2cdd
     26b:	6a 01                	push   $0x1
     26d:	e8 83 1b 00 00       	call   1df5 <printf>
     272:	83 c4 10             	add    $0x10,%esp
    }
}
     275:	c9                   	leave  
     276:	c3                   	ret    
    if (size == 2 && code[0] == 'N') {
     277:	8b 55 0c             	mov    0xc(%ebp),%edx
     27a:	80 3a 4e             	cmpb   $0x4e,(%edx)
     27d:	75 dc                	jne    25b <_allocation_failure_message+0xe>
        if (code[1] == 'A') {
     27f:	0f b6 42 01          	movzbl 0x1(%edx),%eax
     283:	3c 41                	cmp    $0x41,%al
     285:	74 28                	je     2af <_allocation_failure_message+0x62>
        } else if (code[1] == 'I') {
     287:	3c 49                	cmp    $0x49,%al
     289:	74 38                	je     2c3 <_allocation_failure_message+0x76>
        } else if (code[1] == 'R') {
     28b:	3c 52                	cmp    $0x52,%al
     28d:	74 48                	je     2d7 <_allocation_failure_message+0x8a>
        } else if (code[1] == 'S') {
     28f:	3c 53                	cmp    $0x53,%al
     291:	74 58                	je     2eb <_allocation_failure_message+0x9e>
        } else if (code[1] == 's') {
     293:	3c 73                	cmp    $0x73,%al
     295:	74 6b                	je     302 <_allocation_failure_message+0xb5>
        } else if (code[1] == 'F') {
     297:	3c 46                	cmp    $0x46,%al
     299:	74 7e                	je     319 <_allocation_failure_message+0xcc>
            printf(1, FAIL_MSG "unknown error\n");
     29b:	83 ec 08             	sub    $0x8,%esp
     29e:	68 dd 2c 00 00       	push   $0x2cdd
     2a3:	6a 01                	push   $0x1
     2a5:	e8 4b 1b 00 00       	call   1df5 <printf>
     2aa:	83 c4 10             	add    $0x10,%esp
     2ad:	eb c6                	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "allocating (but not using) memory with sbrk() returned error\n");
     2af:	83 ec 08             	sub    $0x8,%esp
     2b2:	68 64 21 00 00       	push   $0x2164
     2b7:	6a 01                	push   $0x1
     2b9:	e8 37 1b 00 00       	call   1df5 <printf>
     2be:	83 c4 10             	add    $0x10,%esp
     2c1:	eb b2                	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "allocation initialized to non-zero value\n");
     2c3:	83 ec 08             	sub    $0x8,%esp
     2c6:	68 b0 21 00 00       	push   $0x21b0
     2cb:	6a 01                	push   $0x1
     2cd:	e8 23 1b 00 00       	call   1df5 <printf>
     2d2:	83 c4 10             	add    $0x10,%esp
     2d5:	eb 9e                	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "using parts of allocation read wrong value\n");
     2d7:	83 ec 08             	sub    $0x8,%esp
     2da:	68 e8 21 00 00       	push   $0x21e8
     2df:	6a 01                	push   $0x1
     2e1:	e8 0f 1b 00 00       	call   1df5 <printf>
     2e6:	83 c4 10             	add    $0x10,%esp
     2e9:	eb 8a                	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "sbrk() returned wrong value (wrong amount allocated?)\n");
     2eb:	83 ec 08             	sub    $0x8,%esp
     2ee:	68 24 22 00 00       	push   $0x2224
     2f3:	6a 01                	push   $0x1
     2f5:	e8 fb 1a 00 00       	call   1df5 <printf>
     2fa:	83 c4 10             	add    $0x10,%esp
     2fd:	e9 73 ff ff ff       	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "sbrk() failed (returned -1)\n");
     302:	83 ec 08             	sub    $0x8,%esp
     305:	68 68 22 00 00       	push   $0x2268
     30a:	6a 01                	push   $0x1
     30c:	e8 e4 1a 00 00       	call   1df5 <printf>
     311:	83 c4 10             	add    $0x10,%esp
     314:	e9 5c ff ff ff       	jmp    275 <_allocation_failure_message+0x28>
            printf(1, FAIL_MSG "fork failed\n");
     319:	83 ec 08             	sub    $0x8,%esp
     31c:	68 c3 2c 00 00       	push   $0x2cc3
     321:	6a 01                	push   $0x1
     323:	e8 cd 1a 00 00       	call   1df5 <printf>
     328:	83 c4 10             	add    $0x10,%esp
     32b:	e9 45 ff ff ff       	jmp    275 <_allocation_failure_message+0x28>
        printf(1, FAIL_MSG "unknown crash?\n");
     330:	83 ec 08             	sub    $0x8,%esp
     333:	68 f9 2c 00 00       	push   $0x2cf9
     338:	6a 01                	push   $0x1
     33a:	e8 b6 1a 00 00       	call   1df5 <printf>
     33f:	83 c4 10             	add    $0x10,%esp
     342:	e9 2e ff ff ff       	jmp    275 <_allocation_failure_message+0x28>

00000347 <_fail_allocation_test>:

void _fail_allocation_test(int pipe_fd, char reason) {
     347:	55                   	push   %ebp
     348:	89 e5                	mov    %esp,%ebp
     34a:	83 ec 18             	sub    $0x18,%esp
    char temp[2] = {'N', reason};
     34d:	c6 45 f6 4e          	movb   $0x4e,-0xa(%ebp)
     351:	8b 45 0c             	mov    0xc(%ebp),%eax
     354:	88 45 f7             	mov    %al,-0x9(%ebp)
    if (pipe_fd == -1) {
     357:	83 7d 08 ff          	cmpl   $0xffffffff,0x8(%ebp)
     35b:	75 10                	jne    36d <_fail_allocation_test+0x26>
      _allocation_failure_message(2, temp);
     35d:	83 ec 08             	sub    $0x8,%esp
     360:	8d 45 f6             	lea    -0xa(%ebp),%eax
     363:	50                   	push   %eax
     364:	6a 02                	push   $0x2
     366:	e8 e2 fe ff ff       	call   24d <_allocation_failure_message>
    } else {
      write(pipe_fd, temp, 2);
      exit();
    }
}
     36b:	c9                   	leave  
     36c:	c3                   	ret    
      write(pipe_fd, temp, 2);
     36d:	83 ec 04             	sub    $0x4,%esp
     370:	6a 02                	push   $0x2
     372:	8d 45 f6             	lea    -0xa(%ebp),%eax
     375:	50                   	push   %eax
     376:	ff 75 08             	pushl  0x8(%ebp)
     379:	e8 25 19 00 00       	call   1ca3 <write>
      exit();
     37e:	e8 00 19 00 00       	call   1c83 <exit>

00000383 <_pass_allocation_test>:

void _pass_allocation_test(int pipe_fd, const char *message) {
     383:	55                   	push   %ebp
     384:	89 e5                	mov    %esp,%ebp
     386:	83 ec 18             	sub    $0x18,%esp
    char temp[2] = {'Y', 'Y'};
     389:	c6 45 f6 59          	movb   $0x59,-0xa(%ebp)
     38d:	c6 45 f7 59          	movb   $0x59,-0x9(%ebp)
    if (pipe_fd == -1) {
     391:	83 7d 08 ff          	cmpl   $0xffffffff,0x8(%ebp)
     395:	75 14                	jne    3ab <_pass_allocation_test+0x28>
      printf(1, PASS_MSG "%s", message);
     397:	83 ec 04             	sub    $0x4,%esp
     39a:	ff 75 0c             	pushl  0xc(%ebp)
     39d:	68 16 2d 00 00       	push   $0x2d16
     3a2:	6a 01                	push   $0x1
     3a4:	e8 4c 1a 00 00       	call   1df5 <printf>
    } else {
      write(pipe_fd, temp, 2);
      exit();
    }
}
     3a9:	c9                   	leave  
     3aa:	c3                   	ret    
      write(pipe_fd, temp, 2);
     3ab:	83 ec 04             	sub    $0x4,%esp
     3ae:	6a 02                	push   $0x2
     3b0:	8d 45 f6             	lea    -0xa(%ebp),%eax
     3b3:	50                   	push   %eax
     3b4:	ff 75 08             	pushl  0x8(%ebp)
     3b7:	e8 e7 18 00 00       	call   1ca3 <write>
      exit();
     3bc:	e8 c2 18 00 00       	call   1c83 <exit>

000003c1 <_test_allocation_generic>:

int _test_allocation_generic(
    int fork_before, int fork_after,
    int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero,
    int write_after
) {
     3c1:	55                   	push   %ebp
     3c2:	89 e5                	mov    %esp,%ebp
     3c4:	57                   	push   %edi
     3c5:	56                   	push   %esi
     3c6:	53                   	push   %ebx
     3c7:	83 ec 2c             	sub    $0x2c,%esp
     3ca:	8b 5d 08             	mov    0x8(%ebp),%ebx
     3cd:	8b 7d 18             	mov    0x18(%ebp),%edi
     3d0:	8b 75 2c             	mov    0x2c(%ebp),%esi
  printf(1, "testing allocating %s and reading/writing to %s segments of it\n", describe_size, describe_amount);
     3d3:	57                   	push   %edi
     3d4:	ff 75 14             	pushl  0x14(%ebp)
     3d7:	68 18 23 00 00       	push   $0x2318
     3dc:	6a 01                	push   $0x1
     3de:	e8 12 1a 00 00       	call   1df5 <printf>
  if (check_zero)
     3e3:	83 c4 10             	add    $0x10,%esp
     3e6:	85 f6                	test   %esi,%esi
     3e8:	0f 85 0b 01 00 00    	jne    4f9 <_test_allocation_generic+0x138>
    printf(1, "... and verifying that (at least some of) the heap is initialized to zeroes\n");
  if (fork_before)
     3ee:	85 db                	test   %ebx,%ebx
     3f0:	0f 85 1a 01 00 00    	jne    510 <_test_allocation_generic+0x14f>
    printf(1, "... in a subprocess\n");
  if (fork_after)
     3f6:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     3fa:	74 20                	je     41c <_test_allocation_generic+0x5b>
    printf(1, "... and fork'ing%s after writing to parts of the heap\n",
     3fc:	85 db                	test   %ebx,%ebx
     3fe:	0f 84 23 01 00 00    	je     527 <_test_allocation_generic+0x166>
     404:	b8 26 2d 00 00       	mov    $0x2d26,%eax
     409:	83 ec 04             	sub    $0x4,%esp
     40c:	50                   	push   %eax
     40d:	68 a8 23 00 00       	push   $0x23a8
     412:	6a 01                	push   $0x1
     414:	e8 dc 19 00 00       	call   1df5 <printf>
     419:	83 c4 10             	add    $0x10,%esp
        fork_before ? " again" : "");
  if (write_after)
     41c:	83 7d 30 00          	cmpl   $0x0,0x30(%ebp)
     420:	0f 85 0b 01 00 00    	jne    531 <_test_allocation_generic+0x170>
    printf(1, "... and writing in the child process after forking and reading from the parent after that\n");
  dump_for("allocation-pre-allocate", getpid());
     426:	e8 d8 18 00 00       	call   1d03 <getpid>
     42b:	83 ec 08             	sub    $0x8,%esp
     42e:	50                   	push   %eax
     42f:	68 42 2d 00 00       	push   $0x2d42
     434:	e8 e2 fb ff ff       	call   1b <dump_for>
  int fds[2] = {-1, -1};
     439:	c7 45 e0 ff ff ff ff 	movl   $0xffffffff,-0x20(%ebp)
     440:	c7 45 e4 ff ff ff ff 	movl   $0xffffffff,-0x1c(%ebp)
  int main_pid = -1;
  if (fork_before) {
     447:	83 c4 10             	add    $0x10,%esp
     44a:	85 db                	test   %ebx,%ebx
     44c:	0f 84 9a 01 00 00    	je     5ec <_test_allocation_generic+0x22b>
    pipe(fds);
     452:	83 ec 0c             	sub    $0xc,%esp
     455:	8d 45 e0             	lea    -0x20(%ebp),%eax
     458:	50                   	push   %eax
     459:	e8 35 18 00 00       	call   1c93 <pipe>
    main_pid = fork();
     45e:	e8 18 18 00 00       	call   1c7b <fork>
    if (main_pid == -1) {
     463:	83 c4 10             	add    $0x10,%esp
     466:	83 f8 ff             	cmp    $0xffffffff,%eax
     469:	0f 84 d9 00 00 00    	je     548 <_test_allocation_generic+0x187>
      printf(1, FAIL_MSG "fork failed");
    } else if (main_pid != 0) {
     46f:	85 c0                	test   %eax,%eax
     471:	0f 84 67 01 00 00    	je     5de <_test_allocation_generic+0x21d>
      /* parent process */
      char text[10];
      close(fds[1]);
     477:	83 ec 0c             	sub    $0xc,%esp
     47a:	ff 75 e4             	pushl  -0x1c(%ebp)
     47d:	e8 29 18 00 00       	call   1cab <close>
      wait();
     482:	e8 04 18 00 00       	call   1c8b <wait>
      int size = read(fds[0], text, 10);
     487:	83 c4 0c             	add    $0xc,%esp
     48a:	6a 0a                	push   $0xa
     48c:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     48f:	50                   	push   %eax
     490:	ff 75 e0             	pushl  -0x20(%ebp)
     493:	e8 03 18 00 00       	call   1c9b <read>
     498:	89 c3                	mov    %eax,%ebx
      close(fds[0]);
     49a:	83 c4 04             	add    $0x4,%esp
     49d:	ff 75 e0             	pushl  -0x20(%ebp)
     4a0:	e8 06 18 00 00       	call   1cab <close>
      if (fork_after) {
     4a5:	83 c4 10             	add    $0x10,%esp
     4a8:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     4ac:	0f 84 f3 00 00 00    	je     5a5 <_test_allocation_generic+0x1e4>
        if (size != 4) {
     4b2:	83 fb 04             	cmp    $0x4,%ebx
     4b5:	0f 85 a4 00 00 00    	jne    55f <_test_allocation_generic+0x19e>
          printf(1, FAIL_MSG "allocation test did not return result from both processes after fork()ing after allocation?");
          return 0;
        } else if (text[0] != 'Y') {
     4bb:	80 7d d6 59          	cmpb   $0x59,-0x2a(%ebp)
     4bf:	0f 85 b6 00 00 00    	jne    57b <_test_allocation_generic+0x1ba>
          printf(1, "... test failed in child process:\n");
          _allocation_failure_message(2, text);
          return 0;
        } else if (text[2] != 'Y') {
     4c5:	80 7d d8 59          	cmpb   $0x59,-0x28(%ebp)
     4c9:	0f 84 1d 01 00 00    	je     5ec <_test_allocation_generic+0x22b>
          printf(1, "... test failed in grandchild process :\n");
     4cf:	83 ec 08             	sub    $0x8,%esp
     4d2:	68 cc 24 00 00       	push   $0x24cc
     4d7:	6a 01                	push   $0x1
     4d9:	e8 17 19 00 00       	call   1df5 <printf>
          _allocation_failure_message(2, text + 2);
     4de:	83 c4 08             	add    $0x8,%esp
     4e1:	8d 45 d8             	lea    -0x28(%ebp),%eax
     4e4:	50                   	push   %eax
     4e5:	6a 02                	push   $0x2
     4e7:	e8 61 fd ff ff       	call   24d <_allocation_failure_message>
          return 0;
     4ec:	83 c4 10             	add    $0x10,%esp
     4ef:	be 00 00 00 00       	mov    $0x0,%esi
     4f4:	e9 30 01 00 00       	jmp    629 <_test_allocation_generic+0x268>
    printf(1, "... and verifying that (at least some of) the heap is initialized to zeroes\n");
     4f9:	83 ec 08             	sub    $0x8,%esp
     4fc:	68 58 23 00 00       	push   $0x2358
     501:	6a 01                	push   $0x1
     503:	e8 ed 18 00 00       	call   1df5 <printf>
     508:	83 c4 10             	add    $0x10,%esp
     50b:	e9 de fe ff ff       	jmp    3ee <_test_allocation_generic+0x2d>
    printf(1, "... in a subprocess\n");
     510:	83 ec 08             	sub    $0x8,%esp
     513:	68 2d 2d 00 00       	push   $0x2d2d
     518:	6a 01                	push   $0x1
     51a:	e8 d6 18 00 00       	call   1df5 <printf>
     51f:	83 c4 10             	add    $0x10,%esp
     522:	e9 cf fe ff ff       	jmp    3f6 <_test_allocation_generic+0x35>
    printf(1, "... and fork'ing%s after writing to parts of the heap\n",
     527:	b8 97 2e 00 00       	mov    $0x2e97,%eax
     52c:	e9 d8 fe ff ff       	jmp    409 <_test_allocation_generic+0x48>
    printf(1, "... and writing in the child process after forking and reading from the parent after that\n");
     531:	83 ec 08             	sub    $0x8,%esp
     534:	68 e0 23 00 00       	push   $0x23e0
     539:	6a 01                	push   $0x1
     53b:	e8 b5 18 00 00       	call   1df5 <printf>
     540:	83 c4 10             	add    $0x10,%esp
     543:	e9 de fe ff ff       	jmp    426 <_test_allocation_generic+0x65>
      printf(1, FAIL_MSG "fork failed");
     548:	83 ec 08             	sub    $0x8,%esp
     54b:	68 a8 2c 00 00       	push   $0x2ca8
     550:	6a 01                	push   $0x1
     552:	e8 9e 18 00 00       	call   1df5 <printf>
     557:	83 c4 10             	add    $0x10,%esp
     55a:	e9 8d 00 00 00       	jmp    5ec <_test_allocation_generic+0x22b>
          printf(1, FAIL_MSG "allocation test did not return result from both processes after fork()ing after allocation?");
     55f:	83 ec 08             	sub    $0x8,%esp
     562:	68 3c 24 00 00       	push   $0x243c
     567:	6a 01                	push   $0x1
     569:	e8 87 18 00 00       	call   1df5 <printf>
          return 0;
     56e:	83 c4 10             	add    $0x10,%esp
     571:	be 00 00 00 00       	mov    $0x0,%esi
     576:	e9 ae 00 00 00       	jmp    629 <_test_allocation_generic+0x268>
          printf(1, "... test failed in child process:\n");
     57b:	83 ec 08             	sub    $0x8,%esp
     57e:	68 a8 24 00 00       	push   $0x24a8
     583:	6a 01                	push   $0x1
     585:	e8 6b 18 00 00       	call   1df5 <printf>
          _allocation_failure_message(2, text);
     58a:	83 c4 08             	add    $0x8,%esp
     58d:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     590:	50                   	push   %eax
     591:	6a 02                	push   $0x2
     593:	e8 b5 fc ff ff       	call   24d <_allocation_failure_message>
          return 0;
     598:	83 c4 10             	add    $0x10,%esp
     59b:	be 00 00 00 00       	mov    $0x0,%esi
     5a0:	e9 84 00 00 00       	jmp    629 <_test_allocation_generic+0x268>
        }
      } else if (size < 1 || text[0] != 'Y') {
     5a5:	85 db                	test   %ebx,%ebx
     5a7:	7e 06                	jle    5af <_test_allocation_generic+0x1ee>
     5a9:	80 7d d6 59          	cmpb   $0x59,-0x2a(%ebp)
     5ad:	74 15                	je     5c4 <_test_allocation_generic+0x203>
        _allocation_failure_message(size, text);
     5af:	83 ec 08             	sub    $0x8,%esp
     5b2:	8d 45 d6             	lea    -0x2a(%ebp),%eax
     5b5:	50                   	push   %eax
     5b6:	53                   	push   %ebx
     5b7:	e8 91 fc ff ff       	call   24d <_allocation_failure_message>
        return 0;
     5bc:	83 c4 10             	add    $0x10,%esp
     5bf:	8b 75 0c             	mov    0xc(%ebp),%esi
     5c2:	eb 65                	jmp    629 <_test_allocation_generic+0x268>
      } else {
        printf(1, PASS_MSG "allocating %s and using %s parts of allocation passed\n", describe_size, describe_amount);
     5c4:	57                   	push   %edi
     5c5:	ff 75 14             	pushl  0x14(%ebp)
     5c8:	68 f8 24 00 00       	push   $0x24f8
     5cd:	6a 01                	push   $0x1
     5cf:	e8 21 18 00 00       	call   1df5 <printf>
        return 1;
     5d4:	83 c4 10             	add    $0x10,%esp
     5d7:	be 01 00 00 00       	mov    $0x1,%esi
     5dc:	eb 4b                	jmp    629 <_test_allocation_generic+0x268>
      }
    } else {
      close(fds[0]);
     5de:	83 ec 0c             	sub    $0xc,%esp
     5e1:	ff 75 e0             	pushl  -0x20(%ebp)
     5e4:	e8 c2 16 00 00       	call   1cab <close>
     5e9:	83 c4 10             	add    $0x10,%esp
    }
  }
  char *old_end_of_heap = sbrk(size);
     5ec:	83 ec 0c             	sub    $0xc,%esp
     5ef:	ff 75 10             	pushl  0x10(%ebp)
     5f2:	e8 14 17 00 00       	call   1d0b <sbrk>
     5f7:	89 c7                	mov    %eax,%edi
  char *new_end_of_heap = sbrk(0);
     5f9:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     600:	e8 06 17 00 00       	call   1d0b <sbrk>
  if (old_end_of_heap == (char*) -1) {
     605:	83 c4 10             	add    $0x10,%esp
     608:	83 ff ff             	cmp    $0xffffffff,%edi
     60b:	74 26                	je     633 <_test_allocation_generic+0x272>
    _fail_allocation_test(fds[1], 's');
    return 0;
  } else if (new_end_of_heap - old_end_of_heap != size) {
     60d:	29 f8                	sub    %edi,%eax
     60f:	3b 45 10             	cmp    0x10(%ebp),%eax
     612:	74 36                	je     64a <_test_allocation_generic+0x289>
    _fail_allocation_test(fds[1], 'S');
     614:	83 ec 08             	sub    $0x8,%esp
     617:	6a 53                	push   $0x53
     619:	ff 75 e4             	pushl  -0x1c(%ebp)
     61c:	e8 26 fd ff ff       	call   347 <_fail_allocation_test>
    return 0;
     621:	83 c4 10             	add    $0x10,%esp
     624:	be 00 00 00 00       	mov    $0x0,%esi
        "allocation passed (expand + write + read heap)\n"
      );
      return 1;
    }
  }
}
     629:	89 f0                	mov    %esi,%eax
     62b:	8d 65 f4             	lea    -0xc(%ebp),%esp
     62e:	5b                   	pop    %ebx
     62f:	5e                   	pop    %esi
     630:	5f                   	pop    %edi
     631:	5d                   	pop    %ebp
     632:	c3                   	ret    
    _fail_allocation_test(fds[1], 's');
     633:	83 ec 08             	sub    $0x8,%esp
     636:	6a 73                	push   $0x73
     638:	ff 75 e4             	pushl  -0x1c(%ebp)
     63b:	e8 07 fd ff ff       	call   347 <_fail_allocation_test>
    return 0;
     640:	83 c4 10             	add    $0x10,%esp
     643:	be 00 00 00 00       	mov    $0x0,%esi
     648:	eb df                	jmp    629 <_test_allocation_generic+0x268>
    dump_for("allocation-pre-access", getpid());
     64a:	e8 b4 16 00 00       	call   1d03 <getpid>
     64f:	83 ec 08             	sub    $0x8,%esp
     652:	50                   	push   %eax
     653:	68 5a 2d 00 00       	push   $0x2d5a
     658:	e8 be f9 ff ff       	call   1b <dump_for>
    char *place_one = &old_end_of_heap[offset1];
     65d:	89 fb                	mov    %edi,%ebx
     65f:	03 5d 1c             	add    0x1c(%ebp),%ebx
    char *place_two = &old_end_of_heap[offset2];
     662:	03 7d 24             	add    0x24(%ebp),%edi
    for (i = 0; i < count1; ++i) {
     665:	83 c4 10             	add    $0x10,%esp
     668:	b8 00 00 00 00       	mov    $0x0,%eax
     66d:	8b 55 20             	mov    0x20(%ebp),%edx
     670:	eb 07                	jmp    679 <_test_allocation_generic+0x2b8>
      place_one[i] = 'A';
     672:	c6 04 03 41          	movb   $0x41,(%ebx,%eax,1)
    for (i = 0; i < count1; ++i) {
     676:	83 c0 01             	add    $0x1,%eax
     679:	39 d0                	cmp    %edx,%eax
     67b:	7d 21                	jge    69e <_test_allocation_generic+0x2dd>
      if (check_zero && place_one[i] != '\0') {
     67d:	85 f6                	test   %esi,%esi
     67f:	74 f1                	je     672 <_test_allocation_generic+0x2b1>
     681:	80 3c 03 00          	cmpb   $0x0,(%ebx,%eax,1)
     685:	74 eb                	je     672 <_test_allocation_generic+0x2b1>
        _fail_allocation_test(fds[1], 'I');
     687:	83 ec 08             	sub    $0x8,%esp
     68a:	6a 49                	push   $0x49
     68c:	ff 75 e4             	pushl  -0x1c(%ebp)
     68f:	e8 b3 fc ff ff       	call   347 <_fail_allocation_test>
        return 0;
     694:	83 c4 10             	add    $0x10,%esp
     697:	be 00 00 00 00       	mov    $0x0,%esi
     69c:	eb 8b                	jmp    629 <_test_allocation_generic+0x268>
    for (i = 0; i < count2; ++i) {
     69e:	b8 00 00 00 00       	mov    $0x0,%eax
     6a3:	8b 55 28             	mov    0x28(%ebp),%edx
     6a6:	eb 07                	jmp    6af <_test_allocation_generic+0x2ee>
      place_two[i] = 'B';
     6a8:	c6 04 07 42          	movb   $0x42,(%edi,%eax,1)
    for (i = 0; i < count2; ++i) {
     6ac:	83 c0 01             	add    $0x1,%eax
     6af:	39 d0                	cmp    %edx,%eax
     6b1:	7d 24                	jge    6d7 <_test_allocation_generic+0x316>
      if (check_zero && place_two[i] != '\0') {
     6b3:	85 f6                	test   %esi,%esi
     6b5:	74 f1                	je     6a8 <_test_allocation_generic+0x2e7>
     6b7:	80 3c 07 00          	cmpb   $0x0,(%edi,%eax,1)
     6bb:	74 eb                	je     6a8 <_test_allocation_generic+0x2e7>
        _fail_allocation_test(fds[1], 'I');
     6bd:	83 ec 08             	sub    $0x8,%esp
     6c0:	6a 49                	push   $0x49
     6c2:	ff 75 e4             	pushl  -0x1c(%ebp)
     6c5:	e8 7d fc ff ff       	call   347 <_fail_allocation_test>
        return 0;
     6ca:	83 c4 10             	add    $0x10,%esp
     6cd:	be 00 00 00 00       	mov    $0x0,%esi
     6d2:	e9 52 ff ff ff       	jmp    629 <_test_allocation_generic+0x268>
    dump_for("allocation-post-access", getpid());
     6d7:	e8 27 16 00 00       	call   1d03 <getpid>
     6dc:	83 ec 08             	sub    $0x8,%esp
     6df:	50                   	push   %eax
     6e0:	68 70 2d 00 00       	push   $0x2d70
     6e5:	e8 31 f9 ff ff       	call   1b <dump_for>
    if (fork_after) {
     6ea:	83 c4 10             	add    $0x10,%esp
     6ed:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     6f1:	75 29                	jne    71c <_test_allocation_generic+0x35b>
      if (fork_after) {
     6f3:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     6f7:	0f 85 d9 00 00 00    	jne    7d6 <_test_allocation_generic+0x415>
      for (i = 0; i < count1; ++i) {
     6fd:	b8 00 00 00 00       	mov    $0x0,%eax
     702:	8b 55 20             	mov    0x20(%ebp),%edx
     705:	39 d0                	cmp    %edx,%eax
     707:	0f 8d 03 01 00 00    	jge    810 <_test_allocation_generic+0x44f>
        if (place_one[i] != 'A') {
     70d:	80 3c 03 41          	cmpb   $0x41,(%ebx,%eax,1)
     711:	0f 85 df 00 00 00    	jne    7f6 <_test_allocation_generic+0x435>
      for (i = 0; i < count1; ++i) {
     717:	83 c0 01             	add    $0x1,%eax
     71a:	eb e9                	jmp    705 <_test_allocation_generic+0x344>
      pid = fork();
     71c:	e8 5a 15 00 00       	call   1c7b <fork>
     721:	89 c6                	mov    %eax,%esi
      if (pid == -1) {
     723:	83 f8 ff             	cmp    $0xffffffff,%eax
     726:	74 2e                	je     756 <_test_allocation_generic+0x395>
    if (pid == 0) {
     728:	85 c0                	test   %eax,%eax
     72a:	75 c7                	jne    6f3 <_test_allocation_generic+0x332>
      dump_for("allocation-post-fork-child", getpid());
     72c:	e8 d2 15 00 00       	call   1d03 <getpid>
     731:	83 ec 08             	sub    $0x8,%esp
     734:	50                   	push   %eax
     735:	68 87 2d 00 00       	push   $0x2d87
     73a:	e8 dc f8 ff ff       	call   1b <dump_for>
      for (i = 0; i < count1; ++i) {
     73f:	83 c4 10             	add    $0x10,%esp
     742:	89 f0                	mov    %esi,%eax
     744:	8b 55 20             	mov    0x20(%ebp),%edx
     747:	39 d0                	cmp    %edx,%eax
     749:	7d 3a                	jge    785 <_test_allocation_generic+0x3c4>
        if (place_one[i] != 'A') {
     74b:	80 3c 03 41          	cmpb   $0x41,(%ebx,%eax,1)
     74f:	75 1f                	jne    770 <_test_allocation_generic+0x3af>
      for (i = 0; i < count1; ++i) {
     751:	83 c0 01             	add    $0x1,%eax
     754:	eb f1                	jmp    747 <_test_allocation_generic+0x386>
        _fail_allocation_test(fds[1], 'F');
     756:	83 ec 08             	sub    $0x8,%esp
     759:	6a 46                	push   $0x46
     75b:	ff 75 e4             	pushl  -0x1c(%ebp)
     75e:	e8 e4 fb ff ff       	call   347 <_fail_allocation_test>
        return 0;
     763:	83 c4 10             	add    $0x10,%esp
     766:	be 00 00 00 00       	mov    $0x0,%esi
     76b:	e9 b9 fe ff ff       	jmp    629 <_test_allocation_generic+0x268>
          _fail_allocation_test(fds[1], 'R');
     770:	83 ec 08             	sub    $0x8,%esp
     773:	6a 52                	push   $0x52
     775:	ff 75 e4             	pushl  -0x1c(%ebp)
     778:	e8 ca fb ff ff       	call   347 <_fail_allocation_test>
          return 0;
     77d:	83 c4 10             	add    $0x10,%esp
     780:	e9 a4 fe ff ff       	jmp    629 <_test_allocation_generic+0x268>
      for (i = 0; i < count2; ++i) {
     785:	89 f0                	mov    %esi,%eax
     787:	8b 55 28             	mov    0x28(%ebp),%edx
     78a:	eb 03                	jmp    78f <_test_allocation_generic+0x3ce>
     78c:	83 c0 01             	add    $0x1,%eax
     78f:	39 d0                	cmp    %edx,%eax
     791:	7d 1b                	jge    7ae <_test_allocation_generic+0x3ed>
        if (place_two[i] != 'B') {
     793:	80 3c 07 42          	cmpb   $0x42,(%edi,%eax,1)
     797:	74 f3                	je     78c <_test_allocation_generic+0x3cb>
          _fail_allocation_test(fds[1], 'R');
     799:	83 ec 08             	sub    $0x8,%esp
     79c:	6a 52                	push   $0x52
     79e:	ff 75 e4             	pushl  -0x1c(%ebp)
     7a1:	e8 a1 fb ff ff       	call   347 <_fail_allocation_test>
          return 0;
     7a6:	83 c4 10             	add    $0x10,%esp
     7a9:	e9 7b fe ff ff       	jmp    629 <_test_allocation_generic+0x268>
     7ae:	89 c6                	mov    %eax,%esi
      _pass_allocation_test(fds[1], "allocation passed in child (expand + write + fork + read heap in child)\n");
     7b0:	83 ec 08             	sub    $0x8,%esp
     7b3:	68 3c 25 00 00       	push   $0x253c
     7b8:	ff 75 e4             	pushl  -0x1c(%ebp)
     7bb:	e8 c3 fb ff ff       	call   383 <_pass_allocation_test>
      if (write_after) {
     7c0:	83 c4 10             	add    $0x10,%esp
     7c3:	83 7d 30 00          	cmpl   $0x0,0x30(%ebp)
     7c7:	74 08                	je     7d1 <_test_allocation_generic+0x410>
        place_one[i] = 'X';
     7c9:	c6 04 33 58          	movb   $0x58,(%ebx,%esi,1)
        place_two[i] = 'Y';
     7cd:	c6 04 37 59          	movb   $0x59,(%edi,%esi,1)
      exit();
     7d1:	e8 ad 14 00 00       	call   1c83 <exit>
        wait();
     7d6:	e8 b0 14 00 00       	call   1c8b <wait>
        dump_for("allocation-post-fork-parent", getpid());
     7db:	e8 23 15 00 00       	call   1d03 <getpid>
     7e0:	83 ec 08             	sub    $0x8,%esp
     7e3:	50                   	push   %eax
     7e4:	68 a2 2d 00 00       	push   $0x2da2
     7e9:	e8 2d f8 ff ff       	call   1b <dump_for>
     7ee:	83 c4 10             	add    $0x10,%esp
     7f1:	e9 07 ff ff ff       	jmp    6fd <_test_allocation_generic+0x33c>
          _fail_allocation_test(fds[1], 'R');
     7f6:	83 ec 08             	sub    $0x8,%esp
     7f9:	6a 52                	push   $0x52
     7fb:	ff 75 e4             	pushl  -0x1c(%ebp)
     7fe:	e8 44 fb ff ff       	call   347 <_fail_allocation_test>
          return 0;
     803:	83 c4 10             	add    $0x10,%esp
     806:	be 00 00 00 00       	mov    $0x0,%esi
     80b:	e9 19 fe ff ff       	jmp    629 <_test_allocation_generic+0x268>
      for (i = 0; i < count2; ++i) {
     810:	b8 00 00 00 00       	mov    $0x0,%eax
     815:	8b 55 28             	mov    0x28(%ebp),%edx
     818:	39 d0                	cmp    %edx,%eax
     81a:	7d 25                	jge    841 <_test_allocation_generic+0x480>
        if (place_two[i] != 'B') {
     81c:	80 3c 07 42          	cmpb   $0x42,(%edi,%eax,1)
     820:	75 05                	jne    827 <_test_allocation_generic+0x466>
      for (i = 0; i < count2; ++i) {
     822:	83 c0 01             	add    $0x1,%eax
     825:	eb f1                	jmp    818 <_test_allocation_generic+0x457>
          _fail_allocation_test(fds[1], 'R');
     827:	83 ec 08             	sub    $0x8,%esp
     82a:	6a 52                	push   $0x52
     82c:	ff 75 e4             	pushl  -0x1c(%ebp)
     82f:	e8 13 fb ff ff       	call   347 <_fail_allocation_test>
          return 0;
     834:	83 c4 10             	add    $0x10,%esp
     837:	be 00 00 00 00       	mov    $0x0,%esi
     83c:	e9 e8 fd ff ff       	jmp    629 <_test_allocation_generic+0x268>
      _pass_allocation_test(fds[1], fork_after ?
     841:	83 7d 0c 00          	cmpl   $0x0,0xc(%ebp)
     845:	74 1e                	je     865 <_test_allocation_generic+0x4a4>
     847:	b8 94 22 00 00       	mov    $0x2294,%eax
     84c:	83 ec 08             	sub    $0x8,%esp
     84f:	50                   	push   %eax
     850:	ff 75 e4             	pushl  -0x1c(%ebp)
     853:	e8 2b fb ff ff       	call   383 <_pass_allocation_test>
      return 1;
     858:	83 c4 10             	add    $0x10,%esp
     85b:	be 01 00 00 00       	mov    $0x1,%esi
     860:	e9 c4 fd ff ff       	jmp    629 <_test_allocation_generic+0x268>
      _pass_allocation_test(fds[1], fork_after ?
     865:	b8 e8 22 00 00       	mov    $0x22e8,%eax
     86a:	eb e0                	jmp    84c <_test_allocation_generic+0x48b>

0000086c <test_allocation_no_fork>:


int test_allocation_no_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero) {
     86c:	55                   	push   %ebp
     86d:	89 e5                	mov    %esp,%ebp
     86f:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(0, 0, size, describe_size, describe_amount, offset1, count1, offset2, count2, check_zero, 0);
     872:	6a 00                	push   $0x0
     874:	ff 75 24             	pushl  0x24(%ebp)
     877:	ff 75 20             	pushl  0x20(%ebp)
     87a:	ff 75 1c             	pushl  0x1c(%ebp)
     87d:	ff 75 18             	pushl  0x18(%ebp)
     880:	ff 75 14             	pushl  0x14(%ebp)
     883:	ff 75 10             	pushl  0x10(%ebp)
     886:	ff 75 0c             	pushl  0xc(%ebp)
     889:	ff 75 08             	pushl  0x8(%ebp)
     88c:	6a 00                	push   $0x0
     88e:	6a 00                	push   $0x0
     890:	e8 2c fb ff ff       	call   3c1 <_test_allocation_generic>
}
     895:	c9                   	leave  
     896:	c3                   	ret    

00000897 <test_allocation_then_fork>:

int test_allocation_then_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2, int check_zero, int write_after) {
     897:	55                   	push   %ebp
     898:	89 e5                	mov    %esp,%ebp
     89a:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(0, 1, size, describe_size, describe_amount, offset1, count1, offset2, count2, check_zero, write_after);
     89d:	ff 75 28             	pushl  0x28(%ebp)
     8a0:	ff 75 24             	pushl  0x24(%ebp)
     8a3:	ff 75 20             	pushl  0x20(%ebp)
     8a6:	ff 75 1c             	pushl  0x1c(%ebp)
     8a9:	ff 75 18             	pushl  0x18(%ebp)
     8ac:	ff 75 14             	pushl  0x14(%ebp)
     8af:	ff 75 10             	pushl  0x10(%ebp)
     8b2:	ff 75 0c             	pushl  0xc(%ebp)
     8b5:	ff 75 08             	pushl  0x8(%ebp)
     8b8:	6a 01                	push   $0x1
     8ba:	6a 00                	push   $0x0
     8bc:	e8 00 fb ff ff       	call   3c1 <_test_allocation_generic>
}
     8c1:	c9                   	leave  
     8c2:	c3                   	ret    

000008c3 <test_allocation_fork>:


int test_allocation_fork(int size, const char *describe_size, const char *describe_amount, int offset1, int count1, int offset2, int count2) {
     8c3:	55                   	push   %ebp
     8c4:	89 e5                	mov    %esp,%ebp
     8c6:	83 ec 0c             	sub    $0xc,%esp
    return _test_allocation_generic(1, 0, size, describe_size, describe_amount, offset1, count1, offset2, count2, 1, 0);
     8c9:	6a 00                	push   $0x0
     8cb:	6a 01                	push   $0x1
     8cd:	ff 75 20             	pushl  0x20(%ebp)
     8d0:	ff 75 1c             	pushl  0x1c(%ebp)
     8d3:	ff 75 18             	pushl  0x18(%ebp)
     8d6:	ff 75 14             	pushl  0x14(%ebp)
     8d9:	ff 75 10             	pushl  0x10(%ebp)
     8dc:	ff 75 0c             	pushl  0xc(%ebp)
     8df:	ff 75 08             	pushl  0x8(%ebp)
     8e2:	6a 00                	push   $0x0
     8e4:	6a 01                	push   $0x1
     8e6:	e8 d6 fa ff ff       	call   3c1 <_test_allocation_generic>
}
     8eb:	c9                   	leave  
     8ec:	c3                   	ret    

000008ed <wait_forever>:

void wait_forever() {
     8ed:	55                   	push   %ebp
     8ee:	89 e5                	mov    %esp,%ebp
     8f0:	83 ec 08             	sub    $0x8,%esp
  while (1) { sleep(1000); }
     8f3:	83 ec 0c             	sub    $0xc,%esp
     8f6:	68 e8 03 00 00       	push   $0x3e8
     8fb:	e8 13 14 00 00       	call   1d13 <sleep>
     900:	83 c4 10             	add    $0x10,%esp
     903:	eb ee                	jmp    8f3 <wait_forever+0x6>

00000905 <test_copy_on_write_main_child>:
}

void test_copy_on_write_main_child(int result_fd, int size, const char *describe_size, int forks) {
     905:	55                   	push   %ebp
     906:	89 e5                	mov    %esp,%ebp
     908:	57                   	push   %edi
     909:	56                   	push   %esi
     90a:	53                   	push   %ebx
     90b:	83 ec 78             	sub    $0x78,%esp
  char *old_end_of_heap = sbrk(size);
     90e:	ff 75 0c             	pushl  0xc(%ebp)
     911:	e8 f5 13 00 00       	call   1d0b <sbrk>
     916:	89 c6                	mov    %eax,%esi
  char *new_end_of_heap = sbrk(0);
     918:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     91f:	e8 e7 13 00 00       	call   1d0b <sbrk>
     924:	89 c2                	mov    %eax,%edx
     926:	89 45 8c             	mov    %eax,-0x74(%ebp)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     929:	83 c4 10             	add    $0x10,%esp
     92c:	89 f0                	mov    %esi,%eax
     92e:	eb 06                	jmp    936 <test_copy_on_write_main_child+0x31>
      *p = 'A';
     930:	c6 00 41             	movb   $0x41,(%eax)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     933:	83 c0 01             	add    $0x1,%eax
     936:	39 d0                	cmp    %edx,%eax
     938:	72 f6                	jb     930 <test_copy_on_write_main_child+0x2b>
  }
  int children[MAX_CHILDREN] = {0};
     93a:	8d 7d a8             	lea    -0x58(%ebp),%edi
     93d:	b9 10 00 00 00       	mov    $0x10,%ecx
     942:	b8 00 00 00 00       	mov    $0x0,%eax
     947:	f3 ab                	rep stos %eax,%es:(%edi)
  if (forks > MAX_CHILDREN) {
     949:	83 7d 14 10          	cmpl   $0x10,0x14(%ebp)
     94d:	7f 31                	jg     980 <test_copy_on_write_main_child+0x7b>
    printf(2, "unsupported number of children in test_copy_on_write\n");
  }
  int failed = 0;
  char failed_code = ' ';
  dump_for("copy-write-parent-before", getpid());
     94f:	e8 af 13 00 00       	call   1d03 <getpid>
     954:	83 ec 08             	sub    $0x8,%esp
     957:	50                   	push   %eax
     958:	68 c2 2d 00 00       	push   $0x2dc2
     95d:	e8 b9 f6 ff ff       	call   1b <dump_for>
  for (int i = 0; i < forks; ++i) {
     962:	83 c4 10             	add    $0x10,%esp
     965:	bf 00 00 00 00       	mov    $0x0,%edi
  char failed_code = ' ';
     96a:	c6 45 90 20          	movb   $0x20,-0x70(%ebp)
  int failed = 0;
     96e:	c7 45 94 00 00 00 00 	movl   $0x0,-0x6c(%ebp)
     975:	89 75 88             	mov    %esi,-0x78(%ebp)
     978:	8b 75 14             	mov    0x14(%ebp),%esi
  for (int i = 0; i < forks; ++i) {
     97b:	e9 28 02 00 00       	jmp    ba8 <test_copy_on_write_main_child+0x2a3>
    printf(2, "unsupported number of children in test_copy_on_write\n");
     980:	83 ec 08             	sub    $0x8,%esp
     983:	68 88 25 00 00       	push   $0x2588
     988:	6a 02                	push   $0x2
     98a:	e8 66 14 00 00       	call   1df5 <printf>
     98f:	83 c4 10             	add    $0x10,%esp
     992:	eb bb                	jmp    94f <test_copy_on_write_main_child+0x4a>
     994:	8b 75 88             	mov    -0x78(%ebp),%esi
    int child_fds[2];
    pipe(child_fds);
    children[i] = fork();
    if (children[i] == -1) {
      printf(2, "fork failed\n");
     997:	83 ec 08             	sub    $0x8,%esp
     99a:	68 d0 2c 00 00       	push   $0x2cd0
     99f:	6a 02                	push   $0x2
     9a1:	e8 4f 14 00 00       	call   1df5 <printf>
      failed = 1;
      failed_code = 'f';
      break;
     9a6:	83 c4 10             	add    $0x10,%esp
      failed_code = 'f';
     9a9:	c6 45 90 66          	movb   $0x66,-0x70(%ebp)
      failed = 1;
     9ad:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     9b4:	e9 53 02 00 00       	jmp    c0c <test_copy_on_write_main_child+0x307>
     9b9:	8b 75 88             	mov    -0x78(%ebp),%esi
    } else if (children[i] == 0) {
      dump_for("copy-write-child-before-writes", getpid());
     9bc:	e8 42 13 00 00       	call   1d03 <getpid>
     9c1:	83 ec 08             	sub    $0x8,%esp
     9c4:	50                   	push   %eax
     9c5:	68 c0 25 00 00       	push   $0x25c0
     9ca:	e8 4c f6 ff ff       	call   1b <dump_for>
      int found_wrong_memory = 0;
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     9cf:	83 c4 10             	add    $0x10,%esp
     9d2:	89 f0                	mov    %esi,%eax
     9d4:	eb 03                	jmp    9d9 <test_copy_on_write_main_child+0xd4>
     9d6:	83 c0 01             	add    $0x1,%eax
     9d9:	3b 45 8c             	cmp    -0x74(%ebp),%eax
     9dc:	73 0c                	jae    9ea <test_copy_on_write_main_child+0xe5>
        if (*p != 'A') {
     9de:	80 38 41             	cmpb   $0x41,(%eax)
     9e1:	74 f3                	je     9d6 <test_copy_on_write_main_child+0xd1>
          found_wrong_memory = 1;
     9e3:	bb 01 00 00 00       	mov    $0x1,%ebx
     9e8:	eb ec                	jmp    9d6 <test_copy_on_write_main_child+0xd1>
        }
      }
      int place_one = size / 2;
     9ea:	8b 45 0c             	mov    0xc(%ebp),%eax
     9ed:	c1 e8 1f             	shr    $0x1f,%eax
     9f0:	03 45 0c             	add    0xc(%ebp),%eax
     9f3:	d1 f8                	sar    %eax
      old_end_of_heap[place_one] = 'B' + i;
     9f5:	01 f0                	add    %esi,%eax
     9f7:	89 c1                	mov    %eax,%ecx
     9f9:	89 45 94             	mov    %eax,-0x6c(%ebp)
     9fc:	8d 47 42             	lea    0x42(%edi),%eax
     9ff:	88 01                	mov    %al,(%ecx)
      int place_two = 4096 * i;
     a01:	89 f8                	mov    %edi,%eax
     a03:	c1 e0 0c             	shl    $0xc,%eax
     a06:	89 45 90             	mov    %eax,-0x70(%ebp)
      if (place_two >= size) {
     a09:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a0c:	7f 09                	jg     a17 <test_copy_on_write_main_child+0x112>
          place_two = size - 1;
     a0e:	8b 45 0c             	mov    0xc(%ebp),%eax
     a11:	83 e8 01             	sub    $0x1,%eax
     a14:	89 45 90             	mov    %eax,-0x70(%ebp)
      }
      if (size <= 4096) {
     a17:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a1e:	0f 8f b8 00 00 00    	jg     adc <test_copy_on_write_main_child+0x1d7>
          dump_for("copy-write-child-after-first-write", getpid());
     a24:	e8 da 12 00 00       	call   1d03 <getpid>
     a29:	83 ec 08             	sub    $0x8,%esp
     a2c:	50                   	push   %eax
     a2d:	68 e0 25 00 00       	push   $0x25e0
     a32:	e8 e4 f5 ff ff       	call   1b <dump_for>
     a37:	83 c4 10             	add    $0x10,%esp
      } else if (size > 4096) {
          dump_for("copy-write-child-after-write-1", getpid());
      }
      old_end_of_heap[place_two] = 'C';
     a3a:	8b 45 90             	mov    -0x70(%ebp),%eax
     a3d:	c6 04 06 43          	movb   $0x43,(%esi,%eax,1)
      int place_three = 4096 * (i - 1);
     a41:	8d 47 ff             	lea    -0x1(%edi),%eax
     a44:	c1 e0 0c             	shl    $0xc,%eax
     a47:	89 45 90             	mov    %eax,-0x70(%ebp)
      if (place_three >= size || place_three < 0) {
     a4a:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a4d:	0f 9e c2             	setle  %dl
     a50:	c1 e8 1f             	shr    $0x1f,%eax
     a53:	08 c2                	or     %al,%dl
     a55:	74 09                	je     a60 <test_copy_on_write_main_child+0x15b>
          place_three = size - 2;
     a57:	8b 45 0c             	mov    0xc(%ebp),%eax
     a5a:	83 e8 02             	sub    $0x2,%eax
     a5d:	89 45 90             	mov    %eax,-0x70(%ebp)
      }
      if (size > 4096) {
     a60:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a67:	0f 8f 8a 00 00 00    	jg     af7 <test_copy_on_write_main_child+0x1f2>
          dump_for("copy-write-child-after-write-2", getpid());
      }
      int place_four = 4096 * (i + 1);
     a6d:	8d 47 01             	lea    0x1(%edi),%eax
     a70:	c1 e0 0c             	shl    $0xc,%eax
     a73:	89 45 8c             	mov    %eax,-0x74(%ebp)
      if (place_four >= size) {
     a76:	39 45 0c             	cmp    %eax,0xc(%ebp)
     a79:	7f 09                	jg     a84 <test_copy_on_write_main_child+0x17f>
          place_four = size - 3;
     a7b:	8b 45 0c             	mov    0xc(%ebp),%eax
     a7e:	83 e8 03             	sub    $0x3,%eax
     a81:	89 45 8c             	mov    %eax,-0x74(%ebp)
      }
      if (size > 4096) {
     a84:	81 7d 0c 00 10 00 00 	cmpl   $0x1000,0xc(%ebp)
     a8b:	0f 8f 81 00 00 00    	jg     b12 <test_copy_on_write_main_child+0x20d>
          dump_for("copy-write-child-after-write-3", getpid());
      }
      printf(1, "[Debugging info: three: %c; one: %c; four: %c; already_wrong: %d; i: %d]\n",
        old_end_of_heap[place_three],
        old_end_of_heap[place_one],
        old_end_of_heap[place_four],
     a91:	8b 45 8c             	mov    -0x74(%ebp),%eax
     a94:	01 f0                	add    %esi,%eax
     a96:	89 45 8c             	mov    %eax,-0x74(%ebp)
        old_end_of_heap[place_three],
     a99:	03 75 90             	add    -0x70(%ebp),%esi
      printf(1, "[Debugging info: three: %c; one: %c; four: %c; already_wrong: %d; i: %d]\n",
     a9c:	83 ec 04             	sub    $0x4,%esp
     a9f:	57                   	push   %edi
     aa0:	53                   	push   %ebx
     aa1:	0f be 00             	movsbl (%eax),%eax
     aa4:	50                   	push   %eax
     aa5:	8b 45 94             	mov    -0x6c(%ebp),%eax
     aa8:	0f be 00             	movsbl (%eax),%eax
     aab:	50                   	push   %eax
     aac:	0f be 06             	movsbl (%esi),%eax
     aaf:	50                   	push   %eax
     ab0:	68 64 26 00 00       	push   $0x2664
     ab5:	6a 01                	push   $0x1
     ab7:	e8 39 13 00 00       	call   1df5 <printf>
        found_wrong_memory,
        i);
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     abc:	83 c4 20             	add    $0x20,%esp
     abf:	80 3e 41             	cmpb   $0x41,(%esi)
     ac2:	74 69                	je     b2d <test_copy_on_write_main_child+0x228>
          old_end_of_heap[place_four] != 'A') {
          found_wrong_memory = 1;
      }
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     ac4:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     ac9:	83 ec 04             	sub    $0x4,%esp
     acc:	6a 01                	push   $0x1
     ace:	50                   	push   %eax
     acf:	ff 75 a4             	pushl  -0x5c(%ebp)
     ad2:	e8 cc 11 00 00       	call   1ca3 <write>
      wait_forever();
     ad7:	e8 11 fe ff ff       	call   8ed <wait_forever>
          dump_for("copy-write-child-after-write-1", getpid());
     adc:	e8 22 12 00 00       	call   1d03 <getpid>
     ae1:	83 ec 08             	sub    $0x8,%esp
     ae4:	50                   	push   %eax
     ae5:	68 04 26 00 00       	push   $0x2604
     aea:	e8 2c f5 ff ff       	call   1b <dump_for>
     aef:	83 c4 10             	add    $0x10,%esp
     af2:	e9 43 ff ff ff       	jmp    a3a <test_copy_on_write_main_child+0x135>
          dump_for("copy-write-child-after-write-2", getpid());
     af7:	e8 07 12 00 00       	call   1d03 <getpid>
     afc:	83 ec 08             	sub    $0x8,%esp
     aff:	50                   	push   %eax
     b00:	68 24 26 00 00       	push   $0x2624
     b05:	e8 11 f5 ff ff       	call   1b <dump_for>
     b0a:	83 c4 10             	add    $0x10,%esp
     b0d:	e9 5b ff ff ff       	jmp    a6d <test_copy_on_write_main_child+0x168>
          dump_for("copy-write-child-after-write-3", getpid());
     b12:	e8 ec 11 00 00       	call   1d03 <getpid>
     b17:	83 ec 08             	sub    $0x8,%esp
     b1a:	50                   	push   %eax
     b1b:	68 44 26 00 00       	push   $0x2644
     b20:	e8 f6 f4 ff ff       	call   1b <dump_for>
     b25:	83 c4 10             	add    $0x10,%esp
     b28:	e9 64 ff ff ff       	jmp    a91 <test_copy_on_write_main_child+0x18c>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     b2d:	8b 45 94             	mov    -0x6c(%ebp),%eax
     b30:	0f be 00             	movsbl (%eax),%eax
     b33:	83 c7 42             	add    $0x42,%edi
     b36:	39 f8                	cmp    %edi,%eax
     b38:	74 07                	je     b41 <test_copy_on_write_main_child+0x23c>
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     b3a:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     b3f:	eb 88                	jmp    ac9 <test_copy_on_write_main_child+0x1c4>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     b41:	8b 45 8c             	mov    -0x74(%ebp),%eax
     b44:	80 38 41             	cmpb   $0x41,(%eax)
     b47:	75 0e                	jne    b57 <test_copy_on_write_main_child+0x252>
      write(child_fds[1], found_wrong_memory ? "-" : "+", 1);
     b49:	85 db                	test   %ebx,%ebx
     b4b:	74 14                	je     b61 <test_copy_on_write_main_child+0x25c>
     b4d:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     b52:	e9 72 ff ff ff       	jmp    ac9 <test_copy_on_write_main_child+0x1c4>
     b57:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     b5c:	e9 68 ff ff ff       	jmp    ac9 <test_copy_on_write_main_child+0x1c4>
     b61:	b8 c0 2d 00 00       	mov    $0x2dc0,%eax
     b66:	e9 5e ff ff ff       	jmp    ac9 <test_copy_on_write_main_child+0x1c4>
      read(child_fds[0], buffer, 1);
      if (buffer[0] != '+') {
        failed = 1;
        failed_code = 'c';
      }
      close(child_fds[0]); close(child_fds[1]);
     b6b:	83 ec 0c             	sub    $0xc,%esp
     b6e:	ff 75 a0             	pushl  -0x60(%ebp)
     b71:	e8 35 11 00 00       	call   1cab <close>
     b76:	83 c4 04             	add    $0x4,%esp
     b79:	ff 75 a4             	pushl  -0x5c(%ebp)
     b7c:	e8 2a 11 00 00       	call   1cab <close>
      dump_for("copy-write-parent-after", getpid());
     b81:	e8 7d 11 00 00       	call   1d03 <getpid>
     b86:	83 c4 08             	add    $0x8,%esp
     b89:	50                   	push   %eax
     b8a:	68 db 2d 00 00       	push   $0x2ddb
     b8f:	e8 87 f4 ff ff       	call   1b <dump_for>
      dump_for("copy-write-child-after", children[i]);
     b94:	83 c4 08             	add    $0x8,%esp
     b97:	53                   	push   %ebx
     b98:	68 f3 2d 00 00       	push   $0x2df3
     b9d:	e8 79 f4 ff ff       	call   1b <dump_for>
  for (int i = 0; i < forks; ++i) {
     ba2:	83 c7 01             	add    $0x1,%edi
     ba5:	83 c4 10             	add    $0x10,%esp
     ba8:	39 f7                	cmp    %esi,%edi
     baa:	7d 5d                	jge    c09 <test_copy_on_write_main_child+0x304>
    pipe(child_fds);
     bac:	83 ec 0c             	sub    $0xc,%esp
     baf:	8d 45 a0             	lea    -0x60(%ebp),%eax
     bb2:	50                   	push   %eax
     bb3:	e8 db 10 00 00       	call   1c93 <pipe>
    children[i] = fork();
     bb8:	e8 be 10 00 00       	call   1c7b <fork>
     bbd:	89 c3                	mov    %eax,%ebx
     bbf:	89 44 bd a8          	mov    %eax,-0x58(%ebp,%edi,4)
    if (children[i] == -1) {
     bc3:	83 c4 10             	add    $0x10,%esp
     bc6:	83 f8 ff             	cmp    $0xffffffff,%eax
     bc9:	0f 84 c5 fd ff ff    	je     994 <test_copy_on_write_main_child+0x8f>
    } else if (children[i] == 0) {
     bcf:	85 c0                	test   %eax,%eax
     bd1:	0f 84 e2 fd ff ff    	je     9b9 <test_copy_on_write_main_child+0xb4>
      char buffer[1] = {'X'};
     bd7:	c6 45 9f 58          	movb   $0x58,-0x61(%ebp)
      read(child_fds[0], buffer, 1);
     bdb:	83 ec 04             	sub    $0x4,%esp
     bde:	6a 01                	push   $0x1
     be0:	8d 45 9f             	lea    -0x61(%ebp),%eax
     be3:	50                   	push   %eax
     be4:	ff 75 a0             	pushl  -0x60(%ebp)
     be7:	e8 af 10 00 00       	call   1c9b <read>
      if (buffer[0] != '+') {
     bec:	83 c4 10             	add    $0x10,%esp
     bef:	80 7d 9f 2b          	cmpb   $0x2b,-0x61(%ebp)
     bf3:	0f 84 72 ff ff ff    	je     b6b <test_copy_on_write_main_child+0x266>
        failed_code = 'c';
     bf9:	c6 45 90 63          	movb   $0x63,-0x70(%ebp)
        failed = 1;
     bfd:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     c04:	e9 62 ff ff ff       	jmp    b6b <test_copy_on_write_main_child+0x266>
     c09:	8b 75 88             	mov    -0x78(%ebp),%esi
    }
  }
  for (int i = 0; i < forks; ++i) {
     c0c:	bb 00 00 00 00       	mov    $0x0,%ebx
     c11:	8b 7d 14             	mov    0x14(%ebp),%edi
     c14:	eb 17                	jmp    c2d <test_copy_on_write_main_child+0x328>
    kill(children[i]);
     c16:	83 ec 0c             	sub    $0xc,%esp
     c19:	ff 74 9d a8          	pushl  -0x58(%ebp,%ebx,4)
     c1d:	e8 91 10 00 00       	call   1cb3 <kill>
    wait();
     c22:	e8 64 10 00 00       	call   1c8b <wait>
  for (int i = 0; i < forks; ++i) {
     c27:	83 c3 01             	add    $0x1,%ebx
     c2a:	83 c4 10             	add    $0x10,%esp
     c2d:	39 fb                	cmp    %edi,%ebx
     c2f:	7c e5                	jl     c16 <test_copy_on_write_main_child+0x311>
     c31:	8b 45 8c             	mov    -0x74(%ebp),%eax
     c34:	eb 03                	jmp    c39 <test_copy_on_write_main_child+0x334>
  }
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     c36:	83 c6 01             	add    $0x1,%esi
     c39:	39 c6                	cmp    %eax,%esi
     c3b:	73 12                	jae    c4f <test_copy_on_write_main_child+0x34a>
    if (*p != 'A') {
     c3d:	80 3e 41             	cmpb   $0x41,(%esi)
     c40:	74 f4                	je     c36 <test_copy_on_write_main_child+0x331>
      failed = 1;
      failed_code = 'p';
     c42:	c6 45 90 70          	movb   $0x70,-0x70(%ebp)
      failed = 1;
     c46:	c7 45 94 01 00 00 00 	movl   $0x1,-0x6c(%ebp)
     c4d:	eb e7                	jmp    c36 <test_copy_on_write_main_child+0x331>
    }
  }
  if (failed) {
     c4f:	83 7d 94 00          	cmpl   $0x0,-0x6c(%ebp)
     c53:	75 1d                	jne    c72 <test_copy_on_write_main_child+0x36d>
    char buffer[2] = {'N', ' '};
    buffer[1] = failed_code;
    write(result_fd, buffer, 2);
  } else {
    write(result_fd, "YY", 2);
     c55:	83 ec 04             	sub    $0x4,%esp
     c58:	6a 02                	push   $0x2
     c5a:	68 0a 2e 00 00       	push   $0x2e0a
     c5f:	ff 75 08             	pushl  0x8(%ebp)
     c62:	e8 3c 10 00 00       	call   1ca3 <write>
     c67:	83 c4 10             	add    $0x10,%esp
  }
}
     c6a:	8d 65 f4             	lea    -0xc(%ebp),%esp
     c6d:	5b                   	pop    %ebx
     c6e:	5e                   	pop    %esi
     c6f:	5f                   	pop    %edi
     c70:	5d                   	pop    %ebp
     c71:	c3                   	ret    
    char buffer[2] = {'N', ' '};
     c72:	c6 45 a0 4e          	movb   $0x4e,-0x60(%ebp)
    buffer[1] = failed_code;
     c76:	0f b6 45 90          	movzbl -0x70(%ebp),%eax
     c7a:	88 45 a1             	mov    %al,-0x5f(%ebp)
    write(result_fd, buffer, 2);
     c7d:	83 ec 04             	sub    $0x4,%esp
     c80:	6a 02                	push   $0x2
     c82:	8d 45 a0             	lea    -0x60(%ebp),%eax
     c85:	50                   	push   %eax
     c86:	ff 75 08             	pushl  0x8(%ebp)
     c89:	e8 15 10 00 00       	call   1ca3 <write>
     c8e:	83 c4 10             	add    $0x10,%esp
     c91:	eb d7                	jmp    c6a <test_copy_on_write_main_child+0x365>

00000c93 <test_copy_on_write_main_child_alt>:

void test_copy_on_write_main_child_alt(int result_fd, int size, const char *describe_size, int forks, int early_term) {
     c93:	55                   	push   %ebp
     c94:	89 e5                	mov    %esp,%ebp
     c96:	57                   	push   %edi
     c97:	56                   	push   %esi
     c98:	53                   	push   %ebx
     c99:	81 ec f8 00 00 00    	sub    $0xf8,%esp
  char *old_end_of_heap = sbrk(size);
     c9f:	ff 75 0c             	pushl  0xc(%ebp)
     ca2:	e8 64 10 00 00       	call   1d0b <sbrk>
     ca7:	89 c6                	mov    %eax,%esi
  char *new_end_of_heap = sbrk(0);
     ca9:	c7 04 24 00 00 00 00 	movl   $0x0,(%esp)
     cb0:	e8 56 10 00 00       	call   1d0b <sbrk>
     cb5:	89 c2                	mov    %eax,%edx
     cb7:	89 85 0c ff ff ff    	mov    %eax,-0xf4(%ebp)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     cbd:	83 c4 10             	add    $0x10,%esp
     cc0:	89 f0                	mov    %esi,%eax
     cc2:	eb 06                	jmp    cca <test_copy_on_write_main_child_alt+0x37>
      *p = 'A';
     cc4:	c6 00 41             	movb   $0x41,(%eax)
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     cc7:	83 c0 01             	add    $0x1,%eax
     cca:	39 d0                	cmp    %edx,%eax
     ccc:	72 f6                	jb     cc4 <test_copy_on_write_main_child_alt+0x31>
  }
  int children[MAX_CHILDREN] = {0};
     cce:	8d 7d a8             	lea    -0x58(%ebp),%edi
     cd1:	b9 10 00 00 00       	mov    $0x10,%ecx
     cd6:	b8 00 00 00 00       	mov    $0x0,%eax
     cdb:	f3 ab                	rep stos %eax,%es:(%edi)
  int child_fds[MAX_CHILDREN][2];
  if (forks > MAX_CHILDREN) {
     cdd:	83 7d 14 10          	cmpl   $0x10,0x14(%ebp)
     ce1:	7f 43                	jg     d26 <test_copy_on_write_main_child_alt+0x93>
      if (old_end_of_heap[place_three] != 'A' || 
          old_end_of_heap[place_four] != 'A' ||
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
          found_wrong_memory = 1;
      }
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     ce3:	bb 00 00 00 00       	mov    $0x0,%ebx
     ce8:	8b 7d 14             	mov    0x14(%ebp),%edi
  for (int i = 0; i < forks; ++i) {
     ceb:	39 fb                	cmp    %edi,%ebx
     ced:	0f 8d e6 01 00 00    	jge    ed9 <test_copy_on_write_main_child_alt+0x246>
    sleep(1);
     cf3:	83 ec 0c             	sub    $0xc,%esp
     cf6:	6a 01                	push   $0x1
     cf8:	e8 16 10 00 00       	call   1d13 <sleep>
    pipe(child_fds[i]);
     cfd:	8d 84 dd 28 ff ff ff 	lea    -0xd8(%ebp,%ebx,8),%eax
     d04:	89 04 24             	mov    %eax,(%esp)
     d07:	e8 87 0f 00 00       	call   1c93 <pipe>
    children[i] = fork();
     d0c:	e8 6a 0f 00 00       	call   1c7b <fork>
     d11:	89 44 9d a8          	mov    %eax,-0x58(%ebp,%ebx,4)
    if (children[i] == -1) {
     d15:	83 c4 10             	add    $0x10,%esp
     d18:	83 f8 ff             	cmp    $0xffffffff,%eax
     d1b:	74 1d                	je     d3a <test_copy_on_write_main_child_alt+0xa7>
    } else if (children[i] == 0) {
     d1d:	85 c0                	test   %eax,%eax
     d1f:	74 54                	je     d75 <test_copy_on_write_main_child_alt+0xe2>
  for (int i = 0; i < forks; ++i) {
     d21:	83 c3 01             	add    $0x1,%ebx
     d24:	eb c5                	jmp    ceb <test_copy_on_write_main_child_alt+0x58>
    printf(2, "unsupported number of children in test_copy_on_write\n");
     d26:	83 ec 08             	sub    $0x8,%esp
     d29:	68 88 25 00 00       	push   $0x2588
     d2e:	6a 02                	push   $0x2
     d30:	e8 c0 10 00 00       	call   1df5 <printf>
     d35:	83 c4 10             	add    $0x10,%esp
     d38:	eb a9                	jmp    ce3 <test_copy_on_write_main_child_alt+0x50>
      printf(2, "fork failed\n");
     d3a:	83 ec 08             	sub    $0x8,%esp
     d3d:	68 d0 2c 00 00       	push   $0x2cd0
     d42:	6a 02                	push   $0x2
     d44:	e8 ac 10 00 00       	call   1df5 <printf>
      break;
     d49:	83 c4 10             	add    $0x10,%esp
      failed_code = 'f';
     d4c:	c6 85 10 ff ff ff 66 	movb   $0x66,-0xf0(%ebp)
      failed = 1;
     d53:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     d5a:	00 00 00 
      break;
     d5d:	e9 88 01 00 00       	jmp    eea <test_copy_on_write_main_child_alt+0x257>
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     d62:	83 c0 01             	add    $0x1,%eax
     d65:	39 d0                	cmp    %edx,%eax
     d67:	73 18                	jae    d81 <test_copy_on_write_main_child_alt+0xee>
        if (*p != 'A') {
     d69:	80 38 41             	cmpb   $0x41,(%eax)
     d6c:	74 f4                	je     d62 <test_copy_on_write_main_child_alt+0xcf>
          found_wrong_memory = 1;
     d6e:	bf 01 00 00 00       	mov    $0x1,%edi
     d73:	eb ed                	jmp    d62 <test_copy_on_write_main_child_alt+0xcf>
     d75:	89 c7                	mov    %eax,%edi
      for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     d77:	89 f0                	mov    %esi,%eax
     d79:	8b 95 0c ff ff ff    	mov    -0xf4(%ebp),%edx
     d7f:	eb e4                	jmp    d65 <test_copy_on_write_main_child_alt+0xd2>
      int place_one = size / 2;
     d81:	8b 45 0c             	mov    0xc(%ebp),%eax
     d84:	c1 e8 1f             	shr    $0x1f,%eax
     d87:	03 45 0c             	add    0xc(%ebp),%eax
     d8a:	d1 f8                	sar    %eax
      old_end_of_heap[place_one] = 'B' + i;
     d8c:	89 da                	mov    %ebx,%edx
     d8e:	01 f0                	add    %esi,%eax
     d90:	89 c1                	mov    %eax,%ecx
     d92:	89 85 0c ff ff ff    	mov    %eax,-0xf4(%ebp)
     d98:	8d 43 42             	lea    0x42(%ebx),%eax
     d9b:	88 01                	mov    %al,(%ecx)
      int place_two = 4096 * i;
     d9d:	89 d8                	mov    %ebx,%eax
     d9f:	c1 e0 0c             	shl    $0xc,%eax
      if (place_two >= size) {
     da2:	39 45 0c             	cmp    %eax,0xc(%ebp)
     da5:	7f 06                	jg     dad <test_copy_on_write_main_child_alt+0x11a>
          place_two = size - 1;
     da7:	8b 45 0c             	mov    0xc(%ebp),%eax
     daa:	83 e8 01             	sub    $0x1,%eax
      old_end_of_heap[place_two] = 'C' + i;
     dad:	01 f0                	add    %esi,%eax
     daf:	89 85 08 ff ff ff    	mov    %eax,-0xf8(%ebp)
     db5:	83 c2 43             	add    $0x43,%edx
     db8:	88 10                	mov    %dl,(%eax)
      int place_three = 4096 * (i - 1);
     dba:	8d 43 ff             	lea    -0x1(%ebx),%eax
     dbd:	c1 e0 0c             	shl    $0xc,%eax
     dc0:	89 c1                	mov    %eax,%ecx
      if (place_three >= size || place_three < 0) {
     dc2:	39 45 0c             	cmp    %eax,0xc(%ebp)
     dc5:	0f 9e c2             	setle  %dl
     dc8:	c1 e8 1f             	shr    $0x1f,%eax
     dcb:	08 c2                	or     %al,%dl
     dcd:	74 06                	je     dd5 <test_copy_on_write_main_child_alt+0x142>
          place_three = size - 2;
     dcf:	8b 45 0c             	mov    0xc(%ebp),%eax
     dd2:	8d 48 fe             	lea    -0x2(%eax),%ecx
      int place_four = 4096 * (i + 1);
     dd5:	8d 43 01             	lea    0x1(%ebx),%eax
     dd8:	c1 e0 0c             	shl    $0xc,%eax
     ddb:	89 85 10 ff ff ff    	mov    %eax,-0xf0(%ebp)
      if (place_four >= size || place_four < 0) {
     de1:	39 45 0c             	cmp    %eax,0xc(%ebp)
     de4:	0f 9e c2             	setle  %dl
     de7:	c1 e8 1f             	shr    $0x1f,%eax
     dea:	08 c2                	or     %al,%dl
     dec:	74 0c                	je     dfa <test_copy_on_write_main_child_alt+0x167>
          place_four = size - 3;
     dee:	8b 45 0c             	mov    0xc(%ebp),%eax
     df1:	83 e8 03             	sub    $0x3,%eax
     df4:	89 85 10 ff ff ff    	mov    %eax,-0xf0(%ebp)
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     dfa:	8d 04 0e             	lea    (%esi,%ecx,1),%eax
     dfd:	89 85 14 ff ff ff    	mov    %eax,-0xec(%ebp)
     e03:	80 38 41             	cmpb   $0x41,(%eax)
     e06:	74 46                	je     e4e <test_copy_on_write_main_child_alt+0x1bb>
          found_wrong_memory = 1;
     e08:	bf 01 00 00 00       	mov    $0x1,%edi
      sleep(5);
     e0d:	83 ec 0c             	sub    $0xc,%esp
     e10:	6a 05                	push   $0x5
     e12:	e8 fc 0e 00 00       	call   1d13 <sleep>
      if (old_end_of_heap[place_three] != 'A' || 
     e17:	83 c4 10             	add    $0x10,%esp
     e1a:	8b 85 14 ff ff ff    	mov    -0xec(%ebp),%eax
     e20:	80 38 41             	cmpb   $0x41,(%eax)
     e23:	74 53                	je     e78 <test_copy_on_write_main_child_alt+0x1e5>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     e25:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     e2a:	83 ec 04             	sub    $0x4,%esp
     e2d:	6a 01                	push   $0x1
     e2f:	50                   	push   %eax
     e30:	ff b4 dd 2c ff ff ff 	pushl  -0xd4(%ebp,%ebx,8)
     e37:	e8 67 0e 00 00       	call   1ca3 <write>
      if (early_term) {
     e3c:	83 c4 10             	add    $0x10,%esp
     e3f:	83 7d 18 00          	cmpl   $0x0,0x18(%ebp)
     e43:	0f 84 8b 00 00 00    	je     ed4 <test_copy_on_write_main_child_alt+0x241>
          exit();
     e49:	e8 35 0e 00 00       	call   1c83 <exit>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     e4e:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     e54:	0f be 10             	movsbl (%eax),%edx
     e57:	8d 43 42             	lea    0x42(%ebx),%eax
     e5a:	39 c2                	cmp    %eax,%edx
     e5c:	74 07                	je     e65 <test_copy_on_write_main_child_alt+0x1d2>
          found_wrong_memory = 1;
     e5e:	bf 01 00 00 00       	mov    $0x1,%edi
     e63:	eb a8                	jmp    e0d <test_copy_on_write_main_child_alt+0x17a>
      if (old_end_of_heap[place_three] != 'A' || old_end_of_heap[place_one] != 'B' + i ||
     e65:	8b 85 10 ff ff ff    	mov    -0xf0(%ebp),%eax
     e6b:	80 3c 06 41          	cmpb   $0x41,(%esi,%eax,1)
     e6f:	74 9c                	je     e0d <test_copy_on_write_main_child_alt+0x17a>
          found_wrong_memory = 1;
     e71:	bf 01 00 00 00       	mov    $0x1,%edi
     e76:	eb 95                	jmp    e0d <test_copy_on_write_main_child_alt+0x17a>
      if (old_end_of_heap[place_three] != 'A' || 
     e78:	8b 85 10 ff ff ff    	mov    -0xf0(%ebp),%eax
     e7e:	80 3c 06 41          	cmpb   $0x41,(%esi,%eax,1)
     e82:	74 07                	je     e8b <test_copy_on_write_main_child_alt+0x1f8>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     e84:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     e89:	eb 9f                	jmp    e2a <test_copy_on_write_main_child_alt+0x197>
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
     e8b:	8b 85 08 ff ff ff    	mov    -0xf8(%ebp),%eax
     e91:	0f be 10             	movsbl (%eax),%edx
     e94:	8d 43 43             	lea    0x43(%ebx),%eax
          old_end_of_heap[place_four] != 'A' ||
     e97:	39 c2                	cmp    %eax,%edx
     e99:	74 07                	je     ea2 <test_copy_on_write_main_child_alt+0x20f>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     e9b:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     ea0:	eb 88                	jmp    e2a <test_copy_on_write_main_child_alt+0x197>
          old_end_of_heap[place_two] != 'C' + i || old_end_of_heap[place_one] != 'B' + i) {
     ea2:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     ea8:	0f be 10             	movsbl (%eax),%edx
     eab:	8d 43 42             	lea    0x42(%ebx),%eax
     eae:	39 c2                	cmp    %eax,%edx
     eb0:	74 0a                	je     ebc <test_copy_on_write_main_child_alt+0x229>
      write(child_fds[i][1], found_wrong_memory ? "-" : "+", 1);
     eb2:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     eb7:	e9 6e ff ff ff       	jmp    e2a <test_copy_on_write_main_child_alt+0x197>
     ebc:	85 ff                	test   %edi,%edi
     ebe:	74 0a                	je     eca <test_copy_on_write_main_child_alt+0x237>
     ec0:	b8 be 2d 00 00       	mov    $0x2dbe,%eax
     ec5:	e9 60 ff ff ff       	jmp    e2a <test_copy_on_write_main_child_alt+0x197>
     eca:	b8 c0 2d 00 00       	mov    $0x2dc0,%eax
     ecf:	e9 56 ff ff ff       	jmp    e2a <test_copy_on_write_main_child_alt+0x197>
      } else {
          wait_forever();
     ed4:	e8 14 fa ff ff       	call   8ed <wait_forever>
  char failed_code = ' ';
     ed9:	c6 85 10 ff ff ff 20 	movb   $0x20,-0xf0(%ebp)
  int failed = 0;
     ee0:	c7 85 14 ff ff ff 00 	movl   $0x0,-0xec(%ebp)
     ee7:	00 00 00 
      failed = 1;
     eea:	bb 00 00 00 00       	mov    $0x0,%ebx
     eef:	89 b5 08 ff ff ff    	mov    %esi,-0xf8(%ebp)
     ef5:	8b 75 14             	mov    0x14(%ebp),%esi
     ef8:	eb 43                	jmp    f3d <test_copy_on_write_main_child_alt+0x2aa>
    if (children[i] != -1) {
      char buffer[1] = {'X'};
      read(child_fds[i][0], buffer, 1);
      if (buffer[0] == 'X') {
        failed = 1;
        failed_code = 'P';
     efa:	c6 85 10 ff ff ff 50 	movb   $0x50,-0xf0(%ebp)
        failed = 1;
     f01:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     f08:	00 00 00 
      } else if (buffer[0] != '+') {
        failed = 1;
        failed_code = 'c';
      }
      close(child_fds[i][0]); close(child_fds[i][1]);
     f0b:	83 ec 0c             	sub    $0xc,%esp
     f0e:	ff b4 dd 28 ff ff ff 	pushl  -0xd8(%ebp,%ebx,8)
     f15:	e8 91 0d 00 00       	call   1cab <close>
     f1a:	83 c4 04             	add    $0x4,%esp
     f1d:	ff b4 dd 2c ff ff ff 	pushl  -0xd4(%ebp,%ebx,8)
     f24:	e8 82 0d 00 00       	call   1cab <close>
      dump_for("copy-write-child", children[i]);
     f29:	83 c4 08             	add    $0x8,%esp
     f2c:	57                   	push   %edi
     f2d:	68 0d 2e 00 00       	push   $0x2e0d
     f32:	e8 e4 f0 ff ff       	call   1b <dump_for>
     f37:	83 c4 10             	add    $0x10,%esp
  for (int i = 0; i < forks; ++i) {
     f3a:	83 c3 01             	add    $0x1,%ebx
     f3d:	39 f3                	cmp    %esi,%ebx
     f3f:	7d 50                	jge    f91 <test_copy_on_write_main_child_alt+0x2fe>
    if (children[i] != -1) {
     f41:	8b 7c 9d a8          	mov    -0x58(%ebp,%ebx,4),%edi
     f45:	83 ff ff             	cmp    $0xffffffff,%edi
     f48:	74 f0                	je     f3a <test_copy_on_write_main_child_alt+0x2a7>
      char buffer[1] = {'X'};
     f4a:	c6 85 26 ff ff ff 58 	movb   $0x58,-0xda(%ebp)
      read(child_fds[i][0], buffer, 1);
     f51:	83 ec 04             	sub    $0x4,%esp
     f54:	6a 01                	push   $0x1
     f56:	8d 85 26 ff ff ff    	lea    -0xda(%ebp),%eax
     f5c:	50                   	push   %eax
     f5d:	ff b4 dd 28 ff ff ff 	pushl  -0xd8(%ebp,%ebx,8)
     f64:	e8 32 0d 00 00       	call   1c9b <read>
      if (buffer[0] == 'X') {
     f69:	0f b6 85 26 ff ff ff 	movzbl -0xda(%ebp),%eax
     f70:	83 c4 10             	add    $0x10,%esp
     f73:	3c 58                	cmp    $0x58,%al
     f75:	74 83                	je     efa <test_copy_on_write_main_child_alt+0x267>
      } else if (buffer[0] != '+') {
     f77:	3c 2b                	cmp    $0x2b,%al
     f79:	74 90                	je     f0b <test_copy_on_write_main_child_alt+0x278>
        failed_code = 'c';
     f7b:	c6 85 10 ff ff ff 63 	movb   $0x63,-0xf0(%ebp)
        failed = 1;
     f82:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     f89:	00 00 00 
     f8c:	e9 7a ff ff ff       	jmp    f0b <test_copy_on_write_main_child_alt+0x278>
     f91:	8b b5 08 ff ff ff    	mov    -0xf8(%ebp),%esi
    }
  }
  dump_for("copy-write-parent", getpid());
     f97:	e8 67 0d 00 00       	call   1d03 <getpid>
     f9c:	83 ec 08             	sub    $0x8,%esp
     f9f:	50                   	push   %eax
     fa0:	68 1e 2e 00 00       	push   $0x2e1e
     fa5:	e8 71 f0 ff ff       	call   1b <dump_for>
  for (int i = 0; i < forks; ++i) {
     faa:	83 c4 10             	add    $0x10,%esp
     fad:	bb 00 00 00 00       	mov    $0x0,%ebx
     fb2:	8b 7d 14             	mov    0x14(%ebp),%edi
     fb5:	eb 17                	jmp    fce <test_copy_on_write_main_child_alt+0x33b>
    kill(children[i]);
     fb7:	83 ec 0c             	sub    $0xc,%esp
     fba:	ff 74 9d a8          	pushl  -0x58(%ebp,%ebx,4)
     fbe:	e8 f0 0c 00 00       	call   1cb3 <kill>
    wait();
     fc3:	e8 c3 0c 00 00       	call   1c8b <wait>
  for (int i = 0; i < forks; ++i) {
     fc8:	83 c3 01             	add    $0x1,%ebx
     fcb:	83 c4 10             	add    $0x10,%esp
     fce:	39 fb                	cmp    %edi,%ebx
     fd0:	7c e5                	jl     fb7 <test_copy_on_write_main_child_alt+0x324>
     fd2:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
     fd8:	eb 03                	jmp    fdd <test_copy_on_write_main_child_alt+0x34a>
  }
  for (char *p = old_end_of_heap; p < new_end_of_heap; ++p) {
     fda:	83 c6 01             	add    $0x1,%esi
     fdd:	39 c6                	cmp    %eax,%esi
     fdf:	73 18                	jae    ff9 <test_copy_on_write_main_child_alt+0x366>
    if (*p != 'A') {
     fe1:	80 3e 41             	cmpb   $0x41,(%esi)
     fe4:	74 f4                	je     fda <test_copy_on_write_main_child_alt+0x347>
      failed = 1;
      failed_code = 'p';
     fe6:	c6 85 10 ff ff ff 70 	movb   $0x70,-0xf0(%ebp)
      failed = 1;
     fed:	c7 85 14 ff ff ff 01 	movl   $0x1,-0xec(%ebp)
     ff4:	00 00 00 
     ff7:	eb e1                	jmp    fda <test_copy_on_write_main_child_alt+0x347>
    }
  }
  if (failed) {
     ff9:	83 bd 14 ff ff ff 00 	cmpl   $0x0,-0xec(%ebp)
    1000:	75 1d                	jne    101f <test_copy_on_write_main_child_alt+0x38c>
    char buffer[2] = {'N', ' '};
    buffer[1] = failed_code;
    write(result_fd, buffer, 2);
  } else {
    write(result_fd, "YY", 2);
    1002:	83 ec 04             	sub    $0x4,%esp
    1005:	6a 02                	push   $0x2
    1007:	68 0a 2e 00 00       	push   $0x2e0a
    100c:	ff 75 08             	pushl  0x8(%ebp)
    100f:	e8 8f 0c 00 00       	call   1ca3 <write>
    1014:	83 c4 10             	add    $0x10,%esp
  }
}
    1017:	8d 65 f4             	lea    -0xc(%ebp),%esp
    101a:	5b                   	pop    %ebx
    101b:	5e                   	pop    %esi
    101c:	5f                   	pop    %edi
    101d:	5d                   	pop    %ebp
    101e:	c3                   	ret    
    char buffer[2] = {'N', ' '};
    101f:	c6 85 26 ff ff ff 4e 	movb   $0x4e,-0xda(%ebp)
    buffer[1] = failed_code;
    1026:	0f b6 85 10 ff ff ff 	movzbl -0xf0(%ebp),%eax
    102d:	88 85 27 ff ff ff    	mov    %al,-0xd9(%ebp)
    write(result_fd, buffer, 2);
    1033:	83 ec 04             	sub    $0x4,%esp
    1036:	6a 02                	push   $0x2
    1038:	8d 85 26 ff ff ff    	lea    -0xda(%ebp),%eax
    103e:	50                   	push   %eax
    103f:	ff 75 08             	pushl  0x8(%ebp)
    1042:	e8 5c 0c 00 00       	call   1ca3 <write>
    1047:	83 c4 10             	add    $0x10,%esp
    104a:	eb cb                	jmp    1017 <test_copy_on_write_main_child_alt+0x384>

0000104c <_show_cow_test_error>:

void _show_cow_test_error(char *code) {
    104c:	55                   	push   %ebp
    104d:	89 e5                	mov    %esp,%ebp
    104f:	83 ec 08             	sub    $0x8,%esp
    1052:	8b 55 08             	mov    0x8(%ebp),%edx
  if (code[0] == 'X') {
    1055:	0f b6 02             	movzbl (%edx),%eax
    1058:	3c 58                	cmp    $0x58,%al
    105a:	74 06                	je     1062 <_show_cow_test_error+0x16>
    printf(1, FAIL_MSG "copy on write test failed --- crash?\n");
  } else if (code[0] == 'N') {
    105c:	3c 4e                	cmp    $0x4e,%al
    105e:	74 16                	je     1076 <_show_cow_test_error+0x2a>
    default:
      printf(1, FAIL_MSG"copy on write test failed --- unknown reason\n");
      break;
    }
  }
}
    1060:	c9                   	leave  
    1061:	c3                   	ret    
    printf(1, FAIL_MSG "copy on write test failed --- crash?\n");
    1062:	83 ec 08             	sub    $0x8,%esp
    1065:	68 b0 26 00 00       	push   $0x26b0
    106a:	6a 01                	push   $0x1
    106c:	e8 84 0d 00 00       	call   1df5 <printf>
    1071:	83 c4 10             	add    $0x10,%esp
    1074:	eb ea                	jmp    1060 <_show_cow_test_error+0x14>
    switch (code[1]) {
    1076:	0f b6 42 01          	movzbl 0x1(%edx),%eax
    107a:	3c 63                	cmp    $0x63,%al
    107c:	74 4c                	je     10ca <_show_cow_test_error+0x7e>
    107e:	3c 63                	cmp    $0x63,%al
    1080:	7e 1c                	jle    109e <_show_cow_test_error+0x52>
    1082:	3c 66                	cmp    $0x66,%al
    1084:	74 30                	je     10b6 <_show_cow_test_error+0x6a>
    1086:	3c 70                	cmp    $0x70,%al
    1088:	75 54                	jne    10de <_show_cow_test_error+0x92>
      printf(1, FAIL_MSG "copy on write test failed --- wrong value for memory in parent\n");
    108a:	83 ec 08             	sub    $0x8,%esp
    108d:	68 1c 27 00 00       	push   $0x271c
    1092:	6a 01                	push   $0x1
    1094:	e8 5c 0d 00 00       	call   1df5 <printf>
      break;
    1099:	83 c4 10             	add    $0x10,%esp
    109c:	eb c2                	jmp    1060 <_show_cow_test_error+0x14>
    switch (code[1]) {
    109e:	3c 50                	cmp    $0x50,%al
    10a0:	75 3c                	jne    10de <_show_cow_test_error+0x92>
      printf(1, FAIL_MSG "copy on write test failed --- pipe read problem\n");
    10a2:	83 ec 08             	sub    $0x8,%esp
    10a5:	68 6c 27 00 00       	push   $0x276c
    10aa:	6a 01                	push   $0x1
    10ac:	e8 44 0d 00 00       	call   1df5 <printf>
      break;
    10b1:	83 c4 10             	add    $0x10,%esp
    10b4:	eb aa                	jmp    1060 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG "copy on write test failed --- fork failed\n");
    10b6:	83 ec 08             	sub    $0x8,%esp
    10b9:	68 e4 26 00 00       	push   $0x26e4
    10be:	6a 01                	push   $0x1
    10c0:	e8 30 0d 00 00       	call   1df5 <printf>
      break;
    10c5:	83 c4 10             	add    $0x10,%esp
    10c8:	eb 96                	jmp    1060 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG "copy on write test failed --- wrong value for memory in child\n");
    10ca:	83 ec 08             	sub    $0x8,%esp
    10cd:	68 ac 27 00 00       	push   $0x27ac
    10d2:	6a 01                	push   $0x1
    10d4:	e8 1c 0d 00 00       	call   1df5 <printf>
      break;
    10d9:	83 c4 10             	add    $0x10,%esp
    10dc:	eb 82                	jmp    1060 <_show_cow_test_error+0x14>
      printf(1, FAIL_MSG"copy on write test failed --- unknown reason\n");
    10de:	83 ec 08             	sub    $0x8,%esp
    10e1:	68 f8 27 00 00       	push   $0x27f8
    10e6:	6a 01                	push   $0x1
    10e8:	e8 08 0d 00 00       	call   1df5 <printf>
      break;
    10ed:	83 c4 10             	add    $0x10,%esp
}
    10f0:	e9 6b ff ff ff       	jmp    1060 <_show_cow_test_error+0x14>

000010f5 <test_copy_on_write_less_forks>:

int test_copy_on_write_less_forks(int size, const char *describe_size, int forks) {
    10f5:	55                   	push   %ebp
    10f6:	89 e5                	mov    %esp,%ebp
    10f8:	56                   	push   %esi
    10f9:	53                   	push   %ebx
    10fa:	83 ec 1c             	sub    $0x1c,%esp
    10fd:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    1100:	8b 75 10             	mov    0x10(%ebp),%esi
  int fds[2];
  pipe(fds);
    1103:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1106:	50                   	push   %eax
    1107:	e8 87 0b 00 00       	call   1c93 <pipe>
  test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    110c:	56                   	push   %esi
    110d:	53                   	push   %ebx
    110e:	ff 75 08             	pushl  0x8(%ebp)
    1111:	ff 75 f4             	pushl  -0xc(%ebp)
    1114:	e8 ec f7 ff ff       	call   905 <test_copy_on_write_main_child>
  char text[2] = {'X', 'X'};
    1119:	c6 45 ee 58          	movb   $0x58,-0x12(%ebp)
    111d:	c6 45 ef 58          	movb   $0x58,-0x11(%ebp)
  read(fds[0], text, 2);
    1121:	83 c4 1c             	add    $0x1c,%esp
    1124:	6a 02                	push   $0x2
    1126:	8d 45 ee             	lea    -0x12(%ebp),%eax
    1129:	50                   	push   %eax
    112a:	ff 75 f0             	pushl  -0x10(%ebp)
    112d:	e8 69 0b 00 00       	call   1c9b <read>
  close(fds[0]); close(fds[1]);
    1132:	83 c4 04             	add    $0x4,%esp
    1135:	ff 75 f0             	pushl  -0x10(%ebp)
    1138:	e8 6e 0b 00 00       	call   1cab <close>
    113d:	83 c4 04             	add    $0x4,%esp
    1140:	ff 75 f4             	pushl  -0xc(%ebp)
    1143:	e8 63 0b 00 00       	call   1cab <close>
  if (text[0] != 'Y') {
    1148:	83 c4 10             	add    $0x10,%esp
    114b:	80 7d ee 59          	cmpb   $0x59,-0x12(%ebp)
    114f:	74 1b                	je     116c <test_copy_on_write_less_forks+0x77>
    _show_cow_test_error(text);
    1151:	83 ec 0c             	sub    $0xc,%esp
    1154:	8d 45 ee             	lea    -0x12(%ebp),%eax
    1157:	50                   	push   %eax
    1158:	e8 ef fe ff ff       	call   104c <_show_cow_test_error>
    return 0;
    115d:	83 c4 10             	add    $0x10,%esp
    1160:	b8 00 00 00 00       	mov    $0x0,%eax
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
           "fork %d children; read+write small parts in each child\n",
           describe_size, forks);
    return 1;
  }
}
    1165:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1168:	5b                   	pop    %ebx
    1169:	5e                   	pop    %esi
    116a:	5d                   	pop    %ebp
    116b:	c3                   	ret    
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    116c:	56                   	push   %esi
    116d:	53                   	push   %ebx
    116e:	68 34 28 00 00       	push   $0x2834
    1173:	6a 01                	push   $0x1
    1175:	e8 7b 0c 00 00       	call   1df5 <printf>
    return 1;
    117a:	83 c4 10             	add    $0x10,%esp
    117d:	b8 01 00 00 00       	mov    $0x1,%eax
    1182:	eb e1                	jmp    1165 <test_copy_on_write_less_forks+0x70>

00001184 <test_copy_on_write_less_forks_alt>:

int test_copy_on_write_less_forks_alt(int size, const char *describe_size, int forks, int early_term) {
    1184:	55                   	push   %ebp
    1185:	89 e5                	mov    %esp,%ebp
    1187:	56                   	push   %esi
    1188:	53                   	push   %ebx
    1189:	83 ec 1c             	sub    $0x1c,%esp
    118c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    118f:	8b 75 10             	mov    0x10(%ebp),%esi
  int fds[2];
  pipe(fds);
    1192:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1195:	50                   	push   %eax
    1196:	e8 f8 0a 00 00       	call   1c93 <pipe>
  test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    119b:	83 c4 04             	add    $0x4,%esp
    119e:	ff 75 14             	pushl  0x14(%ebp)
    11a1:	56                   	push   %esi
    11a2:	53                   	push   %ebx
    11a3:	ff 75 08             	pushl  0x8(%ebp)
    11a6:	ff 75 f4             	pushl  -0xc(%ebp)
    11a9:	e8 e5 fa ff ff       	call   c93 <test_copy_on_write_main_child_alt>
  char text[2] = {'X', 'X'};
    11ae:	c6 45 ee 58          	movb   $0x58,-0x12(%ebp)
    11b2:	c6 45 ef 58          	movb   $0x58,-0x11(%ebp)
  read(fds[0], text, 2);
    11b6:	83 c4 1c             	add    $0x1c,%esp
    11b9:	6a 02                	push   $0x2
    11bb:	8d 45 ee             	lea    -0x12(%ebp),%eax
    11be:	50                   	push   %eax
    11bf:	ff 75 f0             	pushl  -0x10(%ebp)
    11c2:	e8 d4 0a 00 00       	call   1c9b <read>
  close(fds[0]); close(fds[1]);
    11c7:	83 c4 04             	add    $0x4,%esp
    11ca:	ff 75 f0             	pushl  -0x10(%ebp)
    11cd:	e8 d9 0a 00 00       	call   1cab <close>
    11d2:	83 c4 04             	add    $0x4,%esp
    11d5:	ff 75 f4             	pushl  -0xc(%ebp)
    11d8:	e8 ce 0a 00 00       	call   1cab <close>
  if (text[0] != 'Y') {
    11dd:	83 c4 10             	add    $0x10,%esp
    11e0:	80 7d ee 59          	cmpb   $0x59,-0x12(%ebp)
    11e4:	74 1b                	je     1201 <test_copy_on_write_less_forks_alt+0x7d>
    _show_cow_test_error(text);
    11e6:	83 ec 0c             	sub    $0xc,%esp
    11e9:	8d 45 ee             	lea    -0x12(%ebp),%eax
    11ec:	50                   	push   %eax
    11ed:	e8 5a fe ff ff       	call   104c <_show_cow_test_error>
    return 0;
    11f2:	83 c4 10             	add    $0x10,%esp
    11f5:	b8 00 00 00 00       	mov    $0x0,%eax
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
           "fork %d children; read+write small parts in each child\n",
           describe_size, forks);
    return 1;
  }
}
    11fa:	8d 65 f8             	lea    -0x8(%ebp),%esp
    11fd:	5b                   	pop    %ebx
    11fe:	5e                   	pop    %esi
    11ff:	5d                   	pop    %ebp
    1200:	c3                   	ret    
    printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    1201:	56                   	push   %esi
    1202:	53                   	push   %ebx
    1203:	68 34 28 00 00       	push   $0x2834
    1208:	6a 01                	push   $0x1
    120a:	e8 e6 0b 00 00       	call   1df5 <printf>
    return 1;
    120f:	83 c4 10             	add    $0x10,%esp
    1212:	b8 01 00 00 00       	mov    $0x1,%eax
    1217:	eb e1                	jmp    11fa <test_copy_on_write_less_forks_alt+0x76>

00001219 <_test_copy_on_write>:

int _test_copy_on_write(int size,  const char *describe_size, int forks, int use_alt, int early_term, int pre_alloc, const char* describe_prealloc) {
    1219:	55                   	push   %ebp
    121a:	89 e5                	mov    %esp,%ebp
    121c:	83 ec 34             	sub    $0x34,%esp
  int fds[2];
  pipe(fds);
    121f:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1222:	50                   	push   %eax
    1223:	e8 6b 0a 00 00       	call   1c93 <pipe>
  int pid = fork();
    1228:	e8 4e 0a 00 00       	call   1c7b <fork>
  if (pid == -1) {
    122d:	83 c4 10             	add    $0x10,%esp
    1230:	83 f8 ff             	cmp    $0xffffffff,%eax
    1233:	0f 84 c2 00 00 00    	je     12fb <_test_copy_on_write+0xe2>
    printf(1, FAIL_MSG "fork failed");
  } else if (pid == 0) {
    1239:	85 c0                	test   %eax,%eax
    123b:	0f 84 d6 00 00 00    	je     1317 <_test_copy_on_write+0xfe>
      test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    } else {
      test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    }
    exit();
  } else if (pid > 0) {
    1241:	85 c0                	test   %eax,%eax
    1243:	0f 8e 6c 01 00 00    	jle    13b5 <_test_copy_on_write+0x19c>
    printf(1, "running copy on write test: ");
    1249:	83 ec 08             	sub    $0x8,%esp
    124c:	68 30 2e 00 00       	push   $0x2e30
    1251:	6a 01                	push   $0x1
    1253:	e8 9d 0b 00 00       	call   1df5 <printf>
    if (pre_alloc > 0) {
    1258:	83 c4 10             	add    $0x10,%esp
    125b:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
    125f:	0f 8f 03 01 00 00    	jg     1368 <_test_copy_on_write+0x14f>
      printf(1, "allocate but do not use %s; ", describe_prealloc);
    }
    printf(1, "allocate and use %s; fork %d children; read+write small parts in each child",
    1265:	ff 75 10             	pushl  0x10(%ebp)
    1268:	ff 75 0c             	pushl  0xc(%ebp)
    126b:	68 a4 28 00 00       	push   $0x28a4
    1270:	6a 01                	push   $0x1
    1272:	e8 7e 0b 00 00       	call   1df5 <printf>
        describe_size, forks);
    if (use_alt) {
    1277:	83 c4 10             	add    $0x10,%esp
    127a:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
    127e:	0f 85 fe 00 00 00    	jne    1382 <_test_copy_on_write+0x169>
      printf(1, " [and try to keep children running in parallel]");
    }
    printf(1, "\n");
    1284:	83 ec 08             	sub    $0x8,%esp
    1287:	68 96 2e 00 00       	push   $0x2e96
    128c:	6a 01                	push   $0x1
    128e:	e8 62 0b 00 00       	call   1df5 <printf>
    char text[10] = {'X', 'X'};
    1293:	c7 45 e8 00 00 00 00 	movl   $0x0,-0x18(%ebp)
    129a:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%ebp)
    12a1:	c6 45 e6 58          	movb   $0x58,-0x1a(%ebp)
    12a5:	c6 45 e7 58          	movb   $0x58,-0x19(%ebp)
    close(fds[1]);
    12a9:	83 c4 04             	add    $0x4,%esp
    12ac:	ff 75 f4             	pushl  -0xc(%ebp)
    12af:	e8 f7 09 00 00       	call   1cab <close>
    read(fds[0], text, 10);
    12b4:	83 c4 0c             	add    $0xc,%esp
    12b7:	6a 0a                	push   $0xa
    12b9:	8d 45 e6             	lea    -0x1a(%ebp),%eax
    12bc:	50                   	push   %eax
    12bd:	ff 75 f0             	pushl  -0x10(%ebp)
    12c0:	e8 d6 09 00 00       	call   1c9b <read>
    wait();
    12c5:	e8 c1 09 00 00       	call   1c8b <wait>
    close(fds[0]);
    12ca:	83 c4 04             	add    $0x4,%esp
    12cd:	ff 75 f0             	pushl  -0x10(%ebp)
    12d0:	e8 d6 09 00 00       	call   1cab <close>
    if (text[0] != 'Y') {
    12d5:	83 c4 10             	add    $0x10,%esp
    12d8:	80 7d e6 59          	cmpb   $0x59,-0x1a(%ebp)
    12dc:	0f 84 b7 00 00 00    	je     1399 <_test_copy_on_write+0x180>
      _show_cow_test_error(text);
    12e2:	83 ec 0c             	sub    $0xc,%esp
    12e5:	8d 45 e6             	lea    -0x1a(%ebp),%eax
    12e8:	50                   	push   %eax
    12e9:	e8 5e fd ff ff       	call   104c <_show_cow_test_error>
      return 0;
    12ee:	83 c4 10             	add    $0x10,%esp
    12f1:	b8 00 00 00 00       	mov    $0x0,%eax
    12f6:	e9 bf 00 00 00       	jmp    13ba <_test_copy_on_write+0x1a1>
    printf(1, FAIL_MSG "fork failed");
    12fb:	83 ec 08             	sub    $0x8,%esp
    12fe:	68 a8 2c 00 00       	push   $0x2ca8
    1303:	6a 01                	push   $0x1
    1305:	e8 eb 0a 00 00       	call   1df5 <printf>
    130a:	83 c4 10             	add    $0x10,%esp
      return 1;
    }
  } else if (pid == -1) {
     printf(1, FAIL_MSG "copy on write test failed --- first fork failed\n");
  }
  return 0;
    130d:	b8 00 00 00 00       	mov    $0x0,%eax
    1312:	e9 a3 00 00 00       	jmp    13ba <_test_copy_on_write+0x1a1>
    if (pre_alloc > 0) {
    1317:	83 7d 1c 00          	cmpl   $0x0,0x1c(%ebp)
    131b:	7f 25                	jg     1342 <_test_copy_on_write+0x129>
    if (use_alt) {
    131d:	83 7d 14 00          	cmpl   $0x0,0x14(%ebp)
    1321:	74 2f                	je     1352 <_test_copy_on_write+0x139>
      test_copy_on_write_main_child_alt(fds[1], size, describe_size, forks, early_term);
    1323:	83 ec 0c             	sub    $0xc,%esp
    1326:	ff 75 18             	pushl  0x18(%ebp)
    1329:	ff 75 10             	pushl  0x10(%ebp)
    132c:	ff 75 0c             	pushl  0xc(%ebp)
    132f:	ff 75 08             	pushl  0x8(%ebp)
    1332:	ff 75 f4             	pushl  -0xc(%ebp)
    1335:	e8 59 f9 ff ff       	call   c93 <test_copy_on_write_main_child_alt>
    133a:	83 c4 20             	add    $0x20,%esp
    exit();
    133d:	e8 41 09 00 00       	call   1c83 <exit>
      sbrk(pre_alloc);
    1342:	83 ec 0c             	sub    $0xc,%esp
    1345:	ff 75 1c             	pushl  0x1c(%ebp)
    1348:	e8 be 09 00 00       	call   1d0b <sbrk>
    134d:	83 c4 10             	add    $0x10,%esp
    1350:	eb cb                	jmp    131d <_test_copy_on_write+0x104>
      test_copy_on_write_main_child(fds[1], size, describe_size, forks);
    1352:	ff 75 10             	pushl  0x10(%ebp)
    1355:	ff 75 0c             	pushl  0xc(%ebp)
    1358:	ff 75 08             	pushl  0x8(%ebp)
    135b:	ff 75 f4             	pushl  -0xc(%ebp)
    135e:	e8 a2 f5 ff ff       	call   905 <test_copy_on_write_main_child>
    1363:	83 c4 10             	add    $0x10,%esp
    1366:	eb d5                	jmp    133d <_test_copy_on_write+0x124>
      printf(1, "allocate but do not use %s; ", describe_prealloc);
    1368:	83 ec 04             	sub    $0x4,%esp
    136b:	ff 75 20             	pushl  0x20(%ebp)
    136e:	68 4d 2e 00 00       	push   $0x2e4d
    1373:	6a 01                	push   $0x1
    1375:	e8 7b 0a 00 00       	call   1df5 <printf>
    137a:	83 c4 10             	add    $0x10,%esp
    137d:	e9 e3 fe ff ff       	jmp    1265 <_test_copy_on_write+0x4c>
      printf(1, " [and try to keep children running in parallel]");
    1382:	83 ec 08             	sub    $0x8,%esp
    1385:	68 f0 28 00 00       	push   $0x28f0
    138a:	6a 01                	push   $0x1
    138c:	e8 64 0a 00 00       	call   1df5 <printf>
    1391:	83 c4 10             	add    $0x10,%esp
    1394:	e9 eb fe ff ff       	jmp    1284 <_test_copy_on_write+0x6b>
      printf(1, PASS_MSG "copy on write test passed --- allocate %s; "
    1399:	ff 75 10             	pushl  0x10(%ebp)
    139c:	ff 75 0c             	pushl  0xc(%ebp)
    139f:	68 34 28 00 00       	push   $0x2834
    13a4:	6a 01                	push   $0x1
    13a6:	e8 4a 0a 00 00       	call   1df5 <printf>
      return 1;
    13ab:	83 c4 10             	add    $0x10,%esp
    13ae:	b8 01 00 00 00       	mov    $0x1,%eax
    13b3:	eb 05                	jmp    13ba <_test_copy_on_write+0x1a1>
  return 0;
    13b5:	b8 00 00 00 00       	mov    $0x0,%eax
}
    13ba:	c9                   	leave  
    13bb:	c3                   	ret    

000013bc <test_copy_on_write>:

int test_copy_on_write(int size, const char *describe_size, int forks) {
    13bc:	55                   	push   %ebp
    13bd:	89 e5                	mov    %esp,%ebp
    13bf:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 0, 0, 0, "");
    13c2:	68 97 2e 00 00       	push   $0x2e97
    13c7:	6a 00                	push   $0x0
    13c9:	6a 00                	push   $0x0
    13cb:	6a 00                	push   $0x0
    13cd:	ff 75 10             	pushl  0x10(%ebp)
    13d0:	ff 75 0c             	pushl  0xc(%ebp)
    13d3:	ff 75 08             	pushl  0x8(%ebp)
    13d6:	e8 3e fe ff ff       	call   1219 <_test_copy_on_write>
}
    13db:	c9                   	leave  
    13dc:	c3                   	ret    

000013dd <test_copy_on_write_alloc_unused>:

int test_copy_on_write_alloc_unused(int unused_size, const char *describe_unused_size, int size, const char *describe_size, int forks) {
    13dd:	55                   	push   %ebp
    13de:	89 e5                	mov    %esp,%ebp
    13e0:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 0, 0, unused_size, describe_unused_size);
    13e3:	ff 75 0c             	pushl  0xc(%ebp)
    13e6:	ff 75 08             	pushl  0x8(%ebp)
    13e9:	6a 00                	push   $0x0
    13eb:	6a 00                	push   $0x0
    13ed:	ff 75 18             	pushl  0x18(%ebp)
    13f0:	ff 75 14             	pushl  0x14(%ebp)
    13f3:	ff 75 10             	pushl  0x10(%ebp)
    13f6:	e8 1e fe ff ff       	call   1219 <_test_copy_on_write>
}
    13fb:	c9                   	leave  
    13fc:	c3                   	ret    

000013fd <test_copy_on_write_alt>:

int test_copy_on_write_alt(int size, const char *describe_size, int forks) {
    13fd:	55                   	push   %ebp
    13fe:	89 e5                	mov    %esp,%ebp
    1400:	83 ec 0c             	sub    $0xc,%esp
  return _test_copy_on_write(size, describe_size, forks, 1, 0, 0, "");
    1403:	68 97 2e 00 00       	push   $0x2e97
    1408:	6a 00                	push   $0x0
    140a:	6a 00                	push   $0x0
    140c:	6a 01                	push   $0x1
    140e:	ff 75 10             	pushl  0x10(%ebp)
    1411:	ff 75 0c             	pushl  0xc(%ebp)
    1414:	ff 75 08             	pushl  0x8(%ebp)
    1417:	e8 fd fd ff ff       	call   1219 <_test_copy_on_write>
}
    141c:	c9                   	leave  
    141d:	c3                   	ret    

0000141e <test_read_into_alloc_no_fork>:

int test_read_into_alloc_no_fork(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    141e:	55                   	push   %ebp
    141f:	89 e5                	mov    %esp,%ebp
    1421:	57                   	push   %edi
    1422:	56                   	push   %esi
    1423:	53                   	push   %ebx
    1424:	83 ec 28             	sub    $0x28,%esp
    1427:	8b 5d 10             	mov    0x10(%ebp),%ebx
    printf(1, "testing read(), writing %d bytes to a location %s into a %s allocation\n",
    142a:	ff 75 14             	pushl  0x14(%ebp)
    142d:	ff 75 18             	pushl  0x18(%ebp)
    1430:	53                   	push   %ebx
    1431:	68 20 29 00 00       	push   $0x2920
    1436:	6a 01                	push   $0x1
    1438:	e8 b8 09 00 00       	call   1df5 <printf>
        read_count, describe_offset, describe_size);
    int fd = open("tempfile", O_WRONLY | O_CREATE);
    143d:	83 c4 18             	add    $0x18,%esp
    1440:	68 01 02 00 00       	push   $0x201
    1445:	68 78 2e 00 00       	push   $0x2e78
    144a:	e8 74 08 00 00       	call   1cc3 <open>
    144f:	89 c6                	mov    %eax,%esi
    static char buffer[128]; // static to avoid running out of stack space
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1451:	83 c4 10             	add    $0x10,%esp
    1454:	b8 00 00 00 00       	mov    $0x0,%eax
    1459:	eb 0a                	jmp    1465 <test_read_into_alloc_no_fork+0x47>
        buffer[i] = 'X';
    145b:	c6 80 60 36 00 00 58 	movb   $0x58,0x3660(%eax)
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1462:	83 c0 01             	add    $0x1,%eax
    1465:	83 f8 7f             	cmp    $0x7f,%eax
    1468:	76 f1                	jbe    145b <test_read_into_alloc_no_fork+0x3d>
    }
    for (int i = 0; i < read_count; i += sizeof buffer) {
    146a:	bf 00 00 00 00       	mov    $0x0,%edi
    146f:	eb 19                	jmp    148a <test_read_into_alloc_no_fork+0x6c>
        write(fd, buffer, sizeof buffer);
    1471:	83 ec 04             	sub    $0x4,%esp
    1474:	68 80 00 00 00       	push   $0x80
    1479:	68 60 36 00 00       	push   $0x3660
    147e:	56                   	push   %esi
    147f:	e8 1f 08 00 00       	call   1ca3 <write>
    for (int i = 0; i < read_count; i += sizeof buffer) {
    1484:	83 ef 80             	sub    $0xffffff80,%edi
    1487:	83 c4 10             	add    $0x10,%esp
    148a:	39 df                	cmp    %ebx,%edi
    148c:	7c e3                	jl     1471 <test_read_into_alloc_no_fork+0x53>
    }
    close(fd);
    148e:	83 ec 0c             	sub    $0xc,%esp
    1491:	56                   	push   %esi
    1492:	e8 14 08 00 00       	call   1cab <close>
    fd = open("tempfile", O_RDONLY);
    1497:	83 c4 08             	add    $0x8,%esp
    149a:	6a 00                	push   $0x0
    149c:	68 78 2e 00 00       	push   $0x2e78
    14a1:	e8 1d 08 00 00       	call   1cc3 <open>
    14a6:	89 c7                	mov    %eax,%edi
    if (fd == -1) {
    14a8:	83 c4 10             	add    $0x10,%esp
    14ab:	83 f8 ff             	cmp    $0xffffffff,%eax
    14ae:	74 3f                	je     14ef <test_read_into_alloc_no_fork+0xd1>
        printf(2, "error opening tempfile");
    }
    char *heap = sbrk(0);
    14b0:	83 ec 0c             	sub    $0xc,%esp
    14b3:	6a 00                	push   $0x0
    14b5:	e8 51 08 00 00       	call   1d0b <sbrk>
    14ba:	89 c6                	mov    %eax,%esi
    sbrk(size);
    14bc:	83 c4 04             	add    $0x4,%esp
    14bf:	ff 75 08             	pushl  0x8(%ebp)
    14c2:	e8 44 08 00 00       	call   1d0b <sbrk>
    char *loc = heap + offset;
    14c7:	03 75 0c             	add    0xc(%ebp),%esi
    int count = read(fd, loc, read_count);
    14ca:	83 c4 0c             	add    $0xc,%esp
    14cd:	53                   	push   %ebx
    14ce:	56                   	push   %esi
    14cf:	57                   	push   %edi
    14d0:	e8 c6 07 00 00       	call   1c9b <read>
    14d5:	89 45 e0             	mov    %eax,-0x20(%ebp)
    int failed_value = 0;
    failed_value = loc[-1] != '\0';
    14d8:	83 c4 10             	add    $0x10,%esp
    14db:	80 7e ff 00          	cmpb   $0x0,-0x1(%esi)
    14df:	0f 95 c0             	setne  %al
    14e2:	0f b6 c0             	movzbl %al,%eax
    14e5:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for (int i = 0; i < read_count; ++i) {
    14e8:	b8 00 00 00 00       	mov    $0x0,%eax
    14ed:	eb 17                	jmp    1506 <test_read_into_alloc_no_fork+0xe8>
        printf(2, "error opening tempfile");
    14ef:	83 ec 08             	sub    $0x8,%esp
    14f2:	68 6a 2e 00 00       	push   $0x2e6a
    14f7:	6a 02                	push   $0x2
    14f9:	e8 f7 08 00 00       	call   1df5 <printf>
    14fe:	83 c4 10             	add    $0x10,%esp
    1501:	eb ad                	jmp    14b0 <test_read_into_alloc_no_fork+0x92>
    for (int i = 0; i < read_count; ++i) {
    1503:	83 c0 01             	add    $0x1,%eax
    1506:	39 d8                	cmp    %ebx,%eax
    1508:	7d 0f                	jge    1519 <test_read_into_alloc_no_fork+0xfb>
        if (loc[i] != 'X') {
    150a:	80 3c 06 58          	cmpb   $0x58,(%esi,%eax,1)
    150e:	74 f3                	je     1503 <test_read_into_alloc_no_fork+0xe5>
            failed_value = 1;
    1510:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
    1517:	eb ea                	jmp    1503 <test_read_into_alloc_no_fork+0xe5>
        }
    }
    if (loc[read_count] != '\0') {
    1519:	80 3c 1e 00          	cmpb   $0x0,(%esi,%ebx,1)
    151d:	74 07                	je     1526 <test_read_into_alloc_no_fork+0x108>
        failed_value = 1;
    151f:	c7 45 e4 01 00 00 00 	movl   $0x1,-0x1c(%ebp)
    }
    close(fd);
    1526:	83 ec 0c             	sub    $0xc,%esp
    1529:	57                   	push   %edi
    152a:	e8 7c 07 00 00       	call   1cab <close>
    unlink("tempfile");
    152f:	c7 04 24 78 2e 00 00 	movl   $0x2e78,(%esp)
    1536:	e8 98 07 00 00       	call   1cd3 <unlink>
    if (count != read_count) {
    153b:	83 c4 10             	add    $0x10,%esp
    153e:	3b 5d e0             	cmp    -0x20(%ebp),%ebx
    1541:	75 1f                	jne    1562 <test_read_into_alloc_no_fork+0x144>
        printf(1, FAIL_MSG "wrong return value from read()\n");
        return 0;
    } else if (failed_value) {
    1543:	83 7d e4 00          	cmpl   $0x0,-0x1c(%ebp)
    1547:	75 38                	jne    1581 <test_read_into_alloc_no_fork+0x163>
        printf(1, FAIL_MSG "wrong value written to memory by read()\n");
        return 0;
    } else {
        printf(1, PASS_MSG "read() into heap allocation\n");
    1549:	83 ec 08             	sub    $0x8,%esp
    154c:	68 d0 29 00 00       	push   $0x29d0
    1551:	6a 01                	push   $0x1
    1553:	e8 9d 08 00 00       	call   1df5 <printf>
        return 1;
    1558:	83 c4 10             	add    $0x10,%esp
    155b:	b8 01 00 00 00       	mov    $0x1,%eax
    1560:	eb 17                	jmp    1579 <test_read_into_alloc_no_fork+0x15b>
        printf(1, FAIL_MSG "wrong return value from read()\n");
    1562:	83 ec 08             	sub    $0x8,%esp
    1565:	68 68 29 00 00       	push   $0x2968
    156a:	6a 01                	push   $0x1
    156c:	e8 84 08 00 00       	call   1df5 <printf>
        return 0;
    1571:	83 c4 10             	add    $0x10,%esp
    1574:	b8 00 00 00 00       	mov    $0x0,%eax
    }
}
    1579:	8d 65 f4             	lea    -0xc(%ebp),%esp
    157c:	5b                   	pop    %ebx
    157d:	5e                   	pop    %esi
    157e:	5f                   	pop    %edi
    157f:	5d                   	pop    %ebp
    1580:	c3                   	ret    
        printf(1, FAIL_MSG "wrong value written to memory by read()\n");
    1581:	83 ec 08             	sub    $0x8,%esp
    1584:	68 98 29 00 00       	push   $0x2998
    1589:	6a 01                	push   $0x1
    158b:	e8 65 08 00 00       	call   1df5 <printf>
        return 0;
    1590:	83 c4 10             	add    $0x10,%esp
    1593:	b8 00 00 00 00       	mov    $0x0,%eax
    1598:	eb df                	jmp    1579 <test_read_into_alloc_no_fork+0x15b>

0000159a <test_read_into_alloc>:

int test_read_into_alloc(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    159a:	55                   	push   %ebp
    159b:	89 e5                	mov    %esp,%ebp
    159d:	83 ec 24             	sub    $0x24,%esp
    int pipe_fds[2];
    pipe(pipe_fds);
    15a0:	8d 45 f0             	lea    -0x10(%ebp),%eax
    15a3:	50                   	push   %eax
    15a4:	e8 ea 06 00 00       	call   1c93 <pipe>
    int pid = fork();
    15a9:	e8 cd 06 00 00       	call   1c7b <fork>
    if (pid == -1) {
    15ae:	83 c4 10             	add    $0x10,%esp
    15b1:	83 f8 ff             	cmp    $0xffffffff,%eax
    15b4:	74 38                	je     15ee <test_read_into_alloc+0x54>
        printf(1, FAIL_MSG "fork failed");
    } else if (pid == 0) {
    15b6:	85 c0                	test   %eax,%eax
    15b8:	74 4d                	je     1607 <test_read_into_alloc+0x6d>
            result_str[0] = 'Y';
        }
        write(pipe_fds[1], result_str, 1);
        exit();
    } else {
        close(pipe_fds[1]);
    15ba:	83 ec 0c             	sub    $0xc,%esp
    15bd:	ff 75 f4             	pushl  -0xc(%ebp)
    15c0:	e8 e6 06 00 00       	call   1cab <close>
        char result_str[1] = {'N'};
    15c5:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        read(pipe_fds[0], result_str, 1);
    15c9:	83 c4 0c             	add    $0xc,%esp
    15cc:	6a 01                	push   $0x1
    15ce:	8d 45 ef             	lea    -0x11(%ebp),%eax
    15d1:	50                   	push   %eax
    15d2:	ff 75 f0             	pushl  -0x10(%ebp)
    15d5:	e8 c1 06 00 00       	call   1c9b <read>
        wait();
    15da:	e8 ac 06 00 00       	call   1c8b <wait>
        return result_str[0] == 'Y';
    15df:	83 c4 10             	add    $0x10,%esp
    15e2:	80 7d ef 59          	cmpb   $0x59,-0x11(%ebp)
    15e6:	0f 94 c0             	sete   %al
    15e9:	0f b6 c0             	movzbl %al,%eax
    }
    return 0;
}
    15ec:	c9                   	leave  
    15ed:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
    15ee:	83 ec 08             	sub    $0x8,%esp
    15f1:	68 a8 2c 00 00       	push   $0x2ca8
    15f6:	6a 01                	push   $0x1
    15f8:	e8 f8 07 00 00       	call   1df5 <printf>
    return 0;
    15fd:	83 c4 10             	add    $0x10,%esp
    1600:	b8 00 00 00 00       	mov    $0x0,%eax
    1605:	eb e5                	jmp    15ec <test_read_into_alloc+0x52>
        close(pipe_fds[0]);
    1607:	83 ec 0c             	sub    $0xc,%esp
    160a:	ff 75 f0             	pushl  -0x10(%ebp)
    160d:	e8 99 06 00 00       	call   1cab <close>
        char result_str[1] = {'N'};
    1612:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        if (test_read_into_alloc_no_fork(size, offset, read_count, describe_size, describe_offset)) {
    1616:	83 c4 04             	add    $0x4,%esp
    1619:	ff 75 18             	pushl  0x18(%ebp)
    161c:	ff 75 14             	pushl  0x14(%ebp)
    161f:	ff 75 10             	pushl  0x10(%ebp)
    1622:	ff 75 0c             	pushl  0xc(%ebp)
    1625:	ff 75 08             	pushl  0x8(%ebp)
    1628:	e8 f1 fd ff ff       	call   141e <test_read_into_alloc_no_fork>
    162d:	83 c4 20             	add    $0x20,%esp
    1630:	85 c0                	test   %eax,%eax
    1632:	74 04                	je     1638 <test_read_into_alloc+0x9e>
            result_str[0] = 'Y';
    1634:	c6 45 ef 59          	movb   $0x59,-0x11(%ebp)
        write(pipe_fds[1], result_str, 1);
    1638:	83 ec 04             	sub    $0x4,%esp
    163b:	6a 01                	push   $0x1
    163d:	8d 45 ef             	lea    -0x11(%ebp),%eax
    1640:	50                   	push   %eax
    1641:	ff 75 f4             	pushl  -0xc(%ebp)
    1644:	e8 5a 06 00 00       	call   1ca3 <write>
        exit();
    1649:	e8 35 06 00 00       	call   1c83 <exit>

0000164e <test_read_into_cow_less_forks>:

int test_read_into_cow_less_forks(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    164e:	55                   	push   %ebp
    164f:	89 e5                	mov    %esp,%ebp
    1651:	57                   	push   %edi
    1652:	56                   	push   %esi
    1653:	53                   	push   %ebx
    1654:	83 ec 38             	sub    $0x38,%esp
    1657:	8b 75 08             	mov    0x8(%ebp),%esi
    printf(1, "testing read(), writing %d bytes to a location %s into a %s copy-on-write allocation\n",
    165a:	ff 75 14             	pushl  0x14(%ebp)
    165d:	ff 75 18             	pushl  0x18(%ebp)
    1660:	ff 75 10             	pushl  0x10(%ebp)
    1663:	68 fc 29 00 00       	push   $0x29fc
    1668:	6a 01                	push   $0x1
    166a:	e8 86 07 00 00       	call   1df5 <printf>
        read_count, describe_offset, describe_size);
    int fd = open("tempfile", O_WRONLY | O_CREATE);
    166f:	83 c4 18             	add    $0x18,%esp
    1672:	68 01 02 00 00       	push   $0x201
    1677:	68 78 2e 00 00       	push   $0x2e78
    167c:	e8 42 06 00 00       	call   1cc3 <open>
    1681:	89 c7                	mov    %eax,%edi
    static char buffer[128]; // static to avoid running out of stack space
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1683:	83 c4 10             	add    $0x10,%esp
    1686:	b8 00 00 00 00       	mov    $0x0,%eax
    168b:	eb 0a                	jmp    1697 <test_read_into_cow_less_forks+0x49>
        buffer[i] = 'X';
    168d:	c6 80 e0 35 00 00 58 	movb   $0x58,0x35e0(%eax)
    for (int i = 0 ; i < sizeof buffer; ++i) {
    1694:	83 c0 01             	add    $0x1,%eax
    1697:	83 f8 7f             	cmp    $0x7f,%eax
    169a:	76 f1                	jbe    168d <test_read_into_cow_less_forks+0x3f>
    }
    for (int i = 0; i < read_count; i += sizeof buffer) {
    169c:	bb 00 00 00 00       	mov    $0x0,%ebx
    16a1:	eb 19                	jmp    16bc <test_read_into_cow_less_forks+0x6e>
        write(fd, buffer, sizeof buffer);
    16a3:	83 ec 04             	sub    $0x4,%esp
    16a6:	68 80 00 00 00       	push   $0x80
    16ab:	68 e0 35 00 00       	push   $0x35e0
    16b0:	57                   	push   %edi
    16b1:	e8 ed 05 00 00       	call   1ca3 <write>
    for (int i = 0; i < read_count; i += sizeof buffer) {
    16b6:	83 eb 80             	sub    $0xffffff80,%ebx
    16b9:	83 c4 10             	add    $0x10,%esp
    16bc:	3b 5d 10             	cmp    0x10(%ebp),%ebx
    16bf:	7c e2                	jl     16a3 <test_read_into_cow_less_forks+0x55>
    }
    close(fd);
    16c1:	83 ec 0c             	sub    $0xc,%esp
    16c4:	57                   	push   %edi
    16c5:	e8 e1 05 00 00       	call   1cab <close>
    fd = open("tempfile", O_RDONLY);
    16ca:	83 c4 08             	add    $0x8,%esp
    16cd:	6a 00                	push   $0x0
    16cf:	68 78 2e 00 00       	push   $0x2e78
    16d4:	e8 ea 05 00 00       	call   1cc3 <open>
    16d9:	89 45 d0             	mov    %eax,-0x30(%ebp)
    if (fd == -1) {
    16dc:	83 c4 10             	add    $0x10,%esp
    16df:	83 f8 ff             	cmp    $0xffffffff,%eax
    16e2:	74 1e                	je     1702 <test_read_into_cow_less_forks+0xb4>
        printf(2, "error opening tempfile");
    }
    char *heap = sbrk(0);
    16e4:	83 ec 0c             	sub    $0xc,%esp
    16e7:	6a 00                	push   $0x0
    16e9:	e8 1d 06 00 00       	call   1d0b <sbrk>
    16ee:	89 c3                	mov    %eax,%ebx
    sbrk(size);
    16f0:	89 34 24             	mov    %esi,(%esp)
    16f3:	e8 13 06 00 00       	call   1d0b <sbrk>
    for (int i = 0; i < size; ++i) {
    16f8:	83 c4 10             	add    $0x10,%esp
    16fb:	b8 00 00 00 00       	mov    $0x0,%eax
    1700:	eb 1b                	jmp    171d <test_read_into_cow_less_forks+0xcf>
        printf(2, "error opening tempfile");
    1702:	83 ec 08             	sub    $0x8,%esp
    1705:	68 6a 2e 00 00       	push   $0x2e6a
    170a:	6a 02                	push   $0x2
    170c:	e8 e4 06 00 00       	call   1df5 <printf>
    1711:	83 c4 10             	add    $0x10,%esp
    1714:	eb ce                	jmp    16e4 <test_read_into_cow_less_forks+0x96>
        heap[i] = 'Y';
    1716:	c6 04 03 59          	movb   $0x59,(%ebx,%eax,1)
    for (int i = 0; i < size; ++i) {
    171a:	83 c0 01             	add    $0x1,%eax
    171d:	39 f0                	cmp    %esi,%eax
    171f:	7c f5                	jl     1716 <test_read_into_cow_less_forks+0xc8>
    }
    char *loc = heap + offset;
    1721:	89 d8                	mov    %ebx,%eax
    1723:	03 45 0c             	add    0xc(%ebp),%eax
    1726:	89 45 d4             	mov    %eax,-0x2c(%ebp)
    int pipe_fds[2];
    pipe(pipe_fds);
    1729:	83 ec 0c             	sub    $0xc,%esp
    172c:	8d 45 e0             	lea    -0x20(%ebp),%eax
    172f:	50                   	push   %eax
    1730:	e8 5e 05 00 00       	call   1c93 <pipe>
    int pid = fork();
    1735:	e8 41 05 00 00       	call   1c7b <fork>
    173a:	89 c7                	mov    %eax,%edi
    if (pid == -1) {
    173c:	83 c4 10             	add    $0x10,%esp
    173f:	83 f8 ff             	cmp    $0xffffffff,%eax
    1742:	74 55                	je     1799 <test_read_into_cow_less_forks+0x14b>
        printf(1, FAIL_MSG "fork failed");
        exit();
    } else if (pid == 0) {
    1744:	85 c0                	test   %eax,%eax
    1746:	74 65                	je     17ad <test_read_into_cow_less_forks+0x15f>
            write(pipe_fds[1], "Y", 1);
        }
        close(pipe_fds[1]);
        exit();
    } else {
        close(pipe_fds[1]);
    1748:	83 ec 0c             	sub    $0xc,%esp
    174b:	ff 75 e4             	pushl  -0x1c(%ebp)
    174e:	e8 58 05 00 00       	call   1cab <close>
        char result_buf[1] = {'N'};
    1753:	c6 45 df 4e          	movb   $0x4e,-0x21(%ebp)
        read(pipe_fds[0], result_buf, 1);
    1757:	83 c4 0c             	add    $0xc,%esp
    175a:	6a 01                	push   $0x1
    175c:	8d 45 df             	lea    -0x21(%ebp),%eax
    175f:	50                   	push   %eax
    1760:	ff 75 e0             	pushl  -0x20(%ebp)
    1763:	e8 33 05 00 00       	call   1c9b <read>
        close(pipe_fds[0]);
    1768:	83 c4 04             	add    $0x4,%esp
    176b:	ff 75 e0             	pushl  -0x20(%ebp)
    176e:	e8 38 05 00 00       	call   1cab <close>
        wait();
    1773:	e8 13 05 00 00       	call   1c8b <wait>
        printf(1, "testing correct value for heap in parent after read() in child\n");
    1778:	83 c4 08             	add    $0x8,%esp
    177b:	68 94 2a 00 00       	push   $0x2a94
    1780:	6a 01                	push   $0x1
    1782:	e8 6e 06 00 00       	call   1df5 <printf>
        int found_wrong = 0;
        for (int i = 0; i < size; ++i) {
    1787:	83 c4 10             	add    $0x10,%esp
    178a:	b8 00 00 00 00       	mov    $0x0,%eax
        int found_wrong = 0;
    178f:	bf 00 00 00 00       	mov    $0x0,%edi
        for (int i = 0; i < size; ++i) {
    1794:	e9 0f 01 00 00       	jmp    18a8 <test_read_into_cow_less_forks+0x25a>
        printf(1, FAIL_MSG "fork failed");
    1799:	83 ec 08             	sub    $0x8,%esp
    179c:	68 a8 2c 00 00       	push   $0x2ca8
    17a1:	6a 01                	push   $0x1
    17a3:	e8 4d 06 00 00       	call   1df5 <printf>
        exit();
    17a8:	e8 d6 04 00 00       	call   1c83 <exit>
        close(pipe_fds[0]);
    17ad:	83 ec 0c             	sub    $0xc,%esp
    17b0:	ff 75 e0             	pushl  -0x20(%ebp)
    17b3:	e8 f3 04 00 00       	call   1cab <close>
        int count = read(fd, loc, read_count);
    17b8:	83 c4 0c             	add    $0xc,%esp
    17bb:	ff 75 10             	pushl  0x10(%ebp)
    17be:	8b 5d d4             	mov    -0x2c(%ebp),%ebx
    17c1:	53                   	push   %ebx
    17c2:	ff 75 d0             	pushl  -0x30(%ebp)
    17c5:	e8 d1 04 00 00       	call   1c9b <read>
    17ca:	89 c6                	mov    %eax,%esi
        failed_value = loc[-1] != 'Y';
    17cc:	83 c4 10             	add    $0x10,%esp
    17cf:	80 7b ff 59          	cmpb   $0x59,-0x1(%ebx)
    17d3:	0f 95 c3             	setne  %bl
    17d6:	0f b6 db             	movzbl %bl,%ebx
        for (int i = 0; i < read_count; ++i) {
    17d9:	3b 7d 10             	cmp    0x10(%ebp),%edi
    17dc:	7c 64                	jl     1842 <test_read_into_cow_less_forks+0x1f4>
        if (loc[read_count] != 'Y') {
    17de:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    17e1:	8b 55 10             	mov    0x10(%ebp),%edx
    17e4:	80 3c 10 59          	cmpb   $0x59,(%eax,%edx,1)
    17e8:	74 05                	je     17ef <test_read_into_cow_less_forks+0x1a1>
            failed_value = 1;
    17ea:	bb 01 00 00 00       	mov    $0x1,%ebx
        close(fd);
    17ef:	83 ec 0c             	sub    $0xc,%esp
    17f2:	ff 75 d0             	pushl  -0x30(%ebp)
    17f5:	e8 b1 04 00 00       	call   1cab <close>
        unlink("tempfile");
    17fa:	c7 04 24 78 2e 00 00 	movl   $0x2e78,(%esp)
    1801:	e8 cd 04 00 00       	call   1cd3 <unlink>
        if (count != read_count) {
    1806:	83 c4 10             	add    $0x10,%esp
    1809:	39 75 10             	cmp    %esi,0x10(%ebp)
    180c:	74 47                	je     1855 <test_read_into_cow_less_forks+0x207>
            printf(1, FAIL_MSG "wrong return value from read()\n");
    180e:	83 ec 08             	sub    $0x8,%esp
    1811:	68 68 29 00 00       	push   $0x2968
    1816:	6a 01                	push   $0x1
    1818:	e8 d8 05 00 00       	call   1df5 <printf>
            write(pipe_fds[1], "N", 1);
    181d:	83 c4 0c             	add    $0xc,%esp
    1820:	6a 01                	push   $0x1
    1822:	68 81 2e 00 00       	push   $0x2e81
    1827:	ff 75 e4             	pushl  -0x1c(%ebp)
    182a:	e8 74 04 00 00       	call   1ca3 <write>
    182f:	83 c4 10             	add    $0x10,%esp
        close(pipe_fds[1]);
    1832:	83 ec 0c             	sub    $0xc,%esp
    1835:	ff 75 e4             	pushl  -0x1c(%ebp)
    1838:	e8 6e 04 00 00       	call   1cab <close>
        exit();
    183d:	e8 41 04 00 00       	call   1c83 <exit>
            if (loc[i] != 'X') {
    1842:	8b 45 d4             	mov    -0x2c(%ebp),%eax
    1845:	80 3c 38 58          	cmpb   $0x58,(%eax,%edi,1)
    1849:	74 05                	je     1850 <test_read_into_cow_less_forks+0x202>
                failed_value = 1;
    184b:	bb 01 00 00 00       	mov    $0x1,%ebx
        for (int i = 0; i < read_count; ++i) {
    1850:	83 c7 01             	add    $0x1,%edi
    1853:	eb 84                	jmp    17d9 <test_read_into_cow_less_forks+0x18b>
        } else if (failed_value) {
    1855:	85 db                	test   %ebx,%ebx
    1857:	74 26                	je     187f <test_read_into_cow_less_forks+0x231>
            printf(1, FAIL_MSG "wrong value written to memory by read()\n");
    1859:	83 ec 08             	sub    $0x8,%esp
    185c:	68 98 29 00 00       	push   $0x2998
    1861:	6a 01                	push   $0x1
    1863:	e8 8d 05 00 00       	call   1df5 <printf>
            write(pipe_fds[1], "N", 1);
    1868:	83 c4 0c             	add    $0xc,%esp
    186b:	6a 01                	push   $0x1
    186d:	68 81 2e 00 00       	push   $0x2e81
    1872:	ff 75 e4             	pushl  -0x1c(%ebp)
    1875:	e8 29 04 00 00       	call   1ca3 <write>
    187a:	83 c4 10             	add    $0x10,%esp
    187d:	eb b3                	jmp    1832 <test_read_into_cow_less_forks+0x1e4>
            printf(1, PASS_MSG "correct value read into copy-on-write allocation\n");
    187f:	83 ec 08             	sub    $0x8,%esp
    1882:	68 54 2a 00 00       	push   $0x2a54
    1887:	6a 01                	push   $0x1
    1889:	e8 67 05 00 00       	call   1df5 <printf>
            write(pipe_fds[1], "Y", 1);
    188e:	83 c4 0c             	add    $0xc,%esp
    1891:	6a 01                	push   $0x1
    1893:	68 0b 2e 00 00       	push   $0x2e0b
    1898:	ff 75 e4             	pushl  -0x1c(%ebp)
    189b:	e8 03 04 00 00       	call   1ca3 <write>
    18a0:	83 c4 10             	add    $0x10,%esp
    18a3:	eb 8d                	jmp    1832 <test_read_into_cow_less_forks+0x1e4>
        for (int i = 0; i < size; ++i) {
    18a5:	83 c0 01             	add    $0x1,%eax
    18a8:	39 f0                	cmp    %esi,%eax
    18aa:	7d 0d                	jge    18b9 <test_read_into_cow_less_forks+0x26b>
            if (heap[i] != 'Y') {
    18ac:	80 3c 03 59          	cmpb   $0x59,(%ebx,%eax,1)
    18b0:	74 f3                	je     18a5 <test_read_into_cow_less_forks+0x257>
                found_wrong = 1;
    18b2:	bf 01 00 00 00       	mov    $0x1,%edi
    18b7:	eb ec                	jmp    18a5 <test_read_into_cow_less_forks+0x257>
            }
        }
        if (found_wrong) { 
    18b9:	85 ff                	test   %edi,%edi
    18bb:	75 29                	jne    18e6 <test_read_into_cow_less_forks+0x298>
            printf(1, FAIL_MSG "wrong value in parent after read() in child\n");
        } else {
            printf(1, PASS_MSG "correct value in parent after read into copy-on-write allocation\n");
    18bd:	83 ec 08             	sub    $0x8,%esp
    18c0:	68 10 2b 00 00       	push   $0x2b10
    18c5:	6a 01                	push   $0x1
    18c7:	e8 29 05 00 00       	call   1df5 <printf>
    18cc:	83 c4 10             	add    $0x10,%esp
        }
        return (found_wrong == 0) + (result_buf[0] == 'Y');
    18cf:	83 f7 01             	xor    $0x1,%edi
    18d2:	80 7d df 59          	cmpb   $0x59,-0x21(%ebp)
    18d6:	0f 94 c0             	sete   %al
    18d9:	0f b6 c0             	movzbl %al,%eax
    18dc:	01 f8                	add    %edi,%eax
    }
}
    18de:	8d 65 f4             	lea    -0xc(%ebp),%esp
    18e1:	5b                   	pop    %ebx
    18e2:	5e                   	pop    %esi
    18e3:	5f                   	pop    %edi
    18e4:	5d                   	pop    %ebp
    18e5:	c3                   	ret    
            printf(1, FAIL_MSG "wrong value in parent after read() in child\n");
    18e6:	83 ec 08             	sub    $0x8,%esp
    18e9:	68 d4 2a 00 00       	push   $0x2ad4
    18ee:	6a 01                	push   $0x1
    18f0:	e8 00 05 00 00       	call   1df5 <printf>
    18f5:	83 c4 10             	add    $0x10,%esp
    18f8:	eb d5                	jmp    18cf <test_read_into_cow_less_forks+0x281>

000018fa <test_read_into_cow>:

int test_read_into_cow(int size, int offset, int read_count, char *describe_size, char *describe_offset) {
    18fa:	55                   	push   %ebp
    18fb:	89 e5                	mov    %esp,%ebp
    18fd:	83 ec 24             	sub    $0x24,%esp
    int pipe_fds[2];
    pipe(pipe_fds);
    1900:	8d 45 f0             	lea    -0x10(%ebp),%eax
    1903:	50                   	push   %eax
    1904:	e8 8a 03 00 00       	call   1c93 <pipe>
    int pid = fork();
    1909:	e8 6d 03 00 00       	call   1c7b <fork>
    if (pid == -1) {
    190e:	83 c4 10             	add    $0x10,%esp
    1911:	83 f8 ff             	cmp    $0xffffffff,%eax
    1914:	74 38                	je     194e <test_read_into_cow+0x54>
        printf(1, FAIL_MSG "fork failed");
        exit();
    } else if (pid == 0) {
    1916:	85 c0                	test   %eax,%eax
    1918:	74 48                	je     1962 <test_read_into_cow+0x68>
            result_str[0] = 'Y';
        }
        write(pipe_fds[1], result_str, 1);
        exit();
    } else {
        close(pipe_fds[1]);
    191a:	83 ec 0c             	sub    $0xc,%esp
    191d:	ff 75 f4             	pushl  -0xc(%ebp)
    1920:	e8 86 03 00 00       	call   1cab <close>
        char result_str[1] = {'N'};
    1925:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        read(pipe_fds[0], result_str, 1);
    1929:	83 c4 0c             	add    $0xc,%esp
    192c:	6a 01                	push   $0x1
    192e:	8d 45 ef             	lea    -0x11(%ebp),%eax
    1931:	50                   	push   %eax
    1932:	ff 75 f0             	pushl  -0x10(%ebp)
    1935:	e8 61 03 00 00       	call   1c9b <read>
        wait();
    193a:	e8 4c 03 00 00       	call   1c8b <wait>
        return result_str[0] == 'Y';
    193f:	83 c4 10             	add    $0x10,%esp
    1942:	80 7d ef 59          	cmpb   $0x59,-0x11(%ebp)
    1946:	0f 94 c0             	sete   %al
    1949:	0f b6 c0             	movzbl %al,%eax
    }
}
    194c:	c9                   	leave  
    194d:	c3                   	ret    
        printf(1, FAIL_MSG "fork failed");
    194e:	83 ec 08             	sub    $0x8,%esp
    1951:	68 a8 2c 00 00       	push   $0x2ca8
    1956:	6a 01                	push   $0x1
    1958:	e8 98 04 00 00       	call   1df5 <printf>
        exit();
    195d:	e8 21 03 00 00       	call   1c83 <exit>
        close(pipe_fds[0]);
    1962:	83 ec 0c             	sub    $0xc,%esp
    1965:	ff 75 f0             	pushl  -0x10(%ebp)
    1968:	e8 3e 03 00 00       	call   1cab <close>
        char result_str[1] = {'N'};
    196d:	c6 45 ef 4e          	movb   $0x4e,-0x11(%ebp)
        if (test_read_into_cow_less_forks(size, offset, read_count, describe_size, describe_offset)) {
    1971:	83 c4 04             	add    $0x4,%esp
    1974:	ff 75 18             	pushl  0x18(%ebp)
    1977:	ff 75 14             	pushl  0x14(%ebp)
    197a:	ff 75 10             	pushl  0x10(%ebp)
    197d:	ff 75 0c             	pushl  0xc(%ebp)
    1980:	ff 75 08             	pushl  0x8(%ebp)
    1983:	e8 c6 fc ff ff       	call   164e <test_read_into_cow_less_forks>
    1988:	83 c4 20             	add    $0x20,%esp
    198b:	85 c0                	test   %eax,%eax
    198d:	74 04                	je     1993 <test_read_into_cow+0x99>
            result_str[0] = 'Y';
    198f:	c6 45 ef 59          	movb   $0x59,-0x11(%ebp)
        write(pipe_fds[1], result_str, 1);
    1993:	83 ec 04             	sub    $0x4,%esp
    1996:	6a 01                	push   $0x1
    1998:	8d 45 ef             	lea    -0x11(%ebp),%eax
    199b:	50                   	push   %eax
    199c:	ff 75 f4             	pushl  -0xc(%ebp)
    199f:	e8 ff 02 00 00       	call   1ca3 <write>
        exit();
    19a4:	e8 da 02 00 00       	call   1c83 <exit>

000019a9 <test_dealloc_cow_less_forks>:

int test_dealloc_cow_less_forks(int size) {
    19a9:	55                   	push   %ebp
    19aa:	89 e5                	mov    %esp,%ebp
    19ac:	56                   	push   %esi
    19ad:	53                   	push   %ebx
    19ae:	8b 5d 08             	mov    0x8(%ebp),%ebx
    char *heap = sbrk(0);
    19b1:	83 ec 0c             	sub    $0xc,%esp
    19b4:	6a 00                	push   $0x0
    19b6:	e8 50 03 00 00       	call   1d0b <sbrk>
    19bb:	89 c6                	mov    %eax,%esi
    sbrk(size);
    19bd:	89 1c 24             	mov    %ebx,(%esp)
    19c0:	e8 46 03 00 00       	call   1d0b <sbrk>
    printf(1, "testing that deallocating (with sbrk()) shared copy-on-write memory in child does not change it in parent\n");
    19c5:	83 c4 08             	add    $0x8,%esp
    19c8:	68 60 2b 00 00       	push   $0x2b60
    19cd:	6a 01                	push   $0x1
    19cf:	e8 21 04 00 00       	call   1df5 <printf>
    for (int i = 0; i < size; ++i) {
    19d4:	83 c4 10             	add    $0x10,%esp
    19d7:	b8 00 00 00 00       	mov    $0x0,%eax
    19dc:	eb 07                	jmp    19e5 <test_dealloc_cow_less_forks+0x3c>
        heap[i] = 'Y';
    19de:	c6 04 06 59          	movb   $0x59,(%esi,%eax,1)
    for (int i = 0; i < size; ++i) {
    19e2:	83 c0 01             	add    $0x1,%eax
    19e5:	39 d8                	cmp    %ebx,%eax
    19e7:	7c f5                	jl     19de <test_dealloc_cow_less_forks+0x35>
    }
    int pid = fork();
    19e9:	e8 8d 02 00 00       	call   1c7b <fork>
    if (pid == 0) {
    19ee:	85 c0                	test   %eax,%eax
    19f0:	74 11                	je     1a03 <test_dealloc_cow_less_forks+0x5a>
        sbrk(-size);
        exit();
    } else {
        wait();
    19f2:	e8 94 02 00 00       	call   1c8b <wait>
        int found_wrong = 0;
        for (int i = 0; i < size; ++i) {
    19f7:	b8 00 00 00 00       	mov    $0x0,%eax
        int found_wrong = 0;
    19fc:	ba 00 00 00 00       	mov    $0x0,%edx
        for (int i = 0; i < size; ++i) {
    1a01:	eb 13                	jmp    1a16 <test_dealloc_cow_less_forks+0x6d>
        sbrk(-size);
    1a03:	83 ec 0c             	sub    $0xc,%esp
    1a06:	f7 db                	neg    %ebx
    1a08:	53                   	push   %ebx
    1a09:	e8 fd 02 00 00       	call   1d0b <sbrk>
        exit();
    1a0e:	e8 70 02 00 00       	call   1c83 <exit>
        for (int i = 0; i < size; ++i) {
    1a13:	83 c0 01             	add    $0x1,%eax
    1a16:	39 d8                	cmp    %ebx,%eax
    1a18:	7d 0d                	jge    1a27 <test_dealloc_cow_less_forks+0x7e>
            if (heap[i] != 'Y') {
    1a1a:	80 3c 06 59          	cmpb   $0x59,(%esi,%eax,1)
    1a1e:	74 f3                	je     1a13 <test_dealloc_cow_less_forks+0x6a>
                found_wrong = 1;
    1a20:	ba 01 00 00 00       	mov    $0x1,%edx
    1a25:	eb ec                	jmp    1a13 <test_dealloc_cow_less_forks+0x6a>
            }
        }
        if (found_wrong) {
    1a27:	85 d2                	test   %edx,%edx
    1a29:	75 1e                	jne    1a49 <test_dealloc_cow_less_forks+0xa0>
            printf(1, FAIL_MSG "wrong value in parent after sbrk(-size) in child\n");
            return 0;
        } else {
            printf(1, PASS_MSG "correct values in parent after sbrk(-size) in child\n");
    1a2b:	83 ec 08             	sub    $0x8,%esp
    1a2e:	68 0c 2c 00 00       	push   $0x2c0c
    1a33:	6a 01                	push   $0x1
    1a35:	e8 bb 03 00 00       	call   1df5 <printf>
            return 1;
    1a3a:	83 c4 10             	add    $0x10,%esp
    1a3d:	b8 01 00 00 00       	mov    $0x1,%eax
        }
    }
}
    1a42:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1a45:	5b                   	pop    %ebx
    1a46:	5e                   	pop    %esi
    1a47:	5d                   	pop    %ebp
    1a48:	c3                   	ret    
            printf(1, FAIL_MSG "wrong value in parent after sbrk(-size) in child\n");
    1a49:	83 ec 08             	sub    $0x8,%esp
    1a4c:	68 cc 2b 00 00       	push   $0x2bcc
    1a51:	6a 01                	push   $0x1
    1a53:	e8 9d 03 00 00       	call   1df5 <printf>
            return 0;
    1a58:	83 c4 10             	add    $0x10,%esp
    1a5b:	b8 00 00 00 00       	mov    $0x0,%eax
    1a60:	eb e0                	jmp    1a42 <test_dealloc_cow_less_forks+0x99>

00001a62 <main>:
#include "pagingtestlib.h"

int main(void) {
    1a62:	8d 4c 24 04          	lea    0x4(%esp),%ecx
    1a66:	83 e4 f0             	and    $0xfffffff0,%esp
    1a69:	ff 71 fc             	pushl  -0x4(%ecx)
    1a6c:	55                   	push   %ebp
    1a6d:	89 e5                	mov    %esp,%ebp
    1a6f:	53                   	push   %ebx
    1a70:	51                   	push   %ecx
    setup();
    1a71:	e8 29 e6 ff ff       	call   9f <setup>
    printf(1, COUNT_MSG, 8);
    1a76:	83 ec 04             	sub    $0x4,%esp
    1a79:	6a 08                	push   $0x8
    1a7b:	68 50 2c 00 00       	push   $0x2c50
    1a80:	6a 01                	push   $0x1
    1a82:	e8 6e 03 00 00       	call   1df5 <printf>
    char *p = sbrk(500 * 1024 * 1024);
    1a87:	c7 04 24 00 00 40 1f 	movl   $0x1f400000,(%esp)
    1a8e:	e8 78 02 00 00       	call   1d0b <sbrk>
    p[250 * 1024 * 1024] = 'A';
    1a93:	c6 80 00 00 a0 0f 41 	movb   $0x41,0xfa00000(%eax)
    int pid = fork();
    1a9a:	e8 dc 01 00 00       	call   1c7b <fork>
    if (pid == 0) {
    1a9f:	83 c4 10             	add    $0x10,%esp
    1aa2:	85 c0                	test   %eax,%eax
    1aa4:	74 28                	je     1ace <main+0x6c>
        for (int i = 0; i < 7; ++i) {
            test_copy_on_write(100 * 1024 * 1024, "100MB", 12);
        }
        exit();
    } else {
        wait();
    1aa6:	e8 e0 01 00 00       	call   1c8b <wait>
        printf(1, PASS_MSG "\n");
    1aab:	83 ec 08             	sub    $0x8,%esp
    1aae:	68 89 2e 00 00       	push   $0x2e89
    1ab3:	6a 01                	push   $0x1
    1ab5:	e8 3b 03 00 00       	call   1df5 <printf>
        finish();
    1aba:	e8 39 e6 ff ff       	call   f8 <finish>
    }
}
    1abf:	b8 00 00 00 00       	mov    $0x0,%eax
    1ac4:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1ac7:	59                   	pop    %ecx
    1ac8:	5b                   	pop    %ebx
    1ac9:	5d                   	pop    %ebp
    1aca:	8d 61 fc             	lea    -0x4(%ecx),%esp
    1acd:	c3                   	ret    
    1ace:	89 c3                	mov    %eax,%ebx
    1ad0:	eb 1a                	jmp    1aec <main+0x8a>
            test_copy_on_write(100 * 1024 * 1024, "100MB", 12);
    1ad2:	83 ec 04             	sub    $0x4,%esp
    1ad5:	6a 0c                	push   $0xc
    1ad7:	68 83 2e 00 00       	push   $0x2e83
    1adc:	68 00 00 40 06       	push   $0x6400000
    1ae1:	e8 d6 f8 ff ff       	call   13bc <test_copy_on_write>
        for (int i = 0; i < 7; ++i) {
    1ae6:	83 c3 01             	add    $0x1,%ebx
    1ae9:	83 c4 10             	add    $0x10,%esp
    1aec:	83 fb 06             	cmp    $0x6,%ebx
    1aef:	7e e1                	jle    1ad2 <main+0x70>
        exit();
    1af1:	e8 8d 01 00 00       	call   1c83 <exit>

00001af6 <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
    1af6:	55                   	push   %ebp
    1af7:	89 e5                	mov    %esp,%ebp
    1af9:	53                   	push   %ebx
    1afa:	8b 45 08             	mov    0x8(%ebp),%eax
    1afd:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
    1b00:	89 c2                	mov    %eax,%edx
    1b02:	0f b6 19             	movzbl (%ecx),%ebx
    1b05:	88 1a                	mov    %bl,(%edx)
    1b07:	8d 52 01             	lea    0x1(%edx),%edx
    1b0a:	8d 49 01             	lea    0x1(%ecx),%ecx
    1b0d:	84 db                	test   %bl,%bl
    1b0f:	75 f1                	jne    1b02 <strcpy+0xc>
    ;
  return os;
}
    1b11:	5b                   	pop    %ebx
    1b12:	5d                   	pop    %ebp
    1b13:	c3                   	ret    

00001b14 <strcmp>:

int
strcmp(const char *p, const char *q)
{
    1b14:	55                   	push   %ebp
    1b15:	89 e5                	mov    %esp,%ebp
    1b17:	8b 4d 08             	mov    0x8(%ebp),%ecx
    1b1a:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
    1b1d:	eb 06                	jmp    1b25 <strcmp+0x11>
    p++, q++;
    1b1f:	83 c1 01             	add    $0x1,%ecx
    1b22:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
    1b25:	0f b6 01             	movzbl (%ecx),%eax
    1b28:	84 c0                	test   %al,%al
    1b2a:	74 04                	je     1b30 <strcmp+0x1c>
    1b2c:	3a 02                	cmp    (%edx),%al
    1b2e:	74 ef                	je     1b1f <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
    1b30:	0f b6 c0             	movzbl %al,%eax
    1b33:	0f b6 12             	movzbl (%edx),%edx
    1b36:	29 d0                	sub    %edx,%eax
}
    1b38:	5d                   	pop    %ebp
    1b39:	c3                   	ret    

00001b3a <strlen>:

uint
strlen(const char *s)
{
    1b3a:	55                   	push   %ebp
    1b3b:	89 e5                	mov    %esp,%ebp
    1b3d:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
    1b40:	ba 00 00 00 00       	mov    $0x0,%edx
    1b45:	eb 03                	jmp    1b4a <strlen+0x10>
    1b47:	83 c2 01             	add    $0x1,%edx
    1b4a:	89 d0                	mov    %edx,%eax
    1b4c:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
    1b50:	75 f5                	jne    1b47 <strlen+0xd>
    ;
  return n;
}
    1b52:	5d                   	pop    %ebp
    1b53:	c3                   	ret    

00001b54 <memset>:

void*
memset(void *dst, int c, uint n)
{
    1b54:	55                   	push   %ebp
    1b55:	89 e5                	mov    %esp,%ebp
    1b57:	57                   	push   %edi
    1b58:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
    1b5b:	89 d7                	mov    %edx,%edi
    1b5d:	8b 4d 10             	mov    0x10(%ebp),%ecx
    1b60:	8b 45 0c             	mov    0xc(%ebp),%eax
    1b63:	fc                   	cld    
    1b64:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
    1b66:	89 d0                	mov    %edx,%eax
    1b68:	5f                   	pop    %edi
    1b69:	5d                   	pop    %ebp
    1b6a:	c3                   	ret    

00001b6b <strchr>:

char*
strchr(const char *s, char c)
{
    1b6b:	55                   	push   %ebp
    1b6c:	89 e5                	mov    %esp,%ebp
    1b6e:	8b 45 08             	mov    0x8(%ebp),%eax
    1b71:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
    1b75:	0f b6 10             	movzbl (%eax),%edx
    1b78:	84 d2                	test   %dl,%dl
    1b7a:	74 09                	je     1b85 <strchr+0x1a>
    if(*s == c)
    1b7c:	38 ca                	cmp    %cl,%dl
    1b7e:	74 0a                	je     1b8a <strchr+0x1f>
  for(; *s; s++)
    1b80:	83 c0 01             	add    $0x1,%eax
    1b83:	eb f0                	jmp    1b75 <strchr+0xa>
      return (char*)s;
  return 0;
    1b85:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1b8a:	5d                   	pop    %ebp
    1b8b:	c3                   	ret    

00001b8c <gets>:

char*
gets(char *buf, int max)
{
    1b8c:	55                   	push   %ebp
    1b8d:	89 e5                	mov    %esp,%ebp
    1b8f:	57                   	push   %edi
    1b90:	56                   	push   %esi
    1b91:	53                   	push   %ebx
    1b92:	83 ec 1c             	sub    $0x1c,%esp
    1b95:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
    1b98:	bb 00 00 00 00       	mov    $0x0,%ebx
    1b9d:	8d 73 01             	lea    0x1(%ebx),%esi
    1ba0:	3b 75 0c             	cmp    0xc(%ebp),%esi
    1ba3:	7d 2e                	jge    1bd3 <gets+0x47>
    cc = read(0, &c, 1);
    1ba5:	83 ec 04             	sub    $0x4,%esp
    1ba8:	6a 01                	push   $0x1
    1baa:	8d 45 e7             	lea    -0x19(%ebp),%eax
    1bad:	50                   	push   %eax
    1bae:	6a 00                	push   $0x0
    1bb0:	e8 e6 00 00 00       	call   1c9b <read>
    if(cc < 1)
    1bb5:	83 c4 10             	add    $0x10,%esp
    1bb8:	85 c0                	test   %eax,%eax
    1bba:	7e 17                	jle    1bd3 <gets+0x47>
      break;
    buf[i++] = c;
    1bbc:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
    1bc0:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
    1bc3:	3c 0a                	cmp    $0xa,%al
    1bc5:	0f 94 c2             	sete   %dl
    1bc8:	3c 0d                	cmp    $0xd,%al
    1bca:	0f 94 c0             	sete   %al
    buf[i++] = c;
    1bcd:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
    1bcf:	08 c2                	or     %al,%dl
    1bd1:	74 ca                	je     1b9d <gets+0x11>
      break;
  }
  buf[i] = '\0';
    1bd3:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
    1bd7:	89 f8                	mov    %edi,%eax
    1bd9:	8d 65 f4             	lea    -0xc(%ebp),%esp
    1bdc:	5b                   	pop    %ebx
    1bdd:	5e                   	pop    %esi
    1bde:	5f                   	pop    %edi
    1bdf:	5d                   	pop    %ebp
    1be0:	c3                   	ret    

00001be1 <stat>:

int
stat(const char *n, struct stat *st)
{
    1be1:	55                   	push   %ebp
    1be2:	89 e5                	mov    %esp,%ebp
    1be4:	56                   	push   %esi
    1be5:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    1be6:	83 ec 08             	sub    $0x8,%esp
    1be9:	6a 00                	push   $0x0
    1beb:	ff 75 08             	pushl  0x8(%ebp)
    1bee:	e8 d0 00 00 00       	call   1cc3 <open>
  if(fd < 0)
    1bf3:	83 c4 10             	add    $0x10,%esp
    1bf6:	85 c0                	test   %eax,%eax
    1bf8:	78 24                	js     1c1e <stat+0x3d>
    1bfa:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
    1bfc:	83 ec 08             	sub    $0x8,%esp
    1bff:	ff 75 0c             	pushl  0xc(%ebp)
    1c02:	50                   	push   %eax
    1c03:	e8 d3 00 00 00       	call   1cdb <fstat>
    1c08:	89 c6                	mov    %eax,%esi
  close(fd);
    1c0a:	89 1c 24             	mov    %ebx,(%esp)
    1c0d:	e8 99 00 00 00       	call   1cab <close>
  return r;
    1c12:	83 c4 10             	add    $0x10,%esp
}
    1c15:	89 f0                	mov    %esi,%eax
    1c17:	8d 65 f8             	lea    -0x8(%ebp),%esp
    1c1a:	5b                   	pop    %ebx
    1c1b:	5e                   	pop    %esi
    1c1c:	5d                   	pop    %ebp
    1c1d:	c3                   	ret    
    return -1;
    1c1e:	be ff ff ff ff       	mov    $0xffffffff,%esi
    1c23:	eb f0                	jmp    1c15 <stat+0x34>

00001c25 <atoi>:

int
atoi(const char *s)
{
    1c25:	55                   	push   %ebp
    1c26:	89 e5                	mov    %esp,%ebp
    1c28:	53                   	push   %ebx
    1c29:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
    1c2c:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
    1c31:	eb 10                	jmp    1c43 <atoi+0x1e>
    n = n*10 + *s++ - '0';
    1c33:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
    1c36:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
    1c39:	83 c1 01             	add    $0x1,%ecx
    1c3c:	0f be d2             	movsbl %dl,%edx
    1c3f:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
    1c43:	0f b6 11             	movzbl (%ecx),%edx
    1c46:	8d 5a d0             	lea    -0x30(%edx),%ebx
    1c49:	80 fb 09             	cmp    $0x9,%bl
    1c4c:	76 e5                	jbe    1c33 <atoi+0xe>
  return n;
}
    1c4e:	5b                   	pop    %ebx
    1c4f:	5d                   	pop    %ebp
    1c50:	c3                   	ret    

00001c51 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
    1c51:	55                   	push   %ebp
    1c52:	89 e5                	mov    %esp,%ebp
    1c54:	56                   	push   %esi
    1c55:	53                   	push   %ebx
    1c56:	8b 45 08             	mov    0x8(%ebp),%eax
    1c59:	8b 5d 0c             	mov    0xc(%ebp),%ebx
    1c5c:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
    1c5f:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
    1c61:	eb 0d                	jmp    1c70 <memmove+0x1f>
    *dst++ = *src++;
    1c63:	0f b6 13             	movzbl (%ebx),%edx
    1c66:	88 11                	mov    %dl,(%ecx)
    1c68:	8d 5b 01             	lea    0x1(%ebx),%ebx
    1c6b:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
    1c6e:	89 f2                	mov    %esi,%edx
    1c70:	8d 72 ff             	lea    -0x1(%edx),%esi
    1c73:	85 d2                	test   %edx,%edx
    1c75:	7f ec                	jg     1c63 <memmove+0x12>
  return vdst;
}
    1c77:	5b                   	pop    %ebx
    1c78:	5e                   	pop    %esi
    1c79:	5d                   	pop    %ebp
    1c7a:	c3                   	ret    

00001c7b <fork>:
    1c7b:	b8 01 00 00 00       	mov    $0x1,%eax
    1c80:	cd 40                	int    $0x40
    1c82:	c3                   	ret    

00001c83 <exit>:
    1c83:	b8 02 00 00 00       	mov    $0x2,%eax
    1c88:	cd 40                	int    $0x40
    1c8a:	c3                   	ret    

00001c8b <wait>:
    1c8b:	b8 03 00 00 00       	mov    $0x3,%eax
    1c90:	cd 40                	int    $0x40
    1c92:	c3                   	ret    

00001c93 <pipe>:
    1c93:	b8 04 00 00 00       	mov    $0x4,%eax
    1c98:	cd 40                	int    $0x40
    1c9a:	c3                   	ret    

00001c9b <read>:
    1c9b:	b8 05 00 00 00       	mov    $0x5,%eax
    1ca0:	cd 40                	int    $0x40
    1ca2:	c3                   	ret    

00001ca3 <write>:
    1ca3:	b8 10 00 00 00       	mov    $0x10,%eax
    1ca8:	cd 40                	int    $0x40
    1caa:	c3                   	ret    

00001cab <close>:
    1cab:	b8 15 00 00 00       	mov    $0x15,%eax
    1cb0:	cd 40                	int    $0x40
    1cb2:	c3                   	ret    

00001cb3 <kill>:
    1cb3:	b8 06 00 00 00       	mov    $0x6,%eax
    1cb8:	cd 40                	int    $0x40
    1cba:	c3                   	ret    

00001cbb <exec>:
    1cbb:	b8 07 00 00 00       	mov    $0x7,%eax
    1cc0:	cd 40                	int    $0x40
    1cc2:	c3                   	ret    

00001cc3 <open>:
    1cc3:	b8 0f 00 00 00       	mov    $0xf,%eax
    1cc8:	cd 40                	int    $0x40
    1cca:	c3                   	ret    

00001ccb <mknod>:
    1ccb:	b8 11 00 00 00       	mov    $0x11,%eax
    1cd0:	cd 40                	int    $0x40
    1cd2:	c3                   	ret    

00001cd3 <unlink>:
    1cd3:	b8 12 00 00 00       	mov    $0x12,%eax
    1cd8:	cd 40                	int    $0x40
    1cda:	c3                   	ret    

00001cdb <fstat>:
    1cdb:	b8 08 00 00 00       	mov    $0x8,%eax
    1ce0:	cd 40                	int    $0x40
    1ce2:	c3                   	ret    

00001ce3 <link>:
    1ce3:	b8 13 00 00 00       	mov    $0x13,%eax
    1ce8:	cd 40                	int    $0x40
    1cea:	c3                   	ret    

00001ceb <mkdir>:
    1ceb:	b8 14 00 00 00       	mov    $0x14,%eax
    1cf0:	cd 40                	int    $0x40
    1cf2:	c3                   	ret    

00001cf3 <chdir>:
    1cf3:	b8 09 00 00 00       	mov    $0x9,%eax
    1cf8:	cd 40                	int    $0x40
    1cfa:	c3                   	ret    

00001cfb <dup>:
    1cfb:	b8 0a 00 00 00       	mov    $0xa,%eax
    1d00:	cd 40                	int    $0x40
    1d02:	c3                   	ret    

00001d03 <getpid>:
    1d03:	b8 0b 00 00 00       	mov    $0xb,%eax
    1d08:	cd 40                	int    $0x40
    1d0a:	c3                   	ret    

00001d0b <sbrk>:
    1d0b:	b8 0c 00 00 00       	mov    $0xc,%eax
    1d10:	cd 40                	int    $0x40
    1d12:	c3                   	ret    

00001d13 <sleep>:
    1d13:	b8 0d 00 00 00       	mov    $0xd,%eax
    1d18:	cd 40                	int    $0x40
    1d1a:	c3                   	ret    

00001d1b <uptime>:
    1d1b:	b8 0e 00 00 00       	mov    $0xe,%eax
    1d20:	cd 40                	int    $0x40
    1d22:	c3                   	ret    

00001d23 <yield>:
    1d23:	b8 16 00 00 00       	mov    $0x16,%eax
    1d28:	cd 40                	int    $0x40
    1d2a:	c3                   	ret    

00001d2b <shutdown>:
    1d2b:	b8 17 00 00 00       	mov    $0x17,%eax
    1d30:	cd 40                	int    $0x40
    1d32:	c3                   	ret    

00001d33 <writecount>:
    1d33:	b8 18 00 00 00       	mov    $0x18,%eax
    1d38:	cd 40                	int    $0x40
    1d3a:	c3                   	ret    

00001d3b <setwritecount>:
    1d3b:	b8 19 00 00 00       	mov    $0x19,%eax
    1d40:	cd 40                	int    $0x40
    1d42:	c3                   	ret    

00001d43 <settickets>:
    1d43:	b8 1a 00 00 00       	mov    $0x1a,%eax
    1d48:	cd 40                	int    $0x40
    1d4a:	c3                   	ret    

00001d4b <getprocessesinfo>:
    1d4b:	b8 1b 00 00 00       	mov    $0x1b,%eax
    1d50:	cd 40                	int    $0x40
    1d52:	c3                   	ret    

00001d53 <dumppagetable>:
    1d53:	b8 1c 00 00 00       	mov    $0x1c,%eax
    1d58:	cd 40                	int    $0x40
    1d5a:	c3                   	ret    

00001d5b <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
    1d5b:	55                   	push   %ebp
    1d5c:	89 e5                	mov    %esp,%ebp
    1d5e:	83 ec 1c             	sub    $0x1c,%esp
    1d61:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
    1d64:	6a 01                	push   $0x1
    1d66:	8d 55 f4             	lea    -0xc(%ebp),%edx
    1d69:	52                   	push   %edx
    1d6a:	50                   	push   %eax
    1d6b:	e8 33 ff ff ff       	call   1ca3 <write>
}
    1d70:	83 c4 10             	add    $0x10,%esp
    1d73:	c9                   	leave  
    1d74:	c3                   	ret    

00001d75 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
    1d75:	55                   	push   %ebp
    1d76:	89 e5                	mov    %esp,%ebp
    1d78:	57                   	push   %edi
    1d79:	56                   	push   %esi
    1d7a:	53                   	push   %ebx
    1d7b:	83 ec 2c             	sub    $0x2c,%esp
    1d7e:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
    1d80:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
    1d84:	0f 95 c3             	setne  %bl
    1d87:	89 d0                	mov    %edx,%eax
    1d89:	c1 e8 1f             	shr    $0x1f,%eax
    1d8c:	84 c3                	test   %al,%bl
    1d8e:	74 10                	je     1da0 <printint+0x2b>
    neg = 1;
    x = -xx;
    1d90:	f7 da                	neg    %edx
    neg = 1;
    1d92:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
    1d99:	be 00 00 00 00       	mov    $0x0,%esi
    1d9e:	eb 0b                	jmp    1dab <printint+0x36>
  neg = 0;
    1da0:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
    1da7:	eb f0                	jmp    1d99 <printint+0x24>
  do{
    buf[i++] = digits[x % base];
    1da9:	89 c6                	mov    %eax,%esi
    1dab:	89 d0                	mov    %edx,%eax
    1dad:	ba 00 00 00 00       	mov    $0x0,%edx
    1db2:	f7 f1                	div    %ecx
    1db4:	89 c3                	mov    %eax,%ebx
    1db6:	8d 46 01             	lea    0x1(%esi),%eax
    1db9:	0f b6 92 a0 2e 00 00 	movzbl 0x2ea0(%edx),%edx
    1dc0:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
    1dc4:	89 da                	mov    %ebx,%edx
    1dc6:	85 db                	test   %ebx,%ebx
    1dc8:	75 df                	jne    1da9 <printint+0x34>
    1dca:	89 c3                	mov    %eax,%ebx
  if(neg)
    1dcc:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
    1dd0:	74 16                	je     1de8 <printint+0x73>
    buf[i++] = '-';
    1dd2:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
    1dd7:	8d 5e 02             	lea    0x2(%esi),%ebx
    1dda:	eb 0c                	jmp    1de8 <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
    1ddc:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
    1de1:	89 f8                	mov    %edi,%eax
    1de3:	e8 73 ff ff ff       	call   1d5b <putc>
  while(--i >= 0)
    1de8:	83 eb 01             	sub    $0x1,%ebx
    1deb:	79 ef                	jns    1ddc <printint+0x67>
}
    1ded:	83 c4 2c             	add    $0x2c,%esp
    1df0:	5b                   	pop    %ebx
    1df1:	5e                   	pop    %esi
    1df2:	5f                   	pop    %edi
    1df3:	5d                   	pop    %ebp
    1df4:	c3                   	ret    

00001df5 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
    1df5:	55                   	push   %ebp
    1df6:	89 e5                	mov    %esp,%ebp
    1df8:	57                   	push   %edi
    1df9:	56                   	push   %esi
    1dfa:	53                   	push   %ebx
    1dfb:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
    1dfe:	8d 45 10             	lea    0x10(%ebp),%eax
    1e01:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
    1e04:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
    1e09:	bb 00 00 00 00       	mov    $0x0,%ebx
    1e0e:	eb 14                	jmp    1e24 <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
    1e10:	89 fa                	mov    %edi,%edx
    1e12:	8b 45 08             	mov    0x8(%ebp),%eax
    1e15:	e8 41 ff ff ff       	call   1d5b <putc>
    1e1a:	eb 05                	jmp    1e21 <printf+0x2c>
      }
    } else if(state == '%'){
    1e1c:	83 fe 25             	cmp    $0x25,%esi
    1e1f:	74 25                	je     1e46 <printf+0x51>
  for(i = 0; fmt[i]; i++){
    1e21:	83 c3 01             	add    $0x1,%ebx
    1e24:	8b 45 0c             	mov    0xc(%ebp),%eax
    1e27:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
    1e2b:	84 c0                	test   %al,%al
    1e2d:	0f 84 23 01 00 00    	je     1f56 <printf+0x161>
    c = fmt[i] & 0xff;
    1e33:	0f be f8             	movsbl %al,%edi
    1e36:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
    1e39:	85 f6                	test   %esi,%esi
    1e3b:	75 df                	jne    1e1c <printf+0x27>
      if(c == '%'){
    1e3d:	83 f8 25             	cmp    $0x25,%eax
    1e40:	75 ce                	jne    1e10 <printf+0x1b>
        state = '%';
    1e42:	89 c6                	mov    %eax,%esi
    1e44:	eb db                	jmp    1e21 <printf+0x2c>
      if(c == 'd'){
    1e46:	83 f8 64             	cmp    $0x64,%eax
    1e49:	74 49                	je     1e94 <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
    1e4b:	83 f8 78             	cmp    $0x78,%eax
    1e4e:	0f 94 c1             	sete   %cl
    1e51:	83 f8 70             	cmp    $0x70,%eax
    1e54:	0f 94 c2             	sete   %dl
    1e57:	08 d1                	or     %dl,%cl
    1e59:	75 63                	jne    1ebe <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
    1e5b:	83 f8 73             	cmp    $0x73,%eax
    1e5e:	0f 84 84 00 00 00    	je     1ee8 <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
    1e64:	83 f8 63             	cmp    $0x63,%eax
    1e67:	0f 84 b7 00 00 00    	je     1f24 <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
    1e6d:	83 f8 25             	cmp    $0x25,%eax
    1e70:	0f 84 cc 00 00 00    	je     1f42 <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    1e76:	ba 25 00 00 00       	mov    $0x25,%edx
    1e7b:	8b 45 08             	mov    0x8(%ebp),%eax
    1e7e:	e8 d8 fe ff ff       	call   1d5b <putc>
        putc(fd, c);
    1e83:	89 fa                	mov    %edi,%edx
    1e85:	8b 45 08             	mov    0x8(%ebp),%eax
    1e88:	e8 ce fe ff ff       	call   1d5b <putc>
      }
      state = 0;
    1e8d:	be 00 00 00 00       	mov    $0x0,%esi
    1e92:	eb 8d                	jmp    1e21 <printf+0x2c>
        printint(fd, *ap, 10, 1);
    1e94:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1e97:	8b 17                	mov    (%edi),%edx
    1e99:	83 ec 0c             	sub    $0xc,%esp
    1e9c:	6a 01                	push   $0x1
    1e9e:	b9 0a 00 00 00       	mov    $0xa,%ecx
    1ea3:	8b 45 08             	mov    0x8(%ebp),%eax
    1ea6:	e8 ca fe ff ff       	call   1d75 <printint>
        ap++;
    1eab:	83 c7 04             	add    $0x4,%edi
    1eae:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    1eb1:	83 c4 10             	add    $0x10,%esp
      state = 0;
    1eb4:	be 00 00 00 00       	mov    $0x0,%esi
    1eb9:	e9 63 ff ff ff       	jmp    1e21 <printf+0x2c>
        printint(fd, *ap, 16, 0);
    1ebe:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1ec1:	8b 17                	mov    (%edi),%edx
    1ec3:	83 ec 0c             	sub    $0xc,%esp
    1ec6:	6a 00                	push   $0x0
    1ec8:	b9 10 00 00 00       	mov    $0x10,%ecx
    1ecd:	8b 45 08             	mov    0x8(%ebp),%eax
    1ed0:	e8 a0 fe ff ff       	call   1d75 <printint>
        ap++;
    1ed5:	83 c7 04             	add    $0x4,%edi
    1ed8:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    1edb:	83 c4 10             	add    $0x10,%esp
      state = 0;
    1ede:	be 00 00 00 00       	mov    $0x0,%esi
    1ee3:	e9 39 ff ff ff       	jmp    1e21 <printf+0x2c>
        s = (char*)*ap;
    1ee8:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    1eeb:	8b 30                	mov    (%eax),%esi
        ap++;
    1eed:	83 c0 04             	add    $0x4,%eax
    1ef0:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
    1ef3:	85 f6                	test   %esi,%esi
    1ef5:	75 28                	jne    1f1f <printf+0x12a>
          s = "(null)";
    1ef7:	be 98 2e 00 00       	mov    $0x2e98,%esi
    1efc:	8b 7d 08             	mov    0x8(%ebp),%edi
    1eff:	eb 0d                	jmp    1f0e <printf+0x119>
          putc(fd, *s);
    1f01:	0f be d2             	movsbl %dl,%edx
    1f04:	89 f8                	mov    %edi,%eax
    1f06:	e8 50 fe ff ff       	call   1d5b <putc>
          s++;
    1f0b:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
    1f0e:	0f b6 16             	movzbl (%esi),%edx
    1f11:	84 d2                	test   %dl,%dl
    1f13:	75 ec                	jne    1f01 <printf+0x10c>
      state = 0;
    1f15:	be 00 00 00 00       	mov    $0x0,%esi
    1f1a:	e9 02 ff ff ff       	jmp    1e21 <printf+0x2c>
    1f1f:	8b 7d 08             	mov    0x8(%ebp),%edi
    1f22:	eb ea                	jmp    1f0e <printf+0x119>
        putc(fd, *ap);
    1f24:	8b 7d e4             	mov    -0x1c(%ebp),%edi
    1f27:	0f be 17             	movsbl (%edi),%edx
    1f2a:	8b 45 08             	mov    0x8(%ebp),%eax
    1f2d:	e8 29 fe ff ff       	call   1d5b <putc>
        ap++;
    1f32:	83 c7 04             	add    $0x4,%edi
    1f35:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
    1f38:	be 00 00 00 00       	mov    $0x0,%esi
    1f3d:	e9 df fe ff ff       	jmp    1e21 <printf+0x2c>
        putc(fd, c);
    1f42:	89 fa                	mov    %edi,%edx
    1f44:	8b 45 08             	mov    0x8(%ebp),%eax
    1f47:	e8 0f fe ff ff       	call   1d5b <putc>
      state = 0;
    1f4c:	be 00 00 00 00       	mov    $0x0,%esi
    1f51:	e9 cb fe ff ff       	jmp    1e21 <printf+0x2c>
    }
  }
}
    1f56:	8d 65 f4             	lea    -0xc(%ebp),%esp
    1f59:	5b                   	pop    %ebx
    1f5a:	5e                   	pop    %esi
    1f5b:	5f                   	pop    %edi
    1f5c:	5d                   	pop    %ebp
    1f5d:	c3                   	ret    

00001f5e <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
    1f5e:	55                   	push   %ebp
    1f5f:	89 e5                	mov    %esp,%ebp
    1f61:	57                   	push   %edi
    1f62:	56                   	push   %esi
    1f63:	53                   	push   %ebx
    1f64:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
    1f67:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1f6a:	a1 e0 36 00 00       	mov    0x36e0,%eax
    1f6f:	eb 02                	jmp    1f73 <free+0x15>
    1f71:	89 d0                	mov    %edx,%eax
    1f73:	39 c8                	cmp    %ecx,%eax
    1f75:	73 04                	jae    1f7b <free+0x1d>
    1f77:	39 08                	cmp    %ecx,(%eax)
    1f79:	77 12                	ja     1f8d <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    1f7b:	8b 10                	mov    (%eax),%edx
    1f7d:	39 c2                	cmp    %eax,%edx
    1f7f:	77 f0                	ja     1f71 <free+0x13>
    1f81:	39 c8                	cmp    %ecx,%eax
    1f83:	72 08                	jb     1f8d <free+0x2f>
    1f85:	39 ca                	cmp    %ecx,%edx
    1f87:	77 04                	ja     1f8d <free+0x2f>
    1f89:	89 d0                	mov    %edx,%eax
    1f8b:	eb e6                	jmp    1f73 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
    1f8d:	8b 73 fc             	mov    -0x4(%ebx),%esi
    1f90:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
    1f93:	8b 10                	mov    (%eax),%edx
    1f95:	39 d7                	cmp    %edx,%edi
    1f97:	74 19                	je     1fb2 <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
    1f99:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
    1f9c:	8b 50 04             	mov    0x4(%eax),%edx
    1f9f:	8d 34 d0             	lea    (%eax,%edx,8),%esi
    1fa2:	39 ce                	cmp    %ecx,%esi
    1fa4:	74 1b                	je     1fc1 <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
    1fa6:	89 08                	mov    %ecx,(%eax)
  freep = p;
    1fa8:	a3 e0 36 00 00       	mov    %eax,0x36e0
}
    1fad:	5b                   	pop    %ebx
    1fae:	5e                   	pop    %esi
    1faf:	5f                   	pop    %edi
    1fb0:	5d                   	pop    %ebp
    1fb1:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
    1fb2:	03 72 04             	add    0x4(%edx),%esi
    1fb5:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
    1fb8:	8b 10                	mov    (%eax),%edx
    1fba:	8b 12                	mov    (%edx),%edx
    1fbc:	89 53 f8             	mov    %edx,-0x8(%ebx)
    1fbf:	eb db                	jmp    1f9c <free+0x3e>
    p->s.size += bp->s.size;
    1fc1:	03 53 fc             	add    -0x4(%ebx),%edx
    1fc4:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
    1fc7:	8b 53 f8             	mov    -0x8(%ebx),%edx
    1fca:	89 10                	mov    %edx,(%eax)
    1fcc:	eb da                	jmp    1fa8 <free+0x4a>

00001fce <morecore>:

static Header*
morecore(uint nu)
{
    1fce:	55                   	push   %ebp
    1fcf:	89 e5                	mov    %esp,%ebp
    1fd1:	53                   	push   %ebx
    1fd2:	83 ec 04             	sub    $0x4,%esp
    1fd5:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
    1fd7:	3d ff 0f 00 00       	cmp    $0xfff,%eax
    1fdc:	77 05                	ja     1fe3 <morecore+0x15>
    nu = 4096;
    1fde:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
    1fe3:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
    1fea:	83 ec 0c             	sub    $0xc,%esp
    1fed:	50                   	push   %eax
    1fee:	e8 18 fd ff ff       	call   1d0b <sbrk>
  if(p == (char*)-1)
    1ff3:	83 c4 10             	add    $0x10,%esp
    1ff6:	83 f8 ff             	cmp    $0xffffffff,%eax
    1ff9:	74 1c                	je     2017 <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
    1ffb:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
    1ffe:	83 c0 08             	add    $0x8,%eax
    2001:	83 ec 0c             	sub    $0xc,%esp
    2004:	50                   	push   %eax
    2005:	e8 54 ff ff ff       	call   1f5e <free>
  return freep;
    200a:	a1 e0 36 00 00       	mov    0x36e0,%eax
    200f:	83 c4 10             	add    $0x10,%esp
}
    2012:	8b 5d fc             	mov    -0x4(%ebp),%ebx
    2015:	c9                   	leave  
    2016:	c3                   	ret    
    return 0;
    2017:	b8 00 00 00 00       	mov    $0x0,%eax
    201c:	eb f4                	jmp    2012 <morecore+0x44>

0000201e <malloc>:

void*
malloc(uint nbytes)
{
    201e:	55                   	push   %ebp
    201f:	89 e5                	mov    %esp,%ebp
    2021:	53                   	push   %ebx
    2022:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
    2025:	8b 45 08             	mov    0x8(%ebp),%eax
    2028:	8d 58 07             	lea    0x7(%eax),%ebx
    202b:	c1 eb 03             	shr    $0x3,%ebx
    202e:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
    2031:	8b 0d e0 36 00 00    	mov    0x36e0,%ecx
    2037:	85 c9                	test   %ecx,%ecx
    2039:	74 04                	je     203f <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    203b:	8b 01                	mov    (%ecx),%eax
    203d:	eb 4d                	jmp    208c <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
    203f:	c7 05 e0 36 00 00 e4 	movl   $0x36e4,0x36e0
    2046:	36 00 00 
    2049:	c7 05 e4 36 00 00 e4 	movl   $0x36e4,0x36e4
    2050:	36 00 00 
    base.s.size = 0;
    2053:	c7 05 e8 36 00 00 00 	movl   $0x0,0x36e8
    205a:	00 00 00 
    base.s.ptr = freep = prevp = &base;
    205d:	b9 e4 36 00 00       	mov    $0x36e4,%ecx
    2062:	eb d7                	jmp    203b <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
    2064:	39 da                	cmp    %ebx,%edx
    2066:	74 1a                	je     2082 <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
    2068:	29 da                	sub    %ebx,%edx
    206a:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
    206d:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
    2070:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
    2073:	89 0d e0 36 00 00    	mov    %ecx,0x36e0
      return (void*)(p + 1);
    2079:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
    207c:	83 c4 04             	add    $0x4,%esp
    207f:	5b                   	pop    %ebx
    2080:	5d                   	pop    %ebp
    2081:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
    2082:	8b 10                	mov    (%eax),%edx
    2084:	89 11                	mov    %edx,(%ecx)
    2086:	eb eb                	jmp    2073 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
    2088:	89 c1                	mov    %eax,%ecx
    208a:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
    208c:	8b 50 04             	mov    0x4(%eax),%edx
    208f:	39 da                	cmp    %ebx,%edx
    2091:	73 d1                	jae    2064 <malloc+0x46>
    if(p == freep)
    2093:	39 05 e0 36 00 00    	cmp    %eax,0x36e0
    2099:	75 ed                	jne    2088 <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
    209b:	89 d8                	mov    %ebx,%eax
    209d:	e8 2c ff ff ff       	call   1fce <morecore>
    20a2:	85 c0                	test   %eax,%eax
    20a4:	75 e2                	jne    2088 <malloc+0x6a>
        return 0;
    20a6:	b8 00 00 00 00       	mov    $0x0,%eax
    20ab:	eb cf                	jmp    207c <malloc+0x5e>
