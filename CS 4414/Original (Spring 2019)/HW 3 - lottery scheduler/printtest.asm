
_printtest:     file format elf32-i386


Disassembly of section .text:

00000000 <main>:
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
   0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
   4:	83 e4 f0             	and    $0xfffffff0,%esp
   7:	ff 71 fc             	pushl  -0x4(%ecx)
   a:	55                   	push   %ebp
   b:	89 e5                	mov    %esp,%ebp
   d:	51                   	push   %ecx
   e:	83 ec 04             	sub    $0x4,%esp
  printf(1, "Write count: %d\n", writecount());
  11:	e8 b2 02 00 00       	call   2c8 <writecount>
  16:	83 ec 04             	sub    $0x4,%esp
  19:	50                   	push   %eax
  1a:	68 3c 06 00 00       	push   $0x63c
  1f:	6a 01                	push   $0x1
  21:	e8 5c 03 00 00       	call   382 <printf>
  setwritecount(21);
  26:	c7 04 24 15 00 00 00 	movl   $0x15,(%esp)
  2d:	e8 9e 02 00 00       	call   2d0 <setwritecount>
  printf(1, "Write count: %d\n", writecount());
  32:	e8 91 02 00 00       	call   2c8 <writecount>
  37:	83 c4 0c             	add    $0xc,%esp
  3a:	50                   	push   %eax
  3b:	68 3c 06 00 00       	push   $0x63c
  40:	6a 01                	push   $0x1
  42:	e8 3b 03 00 00       	call   382 <printf>
  printf(1, "Write count: %d\n", writecount());
  47:	e8 7c 02 00 00       	call   2c8 <writecount>
  4c:	83 c4 0c             	add    $0xc,%esp
  4f:	50                   	push   %eax
  50:	68 3c 06 00 00       	push   $0x63c
  55:	6a 01                	push   $0x1
  57:	e8 26 03 00 00       	call   382 <printf>
  printf(1, "Write count: %d\n", writecount());
  5c:	e8 67 02 00 00       	call   2c8 <writecount>
  61:	83 c4 0c             	add    $0xc,%esp
  64:	50                   	push   %eax
  65:	68 3c 06 00 00       	push   $0x63c
  6a:	6a 01                	push   $0x1
  6c:	e8 11 03 00 00       	call   382 <printf>
  printf(1, "Write count: %d\n", writecount());
  71:	e8 52 02 00 00       	call   2c8 <writecount>
  76:	83 c4 0c             	add    $0xc,%esp
  79:	50                   	push   %eax
  7a:	68 3c 06 00 00       	push   $0x63c
  7f:	6a 01                	push   $0x1
  81:	e8 fc 02 00 00       	call   382 <printf>
  exit();
  86:	e8 8d 01 00 00       	call   218 <exit>

0000008b <strcpy>:
#include "user.h"
#include "x86.h"

char*
strcpy(char *s, const char *t)
{
  8b:	55                   	push   %ebp
  8c:	89 e5                	mov    %esp,%ebp
  8e:	53                   	push   %ebx
  8f:	8b 45 08             	mov    0x8(%ebp),%eax
  92:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  char *os;

  os = s;
  while((*s++ = *t++) != 0)
  95:	89 c2                	mov    %eax,%edx
  97:	0f b6 19             	movzbl (%ecx),%ebx
  9a:	88 1a                	mov    %bl,(%edx)
  9c:	8d 52 01             	lea    0x1(%edx),%edx
  9f:	8d 49 01             	lea    0x1(%ecx),%ecx
  a2:	84 db                	test   %bl,%bl
  a4:	75 f1                	jne    97 <strcpy+0xc>
    ;
  return os;
}
  a6:	5b                   	pop    %ebx
  a7:	5d                   	pop    %ebp
  a8:	c3                   	ret    

000000a9 <strcmp>:

int
strcmp(const char *p, const char *q)
{
  a9:	55                   	push   %ebp
  aa:	89 e5                	mov    %esp,%ebp
  ac:	8b 4d 08             	mov    0x8(%ebp),%ecx
  af:	8b 55 0c             	mov    0xc(%ebp),%edx
  while(*p && *p == *q)
  b2:	eb 06                	jmp    ba <strcmp+0x11>
    p++, q++;
  b4:	83 c1 01             	add    $0x1,%ecx
  b7:	83 c2 01             	add    $0x1,%edx
  while(*p && *p == *q)
  ba:	0f b6 01             	movzbl (%ecx),%eax
  bd:	84 c0                	test   %al,%al
  bf:	74 04                	je     c5 <strcmp+0x1c>
  c1:	3a 02                	cmp    (%edx),%al
  c3:	74 ef                	je     b4 <strcmp+0xb>
  return (uchar)*p - (uchar)*q;
  c5:	0f b6 c0             	movzbl %al,%eax
  c8:	0f b6 12             	movzbl (%edx),%edx
  cb:	29 d0                	sub    %edx,%eax
}
  cd:	5d                   	pop    %ebp
  ce:	c3                   	ret    

000000cf <strlen>:

uint
strlen(const char *s)
{
  cf:	55                   	push   %ebp
  d0:	89 e5                	mov    %esp,%ebp
  d2:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  for(n = 0; s[n]; n++)
  d5:	ba 00 00 00 00       	mov    $0x0,%edx
  da:	eb 03                	jmp    df <strlen+0x10>
  dc:	83 c2 01             	add    $0x1,%edx
  df:	89 d0                	mov    %edx,%eax
  e1:	80 3c 11 00          	cmpb   $0x0,(%ecx,%edx,1)
  e5:	75 f5                	jne    dc <strlen+0xd>
    ;
  return n;
}
  e7:	5d                   	pop    %ebp
  e8:	c3                   	ret    

000000e9 <memset>:

void*
memset(void *dst, int c, uint n)
{
  e9:	55                   	push   %ebp
  ea:	89 e5                	mov    %esp,%ebp
  ec:	57                   	push   %edi
  ed:	8b 55 08             	mov    0x8(%ebp),%edx
}

static inline void
stosb(void *addr, int data, int cnt)
{
  asm volatile("cld; rep stosb" :
  f0:	89 d7                	mov    %edx,%edi
  f2:	8b 4d 10             	mov    0x10(%ebp),%ecx
  f5:	8b 45 0c             	mov    0xc(%ebp),%eax
  f8:	fc                   	cld    
  f9:	f3 aa                	rep stos %al,%es:(%edi)
  stosb(dst, c, n);
  return dst;
}
  fb:	89 d0                	mov    %edx,%eax
  fd:	5f                   	pop    %edi
  fe:	5d                   	pop    %ebp
  ff:	c3                   	ret    

00000100 <strchr>:

char*
strchr(const char *s, char c)
{
 100:	55                   	push   %ebp
 101:	89 e5                	mov    %esp,%ebp
 103:	8b 45 08             	mov    0x8(%ebp),%eax
 106:	0f b6 4d 0c          	movzbl 0xc(%ebp),%ecx
  for(; *s; s++)
 10a:	0f b6 10             	movzbl (%eax),%edx
 10d:	84 d2                	test   %dl,%dl
 10f:	74 09                	je     11a <strchr+0x1a>
    if(*s == c)
 111:	38 ca                	cmp    %cl,%dl
 113:	74 0a                	je     11f <strchr+0x1f>
  for(; *s; s++)
 115:	83 c0 01             	add    $0x1,%eax
 118:	eb f0                	jmp    10a <strchr+0xa>
      return (char*)s;
  return 0;
 11a:	b8 00 00 00 00       	mov    $0x0,%eax
}
 11f:	5d                   	pop    %ebp
 120:	c3                   	ret    

00000121 <gets>:

char*
gets(char *buf, int max)
{
 121:	55                   	push   %ebp
 122:	89 e5                	mov    %esp,%ebp
 124:	57                   	push   %edi
 125:	56                   	push   %esi
 126:	53                   	push   %ebx
 127:	83 ec 1c             	sub    $0x1c,%esp
 12a:	8b 7d 08             	mov    0x8(%ebp),%edi
  int i, cc;
  char c;

  for(i=0; i+1 < max; ){
 12d:	bb 00 00 00 00       	mov    $0x0,%ebx
 132:	8d 73 01             	lea    0x1(%ebx),%esi
 135:	3b 75 0c             	cmp    0xc(%ebp),%esi
 138:	7d 2e                	jge    168 <gets+0x47>
    cc = read(0, &c, 1);
 13a:	83 ec 04             	sub    $0x4,%esp
 13d:	6a 01                	push   $0x1
 13f:	8d 45 e7             	lea    -0x19(%ebp),%eax
 142:	50                   	push   %eax
 143:	6a 00                	push   $0x0
 145:	e8 e6 00 00 00       	call   230 <read>
    if(cc < 1)
 14a:	83 c4 10             	add    $0x10,%esp
 14d:	85 c0                	test   %eax,%eax
 14f:	7e 17                	jle    168 <gets+0x47>
      break;
    buf[i++] = c;
 151:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
 155:	88 04 1f             	mov    %al,(%edi,%ebx,1)
    if(c == '\n' || c == '\r')
 158:	3c 0a                	cmp    $0xa,%al
 15a:	0f 94 c2             	sete   %dl
 15d:	3c 0d                	cmp    $0xd,%al
 15f:	0f 94 c0             	sete   %al
    buf[i++] = c;
 162:	89 f3                	mov    %esi,%ebx
    if(c == '\n' || c == '\r')
 164:	08 c2                	or     %al,%dl
 166:	74 ca                	je     132 <gets+0x11>
      break;
  }
  buf[i] = '\0';
 168:	c6 04 1f 00          	movb   $0x0,(%edi,%ebx,1)
  return buf;
}
 16c:	89 f8                	mov    %edi,%eax
 16e:	8d 65 f4             	lea    -0xc(%ebp),%esp
 171:	5b                   	pop    %ebx
 172:	5e                   	pop    %esi
 173:	5f                   	pop    %edi
 174:	5d                   	pop    %ebp
 175:	c3                   	ret    

00000176 <stat>:

int
stat(const char *n, struct stat *st)
{
 176:	55                   	push   %ebp
 177:	89 e5                	mov    %esp,%ebp
 179:	56                   	push   %esi
 17a:	53                   	push   %ebx
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 17b:	83 ec 08             	sub    $0x8,%esp
 17e:	6a 00                	push   $0x0
 180:	ff 75 08             	pushl  0x8(%ebp)
 183:	e8 d0 00 00 00       	call   258 <open>
  if(fd < 0)
 188:	83 c4 10             	add    $0x10,%esp
 18b:	85 c0                	test   %eax,%eax
 18d:	78 24                	js     1b3 <stat+0x3d>
 18f:	89 c3                	mov    %eax,%ebx
    return -1;
  r = fstat(fd, st);
 191:	83 ec 08             	sub    $0x8,%esp
 194:	ff 75 0c             	pushl  0xc(%ebp)
 197:	50                   	push   %eax
 198:	e8 d3 00 00 00       	call   270 <fstat>
 19d:	89 c6                	mov    %eax,%esi
  close(fd);
 19f:	89 1c 24             	mov    %ebx,(%esp)
 1a2:	e8 99 00 00 00       	call   240 <close>
  return r;
 1a7:	83 c4 10             	add    $0x10,%esp
}
 1aa:	89 f0                	mov    %esi,%eax
 1ac:	8d 65 f8             	lea    -0x8(%ebp),%esp
 1af:	5b                   	pop    %ebx
 1b0:	5e                   	pop    %esi
 1b1:	5d                   	pop    %ebp
 1b2:	c3                   	ret    
    return -1;
 1b3:	be ff ff ff ff       	mov    $0xffffffff,%esi
 1b8:	eb f0                	jmp    1aa <stat+0x34>

000001ba <atoi>:

int
atoi(const char *s)
{
 1ba:	55                   	push   %ebp
 1bb:	89 e5                	mov    %esp,%ebp
 1bd:	53                   	push   %ebx
 1be:	8b 4d 08             	mov    0x8(%ebp),%ecx
  int n;

  n = 0;
 1c1:	b8 00 00 00 00       	mov    $0x0,%eax
  while('0' <= *s && *s <= '9')
 1c6:	eb 10                	jmp    1d8 <atoi+0x1e>
    n = n*10 + *s++ - '0';
 1c8:	8d 1c 80             	lea    (%eax,%eax,4),%ebx
 1cb:	8d 04 1b             	lea    (%ebx,%ebx,1),%eax
 1ce:	83 c1 01             	add    $0x1,%ecx
 1d1:	0f be d2             	movsbl %dl,%edx
 1d4:	8d 44 02 d0          	lea    -0x30(%edx,%eax,1),%eax
  while('0' <= *s && *s <= '9')
 1d8:	0f b6 11             	movzbl (%ecx),%edx
 1db:	8d 5a d0             	lea    -0x30(%edx),%ebx
 1de:	80 fb 09             	cmp    $0x9,%bl
 1e1:	76 e5                	jbe    1c8 <atoi+0xe>
  return n;
}
 1e3:	5b                   	pop    %ebx
 1e4:	5d                   	pop    %ebp
 1e5:	c3                   	ret    

000001e6 <memmove>:

void*
memmove(void *vdst, const void *vsrc, int n)
{
 1e6:	55                   	push   %ebp
 1e7:	89 e5                	mov    %esp,%ebp
 1e9:	56                   	push   %esi
 1ea:	53                   	push   %ebx
 1eb:	8b 45 08             	mov    0x8(%ebp),%eax
 1ee:	8b 5d 0c             	mov    0xc(%ebp),%ebx
 1f1:	8b 55 10             	mov    0x10(%ebp),%edx
  char *dst;
  const char *src;

  dst = vdst;
 1f4:	89 c1                	mov    %eax,%ecx
  src = vsrc;
  while(n-- > 0)
 1f6:	eb 0d                	jmp    205 <memmove+0x1f>
    *dst++ = *src++;
 1f8:	0f b6 13             	movzbl (%ebx),%edx
 1fb:	88 11                	mov    %dl,(%ecx)
 1fd:	8d 5b 01             	lea    0x1(%ebx),%ebx
 200:	8d 49 01             	lea    0x1(%ecx),%ecx
  while(n-- > 0)
 203:	89 f2                	mov    %esi,%edx
 205:	8d 72 ff             	lea    -0x1(%edx),%esi
 208:	85 d2                	test   %edx,%edx
 20a:	7f ec                	jg     1f8 <memmove+0x12>
  return vdst;
}
 20c:	5b                   	pop    %ebx
 20d:	5e                   	pop    %esi
 20e:	5d                   	pop    %ebp
 20f:	c3                   	ret    

00000210 <fork>:
  name: \
    movl $SYS_ ## name, %eax; \
    int $T_SYSCALL; \
    ret

SYSCALL(fork)
 210:	b8 01 00 00 00       	mov    $0x1,%eax
 215:	cd 40                	int    $0x40
 217:	c3                   	ret    

00000218 <exit>:
SYSCALL(exit)
 218:	b8 02 00 00 00       	mov    $0x2,%eax
 21d:	cd 40                	int    $0x40
 21f:	c3                   	ret    

00000220 <wait>:
SYSCALL(wait)
 220:	b8 03 00 00 00       	mov    $0x3,%eax
 225:	cd 40                	int    $0x40
 227:	c3                   	ret    

00000228 <pipe>:
SYSCALL(pipe)
 228:	b8 04 00 00 00       	mov    $0x4,%eax
 22d:	cd 40                	int    $0x40
 22f:	c3                   	ret    

00000230 <read>:
SYSCALL(read)
 230:	b8 05 00 00 00       	mov    $0x5,%eax
 235:	cd 40                	int    $0x40
 237:	c3                   	ret    

00000238 <write>:
SYSCALL(write)
 238:	b8 10 00 00 00       	mov    $0x10,%eax
 23d:	cd 40                	int    $0x40
 23f:	c3                   	ret    

00000240 <close>:
SYSCALL(close)
 240:	b8 15 00 00 00       	mov    $0x15,%eax
 245:	cd 40                	int    $0x40
 247:	c3                   	ret    

00000248 <kill>:
SYSCALL(kill)
 248:	b8 06 00 00 00       	mov    $0x6,%eax
 24d:	cd 40                	int    $0x40
 24f:	c3                   	ret    

00000250 <exec>:
SYSCALL(exec)
 250:	b8 07 00 00 00       	mov    $0x7,%eax
 255:	cd 40                	int    $0x40
 257:	c3                   	ret    

00000258 <open>:
SYSCALL(open)
 258:	b8 0f 00 00 00       	mov    $0xf,%eax
 25d:	cd 40                	int    $0x40
 25f:	c3                   	ret    

00000260 <mknod>:
SYSCALL(mknod)
 260:	b8 11 00 00 00       	mov    $0x11,%eax
 265:	cd 40                	int    $0x40
 267:	c3                   	ret    

00000268 <unlink>:
SYSCALL(unlink)
 268:	b8 12 00 00 00       	mov    $0x12,%eax
 26d:	cd 40                	int    $0x40
 26f:	c3                   	ret    

00000270 <fstat>:
SYSCALL(fstat)
 270:	b8 08 00 00 00       	mov    $0x8,%eax
 275:	cd 40                	int    $0x40
 277:	c3                   	ret    

00000278 <link>:
SYSCALL(link)
 278:	b8 13 00 00 00       	mov    $0x13,%eax
 27d:	cd 40                	int    $0x40
 27f:	c3                   	ret    

00000280 <mkdir>:
SYSCALL(mkdir)
 280:	b8 14 00 00 00       	mov    $0x14,%eax
 285:	cd 40                	int    $0x40
 287:	c3                   	ret    

00000288 <chdir>:
SYSCALL(chdir)
 288:	b8 09 00 00 00       	mov    $0x9,%eax
 28d:	cd 40                	int    $0x40
 28f:	c3                   	ret    

00000290 <dup>:
SYSCALL(dup)
 290:	b8 0a 00 00 00       	mov    $0xa,%eax
 295:	cd 40                	int    $0x40
 297:	c3                   	ret    

00000298 <getpid>:
SYSCALL(getpid)
 298:	b8 0b 00 00 00       	mov    $0xb,%eax
 29d:	cd 40                	int    $0x40
 29f:	c3                   	ret    

000002a0 <sbrk>:
SYSCALL(sbrk)
 2a0:	b8 0c 00 00 00       	mov    $0xc,%eax
 2a5:	cd 40                	int    $0x40
 2a7:	c3                   	ret    

000002a8 <sleep>:
SYSCALL(sleep)
 2a8:	b8 0d 00 00 00       	mov    $0xd,%eax
 2ad:	cd 40                	int    $0x40
 2af:	c3                   	ret    

000002b0 <uptime>:
SYSCALL(uptime)
 2b0:	b8 0e 00 00 00       	mov    $0xe,%eax
 2b5:	cd 40                	int    $0x40
 2b7:	c3                   	ret    

000002b8 <yield>:
SYSCALL(yield)
 2b8:	b8 16 00 00 00       	mov    $0x16,%eax
 2bd:	cd 40                	int    $0x40
 2bf:	c3                   	ret    

000002c0 <shutdown>:
SYSCALL(shutdown)
 2c0:	b8 17 00 00 00       	mov    $0x17,%eax
 2c5:	cd 40                	int    $0x40
 2c7:	c3                   	ret    

000002c8 <writecount>:
SYSCALL(writecount)
 2c8:	b8 18 00 00 00       	mov    $0x18,%eax
 2cd:	cd 40                	int    $0x40
 2cf:	c3                   	ret    

000002d0 <setwritecount>:
SYSCALL(setwritecount)
 2d0:	b8 19 00 00 00       	mov    $0x19,%eax
 2d5:	cd 40                	int    $0x40
 2d7:	c3                   	ret    

000002d8 <settickets>:
SYSCALL(settickets)
 2d8:	b8 1a 00 00 00       	mov    $0x1a,%eax
 2dd:	cd 40                	int    $0x40
 2df:	c3                   	ret    

000002e0 <getprocessesinfo>:
 2e0:	b8 1b 00 00 00       	mov    $0x1b,%eax
 2e5:	cd 40                	int    $0x40
 2e7:	c3                   	ret    

000002e8 <putc>:
#include "stat.h"
#include "user.h"

static void
putc(int fd, char c)
{
 2e8:	55                   	push   %ebp
 2e9:	89 e5                	mov    %esp,%ebp
 2eb:	83 ec 1c             	sub    $0x1c,%esp
 2ee:	88 55 f4             	mov    %dl,-0xc(%ebp)
  write(fd, &c, 1);
 2f1:	6a 01                	push   $0x1
 2f3:	8d 55 f4             	lea    -0xc(%ebp),%edx
 2f6:	52                   	push   %edx
 2f7:	50                   	push   %eax
 2f8:	e8 3b ff ff ff       	call   238 <write>
}
 2fd:	83 c4 10             	add    $0x10,%esp
 300:	c9                   	leave  
 301:	c3                   	ret    

00000302 <printint>:

static void
printint(int fd, int xx, int base, int sgn)
{
 302:	55                   	push   %ebp
 303:	89 e5                	mov    %esp,%ebp
 305:	57                   	push   %edi
 306:	56                   	push   %esi
 307:	53                   	push   %ebx
 308:	83 ec 2c             	sub    $0x2c,%esp
 30b:	89 c7                	mov    %eax,%edi
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
  if(sgn && xx < 0){
 30d:	83 7d 08 00          	cmpl   $0x0,0x8(%ebp)
 311:	0f 95 c3             	setne  %bl
 314:	89 d0                	mov    %edx,%eax
 316:	c1 e8 1f             	shr    $0x1f,%eax
 319:	84 c3                	test   %al,%bl
 31b:	74 10                	je     32d <printint+0x2b>
    neg = 1;
    x = -xx;
 31d:	f7 da                	neg    %edx
    neg = 1;
 31f:	c7 45 d4 01 00 00 00 	movl   $0x1,-0x2c(%ebp)
  } else {
    x = xx;
  }

  i = 0;
 326:	be 00 00 00 00       	mov    $0x0,%esi
 32b:	eb 0b                	jmp    338 <printint+0x36>
  neg = 0;
 32d:	c7 45 d4 00 00 00 00 	movl   $0x0,-0x2c(%ebp)
 334:	eb f0                	jmp    326 <printint+0x24>
  do{
    buf[i++] = digits[x % base];
 336:	89 c6                	mov    %eax,%esi
 338:	89 d0                	mov    %edx,%eax
 33a:	ba 00 00 00 00       	mov    $0x0,%edx
 33f:	f7 f1                	div    %ecx
 341:	89 c3                	mov    %eax,%ebx
 343:	8d 46 01             	lea    0x1(%esi),%eax
 346:	0f b6 92 54 06 00 00 	movzbl 0x654(%edx),%edx
 34d:	88 54 35 d8          	mov    %dl,-0x28(%ebp,%esi,1)
  }while((x /= base) != 0);
 351:	89 da                	mov    %ebx,%edx
 353:	85 db                	test   %ebx,%ebx
 355:	75 df                	jne    336 <printint+0x34>
 357:	89 c3                	mov    %eax,%ebx
  if(neg)
 359:	83 7d d4 00          	cmpl   $0x0,-0x2c(%ebp)
 35d:	74 16                	je     375 <printint+0x73>
    buf[i++] = '-';
 35f:	c6 44 05 d8 2d       	movb   $0x2d,-0x28(%ebp,%eax,1)
 364:	8d 5e 02             	lea    0x2(%esi),%ebx
 367:	eb 0c                	jmp    375 <printint+0x73>

  while(--i >= 0)
    putc(fd, buf[i]);
 369:	0f be 54 1d d8       	movsbl -0x28(%ebp,%ebx,1),%edx
 36e:	89 f8                	mov    %edi,%eax
 370:	e8 73 ff ff ff       	call   2e8 <putc>
  while(--i >= 0)
 375:	83 eb 01             	sub    $0x1,%ebx
 378:	79 ef                	jns    369 <printint+0x67>
}
 37a:	83 c4 2c             	add    $0x2c,%esp
 37d:	5b                   	pop    %ebx
 37e:	5e                   	pop    %esi
 37f:	5f                   	pop    %edi
 380:	5d                   	pop    %ebp
 381:	c3                   	ret    

00000382 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void
printf(int fd, const char *fmt, ...)
{
 382:	55                   	push   %ebp
 383:	89 e5                	mov    %esp,%ebp
 385:	57                   	push   %edi
 386:	56                   	push   %esi
 387:	53                   	push   %ebx
 388:	83 ec 1c             	sub    $0x1c,%esp
  char *s;
  int c, i, state;
  uint *ap;

  state = 0;
  ap = (uint*)(void*)&fmt + 1;
 38b:	8d 45 10             	lea    0x10(%ebp),%eax
 38e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  state = 0;
 391:	be 00 00 00 00       	mov    $0x0,%esi
  for(i = 0; fmt[i]; i++){
 396:	bb 00 00 00 00       	mov    $0x0,%ebx
 39b:	eb 14                	jmp    3b1 <printf+0x2f>
    c = fmt[i] & 0xff;
    if(state == 0){
      if(c == '%'){
        state = '%';
      } else {
        putc(fd, c);
 39d:	89 fa                	mov    %edi,%edx
 39f:	8b 45 08             	mov    0x8(%ebp),%eax
 3a2:	e8 41 ff ff ff       	call   2e8 <putc>
 3a7:	eb 05                	jmp    3ae <printf+0x2c>
      }
    } else if(state == '%'){
 3a9:	83 fe 25             	cmp    $0x25,%esi
 3ac:	74 25                	je     3d3 <printf+0x51>
  for(i = 0; fmt[i]; i++){
 3ae:	83 c3 01             	add    $0x1,%ebx
 3b1:	8b 45 0c             	mov    0xc(%ebp),%eax
 3b4:	0f b6 04 18          	movzbl (%eax,%ebx,1),%eax
 3b8:	84 c0                	test   %al,%al
 3ba:	0f 84 23 01 00 00    	je     4e3 <printf+0x161>
    c = fmt[i] & 0xff;
 3c0:	0f be f8             	movsbl %al,%edi
 3c3:	0f b6 c0             	movzbl %al,%eax
    if(state == 0){
 3c6:	85 f6                	test   %esi,%esi
 3c8:	75 df                	jne    3a9 <printf+0x27>
      if(c == '%'){
 3ca:	83 f8 25             	cmp    $0x25,%eax
 3cd:	75 ce                	jne    39d <printf+0x1b>
        state = '%';
 3cf:	89 c6                	mov    %eax,%esi
 3d1:	eb db                	jmp    3ae <printf+0x2c>
      if(c == 'd'){
 3d3:	83 f8 64             	cmp    $0x64,%eax
 3d6:	74 49                	je     421 <printf+0x9f>
        printint(fd, *ap, 10, 1);
        ap++;
      } else if(c == 'x' || c == 'p'){
 3d8:	83 f8 78             	cmp    $0x78,%eax
 3db:	0f 94 c1             	sete   %cl
 3de:	83 f8 70             	cmp    $0x70,%eax
 3e1:	0f 94 c2             	sete   %dl
 3e4:	08 d1                	or     %dl,%cl
 3e6:	75 63                	jne    44b <printf+0xc9>
        printint(fd, *ap, 16, 0);
        ap++;
      } else if(c == 's'){
 3e8:	83 f8 73             	cmp    $0x73,%eax
 3eb:	0f 84 84 00 00 00    	je     475 <printf+0xf3>
          s = "(null)";
        while(*s != 0){
          putc(fd, *s);
          s++;
        }
      } else if(c == 'c'){
 3f1:	83 f8 63             	cmp    $0x63,%eax
 3f4:	0f 84 b7 00 00 00    	je     4b1 <printf+0x12f>
        putc(fd, *ap);
        ap++;
      } else if(c == '%'){
 3fa:	83 f8 25             	cmp    $0x25,%eax
 3fd:	0f 84 cc 00 00 00    	je     4cf <printf+0x14d>
        putc(fd, c);
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 403:	ba 25 00 00 00       	mov    $0x25,%edx
 408:	8b 45 08             	mov    0x8(%ebp),%eax
 40b:	e8 d8 fe ff ff       	call   2e8 <putc>
        putc(fd, c);
 410:	89 fa                	mov    %edi,%edx
 412:	8b 45 08             	mov    0x8(%ebp),%eax
 415:	e8 ce fe ff ff       	call   2e8 <putc>
      }
      state = 0;
 41a:	be 00 00 00 00       	mov    $0x0,%esi
 41f:	eb 8d                	jmp    3ae <printf+0x2c>
        printint(fd, *ap, 10, 1);
 421:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 424:	8b 17                	mov    (%edi),%edx
 426:	83 ec 0c             	sub    $0xc,%esp
 429:	6a 01                	push   $0x1
 42b:	b9 0a 00 00 00       	mov    $0xa,%ecx
 430:	8b 45 08             	mov    0x8(%ebp),%eax
 433:	e8 ca fe ff ff       	call   302 <printint>
        ap++;
 438:	83 c7 04             	add    $0x4,%edi
 43b:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 43e:	83 c4 10             	add    $0x10,%esp
      state = 0;
 441:	be 00 00 00 00       	mov    $0x0,%esi
 446:	e9 63 ff ff ff       	jmp    3ae <printf+0x2c>
        printint(fd, *ap, 16, 0);
 44b:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 44e:	8b 17                	mov    (%edi),%edx
 450:	83 ec 0c             	sub    $0xc,%esp
 453:	6a 00                	push   $0x0
 455:	b9 10 00 00 00       	mov    $0x10,%ecx
 45a:	8b 45 08             	mov    0x8(%ebp),%eax
 45d:	e8 a0 fe ff ff       	call   302 <printint>
        ap++;
 462:	83 c7 04             	add    $0x4,%edi
 465:	89 7d e4             	mov    %edi,-0x1c(%ebp)
 468:	83 c4 10             	add    $0x10,%esp
      state = 0;
 46b:	be 00 00 00 00       	mov    $0x0,%esi
 470:	e9 39 ff ff ff       	jmp    3ae <printf+0x2c>
        s = (char*)*ap;
 475:	8b 45 e4             	mov    -0x1c(%ebp),%eax
 478:	8b 30                	mov    (%eax),%esi
        ap++;
 47a:	83 c0 04             	add    $0x4,%eax
 47d:	89 45 e4             	mov    %eax,-0x1c(%ebp)
        if(s == 0)
 480:	85 f6                	test   %esi,%esi
 482:	75 28                	jne    4ac <printf+0x12a>
          s = "(null)";
 484:	be 4d 06 00 00       	mov    $0x64d,%esi
 489:	8b 7d 08             	mov    0x8(%ebp),%edi
 48c:	eb 0d                	jmp    49b <printf+0x119>
          putc(fd, *s);
 48e:	0f be d2             	movsbl %dl,%edx
 491:	89 f8                	mov    %edi,%eax
 493:	e8 50 fe ff ff       	call   2e8 <putc>
          s++;
 498:	83 c6 01             	add    $0x1,%esi
        while(*s != 0){
 49b:	0f b6 16             	movzbl (%esi),%edx
 49e:	84 d2                	test   %dl,%dl
 4a0:	75 ec                	jne    48e <printf+0x10c>
      state = 0;
 4a2:	be 00 00 00 00       	mov    $0x0,%esi
 4a7:	e9 02 ff ff ff       	jmp    3ae <printf+0x2c>
 4ac:	8b 7d 08             	mov    0x8(%ebp),%edi
 4af:	eb ea                	jmp    49b <printf+0x119>
        putc(fd, *ap);
 4b1:	8b 7d e4             	mov    -0x1c(%ebp),%edi
 4b4:	0f be 17             	movsbl (%edi),%edx
 4b7:	8b 45 08             	mov    0x8(%ebp),%eax
 4ba:	e8 29 fe ff ff       	call   2e8 <putc>
        ap++;
 4bf:	83 c7 04             	add    $0x4,%edi
 4c2:	89 7d e4             	mov    %edi,-0x1c(%ebp)
      state = 0;
 4c5:	be 00 00 00 00       	mov    $0x0,%esi
 4ca:	e9 df fe ff ff       	jmp    3ae <printf+0x2c>
        putc(fd, c);
 4cf:	89 fa                	mov    %edi,%edx
 4d1:	8b 45 08             	mov    0x8(%ebp),%eax
 4d4:	e8 0f fe ff ff       	call   2e8 <putc>
      state = 0;
 4d9:	be 00 00 00 00       	mov    $0x0,%esi
 4de:	e9 cb fe ff ff       	jmp    3ae <printf+0x2c>
    }
  }
}
 4e3:	8d 65 f4             	lea    -0xc(%ebp),%esp
 4e6:	5b                   	pop    %ebx
 4e7:	5e                   	pop    %esi
 4e8:	5f                   	pop    %edi
 4e9:	5d                   	pop    %ebp
 4ea:	c3                   	ret    

000004eb <free>:
static Header base;
static Header *freep;

void
free(void *ap)
{
 4eb:	55                   	push   %ebp
 4ec:	89 e5                	mov    %esp,%ebp
 4ee:	57                   	push   %edi
 4ef:	56                   	push   %esi
 4f0:	53                   	push   %ebx
 4f1:	8b 5d 08             	mov    0x8(%ebp),%ebx
  Header *bp, *p;

  bp = (Header*)ap - 1;
 4f4:	8d 4b f8             	lea    -0x8(%ebx),%ecx
  for(p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 4f7:	a1 ec 08 00 00       	mov    0x8ec,%eax
 4fc:	eb 02                	jmp    500 <free+0x15>
 4fe:	89 d0                	mov    %edx,%eax
 500:	39 c8                	cmp    %ecx,%eax
 502:	73 04                	jae    508 <free+0x1d>
 504:	39 08                	cmp    %ecx,(%eax)
 506:	77 12                	ja     51a <free+0x2f>
    if(p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 508:	8b 10                	mov    (%eax),%edx
 50a:	39 c2                	cmp    %eax,%edx
 50c:	77 f0                	ja     4fe <free+0x13>
 50e:	39 c8                	cmp    %ecx,%eax
 510:	72 08                	jb     51a <free+0x2f>
 512:	39 ca                	cmp    %ecx,%edx
 514:	77 04                	ja     51a <free+0x2f>
 516:	89 d0                	mov    %edx,%eax
 518:	eb e6                	jmp    500 <free+0x15>
      break;
  if(bp + bp->s.size == p->s.ptr){
 51a:	8b 73 fc             	mov    -0x4(%ebx),%esi
 51d:	8d 3c f1             	lea    (%ecx,%esi,8),%edi
 520:	8b 10                	mov    (%eax),%edx
 522:	39 d7                	cmp    %edx,%edi
 524:	74 19                	je     53f <free+0x54>
    bp->s.size += p->s.ptr->s.size;
    bp->s.ptr = p->s.ptr->s.ptr;
  } else
    bp->s.ptr = p->s.ptr;
 526:	89 53 f8             	mov    %edx,-0x8(%ebx)
  if(p + p->s.size == bp){
 529:	8b 50 04             	mov    0x4(%eax),%edx
 52c:	8d 34 d0             	lea    (%eax,%edx,8),%esi
 52f:	39 ce                	cmp    %ecx,%esi
 531:	74 1b                	je     54e <free+0x63>
    p->s.size += bp->s.size;
    p->s.ptr = bp->s.ptr;
  } else
    p->s.ptr = bp;
 533:	89 08                	mov    %ecx,(%eax)
  freep = p;
 535:	a3 ec 08 00 00       	mov    %eax,0x8ec
}
 53a:	5b                   	pop    %ebx
 53b:	5e                   	pop    %esi
 53c:	5f                   	pop    %edi
 53d:	5d                   	pop    %ebp
 53e:	c3                   	ret    
    bp->s.size += p->s.ptr->s.size;
 53f:	03 72 04             	add    0x4(%edx),%esi
 542:	89 73 fc             	mov    %esi,-0x4(%ebx)
    bp->s.ptr = p->s.ptr->s.ptr;
 545:	8b 10                	mov    (%eax),%edx
 547:	8b 12                	mov    (%edx),%edx
 549:	89 53 f8             	mov    %edx,-0x8(%ebx)
 54c:	eb db                	jmp    529 <free+0x3e>
    p->s.size += bp->s.size;
 54e:	03 53 fc             	add    -0x4(%ebx),%edx
 551:	89 50 04             	mov    %edx,0x4(%eax)
    p->s.ptr = bp->s.ptr;
 554:	8b 53 f8             	mov    -0x8(%ebx),%edx
 557:	89 10                	mov    %edx,(%eax)
 559:	eb da                	jmp    535 <free+0x4a>

0000055b <morecore>:

static Header*
morecore(uint nu)
{
 55b:	55                   	push   %ebp
 55c:	89 e5                	mov    %esp,%ebp
 55e:	53                   	push   %ebx
 55f:	83 ec 04             	sub    $0x4,%esp
 562:	89 c3                	mov    %eax,%ebx
  char *p;
  Header *hp;

  if(nu < 4096)
 564:	3d ff 0f 00 00       	cmp    $0xfff,%eax
 569:	77 05                	ja     570 <morecore+0x15>
    nu = 4096;
 56b:	bb 00 10 00 00       	mov    $0x1000,%ebx
  p = sbrk(nu * sizeof(Header));
 570:	8d 04 dd 00 00 00 00 	lea    0x0(,%ebx,8),%eax
 577:	83 ec 0c             	sub    $0xc,%esp
 57a:	50                   	push   %eax
 57b:	e8 20 fd ff ff       	call   2a0 <sbrk>
  if(p == (char*)-1)
 580:	83 c4 10             	add    $0x10,%esp
 583:	83 f8 ff             	cmp    $0xffffffff,%eax
 586:	74 1c                	je     5a4 <morecore+0x49>
    return 0;
  hp = (Header*)p;
  hp->s.size = nu;
 588:	89 58 04             	mov    %ebx,0x4(%eax)
  free((void*)(hp + 1));
 58b:	83 c0 08             	add    $0x8,%eax
 58e:	83 ec 0c             	sub    $0xc,%esp
 591:	50                   	push   %eax
 592:	e8 54 ff ff ff       	call   4eb <free>
  return freep;
 597:	a1 ec 08 00 00       	mov    0x8ec,%eax
 59c:	83 c4 10             	add    $0x10,%esp
}
 59f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
 5a2:	c9                   	leave  
 5a3:	c3                   	ret    
    return 0;
 5a4:	b8 00 00 00 00       	mov    $0x0,%eax
 5a9:	eb f4                	jmp    59f <morecore+0x44>

000005ab <malloc>:

void*
malloc(uint nbytes)
{
 5ab:	55                   	push   %ebp
 5ac:	89 e5                	mov    %esp,%ebp
 5ae:	53                   	push   %ebx
 5af:	83 ec 04             	sub    $0x4,%esp
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1)/sizeof(Header) + 1;
 5b2:	8b 45 08             	mov    0x8(%ebp),%eax
 5b5:	8d 58 07             	lea    0x7(%eax),%ebx
 5b8:	c1 eb 03             	shr    $0x3,%ebx
 5bb:	83 c3 01             	add    $0x1,%ebx
  if((prevp = freep) == 0){
 5be:	8b 0d ec 08 00 00    	mov    0x8ec,%ecx
 5c4:	85 c9                	test   %ecx,%ecx
 5c6:	74 04                	je     5cc <malloc+0x21>
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 5c8:	8b 01                	mov    (%ecx),%eax
 5ca:	eb 4d                	jmp    619 <malloc+0x6e>
    base.s.ptr = freep = prevp = &base;
 5cc:	c7 05 ec 08 00 00 f0 	movl   $0x8f0,0x8ec
 5d3:	08 00 00 
 5d6:	c7 05 f0 08 00 00 f0 	movl   $0x8f0,0x8f0
 5dd:	08 00 00 
    base.s.size = 0;
 5e0:	c7 05 f4 08 00 00 00 	movl   $0x0,0x8f4
 5e7:	00 00 00 
    base.s.ptr = freep = prevp = &base;
 5ea:	b9 f0 08 00 00       	mov    $0x8f0,%ecx
 5ef:	eb d7                	jmp    5c8 <malloc+0x1d>
    if(p->s.size >= nunits){
      if(p->s.size == nunits)
 5f1:	39 da                	cmp    %ebx,%edx
 5f3:	74 1a                	je     60f <malloc+0x64>
        prevp->s.ptr = p->s.ptr;
      else {
        p->s.size -= nunits;
 5f5:	29 da                	sub    %ebx,%edx
 5f7:	89 50 04             	mov    %edx,0x4(%eax)
        p += p->s.size;
 5fa:	8d 04 d0             	lea    (%eax,%edx,8),%eax
        p->s.size = nunits;
 5fd:	89 58 04             	mov    %ebx,0x4(%eax)
      }
      freep = prevp;
 600:	89 0d ec 08 00 00    	mov    %ecx,0x8ec
      return (void*)(p + 1);
 606:	83 c0 08             	add    $0x8,%eax
    }
    if(p == freep)
      if((p = morecore(nunits)) == 0)
        return 0;
  }
}
 609:	83 c4 04             	add    $0x4,%esp
 60c:	5b                   	pop    %ebx
 60d:	5d                   	pop    %ebp
 60e:	c3                   	ret    
        prevp->s.ptr = p->s.ptr;
 60f:	8b 10                	mov    (%eax),%edx
 611:	89 11                	mov    %edx,(%ecx)
 613:	eb eb                	jmp    600 <malloc+0x55>
  for(p = prevp->s.ptr; ; prevp = p, p = p->s.ptr){
 615:	89 c1                	mov    %eax,%ecx
 617:	8b 00                	mov    (%eax),%eax
    if(p->s.size >= nunits){
 619:	8b 50 04             	mov    0x4(%eax),%edx
 61c:	39 da                	cmp    %ebx,%edx
 61e:	73 d1                	jae    5f1 <malloc+0x46>
    if(p == freep)
 620:	39 05 ec 08 00 00    	cmp    %eax,0x8ec
 626:	75 ed                	jne    615 <malloc+0x6a>
      if((p = morecore(nunits)) == 0)
 628:	89 d8                	mov    %ebx,%eax
 62a:	e8 2c ff ff ff       	call   55b <morecore>
 62f:	85 c0                	test   %eax,%eax
 631:	75 e2                	jne    615 <malloc+0x6a>
        return 0;
 633:	b8 00 00 00 00       	mov    $0x0,%eax
 638:	eb cf                	jmp    609 <malloc+0x5e>
