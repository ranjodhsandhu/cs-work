//rss6py
#include "pool.h"
using namespace std;
Task::Task() {
}

Task::~Task() {
}

ThreadPool::ThreadPool(int num_threads) {
    shutdown = false;
    thnum = num_threads;
    tanum = 0;
    int e;
    e = pthread_mutex_init(&qlock, NULL);
    assert(e == 0);
    e = pthread_mutex_init(&mlock, NULL);
    assert(e == 0);
    e = pthread_cond_init(&ntfy, NULL);
    assert(e == 0);
    tarray = (pthread_t*)malloc(sizeof(pthread_t)*num_threads);
    int i = -1;
    while(++i < thnum){
        pthread_create(tarray+i, NULL, ThreadPool::w, this);
    }
}
void * ThreadPool::TLife(void *){
    qnode *n = NULL;
    map<string, pthread_cond_t *>::iterator it;
    for (;;) {
    pthread_mutex_lock(&qlock);
		 while(shutdown == false && tanum == 0)
		 {
		 	pthread_cond_wait(&ntfy, &qlock);
		 } 
		if (shutdown == true && tanum == 0)
		{
			pthread_mutex_unlock(&qlock);
			break;
		}
		assert(tanum > 0);
		n = pq.front();
		pq.pop_front();
		tanum--;
		assert(n != NULL);
		pthread_mutex_lock(&mlock);
		if (pmap.end() == pmap.find(n->name))
		{
			pmap.insert(pair<string, pthread_cond_t *>(n->name, NULL));
		}
		pthread_mutex_unlock(&mlock);
		pthread_mutex_unlock(&qlock);
		n->tsk->Run();
		pthread_mutex_lock(&mlock);
		it = pmap.find(n->name);
		if (it->second != NULL)
		{
			pthread_cond_signal(it->second);	
		}
		pmap.erase(n->name);	
		pthread_mutex_unlock(&mlock);
		delete n->tsk;
		delete n;
    if (!1) break;
}
return NULL;
}
void * ThreadPool::w(void *object){
    ThreadPool *temp = (ThreadPool *)object;
	temp->TLife(NULL);
	return NULL;
}

void ThreadPool::SubmitTask(const std::string &name, Task* task) {
    qnode *n = new qnode;
    n -> name = name;
    n -> tsk = task;
    pthread_mutex_lock(&qlock);
    pq.push_back(n);
    tanum += 1;
    pthread_cond_signal(&ntfy);
    pthread_mutex_unlock(&qlock);
}

void ThreadPool::WaitForTask(const std::string &name) {
    int f = 0;
    list<qnode *>::iterator it;
	map<string, pthread_cond_t *>::iterator mit;
    pthread_mutex_lock(&qlock);
    it = pq.begin();
    while(it != pq.end()){
        if((*it) -> name == name){
            f = 1;
            break;
        }
        it++;
    }
    if(f != 1){
        pthread_mutex_unlock(&qlock);}
    else{
        pthread_mutex_lock(&mlock);
		pthread_cond_t wtask = PTHREAD_COND_INITIALIZER;
		pmap.insert(pair<string, pthread_cond_t *>(name, &wtask));
		pthread_mutex_unlock(&qlock);
		pthread_cond_wait(&wtask, &mlock);
		pthread_mutex_unlock(&mlock);
		return;
	}
    pthread_mutex_lock(&mlock);
	if ((mit = pmap.find(name)) != pmap.end())
	{
		pthread_cond_t wtask = PTHREAD_COND_INITIALIZER;
		mit->second = &wtask;
		pthread_cond_wait(&wtask, &mlock);
	}
	pthread_mutex_unlock(&mlock);
}

void ThreadPool::Stop() {
    shutdown = true;
    pthread_cond_broadcast(&ntfy);
    pthread_mutex_unlock(&lock);
    int i = -1;
    while(++i < thnum){
        pthread_join(tarray[i], NULL);
    }
    assert(tanum == 0);
    free(tarray);
    pthread_mutex_destroy(&qlock);
    pthread_mutex_destroy(&mlock);
    pthread_cond_destroy(&ntfy);
}
