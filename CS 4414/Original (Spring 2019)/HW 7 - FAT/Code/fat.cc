#include <list>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <cerrno>
#include "fat_internal.h"
using namespace std;
struct fDesc
{
    bool space;
    uint32_t sz;
    uint32_t cl;
};
bool flag = false;
int ifd;
vector<DirEntry> cd;
int fds;
uint32_t *FAT;
Fat32BPB b;
vector<fDesc> f_d(128, fDesc{1, 0, 0});
list<int> free_fDesc;

vector<string> tp(const char *path)
{
    string s(path);
    vector<string> ts;
    size_t pos = 0;
    string t;
    string d = "/";
    if (s[0] == '/' && s.size() > 0)
    {
        ts.push_back("/");
    }
    while (string::npos != (pos = s.find(d)))
    {
        t = s.substr(0, pos);
        if (t != "")
        {
            ts.push_back(s.substr(0, pos));
        }
        s.erase(0, pos + d.length());
    }
    if (s != "")
    {
        ts.push_back(s);
    }
    return ts;
}
int read_sec(int sn, void *bf, int sz, int os)
{
    if (lseek(ifd, sn * b.BPB_BytsPerSec + os, 0) != -1)
    {
        return read(ifd, bf, sz);
    }
    return -1;
}
vector<DirEntry> read_dc(uint32_t ccl)
{
    vector<uint32_t> cls;
    bool f = ccl < 0x0FFFFFFF;
    while (f == true)
    {
        cls.push_back(ccl);
        ccl = 0x0FFFFFFF & FAT[ccl];
        if (ccl >= 0x0FFFFFFF)
        {
            f = false;
        }
    }
    int clb = b.BPB_SecPerClus * b.BPB_BytsPerSec;
    int dec = cls.size() * clb / sizeof(DirEntry);
    vector<DirEntry> del(dec);
    uint8_t *ptr = (uint8_t *)del.data();
    uint i = 0;
    while (i < cls.size())
    {
        if (read_sec((fds + (cls[i] - 2) * b.BPB_SecPerClus), ptr, clb, 0) == -1)
        {
            break;
        }
        ptr += clb;
        i++;
    }

    return del;
}
DirEntry *get_de(vector<DirEntry> &des, string s)
{
    uint i = 0;
    while (i < s.length())
    {
        s[i] = toupper(s[i]);
        i++;
    }
    string n;
    string e;
    i = 0;
    while (i < des.size())
    {
        if (des[i].DIR_Name[0] == 0xE5)
        {
            i++;
            continue;
        }
        if (des[i].DIR_Name[0] == 0x00)
        {
            break;
        }
        n.clear();
        e.clear();
        int a = 0;
        while (a < 11)
        {
            if (des[i].DIR_Name[a] == ' ')
            {
                a++;
                continue;
            }
            if (a >= 8)
            {
                e.push_back(des[i].DIR_Name[a]);
                a++;
            }
            else
            {
                n.push_back(des[i].DIR_Name[a]);
                a++;
            }
        }
        if (e.size() > 0)
        {
            n = n + "." + e;
        }
        if (s == n)
        {
            return &des[i];
        }
        i++;
    }
    return NULL;
}
vector<DirEntry> get_path_des(vector<string> path_ts)
{
    vector<DirEntry> des = cd;
    uint32_t cln;
    uint i = 0;
    while (i < path_ts.size())
    {
        if (path_ts[i] != "/")
        {
            DirEntry *ep = get_de(des, path_ts[i]);
            if (ep != NULL && ep->DIR_Attr == 0x10)
            {
                cln = ((*ep).DIR_FstClusHI << 8) | (*ep).DIR_FstClusLO;
            }
            else
            {
                if (path_ts[i] != "." && path_ts[i] != "..")
                {
                    des.clear();
                    break;
                }
                else
                {
                    cln = b.BPB_RootClus;
                }
            }
        }
        else
        {
            cln = b.BPB_RootClus;
        }
        if (cln == 0)
        {
            cln = b.BPB_RootClus;
        }
        des = read_dc(cln);
        i++;
    }
    return des;
}

bool fat_mount(const string &path)
{
    ifd = open(path.c_str(), O_RDWR);
    if ((open(path.c_str(), O_RDWR) == -1) || (read(ifd, &b, sizeof(Fat32BPB)) == -1))
    {
        return 0;
    }
    fds = b.BPB_RsvdSecCnt + (b.BPB_NumFATs * b.BPB_FATSz32);
    int cc = (b.BPB_TotSec32 - fds) / b.BPB_SecPerClus;
    int x = 65525;
    FAT = (uint32_t *)malloc(b.BPB_FATSz32 * b.BPB_BytsPerSec);
    if ((cc < x) || (read_sec(b.BPB_RsvdSecCnt, FAT, b.BPB_FATSz32 * b.BPB_BytsPerSec, 0) == -1))
    {
        return 0;
    }
    int i = 0;
    while (i < 128)
    {
        free_fDesc.push_back(i);
        i++;
    }
    flag = true;
    return 1;
}
int fat_open(const string &path)
{
    if (flag == true)
    {
        vector<string> path_ts = tp(path.c_str());
        if (path_ts.size() == 0)
        {
            return -1;
        }
        string n = path_ts[path_ts.size() - 1];
        path_ts.pop_back();
        vector<DirEntry> des = get_path_des(path_ts);
        if (des.size() == 0)
        {
            return -1;
        }
        DirEntry *ep = get_de(des, n);
        if (ep == NULL || ep->DIR_Attr == 0x10 || (free_fDesc.size() == 0))
        {
            return -1;
        }
        int fd = free_fDesc.front();
        uint32_t cln = ((*ep).DIR_FstClusHI << 8) | (*ep).DIR_FstClusLO;
        fDesc fd_ent{0, ep->DIR_FileSize, cln};
        f_d[fd] = fd_ent;
        free_fDesc.pop_front();
        return fd;
    }
    return -1;
}
bool fat_close(int fd)
{
    if (flag == true)
    {
        if (f_d[fd].space == 0)
        {
            f_d[fd].space = 1;
            free_fDesc.push_back(fd);
            return true;
        }
        return false;
    }
    return -1;
}
int fat_pread(int fd, void *buffer, int count, int offset)
{
    if (flag == true)
    {
        fDesc fd_ent = f_d[fd];
        if (fd_ent.space != 0)
        {
            return -1;
        }
        uint32_t cln = fd_ent.cl;
        int clb = b.BPB_SecPerClus * b.BPB_BytsPerSec;
        uint8_t *ptr = (uint8_t *)buffer;
        int lb = count;
        int lo = offset;
        int lf = fd_ent.sz;
        while (lo < lf && lb != 0)
        {
            if (lo <= clb)
            {
                int rs = clb;
                if (lo != 0)
                {
                    rs -= lo;
                    lf -= lo;
                }
                if (rs > lb)
                {
                    rs = lb;
                }
                if (rs > lf)
                {
                    rs = lf;
                }
                if (read_sec((fds + (cln - 2) * b.BPB_SecPerClus), ptr, rs, lo) == -1)
                {
                    return 0;
                }
                lb -= rs;
                ptr += rs;
                lo = 0;
                lf -= rs;
                cln = 0x0FFFFFFF & FAT[cln];
                ;
                if (cln >= 0x0FFFFFF8)
                {
                    break;
                }
            }
            else
            {
                lf -= clb;
                lo -= clb;
                cln = 0x0FFFFFFF & FAT[cln];
                if (cln >= 0x0FFFFFF8)
                {
                    break;
                }
            }
        }
        return count - lb;
    }
    return -1;
}
vector<AnyDirEntry> fat_readdir(const string &path)
{
    if (flag == true)
    {
        vector<string> ts = tp(path.c_str());
        vector<DirEntry> des = get_path_des(ts);
        vector<AnyDirEntry> r;
        if (des.size() != 0)
        {
            uint i = 0;
            while (i < des.size())
            {
                AnyDirEntry ade;
                ade.dir = des[i];
                r.push_back(ade);
                i++;
            }
        }
        return r;
    }
    vector<AnyDirEntry> e;
    return e;
}
