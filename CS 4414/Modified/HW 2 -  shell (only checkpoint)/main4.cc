#include <cstdlib>
#include <iostream>
#include <string>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sstream>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
using namespace std;
void parse_and_run_command(const std::string &command) {
    /* TODO: Implement this. */
    /* Note that this is not the correct way to test for the exit command.
       For example the command "   exit  " should also exit your shell.
     */

    /*unsigned int pos = 0;
    string com;
    bool foundSpace = false;
    int length = command.length();
    string command2 = command;
    while (command2[0] == ' ') {
	command2 = command2.substr(1, length);
    }
    while (!foundSpace && pos < command2.length()-1) {
	pos++;
	if (command2.at(pos) == ' ') {
	    foundSpace = true;
	    break;
	}
    }
    if (!foundSpace) { 
	com = command2;
    } else {
	com = command2.substr(0, pos);
    }*/
    string token;
    istringstream s(command);
    int pos = 0;
    int pos2 = 0;
    char *tokens[256];
    char *args[256];
    char input[256];
    input[0] = '\0';
    char output[256];
    output[0] = '\0';
    int num_pipe = 0;
    
    s >> token;
    tokens[pos] = (char*)malloc(token.size()+1);
    strcpy(tokens[pos], token.c_str());
    pos++;
    args[pos2] = (char*)malloc(token.size()+1);
    strcpy(args[pos2], token.c_str());
    pos2++;
    while (s >> token) {
	if(token == "<") {
	    s >> input;
	    continue;
	}
	if(token == ">") {
	    s >> output;
	    continue;
	}
	if(token == "|") {
	    s >> token;
    	    tokens[pos] = (char*)malloc(token.size()+1);
	    strcpy(tokens[pos], token.c_str());
	    args[pos2] = (char*)malloc(token.size()+1);
   	    strcpy(args[pos2], token.c_str());
    	    pos2++;
	    pos++;
	    num_pipe++;
	}
        args[pos2] = (char*)malloc(token.size()+1);
	strcpy(args[pos2], token.c_str());
	pos2++;
    }
    tokens[pos] = NULL;
    args[pos2] = NULL;
    if (pos <= 0) {
	cerr << "Not implemented.\n";
    }

    



    if (strcmp(tokens[0], "exit") == 0) {
	for(int i = 0; i < pos; i++) {
            free(tokens[i]);
        }
	for(int i = 0; i < pos2; i++) {
            free(args[i]);
        }       
	exit(0);
    }


    /*if(pos > 1) {
	pipe(command);
	pid_t pid = fork();
	if(pid == 0) {
	    dup2(command[1], 1);
	    close(command[0]);
	    close(command[1]);
	    execv(tokens[0], args);
	}
	pid_t pid2 = fork();
	if(pid2 == 0) {
	    dup2(command[0], 0);
	    close(command[1]);
	    close(command[0]);
	    execv(tokens[1], args);
	}
	close(command[0]);
	close(command[1]);
    }*/

    pid_t stored_pid = 0;
    for(int i = 0; i < pos; i++) {
	pid_t pid = fork();
	if (pid == 0) {
	    if(strlen(output) != 0) {
		int out_fd = open(output, O_WRONLY | O_CREAT | O_TRUNC, 0666);
		dup2(out_fd, 1);
		close(out_fd);
	    }
	    if(strlen(input) != 0) {
		int in_fd = open(input, O_RDONLY);
		dup2(in_fd, 0);
		close(in_fd);
	    }

	    execv(tokens[i], args);
	    //cerr << "Not a command" << endl;
	    exit(1);
	} else {
	    stored_pid = pid;
	}
    }


    for(int i = 0; i < pos; i++) {
	int status;
	waitpid(stored_pid, &status, 0);
	cout << tokens[0] << " exit status: " << WEXITSTATUS(status) << endl;
    }
    
    /*if (strcmp(tokens[0], "echo") == 0) {
	int position = 1;
	while(tokens[position] != NULL && tokens[position+1] != NULL) {
	    cout << tokens[position];
	    cout << " ";
	    position++;
	}
	cout << tokens[position] << endl;
	for(int i = 0; i < pos; i++) {
            free(tokens[i]);
        }    
	return;	
    }*/

    

    for(int i = 0; i < pos; i++) {
       free(tokens[i]);
    }  
    for(int i = 0; i < pos2; i++) {
        free(args[i]);
    }      
}

int main(void) {
    std::string command;
    std::cout << "> ";
    while (std::getline(std::cin, command)) {
        parse_and_run_command(command);
        std::cout << "> ";
    }
    return 0;
}
