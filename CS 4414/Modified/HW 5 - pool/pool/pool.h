//rss6py
#ifndef POOL_H_
#include <string>
#include <pthread.h>
#include <assert.h>
#include <list>
#include <map>
using namespace std;

class Task {
public:
    Task();
    virtual ~Task();

    virtual void Run() = 0;  // implemented by subclass
};

struct qnode{
    string name; 
    Task *tsk;
};

class ThreadPool {
public:
    ThreadPool(int num_threads);

    // Submit a task with a particular name.
    void SubmitTask(const std::string &name, Task *task);
 
    // Wait for a task by name, if it hasn't been waited for yet. Only returns after the task is completed.
    void WaitForTask(const std::string &name);

    // Stop all threads. All tasks must have been waited for before calling this.
    // You may assume that SubmitTask() is not caled after this is called.
    void Stop();
private:
    bool shutdown;
    int thnum;
    int tanum;
    pthread_mutex_t qlock;
    pthread_mutex_t mlock;
    pthread_cond_t ntfy;
    pthread_t *tarray;
    static void *w(void *);
    void *TLife(void *);
    bool NameSearch(const std::string &name);
    list<qnode *> pq;
    map<string, pthread_cond_t *> pmap;
};

#endif
