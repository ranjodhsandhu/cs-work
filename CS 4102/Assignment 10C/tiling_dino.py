#rss6py
def m_flow_helper(graph, y, c, vis, vert):
    ry = range(y)
    for temp_vert in ry:
        if graph[vert][temp_vert] == True and vis[temp_vert] == False:
            vis[temp_vert] = True
            if c[temp_vert] == -1 or m_flow_helper(graph, y, c, vis, vert) == True:
                c[temp_vert] = vert
                return True
    return False
def m_flow(graph, x, y):
    i = 0
    c = y * [-1]
    rx = range(x)
    for vert in rx:
        vis = y * [False]
        if m_flow_helper(graph, y, c, vis, vert) == True:
            i = i + 1
    return i, c
def compute(file_lines):
    imp = ["impossible"]
    if len(file_lines) < 1:
        return imp
    if len(file_lines[0]) < 1:
        return imp
    num_rows = len(file_lines)
    num_columns = len(file_lines[0])
    fst = []
    sec = []
    temp = True
    for row in range(num_rows):
        for column in range(num_columns):
            if temp == True and file_lines[row][column] == "#":
                fst.append((column, row))
            elif temp == False and file_lines[row][column] == "#":
                sec.append((column, row))
            temp = not temp
        if num_columns % 2 == 0:
            temp = not temp
    if len(sec) != len(fst):
        return imp

    bipartite_graph = []
    for column, row in fst:
        connected = []
        for column2, row2 in sec:
            if (column == column2 and row == row2 + 1) or (row == row2 and column == column2 + 1) or (column == column2 and row == row2 - 1) or (row == row2 and column == column2 - 1):
                connected.append(1)
            else:
                connected.append(0)
        bipartite_graph.append(connected)
    i, c = m_flow(bipartite_graph, len(bipartite_graph), len(bipartite_graph[0]))
    ans = []
    if i != len(fst):
        return imp
    else:
        temp = 0
        for x, y in sec:
            a, b = fst[c[temp]][0], fst[c[temp]][1]
            temporary = []
            temporary.append(str(x))
            temporary.append(str(y))
            temporary.append(str(a))
            temporary.append(str(b))
            temporary = ' '.join(temporary)
            ans.append(temporary)
            temp += 1
    return ans
