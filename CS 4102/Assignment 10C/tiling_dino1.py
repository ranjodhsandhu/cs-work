def max_flow(graph, x, y):
    current = [-1] * y
    i = 0
    for vertices in range(x):
        visited = [False] * y
        if max_flow_helper(graph, y, current, visited, vertices):
            i += 1
    return i, current


def max_flow_helper(graph, y, current, visited, vertices):
    for temp_vertex in range(y):
        if not visited[temp_vertex] and graph[vertices][temp_vertex]:
            visited[temp_vertex] = True
            if current[temp_vertex] == -1 or max_flow_helper(graph, y, current, visited, vertices):
                current[temp_vertex] = vertices
                return True
    return False


def compute(file_lines):
    if len(file_lines) < 1:
        return ["impossible"]
    if len(file_lines[0]) < 1:
        return ["impossible"]
    num_rows = len(file_lines)
    num_cols = len(file_lines[0])
    first = []
    second = []
    temp = True
    # assign the pixel to each individual set
    for row in range(num_rows):
        for col in range(num_cols):
            if file_lines[row][col] == "#" and temp:
                first.append((col, row))
            elif file_lines[row][col] == "#" and not temp:
                second.append((col, row))
            temp = not temp
        if num_cols % 2 == 0:
            temp = not temp
    # lengths of two sets must be the same to have dominoes of length 2
    if len(first) != len(second):
        return ["impossible"]

    bipartite_graph = []
    for col, row in first:
        connected = []
        for col2, row2 in second:
            if (row == row2 - 1 and col == col2) or (row == row2 + 1 and col == col2) or (col == col2 - 1 and row == row2) or (col == col2 + 1 and row == row2):
                connected.append(1)
            else:
                connected.append(0)
        bipartite_graph.append(connected)

    i, current = max_flow(bipartite_graph, len(bipartite_graph), len(bipartite_graph[0]))
    ans = []
    if len(first) != i:
        return["impossible"]
    else:
        temp = 0
        for x, y in second:
            a, b = first[current[temp]][0], first[current[temp]][1]
            # ans += str(x) + " " + str(y) + " " + str(a) + " " + str(b)
            temporary = []
            temporary.append(str(x))
            temporary.append(str(y))
            temporary.append(str(a))
            temporary.append(str(b))
            temporary = ' '.join(temporary)
            ans.append(temporary)
            temp += 1
    return ans
