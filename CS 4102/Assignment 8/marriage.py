# CS4102 Fall 2019 -- Homework 8
#################################
# Collaboration Policy: You are encouraged to collaborate with up to 4 other
# students, but all work submitted must be your own independently written
# solution. List the computing ids of all of your collaborators in the comment
# at the top of your java or python file. Do not seek published or online
# solutions for any assignments. If you use any published or online resources
# (which may not include solutions) when completing this assignment, be sure to
# cite them. Do not submit a solution that you are unable to explain orally to a
# member of the course staff.
#################################
# Your Computing ID: rss6py
# Collaborators: jhk4tt, mbr3fa
# Sources: Introduction to Algorithms, Cormen
#################################
from collections import defaultdict
class Marriage:
    lukePath = []
    lorelaiPath = []

    def __init__(self):
        return

    def getLukePath(self):
        return self.lukePath

    def getLorelaiPath(self):
        return self.lorelaiPath

    # This is the method that should set off the computation
    # of marriage.  It takes as input a list lines of input
    # as strings.  You should parse that input and then compute 
    # the shortest paths that both Luke and Lorelai should take.
    # The class fields of lukePath and lorelaiPath should be filled
    # with their respective paths.  The getters above will be called
    # by the grader script.
    #
    # @return the length of the shortest paths (in rooms)
    def compute(self, file_data):       
        graph = defaultdict(list)
        adjacent = defaultdict(list)
        temporary = []
        rooms = int(file_data[0])
        roomrange = range(rooms)
        queue = []
        route = []
        for a in roomrange:
            for b in file_data[a+3].split():
                adjacent[a].append(int(b))
            adjacent[a].append(a)
        for a in file_data[1].split():
            y = int(a)
            self.lukePath.append(y)
        for a in file_data[2].split():
            z = int(a)
            self.lorelaiPath.append(z)
        for a in roomrange:
            for b in roomrange:
                if a not in adjacent[b] and a != b:
                    temporary.append((a, b))
        for a in temporary:
            new_graph = []
            for b in adjacent[a[0]]:
                for c in adjacent[a[1]]:
                    if (b, c) in temporary:
                        new_graph.append((b, c))
            graph[a] = new_graph
        beginning = (self.lukePath[0], self.lorelaiPath[0])
        end = (self.lukePath[1], self.lorelaiPath[1])
        queue.append([beginning])
        while len(queue) != 0:
            route = queue.pop(0)
            if route[-1] == end:
                break
            for room in graph.get(route[-1], []):
                temporary = list(route)
                temporary.append(room)
                queue.append(temporary)
        del self.lukePath[:]
        del self.lorelaiPath[:]
        for pair in route:
            self.lorelaiPath.append(pair[1])
            self.lukePath.append(pair[0])
        solution = min(len(self.lukePath), len(self.lorelaiPath))
        return solution
