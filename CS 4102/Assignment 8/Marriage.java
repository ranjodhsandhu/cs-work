/**
 * CS4102 Fall 2019 -- Homework 8
 *********************************
 * Collaboration Policy: You are encouraged to collaborate with up to 4 other
 * students, but all work submitted must be your own independently written
 * solution. List the computing ids of all of your collaborators in the comment
 * at the top of your java or python file. Do not seek published or online
 * solutions for any assignments. If you use any published or online resources
 * (which may not include solutions) when completing this assignment, be sure to
 * cite them. Do not submit a solution that you are unable to explain orally to a
 * member of the course staff.
 *********************************
 * Your Computing ID: rss6py 
 * Collaborators: mbr3fa, jhk4tt
 * Sources: Introduction to Algorithms, Cormen
 **************************************/
import java.util.List;
import java.util.ArrayList;

public class Marriage {

    private List<Integer> lukePath = new ArrayList<>();
    private List<Integer> lorelaiPath = new ArrayList<>();

    public List<Integer> getLukePath() {
        return lukePath;
    }

    public List<Integer> getLorelaiPath() {
        return lorelaiPath;
    }

    /**
     * This is the method that should set off the computation
     * of marriage.  It takes as input a list lines of input
     * as strings.  You should parse that input and then compute 
     * the shortest paths that both Luke and Lorelai should take.
     * The class fields of lukePath and lorelaiPath should be filled
     * with their respective paths.  The getters above will be called
     * by the grader script.
     *
     * @return the length of the shortest paths (in rooms)
     */
    public int compute(List<String> fileData) {
        return 0;
    }
}
