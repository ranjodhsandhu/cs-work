import sys
import time
from marriage import Marriage 

fp = open("test.txt", 'r')
lines = fp.readlines()

# Call the closest_pair function passing in the
# contents of the file
start = time.time()
m = Marriage()
print(m.compute(lines))
print(m.getLukePath())
print(m.getLorelaiPath())
end = time.time()
print("time: "+ str(end-start))
