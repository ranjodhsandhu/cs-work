# CS4102 Fall 2019 -- Homework 10A 
#################################
# Collaboration Policy: You are encouraged to collaborate with up to 4 other
# students, but all work submitted must be your own independently written
# solution. List the computing ids of all of your collaborators in the comment
# at the top of your java or python file. Do not seek published or online
# solutions for any assignments. If you use any published or online resources
# (which may not include solutions) when completing this assignment, be sure
# to cite them. Do not submit a solution that you are unable to explain orally
# to a member of the course staff. Please remember that you are not allowed to
# share any written notes or documents (these include but are not limited to
# Overleaf documents, LaTeX source code, homework PDFs, group discussion
# notes, etc.). Any solutions that share similar text/code will be considered
# in breach of this policy. Please refer to the syllabus for a complete
# description of the collaboration policy.
#################################
# Your Computing ID: 
# Collaborators: 
# Sources: Introduction to Algorithms, Cormen
#################################

class Millionaire:
    length = 100000
    dp = [0 for i in range(length)]
    v = [0 for i in range(length)] 
    def __init__(self):
        return

    # This method should take in a list of the prize values (non-negative
    # integers) for each door and return the maximum amount of prize money you
    # can win by selecting a subset of doors, subject to the restriction that
    # you cannot select any adjacent pair of doors.
    #
    # @return the maximum amount of prize money
    def maxSum(self, arr, i, n): 
    # Base case 
        if (i >= n): 
            return 0
    
        # To check if a state has 
        # been solved 
        if (self.v[i]): 
            return self.dp[i] 
        self.v[i] = 1
    
        # Required recurrence relation 
        self.dp[i] = max(self.maxSum(arr, i + 1, n), 
                arr[i] + self.maxSum(arr, i + 2, n)) 
  
    # Returning the value 
        return self.dp[i] 
    
    def compute(self, doors):
        n = len(doors)
        return self.maxSum(doors, 0, n)

        

"""         maxIncluding = 0 #max including previous element
        maxExcluding = 0 #max excluding previous element

        for door in doors:
            if maxExcluding > maxIncluding:
                tmp = maxExcluding
            else:
                tmp = maxIncluding 
            maxIncluding = maxExcluding + door
            maxExcluding = tmp
        if maxExcluding > maxIncluding:
            return maxExcluding
        else:
            return maxIncluding """