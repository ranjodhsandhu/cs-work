# CS4102 Fall 2019 -- Homework 5
#################################
# Collaboration Policy: You are encouraged to collaborate with up to 4 other
# students, but all work submitted must be your own independently written
# solution. List the computing ids of all of your collaborators in the comment
# at the top of your java or python file. Do not seek published or online
# solutions for any assignments. If you use any published or online resources
# (which may not include solutions) when completing this assignment, be sure to
# cite them. Do not submit a solution that you are unable to explain orally to a
# member of the course staff.
#################################
# Your Computing ID: rss6py
# Collaborators: bc5fg, jhk4tt, mbr3fa
# Sources: Introduction to Algorithms, Cormen
#################################
import math
class SeamCarving:
    def __init__(self):
        return

    # This method is the one you should implement.  It will be called to perform
    # the seam carving.  You may create any additional data structures as fields
    # in this class or write any additional methods you need.
    # 
    # @return the seam's weight
    def run(self, image):
        global list_of_seams
        list_of_seams = []
        temp_list = [[[0 for rgb in range(3)] for column in range(len(image[0]))] for row in range(len(image))]
        for row in range(len(image)):
            for column in range(len(image[row])):
                for rgb in range(len(image[row][column])):
                    temp_list[row][column][rgb] = image[row][column][rgb]
        temp_row_range = range(len(temp_list[row]))
        temp_list_range = range(len(temp_list))
        list_of_seams = [[0 for column in temp_row_range] for row in temp_list_range]
        temp_list_0_len = len(temp_list[0])
        temp_list_len = len(temp_list)
        temp_list_row_len = len(temp_list[row])
        if temp_list_0_len == 1:
            list_of_seams[0] = 0
            return 0.0
        elif temp_list_len == 1:
            for row in range(temp_list_0_len - 1, 1):
                list_of_seams[row] = self.pixel_difference(temp_list[0][row], temp_list[0][row - 1])
            return 0.0
        min_list = [[0 for column in range(len(temp_list[row]))] for row in range(len(temp_list))]
        e_map = self.getEnergyMap(temp_list)

        for row in range(temp_list_len -1, -1 ,-1):
            for column in range(temp_list_row_len):
                e_map_rc = e_map[row][column]
                if row == temp_list_len - 1:
                    min_list[row][column]  = e_map_rc
                elif column == 0:
                    min_list[row][column] = e_map_rc + min(min_list[row + 1][column], min_list[row + 1][column + 1])
                elif column == len(temp_list[row]) - 1:
                    min_list[row][column] = e_map_rc + min(min_list[row + 1][column - 1], min_list[row + 1][column])
                else:
                    min_list[row][column] = e_map_rc + min(min(min_list[row + 1][column], min_list[row + 1][column + 1]), min(min_list[row + 1][column - 1], min_list[row + 1][column]))

        t = 0
        w = float('inf')
        for column in range(temp_list_0_len):
            if min_list[0][column] < w:
                w = min_list[0][column]
                t = column
        list_of_seams[0] = t
        for row in range(1, temp_list_len):
            if t == 0:
                if min_list[row][t] < min_list[row][t+1]:
                    list_of_seams[row] = t
                else:
                    list_of_seams[row] = t+1
                    t = t+1
            elif t == temp_list_0_len - 1:
                if min_list[row][t-1] > min_list[row][t]:
                    list_of_seams[row] = t
                else:
                    list_of_seams[row] = t-1
                    t = t-1
            else:
                min_temp = min_list[row][t]
                t_temp = t
                for column in range(t-1, t+2):
                    if min_list[row][column] < min_temp:
                        min_temp = min_list[row][column]
                        t_temp = column
                    list_of_seams[row] = t_temp
                    t = t_temp


        return w

    # Get the seam, in order from top to bottom, where the top-left corner of the
    # image is denoted (0,0).
    # 
    # Since the y-coordinate (row) is determined by the order, only return the x-coordinate
    # 
    # @return the ordered list of x-coordinates (column number) of each pixel in the seam
    #         as an array
    def getSeam(self):
        return list_of_seams

    def pixel_difference(self, p1, p2):
        return math.sqrt( ((p2[0] - p1[0]) * (p2[0] - p1[0])) + ((p2[1] - p1[1]) * (p2[1] - p1[1])) + ((p2[2] - p1[2]) * (p2[2] - p1[2]) ))

    def getEnergyMap(self, image):
        image_range = range(len(image))
        e_map = [[0 for column in range(len(image[row]))] for row in image_range]    
        for row in image_range:
            for column in range(len(image[row])):
                image_len = len(image)
                image0_len = len(image[0])
                five5 = image[row][column]
                if (column == 0) and (row == 0):
                    six6 = image[row][column + 1]
                    eight8 = image[row + 1][column]
                    nine9 = image[row + 1][column + 1]
                elif (column == 0) and (row == image_len - 1):
                    two2 = image[row - 1][column]
                    three3 = image[row - 1][column + 1]
                    six6 = image[row][column + 1]
                elif (column == image0_len - 1) and (row == 0):
                    four4 = image[row][column - 1]
                    seven7 = image[row + 1][column - 1]
                    eight8 = image[row + 1][column]
                elif (column == image0_len - 1) and (row == image_len - 1):
                    one1 = image[row - 1][column - 1]
                    two2 = image[row - 1][column]
                    four4 = image[row][column - 1]
                elif row == 0:
                    four4 = image[row][column - 1]
                    six6 = image[row][column + 1]
                    seven7 = image[row + 1][column - 1]
                    eight8 = image[row + 1][column]
                    nine9 = image[row + 1][column + 1]
                elif column == 0:
                    two2 = image[row - 1][column]
                    three3 = image[row - 1][column + 1]
                    six6 = image[row][column + 1]
                    eight8 = image[row + 1][column]
                    nine9 = image[row + 1][column + 1]
                elif row == image_len - 1:
                    one1 = image[row - 1][column - 1]
                    two2 = image[row - 1][column]
                    three3 = image[row - 1][column + 1]
                    four4 = image[row][column - 1]
                    six6 = image[row][column + 1]
                elif column == image0_len - 1:
                    one1 = image[row - 1][column - 1]
                    two2 = image[row - 1][column]
                    four4 = image[row][column - 1]
                    seven7 = image[row + 1][column - 1]
                    eight8 = image[row + 1][column]
                else:
                    one1 = image[row - 1][column - 1]
                    two2 = image[row - 1][column]
                    three3 = image[row - 1][column + 1]
                    four4 = image[row][column - 1]
                    six6 = image[row][column + 1]
                    seven7 = image[row + 1][column - 1]
                    eight8 = image[row + 1][column]
                    nine9 = image[row + 1][column + 1]
                image_row_len = len(image[row])
                if (row != 0) and (column == 0):
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, nine9) + self.pixel_difference(five5, six6) + self.pixel_difference(five5, three3) + self.pixel_difference(five5, two2)) / 5
                elif (row != 0) and (column == image_row_len-1):
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, seven7) + self.pixel_difference(five5, four4) + self.pixel_difference(five5, one1) + self.pixel_difference(five5, two2)) / 5
                elif (column != 0) and (row == 0):
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, four4) + self.pixel_difference(five5, six6) + self.pixel_difference(five5, seven7) + self.pixel_difference(five5, nine9)) / 5
                elif row == image_len - 1:
                    e_map[row][column] = (self.pixel_difference(five5, four4) + self.pixel_difference(five5, one1) + self.pixel_difference(five5, two2) + self.pixel_difference(five5, three3) + self.pixel_difference(five5, six6)) / 5
                elif (column == 0) and (row == 0):
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, nine9) + self.pixel_difference(five5, six6)) / 3
                elif (column == image_row_len - 1) and (row == 0):
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, four4) + self.pixel_difference(five5, seven7)) / 3
                elif (column == image_row_len - 1) and (row == image_len - 1):
                    e_map[row][column] = (self.pixel_difference(five5, two2) + self.pixel_difference(five5, one1) + self.pixel_difference(five5, four4)) / 3
                elif (column == 0) and (row == image_len - 1):
                    e_map[row][column] = (self.pixel_difference(five5, two2) + self.pixel_difference(five5, three3) + self.pixel_difference(five5, six6)) / 3
                else:
                    e_map[row][column] = (self.pixel_difference(five5, eight8) + self.pixel_difference(five5, nine9) + self.pixel_difference(five5, six6) + self.pixel_difference(five5, three3) + self.pixel_difference(five5, two2) + self.pixel_difference(five5, one1) + self.pixel_difference(five5, four4) + self.pixel_difference(five5, seven7)) / 8
        return e_map