# CS4102 Fall 2019 -- Homework 2
#################################
# Collaboration Policy: You are encouraged to collaborate with up to 4 other
# students, but all work submitted must be your own independently written
# solution. List the computing ids of all of your collaborators in the comment
# at the top of your java or python file. Do not seek published or online
# solutions for any assignments. If you use any published or online resources
# (which may not include solutions) when completing this assignment, be sure to
# cite them. Do not submit a solution that you are unable to explain orally to a
# member of the course staff.
#################################
# Your Computing ID: rss6py
# Collaborators: jhk4tt, mbr3fa, bc5fg
# Sources: Introduction to Algorithms, Cormen
#################################
import math

class ClosestPair:
    def __init__(self):
        return

    def distanceFormula(self, x, y):
        a = (y[0]-x[0])
        b = (y[1]-x[1])
        return math.sqrt(a**2 + b**2)

    def closestDistance(self, x, y):
        xSize = len(x)
        if xSize <= 3:
            mi = self.distanceFormula(x[0], x[1])
            for i in range(0, xSize-1):
                for j in range (i+1, xSize-1):
                    if j != 0 & i != 0:
                        if self.distanceFormula(x[i], x[j]) < mi:
                            mi = self.distanceFormula(x[i], x[j])
            return mi
# Split plane in half by x coordinates and Split list of Y coordinates in half based off side of plane
        mid = xSize // 2
        leftX = x[:mid]
        rightX = x[mid:]
        med = x[mid][0]
        leftY = []
        rightY = []
        for p in y:
            if p[0] > med:
                rightY.append(p)
            else:
                leftY.append(p)
#recursive call to find closest point in two halves and check which distance is closer
        leftD = self.closestDistance(leftX, leftY)
        rightD = self.closestDistance(rightX, rightY)
        if leftD > rightD:
            dist = rightD
        else:
            dist = leftD

        midp = x[xSize//2][0]
#sorted list of points in runway based off Y value
        yLst = [ p for p in y if midp - dist <= p[0] <= midp + dist]
        clst = dist
        yLstSize = len(yLst)

        for i in range(0, yLstSize-1):
            temp = self.distanceFormula(yLst[i], yLst[i+1])
            if temp < clst:
                clst = temp
#compare runway distance to distance found in plane and return the smaller value
        if dist > clst:
            return clst
        else:
            return dist

    # This is the method that should set off the computation
    # of closest pair.  It takes as input a list lines of input
    # as strings.  You should parse that input and then call a
    # subroutine that you write to compute the closest pair distance
    # and return that value from this method
    #
    # @return the distance between the closest pair 
    def compute(self, file_data):
        ct = 0
        lst = []
        i = 0
#Add all the data points into one list
        for datac in file_data:
            ct = ct+1
            datac = datac.strip('\n').split()
            if ct > 1:
                lst.append(datac)
                lst[i][0] = float(lst[i][0])
                lst[i][1] = float(lst[i][1])
                i = i+1
#Sort the x and y values
        xSorted = sorted(lst, key=lambda lst:lst[0])
        ySorted = sorted(lst, key=lambda lst:lst[1])

#call closestDistance on lists
        return self.closestDistance(xSorted, ySorted)