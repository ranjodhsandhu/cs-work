/**
 * CS4102 Fall 2019 -- Homework 10B
 *********************************
 * Collaboration Policy: You are encouraged to collaborate with up to 4 other
 * students, but all work submitted must be your own independently written
 * solution. List the computing ids of all of your collaborators in the comment
 * at the top of your java or python file. Do not seek published or online
 * solutions for any assignments. If you use any published or online resources
 * (which may not include solutions) when completing this assignment, be sure to
 * cite them. Do not submit a solution that you are unable to explain orally to a
 * member of the course staff.
 *********************************
 * Your Computing ID: rss6py
 * Collaborators: mbr3fa, jhk4tt
 * Sources: Introduction to Algorithms, Cormen
 **************************************/
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList; 
import java.util.Queue; 

public class Coffee {

    /**
     * This is the method that should set off the computation.
     *
     * @return true if consistent, false otherwise
     */
    public boolean compute(List<String> fileData) {
        int ub = Integer.parseInt(fileData.get(0)+1);
        Map<String, String> adjacent = new HashMap<String, String>(ub);
        ArrayList<Integer> q = new ArrayList<Integer>(ub);
        ArrayList<Integer> a = new ArrayList<Integer>(ub);
        ArrayList<Boolean> v = new ArrayList<Boolean>(ub);
        for(int i = 0; i < ub; i++){
            v.add(false);
            a.add(0);
        }
        for(int ind = 1; ind < ub; i++){
            String line = fileData.get(ind).split(" ");
            int c1 = Character.getNumericValue(line.charAt(0));
            char c2 = line.charAt(1);
            char d = line.charAt(2);
            adjacent<String.valueOf(c1), c2> = d;
        }
        if(Integer.parseInt(fileData.get(0)) < 1){
            return true;
        }
        else{
            q.add(1);
            v.set(1, true);
            while(q){
                
            }
        }
        return true;
    }
}
