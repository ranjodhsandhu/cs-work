class Coffee:
    def __init__(self):
        return

    # This is the method that should set off the computation.
    #
    # @return true if consistent, false otherwise 
    def compute(self, file_data):
        adj = [dict() for i in range(int(file_data[0])+1)]
        arr = [0 for i in range(int(file_data[0])+1)]  # 0 for coffee1 and 1 for coffee 2
        q = []
        visited = [False for i in range(int(file_data[0])+1)]

        for index in range(1, len(file_data), 1):
            line_data = file_data[index].split()
            coffee1 = int(line_data[0])
            coffee2 = line_data[1]
            decision = line_data[2]
            adj[coffee1][coffee2] = decision

        if int(file_data[0]) < 1:
            return True
        else:
            q.append(1)
            visited[1] = True
            while q:
                cur = q.pop(0)
                if not adj[cur] and len(q)==0 :
                    for d in adj:
                        find = False
                        for key in d:
                            # print(adj)
                            if arr[int(key)] == 0:
                                cur = int(key)
                                find = True
                                break
                        if find:
                            break
                for edge in adj[cur]:
                    if not edge:
                        continue
                    # print(adj[cur][edge])
                    if adj[cur][edge] == "same":
                        if arr[cur] != 0 and arr[int(edge)] != 0:  # if both are assigned
                            if arr[cur] != arr[int(edge)]:
                                return False
                        elif arr[cur] != 0:
                            arr[int(edge)] = arr[cur]
                        elif arr[int(edge)] != 0:
                            arr[cur] = arr[int(edge)]
                        else:  # if both are unassigned
                            arr[cur] = 1
                            arr[int(edge)] = 1
                    else:  # if they are supposed to be different
                        if cur == int(edge):
                            return False
                        if arr[cur] != 0 and arr[int(edge)] != 0:  # if both are assigned
                            if arr[cur] == arr[int(edge)]:
                                return False
                        elif arr[cur] != 0:
                            arr[int(edge)] = arr[cur] * -1
                        elif arr[int(edge)] != 0:
                            arr[cur] = arr[int(edge)] * -1
                        else:  # if both are unassigned
                            arr[cur] = -1
                            arr[int(edge)] = 1
                    if not visited[int(edge)]:
                        q.append(int(edge))
                        visited[int(edge)] = True

        '''for edge in adj:
                    if not edge:
                        continue
                    for key in edge:'''

        return True



