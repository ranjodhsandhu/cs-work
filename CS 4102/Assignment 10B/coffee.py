# CS4102 Fall 2019 -- Homework 10B
#################################
# Collaboration Policy: You are encouraged to collaborate with up to 4 other
# students, but all work submitted must be your own independently written
# solution. List the computing ids of all of your collaborators in the comment
# at the top of your java or python file. Do not seek published or online
# solutions for any assignments. If you use any published or online resources
# (which may not include solutions) when completing this assignment, be sure to
# cite them. Do not submit a solution that you are unable to explain orally to a
# member of the course staff.
#################################
# Your Computing ID: rss6py
# Collaborators: mbr3fa, jhk4tt
# Sources: Introduction to Algorithms, Cormen
#################################

class Coffee:
    def __init__(self):
        return

    # This is the method that should set off the computation.
    #
    # @return true if consistent, false otherwise 
    def compute(self, file_data):
        array = [] 
        queue = []
        adjacent = []
        visited = []
        initial = int(file_data[0])
        for i in range(initial+1):
            visited.append(False)
            array.append(0)
            adjacent.append(dict())
        for i in range(1, len(file_data)):
            line = file_data[i].split()
            c1 = int(line[0])
            c2 = line[1]
            d = line[2]
            adjacent[c1][c2] = d
        if initial >= 1:
            visited[1] = True
            queue.append(1)
            while queue:
                currentVal = queue.pop(0)
                if len(queue)==0 and adjacent[currentVal] == False :
                    for dec in adjacent:
                        found = False
                        for key in d:
                            if array[int(key)] == 0:
                                currentVal = int(key)
                                found = True
                                break
                        if found == True:
                            break
                for edgeVal in adjacent[currentVal]:
                    if edgeVal == False:
                        continue
                    acv = array[currentVal]
                    ae = array[int(edgeVal)]
                    if adjacent[currentVal][edgeVal] == "same":
                        if ae != 0 and acv != 0: 
                            if acv != ae:
                                return False
                        elif acv != 0:
                            array[int(edgeVal)] = acv
                        elif ae != 0:
                            array[currentVal] = ae
                        else:  
                            array[int(edgeVal)] = 1                           
                            array[currentVal] = 1
                    else: 
                        if currentVal == int(edgeVal):
                            return False
                        if ae != 0 and acv != 0:  
                            if acv == ae:
                                return False
                        elif acv != 0:
                            array[int(edgeVal)] = acv * -1
                        elif ae != 0:
                            array[currentVal] = ae * -1
                        else:  
                            array[int(edgeVal)] = 1
                            array[currentVal] = -1
                    if visited[int(edgeVal)] == False:
                        visited[int(edgeVal)] = True
                        queue.append(int(edgeVal))

        return True
