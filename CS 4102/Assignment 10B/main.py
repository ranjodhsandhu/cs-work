import sys
import time
from coffee import Coffee 

fp = open("pairings.txt", 'r')
lines = fp.readlines()


# Call the compute function passing in the
# contents of the file
start = time.time()
c = Coffee()
if c.compute(lines):
    print("Consistent")
else:
    print("Inconsistent")
end = time.time()
print("time: "+ str(end-start))
