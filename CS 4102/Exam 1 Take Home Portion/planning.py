"""
Name:   Ranjodh Sandhu
Userid: rss6py

Pledge: I confirm that I followed the collaboration policy.
"""
from __future__ import print_function # make python2 print like python3

def findMissingRSVP(invites, rsvps):
    """
    Complete this function so that it
    returns the missing guest in the list
    of rsvps.

    Input is a list of invites and a list of rsvps.

    You must use the compare() function below
    to check the alphabetical order of two guests.
    """
    mindex = len(invites) // 2
    median = invites[mindex]
    lefti = invites[:mindex]
    righti = invites[mindex+1:]

    leftr = []
    rightr = []
    medianA = []
    for i in range(len(rsvps)):
        comparison = compare(rsvps[i], median)
        if(comparison == -1):
            leftr.append(rsvps[i])
        if(comparison == 1):
            rightr.append(rsvps[i])
        if(comparison == 0):
            medianA.append(rsvps[i])
    if(len(medianA) == 0):
        return median
    if(len(lefti) > len(leftr)):
        return findMissingRSVP(lefti, leftr)
    if(len(righti) > len(rightr)):
        return findMissingRSVP(righti, rightr)
    
        




        
    


#####################################
# Under penalty of the Honor Code   #
# Do Not Change Anything Below Here #
# (In your final submission)        #
#####################################


def compare(a, b):
    """
    Helper function. 
    Will return a negative integer if a comes before b alphabetically.
    Will return a positive integer if a comes after  b alphabetically.
    Will return 0 if a equals b alphabetically.
    """
    global compare_calls_counter
    compare_calls_counter += 1

    # trivial version for Strings
    if a < b:
        return -1
    if a > b:
        return 1
    return 0


#########################################
# Under penalty of the Honor Code       #
# Do not use anything initialized below #
# (In your final submission)            #
#########################################


compare_calls_counter = 0
lists_file = open('lists.txt', 'r')
invites = []
rsvps = []
first = True
for line in lists_file.readlines():
    if len(line.strip()) == 0:
        first = False
        continue
    if first:
        invites.append(line.strip())
    else:
        rsvps.append(line.strip())

# Call the findMissingRSVP function, print the result and the number of times you called compare
print (findMissingRSVP(invites, rsvps),compare_calls_counter)
