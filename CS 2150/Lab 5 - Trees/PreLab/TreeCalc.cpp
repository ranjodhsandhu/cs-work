// Ranjodh Sandhu
//rss6py
//10/11/18
// TreeCalc.cpp:  CS 2150 Tree Calculator method implementations

#include "TreeCalc.h"
#include <iostream>
#include <cstdlib>
using namespace std;

//Constructor
TreeCalc::TreeCalc() {
  stack1 = stack<TreeNode*>();
}

//Destructor- frees memory
TreeCalc::~TreeCalc() {
TreeNode *top = stack1.top();
 cleanTree(top);
}

//Deletes tree/frees memory
void TreeCalc::cleanTree(TreeNode* ptr) {
  if(ptr){
    cleanTree(ptr -> left);
    cleanTree(ptr -> right);
    delete ptr;}
}

//Gets data from user
void TreeCalc::readInput() {
    string response;
    cout << "Enter elements one by one in postfix notation" << endl
         << "Any non-numeric or non-operator character,"
         << " e.g. #, will terminate input" << endl;
    cout << "Enter first element: ";
    cin >> response;
    //while input is legal
    while (isdigit(response[0]) || response[0]=='/' || response[0]=='*'
            || response[0]=='-' || response[0]=='+' ) {
        insert(response);
        cout << "Enter next element: ";
        cin >> response;
    }
}

//Puts value in tree stack
void TreeCalc::insert(const string& val) {
  TreeNode* inserted = new TreeNode(val);
  if (isdigit(val[0]) || isdigit(val[1]))
      stack1.push(inserted);
  else{
      TreeNode *right = stack1.top();
      stack1.pop();
      TreeNode *left = stack1.top();
      stack1.pop();
      inserted -> left = left;
      inserted -> right = right;
      stack1.push(inserted);}
}

//Prints data in prefix form
void TreeCalc::printPrefix(TreeNode* ptr) const {
  if (ptr == NULL)
    return;
  cout << ptr -> value << " ";
  if (ptr -> left)
    printPrefix(ptr -> left);
  if (ptr -> right)
    printPrefix(ptr -> right);
}

//Prints data in infix form
void TreeCalc::printInfix(TreeNode* ptr) const {
    // print tree in infix format with appropriate parentheses
  if (ptr -> left != NULL){
    if (ptr -> value == "+" || ptr -> value == "-" || ptr -> value == "*" || ptr -> value == "/"){
      cout << "(";
      printInfix(ptr -> left);
      cout << " ";
      cout << ptr -> value;
      cout << " ";
      printInfix(ptr -> right);
      cout << ")";}
   else
	cout << ptr -> value;}
      else
	cout << ptr -> value;
  }
		  

//Prints data in postfix form
void TreeCalc::printPostfix(TreeNode* ptr) const {
    // print the tree in postfix form
  if (ptr == NULL)
    return;
  if (ptr -> left)
    printPostfix(ptr -> left);
  if (ptr -> right)
    printPostfix(ptr -> right);
  cout << ptr -> value << " ";
  
}

// Prints tree in all 3 (pre,in,post) forms

void TreeCalc::printOutput() const {
  TreeNode *top = stack1.top();
  if (stack1.size()!=0 && stack1.top()!=NULL) {
        cout << "Expression tree in postfix expression: ";
        printPostfix(top);// call your implementation of printPostfix()
        cout << endl;
        cout << "Expression tree in infix expression: ";
        printInfix(top); // call your implementation of printInfix()
        cout << endl;
        cout << "Expression tree in prefix expression: ";
        printPrefix(top);// call your implementation of printPrefix()
        cout << endl;
    } else
        cout<< "Size is 0." << endl;
}

//Evaluates tree, returns value
// private calculate() method
int TreeCalc::calculate(TreeNode* ptr) const {
    // Traverse the tree and calculates the result
  string valuestr = ptr -> value;
  if (valuestr[0] == '+')
    return (calculate(ptr -> left) + (calculate(ptr -> right)));
  else if (valuestr[0] == '-' && valuestr.length() == 1)
    return (calculate(ptr -> left) - (calculate(ptr -> right)));
  else if (valuestr[0] == '*')
    return (calculate(ptr -> left) * (calculate(ptr -> right)));
  else if (valuestr[0] == '/')
    return (calculate(ptr -> left) / (calculate(ptr -> right)));
  else return atoi(valuestr.c_str());
  
  
}

//Calls calculate, sets the stack back to a blank stack
// public calculate() method. Hides private data from user
int TreeCalc::calculate() {
    int i = 0;
    i = calculate(stack1.top());// call private calculate method here
    return i;
}
