//Ranjodh Sandhu
//rss6py
//12/2/18
//topological.cpp
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <vector>
#include <queue>
using namespace std;

class graphNode{
public:
  graphNode(string x);
  vector<graphNode*> adList; //!< A vector of the graphNodes adjacent to the graphNode. */
  void addAdjNode(graphNode* node);
  string nodeName; //!< Name of the node. */
  int iD; //!< Indegree of the node. */
};
/** @brief Constructs a graph node object.
 *
 * Creates a node and sets the string as the name of the node.
 * @param x The name of the node to be created
 */
graphNode::graphNode(string x){
  iD = 0;
  nodeName = x;}
/** @brief Adds a graphNode to a given node's vector of adjacent nodes.
 *
 * @param node The node to be added to the node's vector of adjacent nodes.
 */
void graphNode::addAdjNode(graphNode* node){
  adList.push_back(node);}
/** @brief Topologically sorts a passed in vector.
 *
 * Topologically sorts a passed in vector and prints results.
 * @param lst The vector of graphNodes to be sorted.
 */
void topologicalSort(vector<graphNode*> lst){
  queue<graphNode*> gNodeQueue; //!< A queue of graphNodes. */
  graphNode *tempNode1; //!< A temporary graphNode. */
  for(int j = 0; j < lst.size(); j++){
    tempNode1 = lst[j];
    if(tempNode1 -> iD == 0)
      gNodeQueue.push(tempNode1);}
  graphNode *tempNode2; //!< A secondary temporary graphNode. */
  while(!(gNodeQueue.empty())){
    tempNode1 = gNodeQueue.front();
    gNodeQueue.pop();
    cout << tempNode1->nodeName << " ";
    for(int i = 0; i < tempNode1 -> adList.size(); i++){
      tempNode2 = tempNode1 -> adList[i];
      tempNode2 -> iD--;
      if(tempNode2 -> iD == 0)
	gNodeQueue.push(tempNode2);}}
  cout << endl;}
vector<graphNode*> lst; /**< Vector of graphNodes. */
/** @brief Sets the index and check value
 *
 * Through the size of the vector passed in, if the name of the node at the index is the same
 * as the string passed in, the index integer is changed to the value of the index of the node in the
 * vector. This also triggers the check to 0, which is used right after the function.
 * @param l The vector of graphNodes.
 * @param s The string in the file.
 * @param in The index value to be changed.
 * @param check The check value used for future "if" statement.
 */

void setIC(vector<graphNode*> l, string s, int& in, int& check){
  for(int j = 0; j < l.size(); j++){
      if(l[j] -> nodeName == s){
	in = j;
	check = 0;}}}
/** @brief Inserts new node into the vector.
 *
 * @return The modified vector of graphNodes
 * @param l The vector of graphNodes.
 * @param s The string in the file.
 * @param in The index value to be changed.
 */
vector<graphNode*> insertNew(vector<graphNode*> l, string s, int& in){
  graphNode* tempN = new graphNode(s);//!< New node to be added to vector. */
  lst.push_back(tempN);
  in = lst.size()-1;
  return l;}

  int main (int argc, char **argv) {
    if ( argc != 2 ) {
        cout << "Must supply the input file name as the one and only parameter" << endl;
        exit(1);
    }
    ifstream file(argv[1], ifstream::binary);
    if ( !file.is_open() ) {
        cout << "Unable to open file '" << argv[1] << "'." << endl;
        exit(2);
    }
    while(true){
      string s1, s2; //! string s1, s2: Strings from the file. */
    file >> s1;
    file >> s2;
    int ck1 = 1; //! int ck1: Case checker used for if statement. */
    int ck2 = 1; //! int ck2: Case checker used for if statement. */
    if(s1 == "0" && s2 == "0"){
      break;}
    int a = 0; //! int a: index holder. */
    int b = 0; //! int b: index holder. */
    setIC(lst, s1, a, ck1);
    if(ck1 == 1)
      insertNew(lst, s1, a);
    setIC(lst, s2, b, ck2);
    if(ck2 == 1)
      insertNew(lst, s2, b);
    lst[b] -> iD++;
    lst[a] -> addAdjNode(lst[b]);}
    topologicalSort(lst);
    file.close();
}
