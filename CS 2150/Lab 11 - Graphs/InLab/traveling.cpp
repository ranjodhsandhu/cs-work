#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <limits>
#include "middleearth.h"
using namespace std;

/** @brief Calculates the distance of the route through the cities that are in the passed in vector.
 *
 * @return The distance of the route.
 * @param me The middleearth object to be used.
 * @param start The name of the starting city.
 * @param dests The vector containing all the cities to be used.
 */
float computeDistance (MiddleEarth &me, string start, vector<string> dests);

/** @brief Prints the route through the cities.
 *
 * @param start The name of the starting city.
 * @param dests The vector containing all the cities to be used.
 */
void printRoute (string start, vector<string> dests);

int main (int argc, char **argv) {
    if ( argc != 6 ) {
        cout << "Usage: " << argv[0] << " <world_height> <world_width> "
             << "<num_cities> <random_seed> <cities_to_visit>" << endl;
        exit(0);}
    int width, height, num_cities, rand_seed, cities_to_visit;
    sscanf (argv[1], "%d", &width);
    sscanf (argv[2], "%d", &height);
    sscanf (argv[3], "%d", &num_cities);
    sscanf (argv[4], "%d", &rand_seed);
    sscanf (argv[5], "%d", &cities_to_visit);
    MiddleEarth me(width, height, num_cities, rand_seed);
    vector<string> dests = me.getItinerary(cities_to_visit);
    int size = dests.size();
    float minVal = std::numeric_limits<float>::max(); //max value of float
    vector<string> itinerary(size);
    float current;
    sort(dests.begin()+1, dests.end());
    if(size <= 2){
        minVal= computeDistance(me, dests[0], dests);
        itinerary = dests;}
    else{
        while (next_permutation(dests.begin()+1, dests.end()) == true){
            current = computeDistance(me, dests[0], dests);
            if (current < minVal){
	      minVal = current;
	      itinerary = dests;}}}
    printRoute(dests[0], itinerary);
    cout << endl << "Distance is " << minVal << endl;
    return 0;
}

// This method will compute the full distance of the cycle that starts
// at the 'start' parameter, goes to each of the cities in the dests
// vector IN ORDER, and ends back at the 'start' parameter.
float computeDistance (MiddleEarth &me, string start, vector<string> dests) {
  int s = dests.size(); //! s: Size of the destinations vector.
  string temp1 = start; //! temp1: Temporary string to hold the start string.
  float dist = 0.0; //! dist: Final float distance.
  int i = 0; 
  while(i < s){
    dist = dist + me.getDistance(temp1, dests[i]);
    temp1 = dests[i];
    i++;}
  dist = dist + me.getDistance(dests[0], dests[s-1]);
  return dist;}

// This method will print the entire route, starting and ending at the
// 'start' parameter.  The output should be of the form:
// Erebor -> Khazad-dum -> Michel Delving -> Bree -> Cirith Ungol -> Erebor
void printRoute (string start, vector<string> dests){
  vector<string> temp = dests; //! temp: Temporary vector to print and remove from.
  while(!(temp.empty())){
    cout << temp[0] << " -> ";
    temp.erase(temp.begin());}
  cout << start << endl;}
