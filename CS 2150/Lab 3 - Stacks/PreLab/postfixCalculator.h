/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: postfixCalculator.h
*/

#ifndef POSTFIXCALCULATOR_H
#define POSTFIXCALCULATOR_H

#include <iostream>
#include <stack>
#include <cstdlib>
using namespace std;

class PostfixCalculator{
 public:
  PostfixCalculator(); //Constructor
  int addition();
  int subtraction();
  int multiplication();
  int division();
  int unaryNegation();
  int getTopValue();
  int pushNum(int a);
private:
  stack <int> s;
};
 #endif

