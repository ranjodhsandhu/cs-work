/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: testPostfixCalc.cpp
*/

#include <iostream>
#include <stack>
#include <cstdlib>
#include "postfixCalculator.h"
using namespace std;
int main() {
    PostfixCalculator p;
    p.pushNum (6);
    p.pushNum (9);
    p.pushNum (7);
    p.pushNum (4);
    p.pushNum (2);
    p.division();
    p.multiplication();
    p.subtraction();
    p.unaryNegation();
    p.addition();
    cout << "Result is: " << p.getTopValue() << endl;
    return 0;
}
