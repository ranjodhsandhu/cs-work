/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: postfixCalculator.cpp
*/

#include "postfixCalculator.h"
#include <stack>
#include <iostream>
#include <cstdlib>
using namespace std;

//-----default constructor
PostfixCalculator::PostfixCalculator(){
  stack <int> s;}
int PostfixCalculator::addition(){
  if(s.empty())
    exit(-1);
  int a = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  if(s.empty())
    exit(-1);
  int b = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  int c = a + b;
  s.push(c);
  return 0;}
int PostfixCalculator::subtraction(){
if(s.empty())
    exit(-1);
  int a = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  if(s.empty())
    exit(-1);
  int b = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  int c = b - a;
  s.push(c);
  return 0;}
int PostfixCalculator::division(){
  if(s.empty())
    exit(-1);
  int a = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  if(s.empty())
    exit(-1);
  int b = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  int c = b / a;
  s.push(c);
  return 0;}
int PostfixCalculator::multiplication(){
  if(s.empty())
    exit(-1);
  int a = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  if(s.empty())
    exit(-1);
  int b = s.top();
  if(s.empty())
    exit(-1);
  s.pop();
  if(s.empty())
    exit(-1);
  int c = a * b;
  s.push(c);
  return 0;}
int PostfixCalculator::unaryNegation(){
  if(s.empty())
    exit(-1);
  int a = s.top();
  int b = 0 - a;
  if(s.empty())
    exit(-1);
  s.pop();
  s.push(b);
  return 0;
  }
int PostfixCalculator::getTopValue(){
  if(s.empty())
    exit(-1);
  return s.top();}
int PostfixCalculator::pushNum(int a){
  s.push(a);
  return 0;}
