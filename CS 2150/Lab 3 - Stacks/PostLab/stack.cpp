/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/19/18
Filename: stack.cpp
*/
#include "stack.h"
#include <iostream>
#include <cstdlib>
using namespace std;

//-----default constructor
stack::stack(){
  list = new List;}
void stack::push(int x){
  list->insertAtTail(x);}
void stack::pop(){
  list->remove(list->last().retrieve());}
int stack::top() const{
  return list->last().retrieve();}
bool stack::empty() const{
  return list->isEmpty();}
    
