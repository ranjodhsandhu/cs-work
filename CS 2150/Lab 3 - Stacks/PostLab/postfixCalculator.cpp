/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: postfixCalculator.cpp
*/

#include "postfixCalculator.h"
#include <iostream>
using namespace std;

//-----default constructor
PostfixCalculator::PostfixCalculator(){
  s = new stack;}
void PostfixCalculator::addition(){
  if(s->empty())
    exit(-1);
  int a = s->top();
  s->pop();
  if(s->empty())
    exit(-1);
  int b = s->top();
  s->pop();
  int c = a + b;
  s->push(c);
  }
void PostfixCalculator::subtraction(){
if(s->empty())
    exit(-1);
  int a = s->top();
  if(s->empty())
    exit(-1);
  s->pop();
  if(s->empty())
    exit(-1);
  int b = s->top();
  s->pop();
  int c = b - a;
  s->push(c);
  }
void PostfixCalculator::division(){
  if(s->empty())
    exit(-1);
  int a = s->top();
  if(s->empty())
    exit(-1);
  s->pop();
  if(s->empty())
    exit(-1);
  int b = s->top();
  s->pop();
  int c = b / a;
  s->push(c);
  }
void PostfixCalculator::multiplication(){
  if(s->empty())
    exit(-1);
  int a = s->top();
  if(s->empty())
    exit(-1);
  s->pop();
  if(s->empty())
    exit(-1);
  int b = s->top();
  s->pop();
  int c = a * b;
  s->push(c);
  }
void PostfixCalculator::unaryNegation(){
  if(s->empty())
    exit(-1);
  int a = s->top();
  int b = 0 - a;
  if(s->empty())
    exit(-1);
  s->pop();
  s->push(b);
  }
int PostfixCalculator::getTopValue(){
  if(s->empty())
    exit(-1);
  return s->top();}
void PostfixCalculator::pushNum(int a){
  s->push(a);}
