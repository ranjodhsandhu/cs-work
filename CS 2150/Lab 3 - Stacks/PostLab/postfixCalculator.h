/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: postfixCalculator.h
*/

#ifndef POSTFIXCALCULATOR_H
#define POSTFIXCALCULATOR_H
#include "stack.h"


class PostfixCalculator{
 public:
  PostfixCalculator(); //Constructor
  void addition();
  void subtraction();
  void multiplication();
  void division();
  void unaryNegation();
  int getTopValue();
  void pushNum(int a);
private:
  stack *s; //stack
};
 #endif

