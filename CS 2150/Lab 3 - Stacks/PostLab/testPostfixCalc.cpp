/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/17/18
Filename: testPostfixCalc.cpp
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include "postfixCalculator.h"

int main(){
    PostfixCalculator p;
    string s;
    while (cin >> s) {
      if(s == "+") // -------- addition
	p.addition();
      else if(s == "/") // ------- division
	p.division();
      else if(s == "*") // ------- multiplication
	p.multiplication();
      else if(s == "-") // ---------- subtraction
	p.subtraction();
      else if (s == "~") //--------- unary negation
	p.unaryNegation();
      else //------- handling ints
	    p.pushNum(atoi(s.c_str()));
	  }
      cout << "Result is: " << p.getTopValue() << endl;
	   return 0;
    }
	  
