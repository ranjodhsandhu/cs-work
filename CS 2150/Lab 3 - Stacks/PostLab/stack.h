/*
Name: Ranjodh Sandhu
Computing ID: rss6py@virginia.edu
Date: 9/19/18
Filename: stack.h
*/

#ifndef STACK_H
#define STACK_H
#include "List.h"
#include "ListItr.h"
#include "ListNode.h"
#include <iostream>
using namespace std;

class stack{
 public:
  stack(); //Constructor
  void push(int x);
  void pop();
  int top() const;
  bool empty() const;
 private:
  List* list; //list to hold values
};
#endif
