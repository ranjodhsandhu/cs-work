//Name: Ranjodh Sandhu
//Computing ID: rss6py@virginia.edu
//Date: 9/26/18
//Filename: bitCounter.cpp
#include <iostream>
#include <cstdlib>
#include <math.h>
using namespace std;

int bits(int n){
  if (n == 0)
    return 0;
  else  if (n == 1)
    return 1;
  else  if (n%2 == 0)
    return bits(n/2);
  else
    return bits(floor(n/2)) + 1;}
int main(int argc, char **argv){
  if (argc != 0){
    int argument = atoi(argv[1]);
    int answer = bits(argument);
    cout << "The Number of 1s: " << answer << endl;}
  else{
    cout<< "Error: No Command-Line Parameters" << endl;
    exit(-1);}
  return 0;}
    
