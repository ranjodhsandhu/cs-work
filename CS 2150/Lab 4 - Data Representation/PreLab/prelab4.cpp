//Name: Ranjodh Sandhu
//Computing ID: RSS6PY@VIRGINIA.EDU
//Date: 9/24/18
//Filename: prelab4.cpp
#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

void sizeOfTest(){
    cout << "Size of int: " << sizeof(int) << endl;
    cout << "Size of unsigned int: " << sizeof(unsigned int) << endl;
    cout << "Size of float: " << sizeof(float) << endl;
    cout << "Size of double: " << sizeof(double) << endl;
    cout << "Size of char: " << sizeof(char) << endl;
    cout << "Size of bool: " << sizeof(bool) << endl;
    cout << "Size of int*: " << sizeof(int*) << endl;
    cout << "Size of char*: " << sizeof(char*) << endl;
    cout << "Size of double*: " << sizeof(double*) << endl;
}
    
void outputBinary(unsigned int x){
  string solution = "0";
  if (x == 0){
    solution += "0000000000000000000000000000000";
    cout <<"Binary Form: "<< solution << endl;
    return;}
  for(int y = 31; y > -1; y--){
    if(x & (1 << y))
      solution += "1";
    else
      solution += "0";}
    cout << "You Entered: " << x << endl;
    cout << "Binary Form: " << solution << endl;
  
}
void overflow(){
  unsigned int x = 4294967295;
  cout << "Maximum Unsigned Integer: " <<  x  << endl;
  cout << "Maximum Unsigned Integer + 1: " <<  x + 1  << endl;
  cout << "This happens because the maximum value for a 32-bit integer in binary has a 1 in every place. When this number is increased by 1, a carry of 1 propogates through which turns all the ones to 0." << endl;
}
  
int main(){
  string input;
  cout << "Enter Int: " << endl;
  cin >> input;
  sizeOfTest();
  outputBinary(atoi(input.c_str()));
  overflow();
  return 0;
 }
