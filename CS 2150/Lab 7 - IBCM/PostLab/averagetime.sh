#!/bin/bash
#Ranjodh Sandhu
#rss6py
#11/1/18
#averagetime.sh

echo "Enter the exponent for counter.cpp: "
read exp
itr=1
sum=0
if [ "$exp" = "quit" ];
then exit 1
else
    while [ $itr -le 5 ]
    do
	echo "Running Iteration" $itr"..."
	temp=`./a.out $exp | tail -1`
	echo "Time Taken: " $temp "ms"
	sum=`expr $sum + $temp`
	((itr++))
    done
    echo "5 Iterations took" $sum "ms" 
    echo "Average Runtime was" `expr $sum / 5` "ms"
fi;

