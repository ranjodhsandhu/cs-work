; Ranjodh Sandhu
; rss6py
; 11/4/18
; mathlib.s
; Parameter 1 (in rdi) is the first int
; Parameter 2 (in rsi) is the second int
	global product
	global power
	section .text
product:
	; Standard prologue: we do not have to create any local
	; variables (those values will be kept in registers), and 
	; we are not using any callee-saved registers.

	; Subroutine body:
	xor	rax, rax	 ; zero out the return register
	xor	r10, r10	 ; zero out the counter i
start:	
	cmp	r10, rsi	 ; does i == 2nd int?
	je	done		 ; if so, we are done with the loop
	add	rax, rdi 	 ; add first int to rax
	inc	r10		 ; increment our counter i
	jmp	start		 ; we are done with this loop iteration
done:	

	; Standard epilogue: the return value is already in rax, we
	; do not have any callee-saved registers to restore, and we do not
	; have any local variables to deallocate, so all we do is return
	xor	r10, r10
	ret
power:
	xor	rax, rax	; zero out the return register
	xor	r8, r8		; set counter = 0
	mov	rbx, rsi	; move 2nd int into new register
	mov	rsi, rdi	; move 1st int into old register for 2nd int
	push	rbp		; push an additional register that is the total
	xor	rbp, rbp	; zero out the register
powerstart:
	cmp	rbx, 1		; compare 2nd int to 1
	jnbe	powerfunc	; if it doesn't equal 1, go to recursion
	mov	rbp, rdi	; else total is equal to x
	jmp	end		; go to end
powerfunc:
	dec	rbx		; decrease 2nd int by 1
	call	product		; call the product
	mov	rdi, rax	; save product where 1st int was
	mov	rbp, rax	; save the total equal to 1st int
	jmp	powerstart	; jump to start of power
end:
	mov	rax, rbp	; moves total to rax in order to return
	pop	rbp		; pop register to get the memory back
	ret			; return
