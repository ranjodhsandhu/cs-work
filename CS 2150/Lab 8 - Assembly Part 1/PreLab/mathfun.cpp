//Ranjodh Sandhu
//rss6py
//11/4/18
//mathfun.cpp

#include <iostream>
#include <cstdlib>
using namespace std;

extern "C" int power(int, int);
extern "C" int product(int, int);

int main(){
  int x, y;
  cout << "Please enter the first integer: "<< endl;
  cin >> x;
  cout << "Please enter the second integer: "<< endl;
  cin >> y;
  cout << "The product of "<< x << " and " << y << " is " << product(x,y) << endl;
  cout << x << " to the power of " << y << " is " << power(x, y) << endl;
  return 0;}
