//Ranjodh Sandhu
//rss6py
//11/19/18
//threexinput.cpp

#include <iostream>
#include <cstdlib>
#include "timer.h"

using namespace std;

extern "C" long threexplusone(long);

int main(){
  long input;
  long count;
  double total;
  double average;
  cout << "Enter input value: " << endl;
  cin >> input;
  cout << "Enter number of times to call subroutine: " << endl;
  cin >> count;
  timer time;
  time.start();
  for(int i = 0; i < count; i++)
    threexplusone(input);
  time.stop();
  total = time.getTime() * 1000;
  average = total/count;

  cout << "Total Steps for " << input << " with " << count << " calls: " << threexplusone(input) << endl;
  cout << "Average Time: " << average << " milliseconds" <<  endl;
  return 0;
}
