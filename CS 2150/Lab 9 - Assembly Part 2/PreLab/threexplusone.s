;Ranjodh Sandhu
;rss6py
;11/19/18

global threexplusone
section .text
	
	;Optimizations:
	;"test" function checks if the lowest bit is clear which means it is zero -> this implies even.
	;if the lowest bit is set, implies odd
	;Using test helped me determine if the number was even or odd much easier
	;Used lea to reduce number of mov lines
	;In the lea line, i did three additions in one instead of using the multiplication function
	;I kept my even and odd subroutines distinct for clarity
	;I also used shr instead of idiv in order to divide which was helpful

threexplusone:
	xor rax, rax 	;zero out rax's register
	push rdx	;push rdx value
	mov rdx, 1	;move 1 into rdx for comparison
	cmp rdi, rdx	;compare input to 1
	je end		;if input == 1 go to end
	pop rdx		;else pop rdx value
	push rcx	;push rcx value
	mov rcx, 1	;move 1 into rcx
	test rdi, rcx	;uses AND operation to see if the result is zero
	pop rcx		;pop rcx value
	jz even		;if result of test is 0, must be even so jump to even
	jmp odd		;else jump to odd
odd:
	lea rdi, [rdi+rdi+rdi]	;do mathematical function 3*number
	inc rdi			;add 1 to complete 3*number + 1
	jmp loop		;jump to the recursion
even:
	shr rdi, 1		;divide number by 2
	jmp loop		;jump to recursion
loop:
	push rdi		;push rdi value
	call threexplusone	;recurse
	inc rax			;increase count
	jmp end			;jump to end
end:
	pop rdx			;pop rdx value
	ret			;return
