//Name: Ranjodh Sandhu
//Computing ID: RSS6PY@VIRGINIA.EDU
//Date: 9/9/18
//Filename: ListItr.cpp
#include "ListItr.h"
#include <string>
#include <iostream>
using namespace std;

ListItr::ListItr(){
  current = NULL;
}
ListItr::ListItr(ListNode* theNode){
  current = theNode;
};
bool ListItr::isPastEnd() const{
  if (current -> next == NULL)
      return true;
  else
    return false;
}
bool ListItr::isPastBeginning() const{
  if (current -> previous == NULL)
      return true;
  else
    return false;
}
void ListItr::moveForward(){
  if (isPastEnd() == false)
      current = current -> next;
}
void ListItr::moveBackward(){
  if (isPastBeginning() == false)
      current = current -> previous;
}
int ListItr::retrieve() const{
  return current -> value;
}
