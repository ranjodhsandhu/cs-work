//Name: Ranjodh Sandhu
//Computing ID: RSS6PY@VIRGINIA.EDU
//Date: 9/9/18
//Filename: List.cpp
#include "List.h"
#include <string>
#include <iostream>
using namespace std;

//----- default constructor
List::List() {
  head=new ListNode();
  tail=new ListNode();
  head->next=tail;
  head->previous=NULL;
  tail->previous=head;
  tail->next=NULL;
  count=0;
}
//------ deconstructor
List::~List(){
  makeEmpty();
  delete head;
  delete tail;
}
//---the rest
List& List::operator=(const List& source) { //Equals operator
    if (this == &source)
        return *this;
    else {
        makeEmpty();
        ListItr iter(source.head->next);
        while (!iter.isPastEnd()) {
            insertAtTail(iter.retrieve());
            iter.moveForward();
        }
    }
    return *this;
}
List::List(const List& source) {      // Copy Constructor
    head=new ListNode;
    tail=new ListNode;
    head->next=tail;
    tail->previous=head;
    count=0;
    ListItr iter(source.head->next);
    while (!iter.isPastEnd()) {       // deep copy of the list
        insertAtTail(iter.retrieve());
        iter.moveForward();
    }
}
bool List::isEmpty() const{
  if (count == 0)
    return true;
  else
    return false;}
void List::makeEmpty(){
  while (isEmpty() != true){
    remove(first().retrieve());}
  count = 0;
}
void List::insertAfter(int x, ListItr position){
  ListNode *inserted = new ListNode();
  inserted -> value = x;
  inserted -> previous = position.current;
  inserted -> next = position.current -> next;
  position.current -> next = inserted;
  inserted -> previous -> next = inserted;
  count++;
}
void List::insertBefore(int x, ListItr position){
  ListNode *inserted = new ListNode();
  inserted -> value = x;
  inserted -> previous = position.current -> previous;
  inserted -> next = position.current;
  position.current -> next = inserted;
  inserted -> previous -> next = inserted;
  count++;
}
void List::insertAtTail(int x){
  ListNode *inserted = new ListNode();
  inserted -> value = x;
  inserted -> next = tail;
  inserted -> previous = tail -> previous;
  tail -> previous -> next = inserted;
  count++;
}
void List::remove(int x){
}
ListItr List::find(int x){
  return NULL;
}
void printList(List& source, bool direction){
}
ListItr List::first(){
  ListItr *yeah = new ListItr(head -> next);
  if (yeah -> isPastEnd() == true){
    yeah -> moveBackward();
    return *yeah;}
  else
    return *yeah;
}
ListItr List::last(){
  ListItr *yeah = new ListItr(head -> next);
  if (yeah -> isPastBeginning() == true){
    yeah -> moveForward();
    return *yeah;}
  else
    return *yeah;
}
int List::size() const{
  return 0;
}

