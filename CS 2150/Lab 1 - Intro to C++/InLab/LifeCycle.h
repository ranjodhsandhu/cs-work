//Name: Ranjodh Sandhu
//Computing ID: RSS6PY@VIRGINIA.EDU
//Date: 9/4/18
//Filename: LifeCycle.h
#ifndef LIFECYCLE_H
#define LIFECYCLE_H
#include <string>
#include <iostream>
using namespace std;

class MyObject {
public:
    static int numObjs;
    MyObject(const char *n = "--default--");      // default constructor
    MyObject(const MyObject& rhs);                // copy constructor
    ~MyObject();                                  // destructor
    string getName() const {
        return name;
    }
    void setName(const string newName) {
        name = newName;
    }
    friend ostream& operator<<(ostream& output, const MyObject& obj);
private:
    string name;
    int id;
};
// ------- Protoypes
MyObject getMaxMyObj(const MyObject o1, const MyObject o2);
int cmpMyObj(const MyObject o1, const MyObject o2);
void swapMyObj(MyObject& o1, MyObject& o2);

#endif
