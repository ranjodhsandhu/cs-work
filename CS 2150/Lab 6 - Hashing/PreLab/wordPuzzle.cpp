//Ranjodh Sandhu
//rss6py
//10/22/18
//wordPuzzle.cpp
//Calculated Big Theta Runtime: n^2 (4 nested for loops but 2 constant)
//Found all words in 1.30261 seconds. (300x300 grid)

#include "hashTable.h"
#include "timer.h"
#include "getWordInGrid.h"
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <sstream>
#include <fstream>

using namespace std;

timer t;

int main(int argc, char* argv[]){
  while(1){
    if(argc < 3){
      cout << "Error! Incomplete Input: Missing Argument(s). Try Again." << endl;
      exit(1);}
    string dictionaryFile = argv[1];
    int numLines = 0;
    string tempString;
    ifstream scan(dictionaryFile.c_str());
    while(getline(scan,tempString)){
      numLines++;}
    scan.close();
    string currentString;
    hashTable *myTable = new hashTable(numLines);
    scan.open(dictionaryFile.c_str());
    while(scan.good()){
      getline(scan, currentString);
      if(currentString.length() > 2)
	myTable -> insert(currentString);}
  scan.close();
  string gridFile = argv[2];
  int rows;
  int cols;
  scan.open(gridFile.c_str());
  readInGrid(gridFile.c_str(), rows, cols);
  string direction;
  int found = 0;
  t.start();
  for(int rowInt = 0; rowInt < rows; rowInt++){
    for(int colInt = 0; colInt < cols; colInt++){
      for(int dirInt = 0; dirInt < 8; dirInt++){
	for(int lenInt = 3; lenInt <= myTable -> maxNumOfWords; lenInt++){
	  string word = getWordInGrid(rowInt, colInt, dirInt, lenInt, rows, cols);
	  if(word.length() < lenInt)
	    break;
	  else if(myTable -> isIn(word)){
	    found++;
	    switch(dirInt){
	    case 0:
	      direction = "N";
	      break;
	    case 1:
	      direction = "NE";
	    case 2:
              direction = "E";
              break;
            case 3:
              direction = "SE";
              break;
            case 4:
              direction = "S";
              break;
            case 5:
              direction = "SW";
              break;
            case 6:
              direction = "W";
              break;
            case 7:
              direction = "NW";
              break;}
	    cout << direction << " (" << rowInt << ", " << colInt << "): " << word << endl;
	  }}}}}
  t.stop();
  double timeTaken = t.getTime();
  cout << found << " words found." << endl;
  cout << "Found all words in " << timeTaken << " seconds." << endl;
  break;}
  return 0;}
      
