//Ranjodh Sandhu
//rss6py
//10/21/18
//hashTable.cpp

#include "hashTable.h"
#include "primenumber.h"

hashTable::hashTable(int size){
  bucks = new vector< list<string> >;
  maxNumOfWords = 0;
  size ++;
  unsigned int temp = size;
  if(checkprime(temp) == false){
    tableCapacity = getNextPrime(temp);
    bucks -> resize(tableCapacity);}
  for(int i = 0; i < tableCapacity; i++){
    list<string> tempholder;
    bucks -> push_back(tempholder);}}

hashTable::~hashTable(){
  delete bucks;}

bool hashTable::isIn(const string & str) const{
  list<string> & newList = bucks -> at(hashFunc(str));
  return(find(newList.begin(), newList.end(), str) != (newList.end()));}

void hashTable::insert(const string & str){
  if(this->isIn(str) == false){
    list<string> & newList = bucks -> at(hashFunc(str));
    newList.push_back(str);
    int lengthofstr = str.length();
    while(lengthofstr > maxNumOfWords){
      maxNumOfWords = lengthofstr;}
  }}
int hashTable::hashFunc(const string & str) const{
  int val = 1;
  int lengthofstr = str.length();
  for(int i = 0; i < lengthofstr; i++){
    val *= (str[i]*37) % (bucks -> size());
    val++;}
  val %= tableCapacity;
  if(val < 0)
    return (val + tableCapacity);
  return val;}
  
