//Ranjodh Sandhu
//rss6py
//10/21/18
//hashTable.cpp

#include "hashTable.h"
#include "primenumber.h"

hashTable::hashTable(int size){
  bucks = new vector<list<string> >;
  maxNumOfWords = 0;
  size ++;
  unsigned int temp = size;
  if(checkprime(temp) == false){
    tableCapacity = getNextPrime(temp)*2;
    bucks -> resize(tableCapacity);}
  for(int i = 0; i < tableCapacity; i++){
    list<string> tempholder;
    bucks -> push_back(tempholder);}}

hashTable::~hashTable(){
  delete bucks;}

bool hashTable::isIn(const string & str) const{
  list<string> & newList = bucks -> at(hashFunc(str));
  return(find(newList.begin(), newList.end(), str) != (newList.end()));}

void hashTable::insert(const string & str){
  if(this->isIn(str))
      return;
    else{
    list<string> & newList = bucks -> at(hashFunc(str));
    newList.push_back(str);
    if(str.length() > maxNumOfWords){
      maxNumOfWords = str.length();}
  }}
unsigned int hashTable::hashFunc(const string & str) const{
  int val = 1;
  for(int i = 0; i < str.length(); i++){
    val *= (str[i]*3);
    val+= (str[i]);}
  val %= tableCapacity;
  if(val < 0)
    return (val + tableCapacity + 1);
  return val;}
  
