//Ranjodh Sandhu
//rss6py
//10/21/18
//hashTable.h

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <cstdlib>
#include <math.h>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>	
#include <list>

using namespace std;

class hashTable{
 public:
  hashTable(int size);
  ~hashTable();
  bool isIn(const string & str) const;
  void insert(const string & str);
 unsigned int hashFunc(const string & str) const;
  int tableCapacity;
  int maxNumOfWords;
 private:
  vector<list<string> > *bucks;
};
#endif
