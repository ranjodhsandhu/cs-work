//Ranjodh Sandhu
//rss6py
//10/25/18
//getWordInGrid.h

#ifndef GETWORDINGRID_H
#define GETWORDINGRID_H
#include <iostream>
using namespace std;
bool readInGrid (string filename, int &rows, int &cols);
char* getWordInGrid (int startRow, int startCol, int dir, int len,
                     int numRows, int numCols);
#endif
