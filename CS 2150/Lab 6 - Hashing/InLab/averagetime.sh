#!/bin/bash
#Ranjodh Sandhu
#rss6py
#10/22/18
#averagetime.sh

echo "Dictionary File: "
read dictionaryFile
echo "Grid File: "
read gridFile

run1=`./a.out $dictionaryFile $gridFile | tail -1`
run2=`./a.out $dictionaryFile $gridFile | tail -1`
run3=`./a.out $dictionaryFile $gridFile | tail -1`
run4=`./a.out $dictionaryFile $gridFile | tail -1`
run5=`./a.out $dictionaryFile $gridFile | tail -1`

echo "Runtimes in Milliseconds:"
echo $run1
echo $run2
echo $run3
echo $run4
echo $run5

echo "Average Runtime in Milliseconds:"
echo $(((run1 + run2 + run3 + run4 + run5)/ (5)))
