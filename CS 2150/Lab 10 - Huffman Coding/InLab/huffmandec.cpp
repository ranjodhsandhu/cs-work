//Ranjodh Sandhu
//rss6py
//11/27/18
//huffmandec.cpp
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include "huffmanNode.h"
using namespace std;
int main (int argc, char **argv) {
    if ( argc != 2 ) {
        cout << "Must supply the input file name as the only parameter" << endl;
        exit(1);
    }
    ifstream file(argv[1], ifstream::binary);
    if ( !file.is_open() ) {
        cout << "Unable to open file '" << argv[1] << "'." << endl;
        exit(2);
    }
    char ct[256];
    huffmanNode *one = new huffmanNode(0, '\0');
    while ( true ) {
        string character, prefix;
        file >> character;
	if(character[0] == '\n')
	  continue;
	if(character[0] == '\r')
	  continue;
        if (character[0] == '-' && (character.length() > 1)){
	  file.getline(ct, 255, '\n');
	  break;}
        if ( character == "space" )
	  character = " ";
	prefix = string(ct);
        file >> prefix;
       one -> createTree(one, prefix, character[0]);
    }
    char b;
    huffmanNode* two = one;
    while( (b = file.get()) != '-'){
      if(b != '0' && b != '1')
	  continue;
      if((two->left) && b == '0')
	two = two -> left;
      else if((two->right) && b == '1')
	two = two -> right;
      if(!(two->left) && !(two->right)){
	cout << two -> getC();
	two = one;}}
    cout << endl;
    file.close();
    return 0;}
        
