//Ranjodh Sandhu
//rss6py
//11/25/18
//heap.cpp
#include "huffmanNode.h"
#include "heap.h"
#include <vector>
#include <iostream>
using namespace std;

heap::heap() : size(0), heapp(101){}
heap::~heap(){
  makeEmpty();}
void heap::makeEmpty(){
  if(isEmpty()){
    cout << "Heap is already empty!" << endl;}
  else
    while(size > 0){
      deleteMin();}}
bool heap::isEmpty() const{
  if(size == 0)
    return true;
  else
    return false;}
int heap::getSize(){
  return size;}
void heap::insert(huffmanNode* myNode){
  if(size == heapp.size() - 1)
    heapp.resize(heapp.size() * 2);
  size++;
  int x = size;
  for(; (x > 1) && (*myNode) < *(heapp[x/2]); x/=2)
    heapp[x] = heapp[x/2];
  heapp[x] = myNode;}
void heap::deleteMin(){
  if (isEmpty() == true)
    cout << "Heap is empty!" << endl;
  heapp[1] = heapp[size--];
  percolateDown(1);}
void heap::percolateDown(int x){
  int y;
  huffmanNode* temporary = heapp[x];
  while(2*x <= size){
    y = 2*x;
    if(y!= size && (*(heapp[y+1])) < (*(heapp[y])))
      y++;
    if((*heapp[y]) < (*temporary)){
      heapp[x] = heapp[y];}
    else break;
    x = y;}
    heapp[x] = temporary;}

huffmanNode* heap::findMin() const{
  if(size == 0)
    cout << "findMin() called on empty heap!" << endl;
    return heapp[1];}
vector<huffmanNode*> heap::getVector(){
  return heapp;}
huffmanNode* heap::rdeleteMin(){
  if(isEmpty())
    cout << "Error!" << endl;
  huffmanNode *ret = heapp[1];
  heapp[1] = heapp[size--];
  percolateDown(1);
  return ret;}
