//Ranjodh Sandhu
//rss6py
//11/25/18
//huffmanNode.h
#ifndef HUFFMANNODE_H
#define HUFFMANNODE_H
#include <string>
using namespace std;

class huffmanNode{
 public:
  huffmanNode(int frq, char c);
  unsigned int getFreq() const;
  ~huffmanNode();
  char getC();
  string prefix;
  string getPrefix();
  bool operator<(const huffmanNode& myNode) const;
  char chrctr;
  int freq;
  huffmanNode* left;
  huffmanNode* right;};
#endif
