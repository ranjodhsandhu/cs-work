//Ranjodh Sandhu
//rss6py
//11/26/18
//huffmanTree.h

#ifndef HUFFMANTREE_H
#define HUFFMANTREE_H
#include <iostream>
#include "heap.h"
#include "huffmanNode.h"

using namespace std;

class huffmanTree{
 public:
  huffmanTree();
  ~huffmanTree();
  void setPrefix(huffmanNode* root, string b);
  huffmanNode *root;
  void printPrefix(huffmanNode* root, string b);
  heap createHuffmantree(heap h);
};
#endif
