//Ranjodh Sandhu
//rss6py
//11/25/18
//heap.h

#ifndef HEAP_H
#define HEAP_H
#include <iostream>
#include <string>
#include <vector>
#include "huffmanNode.h"


using namespace std;

class heap{
 public:
  heap();
  heap(int capacity);
  ~heap();
  void makeEmpty();
  bool isEmpty() const;
  int getSize();
  void insert(huffmanNode *myNode);
  void deleteMin();
  vector <huffmanNode*> getVector();
  huffmanNode* findMin() const;
  unsigned int size;
  huffmanNode* rdeleteMin();
  vector<huffmanNode*> heapp;
  void percolateDown(int x);
};
#endif
