//Ranjodh Sandhu
//rss6py
//11/26/18
//huffmanNode.cpp
#include <iostream>
#include "huffmanNode.h"
using namespace std;

huffmanNode::huffmanNode(int frq, char c){
  chrctr = c;
  prefix = "";
  freq = frq;
  left = NULL;
  right = NULL;}
huffmanNode::~huffmanNode(){
  delete right;
  delete left;}
char huffmanNode::getC(){
  return chrctr;}
unsigned int huffmanNode::getFreq() const{
  return freq;}
string huffmanNode::getPrefix(){
  return prefix;}
bool huffmanNode::operator<(const huffmanNode& myNode) const{
  return(this -> getFreq() < myNode.getFreq());}
