//Ranjodh Sandhu
//rss6py
//11/26/18
//huffmanNode.cpp
#include <iostream>
#include "huffmanNode.h"
using namespace std;

huffmanNode::huffmanNode(int frq, char c){
  chrctr = c;
  prefix = "";
  freq = frq;
  left = NULL;
  right = NULL;}
huffmanNode::~huffmanNode(){
  delete right;
  delete left;}
char huffmanNode::getC(){
  return chrctr;}
unsigned int huffmanNode::getFreq() const{
  return freq;}
string huffmanNode::getPrefix(){
  return prefix;}
bool huffmanNode::operator<(const huffmanNode& myNode) const{
  return(this -> getFreq() < myNode.getFreq());}
void huffmanNode::createTree(huffmanNode *myNode, string pfix, char c){
  if(pfix.length() == 0)
    myNode -> chrctr = c;
  if(pfix[0] == '0'){
    if(!(myNode -> left))
      myNode -> left = new huffmanNode(0, '\0');
    createTree(myNode -> left, pfix.substr(1, pfix.length()-1), c);}
  else if(pfix[0] == '1'){
    if(!(myNode->right))
      myNode->right = new huffmanNode(0, '\0');
    createTree(myNode -> right, pfix.substr(1, pfix.length()-1), c);}}

