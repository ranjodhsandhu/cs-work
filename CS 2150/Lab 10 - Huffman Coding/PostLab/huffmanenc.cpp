//Ranjodh Sandhu
//rss6py
//11/26/18
//huffmanenc.cpp

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "heap.h"
#include "huffmanTree.h"
#include "huffmanNode.h"
using namespace std;

int main(int argc, char *argv[]){
  if(argc != 2){
    cout << "Must supply the input file name as the one and only parameter" << endl;
    exit(1);}
  FILE *fp = fopen(argv[1], "r");
  if (!fp) {
        cout << "File '" << argv[1] << "' does not exist!" << endl;
        exit(2);
   }
   int ct[128];
   for(int i = 0; i < 128; i++){
     ct[i] = 0;}
    char g;
    int val;
    int numL = 0;
    while ( (g = fgetc(fp)) != EOF ){
      val = (int) g;
      if(val > 31 && val < 128){
	ct[val]++;
	numL++;}}
    heap heap1;
    int diffC = 0;
    for(int i = 1; i < 128; i++){
      if(ct[i] > 0){
	diffC++;
	huffmanNode* temp = new huffmanNode(ct[i], (char)i);
	heap1.insert(temp);}}
    huffmanTree* huffmanTree1 = new huffmanTree();
    heap heap2;
    heap2 = huffmanTree1 -> createHuffmantree(heap1);
    huffmanTree1 -> printPrefix(heap2.findMin(),"");
    vector<huffmanNode*> vector1 = heap1.getVector();
    int compressed = 0;
    cout << "----------------------------------------" << endl;
    huffmanTree1 -> setPrefix(heap2.findMin(), ""); 
    rewind(fp);

    while ((g = fgetc(fp)) != EOF){
      for(int i = 1; i <= heap1.getSize(); i++){
	if(g == heap1.heapp[i] -> chrctr ){
	  cout <<  heap1.heapp[i] -> prefix << " ";
	  compressed += vector1[i] -> getPrefix().size();}}}
      cout << endl;
      cout << "-----------------------------------------" << endl;
      int uncompressedBits = numL * 8;
      double ratio =((double) uncompressedBits)/compressed;
      double cost = ((double) compressed)/(uncompressedBits/8);
      cout<<"There are a total of " << numL  << " symbols that are encoded." << endl;
      cout<<"There are " << diffC << " distinct symbols used." << endl;
      cout<<"There were " << uncompressedBits << " bits in the original file." << endl;
      cout<<"There were " << compressed << " bits in the compressed file." << endl;
      cout<<"This gives a compression ratio of "<< ratio <<"." << endl;
      cout<<"The cost of the Huffman Tree is "<< cost <<" bits per character." << endl;
      fclose(fp);
    return 0;
}
