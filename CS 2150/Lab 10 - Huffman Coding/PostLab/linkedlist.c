//Ranjodh Sandhu
//rss6py
//11/28/18
//linkedlist.c
#include <stdio.h>
#include <stdlib.h>
struct Node{
  int value;
  struct Node *right;};
void createFirstNode(struct Node* root, int x){
  root -> value = x;
  root -> right = NULL;}
void insertNode(struct Node* first, int x){
  if(first -> right == NULL){
      struct Node* toInsert = (struct Node*)malloc(sizeof(struct Node));
      toInsert -> value = x;
      first -> right = toInsert;}
  else{
    insertNode(first -> right, x);}}
void printList(struct Node* root){
  while (root != NULL){
    printf("%d ", root -> value);
    root = root -> right;}
  printf("\n");}
int main(){
  int count;
  struct Node *root;
  printf("Enter how many values to input: ");
  scanf("%d", &count);
  root = (struct Node *)malloc(sizeof (struct Node) * count);
  for(int i = 0; i < count; i++){
    if(i == 0){
      int first;
      printf("Enter first value: ");
      scanf("%d", &first);
      createFirstNode(root, first);}
    else{
      int next;
      printf("Enter next value: ");
      scanf("%d", &next);
      insertNode(root, next);}}
  printList(root);
  free(root);
  return 0;}

       
