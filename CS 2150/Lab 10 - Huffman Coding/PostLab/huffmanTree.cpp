//Ranjodh Sandhu
//rss6py
//11/26/18
//huffmanTree.cpp
#include <iostream>
#include "huffmanNode.h"
#include "huffmanTree.h"
#include "heap.h"
using namespace std;

huffmanTree::huffmanTree(){
  root = NULL;}
huffmanTree::~huffmanTree(){
  delete root;}
void huffmanTree::setPrefix(huffmanNode *root, string b){
  if(!(root -> left) && !(root -> right))
    root -> prefix = b;
  if(root -> right)
    setPrefix(root -> right, b + "1");
  if(root -> left)
    setPrefix(root -> left, b + "0");}
void huffmanTree::printPrefix(huffmanNode *root, string b){
  if(root -> chrctr == ' ')
    cout << "space " << b << endl;
  if(!(root -> left) && !(root -> right) && root -> chrctr != ' ')
    cout << root -> chrctr << " " << b << endl;
  if(root -> left)
    printPrefix(root->left, b + "0");
  if(root -> right)
    printPrefix(root->right, b + "1");}
heap huffmanTree::createHuffmantree(heap h){
  while(h.getSize() >= 2){
    huffmanNode* one = h.rdeleteMin();
    huffmanNode* two = h.rdeleteMin();
    int newFreq = one -> getFreq() + two -> getFreq();
    huffmanNode* three = new huffmanNode(newFreq, '1');
    three -> left = one;
    three -> right = two;
    h.insert(three);}
    return h;}
